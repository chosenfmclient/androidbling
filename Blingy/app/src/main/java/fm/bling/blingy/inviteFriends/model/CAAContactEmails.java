package fm.bling.blingy.inviteFriends.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAContactEmails
 * Description:
 * Created by Dawidowicz Nadav on 6/29/15.
 * History:
 * ***********************************
 */
public class CAAContactEmails {

    @SerializedName("contact_emails")
    private List<String> listContacts;

    public CAAContactEmails(List<String> listContacts) {
        this.listContacts = listContacts;
    }

}
