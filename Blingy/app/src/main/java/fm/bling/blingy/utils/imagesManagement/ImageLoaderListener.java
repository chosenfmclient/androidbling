package fm.bling.blingy.utils.imagesManagement;

import fm.bling.blingy.utils.imagesManagement.views.URLImageView;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 2/28/16.
 * History:
 * ***********************************
 */
public interface ImageLoaderListener {

    void onImageLoaded(URLImageView urlImageView);
}
