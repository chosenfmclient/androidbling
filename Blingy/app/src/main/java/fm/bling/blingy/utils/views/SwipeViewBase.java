package fm.bling.blingy.utils.views;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 24/06/2016.
 * History:
 * ***********************************
 */
public abstract class SwipeViewBase extends FrameLayout
{
    private LinearLayout linearLayout;
    private boolean touched = false;

    public SwipeViewBase(Context context)
    {
        super(context);
        init();
    }

    public SwipeViewBase(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public SwipeViewBase(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init()
    {
        setBackgroundColor(Color.argb(0, 0, 0, 0));
        linearLayout = new LinearLayout(getContext());
//        linearLayout.setBackgroundColor(Color.BLUE);
        SwipeViewBase.super.addView(linearLayout, 0);
    }

    public void initialize()
    {
        View tempView = SwipeViewBase.super.getChildAt(1);

        SwipeViewBase.super.removeViewAt(1);
        linearLayout.addView(tempView);
        SwipeViewBase.this.invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent me)
    {
        switch (me.getAction())
        {
            case MotionEvent.ACTION_MOVE:
            {
                actionMove(me.getX(), me.getY());
                break;
            }
            case MotionEvent.ACTION_UP:
            {
                touched = false;
                actionUp(me.getX(), me.getY());
                break;
            }
            case MotionEvent.ACTION_DOWN:
            {
                touched = true;
                actionDown(me.getX(), me.getY());
                break;
            }
            case MotionEvent.ACTION_CANCEL:
            {
                actionCancel(me.getX(), me.getY());
                break;
            }
        }
        return true;
    }

    abstract public void actionMove(float x, float y);

    abstract public void actionDown(float x, float y);

    abstract public void actionUp(float x, float y);

    abstract public void actionCancel(float x, float y);

    @Override
    public void addView(View view)
    {
        linearLayout.addView(view);
    }

    @Override
    public void removeView(View view)
    {
        linearLayout.removeView(view);
    }

    @Override
    public void addView(View view, int position)
    {
        linearLayout.addView(view, position);
    }

    @Override
    public void removeViewAt(int position)
    {
        linearLayout.removeViewAt(position);
    }

    @Override
    public void setLayoutParams(ViewGroup.LayoutParams params)
    {
        int width = params.width;
        int height = params.height;
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        super.setLayoutParams(params);
        linearLayout.setLayoutParams(new LayoutParams(width, height));
    }

    /**
     * Describes how the child views are positioned. Defaults to GRAVITY_TOP. If
     * this layout has a VERTICAL orientation, this controls where all the child
     * views are placed if there is extra vertical space. If this layout has a
     * HORIZONTAL orientation, this controls the alignment of the children.
     *
     * @param gravity See {@link Gravity}
     */
    public void setGravity(int gravity)
    {
        linearLayout.setGravity(gravity);
    }

    /**
     * Should the layout be a column or a row.
     * @param orientation Pass {@link LinearLayout#HORIZONTAL} or {@link LinearLayout#VERTICAL}. Default
     * value is {@link LinearLayout#HORIZONTAL}.
     */
    public void setOrientation(int orientation)
    {
        linearLayout.setOrientation(orientation);
    }

    protected final void setLocation(float x, float y)
    {
        linearLayout.setX(x);
        linearLayout.setY(y);
    }

    public final void setRotation(float degree)
    {
        linearLayout.setRotation(degree);
    }

    public void addFragment(FragmentManager fragmentManager, Fragment fragment)
    {
        fragmentManager.beginTransaction().add(getId(), fragment).commit();
    }

    public boolean isTouched()
    {
        return touched;
    }
}