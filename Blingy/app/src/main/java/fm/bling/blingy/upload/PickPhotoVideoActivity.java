package fm.bling.blingy.upload;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.editVideo.VideoPlayerActivity;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.upload.adapters.PickPhotoVideoFolderRVAdapter;
import fm.bling.blingy.upload.adapters.PickPhotoVideoRVAdapter;
import fm.bling.blingy.upload.adapters.PickPhotoVideoVPAdapter;
import fm.bling.blingy.upload.adapters.SectionedRecyclerViewAdapter;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.dialogs.LoadingDialog;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 08/07/2016.
 * History:
 * ***********************************
 */
public class PickPhotoVideoActivity extends BaseActivity
{
    //    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RecyclerView folderView;
    private LinearLayout folderParentView;
    private Animation animOut;
    private Toolbar mToolbar;
    private Intent nextIntent;
    private boolean isClosed;

    private ArrayList<Drawable> icons;
    private ArrayList<String>packages;
    private ArrayList<MenuItem>menuItems;

    private PickPhotoVideoVPAdapter pickPhotoVideoVPAdapter;
    private PickPhotoVideoFolderRVAdapter pickPhotoVideoRVFolderAdapter;

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.video_photo_pick);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_blue));
            getWindow().setNavigationBarColor(Color.BLACK);
        }

        if(PickPhotoVideoVPAdapter.isProcessingPhotos() || PickPhotoVideoVPAdapter.isProcessingVideos())
            waitForProcessing();
        else
        {
            init();
            setActionBar();
        }
//        PickPhotoVideoVPAdapter.initializeFileManagerNow();
    }

    private void init()
    {
        nextIntent = getIntent();
        nextIntent.setClass(this, VideoPlayerActivity.class);
        nextIntent.putExtra(Constants.TYPE, Constants.UPLOAD);

        animOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        animOut.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation)
            {
                folderParentView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(pickPhotoVideoVPAdapter = new PickPhotoVideoVPAdapter(this, new PickPhotoVideoRVAdapter.onFolderSelectedListener()
        {
            @Override
            public void onFolderSelected(String folderName, String folderPath, byte fileManagerType, FileManager fileManager)
            {
                if(setFolderView(folderPath, fileManagerType, fileManager))
                    mToolbar.setTitle(folderName);
            }
        }));
        viewPager.setOffscreenPageLimit(1);
        ((TabLayout) findViewById(R.id.tab_layout)).setupWithViewPager(viewPager);

        folderParentView = (LinearLayout) findViewById(R.id.folder_parent_view);
        folderView= (RecyclerView) findViewById(R.id.folder_recycle_view);
        folderView.setHasFixedSize(true);
        folderView.setLayoutManager(new GridLayoutManager(this, 3));
        folderView.addItemDecoration(pickPhotoVideoVPAdapter.getFolderGridSpacingItemDecoration());
    }

    private void setActionBar()
    {
        mToolbar = getActionBarToolbar();
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });
        setSupportActionBar(mToolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_upload_photo_video, menu);

        MenuItem menuItem = menu.getItem(0);
        SubMenu subMenu = menuItem.getSubMenu();

        getAllPackages(subMenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int index = menuItems.indexOf(item);
        if(index != -1)
            startPackage(index);
        else if( item.getItemId() != R.id.action_more)
        {
            onBackPressed();
        }
        return true;
    }

    private void startPackage(int index)
    {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);//getPackageManager().getLaunchIntentForPackage(galleryPackAgeName);
        intent.setPackage(packages.get(index));
//        if(viewPager.getCurrentItem() == 0)
//            intent.setType("image/*");
//        else
            intent.setType("video/*");
        startActivityForResult(intent, Constants.PICK_VIDEO_REQUEST);
    }

    private void getAllPackages(SubMenu subMenu)
    {
        PackageManager packageManager = getPackageManager();

        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");

        ArrayList<ResolveInfo> pkgAppsListPhoto = new ArrayList(packageManager.queryIntentActivities(intent, 0));

        intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("video/*");
        ArrayList<ResolveInfo> pkgAppsListVideo = new ArrayList(packageManager.queryIntentActivities(intent, 0));

        icons = new ArrayList<>();
        packages = new ArrayList<>();
        menuItems = new ArrayList<>();

        boolean supportVideoToo;
        Drawable currentDrawable;
        for(ResolveInfo resolveInfoPhoto : pkgAppsListPhoto)
        {
            supportVideoToo = false;
            for(ResolveInfo resolveInfoVideo : pkgAppsListVideo)
            {
                if(resolveInfoVideo.activityInfo.packageName.equals(resolveInfoPhoto.activityInfo.packageName))
                {
                    supportVideoToo = true;
                    break;
                }
            }
            if(!supportVideoToo)continue;

            MenuItem menuItem = null;
            try
            {
                menuItem = subMenu.add(packageManager.getApplicationLabel(packageManager.getApplicationInfo(resolveInfoPhoto.activityInfo.packageName, 0)));
            }
            catch (PackageManager.NameNotFoundException e)
            {
                e.printStackTrace();
            }
            try
            {
                icons.add(currentDrawable = packageManager.getApplicationIcon(resolveInfoPhoto.activityInfo.packageName));
            }
            catch (PackageManager.NameNotFoundException e)
            {
                continue;
            }
            menuItem.setIcon(currentDrawable);
            menuItems.add(menuItem);
            packages.add(resolveInfoPhoto.activityInfo.packageName);
        }
    }

    /**
     *
     * @param folderPath
     * @param fileManagerType
     * @param fileManager
     * @return is folder set
     */
    private boolean setFolderView(String folderPath, byte fileManagerType, FileManager fileManager)
    {
        folderParentView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
        folderParentView.setVisibility(View.VISIBLE);

        FolderManager folderManager = fileManager.getFolderManager(folderPath);//new FolderManager(new File(folderPath), fileManagerType);

        pickPhotoVideoRVFolderAdapter = new PickPhotoVideoFolderRVAdapter(this, folderManager, new PickPhotoVideoFolderRVAdapter.OnFileSelectedListener()
        {
            @Override
            public void onFileSelected(String filePath, byte fileManagerType)
            {
                fileSelected(filePath, fileManagerType);
            }
        });

        ArrayList<SectionedRecyclerViewAdapter.Section> sections = new ArrayList<>();

        if (folderManager.getRecentPosition() != -1)
        {
            sections.add(new SectionedRecyclerViewAdapter.Section(0, "Recent"));
            sections.add(new SectionedRecyclerViewAdapter.Section(0, ""));
            sections.add(new SectionedRecyclerViewAdapter.Section(0, ""));
        }

        int addOn = 0;
        if (folderManager.getLastWeekPosition() != -1)
        {
            if(folderManager.getLastWeekPosition() != 0)
            {
                addOn = 3 - (folderManager.getRecentCount()%3);
                if(addOn != 3)
                    for(int i=0; i < addOn; i++)
                        sections.add(new SectionedRecyclerViewAdapter.Section(folderManager.getLastWeekPosition(), ""));
            }
            sections.add(new SectionedRecyclerViewAdapter.Section(folderManager.getLastWeekPosition(), "Last week"));
            sections.add(new SectionedRecyclerViewAdapter.Section(folderManager.getLastWeekPosition(), ""));
            sections.add(new SectionedRecyclerViewAdapter.Section(folderManager.getLastWeekPosition(), ""));
        }


        if (folderManager.getTwoWeeksPosition() != -1)
        {
            if(folderManager.getTwoWeeksPosition() != 0)
            {
                if(folderManager.getLastWeekPosition() != -1)
                    addOn = 3 - (folderManager.getLastWeekCount()%3);
                else addOn = 3 - (folderManager.getRecentCount()%3);

                if(addOn != 3)
                    for(int i=0; i < addOn; i++)
                        sections.add(new SectionedRecyclerViewAdapter.Section(folderManager.getTwoWeeksPosition(), ""));
            }
            sections.add(new SectionedRecyclerViewAdapter.Section(folderManager.getTwoWeeksPosition(), "Older"));
            sections.add(new SectionedRecyclerViewAdapter.Section(folderManager.getTwoWeeksPosition(), ""));
            sections.add(new SectionedRecyclerViewAdapter.Section(folderManager.getTwoWeeksPosition(), ""));
        }

        SectionedRecyclerViewAdapter mSectionedAdapter = new SectionedRecyclerViewAdapter(this, R.layout.section, R.id.section_text, pickPhotoVideoRVFolderAdapter);
        mSectionedAdapter.setSections(sections.toArray(new SectionedRecyclerViewAdapter.Section[0]));

        folderView.setAdapter(mSectionedAdapter);

        return true;
    }

    @Override
    public void onBackPressed()
    {
        if(folderParentView == null || folderParentView.getVisibility() != View.VISIBLE)
            finish();
        else
        {
            if(pickPhotoVideoRVFolderAdapter != null)
                try{pickPhotoVideoRVFolderAdapter.close();}catch (Throwable err){}
            pickPhotoVideoRVFolderAdapter = null;
            folderParentView.startAnimation(animOut);
            mToolbar.setTitle(R.string.upload_photo_or_video);
        }
    }

    private void fileSelected(String filePath, byte type)
    {
//        if(type == FileManager.PHOTO)
//        {
//            nextIntent.putExtra(Constants.PHOTO_MODE, true);
//            setPhotoFromResult(filePath, null, null);
//        }
//        else
//        {
            if(validateFileDuration(filePath, null)) {
                nextIntent.putExtra(Constants.PHOTO_MODE, false);
                nextIntent.putExtra("path", filePath);
                nextIntent.putExtra("uri", filePath);//Uri.fromFile(new File(filePath)).toString()
                startActivity(nextIntent);
            }
            else{
                new BaseDialog(this, "Video can't be uploaded","Video must be between 3 and 15 seconds long.").show();
            }
//        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        isClosed = true;
        try{close();}catch (Throwable err){}
    }

    private void close()
    {
        ((LinearLayout)findViewById(R.id.drawer_layout)).removeAllViews();
        viewPager = null;
        folderView = null;
        folderParentView = null;
        animOut = null;
        mToolbar = null;
        nextIntent = null;

        try{pickPhotoVideoVPAdapter.close();}catch(Throwable err){}
        pickPhotoVideoVPAdapter = null;

        if(pickPhotoVideoRVFolderAdapter != null)
            try{pickPhotoVideoRVFolderAdapter.close();}catch (Throwable err){}
        pickPhotoVideoRVFolderAdapter = null;

        try
        {
//            for(Drawable drawable:icons)
//                try{((BitmapDrawable)drawable).getBitmap().recycle();}catch (Throwable err1){}
            icons.clear();
        }catch (Throwable err){}
        icons = null;
        packages = null;
        menuItems = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        nextIntent.removeExtra("path");

        if (requestCode == Constants.PICK_VIDEO_REQUEST)
        {
            if (resultCode == RESULT_OK)
            {
                Uri uri = data.getData();
                String filePath = uri.getPath();

                if (validateFileDuration(null, uri)) {
                    nextIntent.putExtra("uri", uri.toString());
                    nextIntent.putExtra(Constants.TYPE, Constants.UPLOAD);

//                if (viewPager.getCurrentItem() == 0)//filePath.toLowerCase().contains("images/")
//                {
//                    nextIntent.putExtra(Constants.PHOTO_MODE, true);
//                    filePath = Utils.getPathFromMediaUri(this, uri);
//
//                    if (filePath != null && filePath.indexOf("http") == 0)
//                    {
//                        GetPhotoFromUri getPhotoFromUri = new GetPhotoFromUri(filePath, uri);
//                        getPhotoFromUri.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                    }
//                    else
//                    {
//                        if (filePath == null)
//                        {
//                            GetPhotoFromUri getPhotoFromUri = new GetPhotoFromUri(filePath, uri);
//                            getPhotoFromUri.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                        }
//                        else
//                        {
//                            setPhotoFromResult(filePath, uri, null);
//                        }
//                    }
//
//                }
//                else
//                {
                    nextIntent.putExtra(Constants.PHOTO_MODE, false);
                    startActivity(nextIntent);
                }
                else{
                    new BaseDialog(this, "Video can't be uploaded","Video must be between 3 and 15 seconds long.").show();
                }
//                }
            }
        }
    }

    private Bitmap scaledToFullHd(Bitmap bitmap)
    {
        if (bitmap.getWidth() > 1080 && bitmap.getHeight() > 1080) // greater than full hd
        {
            float scaleFactor = ((float) bitmap.getWidth()) / ((float) bitmap.getHeight());
            int width, height;
            if (bitmap.getWidth() > bitmap.getHeight())
            {
                height = 1080;
                width = (int) (1080f * scaleFactor);
            }
            else
            {
                width = 1080;
                height = (int) (1080f / scaleFactor);
            }

            Bitmap scaledBitmap;
            scaledBitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
            if (scaledBitmap != bitmap)
                bitmap.recycle();
            return scaledBitmap;
        }
        else return bitmap;
    }

    private void rotateUploadedPhoto(File uploadedPhoto, Uri imageUri, String originFilePath) throws Throwable
    {
        ExifInterface exifInterface = new ExifInterface(originFilePath);
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);

        if (orientation != -1)
        {
            Bitmap canvasBitmap = null;
            Matrix matrix = new Matrix();
            Bitmap bitmap = BitmapFactory.decodeFile(uploadedPhoto.getPath());

            switch (orientation)
            {
                case ExifInterface.ORIENTATION_ROTATE_180:
                    canvasBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                    matrix.postRotate(180, bitmap.getWidth() / 2, bitmap.getHeight() / 2);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    canvasBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
                    matrix.postRotate(90, bitmap.getHeight() / 2, bitmap.getWidth() / 2);
                    matrix.postTranslate((canvasBitmap.getWidth() - bitmap.getWidth()) / 2, (bitmap.getHeight() - canvasBitmap.getHeight()) / 2);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    canvasBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
                    matrix.postRotate(270, bitmap.getHeight() / 2, bitmap.getWidth() / 2);
                    matrix.postTranslate((bitmap.getWidth() - canvasBitmap.getWidth()) / 2, (canvasBitmap.getHeight() - bitmap.getHeight()) / 2);
                    break;
                case ExifInterface.ORIENTATION_UNDEFINED:
                    String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};
                    Cursor cursor = PickPhotoVideoActivity.this.getContentResolver().query(imageUri, projection, null, null, null);
                    orientation = -1;
                    if (cursor != null && cursor.moveToFirst())
                    {
                        orientation = cursor.getInt(0);
                        cursor.close();
                    }

                    if (orientation == 90)
                    {
                        canvasBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
                        matrix.postRotate(orientation, bitmap.getHeight() / 2, bitmap.getWidth() / 2);
                        matrix.postTranslate((canvasBitmap.getWidth() - bitmap.getWidth()) / 2, (bitmap.getHeight() - canvasBitmap.getHeight()) / 2);
                    }
                    else if (orientation == 270)
                    {
                        canvasBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
                        matrix.postRotate(orientation, bitmap.getHeight() / 2, bitmap.getWidth() / 2);
                        matrix.postTranslate((bitmap.getWidth() - canvasBitmap.getWidth()) / 2, (canvasBitmap.getHeight() - bitmap.getHeight()) / 2);
                    }
                    else if (orientation == 180)
                    {
                        canvasBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                        matrix.postRotate(180, bitmap.getWidth() / 2, bitmap.getHeight() / 2);
                    }

                    break;
            }

            if (canvasBitmap == null)
            {
                bitmap.recycle();
                return;
            }
            Canvas canvas = new Canvas(canvasBitmap);
            canvas.drawColor(Color.BLUE);
            canvas.drawBitmap(bitmap, matrix, null);

            bitmap.recycle();

            FileOutputStream fos = new FileOutputStream(uploadedPhoto);
            canvasBitmap.compress(Bitmap.CompressFormat.JPEG, 85, fos);
            fos.flush();
            fos.close();

            canvasBitmap.recycle();
        }
        exifInterface = new ExifInterface(uploadedPhoto.getPath());
        exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, 1 + "");
        exifInterface.saveAttributes();
    }

    private void setPhotoFromResult(String inPath, Uri uri, Bitmap bitmap)
    {
        if (bitmap == null)
        {
            bitmap = BitmapFactory.decodeFile(inPath);
        }
        bitmap = scaledToFullHd(bitmap);
        String outPath = App.VIDEO_WORKING_FOLDER + "/blingy_photo.jpg";
        File file = new File(outPath);

        try
        {
            FileOutputStream fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fos);

            fos.flush();
            fos.close();
            bitmap.recycle();

            try{rotateUploadedPhoto(new File(outPath), uri, inPath);}catch (Throwable err){}

//            rotateUploadedPhoto(file, uri);

            //path = outPath;
        }
        catch (Throwable er)
        {
            er.printStackTrace();
        }
        set4Photo(outPath);
    }

    private class GetPhotoFromUri extends AsyncTask<String, Void, Bitmap>
    {
        private LoadingDialog loadingDialog;
        private String inPath;
        private Uri uri;

        public GetPhotoFromUri(String inPath, Uri uri)
        {
            this.inPath = inPath;
            this.uri = uri;
        }

        @Override
        protected void onPreExecute()
        {
            loadingDialog = new LoadingDialog(PickPhotoVideoActivity.this,false,false);
        }

        @Override
        protected Bitmap doInBackground(String... params)
        {
            Bitmap bitmap = null;
            try
            {
                if(inPath != null)
                    bitmap = scaledToFullHd(BitmapFactory.decodeStream(new URL(inPath).openStream()));
                else
                    bitmap = scaledToFullHd(BitmapFactory.decodeStream(getContentResolver().openInputStream(Uri.parse(uri.toString()))));
            }
            catch (Throwable er)
            {
                er.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap)
        {
            setPhotoFromResult(inPath, uri, bitmap);
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    private void set4Photo(String path)
    {
        nextIntent.putExtra("path", path);
        nextIntent.putExtra("uri", path);//Uri.fromFile(new File(path)));
        startActivity(nextIntent);
    }

    private void waitForProcessing()
    {
        new WaitingTask().execute();
    }

    private class WaitingTask extends AsyncTask<Void, Void, Void>
    {
        private LoadingDialog loadingDialog;
        private boolean isCanceled = false;

        @Override
        public void onPreExecute()
        {
            loadingDialog = new LoadingDialog(PickPhotoVideoActivity.this,false,false);
            loadingDialog.setCancelable(true);
            loadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
            {
                @Override
                public void onCancel(DialogInterface dialog)
                {
                    isCanceled = true;
                    loadingDialog.dismiss();
                    loadingDialog = null;
                    PickPhotoVideoActivity.this.onBackPressed();
                }
            });
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            while(true)
            {
                if((PickPhotoVideoVPAdapter.isProcessingPhotos() || PickPhotoVideoVPAdapter.isProcessingVideos()) && !isClosed)
                    try{Thread.sleep(100);}catch (Throwable err){}
                else break;
            }
            return null;
        }

        @Override
        public void onPostExecute(Void result)
        {
            if(!isCanceled && !isClosed)
            {
                init();
                setActionBar();
                loadingDialog.dismiss();
                loadingDialog = null;
            }
        }
    }

    public boolean validateFileDuration(String path, Uri uri){
        try{
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            if(path != null)
                retriever.setDataSource(path);
            else
                retriever.setDataSource(this, uri);
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            long seconds = Long.parseLong(time) / 1000;
            return (seconds < 16 && seconds > 2);}
        catch (Throwable throwable){
            throwable.printStackTrace();
            return true;
        }
    }

}