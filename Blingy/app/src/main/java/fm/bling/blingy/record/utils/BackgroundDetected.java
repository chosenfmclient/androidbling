package fm.bling.blingy.record.utils;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 9/12/16.
 * History:
 * ***********************************
 */
public interface BackgroundDetected {

    void onBackgroundDetected(int detectionResponseCode);

}
