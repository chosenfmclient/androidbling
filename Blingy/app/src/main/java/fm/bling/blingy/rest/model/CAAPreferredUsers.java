package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Zach on 12/07/2016.
 */

public class CAAPreferredUsers {
    @SerializedName("data")
    ArrayList<User> users;

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }
}
