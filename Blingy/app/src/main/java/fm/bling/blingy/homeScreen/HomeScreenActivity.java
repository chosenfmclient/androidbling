package fm.bling.blingy.homeScreen;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.ScrollToTopBaseActivity;
import fm.bling.blingy.database.handler.LikesDataHandler;
import fm.bling.blingy.dialogs.LikeAnimationDialog;
import fm.bling.blingy.dialogs.listeners.UpdateLikeTotal;
import fm.bling.blingy.dialogs.model.CAAType;
import fm.bling.blingy.dialogs.share.ShareDialogVisibilityCallback;
import fm.bling.blingy.discover.model.Videos;
import fm.bling.blingy.gcm.CAAFCMRegistration;
import fm.bling.blingy.homeScreen.adapters.HomeTileRecyclerAdapter;
import fm.bling.blingy.homeScreen.model.VideoFeed;
import fm.bling.blingy.inviteFriends.InviteFriendsActivity;
import fm.bling.blingy.mashup.listeners.CreateCameoListener;
import fm.bling.blingy.profile.ProfileActivity;
import fm.bling.blingy.record.CreateVideoActivity;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.rest.model.CAALike;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.stateMachine.PermissionListener;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.Tic;
import fm.bling.blingy.utils.imagesManagement.views.ProgressBarView;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.utils.imagesManagement.views.WhiteProgressBarView;
import fm.bling.blingy.videoHome.VideoSocialDetailsActivity;
import fm.bling.blingy.videoHome.model.CAAVideo;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 15/09/2016.
 * History:
 * ***********************************
 */
public class HomeScreenActivity extends ScrollToTopBaseActivity implements UpdateLikeTotal, ShareDialogVisibilityCallback, PermissionListener {
    private static final String TAG = "HomeScreenActivity";

//    private final String TAG = "HomeScreenActivity";
//    private final int REQUEST_NOTIFICATION_UPDATE = 1;

    /**
     * Feed and Main Views
     */
    private LikeAnimationDialog likeDialog;
    private WhiteProgressBarView mProgressBarView;
    private View.OnTouchListener touchListener;
    private GestureDetector gestureDetector;
    private View.OnClickListener mSelectedClickListener, mLikeClickListener,
            mCommentClickListener, mProfileClickListener,
            mCloseTutorialListener, mRightIconListener;
    private CreateCameoListener mCreateCameoListener;
    private RecyclerView mRecycler;
    private HomeTileRecyclerAdapter mHomeFeedAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private int noData = 0;
    private int updatePosition = 0;
    private boolean calling = false;
    private boolean endReached = false;
    private boolean listInitialized = false;
    private boolean isShareVisible = false;

    /**
     * Player
     */
    private HomeTileRecyclerAdapter.DataObjectHolder mCurrentHolder;
    private MediaPlayer mMediaPlayer;
    private FrameLayout clickedView;
    private View lastProgressBar;
    private ScaledImageView lasViewImage;
    private String mLastClipURL = "";
    private boolean isPlaying = false;
    private boolean isPaused = false;
    private boolean isPrepared = false;
    private boolean isLoadingMusic = false;
    private CAAVideo mLastMyVideo;

    private BroadcastReceiver audioReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen_layout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_20));
        }
        getWindow().setBackgroundDrawable(null);

        mProgressBarView = (WhiteProgressBarView) findViewById(R.id.progress_bar);
        mProgressBarView.updateLayout();

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecycler = (RecyclerView) findViewById(R.id.feed_recycler);
        mRecycler.setLayoutManager(mLayoutManager);
        loadListeners();

        if (!CAAUserDataSingleton.getInstance().getSentGcmToken()) {
            new CAAFCMRegistration().onHandleIntent(this);
            CAAUserDataSingleton.getInstance().setSentGcmToken(true);
        }
        listenToAudioChanges();
    }

    @Override
    protected void onStart() {
        super.onStart();
        TrackingManager.getInstance().pageView(EventConstants.HOME_PAGE);
        getHomeScreen();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (lasViewImage != null)
            lasViewImage.setVisibility(View.VISIBLE);
        releaseMediaPlayer();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(mLastClipURL.equalsIgnoreCase(Constants.PLAY_DUET_TUTORIAL)) {
            try {
                isPlaying = false;
                lasViewImage.setImageResource(0);
                mCurrentHolder.fadeOutCloseButton();
                mCurrentHolder.fadeInRightIcon();
            }catch (Throwable throwable){
                Log.d(TAG,"Image resource error");
            }
        }

        mLastClipURL = "";
        if (lasViewImage != null) {
            lasViewImage.setVisibility(View.VISIBLE);
        }



        releaseMediaPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (audioReceiver != null)
            unregisterReceiver(audioReceiver);

    }

    private void listenToAudioChanges() {
        IntentFilter filter = new IntentFilter(Constants.AUDIO_FOCUS_CHANGED);
        audioReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int focus = intent.getIntExtra(Constants.FOCUS, -1);
                switch (focus) {
                    case AudioManager.AUDIOFOCUS_LOSS:
                        if (mMediaPlayer != null && !isPaused && isPlaying)
                            handleSingleTap();
                        break;
                }

            }
        };
        registerReceiver(audioReceiver, filter);
    }

    @Override
    public void scrollToTop() {
        try {
            releaseMediaPlayer();
            if (lastProgressBar != null)
                lastProgressBar.setVisibility(View.GONE);
            if (lasViewImage != null)
                lasViewImage.setVisibility(View.VISIBLE);
            Log.e(TAG, "SCroll to top");
            clickedView = null;
            if (mRecycler != null)
                mRecycler.smoothScrollToPosition(0);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private void loadListeners() {
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                RecyclerView.ViewHolder holder = null;
                if (newState == SCROLL_STATE_IDLE) {
                    int firstVisibleItem = ((LinearLayoutManager) mLayoutManager).findFirstCompletelyVisibleItemPosition();

                    if (firstVisibleItem == -1)
                        firstVisibleItem = ((LinearLayoutManager) mLayoutManager).findLastCompletelyVisibleItemPosition();

                    if (firstVisibleItem == -1) {
                        firstVisibleItem = ((LinearLayoutManager) mLayoutManager).findFirstVisibleItemPosition();
                        int lastVisibleItem = ((LinearLayoutManager) mLayoutManager).findLastVisibleItemPosition();

                        if (lastVisibleItem != -1) {
                            holder = mRecycler.findViewHolderForAdapterPosition(firstVisibleItem);
                            RecyclerView.ViewHolder secHolder = mRecycler.findViewHolderForAdapterPosition(lastVisibleItem);
                            if (holder instanceof HomeTileRecyclerAdapter.DataObjectHolder && secHolder instanceof HomeTileRecyclerAdapter.DataObjectHolder) {
                                int firstDif = Math.abs((App.HEIGHT / 2) - ((HomeTileRecyclerAdapter.DataObjectHolder) holder).getClipBottom());
                                int secDif = Math.abs((App.HEIGHT / 2) - ((HomeTileRecyclerAdapter.DataObjectHolder) secHolder).getClipTop());
                                if (secDif < firstDif) {
                                    holder = secHolder;
                                    Constants.lastPosition = lastVisibleItem;
                                }
                            }
                        }
                    }
                    if (Constants.lastPosition != firstVisibleItem)
                        isPaused = false;

                    if (holder == null)
                        holder = mRecycler.findViewHolderForAdapterPosition(Constants.lastPosition = firstVisibleItem);
                    if (holder != null && holder instanceof HomeTileRecyclerAdapter.DataObjectHolder) {
                        if (mLastClipURL.isEmpty())
                            ((HomeTileRecyclerAdapter.DataObjectHolder) holder).playVideo();
                        else {
                            if (!mLastClipURL.equalsIgnoreCase(Constants.PLAY_DUET_TUTORIAL) &&
                                    !(((HomeTileRecyclerAdapter.DataObjectHolder) holder).getUrl()).equalsIgnoreCase(mLastClipURL) && !isPaused)
                                ((HomeTileRecyclerAdapter.DataObjectHolder) holder).playVideo();
                        }
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //Handle Video playing
                if (isPlaying) {

                    if(clickedView == null)
                    {
                        releaseMediaPlayer();
                        return;
                    }
//                    if(clickedView == null)
//                        Snackbar.make(mRecycler,"clicked view is null",500).show();
                    int[] pos = new int[2];
                    clickedView.getLocationOnScreen(pos);
                    if (((pos[1] - App.STATUS_BAR_HEIGHT) <= ((clickedView.getHeight() * -1) + 200)) || pos[1] + 200 >= App.HEIGHT) {
                        if (mMediaPlayer != null) {
                            releaseMediaPlayer();
                        }
                        ((ScaledImageView) clickedView.getTag()).setVisibility(View.VISIBLE);
                        if(mLastClipURL.equals(Constants.PLAY_DUET_TUTORIAL)) {
                            isPlaying = false;
                            mLastClipURL = "";
                            mHandler.removeCallbacksAndMessages(null);
                            SharedPreferencesManager.getEditor().putBoolean(Constants.FIRST_DUET_TUTORIAL, false).apply();
                            mMultiImageLoader.DisplayImage(lasViewImage);
                            mCurrentHolder.fadeInRightIcon();
                            mCurrentHolder.fadeOutCloseButton();
                        }
                        clickedView = null;
//                        Log.e(TAG, "onScrolled");
                    }
                }

                //Next Page Section
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItem = ((LinearLayoutManager) mLayoutManager).findFirstVisibleItemPosition();
                if (listInitialized) {
                    final int lastItem = firstVisibleItem + visibleItemCount;
                    if (lastItem >= totalItemCount - 4 && !calling && !endReached) {
                        if (Constants.previousLastItem != lastItem) { //to avoid multiple calls for last item
                            Constants.previousLastItem = lastItem;
                            getNextPage();
                        }
                    }
                }
            }
        });

        gestureDetector = new GestureDetector(this, new GestureListener());

        touchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                clickedView = ((FrameLayout) v);
                if (event.getDownTime() == Constants.BLINGY_FAKE_DOWN_TIME) {
                    handleSingleTap();
                    return true;
                } else {
                    return gestureDetector.onTouchEvent(event);
                }
            }

        };

        mCreateCameoListener = new CreateCameoListener() {
            @Override
            public void onCreateCameo(CAAVideo video) {
                Intent recordVideo = new Intent(HomeScreenActivity.this, CreateVideoActivity.class);
                recordVideo.putExtra(Constants.PARENT_ID, video.getVideoId());
                recordVideo.putExtra(Constants.TYPE, Constants.CAMEO_TYPE);
                recordVideo.putExtra(Constants.CLIP_URL, video.getUrl());
                recordVideo.putExtra(Constants.CLIP_ID, video.getParent() == null ? video.getClipId() : video.getParent().getClipId());
                recordVideo.putExtra(Constants.CLIP_THUMBNAIL, video.getSongClipUrl());
                recordVideo.putExtra(Constants.SONG_NAME, video.getSongName());
                recordVideo.putExtra(Constants.ARTIST_NAME, video.getArtistName());
                getRecordPermissions(recordVideo);
            }
        };

        mCommentClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAAVideo video = ((CAAVideo) v.getTag());
                updatePosition = Constants.mVideoFeedData.indexOf(video);
                Intent socialComment = new Intent(HomeScreenActivity.this, VideoSocialDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.CAAVIDEO, video);
                bundle.putInt(Constants.SELECTED_TAB, VideoSocialDetailsActivity.COMMENTS_POS);
                socialComment.putExtra(Constants.BUNDLE, bundle);
                startActivityForResult(socialComment, Constants.REQ_VIDEO_UPDATE);
            }
        };

        mProfileClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CAAVideo video = ((CAAVideo) view.getTag());
                updatePosition = Constants.mVideoFeedData.indexOf(video);
                Intent profileIntent = new Intent(HomeScreenActivity.this, ProfileActivity.class);
                profileIntent.putExtra(Constants.USER_ID, video.getUser().getUserId());
                startActivityForResult(profileIntent, Constants.REQ_PROFILE_FOLLOWING);
            }
        };

        mLikeClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView mLikesCount = ((TextView) view.getTag());
                TextureView texture = ((TextureView) mLikesCount.getTag());
                CAAVideo video = ((CAAVideo) texture.getTag());
                if (video.getLikedByMe() == 1) {
                    ((ImageView) view).setImageDrawable(getResources().getDrawable(R.drawable.ic_like_24dp));
                    video.setLikedByMe(0);
                    video.setTotalLikes(video.getTotalLikes() - 1);
                    if (mLikesCount != null) {
                        mLikesCount.setText(String.valueOf(video.getTotalLikes()));
                    }
                    unLikeVideo(video);
                } else {
                    ((ImageView) view).setImageDrawable(getResources().getDrawable(R.drawable.ic_liked_24dp));
                    video.setLikedByMe(1);
                    video.setTotalLikes(video.getTotalLikes() + 1);
                    if (mLikesCount != null) {
                        mLikesCount.setText(String.valueOf(video.getTotalLikes()));
                    }
                    likeVideo(video);
                    int[] pos = new int[2];
                    texture.getLocationOnScreen(pos);
                    likeAnimation(video, pos, Constants.mVideoFeedData.indexOf(video));
                }
            }
        };


        mSelectedClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAAVideo caaVideo = ((CAAVideo) v.getTag());
                if (Utils.userIsMe(caaVideo.getUser().getUserId()) && !caaVideo.getType().equals(Constants.CAMEO_TYPE)) {
                    mLastMyVideo = caaVideo;
                    getContactsPermissions(HomeScreenActivity.this);
                } else {
                    Intent intent = new Intent(HomeScreenActivity.this, CreateVideoActivity.class);
                    intent.putExtra(Constants.CLIP_URL, caaVideo.getSongClipUrl());
                    intent.putExtra(Constants.CLIP_THUMBNAIL, caaVideo.getSongImageUrl().replace("100x100", "400x400"));
                    intent.putExtra(Constants.SONG_NAME, caaVideo.getSongName());
                    intent.putExtra(Constants.ARTIST_NAME, caaVideo.getArtistName());
                    intent.putExtra(Constants.CLIP_ID, caaVideo.getParent() == null ? caaVideo.getClipId() : caaVideo.getParent().getClipId());
                    TrackingManager.getInstance().tapShootNow("home_screen", caaVideo.getVideoId(), caaVideo.getArtistName(), caaVideo.getSongName(), Constants.mVideoFeedData.indexOf(caaVideo));
                    getRecordPermissions(intent);
                }
            }
        };

        mCloseTutorialListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.removeCallbacksAndMessages(null);
                SharedPreferencesManager.getEditor().putBoolean(Constants.PLAY_DUET_TUTORIAL, false).apply();
                mMultiImageLoader.DisplayImage(lasViewImage);
                mLastClipURL = "";
                isPlaying = false;
                handleSingleTap();
                mCurrentHolder.fadeOutCloseButton();
                mCurrentHolder.fadeInRightIcon();
                mHandler.removeCallbacksAndMessages(null);
            }
        };

        mRightIconListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferencesManager.getEditor().putBoolean(Constants.PLAY_DUET_TUTORIAL, true).apply();
                releaseMediaPlayer();
                handleSingleTap();
            }
        };
    }

    private void getHomeScreen() {
        if (Constants.mVideoFeedData == null) {
            SharedPreferencesManager.getEditor().putBoolean(Constants.ADDED_VIDEO_TO_10, false).apply();
            App.getUrlService().getHomeScreen(0, new Callback<VideoFeed>() {
                @Override
                public void success(VideoFeed videoFeed, Response response) {
                    addVideoForPosTen();
                    listInitialized = true;
                    mProgressBarView.setVisibility(View.GONE);
                    mRecycler.setVisibility(View.VISIBLE);
                    if (response != null && response.getStatus() == HttpsURLConnection.HTTP_NO_CONTENT) {
                        // show no content
                    } else {
                        if (Constants.mVideoFeedData == null)
                            Constants.mVideoFeedData = new ArrayList<>();

                        for (int i = 0; i < videoFeed.getVideoFeed().size(); i++)
                            Constants.mVideoFeedData.add(i, videoFeed.getVideoFeed().get(i));

                        Constants.mVideoFeedData.add(new CAAVideo()); // for loading
                        mHomeFeedAdapter = new HomeTileRecyclerAdapter(HomeScreenActivity.this, Constants.mVideoFeedData, getMultiImageLodaer(),
                                mSelectedClickListener, touchListener, mLikeClickListener,
                                mCommentClickListener, mProfileClickListener,
                                mRightIconListener, mCloseTutorialListener,mCreateCameoListener);
                        mRecycler.setAdapter(mHomeFeedAdapter);
                        if (!isFinishing())
                            mRecycler.smoothScrollBy(1, 1);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    mProgressBarView.setVisibility(View.GONE);
                }
            });
        } else {
            addVideoForPosTen();
            listInitialized = true;
            mProgressBarView.setVisibility(View.GONE);
            mRecycler.setVisibility(View.VISIBLE);
            if (mHomeFeedAdapter != null) {
                mHomeFeedAdapter.setImageLoaderManager(getMultiImageLodaer());
                mHomeFeedAdapter.notifyDataSetChanged();
            } else {
                mHomeFeedAdapter = new HomeTileRecyclerAdapter(this, Constants.mVideoFeedData, getMultiImageLodaer(),
                        mSelectedClickListener, touchListener, mLikeClickListener, mCommentClickListener, mProfileClickListener,
                        mRightIconListener, mCloseTutorialListener,mCreateCameoListener);
                mRecycler.setAdapter(mHomeFeedAdapter);
            }
            if (!isFinishing() && mLayoutManager != null) {
                mLayoutManager.scrollToPosition(Constants.lastPosition);
                mRecycler.smoothScrollBy(10, 10);
            }
        }
    }

    private void addVideoForPosTen() {
        if(!SharedPreferencesManager.getInstance().getBoolean(Constants.INVITED_TO_DUET, false) &&
                !SharedPreferencesManager.getInstance().getBoolean(Constants.ADDED_VIDEO_TO_10, false)){
            App.getUrlService().getMyVideos(new Callback<Videos>() {
                @Override
                public void success(Videos videos, Response response) {
                    if(videos != null && videos.getVideos() != null && videos.getVideos().size() > 0){
                        ArrayList<CAAVideo> caaVideos = videos.getVideos();
                        for(int i = 0;i < caaVideos.size(); i++){
                            if(!caaVideos.get(i).getType().equals(Constants.CAMEO_TYPE) && caaVideos.get(i).getStatus().equals(Constants.PUBLIC)){
                                SharedPreferencesManager.getEditor().putBoolean(Constants.ADDED_VIDEO_TO_10, true).apply();
                                Constants.mVideoFeedData.add(9,caaVideos.get(i));
                                mHomeFeedAdapter.notifyItemInserted(9);
                                break;
                            }
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }

    private void getNextPage() {
        Constants.pageNum++;
        calling = true;
        App.getUrlService().getHomeScreen(Constants.pageNum, new Callback<VideoFeed>() {
            @Override
            public void success(VideoFeed videoFeed, Response response) {
                try {
                    int startRange = mHomeFeedAdapter.getItemCount() - 1;
                    Constants.mVideoFeedData.remove(startRange);
                    mHomeFeedAdapter.notifyItemRemoved(startRange);

                    if (response.getStatus() != HttpURLConnection.HTTP_NO_CONTENT && videoFeed.getVideoFeed().size() != 0) {
                        if (mHomeFeedAdapter.getItemCount() < 1) {
                            noData++;
                            if (noData >= 2) {
                                endReached = true;
                            }
                        } else {
                            noData = 0;
                            int previousItemCount = mHomeFeedAdapter.getItemCount();
                            for (CAAVideo caaActivity : videoFeed.getVideoFeed()) {
                                Constants.mVideoFeedData.add(caaActivity);
                            }

                            Constants.mVideoFeedData.add(new CAAVideo());
                            mHomeFeedAdapter.notifyItemRangeInserted(previousItemCount, mHomeFeedAdapter.getItemCount());
                        }
                    } else {
                        endReached = true;
                    }
                    mHomeFeedAdapter.notifyItemRangeChanged(startRange, mHomeFeedAdapter.getItemCount(), null);
                } catch (Throwable err) {
                    err.printStackTrace();
                }
                calling = false;
            }

            @Override
            public void failure(RetrofitError error) {
                if (Constants.mVideoFeedData.get(Constants.mVideoFeedData.size() - 1) == null || Constants.mVideoFeedData.get(Constants.mVideoFeedData.size() - 1).getVideoId() == null) {
                    Constants.mVideoFeedData.remove(Constants.mVideoFeedData.size() - 1);
                    mHomeFeedAdapter.notifyItemRemoved(Constants.mVideoFeedData.size() - 1);
                }
            }
        });
    }

    private void attachSurface(final TextureView textureView, final ProgressBarView progressBar) {
        try {
            if (textureView.isAvailable()) {
                mMediaPlayer.setSurface(new Surface(textureView.getSurfaceTexture()));
                if (isPrepared && !isPlaying)
                    playMusic();
            } else {
                textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
                    @Override
                    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                        if (mMediaPlayer != null) {
                            mMediaPlayer.setSurface(new Surface(textureView.getSurfaceTexture()));
                            if (isPrepared && !isPlaying)
                                playMusic();
                        }
                    }

                    @Override
                    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                    }

                    @Override
                    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                        return false;
                    }

                    @Override
                    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
                    }
                });
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void MusicPrepare(TextureView textureView, ProgressBarView progressBar) {

        isLoadingMusic = true;
        isPrepared = false;
        isPlaying = false;
        CAAVideo video = ((CAAVideo) textureView.getTag());

        TrackingManager.getInstance().videoPlay("home_screen", video.getVideoId(), video.getArtistName(), video.getSongName(), Constants.mVideoFeedData.indexOf(video));
        mCurrentHolder = (HomeTileRecyclerAdapter.DataObjectHolder) mRecycler.getChildViewHolder((View) (clickedView.getParent().getParent()));

        final String url;

        if(SharedPreferencesManager.getInstance().getBoolean(Constants.FIRST_DUET_TUTORIAL, true)){
            if(Constants.mVideoFeedData.indexOf(video) == 2 && video.getType().equals(Constants.CAMEO_TYPE) ||
                    SharedPreferencesManager.getInstance().getBoolean(Constants.PLAY_DUET_TUTORIAL, false)){
                mLastClipURL = Constants.PLAY_DUET_TUTORIAL;
                showTutorialImage();
                return;
            }
            else {
                url = video.getUrl();
            }
        }
        else{
            if (video.getType().equals(Constants.CAMEO_TYPE) &&
                    SharedPreferencesManager.getInstance().getBoolean(Constants.PLAY_DUET_TUTORIAL, false)) {

                mLastClipURL = Constants.PLAY_DUET_TUTORIAL;
                showTutorialImage();
                return;

            } else {
                url = video.getUrl();
            }
        }



        isVideoStartedOrCanceled = false;
        slideOutBanner(5000);
        delayedTimer(progressBar);
        attachSurface(textureView, progressBar);

        // Listen for if the audio file can't be prepared
        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                releaseMediaPlayer();
                lastProgressBar.setVisibility(View.GONE);
                lasViewImage.setVisibility(View.VISIBLE);
                isVideoStartedOrCanceled = true;
                handleSingleTap();
                return false;
            }
        });
        // Attach to when audio file is prepared for playing
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                isPrepared = true;
                isVideoStartedOrCanceled = true;
                if (!isPlaying) {
//                    if (url.equals(Constants.PLAY_DUET_TUTORIAL)) {
//                        SharedPreferencesManager.getEditor().putBoolean(Constants.PLAY_DUET_TUTORIAL, false).apply();
//                        mCurrentHolder.fadeOutRightIcon();
//                        if (!SharedPreferencesManager.getInstance().getBoolean(Constants.FIRST_DUET_TUTORIAL, true))
//                            mCurrentHolder.fadeInCloseButton();
//                    }
                    mLastClipURL = url;
                    playMusic();
                }
            }
        });
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
//                if (url.equals(Constants.PLAY_DUET_TUTORIAL)) {
//                    SharedPreferencesManager.getEditor().putBoolean(Constants.FIRST_DUET_TUTORIAL, false).apply();
//                    mCurrentHolder.fadeInRightIcon();
//                    mCurrentHolder.fadeOutCloseButton();
//                    lasViewImage.setVisibility(View.VISIBLE);
//                    releaseMediaPlayer();
//                    handleSingleTap();
//                } else {
                    if (mp != null && isActivityVisible)
                        mp.start();
//                }

            }
        });

        mMediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    mp.setOnInfoListener(null);
                    isVideoStartedOrCanceled = true;
                    if (lasViewImage != null)
                        lasViewImage.setVisibility(View.GONE);
                    if (lastProgressBar != null)
                        lastProgressBar.setVisibility(View.GONE);
                }
                return true;
            }
        });

        try {

            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//            if (url.equals(Constants.PLAY_DUET_TUTORIAL)) {
//                Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.duet_popup_01);
//                mMediaPlayer.setDataSource(this, uri);
//            } else {

                mMediaPlayer.setDataSource(url);
//            }
            mMediaPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showTutorialImage()
    {
        isLoadingMusic = false;
        releaseMediaPlayer();
        slideOutBanner(100);
        lasViewImage.setVisibility(View.VISIBLE);
        lasViewImage.setImageResource(R.drawable.duet_popup_image_full);
        isPlaying = true;

        SharedPreferencesManager.getEditor().putBoolean(Constants.PLAY_DUET_TUTORIAL, false).apply();
        mCurrentHolder.fadeOutRightIcon();
        if (!SharedPreferencesManager.getInstance().getBoolean(Constants.FIRST_DUET_TUTORIAL, true))
            mCurrentHolder.fadeInCloseButton();

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!isActivityVisible)
                    return;
                if(mMultiImageLoader!= null && mLastClipURL.equalsIgnoreCase(Constants.PLAY_DUET_TUTORIAL)) {
                    isPlaying = false;

                    SharedPreferencesManager.getEditor().putBoolean(Constants.FIRST_DUET_TUTORIAL, false).apply();
                    mMultiImageLoader.DisplayImage(lasViewImage);
                    handleSingleTap();
                    mCurrentHolder.fadeInRightIcon();
                    mCurrentHolder.fadeOutCloseButton();
                }
            }
        }, Constants.DUET_TURORIAL_TIME);
    }

    private boolean isVideoStartedOrCanceled;
    private final int delayTime = 1000 * 2;

    private void delayedTimer(final ProgressBarView iProgressBar) {
        new Thread() {
            public void run() {
                int totalTime = 0;

                while (!isVideoStartedOrCanceled) {
                    try {
                        Thread.sleep(50);
                    } catch (Throwable err) {
                    }
                    totalTime += 50;
                    if (totalTime > delayTime)
                        break;
                }
                if (!isVideoStartedOrCanceled && iProgressBar == lastProgressBar) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showProgressBar(iProgressBar);
                        }
                    });
                }
            }
        }.start();
    }

    private void slideOutBanner(int delayTime) {
        mCurrentHolder.slideOutBanner(delayTime);
    }

    private void releaseMediaPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer = null;
            isPlaying = false;
            isLoadingMusic = false;
            isPrepared = false;
        }
    }

    private void playMusic() {
        if (!isFinishing()) {
            isPlaying = true;
            isPaused = false;
            isLoadingMusic = false;
            mMediaPlayer.start();
            gainAudioFocus();

            if (isShareVisible) {
                mMediaPlayer.pause();
                isPlaying = false;
            }
        }
    }

    private void showProgressBar(ProgressBarView progressBar) {
        if (lastProgressBar != null)
            lastProgressBar.setVisibility(View.INVISIBLE);

        if(!mLastClipURL.equals(Constants.PLAY_DUET_TUTORIAL))
            progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUpdateLikeTotal(int pos) {
        HomeTileRecyclerAdapter.DataObjectHolder holder = (HomeTileRecyclerAdapter.DataObjectHolder) mRecycler.findViewHolderForAdapterPosition(pos);
        holder.updateLikeTotal();
    }

    @Override
    public void onShareDialogShow() {
        isShareVisible = true;
        if (mMediaPlayer != null) {
            if (isPlaying) {
                isPlaying = false;
                mMediaPlayer.pause();
            }
        }
    }

    @Override
    public void onShareDialogDismissed() {
        isShareVisible = false;
        if (isPaused)
            return;

        if (mMediaPlayer != null) {
            if (!isLoadingMusic) {
                isPlaying = true;
                mMediaPlayer.start();
                gainAudioFocus();
                if (lasViewImage != null)
                    lasViewImage.setVisibility(View.GONE);
            }
        } else
            handleSingleTap();
    }

    @Override
    public void onPermissionGranted() {
        InviteFriendsToCameo();
    }

    @Override
    public void onPermissionDenied() {
    }

    private void InviteFriendsToCameo() {
        Intent inviteCameo = new Intent(this, InviteFriendsActivity.class);
        inviteCameo.putExtra(Constants.TYPE, Constants.CAMEO_TYPE);
        inviteCameo.putExtra(Constants.VIDEO_ID, mLastMyVideo.getVideoId());
        startActivity(inviteCameo);
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            handleSingleTap();
            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            handleDoubleTap();
            return true;
        }
    }

    private synchronized void handleSingleTap() {
        if((mLastClipURL.equals(Constants.PLAY_DUET_TUTORIAL) && isPlaying) || clickedView == null)
            return;

        if (isLoadingMusic) {
            try {
                lastProgressBar.setVisibility(View.GONE);
                lasViewImage.setVisibility(View.VISIBLE);
                mMediaPlayer.reset();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
        if (isActivityVisible) {
            ScaledImageView item = ((ScaledImageView) clickedView.getTag());
            ProgressBarView progressBar = ((ProgressBarView) item.getTag());
            TextureView textureView = ((TextureView) progressBar.getTag());
            if (mMediaPlayer != null) {
                if (isPlaying) {
                    if (item == lasViewImage) {
                        isPlaying = false;
                        isPaused = true;
                        mMediaPlayer.pause();
                    } else {
                        lastProgressBar.setVisibility(View.GONE);
                        lasViewImage.setVisibility(View.VISIBLE);
                        lasViewImage = item;
                        lastProgressBar = progressBar;
                        mMediaPlayer.reset();
                        MusicPrepare(textureView, progressBar);
                    }

                } else {
                    if (item == lasViewImage) {
                        playMusic();
                    } else {
                        lastProgressBar = progressBar;
                        lasViewImage = item;
                        mMediaPlayer.reset();
                        MusicPrepare(textureView, progressBar);
                    }
                }
            } else {
                lastProgressBar = progressBar;
                lasViewImage = item;
                mMediaPlayer = new MediaPlayer();
                MusicPrepare(textureView, progressBar);
            }
        }
    }

    private void handleDoubleTap() {
        int[] loc = new int[2];
        clickedView.getLocationOnScreen(loc);
        CAAVideo video = ((CAAVideo) (((TextureView) (((ProgressBarView) (((ScaledImageView) clickedView.getTag()).getTag())).getTag())).getTag()));
        likeAnimation(video, loc, Constants.mVideoFeedData.indexOf(video));
    }

    private void likeAnimation(CAAVideo video, int[] location, int pos) {
        if (likeDialog != null)
            likeDialog.dismiss();
        location[0] += (App.landscapItemWidth / 2);
        location[1] += (App.landscapitemHeight / 2);
        likeDialog = new LikeAnimationDialog(this, video, location, this, pos);
        likeDialog.show();
    }

    private void likeVideo(final CAAVideo video) {
        CAAType type = new CAAType("home");
        LikesDataHandler.getInstance().add(video.getVideoId());
        TrackingManager.getInstance().tapLikeButton(video.getVideoId(), "home_screen", true);
        App.getUrlService().postLikeVideo(video.getVideoId(), type, new Callback<CAALike>() {
            @Override
            public void success(CAALike like, Response response) {
            }

            @Override
            public void failure(RetrofitError error) {
                video.setLikedByMe(0);
                video.setTotalLikes(video.getTotalLikes() - 1);
            }
        });
    }

    private void unLikeVideo(final CAAVideo video) {
        LikesDataHandler.getInstance().remove(video.getVideoId());
        TrackingManager.getInstance().tapLikeButton(video.getVideoId(), "home_screen", false);
        App.getUrlService().deleteLikeVideo(video.getVideoId(), new Callback<String>() {
            @Override
            public void success(String s, Response response) {
            }

            @Override
            public void failure(RetrofitError error) {
                video.setLikedByMe(1);
                video.setTotalLikes(video.getTotalLikes() + 1);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQ_VIDEO_UPDATE && resultCode == Activity.RESULT_OK) {
            if (mHomeFeedAdapter != null && mRecycler != null && (updatePosition >= 0 && updatePosition < Constants.mVideoFeedData.size())) {
                if (data.hasExtra(Constants.COMMENT_TEXT)) {
                    Constants.mVideoFeedData.get(updatePosition).setTotalComments(data.getIntExtra(Constants.COMMENT_TEXT, Constants.mVideoFeedData.get(updatePosition).getTotalComments()));
                    mHomeFeedAdapter.notifyItemChanged(updatePosition);
                }
                if (data.hasExtra(Constants.LIKES_UPDATE)) {
                    Constants.mVideoFeedData.get(updatePosition).setTotalLikes(data.getIntExtra(Constants.LIKES_UPDATE, Constants.mVideoFeedData.get(updatePosition).getTotalLikes()));
                    mHomeFeedAdapter.notifyItemChanged(updatePosition);
                }
                updatePosition = -1;
            }
        }
        if (requestCode == Constants.REQ_PROFILE_FOLLOWING && resultCode == Activity.RESULT_OK) {
            if (mHomeFeedAdapter != null && mRecycler != null) {
                if (updatePosition >= 0 && updatePosition < Constants.mVideoFeedData.size()) {
                    Constants.mVideoFeedData.get(updatePosition).getUser().setFollowedByMe(data.getStringExtra(Constants.FOLLOW_BY_ME));
                    mHomeFeedAdapter.notifyItemChanged(updatePosition);
                    updatePosition = -1;
                }
            }
        }
    }

    @Override
    public int getSelfID() {
        return HOME_SCREEN;
    }
}