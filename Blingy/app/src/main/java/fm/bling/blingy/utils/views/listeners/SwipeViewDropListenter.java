package fm.bling.blingy.utils.views.listeners;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 24/06/2016.
 * History:
 * ***********************************
 */
public interface SwipeViewDropListenter
{

    void onViewDropped();

}