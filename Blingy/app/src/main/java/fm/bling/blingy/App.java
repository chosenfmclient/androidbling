package fm.bling.blingy;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Process;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.amplitude.api.Amplitude;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.facebook.FacebookSdk;
import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.util.ByteConstants;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.flurry.android.FlurryAgent;

import java.io.File;

import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.rest.RestClient;
import fm.bling.blingy.rest.UrlService;
import fm.bling.blingy.upload.adapters.PickPhotoVideoVPAdapter;
import fm.bling.blingy.utils.Constants;
import io.fabric.sdk.android.Fabric;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 14/09/2016.
 * Created by Oren Zakay on 02/10/2016.
 * History:
 * ***********************************
 */
public class App extends MultiDexApplication implements AudioManager.OnAudioFocusChangeListener{
    public static int WIDTH = Resources.getSystem().getDisplayMetrics().widthPixels, HEIGHT;

    public static float SCALE_X = ((float) WIDTH) / 1080f, SCALE_Y;

    public static int STATUS_BAR_HEIGHT, TOOLBAR_HEIGHT, MAIN_MARGIN, BLOCK_MARGIN;

    public static String MAIN_FOLDER_PATH, VIDEO_WORKING_FOLDER, TEMP_MEDIA_FOLDER;

    public static Typeface ROBOTO_BOLD, ROBOTO_MEDIUM, ROBOTO_REGULAR, ROBOTO_ITALIC, ROBOTO_LIGHT;

    public static int landscapItemWidth, landscapitemHeight;

    /**
     * Twitter keys
     */
    private static final String TWITTER_KEY = "hNQmv2GiL9JoqjRCPhEHjsmfs";
    private static final String TWITTER_SECRET = "ddgDHrML8cKPAqqVkV2Z4qefdtpABwShTbuHzxBDbE7Sj08J6g";

    public NetworkConnectivityListener mNetworkConnectivityListener;

    private static RestClient restClient;

    private static Context mAppContext;

    private boolean isFirstFocus = true;
    public static int sAudioFocus = 1;
    public static  AudioManager.OnAudioFocusChangeListener sAudioListener;

    public static boolean isCrashed = false;

    @Override
    public void onCreate()
    {
//        MultiDex.install(this);
//
        super.onCreate();
        mAppContext = getApplicationContext();

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException(Thread thread, Throwable err)
            {
                isCrashed = true;
                System.exit(0);
            }
        });

        AudioManager am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        sAudioFocus = am.requestAudioFocus(App.this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        sAudioListener = this;

        if (landscapItemWidth <= 0)
        {
            landscapItemWidth = App.WIDTH;
            landscapitemHeight = (int) (((float) landscapItemWidth) * (3f / 4f));
        }

        /** Internal Storage **/
        MAIN_FOLDER_PATH = getFilesDir().getPath();
        VIDEO_WORKING_FOLDER = MAIN_FOLDER_PATH + "/videos/";

//        if( BuildConfig.DEBUG)
//        {
//            /** External Storage **/
////            MAIN_FOLDER_PATH = android.os.Environment.getExternalStorageDirectory().getPath();
//            VIDEO_WORKING_FOLDER = android.os.Environment.getExternalStorageDirectory().getPath() + "/videos/";
//        }

        if (!new File(VIDEO_WORKING_FOLDER).exists())
            new File(VIDEO_WORKING_FOLDER).mkdir();

        TEMP_MEDIA_FOLDER = MAIN_FOLDER_PATH + "/tempMedia/";
        if (!new File(TEMP_MEDIA_FOLDER).exists())
            new File(TEMP_MEDIA_FOLDER).mkdir();


        if (TOOLBAR_HEIGHT <= 0) {
            TypedArray styledAttributes = getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
            TOOLBAR_HEIGHT = (int) styledAttributes.getDimension(0, 0);
            styledAttributes.recycle();
        }

        if (ROBOTO_REGULAR == null) {
            ROBOTO_ITALIC = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Italic.ttf");
            ROBOTO_MEDIUM = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
            ROBOTO_REGULAR = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            ROBOTO_BOLD = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");
            ROBOTO_LIGHT = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        }

        restClient = new RestClient();

        if (PackageManager.PERMISSION_DENIED != ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE))
            PickPhotoVideoVPAdapter.initializeFileManager();

        new Thread() {
            @Override
            public void run() {
                super.run();
                MAIN_MARGIN = getResources().getDimensionPixelSize(R.dimen.main_margin);
                BLOCK_MARGIN = getResources().getDimensionPixelSize(R.dimen.blocks_margin);

//                TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
//                Fabric.with(mAppContext, new Twitter(authConfig), new TweetComposer(), new Crashlytics(), new CrashlyticsNdk());
                Fabric.with(mAppContext, new Crashlytics(), new CrashlyticsNdk());

                FacebookSdk.sdkInitialize(getApplicationContext());

                FlurryAgent.setLogEnabled(true);
                FlurryAgent.setLogEvents(true);
                FlurryAgent.setVersionName(BuildConfig.VERSION_NAME);
                FlurryAgent.setLogLevel(Log.VERBOSE);
                FlurryAgent.init(mAppContext, Constants.FLURRY_KEY);// must be last one


                DiskCacheConfig diskCacheConfig = DiskCacheConfig.newBuilder(mAppContext).setBaseDirectoryPath(mAppContext.getExternalCacheDir())
                        .setBaseDirectoryName("v1")
                        .setMaxCacheSize(30 * ByteConstants.MB)
                        .setMaxCacheSizeOnLowDiskSpace(20 * ByteConstants.MB)
                        .setMaxCacheSizeOnVeryLowDiskSpace(5 * ByteConstants.MB)
                        .setVersion(1)
                        .build();

                ImagePipelineConfig config = ImagePipelineConfig.newBuilder(mAppContext)
                        .setMainDiskCacheConfig(diskCacheConfig)
                        .setDownsampleEnabled(true)
                        .build();

                Fresco.initialize(getApplicationContext(), config);
                Amplitude.getInstance().initialize(mAppContext, getString(R.string.amplitude_key)).enableForegroundTracking(App.this);
            }
        }.start();


        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                if (mNetworkConnectivityListener == null) {
                    mNetworkConnectivityListener = new NetworkConnectivityListener();
                    mNetworkConnectivityListener.startListening(activity);
                }
            }

            @Override
            public void onActivityStarted(Activity activity) {
                if (mNetworkConnectivityListener == null) {
                    mNetworkConnectivityListener = new NetworkConnectivityListener();
                    mNetworkConnectivityListener.startListening(activity);
                }

                if (activity != null) {
                    if (NetworkConnectivityListener.mListening)
                        NetworkConnectivityListener.isConnectedWithDialog(activity);
                }
            }

            @Override
            public void onActivityResumed(Activity activity) {
                if (mNetworkConnectivityListener != null)
                    mNetworkConnectivityListener.setCurrentContext(activity);
            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {
                if (!Utils.isAppOnForeground(activity)) {
                    if (mNetworkConnectivityListener != null && NetworkConnectivityListener.mListening) {
                        mNetworkConnectivityListener.stopListening();
                        mNetworkConnectivityListener = null;
                    }
                }
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
    }


    public static UrlService getUrlService() {
        return restClient.getUrlService();
    }

    public static Context getAppContext() {
        return mAppContext;
    }

//    @Override
//    protected void attachBaseContext(Context base)
//    {
//        super.attachBaseContext(base);
//        MultiDex.install(this);
//    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public static AudioManager.OnAudioFocusChangeListener getAudioListener(){
        return sAudioListener;
    }

    public static boolean shouldGainAudioFocus(){
        return sAudioFocus != AudioManager.AUDIOFOCUS_GAIN;
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_LOSS:
                Intent intent = new Intent();
                intent.setAction(Constants.AUDIO_FOCUS_CHANGED);
                intent.putExtra(Constants.FOCUS, -1);
                sAudioFocus = -1;
                sendBroadcast(intent);
                break;
        }
    }
}