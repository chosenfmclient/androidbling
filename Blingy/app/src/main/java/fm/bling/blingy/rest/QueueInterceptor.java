package fm.bling.blingy.rest;


import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import fm.bling.blingy.singletones.CAAUserDataSingleton;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 22/02/17.
 * History:
 * ***********************************
 */
public class QueueInterceptor implements Interceptor {

    private static final String TAG = "QueueInterceptor";

    @Override
    public Response intercept(Chain chain) throws IOException {
        String url = chain.request().urlString();

        if (BRequestInterceptor.waitingForAccess && isNotTokenRequest(url)) {
//            Log.d(TAG,"old token = " + CAAUserDataSingleton.getInstance().getAccessToken());
            while (BRequestInterceptor.waitingForAccess) {
//                Log.d(TAG,"waitingForAccess");
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Request oldReq = chain.request();
            String oldUrl = oldReq.urlString();
            int index;

//            Log.d(TAG,"new token = " + CAAUserDataSingleton.getInstance().getAccessToken());

            if ((index = oldUrl.indexOf("access_token")) > 0) {
                String urlWithoutAccessToken = oldUrl.substring(0, index);
                StringBuilder newUrl = new StringBuilder(urlWithoutAccessToken).append("access_token=").append(CAAUserDataSingleton.getInstance().getAccessToken());
                Request request = oldReq.newBuilder().method(oldReq.method(), oldReq.body()).headers(oldReq.headers()).url(newUrl.toString()).build();
                return chain.proceed(request);
//                .append("&v=").append(RestClient.version);
            }
        }
        return chain.proceed(chain.request());
    }

    private boolean isNotTokenRequest(String url) {
        return  !url.contains("/google-token") && !url.contains("/token");
    }
}
