package fm.bling.blingy.dialogs.share.workers.listeners;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 5/22/16.
 * History:
 * ***********************************
 */
public interface onRenderInstagramFinished {
    void onRenderInstagramFinished(boolean result);
}
