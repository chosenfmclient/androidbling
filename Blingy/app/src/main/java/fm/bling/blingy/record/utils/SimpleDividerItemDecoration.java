package fm.bling.blingy.record.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import fm.bling.blingy.R;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 6/13/16.
 * History:
 * ***********************************
 */
public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
    private Drawable mDivider;
    private int margin;

    public SimpleDividerItemDecoration(Context context, int dividerColorResID, boolean addMargin) {
        mDivider = ContextCompat.getDrawable(context, dividerColorResID);
        if(addMargin)
            margin = context.getResources().getDimensionPixelSize(R.dimen.main_margin);
    }

    public SimpleDividerItemDecoration(Context context, int dividerColorResID) {
        mDivider = ContextCompat.getDrawable(context, dividerColorResID);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {


        int left = parent.getPaddingLeft() + margin;
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            // find child if is SectionHolder so we don't need the decoration.
//            View nextChild = i + 1 > childCount ? null : parent.getChildAt(i + 1);

//            if(parent.getChildViewHolder(child) instanceof InviteMashupAdapter.SectionHolder ||
//                    (nextChild != null && parent.getChildViewHolder(nextChild) instanceof InviteMashupAdapter.SectionHolder ))
//                continue;

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }
}
