package fm.bling.blingy.mashup.model;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 17/01/17.
 * History:
 * ***********************************
 */
public class MashupDetails {

    private String mashupID;
    private int step;
    private int videosNum;

    public MashupDetails(String mashupID, int step, int videosNum) {
        this.mashupID = mashupID;
        this.step = step;
        this.videosNum = videosNum;
    }

    public String getMashupID() {
        return mashupID;
    }

    public int getStep() {
        return step;
    }

    public int getVideosNum() {
        return videosNum;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public void setVideosNum(int videosNum) {
        this.videosNum = videosNum;
    }

    @Override
    public String toString() {
        return "State>>>>Mash>>details>>>" + getMashupID() + ">>>" + getStep() + ">>>" + getVideosNum();
    }
}
