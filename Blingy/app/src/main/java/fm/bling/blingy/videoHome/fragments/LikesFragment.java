package fm.bling.blingy.videoHome.fragments;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.database.handler.LikesDataHandler;
import fm.bling.blingy.dialogs.model.CAAType;
import fm.bling.blingy.profile.ProfileActivity;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.record.utils.SimpleDividerItemDecoration;
import fm.bling.blingy.rest.model.CAALike;
import fm.bling.blingy.rest.model.CAALikeParams;
import fm.bling.blingy.rest.model.CAALikeResponse;

import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.AdaptersDataTypes;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.videoHome.VideoHomeActivity;
import fm.bling.blingy.videoHome.adapters.LikesRecyclerAdapter;
import fm.bling.blingy.videoHome.model.CAAVideo;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 9/5/16.
 * History:
 * ***********************************
 */
public class LikesFragment extends Fragment {

    private String mVideoID;
    private CAAVideo mVideo;
    private String mMediaType;

    private ArrayList<CAALikeParams> items;
    private CAALikeParams singleItem;
    private int previousLastItem = 0;
    private int pageNumber = 0;
    private int noData = 0;
    private boolean listInitialized = false;
    private boolean calling = false;
    private boolean endReached = false;
    private int totalLikes;


    private LikesRecyclerAdapter adapter;


    private ObjectAnimator anim;
    private RecyclerView mRecycler;
    private ImageView spinner;
    private FrameLayout listViewEmpty;
    private FrameLayout loadingLayout;
    private TextView mTextTotal;
    private RecyclerView.LayoutManager mLayoutManager;

    private View.OnClickListener mItemClickListener;

    public static LikesFragment newInstance(String videoId, CAAVideo video, String mediaType) {
        LikesFragment myFragment = new LikesFragment();
        myFragment.mVideoID = videoId;
        myFragment.mVideo = video;
        myFragment.mMediaType = mediaType;
        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.like_fragment, null);
        mRecycler = (RecyclerView) v.findViewById(R.id.list_view);
        mTextTotal = (TextView)v.findViewById(R.id.text_total);
        loadingLayout = (FrameLayout)v.findViewById(R.id.loading_layout);
        listViewEmpty = (FrameLayout)v.findViewById(R.id.likes_empty);
        spinner = (ImageView)v.findViewById(R.id.spinner);

        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext(), R.drawable.line_divider_white_50, true));

        removeEmptyLayout();
        loadListeners();
        loadAnimation();
        return v;
    }

    private void loadListeners() {
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItem = ((LinearLayoutManager)mLayoutManager).findFirstVisibleItemPosition();
                if (listInitialized) {
                    final int lastItem = firstVisibleItem + visibleItemCount;
                    if (lastItem >= totalItemCount - 4 && !calling && !endReached) {
                        if (previousLastItem != lastItem) { //to avoid multiple calls for last item
                            Log.d("Scroll", "Last:" + lastItem);
                            previousLastItem = lastItem;
                            getNextPage();
                        }
                    }
                }
            }
        });
        mItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int)v.getTag();
                if(adapter.getItemViewType(position) != AdaptersDataTypes.TYPE_LOADING ) {
                    CAALikeParams caaLikeParams = items.get(position);
                    User newObj = caaLikeParams.getUser();
                    String userID = newObj.getUserId();
                    Intent i = new Intent(getActivity(), ProfileActivity.class);
                    i.putExtra(Constants.USER_ID, userID);
                    startActivity(i);
                }
            }
        };

    }

    private void getNextPage() {
        pageNumber++;
        calling = true;
        App.getUrlService().getVideoLikes(mVideoID, pageNumber, new Callback<CAALikeResponse>() {
            @Override
            public void success(CAALikeResponse cAALikeResponse, Response response)
            {
                if (response.getStatus() != HttpURLConnection.HTTP_NO_CONTENT)
                {
                    if (cAALikeResponse.getData().size() < 1)
                    {
                        noData++;
                        if (noData >= 2)
                        {
                            endReached = true;
                            items.remove(items.size() - 1);
                            adapter.notifyItemRemoved(items.size());
//                            adapter.notifyDataSetChanged();
                        }
                        else
                        {
                            getNextPage();
                        }
                    }
                    else
                    {
                        items.remove(items.size() - 1);
                        adapter.notifyItemRemoved(items.size());
                        noData = 0;
                        int partitionStart = items.size();
                        for (CAALikeParams cAALikeParams : cAALikeResponse.getData())
                        {
                            items.add(cAALikeParams);
                        }
                        items.add(new CAALikeParams());
                        adapter.notifyItemRangeInserted(partitionStart, items.size() - partitionStart);
//                        adapter.notifyDataSetChanged();
                    }
                }
                else
                {
                    endReached = true;
                    items.remove(items.size() - 1);
                    adapter.notifyItemRemoved(items.size());//DataSetChanged();
                }
                calling = false;
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getVideoLikes();

    }

    private void getVideoLikes() {
        App.getUrlService().getVideoLikes(mVideoID, pageNumber, new Callback<CAALikeResponse>() {
            @Override
            public void success(CAALikeResponse cAALikeResponse, Response response) {
                canelAnimation();
                if(mRecycler == null || getActivity() == null)
                    return;
                if(cAALikeResponse != null && cAALikeResponse.getData().size() > 0) {
                    removeEmptyLayout();
                    items = cAALikeResponse.getData();
                    items.add(new CAALikeParams());
                    adapter = new LikesRecyclerAdapter(items, mItemClickListener, getActivity());
                    mRecycler.setAdapter(adapter);
                    totalLikes = Integer.valueOf(cAALikeResponse.getTotalLikes());
                    listInitialized = true;

                    StringBuilder totalText = new StringBuilder();
                    if(totalLikes == 1)
                        totalText.append(totalLikes).append(" Like");
                    else{
                        totalText.append(totalLikes).append(" Likes");
                    }
                    mTextTotal.setText(totalText);
                }
                else {
                    showEmptyLayout();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                canelAnimation();
            }
        });
    }

    private void showEmptyLayout() {
        mRecycler.setVisibility(View.GONE);
        listViewEmpty.setVisibility(View.VISIBLE);
        listViewEmpty.findViewById(R.id.linearLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                items = new ArrayList();
                items.add(new CAALikeParams(Utils.getMyUser(),mVideoID, "0"));
                adapter = new LikesRecyclerAdapter(items, mItemClickListener, getActivity());
                mRecycler.setAdapter(adapter);
                mRecycler.setVisibility(View.VISIBLE);
                removeEmptyLayout();
                mVideo.setLikedByMe(1);
                mVideo.setTotalLikes(1);
                CAAType type = new CAAType(getActivity().getApplicationContext() instanceof VideoHomeActivity ? "video_home" : "home");
                LikesDataHandler.getInstance().add(mVideoID);
                getActivity().setResult(Activity.RESULT_OK, new Intent().putExtra(Constants.LIKES_UPDATE, 1));
                TrackingManager.getInstance().tapLikeButton(mVideoID,getActivity().getApplicationContext() instanceof VideoHomeActivity ? "video_home" : "home_screen",true);
                App.getUrlService().postLikeVideo(mVideoID, type, new Callback<CAALike>() {
                    @Override
                    public void success(CAALike like, Response response) {

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        mVideo.setLikedByMe(0);
                        mVideo.setTotalLikes(mVideo.getTotalLikes() - 1);
                        LikesDataHandler.getInstance().remove(mVideoID);
                    }
                });
            }
        });
    }
    private  void removeEmptyLayout(){
        listViewEmpty.setVisibility(View.GONE);
    }

    public void canelAnimation() {
        if(loadingLayout != null)
            loadingLayout.setVisibility(View.GONE);
        anim.cancel();
    }

    private void loadAnimation() {
        anim = ObjectAnimator.ofFloat(spinner, "rotation", 0f, 359f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setDuration(1300);
        anim.setRepeatCount(Animation.INFINITE);
        anim.start();
    }
}
