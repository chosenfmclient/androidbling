package fm.bling.blingy.profile.fragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.database.handler.DuetInvitedHandler;
import fm.bling.blingy.database.handler.FollowingDataHandler;
import fm.bling.blingy.database.handler.LikesDataHandler;
import fm.bling.blingy.dialogs.listeners.OnDoneLoadingListener;
import fm.bling.blingy.inviteFriends.model.CAAFollowAll;
import fm.bling.blingy.inviteFriends.model.ContactInfo;
import fm.bling.blingy.mashup.listeners.CameoInviteListener;
import fm.bling.blingy.mashup.model.Invitee;
import fm.bling.blingy.mashup.model.InviteeContact;
import fm.bling.blingy.profile.ProfileActivity;
import fm.bling.blingy.profile.UserConnectionsActivity2;
import fm.bling.blingy.profile.adapters.UserConnectionRecyclerAdapter;
import fm.bling.blingy.profile.model.CAAApiUsers;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.record.utils.SimpleDividerItemDecoration;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.dialogs.LoadingDialog;
import fm.bling.blingy.utils.imagesManagement.MultiImageLoader;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 11/30/16.
 * History:
 * ***********************************
 */
public class FollowingFragment extends BaseSearchFragment implements CameoInviteListener<User> {

    /**
     * Main Views
     */
    private RecyclerView mRecycler;
    private FrameLayout listViewEmpty;
    private TextView mTextTotal;
    private RecyclerView.LayoutManager mLayoutManager;
    private UserConnectionRecyclerAdapter adapter;
    private View.OnClickListener mItemClickListener;
    private View.OnClickListener mFollowClickListener;
    private TextView followAll;

    private OnDoneLoadingListener onDoneLoadingListener;
    private MultiImageLoader mMultiImageLoader;
    private ArrayList<User> items;
    private ArrayList<User> searchDataSet;
    private String mUserId;
    private int mAmount;
    private boolean isInviteFriends = false;

    private int previousLastItem = 0;
    private int pageNumber = 0;
    private boolean calling = false;
    private boolean endPages = false;
    private boolean listInitialized = false;
    private int lastSearchLength;

    private ArrayList<InviteeContact> sendList;
    private ArrayList<InviteeContact> invitedDuetList;
    private boolean isCameo = false;
    private String mVideoID;

    public static FollowingFragment newInstance(String user_id, int amount ,OnDoneLoadingListener onDoneLoadingListener) {
        FollowingFragment myFragment = new FollowingFragment();
        myFragment.mUserId = user_id;
        myFragment.mAmount = amount;
        myFragment.onDoneLoadingListener = onDoneLoadingListener;
        return myFragment;
    }

    public static FollowingFragment newInstance(String user_id, boolean inviteFriends, List<User> blingyFriends, boolean isCameo, String mVideoID) {
        FollowingFragment myFragment = new FollowingFragment();
        myFragment.mUserId = user_id;
        myFragment.mAmount = FollowingDataHandler.getInstance().size();
        myFragment.isCameo = isCameo;
        myFragment.mVideoID = mVideoID;
        myFragment.isInviteFriends = inviteFriends;
        myFragment.searchDataSet = new ArrayList<>();
        myFragment.items = new ArrayList<>();
        if (blingyFriends != null) {
            for (int i = 0; i < blingyFriends.size(); i++) {
                myFragment.searchDataSet.add(blingyFriends.get(i));
                myFragment.items.add(blingyFriends.get(i));
            }
        }

        return myFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isInviteFriends)
            getFollowing();
    }


    @Override
    public void onResume() {
        super.onResume();
        mMultiImageLoader = ((BaseActivity) getActivity()).getMultiImageLodaer();
        if (adapter != null) {
            adapter.setmMultiImageLoader(mMultiImageLoader);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isCameo && sendList != null && sendList.size() > 0)
            invitePeople();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = null;
        if (isInviteFriends || isCameo) {
            v = inflater.inflate(R.layout.follow_invite_fragment, null);
            followAll = (TextView) v.findViewById(R.id.follow_all);
            followAll.setVisibility(View.GONE);
            followAll.setText(isCameo ? "+ Invite All" : "+ Follow All");
            if (isCameo) {
                followAll.setBackgroundResource(R.drawable.button_shape_pink);
                invitedDuetList = DuetInvitedHandler.getInstance().getAll(mVideoID);
            }
            followAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isCameo) {
                        int count = 0;
                        for (User user : searchDataSet) {
                            if (user.isCameoInvited())
                                continue;
                            addToInvite(user);
                            count++;
                        }
                        showToastInvitedCount(count);
                        adapter.notifyDataSetChanged();
                        invitePeople();
                    } else
                        followAllContacts();
                }
            });
        } else {
            v = inflater.inflate(R.layout.user_connection_fragment, null);
        }

        mRecycler = (RecyclerView) v.findViewById(R.id.connections_recycler);
        mTextTotal = (TextView) v.findViewById(R.id.text_total);
        listViewEmpty = (FrameLayout) v.findViewById(R.id.connection_empty);
        ((TextView) listViewEmpty.findViewById(R.id.empty_text)).setText(R.string.no_following);


        if (isInviteFriends)
            mTextTotal.setText(searchDataSet.size() + " Contacts");


        mItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User friend = ((User) v.getTag());
                if (isCameo) {
                    if (!friend.isCameoInvited()) {
                        UserConnectionRecyclerAdapter.DataObjectHolder holder = (UserConnectionRecyclerAdapter.DataObjectHolder) mRecycler.getChildViewHolder(v);
                        addToInvite(friend);
                        holder.addFriend.setImageResource(R.drawable.ic_followed_grey_24dp);
                        showToastInvited(friend.getFullName());
                    }
                } else {
                    Intent i = new Intent(getActivity(), ProfileActivity.class);
                    i.putExtra(Constants.USER_ID, friend.getUserId());
                    getActivity().startActivity(i);
                }
            }
        };

        mFollowClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User friend = ((User) v.getTag());
                int position = searchDataSet.indexOf(friend);
                UserConnectionRecyclerAdapter.DataObjectHolder holder = (UserConnectionRecyclerAdapter.DataObjectHolder) mRecycler.getChildViewHolder((View) (v.getParent()));
                if (isCameo) {
                    if (!friend.isCameoInvited()) {
                        addToInvite(friend);
                        holder.addFriend.setImageResource(R.drawable.ic_followed_grey_24dp);
                        showToastInvited(friend.getFullName());
                    }
                } else {
                    if (!friend.getUserId().contentEquals(Constants.ME) && !friend.getUserId().contentEquals(CAAUserDataSingleton.getInstance().getUserId())) {
                        Drawable d = !iFollowYou(friend) ? getResources().getDrawable(R.drawable.ic_follow_green_24dp) : getResources().getDrawable(R.drawable.ic_followed_grey_24dp);
                        holder.addFriend.setImageDrawable(d);
                        int pos = searchDataSet.indexOf(friend);
                        if (iFollowYou(friend)) {
                            friend.setFollowedByMe("0");
                            unFollowUser(friend.getUserId(), position);
                            if (mUserId.equalsIgnoreCase(Constants.ME) && !isInviteFriends)
                                ((UserConnectionsActivity2) getActivity()).updateFollowing(false, null);
                            TrackingManager.getInstance().tapUnfollow(updatedFollowing(-1), friend.getUserId(), isInviteFriends ? "invite_friends" : "following", pos + "");
                            updatedFollowing(-1);
                        } else {
                            friend.setFollowedByMe("1");
                            sendFollowUser(friend.getUserId(), position);
                            if (mUserId.equalsIgnoreCase(Constants.ME) && !isInviteFriends)
                                ((UserConnectionsActivity2) getActivity()).updateFollowing(true, null);
                            TrackingManager.getInstance().tapFollow(friend.getUserId(), isInviteFriends ? "invite_friends" : "following", position, updatedFollowing(1));
                        }
                    }
                }
            }
        };

        mMultiImageLoader = ((BaseActivity) getActivity()).getMultiImageLodaer();
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext(), R.drawable.line_divider, true));

        removeEmptyLayout();
        if (isInviteFriends) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (onDoneLoadingListener != null)
                        onDoneLoadingListener.onDoneLoading();
                }
            }, 500);

            if (items != null && items.size() > 0) {
                adapter = new UserConnectionRecyclerAdapter(getContext(), mMultiImageLoader, searchDataSet, mItemClickListener, mFollowClickListener, isCameo);
                mRecycler.setAdapter(adapter);
            } else if (mAmount >= 0)
                showEmptyLayout();
        } else {
            mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (!isInviteFriends) {
                        int visibleItemCount = mLayoutManager.getChildCount();
                        int totalItemCount = mLayoutManager.getItemCount();
                        int firstVisibleItem = ((LinearLayoutManager) mLayoutManager).findFirstVisibleItemPosition();
                        if (listInitialized) {
                            final int lastItem = firstVisibleItem + visibleItemCount;
                            if (lastItem >= totalItemCount - 6 && !calling && !endPages) {
                                if (previousLastItem != lastItem) { //to avoid multiple calls for last item
                                    previousLastItem = lastItem;
                                    getNextPage();
                                }
                            }
                        }
                    }
                }
            });
        }

        if (mAmount < 0) {
            try {
                v.findViewById(R.id.title_container).setVisibility(View.GONE);
            } catch (Throwable err) {
            }
        }

        return v;
    }

    public synchronized int updateFollowing(boolean increase, User friend) {
        if (increase) {
            mTextTotal.setText("Following " + (++mAmount) + " people");
            if (friend != null) {
                if (lastSearchLength == 0 && searchDataSet != null) {
                    searchDataSet.add(friend);
                    adapter.notifyItemChanged(searchDataSet.size() - 1);
                } else
                    items.add(friend);
            }

        } else {
            mTextTotal.setText("Following " + (mAmount == 0 ? 0 : --mAmount) + " people");
            if (friend != null) {
                if (lastSearchLength == 0 && searchDataSet != null) {
                    searchDataSet.remove(friend);
                    adapter.notifyItemRemoved(searchDataSet.size());
                } else
                    items.remove(friend);
            }
        }
        return mAmount;
    }

    private void getFollowing() {
        App.getUrlService().getSpecificUserFollowingPage(mUserId, pageNumber, new Callback<CAAApiUsers>() {
            @Override
            public void success(CAAApiUsers caaApiUsers, Response response) {
                if (mRecycler == null || getActivity() == null)
                    return;

//                cancelAnimation();
                items = caaApiUsers.getData();
                if (items.size() < 1) {
                    showEmptyLayout();
                    endPages = true;
                } else {
                    if (followAll != null)
                        followAll.setVisibility(View.VISIBLE);
                    searchDataSet = new ArrayList<>();
                    ArrayList<String> userIds = new ArrayList<>(1);
                    for (int i = 0; i < items.size(); i++) {
                        User user = items.get(i);
                        if (isCameo && invitedDuetList != null) {
                            for (int j = 0; j < invitedDuetList.size(); j++) {
                                if (user.getUserId().equals(invitedDuetList.get(j).getUser_id())) {
                                    user.setCameoInvited(1);
                                    break;
                                }
                            }
                        }
                        searchDataSet.add(user);
                        userIds.add(user.getUserId());
                    }

                    if(userIds.size() > 0 && Utils.userIsMe(mUserId))
                        FollowingDataHandler.getInstance().addMany(userIds);

                    adapter = new UserConnectionRecyclerAdapter(getContext(), mMultiImageLoader, searchDataSet, mItemClickListener, mFollowClickListener, isCameo);
                    mRecycler.setAdapter(adapter);
                    if (isCameo)
                        mTextTotal.setText(mAmount + " Friends to do a Duet with");
                    else
                        mTextTotal.setText("Following " + mAmount + " people");

                    listInitialized = true;
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (onDoneLoadingListener != null)
                            onDoneLoadingListener.onDoneLoading();
                    }
                }, 500);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void getNextPage() {
        pageNumber++;
        calling = true;
        App.getUrlService().getSpecificUserFollowingPage(mUserId, pageNumber, new Callback<CAAApiUsers>() {
            @Override
            public void success(CAAApiUsers caaApiUsers, Response response) {
                if (mRecycler == null || getActivity() == null)
                    return;

                if (caaApiUsers.getData().size() < 1) {
                    endPages = true;
                } else {
                    ArrayList<String> userIds = new ArrayList<>(1);
                    for (User user : caaApiUsers.getData()) {
                        items.add(user);
                        searchDataSet.add(user);
                        userIds.add(user.getUserId());
                    }
                    if(userIds.size() > 0 && Utils.userIsMe(mUserId))
                        FollowingDataHandler.getInstance().addMany(userIds);
                    if (items.size() == mAmount) {
                        endPages = true;
                    }
                    adapter.notifyDataSetChanged();
                }
                calling = false;

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void showEmptyLayout() {
        mRecycler.setVisibility(View.GONE);
        listViewEmpty.setVisibility(View.VISIBLE);
    }

    private void removeEmptyLayout() {
        listViewEmpty.setVisibility(View.GONE);
    }

    private boolean iFollowYou(User friend) {
        if (FollowingDataHandler.getInstance().contains(friend.getUserId())) {
            return true;
        }
        return false;
    }

    private void sendFollowUser(String userID, final int position) {

        FollowingDataHandler.getInstance().add(userID);
        App.getUrlService().putsUsersFollow(userID, "", new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                searchDataSet.get(position).setFollowedByMe("1");
                adapter.notifyItemChanged(position);
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
        StateMachine.getInstance(getContext().getApplicationContext()).insertEvent(
                new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL,
                        StateMachineEvent.EVENT_TYPE.FOLLOW_USER.ordinal(),
                        Integer.parseInt(userID)
                )
        );
    }


    private void unFollowUser(String userID, final int position) {
        FollowingDataHandler.getInstance().remove(userID);
        App.getUrlService().deleteUsersFollow(userID, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                searchDataSet.get(position).setFollowedByMe("0");
                adapter.notifyItemChanged(position);
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    public void followAllContacts() {
        final LoadingDialog loadingDialog = new LoadingDialog(getContext(), false, false);
        if (!searchDataSet.isEmpty()) {
            ArrayList<String> Ids = new ArrayList<>();
            for (User user : searchDataSet) {
                if (user.getFollowedByMe().equals("0"))
                    Ids.add(user.getUserId());
            }

            if (Ids.size() == 0) {
                loadingDialog.dismiss();
                Toast.makeText(getContext(), R.string.invite_a_friend_follow_already_following, Toast.LENGTH_SHORT).show();
                return;
            }

            CAAFollowAll userIds = new CAAFollowAll(Ids);

            FollowingDataHandler.getInstance().addMany(Ids);
            App.getUrlService().postFollowAll(userIds, new Callback<String>() {
                @Override
                public void success(String s, Response response) {

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            for (User contact : searchDataSet) {
                                contact.setFollowedByMe("1");
                            }
                            mAmount = searchDataSet.size();
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.notifyDataSetChanged();
                                    loadingDialog.dismiss();
                                }
                            });
                        }
                    }).start();
                }

                @Override
                public void failure(RetrofitError error) {
                    loadingDialog.dismiss();
                }
            });

            TrackingManager.getInstance().tapFollow(userIds.getUserIds().toString(), isInviteFriends ? "invite_friends" : "following", -1, userIds.getUserIds().size());
            StateMachine.getInstance(getContext().getApplicationContext()).insertEvent(
                    new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL,
                            StateMachineEvent.EVENT_TYPE.FOLLOW_USER.ordinal()
                    )
            );
        } else {
            loadingDialog.dismiss();
            Toast.makeText(getContext(), R.string.invite_a_friend_follow_no_friends, Toast.LENGTH_SHORT).show();
            return;
        }

    }

    @Override
    public void onSearch(String text) {
        if (searchDataSet == null || adapter == null)
            return;
        String search = text.toLowerCase();
        if (lastSearchLength < search.length()) {
            for (int i = searchDataSet.size() - 1; i > -1; i--) {
                if (searchDataSet.get(i) != null && !searchDataSet.get(i).getFullName().toLowerCase().contains(search)) {
                    searchDataSet.remove(i);
                }
            }
        } else {
            searchDataSet.clear();
            if (search.length() > 0) {
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).getFullName().toLowerCase().contains(search))
                        searchDataSet.add(items.get(i));
                }
            } else {
                for (int i = 0; i < items.size(); i++) {
                    searchDataSet.add(items.get(i));
                }
            }
        }
        adapter.notifyDataSetChanged();
        lastSearchLength = search.length();
    }

    @Override
    public void addToInvite(User user) {
        user.setCameoInvited(ContactInfo.INVITED);
        if (sendList == null)
            sendList = new ArrayList<>(1);
        sendList.add(new InviteeContact(user.getFullName(), user.getUserId(), user.getPhoto(), mVideoID));
    }

    private void invitePeople() {
        if (sendList != null && sendList.size() > 0) {
            SharedPreferencesManager.getEditor().putBoolean(Constants.INVITED_TO_DUET,true).apply();
            DuetInvitedHandler.getInstance().addMany(sendList);
            TrackingManager.getInstance().tapInviteFriendsCameo(sendList.size());
            App.getUrlService().postCameoInvite(new Invitee(sendList, mVideoID), new Callback<String>() {
                @Override
                public void success(String s, Response response) {
                    sendList.clear();
                }

                @Override
                public void failure(RetrofitError error) {
                }
            });

        }
    }

    private void showToastInvitedCount(int count) {
        if(isCameo){
            Toast.makeText(getContext(), "Invited " + count + " users to your Duet!", Toast.LENGTH_SHORT).show();
        }
        else{
            //TODO show other msg
        }

    }

    private void showToastInvited(String name) {
        if(isCameo){
            Toast.makeText(getContext(), "Invited " + name + " to your Duet!", Toast.LENGTH_SHORT).show();
        }
        else{
            //TODO show other msg
        }

    }

    public void clearData() {
        if (searchDataSet != null)
            searchDataSet.clear();
        if (items != null)
            items.clear();
    }

    public void setData(ArrayList<User> blingyFriends) {
        if (blingyFriends != null) {
            for (int i = 0; i < blingyFriends.size(); i++) {
                searchDataSet.add(blingyFriends.get(i));
                items.add(blingyFriends.get(i));
            }
        }
    }

    public void notifyDataSetChanged() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
            mRecycler.setAdapter(adapter);
        } else {
            adapter = new UserConnectionRecyclerAdapter(getContext(), mMultiImageLoader, searchDataSet, mItemClickListener, mFollowClickListener, isCameo);
            mRecycler.setAdapter(adapter);
        }
    }
}
