package fm.bling.blingy.inviteFriends.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAFollowAll
 * Description:
 * Created by Dawidowicz Nadav on 7/2/15.
 * History:
 * ***********************************
 */
public class CAAFollowAll {

    @SerializedName("user_ids")
    private ArrayList<String> userIds;

    public CAAFollowAll(ArrayList<String> userIds) {
        this.userIds = userIds;
    }

    public ArrayList<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(ArrayList<String> userIds) {
        this.userIds = userIds;
    }
}

