package fm.bling.blingy;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.dialogs.share.ShareDialogCallbackSetter;
import fm.bling.blingy.discover.DiscoverActivity;
import fm.bling.blingy.homeScreen.HomeScreenActivity;
import fm.bling.blingy.homeScreen.model.CAANotificationUnread;
import fm.bling.blingy.notification.NotificationActivity;
import fm.bling.blingy.profile.ProfileActivity;
import fm.bling.blingy.record.PickMusicClipActivity;
import fm.bling.blingy.record.callbacks.ActivityResultsReceiver;
import fm.bling.blingy.record.callbacks.PermissionResultReciever;
import fm.bling.blingy.registration.CAALogin;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.splashScreen.SplashScreenActivity;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineMessageListener;
import fm.bling.blingy.stateMachine.PermissionListener;
import fm.bling.blingy.upload.adapters.PickPhotoVideoVPAdapter;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.MultiImageLoader;
import fm.bling.blingy.widget.CAAWidgetSettingsSingleton;
import fm.bling.blingy.widget.dialogs.InviteWidgetBaseDialog;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 14/09/2016.
 * History:
 * ***********************************
 */
public class BaseActivity extends AppCompatActivity implements ShareDialogCallbackSetter, ActivityCompat.OnRequestPermissionsResultCallback, StateMachineMessageListener{
    protected ImageLoaderManager mImageLoaderManager;
    protected MultiImageLoader mMultiImageLoader;
    private Toolbar mToolbar;
    private Intent intentToStart;
    protected boolean isActivityVisible = false;

    private ActivityResultsReceiver mActivityResultsReceiver;
    private PermissionResultReciever mPermissionResultReciever;

    private PermissionListener stateMachineListener;
    private boolean checkingContactPermission = false;

    private Boolean recordPermissionsDialogShown = false;
    private String[] recordPermissions = new String[]{};
    private String[] contactPermission = new String[]{};

    protected Handler mHandler;

    private InviteWidgetBaseDialog inviteWidget;

    protected boolean showWidget = true;
    protected boolean isWidgetVisible = false;

    /**
     * Main Views
     */
    public FrameLayout mBottomBar;
    protected FrameLayout mHomeButton;
    protected FrameLayout mSearchButton;
    protected FrameLayout mRecordButton;
    protected ImageView mRecordIcon;
    protected FrameLayout mNotificationButton;
    protected FrameLayout mProfileButton;
    protected FrameLayout mNotificationContainer;
    protected TextView mNotificationText;
    protected boolean isOpenApp = false;

    /**
     * Screens ID's
     */
    protected final int NO_BAR = -1;
    protected final int HOME_SCREEN = 1;
    protected final int DISCVOER_SCREEN = 2;
    protected final int NOTIFICATION_SCREEN = 3;
    protected final int PROFILE_SCREEN = 4;
    protected final int RECORD_SCREEN = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        CAAUserDataSingleton.getInstance().setContext(getApplicationContext());
        mImageLoaderManager = new ImageLoaderManager(this);
        mMultiImageLoader = new MultiImageLoader(this);
        if (mHandler == null) ;
            mHandler = new Handler();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

//        if(App.isCrashed)
//            System.exit(1);

        isActivityVisible = true;

        startMultiImageLoader();

        if (mHandler == null) ;
            mHandler = new Handler();
    }

    @Override
    protected void onStart() {
        addBottomBar();
        super.onStart();
        isActivityVisible = true;

        startMultiImageLoader();

        try {
            if (mImageLoaderManager == null) {
                mImageLoaderManager = new ImageLoaderManager(this);
            }
        }catch (Throwable throwable){
            throwable.printStackTrace();
        }

        if (showWidget) {
            if (CAAWidgetSettingsSingleton.getInstance().isWidgetOn() && inviteWidget == null) {
                StateMachine.getInstance(getApplicationContext()).getStateMessage(this, this);
            }
        }
        if (!(this instanceof NotificationActivity)) {
            if (Constants.mNotificationCount != null && !Constants.mNotificationCount.isEmpty())
                setNotifCount(Constants.mNotificationCount);
            else
                getNotifications();
        }
    }

    @Override
    protected void onPause()
    {
        try {   mMultiImageLoader.close();  }catch (Throwable err) {}
        mMultiImageLoader = null;
        System.gc();

        isActivityVisible = false;
        super.onPause();
    }

    @Override
    protected void onStop()
    {
        isActivityVisible = false;
        super.onStop();

        try {
            mImageLoaderManager.clearCache();
            mImageLoaderManager.clearImageViews();
        } catch (Throwable err) {
            err.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{mMultiImageLoader.close();}catch (Throwable err){}
        mMultiImageLoader = null;

        try {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
            mImageLoaderManager.close();
            mMultiImageLoader.close();
        } catch (Throwable err) {
        }
    }

    public void getNotifications() {
        if(CAAUserDataSingleton.getInstance().getAccessToken().isEmpty())
            return;

        App.getUrlService().getsUserNotificationsUnreadCount(new Callback<CAANotificationUnread>() {
            @Override
            public void success(CAANotificationUnread caaNotificationUnread, Response response) {
                if (BaseActivity.this != null && !caaNotificationUnread.getUnread().equalsIgnoreCase(""))
                    setNotifCount(caaNotificationUnread.getUnread());
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void setNotifCount(String unread) {
        if (mNotificationContainer != null) {
            if (unread.equalsIgnoreCase("") || unread.equalsIgnoreCase("0")) {
                mNotificationContainer.setVisibility(View.GONE);
                mNotificationText.setText("");

            } else {
                if (unread.length() > 1) {
                    int padding = ((int) (10f * App.SCALE_Y));
                    mNotificationText.setTextSize(11f);
                    mNotificationContainer.setBackgroundResource(R.drawable.red_circle_for_2);
                    mNotificationContainer.setPadding(padding + 5, padding, padding + 5, padding);
                } else {
                    int padding = ((int) (15f * App.SCALE_Y));
                    mNotificationContainer.setBackgroundResource(R.drawable.red_circle);
                    mNotificationContainer.setPadding(padding, padding, padding, padding);
                }
                mNotificationText.setText(unread);
                mNotificationContainer.setVisibility(View.VISIBLE);
            }
        }
    }

    protected void addBottomBar(){
        if (mBottomBar == null)
            mBottomBar = (FrameLayout) findViewById(R.id.main_bottom_bar);

        if (mRecordButton == null) {
            mRecordButton = (FrameLayout) findViewById(R.id.record_button);
            mRecordIcon = (ImageView) findViewById(R.id.record_icon);
        }

        if (mSearchButton == null)
            mSearchButton = (FrameLayout) findViewById(R.id.navigation_search_button);

        if (mHomeButton == null)
            mHomeButton = (FrameLayout) findViewById(R.id.home_button);

        if (mNotificationButton == null) {
            mNotificationButton = (FrameLayout) findViewById(R.id.notifications_button);
            mNotificationContainer = (FrameLayout) findViewById(R.id.notification_container);
            mNotificationText = (TextView) findViewById(R.id.notification_num);
        }

        if (mProfileButton == null)
            mProfileButton = (FrameLayout) findViewById(R.id.profile_button);

        loadMainListeners();
        handleBottomBarView();
    }

    public Toolbar getActionBarToolbar() {
        if (mToolbar == null) {
            mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
            if (mToolbar != null) {
                setSupportActionBar(mToolbar);
                mToolbar.setBackgroundResource(R.color.main_blue);
                setToolbarFont();
            }
        } else {
            mToolbar.setBackgroundResource(R.color.main_blue);
        }
        return mToolbar;
    }

    public void setToolbarFont() {
        for (int i = 0; i < mToolbar.getChildCount(); i++) {
            if (mToolbar.getChildAt(i) instanceof TextView) {
                ((TextView) mToolbar.getChildAt(i)).setTypeface(App.ROBOTO_REGULAR);
            }
        }
    }

    private void loadMainListeners() {
        if (mRecordButton != null) {
            mRecordButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getSelfID() != RECORD_SCREEN) {
                        Intent intent = new Intent(BaseActivity.this, PickMusicClipActivity.class);
                        overridePendingTransition(0, 0);
                        startActivity(intent);
                    }
                    else
                        if(BaseActivity.this instanceof ScrollToTopBaseActivity)
                            ((ScrollToTopBaseActivity)BaseActivity.this).scrollToTop();

                }
            });
        }
        if (mSearchButton != null) {
            mSearchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getSelfID() != DISCVOER_SCREEN) {
                        Intent discover = new Intent(BaseActivity.this, DiscoverActivity.class);
                        overridePendingTransition(0, 0);
                        startActivity(discover);
                    }
                    else
                        if(BaseActivity.this instanceof ScrollToTopBaseActivity)
                            ((ScrollToTopBaseActivity)BaseActivity.this).scrollToTop();
                }
            });
        }
        if (mHomeButton != null) {
            mHomeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getSelfID() != HOME_SCREEN) {
                        Intent home = new Intent(BaseActivity.this, HomeScreenActivity.class);
                        overridePendingTransition(0, 0);
                        startActivity(home);
                    }
                    else
                        if(BaseActivity.this instanceof ScrollToTopBaseActivity)
                            ((ScrollToTopBaseActivity)BaseActivity.this).scrollToTop();
                }
            });
        }
        if (mProfileButton != null) {
            mProfileButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getSelfID() != PROFILE_SCREEN) {
                        Intent profile = new Intent(BaseActivity.this, ProfileActivity.class);
                        overridePendingTransition(0, 0);
                        startActivity(profile);
                    }
                    else
                        if(BaseActivity.this instanceof ScrollToTopBaseActivity)
                            ((ScrollToTopBaseActivity)BaseActivity.this).scrollToTop();
                }
            });
        }
        if (mNotificationButton != null) {
            mNotificationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getSelfID() != NOTIFICATION_SCREEN) {
                        Intent profile = new Intent(BaseActivity.this, NotificationActivity.class);
                        overridePendingTransition(0, 0);
                        startActivity(profile);
                    }
                    else
                        if(BaseActivity.this instanceof ScrollToTopBaseActivity)
                            ((ScrollToTopBaseActivity)BaseActivity.this).scrollToTop();
                }
            });
        }
    }

    public void handleBottomBarView() {
        if (getSelfID() != -1)
            Constants.mLastBarID = getSelfID();

        if (mHomeButton != null) {
            mHomeButton.getChildAt(0).setAlpha(0.5f);
            mSearchButton.getChildAt(0).setAlpha(0.5f);
            mNotificationButton.getChildAt(1).setAlpha(0.5f);
            mProfileButton.getChildAt(0).setAlpha(0.5f);
            mRecordIcon.setImageResource(R.drawable.ic_tab_record_24dp);

            switch (Constants.mLastBarID) {
                default:
                case NO_BAR:
                    break;
                case RECORD_SCREEN:
                    mRecordIcon.setImageResource(R.drawable.ic_tab_record_active_24dp);
                    break;
                case DISCVOER_SCREEN:
                    mSearchButton.getChildAt(0).setAlpha(1f);
                    break;
                case NOTIFICATION_SCREEN:
                    mNotificationButton.getChildAt(1).setAlpha(1f);
                    break;
                case PROFILE_SCREEN:
                    mProfileButton.getChildAt(0).setAlpha(1f);
                    break;
                case HOME_SCREEN:
                    mHomeButton.getChildAt(0).setAlpha(1f);
                    break;
            }
        }
    }

    private void reloadLoginData() {
        String accessToken = SharedPreferencesManager.getInstance().getString("access_token", "");
        String firstName = SharedPreferencesManager.getInstance().getString("first_name", "");
        String lastName = SharedPreferencesManager.getInstance().getString("last_name", "");
        String userId = SharedPreferencesManager.getInstance().getString("user_id", "");
        String accountType = SharedPreferencesManager.getInstance().getString(Constants.ACCOUNT_TYPE, "");
        String email = "";
        String password = "";
        String deviceIdentifier = "";

        if (accountType.isEmpty()) {
            startActivity(new Intent(this, SplashScreenActivity.class));
            finish();
        }

        switch (accountType) {
            case "facebook":
                break;
            case "email":
                email = SharedPreferencesManager.getInstance().getString("email", "");
                password = SharedPreferencesManager.getInstance().getString("password", "");
                break;
            case "anonymous":
                deviceIdentifier = SharedPreferencesManager.getInstance().getString("device_identifier", "");
                break;
            default:
                break;
        }

        CAALogin.updateUserSingleton(accountType, accessToken, email, password, firstName, lastName, deviceIdentifier, userId);

    }

    public void uploadPermissionsDialog() {
        BaseDialog dialog = new BaseDialog(this, "Permission", getResources().getString(R.string.upload_permission_dialog));
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (((BaseDialog) dialogInterface).getAnswer()) {
                    ActivityCompat.requestPermissions(BaseActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.PICK_VIDEO_REQUEST);
                }
            }
        });
    }

    public void recordingPermissionsDialog() {
        BaseDialog dialog = new BaseDialog(this, "Permission", getResources().getString(R.string.record_permission_dialog));
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                recordPermissionsDialogShown = false;
                if (((BaseDialog) dialogInterface).getAnswer()) {
                    recordPermissions = new String[]{"", ""};
                    if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.CAMERA)) {
                        recordPermissions[0] = Manifest.permission.CAMERA;
                    }
                    if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.RECORD_AUDIO)) {
                        recordPermissions[1] = Manifest.permission.RECORD_AUDIO;
                    }
                    ActivityCompat.requestPermissions(BaseActivity.this, BaseActivity.this.recordPermissions,
                            Constants.RECORD_PERMISSION);
                } else {
                    BaseActivity.this.intentToStart = null;
                }
            }
        });
    }

    public void checkUploadPermissions(Intent intent) {
        intentToStart = intent;
        if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                uploadPermissionsDialog();
            } else {
                ActivityCompat.requestPermissions(BaseActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.PICK_VIDEO_REQUEST);
            }
        } else pickPhotoVideoUpload();
    }

    public void getRecordPermissions(Intent recordIntent) {
        intentToStart = recordIntent;
        recordPermissions = new String[]{"", ""};
        if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)) {
            recordPermissions[0] = Manifest.permission.CAMERA;
        }
        if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)) {
            recordPermissions[1] = Manifest.permission.RECORD_AUDIO;
        }
        if ((!recordPermissions[0].isEmpty() || !recordPermissions[1].isEmpty()) && !recordPermissionsDialogShown) {
            recordPermissionsDialogShown = true;
            if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.CAMERA) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.RECORD_AUDIO)) {
                recordingPermissionsDialog();
            } else {
                ActivityCompat.requestPermissions(BaseActivity.this, recordPermissions,
                        Constants.RECORD_PERMISSION);
            }
        } else {
            goToRecord(recordIntent);
        }
    }

    public boolean getContactsPermissions(PermissionListener onPermissionGivenListener) {
        if (!checkingContactPermission) {
            checkingContactPermission = true;
            contactPermission = new String[]{""};
            if (onPermissionGivenListener != null)
                this.stateMachineListener = onPermissionGivenListener;
            if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.READ_CONTACTS)) {
                contactPermission[0] = Manifest.permission.READ_CONTACTS;
            }

            if (!contactPermission[0].isEmpty()) {
                ActivityCompat.requestPermissions(BaseActivity.this, contactPermission, Constants.STATE_MACHINE_PERMISSION_REQUEST);
                return false;
            } else {
                if (onPermissionGivenListener != null)
                    onPermissionGivenListener.onPermissionGranted();
                checkingContactPermission = false;
                return true;
            }
        }
        return false;
    }

    private void goToRecord(Intent recordIntent) {
        if (recordIntent == null)
            return;
        startActivity(recordIntent);
    }

    private void pickPhotoVideoUpload() {

        startActivity(intentToStart);
    }

    public void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void openKeyboard(final View view) {
        if (mHandler != null) {
            mHandler.postDelayed(
                    new Runnable() {
                        public void run() {
                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                            inputMethodManager.toggleSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_NOT_ALWAYS);
                            view.requestFocus();
                        }
                    }, 250);
        }
    }

    public ImageLoaderManager getImageLoaderManager() {
        if(mImageLoaderManager == null)
            mImageLoaderManager = new ImageLoaderManager(this);

        return mImageLoaderManager;
    }

    public MultiImageLoader getMultiImageLodaer() {
        if(mMultiImageLoader == null)
            startMultiImageLoader();

        return mMultiImageLoader;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == Constants.STATE_MACHINE_PERMISSION_REQUEST) {
            checkingContactPermission = false;
            this.contactPermission = new String[]{""};
            if (grantResults.length < 1) {
                stateMachineListener.onPermissionDenied();
                return;
            }
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED &&
                        (permissions[i].equalsIgnoreCase(Manifest.permission.READ_CONTACTS))) {
                    this.contactPermission[i] = permissions[i];
                }
            }
            //If permissions denied
            if (!this.contactPermission[0].isEmpty()) {
                stateMachineListener.onPermissionDenied();
            } else {
                stateMachineListener.onPermissionGranted();
            }
        } else if (requestCode == Constants.RECORD_PERMISSION) {
            this.recordPermissions = new String[]{"", ""};
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED &&
                        (permissions[i].equalsIgnoreCase(Manifest.permission.RECORD_AUDIO) || permissions[i].equalsIgnoreCase(Manifest.permission.CAMERA))) {
                    this.recordPermissions[i] = permissions[i];
                }
            }
            //If permissions denied
            if (!this.recordPermissions[0].isEmpty() || !this.recordPermissions[1].isEmpty()) {
                recordingPermissionsDialog();
            } else {
                recordPermissionsDialogShown = false;
                goToRecord(intentToStart);
            }
        } else if (requestCode == Constants.PICK_VIDEO_REQUEST) {
            for (int i = 0; i < grantResults.length; i++) {
                if (permissions[i].equalsIgnoreCase(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        PickPhotoVideoVPAdapter.initializeFileManager();
                        pickPhotoVideoUpload();
                    } else {
                        uploadPermissionsDialog();
                    }
                }
            }
        }

        if (mPermissionResultReciever != null) {
            mPermissionResultReciever.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_LOGIN && resultCode == RESULT_OK) {
            reloadLoginData();
        }

        if (mActivityResultsReceiver != null) {
            mActivityResultsReceiver.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void setActivityResultsReceiver(ActivityResultsReceiver activityResultsReceiver) {
        mActivityResultsReceiver = activityResultsReceiver;
    }

    @Override
    public void setPermissionResultReiever(PermissionResultReciever permissionResultReiever) {
        mPermissionResultReciever = permissionResultReiever;
    }

    public void setIsWidgetVisible(boolean isVisible) {
        isWidgetVisible = isVisible;
    }

    public boolean getIsWidgetVisible() {
        return isWidgetVisible;
    }

    public void setShowWidget(boolean showWidget) {
        this.showWidget = showWidget;
    }

    @Override
    public void onWidgetReady(final InviteWidgetBaseDialog dialog) {
        if (showWidget) {
            if (inviteWidget != null) return; // we have another widget visible.

            if (!isFinishing() && dialog != null) {
                inviteWidget = dialog;
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        inviteWidget = null;
                    }
                });
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            isWidgetVisible = true;
                            dialog.show();
                        } catch (Throwable err) {
                        }
                    }
                });
            }
        }
    }

    public Handler getHandler() {
        return mHandler;
    }

    public int getSelfID()
    {
        return NO_BAR;
    }

    protected synchronized void startMultiImageLoader()
    {
        if(mMultiImageLoader == null)
            mMultiImageLoader = new MultiImageLoader(this);
    }

    public void gainAudioFocus(){
        if(App.shouldGainAudioFocus()) {
            AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            App.sAudioFocus = am.requestAudioFocus(App.getAudioListener(), AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        }
    }

}