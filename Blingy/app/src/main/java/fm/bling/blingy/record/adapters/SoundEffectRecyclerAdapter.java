package fm.bling.blingy.record.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import fm.bling.blingy.R;
import fm.bling.blingy.record.rest.model.CAASoundEffects;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 5/5/16.
 * History:
 * ***********************************
 */
public class SoundEffectRecyclerAdapter extends RecyclerView.Adapter<SoundEffectRecyclerAdapter.DataObjectHolder>{
    private ArrayList<CAASoundEffects.CAAEffect> mDataset;
    private SparseArray<DataObjectHolder> mHolders;
    private View.OnClickListener mItemClickListener;
    private View.OnClickListener mPlayPauseClickListener;

    public SoundEffectRecyclerAdapter(ArrayList<CAASoundEffects.CAAEffect> myDataset, View.OnClickListener itemClickListener , View.OnClickListener playPauseClickListener) {
        mDataset = myDataset;
        mHolders = new SparseArray<>();
        mItemClickListener = itemClickListener;
        mPlayPauseClickListener = playPauseClickListener;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewIndex) {
        DataObjectHolder dataObjectHolder = mHolders.get(viewIndex);
        if(dataObjectHolder == null) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.soundeffect_list_item, parent, false);
            dataObjectHolder = new DataObjectHolder(view);
            mHolders.put(viewIndex, dataObjectHolder);
        }
        return dataObjectHolder;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        CAASoundEffects.CAAEffect effect = mDataset.get(position);
        holder.mItem.setTag(effect);
        holder.mPlayPause.setTag(holder.mItem);
        holder.mSoundName.setText(effect.getName());
        // holder.mPosition.setTag(holder.mItem);
        //holder.mPosition.setText((position + 1) + "");
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    @Override
    public void onViewRecycled(DataObjectHolder holder) {

    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        private LinearLayout mItem;
        private TextView mSoundName;
        private ImageView mPlayPause;
        private ProgressBar mProgressBar;

        public DataObjectHolder(View itemView) {
            super(itemView);
            mItem = (LinearLayout)itemView.findViewById(R.id.sound_effect_item);
            mPlayPause = (ImageView) itemView.findViewById(R.id.play_pause);
            mSoundName = (TextView)itemView.findViewById(R.id.sound_name);
            mProgressBar = (ProgressBar)itemView.findViewById(R.id.progress_bar);

            mItem.setOnClickListener(mItemClickListener);
            mPlayPause.setOnClickListener(mPlayPauseClickListener);
        }
    }

    public void clearData() {
        int size = this.mDataset.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.mDataset.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }
}
