package fm.bling.blingy.inviteFriends.listeners;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 3/6/16.
 * History:
 * ***********************************
 */
public interface ErrorListener
{
    void onError();

}