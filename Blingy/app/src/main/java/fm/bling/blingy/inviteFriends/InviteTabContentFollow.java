package fm.bling.blingy.inviteFriends;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.database.handler.FollowingDataHandler;
import fm.bling.blingy.inviteFriends.adapters.RecyclerAdapterFollow;
import fm.bling.blingy.inviteFriends.listeners.FollowListener;
import fm.bling.blingy.inviteFriends.listeners.InvalidateNotificationListener;
import fm.bling.blingy.inviteFriends.listeners.TabTitleListener;
import fm.bling.blingy.inviteFriends.model.CAAFollowAll;
import fm.bling.blingy.inviteFriends.model.ContactInfo;
import fm.bling.blingy.registration.CAALogin;
import fm.bling.blingy.registration.SignupActivity;

import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.dialogs.LoadingDialog;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.ClickableTextView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/14/16.
 * History:
 * ***********************************
 */
public class InviteTabContentFollow extends InviteTabContentBase
{
    private LinearLayout followAllLlinearLayout;
    private ClickableTextView followALL;
    private TextView numberOfContacts;
    private LoadingDialog loadingDialog;
    private TabTitleListener mTabTitleListener;
    private int numberOfFollowingFriend;
    private String followStr;

    private RecyclerView mRecyclerView;
    private ArrayList<ContactInfo> mContactsDataSet;
    private RecyclerAdapterFollow mAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<User> mDataSet;
    private InvalidateNotificationListener mInvalidateNotificationListener;
    private Context mContext;

    public InviteTabContentFollow(Context context, Paint paint, ArrayList<ContactInfo> contactsDataset, List<User> dataSet, ImageLoaderManager imageLoaderManager,
                                  TabTitleListener tabTitleListener, InvalidateNotificationListener invalidateNotificationListener)
    {
        super(context, paint, false);
        mInvalidateNotificationListener = invalidateNotificationListener;
        mDataSet= dataSet;
        mContactsDataSet = contactsDataset;
        followStr = context.getResources().getString(R.string.follow);
        mTabTitleListener = tabTitleListener;
        if(dataSet != null)
        {
            for (User friend : dataSet)
            {
                if (friend.getFollowedByMe().equals("1"))
                    numberOfFollowingFriend++;
            }
            setTitle();

        }

        if(mDataSet == null)
            mDataSet = new ArrayList<>();
        setFirstLine(context);
        setContent(context, imageLoaderManager);
        mContext = context;
    }

    private void setTitle()
    {
        if (numberOfFollowingFriend > 0)
            mTabTitleListener.onTitleChanged(followStr + " (" + numberOfFollowingFriend + ")");
        else mTabTitleListener.onTitleChanged(followStr);
        post(new Runnable()
        {
            @Override
            public void run()
            {
                try { numberOfContacts.invalidate(); }
                catch (Throwable ex){ }
            }
        });
    }

    private void setFirstLine(Context context)
    {
        numberOfContacts = new TextView(context);
        numberOfContacts.setTypeface(App.ROBOTO_REGULAR);
        numberOfContacts.setTextColor(context.getResources().getColor(R.color.grey_font));
        numberOfContacts.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(R.dimen.font_description));
        numberOfContacts.setPadding(padding, 0, 0, 0);
        numberOfContacts.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));
        numberOfContacts.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        numberOfContacts.setIncludeFontPadding(false);
        numberOfContacts.setSingleLine();

        if(mDataSet.size() == 1)
            numberOfContacts.setText(getContext().getResources().getString(R.string.invite_a_friend_contacts_follow_single));
        else if(mDataSet.size() > 1)
            numberOfContacts.setText(mDataSet.size()+" "+getContext().getResources().getString(R.string.invite_a_friend_contacts_follow_plural));

        followALL = new ClickableTextView(context);
        followALL.setTypeface(App.ROBOTO_MEDIUM);
        followALL.setTextColor(context.getResources().getColor(R.color.grey_font));
        followALL.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(R.dimen.font_description));
        followALL.setText(R.string.follow_all);
        followALL.setPadding(padding, 0, padding, 0);
        followALL.setLayoutParams(new LinearLayout.LayoutParams((int) (300f * App.SCALE_X), (int) (100f * App.SCALE_Y)));
        followALL.setGravity(Gravity.CENTER);
        followALL.setTextColor(Color.WHITE);
        followALL.setSingleLine();
        followALL.setBackgroundResource(R.drawable.invite_blue_button_bg);
        followALL.setClickable(true);
        followALL.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                followAllFriends();
            }
        });

        followAllLlinearLayout = new LinearLayout(context);
        followAllLlinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        followAllLlinearLayout.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
        followAllLlinearLayout.setPadding(0, 0, padding, 0);
        followAllLlinearLayout.addView(followALL);

        firstLine.addView(numberOfContacts);
        firstLine.addView(followAllLlinearLayout);
    }

    private void setContent(Context context, ImageLoaderManager imageLoaderManager)
    {
        mRecyclerView = new RecyclerView(context);
        mRecyclerView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new RecyclerAdapterFollow(mDataSet, imageLoaderManager, new FollowListener()
        {
            @Override
            public void follow(String id)
            {
                followFriend(id);//getContactInfo(id));
            }

            @Override
            public void unFollow(String id)
            {
                unFollowFriend(id);//getContactInfo(id));
            }
        });
        mRecyclerView.setAdapter(mAdapter);

        addView(mRecyclerView);
    }

    private ContactInfo getContactInfo(String id) {
        for (int i = 0; i < mContactsDataSet.size(); i++) {
            if(mContactsDataSet.get(i).chosenId == id)
                return mContactsDataSet.get(i);
        }
        return null;
    }

    public void setData(List<User> dataSet)
    {
        this.mDataSet = dataSet;
        mAdapter.setData(dataSet);

        if(dataSet.size() == 1)
            numberOfContacts.setText( getContext().getResources().getString(R.string.invite_a_friend_contacts_follow_single) );
        else if(dataSet.size() > 1)
            numberOfContacts.setText(dataSet.size()+" "+ getContext().getResources().getString(R.string.invite_a_friend_contacts_follow_plural) );

        if(dataSet != null)
        {
            for (User friend : dataSet)
            {
                if (friend.getFollowedByMe().equals("1"))
                    numberOfFollowingFriend++;
            }
            setTitle();
        }
    }

    private void followAllFriends()
    {
        if(CAALogin.userIsAnonymous())
        {
            startSignUpScreen();
            return;
        }
        loadingDialog = new LoadingDialog(getContext(),false,false);
        if (!mDataSet.isEmpty())
        {
            ArrayList<String> Ids = new ArrayList<>();
            for (User user : mDataSet)
            {
                if(user.getFollowedByMe().equals("0"))
                    Ids.add(user.getUserId());
            }

            if(Ids.size() == 0)
            {
                loadingDialog.dismiss();
                Toast.makeText(getContext(), R.string.invite_a_friend_follow_already_following, Toast.LENGTH_SHORT).show();
                return;
            }

            CAAFollowAll userIds = new CAAFollowAll(Ids);

            FollowingDataHandler.getInstance().addMany(Ids);
            App.getUrlService().postFollowAll(userIds, new Callback<String>()
            {
                @Override
                public void success(String s, Response response)
                {

                    post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            for (User contact : mDataSet)
                            {
                                contact.setFollowedByMe("1");
                            }
                            numberOfFollowingFriend = mDataSet.size();
                            setTitle();
                            mAdapter.notifyDataSetChanged();
                            mInvalidateNotificationListener.invalidateNotification();
                            loadingDialog.dismiss();
                            Toast.makeText(getContext(), R.string.follow_all, Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void failure(RetrofitError error)
                {
                    loadingDialog.dismiss();
                    if (error != null) {
                        if (error.getResponse() != null && error.getResponse().getStatus() == 403)  // error code 403, user is not registered
                        {
                            startSignUpScreen();
                        }
                        Log.d("TAG", error.getMessage());
                    }

                    showNetworkErrorDialog();
                }
            });
            StateMachine.getInstance(mContext.getApplicationContext()).insertEvent(
                    new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL,
                            StateMachineEvent.EVENT_TYPE.FOLLOW_USER.ordinal()
                    )
            );
        }
        else
        {
            loadingDialog.dismiss();
            Toast.makeText(getContext(), R.string.invite_a_friend_follow_no_friends, Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public void followFriend(final String id)
    {
        if(CAALogin.userIsAnonymous())
        {
            startSignUpScreen();
            return;
        }

        loadingDialog = new LoadingDialog(getContext(),false,false);
        ArrayList<String> Ids = new ArrayList<>();
        Ids.add(id);

        CAAFollowAll userIds = new CAAFollowAll(Ids);

        FollowingDataHandler.getInstance().add(id);
        App.getUrlService().postFollowAll(userIds, new Callback<String>()
        {
            @Override
            public void success(String s, Response response)
            {
                post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        for (User contact : mDataSet)
                        {
                            if (contact.getUserId().equals(id))
                            {
                                contact.setFollowedByMe("1");
                                break;
                            }
                        }
//                        contactInfo.status = ContactInfo.FOLLOWING;
                        numberOfFollowingFriend++;
                        setTitle();
                        mAdapter.notifyDataSetChanged();
                        mInvalidateNotificationListener.invalidateNotification();
                        loadingDialog.dismiss();
                    }
                });
            }

            @Override
            public void failure(RetrofitError error)
            {
                loadingDialog.dismiss();
                if (error != null) {
                    if (error.getResponse() != null && error.getResponse().getStatus() == 403)  // error code 403, user is not registered
                    {
                        startSignUpScreen();
                        return;
                    }
                    Log.d("TAG", error.getMessage());
                }
                showNetworkErrorDialog();
            }
        });
        StateMachine.getInstance(mContext.getApplicationContext()).insertEvent(
                new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL,
                        StateMachineEvent.EVENT_TYPE.FOLLOW_USER.ordinal()
                )
        );
    }

    public void unFollowFriend(final String id)
    {
        if(CAALogin.userIsAnonymous())
        {
            startSignUpScreen();
            return;
        }
        loadingDialog = new LoadingDialog(getContext(),false,false);
        FollowingDataHandler.getInstance().remove(id);
        App.getUrlService().deleteUsersFollow(id, new Callback<String>()
        {
            @Override
            public void success(String s, Response response)
            {
                post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        for (User contact : mDataSet)
                        {
                            if (contact.getUserId().equals(id))
                            {
                                contact.setFollowedByMe("0");
                                break;
                            }
                        }
//                        contactInfo.status = ContactInfo.NOT_FOLLOWING;
                        numberOfFollowingFriend--;
                        setTitle();
                        mAdapter.notifyDataSetChanged();
                        mInvalidateNotificationListener.invalidateNotification();
                        loadingDialog.dismiss();
                    }
                });
            }

            @Override
            public void failure(RetrofitError error)
            {
                loadingDialog.dismiss();
                showNetworkErrorDialog();
            }
        });
    }

    public void startSignUpScreen()
    {
        Intent loginActivityIntent = new Intent(getContext(), SignupActivity.class);
        getContext().startActivity(loginActivityIntent);
    }

    public void notifyDataSetChanged()
    {
        post(new Runnable()
        {
            @Override
            public void run()
            {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    public void close()
    {
        super.close();

        try { followAllLlinearLayout.removeAllViews();followAllLlinearLayout.destroyDrawingCache();}catch (Throwable xz){}followAllLlinearLayout = null;
        followALL.close();
        numberOfContacts = null;
        loadingDialog = null;

        mRecyclerView.removeAllViews(); mRecyclerView.destroyDrawingCache();mRecyclerView = null;
        mAdapter.close();
        mLayoutManager = null;
        mDataSet = null;
    }

}