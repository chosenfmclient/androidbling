package fm.bling.blingy.dialogs.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import fm.bling.blingy.R;


/**
 * Created by Kiril on 5/20/2015.
 */
public class CustomDialogAdapter extends ArrayAdapter<String> {
    private String [] baseList;
    private LayoutInflater inflater;
    TextView tv;

    public CustomDialogAdapter(Context context, String[] items) {
        super(context, R.layout.custom_dialog_list_item, items);
        inflater = LayoutInflater.from(context);
        baseList = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_dialog_list_item, null);
            tv = (TextView) convertView.findViewById(R.id.text_view_dialog);
        }

        String s = baseList[position];
        tv.setText(s);

        return convertView;
    }
}
