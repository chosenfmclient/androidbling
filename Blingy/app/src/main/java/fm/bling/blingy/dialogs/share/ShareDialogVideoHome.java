package fm.bling.blingy.dialogs.share;

import android.app.Activity;

import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.videoHome.model.CAAVideo;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Zach on 7/28/16.
 * History:
 * ***********************************
 */
public class ShareDialogVideoHome extends ShareDialogInstagram {

    public ShareDialogVideoHome(Activity context, ShareDialogCallbackSetter shareDialogCallbackSetter, CAAVideo video, boolean  isAfterRecording)
    {
        super(context, shareDialogCallbackSetter,video);
        this.isAfterRecording = isAfterRecording;
        setContentId(video.getVideoId(), isPhoto ? Constants.PHOTO_TYPE : Constants.VIDEO_TYPE);
    }

    @Override
    public void onInstagramClicked() {
        super.onInstagramClicked();
    }

    @Override
    public void onFacebookClicked()
    {
        sendFacebook();
    }

    @Override
    public void onTwitterClicked()
    {
        sendTwitter();
    }

    @Override
    public void onSmsClicked() {
        sendSms();
    }

    public void onMailClicked()
    {
        sendEmail();
    }
}