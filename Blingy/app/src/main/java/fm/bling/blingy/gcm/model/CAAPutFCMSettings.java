package fm.bling.blingy.gcm.model;

import com.google.gson.annotations.SerializedName;


/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAPutUser
 * Description:
 * Created by Zach Gerstman on 6/22/15.
 * History:
 * ***********************************
 */
public class CAAPutFCMSettings {

//    public String getAppVersion() {
//        return appVersion;
//    }
//
//    public void setAppVersion(String appVersion) {
//        this.appVersion = appVersion;
//    }
//
//    public String getGcmToken() {
//        return gcmToken;
//    }
//
//    public void setGcmToken(String gcmToken) {
//        this.gcmToken = gcmToken;
//    }
//
//    public String getPackageName() {
//        return packageName;
//    }
//
//    public void setPackageName(String packageName) {
//        this.packageName = packageName;
//    }


    @SerializedName("app_version")
    private String appVersion;

    @SerializedName("fcm_token")
    private String gcmToken;

    @SerializedName("package_name")
    private String packageName;

//    public String getOsVersion() {
//        return osVersion;
//    }
//
//    public void setOsVersion(String osVersion) {
//        this.osVersion = osVersion;
//    }

    @SerializedName("os_version")
    private String osVersion;

    public CAAPutFCMSettings(String appVersion, String gcmToken, String packageName, String osVersion) {
        this.appVersion = appVersion;
        this.gcmToken = gcmToken;
        this.packageName = packageName;
        this.osVersion = osVersion;
    }
}
