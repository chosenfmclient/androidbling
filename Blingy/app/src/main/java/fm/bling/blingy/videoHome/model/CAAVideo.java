package fm.bling.blingy.videoHome.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


import java.util.ArrayList;

import fm.bling.blingy.editVideo.rest.model.CAASongInfo;
import fm.bling.blingy.mashup.model.MashupOBJ;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.videoHome.fragments.CAAFeatured;

public class CAAVideo implements Parcelable{

    @SerializedName("video_id")
    private String videoId;

    @SerializedName("parent_id")
    private String parentId;

    @SerializedName("clip_id")
    private String clipId;

    @SerializedName("user")
    private User user;

    @SerializedName("parent")
    private CAAVideo parent;

    @SerializedName("type")
    private String type;

    @SerializedName("status")
    private String status;

    @SerializedName("date")
    private String date;

    @SerializedName("song_name")
    private String songName;

    @SerializedName("artist_name")
    private String artistName;

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("first_frame_url")
    private String firstFrame;

    @SerializedName("url")
    private String url;

    @SerializedName("video_url")
    private String videoUrl;

    @SerializedName("clip")
    private ArrayList<String> clip;

    @SerializedName("clip_url")
    private String clipUrl;

    @SerializedName("total_likes")
    private String totalLikes;

    @SerializedName("liked_by_me")
    private String likedByMe;

    @SerializedName("total_comments")
    private String totalComments;

    @SerializedName("media_type")
    private String mediaType;

    @SerializedName("intro_url")
    private String introUrl;

    @SerializedName("shared_url")
    private String sharedUrl;

    @SerializedName("instagram_url")
    private String instagramUrl;

    @SerializedName("song_clip_url")
    private String songClipUrl;

    @SerializedName("song_image_url")
    private String songImageUrl;

    @SerializedName("cameo_permission")
    private boolean cameoPermission = false;

    @SerializedName("video_permission_level")
    private int permissionLevel;

    public CAAVideo(){

    }


    public boolean isCameoPermission() {
        return cameoPermission;
    }

    public int getPermissionLevel() { return permissionLevel; }

    public void setPermissionLevel(int level) { permissionLevel = level; }

    public String getParentId() {
        return parentId;
    }

    public String getSongClipUrl() {
        return songClipUrl;
    }

    public String getSongImageUrl() {
        return songImageUrl;
    }


    public String getInstagramUrl() {
        if(instagramUrl != null)
            return instagramUrl;
        else return videoUrl;
    }

    public String getSharedUrl(){
        if(sharedUrl != null)
            return sharedUrl;
        else return videoUrl;
    }

    public String getIntroUrl(){
        return introUrl;
    }

    public Integer getTotalComments() {
        return totalComments == null || totalComments.isEmpty() ? 0 : Integer.parseInt(totalComments);
    }

    public void setTotalComments(int totalComments){
        this.totalComments = totalComments < 0 ? "0" : String.valueOf(totalComments);
    }

    public String getFirstFrame() {
        return firstFrame != null ? firstFrame : thumbnail;
    }

    public String getMediaType() {
        return mediaType;
    }

    public int getTotalLikes() {
        return totalLikes == null || totalLikes.isEmpty() ? 0 : Integer.parseInt(totalLikes);
    }

    public void setTotalLikes(Integer totalLikes) {
        this.totalLikes = String.valueOf(totalLikes);
    }

    public Integer getLikedByMe() {
        return likedByMe == null || likedByMe.isEmpty() ? 0 : Integer.parseInt(likedByMe);
    }

    public void setLikedByMe(Integer likedByMe) {
        this.likedByMe = String.valueOf(likedByMe);
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public CAAVideo getParent() {
        return parent;
    }

    public void setParent(CAAVideo parent) {
        this.parent = parent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public ArrayList<String> getClip() {
        return clip;
    }

    public String getClipId() {
        return clipId;
    }

    protected CAAVideo(Parcel in) {
        videoId = in.readString();
        parentId = in.readString();
        clipId = in.readString();
        user = (User) in.readValue(User.class.getClassLoader());
        parent = (CAAVideo) in.readValue(CAAVideo.class.getClassLoader());
        type = in.readString();
        status = in.readString();
        date = in.readString();
        songName = in.readString();
        artistName = in.readString();
        thumbnail = in.readString();
        firstFrame = in.readString();
        url = in.readString();
        videoUrl = in.readString();
        if (in.readByte() == 0x01) {
            clip = new ArrayList<String>();
            in.readList(clip, String.class.getClassLoader());
        } else {
            clip = null;
        }
        clipUrl = in.readString();
        totalLikes = in.readString();
        likedByMe = in.readString();
        totalComments = in.readString();
        mediaType = in.readString();
        introUrl = in.readString();
        sharedUrl = in.readString();
        instagramUrl = in.readString();
        songClipUrl = in.readString();
        songImageUrl = in.readString();
        cameoPermission = in.readByte() != 0x00;
        permissionLevel = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(videoId);
        dest.writeString(parentId);
        dest.writeString(clipId);
        dest.writeValue(user);
        dest.writeValue(parent);
        dest.writeString(type);
        dest.writeString(status);
        dest.writeString(date);
        dest.writeString(songName);
        dest.writeString(artistName);
        dest.writeString(thumbnail);
        dest.writeString(firstFrame);
        dest.writeString(url);
        dest.writeString(videoUrl);
        if (clip == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(clip);
        }
        dest.writeString(clipUrl);
        dest.writeString(totalLikes);
        dest.writeString(likedByMe);
        dest.writeString(totalComments);
        dest.writeString(mediaType);
        dest.writeString(introUrl);
        dest.writeString(sharedUrl);
        dest.writeString(instagramUrl);
        dest.writeString(songClipUrl);
        dest.writeString(songImageUrl);
        dest.writeByte((byte) (cameoPermission ? 0x01 : 0x00));
        dest.writeInt(permissionLevel);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CAAVideo> CREATOR = new Parcelable.Creator<CAAVideo>() {
        @Override
        public CAAVideo createFromParcel(Parcel in) {
            return new CAAVideo(in);
        }

        @Override
        public CAAVideo[] newArray(int size) {
            return new CAAVideo[size];
        }
    };

}
