package fm.bling.blingy.inviteFriends.listeners;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/16/16.
 * History:
 * ***********************************
 */
public interface FollowListener
{
    void follow(String id);
    void unFollow(String id);
}