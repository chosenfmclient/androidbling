package fm.bling.blingy.editVideo.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import fm.bling.blingy.rest.ItemTypeAdapterFactory;
import fm.bling.blingy.rest.UrlPostService;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: RestClientUploadFile
 * Description:
 * Created by Dawidowicz Nadav on 5/19/15.
 * History:
 * ***********************************
 */
public class RestClientUploadFile
{

    private static String BASE_URL = "http://storage.googleapis.com/";
    private UrlPostService apiService;

    public RestClientUploadFile(String url)
    {

        OkHttpClient client = new OkHttpClient();
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        client.setCookieHandler(cookieManager);
//        client.interceptors().add(new CAAResponseInterceptor());
        client.setReadTimeout(30, TimeUnit.MINUTES);
        client.setWriteTimeout(30, TimeUnit.MINUTES);
        client.setConnectTimeout(30, TimeUnit.MINUTES);

        OkClient serviceClient = new OkClient(client);
        BASE_URL = url;
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.HEADERS)
                .setEndpoint(BASE_URL)
                .setConverter(new GsonConverter(gson))
                .setClient(serviceClient)
                .build();

        apiService = restAdapter.create(UrlPostService.class);

    }

    public UrlPostService getUrlPostService()
    {
        return apiService;
    }
}
