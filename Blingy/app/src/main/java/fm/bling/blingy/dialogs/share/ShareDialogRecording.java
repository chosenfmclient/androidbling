package fm.bling.blingy.dialogs.share;

import android.app.Activity;

import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.utils.Constants;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Zach on 2/28/16.
 * History:
 * ***********************************
 */
public class ShareDialogRecording extends ShareDialogInstagram {

    public ShareDialogRecording(Activity context, ShareDialogProccessCalback shareDialogProccessCalback,
                                String videoId, String userId,String videoType, String songName, boolean isDuet, String filePath) {
        super(context, shareDialogProccessCalback, songName, videoId, userId, isDuet);
        setContentId(videoId, isPhoto ? Constants.PHOTO_TYPE : Constants.VIDEO_TYPE);
        this.videoType = videoType;
        this.videoID = videoId;
        this.songName = songName;
        this.fileUserName = CAAUserDataSingleton.getInstance().getFullName();
        this.mFilePath = filePath;
        this.isAfterRecording = true;
    }

    @Override
    public void onInstagramClicked() {
        super.onInstagramClicked();
    }

    @Override
    public void onFacebookClicked()
    {
        sendFacebook();
    }

    @Override
    public void onTwitterClicked()
    {
        sendTwitter();
    }

    @Override
    public void onSmsClicked()
    {
        sendSms();
    }

    @Override
    public void onMailClicked()
    {
        sendEmail();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public boolean isFacebookClicked(){
        return this.mIsFacebookClicked;
    }

    public void callSendFacebook(){
        sendFacebook();
    }
}