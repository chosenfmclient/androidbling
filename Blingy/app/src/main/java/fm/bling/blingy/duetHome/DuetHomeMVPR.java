package fm.bling.blingy.duetHome;


import java.util.ArrayList;

import fm.bling.blingy.duetHome.model.DuetHomeFeed;
import fm.bling.blingy.videoHome.model.CAAVideo;

public interface DuetHomeMVPR {

    interface View {

        void init();

        void listenToAudioChanges();

        void MusicPrepare();

        void playMusic();

        void adjustTextureViewSize();

        void releaseMediaPlayer();

        void setUpLabel(String label, String color);

        void showDuetData(ArrayList<CAAVideo> trendingVideos);

        void setupFeatured(final CAAVideo featuredDuet);

        void loadDataFailed();
    }

    interface Presenter {

        void setView(DuetHomeMVPR.View view);

        void getDuetHome(int page);

        void loadDuetHomeData(DuetHomeFeed data);

        void loadDataFailed();

    }

    interface Model {

        void getDuetHome(int page);

        void loadDuetHomeData(DuetHomeFeed data);

        void loadDataFailed();

    }

    interface Repository {

        void getDuetHome(int page);
    }
}
