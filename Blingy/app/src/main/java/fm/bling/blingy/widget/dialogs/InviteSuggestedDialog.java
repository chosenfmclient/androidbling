package fm.bling.blingy.widget.dialogs;

import android.os.Handler;
import android.view.View;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.EnterNameDialog;
import fm.bling.blingy.dialogs.listeners.EnterNameDialogListener;
import fm.bling.blingy.inviteFriends.model.CAAEmailInvite;
import fm.bling.blingy.inviteFriends.model.CAAEmailInvites;
import fm.bling.blingy.inviteFriends.model.CAASmsInvite;
import fm.bling.blingy.inviteFriends.model.CAASmsInvites;
import fm.bling.blingy.inviteFriends.model.ContactInfo;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.stateMachine.StateMachineMessageListener;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.widget.CAAWidgetSettingsSingleton;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 7/13/16.
 * History:
 * ***********************************
 */
public class InviteSuggestedDialog extends SuggestedFriendsDialog {

    private ArrayList<ContactInfo> mContactsList;

    public InviteSuggestedDialog(StateMachineMessageListener onReadyListener, BaseActivity context, StateMachineEvent.MESSAGE_TYPE messageType, ArrayList<ContactInfo> contactInfos) {
        super(onReadyListener, context, messageType, contactInfos.size());
        this.mContactsList = contactInfos;
        setUpUserCurrent();
    }

    @Override
    public void setUpUserCurrent() {
        ContactInfo contactInfo = mContactsList.get(mCurrentUserNum);
        mButtonUserPic.setUrl(contactInfo.photoUrl);
        StringBuilder stringBuilder = new StringBuilder("Invite ");
        stringBuilder.append(contactInfo.name);
        mMainText.setText(stringBuilder.toString());
        mButtonText.setText("INVITE");
        dialogLayout.findViewById(R.id.top_bar_container).setVisibility(View.VISIBLE);
//        mPointsLayout.setVisibility(View.VISIBLE);

        mButtonUserPic.setVisibility(View.VISIBLE);

        if(mImageLoaderManager == null && mContext != null)
            this.mImageLoaderManager = mContext.getImageLoaderManager();

        if(mImageLoaderManager != null)
            mImageLoaderManager.DisplayImage(mButtonUserPic);

        mButtonFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CAAUserDataSingleton.getInstance().getFullName().trim().isEmpty()) {
                    String name = SharedPreferencesManager.getInstance().getString("temp_name", null);
                    if (name == null || name.isEmpty()) {
                        hideDialog();
                        getNameThenInviteUser();
                    } else {
                        CAAUserDataSingleton.getInstance().setFirstName(name);
                        inviteUser();
                    }
                } else {
                    inviteUser();
                }
            }
        });
    }

    private void getNameThenInviteUser()
    {
        EnterNameDialog enterNameDialog = new EnterNameDialog(getContext(), R.string.invite_enter_your_name, R.string.invite_enter_your_name_dialog_wrong_name, 3, new EnterNameDialogListener() {
            @Override
            public void onDoneClicked() {
                String name = CAAUserDataSingleton.getInstance().getFullName();
                if (name.length() == 0)
                    return;
                inviteUser();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        reShowDialog();
                    }
                }, 400);
            }

            @Override
            public void onCancel() {
                reShowDialog();
            }
        });
        enterNameDialog.show();
    }

    private void inviteUser()
    {
        StateMachine.getInstance(mContext.getApplicationContext()).insertEvent(
                new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL,
                        StateMachineEvent.EVENT_TYPE.INVITE_FRIEND.ordinal()
                )
        );
        if (mCurrentUserNum >= 0) {
            if (messageType == StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_SMS) {
                StateMachine.getInstance(mContext.getApplicationContext()).excludeFromInvites(mContactsList.get(mCurrentUserNum).phone(), StateMachine.INVITE_EXCLUDE_TYPE_PHONE);
                ArrayList<CAASmsInvite> number = new ArrayList<>();
                number.add(new CAASmsInvite(mContactsList.get(mCurrentUserNum).phone(), mContactsList.get(mCurrentUserNum).getName()));
                CAASmsInvites smsInvite = new CAASmsInvites(number);

                App.getUrlService().postSmsInvite(smsInvite, new Callback<String>() {
                    @Override
                    public void success(String result, Response response) {

                    }

                    @Override
                    public void failure(RetrofitError e) {

                    }
                });
            } else if (messageType == StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_EMAIL) {
                StateMachine.getInstance(mContext.getApplicationContext()).excludeFromInvites(mContactsList.get(mCurrentUserNum).email, StateMachine.INVITE_EXCLUDE_TYPE_EMAIL);
                ArrayList<CAAEmailInvite> invites = new ArrayList<>();
                CAAEmailInvite invite = new CAAEmailInvite(mContactsList.get(mCurrentUserNum).email, mContactsList.get(mCurrentUserNum).name);
                invites.add(invite);
                CAAEmailInvites emailInvites = new CAAEmailInvites(invites, CAAUserDataSingleton.getInstance().getFullName());

                App.getUrlService().postEmailInvite(emailInvites, new Callback<String>() {
                    @Override
                    public void success(String result, Response response) {

                    }

                    @Override
                    public void failure(RetrofitError e) {

                    }
                });
            }

//            showNexPointsPopUp(" Friend Invited!!");
            CAAWidgetSettingsSingleton.getInstance().addPoints();

            mContactsList.remove(mCurrentUserNum);
            mCurrentUserNum--;
            if (mContactsList.isEmpty())
            {
                dismiss();
            }
            else
            {
                setListSize(mContactsList.size());
                super.moveNextUser();
            }
        }
    }

    @Override
    protected void moveNextUser()
    {
        if (messageType == StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_SMS) {
            StateMachine.getInstance(mContext.getApplicationContext()).excludeFromInvites(mContactsList.get(mCurrentUserNum).phone(), StateMachine.INVITE_EXCLUDE_TYPE_PHONE);
        } else {
            StateMachine.getInstance(mContext.getApplicationContext()).excludeFromInvites(mContactsList.get(mCurrentUserNum).email, StateMachine.INVITE_EXCLUDE_TYPE_EMAIL);
        }
        super.moveNextUser();
    }
}
