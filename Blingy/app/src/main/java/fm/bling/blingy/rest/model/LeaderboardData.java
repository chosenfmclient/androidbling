package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 02/03/17.
 * History:
 * ***********************************
 */
public class LeaderboardData {

    @SerializedName("data")
    ArrayList<User> users;

    public ArrayList<User> getUsers() {
        return users;
    }
}
