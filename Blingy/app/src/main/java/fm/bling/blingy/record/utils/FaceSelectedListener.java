package fm.bling.blingy.record.utils;


import fm.bling.blingy.record.rest.model.CAAFaceDetection;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 5/17/16.
 * History:
 * ***********************************
 */
public interface FaceSelectedListener {
    void onFaceSelected(CAAFaceDetection.CAAFaceDetectionObject faceDetectionObject, int selectedItemIndex);
}
