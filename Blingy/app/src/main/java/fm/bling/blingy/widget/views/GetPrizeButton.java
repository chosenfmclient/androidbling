package fm.bling.blingy.widget.views;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.utils.imagesManagement.views.ClickableTextView;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 8/15/16.
 * History:
 * ***********************************
 */
public class GetPrizeButton extends FrameLayout {
    private ClickableTextView prize;
    private int shadowShiftY;
    private int scaledButtonWidth;

    public GetPrizeButton(Context context, OnClickListener onClickListener) {
        super(context);
        float buttonWidth = 180f;
        scaledButtonWidth = (int) (buttonWidth * App.SCALE_X);
        shadowShiftY = (int) (10f * App.SCALE_Y);

        prize = new ClickableTextView(context);
        prize.setVisibility(INVISIBLE);
        prize.setText("GET\nPRIZE");
        prize.setLayoutParams(new LayoutParams(scaledButtonWidth, scaledButtonWidth));
        prize.setBackgroundResource(R.drawable.orange_gradient_oval);
        prize.setClickable(true);
        prize.setTextColor(Color.WHITE);
        prize.setGravity(Gravity.CENTER);
        prize.setTypeface(App.ROBOTO_MEDIUM);
        prize.setShadow(shadowShiftY, shadowShiftY, Color.BLACK, scaledButtonWidth);
        prize.setOnClickListener(onClickListener);

        setLayoutParams(new LayoutParams(scaledButtonWidth, scaledButtonWidth));
        addView(prize);
    }

    public void close()
    {
        removeAllViews(); destroyDrawingCache();
        prize.close();
        prize = null;
    }

    public void show()
    {
        if(prize.getVisibility() == VISIBLE)
            return;

        Animation anim1 = AnimationUtils.loadAnimation(getContext(), R.anim.scale_up_200);
        anim1.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation)
            {
                prize.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        prize.startAnimation(anim1);
    }

    public void hide()
    {
        if(prize.getVisibility() == INVISIBLE)
            return;

        Animation anim1 = AnimationUtils.loadAnimation(getContext(), R.anim.scale_down_200);
        anim1.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation)
            {
                prize.setVisibility(INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        prize.startAnimation(anim1);
    }
}
