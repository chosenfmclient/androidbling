package fm.bling.blingy.registration.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAPostSignup
 * Description:
 * Created by Dawidowicz Nadav on 5/12/15.
 * History:
 * ***********************************
 */
public class CAAPostSignup {
    public CAAPostSignup(String email, String password, String appId, String secret, String firstName, String lastName) {
        this.email = email;
        this.password = password;
        this.appId = appId;
        this.secret = secret;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    @SerializedName("app_id")
    private String appId;

    @SerializedName("secret")
    private String secret;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
