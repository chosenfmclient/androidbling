package fm.bling.blingy.inviteFriends.views;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.utils.imagesManagement.views.ClickableTextView;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 3/6/16.
 * History:
 * ***********************************
 */
public class InviteTabContentParkingView extends LinearLayout
{
    private TextView textView;
    private View mainImage;
    private LinearLayout paddingLayout;
    private ClickableTextView connectButton;

    public InviteTabContentParkingView(Context context, OnClickListener onClickListener)
    {
        super(context);

        int padding = (int) (80f * App.SCALE_X);//context.getResources().getDimensionPixelSize(R.dimen.main_margin);

        textView = new TextView(context);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, 80f * App.SCALE_X);
        textView.setTextColor(Color.BLACK);
        textView.setText(R.string.blingy_works_better_with_friends);
        textView.setLayoutParams(new LayoutParams(App.WIDTH - (padding * 2), LayoutParams.WRAP_CONTENT));
        textView.setGravity(Gravity.CENTER);
        textView.setPadding(0, 0, 0, (int) (130f * App.SCALE_X));
        textView.setTypeface(App.ROBOTO_REGULAR);
        textView.setAllCaps(true);

        mainImage = new View(context);
        mainImage.setLayoutParams(new LayoutParams((int) (700f * App.SCALE_X), (int) (318.182f * App.SCALE_X)));
        mainImage.setBackgroundResource(R.drawable.invite_friends_pics);

        paddingLayout = new LinearLayout(context);
        paddingLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, (int)(159f * App.SCALE_X)));

        connectButton = new ClickableTextView(context);
        connectButton.setLayoutParams(new LayoutParams(App.WIDTH - (padding * 2), (int) (180f * App.SCALE_X)));
        //connectButton.setPadding(0, padding, 0, padding);
        connectButton.setBackgroundResource(R.drawable.button_shape_orange_color);
        connectButton.setSingleLine();
        connectButton.setText(R.string.connect_to_invite_friends);
        connectButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, 50f * App.SCALE_X);
        connectButton.setGravity(Gravity.CENTER);
        connectButton.setTextColor(Color.WHITE);
        connectButton.setIncludeFontPadding(false);
        connectButton.setClickable(true);
        connectButton.setOnClickListener(onClickListener);
        connectButton.setTypeface(App.ROBOTO_REGULAR);
        connectButton.setAllCaps(true);

        setBackgroundResource(R.color.white);
        setGravity(Gravity.CENTER);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        setOrientation(VERTICAL);

        addView(textView);
        addView(mainImage);
        addView(paddingLayout);
        addView(connectButton);
    }

    public void close()
    {
        removeAllViews();destroyDrawingCache();
        textView      = null;
        mainImage     = null;
        paddingLayout = null;
        connectButton = null;
    }

}