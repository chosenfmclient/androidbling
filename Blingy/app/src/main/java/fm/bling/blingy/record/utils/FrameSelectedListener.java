package fm.bling.blingy.record.utils;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 5/14/16.
 * History:
 * ***********************************
 */
public interface FrameSelectedListener {
    void onFrameSelected(String frameID, String framePath, int selectedItemIndex);
}