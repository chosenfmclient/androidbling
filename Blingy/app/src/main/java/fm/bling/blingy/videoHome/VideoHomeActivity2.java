//package fm.bling.blingy.videoHome;
//
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.pm.ActivityInfo;
//import android.content.res.Configuration;
//import android.graphics.Color;
//import android.graphics.Matrix;
//import android.graphics.SurfaceTexture;
//import android.hardware.SensorManager;
//import android.media.MediaMetadataRetriever;
//import android.media.MediaPlayer;
//import android.os.AsyncTask;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.Handler;
//import android.provider.Settings;
//import android.support.v7.widget.Toolbar;
//import android.view.ContextThemeWrapper;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.MenuItem;
//import android.view.MotionEvent;
//import android.view.OrientationEventListener;
//import android.view.Surface;
//import android.view.TextureView;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.ViewTreeObserver;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.PopupMenu;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import java.net.HttpURLConnection;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Timer;
//import java.util.TimerTask;
//import java.util.concurrent.atomic.AtomicInteger;
//
//import fm.bling.blingy.App;
//import fm.bling.blingy.BaseActivity;
//import fm.bling.blingy.R;
//import fm.bling.blingy.dialogs.BaseDialog;
//import fm.bling.blingy.dialogs.TouchBlockerDialog;
//import fm.bling.blingy.dialogs.comment.CommentBoxDialog;
//import fm.bling.blingy.dialogs.flag.FlagVideoDialog;
//import fm.bling.blingy.dialogs.listeners.FlagDialogListener;
//import fm.bling.blingy.dialogs.model.CAAType;
//import fm.bling.blingy.dialogs.share.ShareDialogVideoHome;
//import fm.bling.blingy.dialogs.share.workers.RenderForInstagram;
//import fm.bling.blingy.dialogs.share.workers.listeners.onRenderInstagramFinished;
//import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
//import fm.bling.blingy.profile.ProfileActivity;
//import fm.bling.blingy.registration.CAALogin;
//import fm.bling.blingy.registration.SignupActivity;
//import fm.bling.blingy.rest.model.CAACommentParams;
//import fm.bling.blingy.rest.model.CAACommentsResponse;
//import fm.bling.blingy.rest.model.CAALike;
//import fm.bling.blingy.rest.model.CAAStatus;
//import fm.bling.blingy.rest.model.User;
//import fm.bling.blingy.singletones.CAAUserDataSingleton;
//import fm.bling.blingy.tracking.FlurryConstants;
//import fm.bling.blingy.tracking.TrackingManager;
//import fm.bling.blingy.utils.Constants;
//import fm.bling.blingy.utils.imagesManagement.ImageLoaderListener;
//import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
//import fm.bling.blingy.utils.imagesManagement.views.URLImageView;
//import fm.bling.blingy.videoHome.model.CAAVideo;
//import fm.bling.blingy.videoHome.model.CAAVideoHome;
//import retrofit.Callback;
//import retrofit.RetrofitError;
//import retrofit.client.Response;
//
///**
// * *********************************
// * Project: Chosen Android Application
// * Description:
// * Created by Oren Zakay on 8/25/16.
// * History:
// * ***********************************
// */
//public class VideoHomeActivity2 extends BaseActivity implements FlagDialogListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener/*, CommentBoxDialog.CommentBoxListener */{
//
//    /**
//     * Main Views
//     */
//    private FrameLayout mContainer;
//    private URLImageView mPreviewThumbnail;
//    private ScaledImageView mBluredThumbnail;
//    private TextureView mVideoTextureView;
//    private ImageView mPlayPauseButton;
//    private ProgressBar mProgressBar;
//    private ScaledImageView mUserPic;
//    private LinearLayout mActionsContainer;
//    private ImageView mMoreButton;
//    private ImageView mShareButton;
//    private ImageView mLikeButton;
////    private TextView mTotalLikesView;
//    private ImageView mActivityButton;
//    private FrameLayout mCommentsContainer;
//    private LinearLayout mMyCommentContainer;
//    private ScaledImageView mCommentUSerPic;
//    private LinearLayout mSaySomething;
//    private TextView mCommentCount;
//    private TextView mTagsText;
//    private MediaPlayer mMediaPlayer;
//    private ImageLoaderListener videoPreviewListener;
//
//    private Toolbar mToolbar;
//    private Context mWrapperContext;
//
//    private CAACommentsResponse mComments = null;
//    private CAACommentParams myComment;
//    private boolean videoHasNoComments = false;
//    private boolean timeToShowComments = false;
//    private boolean commentsLaunched = false;
//    private boolean mLongCommentDelay = false;
//    private Timer mTimer;
//    private AtomicInteger commentIndex = new AtomicInteger(0);
//
//    private LayoutInflater mInflater;
//
//    private CAAVideo videoObj;
//    private String videoUri;
//    private String userID = null;
//    private String userName = null;
//    private boolean photoMode = false;
//
//    private int orientation;
//    private Handler mHandler = new Handler();
//    private String photo;
//    private boolean isShowing = true;
//    private String videoID;
//    private String mMediaType = "video";
//    private String videoType;
//    private String videoStatus;
//    private boolean isOnEventPaused;
//    private boolean isVideoFinished = false;
//
//    private boolean isWhite = false;
//    private OrientationEventListener myOrientationEventListener;
//    private static final int THRESHOLD = 30;
//
//    private boolean mInBackground;
//    private int rotation;
//
//    private boolean isAfterRecording = false;
//    private String  mOriginalVideoPath;
//    private String  mFrame1x1Path;
//    private String  mLogo1x1Path;
//
//    private ShareDialogVideoHome mShareDialog;
//    private boolean wentToSignUp = false;
//
//    private boolean hasMargins;
//    private int textureMargin = 0;
//
//    private TouchBlockerDialog mTouchBlockerDialog;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.video_home_2);
//        init();
//        setToolbar();
//
//        mUserPic.setEnabled(false);
//        mShareButton.setEnabled(false);
//        mLikeButton.setEnabled(false);
//        mMoreButton.setEnabled(false);
//        mActivityButton.setEnabled(false);
//        mProgressBar.setVisibility(View.VISIBLE);
//
//        if(getIntent().hasExtra(Constants.IS_NOTIFICATION))
//            mTouchBlockerDialog = new TouchBlockerDialog(this);
//
//        //Sets the content behind the status bar.
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
//                                                             View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//            getWindow().setStatusBarColor(Color.TRANSPARENT);
//        }
//
//        int topPadding = getResources().getDimensionPixelOffset(R.dimen.filter_button_vertical_padding);
//        mActionsContainer.setPadding(0, App.TOOLBAR_HEIGHT + (topPadding * 2), 0, 0);
//
//        mWrapperContext = new ContextThemeWrapper(this, R.style.PopupMenu);
//        mInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        Intent intent = getIntent();
//        if(intent.hasExtra(Constants.IS_AFTER_REC)){
//            isAfterRecording   = true;
//            mOriginalVideoPath = intent.getStringExtra(Constants.ORIGINAL_VIDEO_PATH);
//            mFrame1x1Path      = intent.getStringExtra(Constants.FRAME_1X1_PATH);
//            mLogo1x1Path       = intent.getStringExtra(Constants.LOGO_1X1_PATH);
//        }
//
//        videoID = getIntent().getStringExtra(Constants.VIDEO_ID);
//
//        loadCommonOnClickEvents();
//
//        myOrientationEventListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
//
//            @Override
//            public void onOrientationChanged(int orientationDegree) {
//                //Detect if user enable the auto rotation.
//                boolean mAutoRotateEnabled = Settings.System.getInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) == 1 ? true : false;
//                if (mAutoRotateEnabled) {
//                    if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
//                        if ((orientationDegree > 90 - THRESHOLD && orientationDegree < 90 + THRESHOLD) || (orientationDegree < 270 + THRESHOLD && orientationDegree > 270 - THRESHOLD)) {
//                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//                            myOrientationEventListener.disable();
//                        }
//
//                    } else {
//                        if ((orientationDegree < THRESHOLD && orientationDegree >= 0) || (orientationDegree > 360 - THRESHOLD && orientationDegree <= 360)) {
//                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//                            myOrientationEventListener.disable();
//                        }
//                    }
//                }
//            }
//        };
//
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        myOrientationEventListener.disable();
//
//        TrackingManager.getInstance().pageViewVideoHome();
//    }
//
//
//    @Override
//    protected void onStart() {
//        if (NetworkConnectivityListener.isConnectedWithDialog(this)) {
//            getVideoHome();
//            getComments();
//        }
////        else {
////            showNoConnectionDialog();
////        }
//        super.onStart();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        isActivityVisible = true;
//        mInBackground = false;
//        showWidget = false;
//        adjustTextureViewSize();
//
//        if(wentToSignUp){ // Checking weather the user clicked like and has logged in (update the heart icon)
//            wentToSignUp = false;
//            if(!(CAALogin.userIsAnonymous(getApplicationContext())))
//                mLikeButton.callOnClick();
//        }
//
//        if (isOnEventPaused && !isVideoFinished) {
//            isOnEventPaused = false;
//            if(mVideoTextureView != null && mMediaPlayer != null)
//                if(mVideoTextureView.isAvailable()) {
//                    resumeMediaPlayer();
//                }
//                else
//                    attachSurface(true);
//            else
//                prepareMediaPlayer();
//            displayComments();
//        }
//
//    }
//
//    @Override
//    protected void onPause() {
//        if (mMediaPlayer != null && mMediaPlayer.isPlaying() && mVideoTextureView != null) {
//            pauseMediaPlayer();
//        }
//        mInBackground = true;
//        super.onPause();
//    }
//
//    @Override
//    protected void onStop() {
//        showWidget = true;
//        cancelCommentAniamtions();
//        super.onStop();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        releaseMediaPlayer();
//        mVideoTextureView = null;
//    }
//
//    @Override
//    public void onBackPressed() {
//        TrackingManager.getInstance().tapBack(FlurryConstants.VIDEO_HOME_FLURRY);
//        super.onBackPressed();
//    }
//
//    private void init() {
//      mContainer = (FrameLayout)findViewById(R.id.video_home_container);
//      mPreviewThumbnail = (URLImageView)findViewById(R.id.video_image_preview);
//      mBluredThumbnail = (ScaledImageView)findViewById(R.id.blur_thumbnail);
//      mVideoTextureView = (TextureView)findViewById(R.id.texture_view_video);
//      mPlayPauseButton = (ImageView)findViewById(R.id.play_pause);
//      mProgressBar = (ProgressBar)findViewById(R.id.loading_progressbar);
//      mUserPic = (ScaledImageView)findViewById(R.id.user_pic);
//      mActionsContainer = (LinearLayout)findViewById(R.id.actions_container);
//      mMoreButton = (ImageView)findViewById(R.id.more_button);
//      mShareButton = (ImageView)findViewById(R.id.share_button);
//      mLikeButton = (ImageView)findViewById(R.id.like_button);
////      mTotalLikesView = (TextView)findViewById(R.id.total_likes);
//      mActivityButton = (ImageView)findViewById(R.id.activity_button);
//      mCommentsContainer = (FrameLayout)findViewById(R.id.comment_layout);
//      mMyCommentContainer = (LinearLayout)findViewById(R.id.my_comment);
//      mCommentUSerPic = (ScaledImageView)findViewById(R.id.my_pic);
//      mSaySomething = (LinearLayout)findViewById(R.id.say_something);
//      mCommentCount = (TextView)findViewById(R.id.commentNumber);
//      mTagsText = (TextView)findViewById(R.id.tags_text);
//    }
//
//    public void releaseMediaPlayer() {
//        if (mMediaPlayer != null) {
//            mMediaPlayer.stop();
//            mMediaPlayer.release();
//            mMediaPlayer = null;
//        }
//    }
//
//    private void pauseMediaPlayer(){
//        if(mMediaPlayer != null) {
//            isOnEventPaused = true;
//            mMediaPlayer.pause();
//        }
//    }
//
//    private void resumeMediaPlayer(){
//        if(mMediaPlayer != null) {
//            mPlayPauseButton.setVisibility(View.INVISIBLE);
//            isOnEventPaused = false;
//            isVideoFinished = false;
//            mMediaPlayer.start();
//        }
//    }
//
//    private void adjustPreviewPicture(){
//        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
//            (mPreviewThumbnail).getLayoutParams().width = FrameLayout.LayoutParams.MATCH_PARENT;
//            (mPreviewThumbnail).getLayoutParams().height = FrameLayout.LayoutParams.WRAP_CONTENT;
//            ((FrameLayout.LayoutParams) mPreviewThumbnail.getLayoutParams()).gravity = Gravity.CENTER;
//            ((FrameLayout.LayoutParams)mPreviewThumbnail.getLayoutParams()).setMargins(0,0, 0, 0);
//            mPreviewThumbnail.requestLayout();
//        }
//        else {
//            mPreviewThumbnail.getLayoutParams().width = FrameLayout.LayoutParams.WRAP_CONTENT;
//            mPreviewThumbnail.getLayoutParams().height = FrameLayout.LayoutParams.MATCH_PARENT;
//            ((FrameLayout.LayoutParams) mPreviewThumbnail.getLayoutParams()).gravity = Gravity.TOP;
//            if(hasMargins){
//                ((FrameLayout.LayoutParams)mPreviewThumbnail.getLayoutParams()).setMargins(0,0, 0, 0);
//            }
//            mPreviewThumbnail.setLayoutWidthHeight(App.WIDTH, ViewGroup.LayoutParams.WRAP_CONTENT);
//            mPreviewThumbnail.requestLayout();
//            mImageLoaderManager.DisplayImage(mPreviewThumbnail);
//        }
//    }
//
//    private  void adjustTextureViewSize()
//    {
//        if (mVideoTextureView == null || mMediaPlayer == null)
//            return;
//
//        float viewWidth = mVideoTextureView.getWidth();
//        float viewHeight = mVideoTextureView.getHeight();
//        //Set it Top Centered.
//        int yoff = 0;
//
//        double aspectRatio;
//        try
//        {
//            aspectRatio = ((double) mMediaPlayer.getVideoHeight()) / ((double) mMediaPlayer.getVideoWidth());
//        }
//        catch (Throwable err)
//        {
//            err.printStackTrace();
//            return;
//        }
//
//        final float newWidth, newHeight;
//
//        if (viewHeight > (int) (viewWidth * aspectRatio))
//        {
//            // limited by narrow width; restrict height
//            newWidth = viewWidth;
//            newHeight = (int) (viewWidth * aspectRatio);
//        }
//        else
//        {
//            // limited by short height; restrict width
//            newWidth = (int) (viewHeight / aspectRatio);
//            newHeight = viewHeight;
//        }
//
//        int xoff = (int) ((viewWidth - newWidth) / 2f);
//        Matrix txform = new Matrix();
//        mVideoTextureView.getTransform(txform);
//        txform.setScale( newWidth / viewWidth, newHeight / viewHeight);
//        txform.postTranslate(xoff, yoff);
//
//        mVideoTextureView.setTransform(txform);
//        mVideoTextureView.requestLayout();
//        setUpPlayButtonLocation(newHeight);
//
//    }
//
//    private void setUpPlayButtonLocation(final float newHeight) {
//
//        mPlayPauseButton.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                if(!isVideoFinished)
//                    mPlayPauseButton.setVisibility(View.INVISIBLE);
//                int[] pos = new int[2];
//                mVideoTextureView.getLocationOnScreen(pos);
//
//                if(orientation == Configuration.ORIENTATION_LANDSCAPE)
//                    mPlayPauseButton.setY(((mContainer.getHeight() / 2)) - (mPlayPauseButton.getHeight() / 2));
//                else
//                    mPlayPauseButton.setY(pos[1] + ((newHeight / 2)) - (mPlayPauseButton.getHeight() / 2));
//                mPlayPauseButton.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//            }
//        });
//    }
//
//    private boolean rotationSet;
//    private void setRotation() // compatibility for api 17 | 18
//    {
//        if(rotationSet || videoObj == null || !mVideoTextureView.isAvailable())
//            return;
//        rotationSet = true;
//        if (Build.VERSION.SDK_INT >= 17 && Build.VERSION.SDK_INT < 19)
//        {
//            MediaMetadataRetriever meta = new MediaMetadataRetriever();
//            try{meta.setDataSource(videoObj.getVideoUrl(), new HashMap<String, String>());}
//            catch(Throwable err){try{meta.setDataSource(videoObj.getVideoUrl());}catch (Throwable err1){return;}}
//            rotation = Integer.parseInt(meta.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));
//        }
//    }
//
//    public void getVideoHome()
//    {
//        App.getUrlService(this).getVideoPreviewHome(videoID, new Callback<CAAVideoHome>()
//                {
//                    @Override
//                    public void success(CAAVideoHome caaVideoHome, Response response) {
//                        if (!isFinishing() || !mInBackground) {
//                            CAAVideo video = caaVideoHome.getRequesterVideo();
//                            videoObj = video;
//                            videoType = video.getType();
//                            userID = caaVideoHome.getRequesterVideo().getUser().getUserId();
//                            mMediaType = video.getMediaType();
//
//                            setRotation();
//                            adjustTextureViewSize();
//
//                            userName =  caaVideoHome.getRequesterVideo().getUser().getFullName();
//                            mToolbar.setTitle(userName);
//
//                            if (!video.getUser().getUserId().equalsIgnoreCase(CAAUserDataSingleton.getInstance().getUserId())) {
//                                if (video.getLikedByMe() == 1)
//                                    mLikeButton.setImageResource(R.drawable.liked);
//                                else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
//                                    mLikeButton.setImageResource(R.drawable.like);
//                            } else
//                                mLikeButton.setVisibility(View.GONE);
//
////                            mTotalLikesView.setText(String.valueOf(video.getTotalLikes()));
//
//                            videoStatus = video.getStatus();
//
//                            if (video.getMediaType().equalsIgnoreCase(Constants.PHOTO_TYPE))
//                                photoMode = true;
//
//                            displayVideoPhotoHome(video);
//
//                            mUserPic.setEnabled(true);
//                            mShareButton.setEnabled(true);
//                            mLikeButton.setEnabled(true);
//                            mMoreButton.setEnabled(true);
//                            mActivityButton.setEnabled(true);
//
//                            if(getIntent().hasExtra(Constants.IS_NOTIFICATION)) {
//                                getIntent().removeExtra(Constants.IS_NOTIFICATION);
//                                if(mTouchBlockerDialog != null) {
//                                    mTouchBlockerDialog.dismiss();
//                                    mTouchBlockerDialog = null;
//                                }
//                                mCommentCount.callOnClick();
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
////                        CAANetworkErrors networkErrors = new CAANetworkErrors();
////                        ChosenAlertDialog dialog = networkErrors.getDialog(VideoHomeActivity2.this, error.getResponse());
////                        if (dialog.shouldBeShown()) {
////                            dialog.show();
////                        }
//                    }
//                }
//
//        );
//    }
//
//    private void displayVideoPhotoHome(CAAVideo video) {
//        StringBuilder tagsBuilder = new StringBuilder();
//        tagsBuilder.append("#" + video.getSongName());
//        ArrayList<String> tags = video.getTags();
//        for (int i = 0; i < tags.size(); i++) {
//            tagsBuilder.append(" #" + tags.get(i));
//        }
//        mTagsText.setText(tagsBuilder);
//
//        videoUri = video.getUrl();
//        photo = mMediaType.equalsIgnoreCase("photo") ? video.getUrl() : video.getFirstFrame();
//
//        mBluredThumbnail.setUrl(photo);
//        mBluredThumbnail.setDontSaveToFileCache(true);
//        mBluredThumbnail.setDontUseExisting(true);
//        mBluredThumbnail.setFitToParent();
//        mBluredThumbnail.setBlurRadius(25);
//        mBluredThumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        mBluredThumbnail.setMaxWidthInternal(App.WIDTH / 2);
//        mBluredThumbnail.setAnimResID(R.anim.fade_in_slow);
//        if(Build.VERSION.SDK_INT >= 21)
//            mBluredThumbnail.setAspectRatio((float)(App.WIDTH) / App.HEIGHT);
//        mBluredThumbnail.setImageLoader(mImageLoaderManager);
//        mBluredThumbnail.setImageLoaderListener(new ImageLoaderListener() {
//            @Override
//            public void onImageLoaded(URLImageView urlImageView) {
//                urlImageView.invalidate();
//                mBluredThumbnail.setAnimResID(URLImageView.NOT_SPECIFIED);
//            }
//        });
//        mImageLoaderManager.DisplayImage(mBluredThumbnail);
//
//        mPreviewThumbnail.setTag(mVideoTextureView);
//        mVideoTextureView.setTag(!photoMode);
//
//        mPreviewThumbnail.setUrl(photo);
//        mPreviewThumbnail.setDontSaveToFileCache(false);
//        mPreviewThumbnail.setDontUseExisting(true);
//        mPreviewThumbnail.setWidthInternal(App.WIDTH);
//        mPreviewThumbnail.setMaxWidthInternal(App.WIDTH);
//        mPreviewThumbnail.setLayoutWidthHeight(App.WIDTH, ViewGroup.LayoutParams.WRAP_CONTENT);
//        mPreviewThumbnail.setKeepAspectRatioAccordingToWidth(true);
//        mPreviewThumbnail.setImageLoader(mImageLoaderManager);
//        mPreviewThumbnail.setImageLoaderListener(videoPreviewListener);
//
//        mImageLoaderManager.DisplayImage(mPreviewThumbnail);
//
//        if (userID.equalsIgnoreCase(Constants.ME) || userID.equalsIgnoreCase(CAAUserDataSingleton.getInstance().getUserId()))
//            mUserPic.setUrl(CAAUserDataSingleton.getInstance().getPhotoUrl());
//        else
//            mUserPic.setUrl(video.getUser().getPhoto());
//
//        mUserPic.setDontSaveToFileCache(false);
//        mUserPic.setIsRoundedImage(true);
//        mUserPic.setAnimResID(R.anim.fade_in);
//        mUserPic.setPlaceHolder(R.drawable.extra_large_userpic_placeholder);
//        mUserPic.setImageLoader(mImageLoaderManager);
//        mImageLoaderManager.DisplayImage(mUserPic);
//
//        mCommentUSerPic.setUrl(CAAUserDataSingleton.getInstance().getPhotoUrl());
//        mCommentUSerPic.setDontSaveToFileCache(false);
//        mCommentUSerPic.setIsRoundedImage(true);
//        mCommentUSerPic.setAnimResID(R.anim.fade_in);
//        mCommentUSerPic.setPlaceHolder(R.drawable.extra_large_userpic_placeholder);
//        mCommentUSerPic.setImageLoader(mImageLoaderManager);
//        mImageLoaderManager.DisplayImage(mCommentUSerPic);
//
//        if(photoMode)
//            mVideoTextureView.setVisibility(View.GONE);
//        else
//            prepareMediaPlayer();
//    }
//
//    private void prepareMediaPlayer() {
//        if(mMediaPlayer == null)
//            mMediaPlayer = new MediaPlayer();
//
//        if(mVideoTextureView == null)
//            mVideoTextureView = (TextureView)findViewById(R.id.texture_view_video);
//
//        if(mVideoTextureView.isAvailable())
//        {
//            mMediaPlayer.setSurface(new Surface(mVideoTextureView.getSurfaceTexture()));
//            setRotation();
//            adjustTextureViewSize();
//        }
//        else
//        {
//            attachSurface(false);
//        }
//
//        try
//        {
//            mMediaPlayer.setDataSource(videoUri);
//            mMediaPlayer.prepareAsync();
//        }
//        catch (Throwable err){}
//
//        mVideoTextureView.requestFocus();
//        mMediaPlayer.setOnInfoListener(this);
//        mMediaPlayer.setOnErrorListener(this);
//        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                adjustTextureViewSize();
//                if (!mInBackground)
//                    playVideo();
//            }
//        });
//    }
//
//    private void attachSurface(final boolean startPlayer) {
//        mVideoTextureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener()
//        {
//            @Override
//            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
//            {
//                mMediaPlayer.setSurface(new Surface(mVideoTextureView.getSurfaceTexture()));
//                setRotation();
//                adjustTextureViewSize();
//                if(startPlayer)
//                    resumeMediaPlayer();
//            }
//
//            @Override
//            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height)
//            {
//                adjustTextureViewSize();
//            }
//
//            @Override
//            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {return false;}
//
//            @Override
//            public void onSurfaceTextureUpdated(SurfaceTexture surface) {}
//        });
//    }
//
//    private void playVideo(){
//        resumeMediaPlayer();
//        if (!commentsLaunched) {
//            cancelCommentAniamtions();
//            mHandler.postDelayed(showComments, 3000);
//        }
//    }
//
//    public void renderVideoForInstagrm(){
//        RenderForInstagram renderForInstagram = new RenderForInstagram(mOriginalVideoPath, mShareDialog.getFileName(), mLogo1x1Path, mFrame1x1Path, videoObj.getClip(), videoObj.getMediaType().equalsIgnoreCase(Constants.PHOTO_TYPE), this, new onRenderInstagramFinished() {
//            @Override
//            public void onRenderInstagramFinished(boolean result) {
//                if(result) {
//                    mShareDialog.setFilePath(mShareDialog.getFileName());
//                    mShareDialog.onInstagramClicked();
//                }
//            }
//        });
//        renderForInstagram.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//    }
//
//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        orientation = newConfig.orientation;
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
//            showLandscapeView();
//        else
//            if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
//                showPortraitView();
//    }
//
//    private void showControls() {
//        isShowing = true;
//        getSupportActionBar().show();
//        mUserPic.setVisibility(View.VISIBLE);
//        mActionsContainer.setVisibility(View.VISIBLE);
//        mTagsText.setVisibility(View.VISIBLE);
//        mMyCommentContainer.setVisibility(View.VISIBLE);
//        displayComments();
//    }
//
//    private void hideControl() {
//        isShowing = false;
//        getSupportActionBar().hide();
//        mUserPic.setVisibility(View.GONE);
//        mActionsContainer.setVisibility(View.GONE);
//        mTagsText.setVisibility(View.GONE);
//        mMyCommentContainer.setVisibility(View.GONE);
//        cancelCommentAniamtions();
//    }
//
//    private void setToolbar() {
//        mToolbar = getActionBarToolbar();
//        if (mToolbar != null) {
//            mToolbar.setBackgroundResource(R.color.transparent);
//            mToolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
//            mToolbar.setNavigationContentDescription("backClose");
//            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    onBackPressed();
//                }
//            });
//
//            if (App.TOOLBAR_HEIGHT <= 0) {
//                ViewTreeObserver vto = mToolbar.getViewTreeObserver();
//                vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                    @Override
//                    public void onGlobalLayout() {
//                        App.TOOLBAR_HEIGHT = mToolbar.getMeasuredHeight();
//                        //Remove the listener once we got the height.
//                        if (Build.VERSION.SDK_INT < 16) {
//                            mToolbar.getViewTreeObserver().removeGlobalOnLayoutListener(this);
//                        } else {
//                            mToolbar.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                        }
//                    }
//                });
//            }
//        }
//    }
//
//    private void loadCommonOnClickEvents() {
//        mContainer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(isShowing) {
//                    hideControl();
//                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//                    myOrientationEventListener.enable();
//                }
//                else {
//                    showControls();
//                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                    myOrientationEventListener.disable();
//                }
//            }
//        });
//        mPlayPauseButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(mMediaPlayer != null)
//                    resumeMediaPlayer();
//                else
//                    prepareMediaPlayer();
//
//            }
//        });
//        mShareButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mShareDialog = new ShareDialogVideoHome(VideoHomeActivity2.this, VideoHomeActivity2.this, videoID, videoObj.getSongName(), userID, videoType, videoObj.getUser().getFullName(), photoMode, videoObj.getInstagramUrl(), isAfterRecording);
//                mShareDialog.show();
//                TrackingManager.getInstance().videoHomeTapShare(mMediaType);
//            }
//        });
//
//        mMoreButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //TODO add tracking.
//                if(userID != null) {
//                    PopupMenu popup = new PopupMenu(mWrapperContext, v);
//                    popup.getMenuInflater().inflate(R.menu.video_home_popup_menu, popup.getMenu());
//                    if (userID.equals(CAAUserDataSingleton.getInstance().getUserId()) || userID.equalsIgnoreCase(Constants.ME)) {
//                        if (videoStatus.equalsIgnoreCase("private"))
//                            popup.getMenu().findItem(R.id.action_private).setTitle("Make public").setVisible(true);
//                        else
//                            popup.getMenu().findItem(R.id.action_private).setTitle("Make private").setVisible(true);
//                        popup.getMenu().findItem(R.id.action_delete).setVisible(true);
//                    }
//                    popup.setOnMenuItemClickListener(
//                            new PopupMenu.OnMenuItemClickListener() {
//                                public boolean onMenuItemClick(MenuItem item) {
//                                    switch (item.getItemId()) {
//                                        case R.id.action_flag_video:
//                                            flagVideo();
//                                            return true;
//                                        case R.id.action_delete:
//                                            deleteVideo();
//                                            return true;
//                                        case R.id.action_private:
//                                            if (videoStatus.equalsIgnoreCase("public"))
//                                                uploadVideoStatus("private");
//                                            else
//                                                uploadVideoStatus("public");
//                                            return true;
//                                        default:
//                                            return true;
//                                    }
//                                }
//                            }
//
//                    );
//                    popup.show();
//                }
//            }
//        });
//
////        mActivityButton.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                if(videoID != null) {
////                    Intent i = new Intent(VideoHomeActivity2.this, VideoSocialDetailsActivity.class);
////                    i.putExtra(Constants.VIDEO_ID, videoID);
////                    i.putExtra(Constants.USER_NAME, userName);
////                    i.putExtra(Constants.MEDIA_TYPE, mMediaType);
////                    i.putExtra(Constants.SELECTED_TAB, VideoSocialDetailsActivity.ACTIVITY_POS);
////                    startActivity(i);
////                }
////            }
////        });
//
////        mTotalLikesView.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                Intent i = new Intent(VideoHomeActivity2.this, VideoSocialDetailsActivity.class);
////                i.putExtra(Constants.VIDEO_ID, videoID);
////                i.putExtra(Constants.USER_NAME, userName);
////                i.putExtra(Constants.MEDIA_TYPE, mMediaType);
////                i.putExtra(Constants.SELECTED_TAB, VideoSocialDetailsActivity.LIKES_POS);
////                startActivity(i);
////            }
////        });
//
//        mCommentCount.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(VideoHomeActivity2.this, VideoSocialDetailsActivity.class);
//                i.putExtra(Constants.VIDEO_ID, videoID);
//                i.putExtra(Constants.USER_NAME, userName);
//                i.putExtra(Constants.MEDIA_TYPE, mMediaType);
//                i.putExtra(Constants.SELECTED_TAB, VideoSocialDetailsActivity.COMMENTS_POS);
//                startActivity(i);
//            }
//        });
//
//        mUserPic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (userID != null && !(CAALogin.userIsAnonymous(VideoHomeActivity2.this) && userID.equals(CAAUserDataSingleton.getInstance().getUserId()))) {
//                    Intent i = new Intent(VideoHomeActivity2.this, ProfileActivity.class);
//                    i.putExtra(Constants.USER_ID, userID);
//                    startActivity(i);
//                    TrackingManager.getInstance().videoHomeTapProfile(videoObj.getMediaType());
//                }
//            }
//        });
//
//
//        mSaySomething.setOnTouchListener(new View.OnTouchListener()
//        {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent)
//            {
//                switch (motionEvent.getAction())
//                {
//                    case MotionEvent.ACTION_DOWN:
//                        if (CAALogin.userIsAnonymous(VideoHomeActivity2.this))
//                        {
//                            Intent i = new Intent(VideoHomeActivity2.this, SignupActivity.class);
//                            VideoHomeActivity2.this.startActivity(i);
//                        }
//                        else
//                        {
//                            pauseMediaPlayer();
//                            CommentBoxDialog commentBox = CommentBoxDialog.newInstance(videoID, new CommentBoxDialog.CommentBoxListener()
//                            {
//                                @Override
//                                public void onFinishCommentVideoDialog(boolean success, String commentId, String text)
//                                {
//                                    if (success)
//                                    {
//                                        addMyComment(text);
//                                        incrementCommentNumber();
//                                    }
//                                    resumeMediaPlayer();
//                                }
//                            }, Constants.VIDEO_HOME_ACTIVITY);
//                            commentBox.show(VideoHomeActivity2.this.getFragmentManager(), "show_comment_box");
//                        }
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        break;
//
//                }
//                return false;
//            }
//        });
//
//        videoPreviewListener = new ImageLoaderListener()
//        {
//            @Override
//            public void onImageLoaded(URLImageView urlImageView)
//            {
//                boolean isVideo = urlImageView.getTag() != null && (boolean) ((TextureView) urlImageView.getTag()).getTag();
//
//                if (urlImageView.getImageWidth() > urlImageView.getImageHeight() || urlImageView.getImageHeight() + App.TOOLBAR_HEIGHT < App.HEIGHT)
//                {
//                    hasMargins = true;
//                    textureMargin = App.TOOLBAR_HEIGHT + (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP  ? App.STATUS_BAR_HEIGHT : 0);
//                    ((FrameLayout.LayoutParams) urlImageView.getLayoutParams()).setMargins(0,textureMargin , 0, 0);
//                    ((FrameLayout.LayoutParams) urlImageView.getLayoutParams()).gravity = Gravity.TOP;
//                    if (isVideo) {
//                        ((FrameLayout.LayoutParams) ((TextureView) urlImageView.getTag()).getLayoutParams()).setMargins(0, textureMargin, 0, 0);
//                    }
//                }
//                else
//                {
//                    ((FrameLayout.LayoutParams) urlImageView.getLayoutParams()).setMargins(0, 0, 0, 0);
//                    ((FrameLayout.LayoutParams) urlImageView.getLayoutParams()).gravity = Gravity.TOP;
//                    urlImageView.requestLayout();
//                    if (isVideo)
//                        ((FrameLayout.LayoutParams) ((TextureView) urlImageView.getTag()).getLayoutParams()).setMargins(0, 0, 0, 0);
//
//                }
//
//                if (isVideo)
//                {
//                    ((TextureView) urlImageView.getTag()).getLayoutParams().height = ((TextureView) urlImageView.getTag()).getLayoutParams().width = FrameLayout.LayoutParams.WRAP_CONTENT;
//                    ((TextureView) urlImageView.getTag()).requestLayout();
//                    ((TextureView) urlImageView.getTag()).setVisibility(View.VISIBLE);
//                }
//                else
//                {
//                    if(urlImageView.getTag() != null)
//                        ((TextureView) urlImageView.getTag()).setVisibility(View.GONE);
//                    mProgressBar.setVisibility(View.GONE);
//                }
//                urlImageView.postInvalidate();
//                if(urlImageView.getTag() != null)
//                    ((TextureView) urlImageView.getTag()).setTag(null);
//                urlImageView.setTag(null);
//            }
//        };
//
//
//
//        mLikeButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(CAALogin.userIsAnonymous(getApplicationContext()))
//                    wentToSignUp = true;
//
//                if (videoObj.getLikedByMe() == 1) {
//                    likeVideo();
//                    mLikeButton.setImageResource(R.drawable.liked);
//                    CAAUserDataSingleton.getInstance().getLikedList().add((Integer.valueOf(videoObj.getVideoId())));
//                    TrackingManager.getInstance().tapVideoHomeLikeButton(videoObj.getVideoId(), videoObj.getMediaType(), FlurryConstants.UNLIKE);
//                } else {
//                    unlikeVideo();
//                    mLikeButton.setImageResource(R.drawable.like);
//                    CAAUserDataSingleton.getInstance().getLikedList().remove((Integer) (Integer.valueOf(videoObj.getVideoId())));
//                    TrackingManager.getInstance().tapVideoHomeLikeButton(videoObj.getVideoId(), videoObj.getMediaType(), FlurryConstants.LIKE);
//                }
//            }
//        });
//    }
//
//    private void likeVideo(){
//        CAAType type = new CAAType("video_home");
//        App.getUrlService(this).postLikeVideo(videoObj.getVideoId(), type, new Callback<CAALike>() {
//            @Override
//            public void success(CAALike like, Response response) {
//                // liked
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                mLikeButton.setImageResource(R.drawable.like);
//                videoObj.setLikedByMe(0);
//                videoObj.setTotalLikes(videoObj.getTotalLikes() - 1);
////                CAANetworkErrors networkErrors = new CAANetworkErrors();
////                ChosenAlertDialog dialog = networkErrors.getDialog(activity, error.getResponse());
////                if (dialog.shouldBeShown()) {
////                    dialog.show();
////                }
//            }
//        });
//    }
//
//    private void unlikeVideo(){
//        App.getUrlService(this).deleteLikeVideo(videoObj.getVideoId(), new Callback<String>() {
//            @Override
//            public void success(String s, Response response) {
//                //unlike
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                videoObj.setLikedByMe(1);
//                videoObj.setTotalLikes(videoObj.getTotalLikes() + 1);
////                CAANetworkErrors networkErrors = new CAANetworkErrors();
////                ChosenAlertDialog dialog = networkErrors.getDialog(activity, error.getResponse());
////                if (dialog.shouldBeShown()) {
////                    dialog.show();
////                }
//            }
//        });
//    }
//
//
//    private void uploadVideoStatus(final String status) {
//
////        if (ConnectivityChecker.isOnline(this)) {
//            if (!CAAUserDataSingleton.getInstance().getAccountType().equalsIgnoreCase("anonymous")) {
//                App.getUrlService(this).putUploadVideoStatus(videoID, new CAAStatus(status), new Callback<CAAVideo>() {
//                    @Override
//                    public void success(CAAVideo caaVideo, Response response) {
//                        videoStatus = caaVideo.getStatus();
//
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
////                        CAANetworkErrors networkErrors = new CAANetworkErrors();
////                        ChosenAlertDialog dialog = networkErrors.getDialog(VideoHomeActivity2.this, error.getResponse());
////                        if (dialog.shouldBeShown()) {
////                            dialog.show();
////                        }
//                    }
//                });
//            } else {
//                Intent i = new Intent(VideoHomeActivity2.this, SignupActivity.class);
//                startActivity(i);
//            }
////        } else {
////            showNoConnectionDialog();
////        }
//    }
//
//    private void getComments()
//    {
//        if (videoID != null && !videoID.equalsIgnoreCase(""))
//        {
//            App.getUrlService(this).getVideoComments(videoID, 0, new Callback<CAACommentsResponse>()
//            {
//                @Override
//                public void success(CAACommentsResponse caaCommentsResponse, Response response)
//                {
//                    if (response.getStatus() != HttpURLConnection.HTTP_NO_CONTENT)
//                    {
//                        mComments = caaCommentsResponse;
//                        prepareComments(caaCommentsResponse);
//                        if (timeToShowComments)
//                        {
//                            runOnUiThread(showComments);
//                        }
//                    }
//                    else
//                    {
//                        videoHasNoComments = true;
//                    }
//                }
//
//                @Override
//                public void failure(RetrofitError error)
//                {
//                    videoHasNoComments = true;
//                }
//            });
//        }
//    }
//
//    private void prepareComments(CAACommentsResponse caaCommentsResponse)
//    {
//        int numComments = Integer.valueOf(caaCommentsResponse.getTotalComments());
//        if (caaCommentsResponse == null || caaCommentsResponse.getTotalComments() == null || numComments == 0)
//        {
//            mCommentCount.setText("");
//        }
//        else
//        {
//            mCommentCount.setText(caaCommentsResponse.getTotalComments());
//        }
//
//        if (numComments >= 1)
//        {
//            for (int i = 0; i < mComments.getData().size(); i++)
//            {
//                if (mComments.getData().get(i).getUser().getUserId().equalsIgnoreCase(CAAUserDataSingleton.getInstance().getUserId()))
//                {
//                    mComments.getData().get(i).getUser().setLastName("");
//                    mComments.getData().get(i).getUser().setFirstName("You!");
//                }
//            }
//            videoHasNoComments = false;
//        }
//        else
//        {
//            videoHasNoComments = true;
//        }
//    }
//
//    public void displayComments()
//    {
//        if(!isActivityVisible)
//            return;
//
//        if (mTimer != null)
//        {
//            mTimer.cancel();
//            mTimer.purge();
//        }
//        if (!mLongCommentDelay)
//            commentIndex.set(0);
//        else
//            mLongCommentDelay = false;
//
//        if (myComment != null)
//        {
//            mComments.getData().add(0, myComment);
//            myComment = null;
//        }
//
//        mTimer = new Timer();
//        mTimer.scheduleAtFixedRate(new TimerTask()
//        {
//            @Override
//            public void run()
//            {
//                try
//                {
//                    if (mComments.getData() != null)
//                    {
//                        if (commentIndex.get() < mComments.getData().size())
//                        {
//                            VideoHomeActivity2.this.runOnUiThread(new Runnable()
//                            {
//                                @Override
//                                public void run()
//                                {
//                                    mCommentsContainer.setVisibility(View.VISIBLE);
//                                    if (mComments != null && mComments.getData().get(commentIndex.get()) != null)
//                                    {
//                                        addCommentView(commentIndex.get());
//                                    }
//                                    else
//                                    {
//                                        mTimer.cancel();
//                                        mTimer.purge();
//                                    }
//                                }
//                            });
//                        }
//                        else
//                        {
//                            VideoHomeActivity2.this.runOnUiThread(new Runnable()
//                            {
//                                @Override
//                                public void run()
//                                {
//                                    mTimer.cancel();
//                                    mTimer.purge();
//                                }
//                            });
//                        }
//                    }
//                }
//                catch (Throwable ex)
//                {
//                    //Happens when the activity is already destroyed.
//                    //Do nothing.
//                }
//            }
//        }, 0, 2000);
//
//    }
//
//    Runnable showComments = new Runnable()
//    {
//        @Override
//        public void run()
//        {
//            if (mComments != null)
//            {
//                commentsLaunched = true;
//                timeToShowComments = false;
//                displayComments();
//            }
//            else
//            {
//                timeToShowComments = true;
//            }
//        }
//    };
//
//    private void addCommentView(int index)
//    {
//        try
//        {
//            if (mComments != null && mComments.getData() != null && mComments.getData().size() > index && mComments.getData().get(index) != null)
//            {
//                if (mLongCommentDelay)
//                {
//                    mTimer.cancel();
//                    mTimer.purge();
//                    displayComments();
//                    return;
//                }
//                View iCommentView = mInflater.inflate(R.layout.swipe_game_card_comment, null);
//                CAACommentParams iCommentParams = mComments.getData().get(index);
//
//                URLImageView userPic = (URLImageView) iCommentView.findViewById(R.id.comment_pic);
//                userPic.setDontSaveToFileCache(true);
//                userPic.setIsRoundedImage(true);
//                userPic.setPlaceHolder(R.drawable.extra_large_userpic_placeholder);
//                userPic.setUrl(iCommentParams.getUser().getPhoto());
//                userPic.setAnimResID(R.anim.fade_in);
//                mImageLoaderManager.DisplayImage(userPic);
//
//
//                TextView userComment = (TextView) iCommentView.findViewById(R.id.comment_text);
//                userComment.setText(iCommentParams.getText());
//
//                TextView userName = (TextView) iCommentView.findViewById(R.id.comment_name);
//                userName.setText(iCommentParams.getUser().getFullName());
//
//                if (userComment.getText().length() >= 40)
//                {
//                    mLongCommentDelay = true;
//                }
//
//                if (iCommentView.getLayoutParams() == null)
//                    iCommentView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//                else
//                {
//                    iCommentView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
//                    iCommentView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
//                }
//                Animation scroll = AnimationUtils.loadAnimation(VideoHomeActivity2.this, R.anim.scroll_up);
//
//                mCommentsContainer.addView(iCommentView);
//                iCommentView.setVisibility(View.VISIBLE);
//                iCommentView.startAnimation(scroll);
//                commentIndex.incrementAndGet();
//            }
//            else
//            {
//                mTimer.cancel();
//                mTimer.purge();
//            }
//        }
//        catch (Throwable err)
//        {
//        }
//    }
//
//
//    public void addMyComment(String text)
//    {
//        //Build my comment
//        myComment = new CAACommentParams();
//        myComment.setText(text);
//        User myUser = new User();
//        myUser.setFirstName("You!");
//        myUser.setLastName("");
//        myUser.setPhoto(CAAUserDataSingleton.getInstance().getPhotoUrl());
//        myComment.setUser(myUser);
//
//        //Insert to start of array
//        if (mComments != null)
//        {
//            if (commentIndex.get() >= mComments.getData().size())
//            {
//                mCommentsContainer.setVisibility(View.INVISIBLE);
//                cancelCommentAniamtions();
//                mComments.setData(new ArrayList<CAACommentParams>());
//                mComments.getData().add(0, myComment);
//                myComment = null;
//                displayComments();
//            }
//            else
//            {
//                mComments.getData().add(commentIndex.get() + 1, myComment);
//                myComment = null;
//            }
//        }
//        else if (videoHasNoComments)
//        {
//            mComments = new CAACommentsResponse();
//            mComments.setData(new ArrayList<CAACommentParams>());
//            displayComments();
//        }
//    }
//
//    private void incrementCommentNumber()
//    {
//        if (mCommentCount.getText().toString().isEmpty())
//        {
//            mCommentCount.setText("1");
//        }
//        else
//        {
//            mCommentCount.setText(String.valueOf(Integer.valueOf(mCommentCount.getText().toString()) + 1));
//        }
//    }
//
//    private void cancelCommentAniamtions()
//    {
//        timeToShowComments = false;
//        if (mTimer != null)
//        {
//            mTimer.cancel();
//            mTimer.purge();
//        }
//        clearCommentViews();
//        if (showComments != null)
//            mHandler.removeCallbacks(showComments);
//    }
//
//    private void clearCommentViews()
//    {
//        if (mCommentsContainer != null)
//        {
//            for (int i = 0; i < mCommentsContainer.getChildCount(); i++)
//            {
//                if (mCommentsContainer.getChildAt(i) != null)
//                {
//                    View view = mCommentsContainer.getChildAt(i);
//                    view.getAnimation().cancel();
//                    view.clearAnimation();
//                    view.removeCallbacks(null);
//                    try {
//                        ((URLImageView) ((ViewGroup) view).getChildAt(0)).close();
//                    }catch (Throwable throwable){}
//                }
//            }
//            mCommentsContainer.removeAllViews();
//        }
//    }
//
//
//    @Override
//    public void onFinishFlagVideoDialog(boolean success) {
//        if (success) {
//            showSuccessAlertDialog();
//        } else {
//            if (isOnEventPaused) {
//                playVideo();
//            }
//        }
//    }
//
//    private void showFlagVideoDialog() {
//        FlagVideoDialog flagVideoDialog = new FlagVideoDialog(getApplicationContext(), videoID, this);
//        flagVideoDialog.show();
//    }
//
//    private void showSuccessAlertDialog() {
////        String yesText = getApplicationContext().getResources().getString(R.string.ok);
////        String title = "Chosen";
////        String text = "Thank you for the report";
////        final ChosenAlertDialog mDialog = ChosenAlertDialog.newInstance(this, title, text, null, yesText, false);
////        mDialog.show();
////        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
////            @Override
////            public void onDismiss(DialogInterface dialog) {
//                if (isOnEventPaused) {
//                    playVideo();
//                }
////
////            }
////        });
//    }
//
//    private void deleteVideo() {
//        if (NetworkConnectivityListener.isConnectedWithDialog(this)) {
//            BaseDialog deleteVideoDialog = new BaseDialog(this, "Are you sure?", "Are you sure you want to delete this " + mMediaType + "?");
//            deleteVideoDialog.setOKText("DELETE");
//            deleteVideoDialog.show();
//            deleteVideoDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                @Override
//                public void onDismiss(DialogInterface dialogInterface) {
//                    if (((BaseDialog)dialogInterface).getAnswer()) {
//                        App.getUrlService(VideoHomeActivity2.this).deleteVideo(videoID, new Callback<String>() {
//                            @Override
//                            public void success(String s, Response response) {
//                                onBackPressed();
//                            }
//
//                            @Override
//                            public void failure(RetrofitError error) {
//                            }
//                        });
//                    }
//                }
//            });
//
//        }
//    }
//
//    private void flagVideo() {
//        if (NetworkConnectivityListener.isConnectedWithDialog(this)) {
//            if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
//                pauseMediaPlayer();
//            } else {
//                isOnEventPaused = false;
//            }
//            showFlagVideoDialog();
//        }
////        else {
////            showNoConnectionDialog();
////        }
//    }
//
//    private void showPortraitView() {
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if(mMediaType.equalsIgnoreCase("video")) {
//                    adjustTextureViewSize();
//                    if(hasMargins) {
//                        ((FrameLayout.LayoutParams) (mVideoTextureView).getLayoutParams()).setMargins(0, textureMargin, 0, 0);
//                        mVideoTextureView.requestLayout();
//                    }
//                }
//                else {
//                    adjustPreviewPicture();
//                }
//            }
//        },250);
//    }
//
//    private void showLandscapeView() {
//        if(mMediaType.equalsIgnoreCase("video")) {
//            mVideoTextureView.setVisibility(View.INVISIBLE);
//            if(isVideoFinished)
//                mPlayPauseButton.setVisibility(View.INVISIBLE);
//        }
//        else
//            mPreviewThumbnail.setVisibility(View.INVISIBLE);
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if(mMediaType.equalsIgnoreCase("video")) {
//                    ((FrameLayout.LayoutParams) (mVideoTextureView).getLayoutParams()).setMargins(0, 0, 0, 0);
//                    mVideoTextureView.requestLayout();
//                    adjustTextureViewSize();
//                    mVideoTextureView.setVisibility(View.VISIBLE);
//                    if(isVideoFinished)
//                        mPlayPauseButton.setVisibility(View.VISIBLE);
//
//                }
//                else {
//                    adjustPreviewPicture();
//                    mPreviewThumbnail.setVisibility(View.VISIBLE);
//                }
//            }
//        },350);
//    }
//
//    @Override
//    public void onCompletion(MediaPlayer mp) {
//        isVideoFinished = true;
//        if(mp.getDuration() > 0 )
//            mPlayPauseButton.setVisibility(View.VISIBLE);
//    }
//
//    @Override
//    public boolean onError(MediaPlayer mp, int what, int extra) {
//        releaseMediaPlayer();
//        if(mPreviewThumbnail != null)
//            mPreviewThumbnail.setVisibility(View.VISIBLE);
//        return false;
//    }
//
//    @Override
//    public boolean onInfo(MediaPlayer mp, int what, int extra)
//    {
//        mMediaPlayer.setOnCompletionListener(this);
//        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START)
//        {
//            mp.setOnInfoListener(null);
//            if (mPreviewThumbnail != null)
//            {
//                mPreviewThumbnail.setVisibility(View.GONE);
//                adjustTextureViewSize();
//            }
//            mProgressBar.setVisibility(View.GONE);
//        }
//
//        return false;
//    }
//
//}
