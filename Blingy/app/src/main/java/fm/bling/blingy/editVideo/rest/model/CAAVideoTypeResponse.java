package fm.bling.blingy.editVideo.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


/**
 * Created by Zach on 2/9/16.
 */
public class CAAVideoTypeResponse {

    @SerializedName("data")
    private ArrayList<CAAVideoTypeData> data;

    public ArrayList<CAAVideoTypeData> getData() {
        return data;
    }

//    public void setData(ArrayList<CAAVideoTypeData> data) {
//        this.data = data;
//    }
}
