package fm.bling.blingy.duetHome.model;


import com.google.gson.annotations.SerializedName;

public class DuetPermission {

    @SerializedName("duet_permission")
    private boolean hasPermission;

    public boolean hasPermission() {
        return hasPermission;
    }
}
