package fm.bling.blingy.mashup.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import fm.bling.blingy.R;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 19/03/17.
 * History:
 * ***********************************
 */
public class CameoDialog extends Dialog implements View.OnClickListener{

    /**
     * Views
     */
    private ConstraintLayout mMainLayout;


    private Context mContext;
    private View.OnClickListener mImageClickListener;
    private boolean dismissed;

    public CameoDialog(Context context, View.OnClickListener imageClickListener) {
        super(context, android.R.style.Theme_Panel);
        setContentView(R.layout.cameo_dialog);
        this.mContext = context;
        this.mImageClickListener = imageClickListener;
        this.findViewById(R.id.close_button).setOnClickListener(this);
        init();
    }

    private void init(){
        this.mMainLayout = (ConstraintLayout) findViewById(R.id.main_container);
        findViewById(R.id.cameo_image).setOnClickListener(mImageClickListener);
    }

    @Override
    public void show() {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        Animation slideIn = AnimationUtils.loadAnimation(mContext, R.anim.slide_up);
        slideIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mMainLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        super.show();
        mMainLayout.startAnimation(slideIn);
    }

    public void dismiss() {
        if (dismissed)
            return;
        dismissed = true;
        Animation slideOut = AnimationUtils.loadAnimation(mContext, R.anim.slide_down);
        slideOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mMainLayout.setVisibility(View.INVISIBLE);
                superDismiss();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        mMainLayout.startAnimation(slideOut);
    }

    public boolean onTouchEvent(MotionEvent me) {
        if (!dismissed) {
            if (me.getAction() == MotionEvent.ACTION_DOWN) {
                if (me.getY() < mMainLayout.getTop())
                {
                    dismiss();
                }
            }
        }
        return true;
    }

    private void superDismiss() {
        super.dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
        default :
        case R.id.close_button :
            dismiss();
            break;
        case R.id.cameo_image :
            mImageClickListener.onClick(v);
            break;
        }
    }
}
