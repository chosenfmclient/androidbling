package fm.bling.blingy.dialogs.share;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 1/5/17.
 * History:
 * ***********************************
 */
public interface ShareDialogVisibilityCallback extends ShareDialogCallbackSetter {

    void onShareDialogShow();

    void onShareDialogDismissed();
}
