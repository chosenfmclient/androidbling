package fm.bling.blingy.record.audio;

import android.content.Context;
import android.media.AudioTrack;
import android.os.SystemClock;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import fm.bling.blingy.utils.FFMPEG;
import fm.bling.blingy.utils.Tic;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 29/06/2016.
 * History:
 * ***********************************
 */
public class AudioPlayer
{
    public final byte SPEED_NORMAL = 0, SPEED_FAST = 1, SPEED_SLOW = 2;
    private AudioTrack audioTrack;
    private InputStream inputStream;
    public boolean isPlaying;
    private OnCompleteListener mOnCompleteListener;
    private Thread currentThread;

    private boolean isFirst = true, audioStarted = false;
    private long startFrameTimeStamp, endFrameTimeStamp;

    private ProcessData mProcessData;
    private ArrayList<AudioPlayerFilter>filters;
//    private boolean isFilterSet;

    private String mWavFileName;
    private String mWavFileNameNormal, mWavFileNameFast, mWavFileNameSlow;
    private Context mContext;

    public AudioPlayer(Context context, String wavFileNameNormal, String wavFileNameFast, String wavFileNameSlow)
    {
        mWavFileName = mWavFileNameNormal = wavFileNameNormal;
        mWavFileNameFast = wavFileNameFast;
        mWavFileNameSlow = wavFileNameSlow;
        mContext = context;
    }

    public void setOnCompleteListener(OnCompleteListener onCompleteListener)
    {
        mOnCompleteListener = onCompleteListener;
    }

//    public void resume() throws Throwable
//    {
//        if(audioTrack == null)
//        {
//            audioTrack = FFMPEG.getInstance(mContext).getAudioTrack(inputStream = new FileInputStream(mWavFileName));
//            mProcessData = new ProcessData(FFMPEG.getInstance(mContext).bufferSize);
//        }
//
//        isPlaying = true;
//        currentThread = new Thread()
//        {
//            public void run()
//            {
//                if(!audioStarted)
//                {
//                    audioStarted = true;
//                    try
//                    {
//                        inputStream.read(mProcessData.originData);
//                    }catch (Throwable err){}
//                }
//
//                startFrameTimeStamp = SystemClock.elapsedRealtimeNanos();
//                audioTrack.play();
//
//                try
//                {
//                    while (isPlaying)
//                    {
//                        mProcessData.originEndOffset = inputStream.read(mProcessData.originData);
//
//                        if (mProcessData.originEndOffset == -1)
//                            break;
//
////                        if(isFilterSet)
////                        {
////                            for (AudioPlayerFilter filter : filters)
////                            {
////                                filter.onProcessing(mProcessData);
////                                mProcessData.setBuffers();
////                            }
////                        }
//
//                        audioTrack.write(mProcessData.originData, 0, mProcessData.originEndOffset);
//                        audioTrack.flush();
//
//                        if(isFirst)
//                        {
//                            isFirst = false;
//                            startFrameTimeStamp += ((SystemClock.elapsedRealtimeNanos() - startFrameTimeStamp)/2L);//+ 800000000L;
//                        }
//                    }
//                    if (isPlaying)
//                        mOnCompleteListener.onComplete();
//
//                }
//                catch (Throwable err)
//                {
//                    err.printStackTrace();
//                }
//                endFrameTimeStamp = SystemClock.elapsedRealtimeNanos();
//            }
//        };
//        currentThread.start();
//    }
//
//    public void pause()
//    {
//        isPlaying = false;
//        if(audioTrack != null)
//            audioTrack.pause();
//        try
//        {
//            currentThread.join();
//        }
//        catch (Throwable e)
//        {
//            e.printStackTrace();
//        }
//    }

//    public void stop()
//    {
//        isPlaying = false;
//        try
//        {
//            audioTrack.stop();
//            audioTrack.release();
//        }catch (Throwable err){}
//        try{inputStream.close();}catch (Throwable err){}
//    }

    private int count;
    public void resume() throws Throwable
    {
//        if(audioTrack == null)
        {
            audioTrack = FFMPEG.getInstance(mContext).getAudioTrack(inputStream = new FileInputStream(mWavFileName));
            mProcessData = new ProcessData(FFMPEG.getInstance(mContext).bufferSize);
        }


        currentThread = new Thread()
        {
            public void run()
            {
//                isPlaying = true;
//                isFirst =  true;
                try
                {
                    if(!audioStarted)
                    {
                        audioStarted = true;
                        try
                        {
                            count = inputStream.read(mProcessData.originData);
                        }catch (Throwable err){}
                    }
                    else inputStream.skip(count);

                    startFrameTimeStamp = SystemClock.elapsedRealtimeNanos();
                    audioTrack.play();
                    isPlaying = true;
                    isFirst =  true;

                    while (isPlaying)
                    {
                        mProcessData.originEndOffset = inputStream.read(mProcessData.originData);

                        if (mProcessData.originEndOffset == -1)
                            break;

//                        if(isFilterSet)
//                        {
//                            for (AudioPlayerFilter filter : filters)
//                            {
//                                filter.onProcessing(mProcessData);
//                                mProcessData.setBuffers();
//                            }
//                        }

                        if(isFirst)
                        {
                            isFirst = false;
                            startFrameTimeStamp = SystemClock.elapsedRealtimeNanos();//((SystemClock.elapsedRealtimeNanos() - startFrameTimeStamp)/2L);//+ 800000000L;
                        }

                        Tic.tic();
                        count += audioTrack.write(mProcessData.originData, 0, mProcessData.originEndOffset);
                        Tic.tac();
                        audioTrack.flush();


//                        if(isFirst)
//                        {
//                            isFirst = false;
//                            startFrameTimeStamp += ((SystemClock.elapsedRealtimeNanos() - startFrameTimeStamp)/2L);//+ 800000000L;
//                        }
                    }
                    if (isPlaying)
                        mOnCompleteListener.onComplete();
                    audioTrack.stop();
                    audioTrack.release();
                }
                catch (Throwable err)
                {
                    err.printStackTrace();
                }
                endFrameTimeStamp = SystemClock.elapsedRealtimeNanos();
            }
        };
        currentThread.start();
    }

    public void pause()
    {
        isPlaying = false;
        try{audioTrack.pause();}
        catch (Throwable err){}
        try
        {
            currentThread.join();
        }
        catch (Throwable e)
        {
            e.printStackTrace();
        }
    }

    public void stop()
    {
        isPlaying = false;
//        audioTrack.stop();
//        audioTrack.release();
        try{inputStream.close();}catch (Throwable err){}
    }

    public void close()
    {
        isPlaying = false;
        try
        {
            audioTrack.stop();
        }
        catch (Throwable err)
        {
        }
        try
        {
            audioTrack.release();
        }
        catch (Throwable err)
        {
        }
        try
        {
            inputStream.close();
        }
        catch (Throwable err)
        {
        }
        try
        {
            if (filters != null)
                for (AudioPlayerFilter filter : filters)
                    try
                    {
                        filter.close();
                    }
                    catch (Throwable err)
                    {
                    }
        }
        catch (Throwable err)
        {
        }
        filters = null;
        mProcessData = null;
    }


    private byte mSpeed = SPEED_NORMAL;
    public void setSpeed(byte speed)
    {
        switch (mSpeed = speed)
        {
            case SPEED_FAST:
                mWavFileName = mWavFileNameFast;
                break;
            case SPEED_SLOW:
                mWavFileName = mWavFileNameSlow;
                break;
            default:
                mWavFileName = mWavFileNameNormal;
                break;
        }
    }

//    public void addFilter(AudioPlayerFilter filter)
//    {
//        if(filters == null)
//            filters = new ArrayList<>();
//        isFilterSet = true;
//        filters.add(filter);
//    }
//
//    public void addFilter(AudioPlayerFilter filter, int index)
//    {
//        if(filters == null)
//            filters = new ArrayList<>();
//        isFilterSet = true;
//        filters.add(index, filter);
//    }
//
//    public void removeFilter(AudioPlayerFilter filter)
//    {
//        try{ filter.close(); }catch (Throwable err){}
//        if(filters == null)     return;
//        filters.remove(filter);
//        if(filters.size() == 0)
//        {
//            isFilterSet = false;
//            filters = null;
//        }
//    }

//    public void removeFilterType(Class filterClassType)
//    {
//        if(filters == null)     return;
//        for(int i = filters.size() - 1; i > -1; i--)
//        {
//            if( filterClassType.isAssignableFrom(filters.get(i).getClass()) )
//                try{filters.remove(i).close();}catch (Throwable err){}
//        }
//    }

    public long getEndFrameTimeStamp()
    {
        return endFrameTimeStamp;
    }

    public long getStartFrameTimeStamp()
    {
        return startFrameTimeStamp;
    }

    public void setTracks(String wavFileNameNormal, String wavFileNameFast, String wavFileNameSlow)
    {
        mWavFileName = mWavFileNameNormal = wavFileNameNormal;
        mWavFileNameFast = wavFileNameFast;
        mWavFileNameSlow = wavFileNameSlow;

        setSpeed(mSpeed);
    }

    public interface OnCompleteListener
    {
        void onComplete();
    }

    public interface AudioPlayerFilter
    {
        void onProcessing(ProcessData processData);

        void close();
    }

    public class ProcessData
    {
        public byte[] originData;
        public int originEndOffset;//originStartOffset,

        public byte[] editedData;
        public int editedEndOffset;//editedStartOffset,

        public ProcessData(int bufferSize)
        {
            originData = new byte[bufferSize];
            editedData = new byte[bufferSize];
        }

        public void setBuffers()
        {
            originData = editedData;
            originEndOffset = editedEndOffset;
        }

    }

}