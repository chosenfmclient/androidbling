package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 31/01/17.
 * History:
 * ***********************************
 */
public class UserInvited {

    @SerializedName("has_invited")
    private boolean hasInvited;

    @SerializedName("user")
    private User user;

    public boolean getHasInvited() {
        return hasInvited;
    }

    public User getUser() {
        return user;
    }
}
