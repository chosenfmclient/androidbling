package fm.bling.blingy.leaderboard.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.base.interfaces.BaseRecyclerAdapter;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.utils.imagesManagement.MultiImageLoader;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 02/03/17.
 * History:
 * ***********************************
 */
public class LeaderboardRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements BaseRecyclerAdapter<MultiImageLoader>{

    private ArrayList<User> mDataset;
    private MultiImageLoader imageLoaderManager;
    private View.OnClickListener mItemClickListener;

    private final int TYPE_USER = 0;
    private final int TYPE_LOADING = -1;
    private Animation anim;
    private final int size_20dp = Utils.getDimensionPixelSize(R.dimen.size_20dp);
    private final int size_30dp = Utils.getDimensionPixelSize(R.dimen.size_30dp);

    public LeaderboardRecyclerAdapter(ArrayList<User> myDataset, View.OnClickListener itemClickListener) {
        mDataset = myDataset;
        mItemClickListener = itemClickListener;
        anim = AnimationUtils.loadAnimation(App.getAppContext(), R.anim.spinner_rotate);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case TYPE_USER:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_type_item, parent, false);
                holder = new DataObjectHolder(view);
                break;
            case TYPE_LOADING:
                View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_list_layout, parent, false);
                holder = new SpinnerDataObjectHolder(view1);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof DataObjectHolder) {
            User user = mDataset.get(position);
            DataObjectHolder objectHolder = ((DataObjectHolder)holder);

            int likesLength = user.getLikes().length();
            if(likesLength == 1 || likesLength == 2)
                objectHolder.mRank.setWidth(size_20dp);
            else
                objectHolder.mRank.setWidth(size_30dp);

            objectHolder.mItem.setTag(user);
            objectHolder.mRank.setText(user.getRank());
            objectHolder.mUserName.setText(user.getFullName());
            objectHolder.mText.setText(getLikesText(user.getLikes()));

            objectHolder.mUserPic.setId(position);
            objectHolder.mUserPic.setUrl(user.getPhoto());
            objectHolder.mUserPic.setKeepAspectRatioAccordingToWidth(true);
            objectHolder.mUserPic.setIsRoundedImage(true);
            objectHolder.mUserPic.setImageLoader(imageLoaderManager);
        }
        else{
            ((SpinnerDataObjectHolder)holder).mSpinnerView.setAnimation(anim);
        }
    }

    private String getLikesText(String likes) {
        String suffix = " likes yesterday";
        switch (likes.length()){
            case 4:
                if(likes.charAt(1) == '0')
                    return likes.charAt(0) + "k" + suffix;
                else
                    return likes.charAt(0) +"." + likes.charAt(1) + "k" + suffix;
            case 5:
                if(likes.charAt(2) == '0')
                    return likes.substring(0,2) + "k" + suffix;
                else
                    return likes.substring(0,2) +"." + likes.charAt(2) + "k" + suffix;
            case 6:
                if(likes.charAt(3) == '0')
                    return likes.substring(0,3) + "k" + suffix;
                else
                    return likes.substring(0,3) +"." + likes.charAt(3) + "k" + suffix;
            case 7:
                if(likes.charAt(1) == '0')
                    return likes.charAt(0) + "M" + suffix;
                else
                    return likes.charAt(0) +"." + likes.charAt(1) + "M" + suffix;
            default:
                return likes + suffix;

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mDataset.get(position).getFirstName() == null) {
            return TYPE_LOADING;
        } else {
            return TYPE_USER;
        }
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    @Override
    public void setImageLoaderManager(MultiImageLoader imageloader) {
        this.imageLoaderManager = imageloader;
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        private LinearLayout mItem;
        private ScaledImageView mUserPic;
        private TextView mUserName;
        private TextView mText;
        private TextView mRank;


        public DataObjectHolder(View itemView) {
            super(itemView);
            mItem = (LinearLayout)itemView.findViewById(R.id.user_type_item);

            mUserName = (TextView)itemView.findViewById(R.id.text_user_name);
            mText = (TextView)itemView.findViewById(R.id.text_comment);
            mRank = (TextView)itemView.findViewById(R.id.rank);
            mRank.setVisibility(View.VISIBLE);


            mUserPic = (ScaledImageView)itemView.findViewById(R.id.image_view_profile);
            mUserPic.setAnimResID(R.anim.fade_in);
            mUserPic.setAutoShowRecycle(true);
            mItem.setOnClickListener(mItemClickListener);

        }
    }

    public class SpinnerDataObjectHolder extends RecyclerView.ViewHolder {
        private ImageView mSpinnerView;
        public SpinnerDataObjectHolder(View itemView) {
            super(itemView);
            mSpinnerView = (ImageView) itemView.findViewById(R.id.page_spinner);
        }
    }
}

