package fm.bling.blingy;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.view.Surface;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Map;

import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.FFMPEG;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 25/09/2016.
 * History:
 * ***********************************
 */
public class ProcessImage
{
    private static ProcessImage instance;

    ////////////// find chroma key return codes ///////////////
    public final static int     DetectionResponseSuccess           = 0,
                                DetectionResponseFailure           = 1,
                                DetectionResponseBadLighting       = 2,
                                DetectionResponseTooManyColors     = 3,
                                DetectionResponseNoChromaColors    = 4,
                                DetectionResponseTooManyEdges      = 5,
                                DetectionResponseNoFaceDetection   = 6,
                                DetectionResponseNotEnoughLighting = 7,
                                DetectionResponseTooMuchLighting   = 8;
    ///////////////////////////////////////////////////////////

    public final static float EXPOSURE_MAX_DIF_ALLOWED = 0.2f; // percentage of the max and min device exposure which is the limit of the current exposure
                                                               // from the max and min values

    private final String dlibDataFileName = "shape_predictor_68_face_landmarks.dat";

    public static ProcessImage getInstance()
    {
        if(instance == null)
            instance = new ProcessImage();
        return instance;
    }

    private ProcessImage()
    {

    }

    public void initialize()
    {
        init();
    }

    public void blurBitmap(Bitmap bitmap, int intensity)
    {
        if(bitmap != null) {
            Mat toBlur = new Mat(bitmap.getWidth(), bitmap.getHeight(), 0);
            Utils.bitmapToMat(bitmap, toBlur);
            blurMat(toBlur.getNativeObjAddr(), intensity);
            Utils.matToBitmap(toBlur, bitmap);
            toBlur.release();
        }
    }

    public void rotateImageFile(String fileName, int rotation)
    {
        rotation %= 360;
        rotateImage(fileName, rotation);
    }

    static {
        System.loadLibrary("blingyNdk");
    }

    public native void init();

    public native byte[] processImage(byte[] inBuffer, byte[] outBuffer, int width, int height, int type);

    public native void processImageAddr(long mAddr, String bgFileName, boolean hasBg, int faceLeft, int faceTop, int faceRight, int faceBottom);

    private native void blurMat(long mAddr, int intensity);

    public native void rotateImage(String fileName, int angle);

    public native void rotateImageCameraII(String fileName, int angle);

    public native boolean checkScreen(long mAddr);

//    public native void findChromaKeys(long mAddr);
    public native int findChromaKeys(long mAddr);

    public native int findChromaKeysCamera2(long mAddr, Surface dstSurface);

    public native int addChromaKeys(long mAddr);

    public native int addChromaKeysCamera2(long mAddr, Surface dstSurface);

    public native boolean isValidChromaKeyAgainstSkinColor();

    public native void gsImage(long mAddr, String bgFileName, boolean hasBg, int faceLeft, int faceTop, int faceRight, int faceBottom);

    public native boolean multiDetection(long mAddr, String bgFileName, boolean hasBg, boolean isRecording, String frameOutputPath, boolean isRealTime);

    public native void getResizedPicture(long mAddr);

    public native void saveFrame(long addr, String frameOutputPath);



    public native void loadFrame(long addr, String frameInputPath);

    public native void setCameraRotation(int cameraRotation, boolean isFrontCamera);

    public native int checkFaceLightness(long mAddr, int faceLeft, int faceTop, int faceRight, int faceBottom);

    public native boolean procSurface(long yuv, Surface dst, String frameFileName, boolean isRecording, String frameOutputPath);

    public native void setSurface(Surface dst);

    public native void getResizedPictureCameraII(long mAddr, Surface dstSurface);

    public native void convertYuvII(ByteBuffer yData, int yLength, ByteBuffer U, int uLength, ByteBuffer V, int vLength, int uvPixelStride, int width, int height, long addrDst);

    /**
     *  from {@link org.opencv.android.JavaCameraView}
     * @param cameraResType - @RES_960X720 @RES_1280_720 @RES_1280X960
     */
    public native void setCameraRes(int cameraResType, boolean isLiteVideo, int cameraWidth, int cameraHeight);

    private native void getMajorColorN(long mAddr);

    private native int getMajorColorRed();
    private native int getMajorColorGreen();
    private native int getMajorColorBlue();

    public synchronized int getMajorColor(Bitmap bitmap)
    {
        if(bitmap == null) return android.graphics.Color.argb(75, 0, 0, 0);
        Mat mat = new Mat(bitmap.getWidth(), bitmap.getHeight(), org.opencv.core.CvType.CV_8UC4);

        Utils.bitmapToMat(bitmap, mat);

        getMajorColorN(mat.getNativeObjAddr());
        int red   = getMajorColorRed()
          , green = getMajorColorGreen()
          , blue  = getMajorColorBlue();
        return android.graphics.Color.argb(75, red, green, blue);
    }

    private String mHaarCascadeFilePath = "";
    private boolean isFaceDetectionIsOn = false;
    public synchronized void turnOnFaceDetection(android.content.Context context)
    {
        if(isFaceDetectionIsOn) return;
        File file = new File(context.getFilesDir(), "haarcascade_frontalface_alt2.xml");
        if(!file.exists())
        {
            try
            {
                createFile(context);
            }
            catch (Throwable err)
            {
                err.printStackTrace();
            }
        }
        mHaarCascadeFilePath = file.getPath();

        turnFaceDetectionOn(mHaarCascadeFilePath, "noFileSupported");
        isFaceDetectionIsOn = true;
    }

    private native void turnFaceDetectionOn(String haarCascadeFilePath, String haarCascadeEyesFilePath);

    private void createFile(android.content.Context context) throws Throwable
    {
        InputStream inputStream = context.getResources().openRawResource(R.raw.haarcascade_frontalface_alt2);
        OutputStream outputStream = context.openFileOutput("haarcascade_frontalface_alt2.xml", 0);
        byte[] buffer = new byte[1024 * 1024];
        int count;
        while (true)
        {
            count = inputStream.read(buffer);
            if (count == -1)
                break;
            outputStream.write(buffer, 0, count);
        }
        inputStream.close();
        outputStream.flush();
        outputStream.close();
    }

    public native void resetChromaKeys();

    /**
     *
     * @param mAddr - src mem address
     * @return - the brightness level
     */
    public native int getImageBrightness(long mAddr);

    private native void initDlib(String dataPath);

    public void initMachineLearning()
    {
        if(FFMPEG.getInstance(App.getAppContext()).isLiteVideo())
            return;

        SharedPreferences sharedPreferences = App.getAppContext().getSharedPreferences(App.getAppContext().getResources().getString(R.string.user_shared_prefs), Context.MODE_MULTI_PROCESS);
        Map<String, ?> prefs = sharedPreferences.getAll();
        Boolean dLibInstalled = (Boolean) prefs.get(Constants.DLIB_EXIST);

        if(dLibInstalled == null || !dLibInstalled.booleanValue())//!new File(App.getAppContext().getFilesDir(), "/shape_predictor_68_face_landmarks.dat").exists())
        {
            new Thread()
            {
                public void run()
                {
                    File fl = new File(App.getAppContext().getFilesDir(), "/shape_predictor_68_face_landmarks.dat");
                    try
                    {
                        downloadDataFile(fl);
                        initDlib(App.getAppContext().getFilesDir().getPath() + "/shape_predictor_68_face_landmarks.dat");

                        SharedPreferences sharedPreferences = App.getAppContext().getSharedPreferences(App.getAppContext().getResources().getString(R.string.user_shared_prefs), Context.MODE_MULTI_PROCESS);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean(Constants.DLIB_EXIST, true);
                        editor.apply();
                    }
                    catch (Throwable err) { }
                }
            }.start();
        }
        else
            initDlib(App.getAppContext().getFilesDir().getPath() + "/shape_predictor_68_face_landmarks.dat");
    }

    public void downloadDataFile(File newFilePath)throws Throwable
    {
        String urlPath = "https://storage.googleapis.com/blingy-test-static/dat-files/shape_predictor_68_face_landmarks.dat";
        URL url = new URL(urlPath);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setConnectTimeout(20000);
        conn.setReadTimeout(20000);
        conn.setInstanceFollowRedirects(true);

        InputStream dataInputStream = conn.getInputStream();
        OutputStream dataOutputStream = new FileOutputStream(newFilePath);

        byte[] buffer = new byte[1024 * 50]; // 50 kb
        int count;

        while ((count = dataInputStream.read(buffer)) != -1)
        {
            dataOutputStream.write(buffer, 0, count);
        }
        dataOutputStream.flush();
        dataOutputStream.close();
        dataInputStream.close();
    }

    /**
     *
     * @param buffer - the buffer to read from
     */
    public native void saveBufferToFile(byte[] buffer, int bufferLength, String filePath);

    /**
     *
     * @param buffer - the buffer to write to
     */
    public native void loadBufferFromFile(byte[] buffer, int bufferLength, String filePath);

}