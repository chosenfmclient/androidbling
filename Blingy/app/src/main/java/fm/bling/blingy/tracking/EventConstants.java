package fm.bling.blingy.tracking;

/**
 * Created by Oren Zakay on 12/16/15.
 */
public class EventConstants {

    /*******************
     *     Keys      *
     *******************/
    public static final String PLATFORM = "platform";
    public static final String INVITE_OPTION = "invite_option";
    public static final String INVITES_SENT = "invites_sent";
    public static final String FAILED_LOGINS = "failed_logins";
    public static final String AUTH_TYPE = "authentication_type";
    public static final String PAGE_NAME = "pagename";
    public static final String OBJECT_ID = "object_id";
    public static final String ARTIST_NAME = "artist_name";
    public static final String VIDEO_NAME = "video_name";
    public static final String POS = "pos";
    public static final String DURATION_SEC = "duration_sec";
    public static final String STATUS = "status";
    public static final String EFFECTS = "effects";
    public static final String VIDEOS_CREATED = "videos_created";
    public static final String SHARE_ID = "share_id";
    public static final String RELATIONSHIP = "relationship";
    public static final String PROFILE_PIC_SET = "profile_pic_set";
    public static final String FOLLOWED_ID = "followed_id";
    public static final String UNFOLLOWED_ID = "unfollowed_id";
    public static final String VIDEO_SPECIAL = "video_special";
    public static final String FOLLOWING_COUNT = "following_count";
    public static final String NOTIFICATION_ID = "notification_id";
    public static final String NOTIFICATION_TYPE = "notification_type";
    public static final String TEXT = "text";
    public static final String SOURCE = "notification_source";
    public static final String NOTIFICATION_METHOD = "notification_mthod";
    public static final String SEARCH_TEXT = "search_text";
    public static final String SEARCH_TYPE = "search_type";
    public static final String GOOGLE_PLUS = "google_plus";
    public static final String FACEBOOK = "facebook";
    public static final String EMAIL = "email";
    public static final String LOGIN = "login";
    public static final String ERROR = "error_msg";

    /*******************
     *     Events      *
     *******************/

    /** Registration **/
    public static final String TAP_SKIP = "invite:skip";
    public static final String TAP_SEND = "invite:send";
    public static final String REGISTRATION_GOOGLE_PLUS = "register:google_plus";
    public static final String REGISTRATION_FACEBOOK = "register:facebook";
    public static final String REGISTRATION_EMAIL = "register:email:signup";
    public static final String REGISTRATION_LOGIN = "register:email:login";
    public static final String REGISRATION_PAGE = "register:pageview";
    public static final String REGISTRATION_CANCEL = "register:cancel";
    public static final String REGISTRATION_FAILED = "register:failed";
    public static final String REGISTRATION_SUBMIT = "register:submit";


    /** Login **/
    public static final String LOGIN_PERMISSIONS_FRIENDS = "login:permissions:better_with_friends";
    public static final String LOGIN_PAGE = "login:pageview";
    public static final String LOGIN_CANCEL = "login:cancel";
    public static final String LOGIN_FAILED = "login:failed";
    public static final String LOGIN_SUBMIT = "login:submit";
    public static final String LOGIN_FACEBOOK_SUBMIT = "facebooklogin:submit";
    public static final String LOGIN_FACEBOOK_FAILED = "facebooklogin:failed";

    /** Record**/
    public static final String RECORD_PAGE = "record:addvideo:pageview";
    public static final String RECORD_VIDEO_TUTORIAL = "record:video_tutorial";
    public static final String RECORD_SHOOT_NOW = "record:shootnow";
    public static final String RECORD_CAMERA = "record:camera";
    public static final String RECORD_CANCEL = "record:cancel";
    public static final String RECORD_TAP_DETECT = "record:detect start";
    public static final String RECORD_DETECTED = "record:detect complete";
    public static final String RECORD_START = "record:start";
    public static final String RECORD_PAUSE = "record:pause";
    public static final String RECORD_STOP = "record:stop";
    public static final String RECORD_REVIEW = "record:review";
    public static final String RECORD_SUBMIT = "record:submit";
    public static final String RECORD_COMPLETED = "record:complete";

    /** Share */
    public static final String TAP_SHARE = "share:tap";
    public static final String TAP_SHARE_PLATFORM = "share:platform";
    public static final String TAP_SHARE_COMPLETED = "share:complete";

    /** Edit Profile **/
    public static final String EDIT_PROFILE_PAGE = "editprofile:pageview";
    public static final String EDIT_PROFILE_CANCEL = "editprofile:cancel";
    public static final String EDIT_PROFILE_SUMBIT = "editprofile:submit";
    public static final String EDIT_PROFILE_EDIT_PIC = "editprofile:editpic";
    public static final String EDIT_PROFILE_SUBMIT_PIC = "editprofile:picsubmit";

    /** Follow **/
    public static final String TAP_FOLLOW = "follow";
    public static final String TAP_UNFOLLOW = "unfollow";

    /** Like **/
    public static final String TAP_LIKE = "like";
    public static final String TAP_UNLIKE = "unlike";

    /** Notification **/
    public static final String NOTIFICATION_PAGE = "notification:pageview";
    public static final String TAP_NOTIFICATION = "notification:tap";
    public static final String NOTIFICATION_PUSH_RECEIVED = "pushnotification:sent";

    /** HomePage **/
    public static final String HOME_PAGE = "home:pageview";
    public static final String VIDEO_VIEW = "video:view";

    /** PickMusicClip **/
    public static final String PICK_MUSIC_PAGE = "record:addvideo:pageview";//Pick music clip
    public static final String PICK_MUSIC_SEARCH = "search:seed";//Pick music clip

    /** Disocver **/
    public static final String DISCOVER_PAGE = "discover:pageview";
    public static final String DISCOVER_SEARCH = "search:ugc";//Disocver

    /**Song Home**/
    public static final String SONG_HOME_PAGE = "seedhome:pageview";

    /** Profile **/
    public static final String MY_PROFILE_PAGE = "myprofile:pageview";
    public static final String PROFILE_PAGE = "profile:pageview";

    /** Camera Error **/
    public static final String CAMERA_ERROR = "camera:error";

    /** Cameo **/
    public static final String DUETS_HOME_PAGE = "duets_home:pageview";
    public static final String CAMEO_INVITE_PAGE = "duet_invite:pageview";
    public static final String CAMEO_INVITE_SENT = "duet_invite:invite";
    public static final String CAMEO_MAKE = "duet:make";
    public static final String CAMEO_COMPLETED = "duet:completed";
    public static final String CAMEO_VIEW_ORIGINAL = "duet:view_original";

    /** Preparing Error **/
    public static final String PREPARING_CLIP_ERROR = "preparingclip:error";
}