package fm.bling.blingy.inviteFriends.listeners;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/19/16.
 * History:
 * ***********************************
 */
public interface TabTitleListener
{
    void onTitleChanged(String newTitle);
}