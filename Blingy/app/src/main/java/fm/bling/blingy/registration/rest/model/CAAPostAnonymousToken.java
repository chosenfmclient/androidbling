package fm.bling.blingy.registration.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAPostToken
 * Description:
 * Created by Zach Gerstman on 6/22/15.
 * History:
 * ***********************************
 */
public class CAAPostAnonymousToken {

    public CAAPostAnonymousToken(String deviceId, String appId, String secret) {
        this.deviceId = deviceId;
        this.appId = appId;
        this.secret = secret;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @SerializedName("device_id")
    private String deviceId;

    @SerializedName("app_id")
    private String appId;

    @SerializedName("secret")
    private String secret;

    @SerializedName("auto")
    private String auto;

    public String getAuto() {
        return auto;
    }

    public void setAuto(String auto) {
        this.auto = auto;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
