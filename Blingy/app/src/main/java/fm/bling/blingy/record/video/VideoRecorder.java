package fm.bling.blingy.record.video;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.SystemClock;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import fm.bling.blingy.App;
import fm.bling.blingy.ProcessImage;
import fm.bling.blingy.record.Camera1View;
import fm.bling.blingy.utils.FFMPEG;
import fm.bling.blingy.utils.Tic;

/**
 * *********************************
 * Project: Chosen Android Application. All rights reserved.
 * Description:
 * Created by Ben Levi on 6/03/16.
 * History:
 * ***********************************
 */
public class VideoRecorder
{
    private final String CONTAINER_OUTPUT_FILE_NAME = "subpart", VIDEO_EXTENSION = ".mp4",
            FRAME_OUTPUT_FILE_NAME = "frame", FRAME_EXTENSION = ".jpg";

    private final long baseSecondInNano = 1000000 * 1000;

    private long secondInNano = baseSecondInNano;
    private int FPS, Nano4Frame;
    private float mRecordSpeed = 1f;
    private volatile int secondsCounter;
    private boolean isFirst = true;//, isResumed;
    private volatile long startContainerTime;
    private volatile long pausedTimestamp;
    private volatile FramesContainer currentFramesContainer;
    private volatile ArrayList<FramesContainer>framesContainers;
    public final String workingDirectory;
    private final Context mContext;
    private int startedFramesContainerIndex;
    private OnDoneProcessingListener mOnDoneProcessingListener;
    private ExecutorService executorService;
    private int THREAD_PRIORITY = -6;


    public VideoRecorder(Context context)
    {
        mContext = context;
        framesContainers = new ArrayList<>();

        workingDirectory = App.VIDEO_WORKING_FOLDER +"videoRecorder/";
        if(! new File(workingDirectory).exists())
            new File(workingDirectory).mkdirs();
    }

    public String getFramePath(long timeStamp)
    {
        String timeStampStr = timeStamp +"";
        if(timeStampStr.length() < 2)
            timeStampStr = 0 + timeStampStr;
        return currentFramesContainer.frameOutputPath + timeStampStr + FRAME_EXTENSION;
    }

//    public void addFrame(Bitmap frameBitmap, long timeStamp)
//    {
//        if(isFirst)
//        {
//            startContainerTime = timeStamp;
//            currentFramesContainer = new FramesContainer(secondsCounter++);
//            framesContainers.add(currentFramesContainer);
//            isFirst = false;
//        }
////        else if(isResumed)
////        {
////            pausedTimestamp = SystemClock.elapsedRealtimeNanos() - pausedTimestamp;
////            startContainerTime += pausedTimestamp;
////            isResumed = false;
////        }
//
//        if(timeStamp - startContainerTime > secondInNano)
//        {
//            startContainerTime += secondInNano;
//            framesContainers.add(currentFramesContainer = new FramesContainer(secondsCounter++));
//        }
//        currentFramesContainer.addFrame(new Frame(frameBitmap, timeStamp));
//    }

    public String addFrame(long timeStamp)
    {
        if(isFirst)
        {
            startContainerTime = timeStamp;
            currentFramesContainer = new FramesContainer(secondsCounter++);
            framesContainers.add(currentFramesContainer);
            isFirst = false;
        }

        if(timeStamp - startContainerTime > secondInNano)
        {
            startContainerTime += secondInNano;
            framesContainers.add(currentFramesContainer = new FramesContainer(secondsCounter++));
        }

        String framePath = getFramePath(timeStamp);
        currentFramesContainer.addFrame(new Frame(new File(framePath), timeStamp, true));
        return framePath;
    }

    public String addDroppedFrame(long timeStamp, String frameBackgroundPath)
    {
        if(isFirst)
        {
            startContainerTime = timeStamp;
            currentFramesContainer = new FramesContainer(secondsCounter++);
            framesContainers.add(currentFramesContainer);
            isFirst = false;
        }

        if(timeStamp - startContainerTime > secondInNano)
        {
            startContainerTime += secondInNano;
            framesContainers.add(currentFramesContainer = new FramesContainer(secondsCounter++));
        }
        else if(timeStamp - startContainerTime < 0)
        {
            //// TODO: 29/05/2017 handle this case - Not handling this might create jumps
//            return ;
        }

        String framePath = getFramePath(timeStamp);
        currentFramesContainer.addFrame(new Frame(new File(framePath), timeStamp, false).setFrameBackgroundPath(frameBackgroundPath));
        return framePath;
    }

    public void pause()
    {
        pausedTimestamp = SystemClock.elapsedRealtimeNanos();
    }

    public void start()
    {
        if(isFirst)
            startedFramesContainerIndex = 0;
        else
        {
            isFirst = true;//isResumed = true;
            startedFramesContainerIndex = framesContainers.size();// - 1;
        }
    }

//    public void finalize(int iFps)
//    {
//        Tic.tic();
//        FPS = ((int)(iFps * mRecordSpeed));
//        Nano4Frame = (int) (baseSecondInNano / iFps); // this uses the base values, so there is no need to multiply by mRecordSpeed the upper part and the base
//        for(int i=0;i<framesContainers.size()-1;i++)
//        {
//            if(framesContainers.get(i).outputFile == null)
//                framesContainers.get(i).close(true);
//        }
//        if(framesContainers.get(framesContainers.size() - 1).outputFile == null)
//        {
//            framesContainers.get(framesContainers.size() - 1).close(false);
//        }
//
//        Tic.tac();
//
//        currentFramesContainer = null;
//        mContext = null;
//    }

//    08-11 15:05:26.831 14174-15544/chosen.fm.chosenandroid I/System.out: >>Tic>> Elapsed time since start : 11582 ms
//    08-11 15:32:16.561 14174-15516/chosen.fm.chosenandroid I/System.out: >>Tic>> Elapsed time since start : 11613 ms
//    08-11 16:05:27.761 2043-2499/chosen.fm.chosenandroid I/System.out: >>tic>tac> Elapsed time since start : 12133 ms  => mama

    private int actionsCounter, numOfActionsToCount;

    public void finalize(int iFps, OnDoneProcessingListener onDoneProcessingListener)
    {
        if(isProceeded)
            return;

//        executorService = Executors.newFixedThreadPool(10);
        mOnDoneProcessingListener = onDoneProcessingListener;
        FPS = ((int)(iFps * mRecordSpeed));
        Nano4Frame = (int) (baseSecondInNano / iFps); // this uses the base values, so there is no need to multiply by mRecordSpeed the upper part and the base

        numOfActionsToCount = framesContainers.size();
        actionsCounter = 0;

        setGaps();

        int skipped = 0;
        System.out.println(">>>>>ffmpeg>>>>>finalize containers>>>>"+framesContainers.size());


        for(int i=0;i<framesContainers.size()-1;i++)
        {
            System.out.println(">>>>>ffmpeg>>>>>finalize containers>>close container>>"+i+">>>>size>"+framesContainers.get(i).container.size());

            if(framesContainers.get(i).container.size() !=0)//framesContainers.get(i).outputFile == null)
            {
                framesContainers.get(i).id -= skipped;
                if(framesContainers.get(i).audioEndTimeStamp != 0)
                    framesContainers.get(i).closeThread(false);
                else framesContainers.get(i).closeThread(true);
            }
            else
            {
                skipped++;
                numOfActionsToCount--;
            }
        }

        System.out.println(">>>>>ffmpeg>>>>>finalize containers>>close container>>"+(framesContainers.size() - 1)+">>>>size>"+framesContainers.get(framesContainers.size() - 1).container.size());

        if(framesContainers.get(framesContainers.size() - 1).container.size() !=0)//framesContainers.get(framesContainers.size() - 1).outputFile == null)
        {
            framesContainers.get(framesContainers.size() - 1).closeThread(false);
        }
        else
        {
            numOfActionsToCount--;
        }

        System.out.println(">>>>>ffmpeg>>>>>finalize containers>>close container> DONE >"+(framesContainers.size() - 1)+">>>>size>"+framesContainers.get(framesContainers.size() - 1).container.size());


//        executorService.shutdown();
//        try{executorService.awaitTermination(40000, TimeUnit.MILLISECONDS);}catch (Throwable err){}

//        proceed();

        currentFramesContainer = null;
//        mContext = null;
    }

    private void setGaps()
    {
        FramesContainer framesContainer;
        int gapSum = 0;
        for(int i = 0; i < framesContainers.size(); i++)
        {
            framesContainer = framesContainers.get(i);

            if(framesContainer.audioStartTimeStamp != 0)
            {
                gapSum += (framesContainer.container.get(0).timeStamp - framesContainer.audioStartTimeStamp);
                framesContainer.missingFrames = gapSum / Nano4Frame;

                if(framesContainer.missingFrames != 0)
                {
                    gapSum -= (framesContainer.missingFrames / Nano4Frame);
                }
            }

            else if(framesContainer.audioEndTimeStamp != 0)
            {
                gapSum += (framesContainer.container.get(framesContainer.container.size() - 1).timeStamp - framesContainer.audioEndTimeStamp);
                framesContainer.missingFrames = gapSum / Nano4Frame;

                if(framesContainer.missingFrames != 0)
                {
                    gapSum -= (framesContainer.missingFrames / Nano4Frame);
                }
            }
            int halfGap = gapSum % Nano4Frame;
            if(halfGap > Nano4Frame / 2)
            {
                framesContainer.missingFrames ++;
                gapSum -= halfGap;
            }
            else if(halfGap < Nano4Frame / 2)
            {
                framesContainer.missingFrames --;
                gapSum -= halfGap;
            }
        }
    }

    private boolean isProceeded = false;
    private synchronized void proceed(boolean isCheckOnly)
    {
        if(isProceeded)
            return;

        if(!isCheckOnly)
            actionsCounter++;

        if(actionsCounter >= numOfActionsToCount)
        {
//            executorService.shutdown();

            if(mOnDoneProcessingListener != null)
            {
                mOnDoneProcessingListener.onDone();
                isProceeded = true;
            }
        }
    }

    private void proceed()
    {
        if(isProceeded)
            return;

        if(mOnDoneProcessingListener != null)
        {
            mOnDoneProcessingListener.onDone();
            isProceeded = true;
        }
    }

    public String[] getSubPaths()
    {
        ArrayList<String>paths = new ArrayList<>();
        for(int i=0;i<framesContainers.size();i++)
        {
            if(framesContainers.get(i).outputFile != null)// && framesContainers.get(i).container.size() > 0)
                paths.add(framesContainers.get(i).outputFile);
        }
        framesContainers = null;
        return paths.toArray(new String[paths.size()]);
    }

    public int getFPS()
    {
        return FPS;
    }

    public void setAudioTimeStamps(long startTimeStamp, long endTimeStamp)
    {
        if(framesContainers == null || framesContainers.size() == 0)
            return;

        framesContainers.get(startedFramesContainerIndex).audioStartTimeStamp = startTimeStamp;
        if(currentFramesContainer == null)
            currentFramesContainer = framesContainers.get(framesContainers.size() - 1);
        if(startedFramesContainerIndex != framesContainers.size() - 1)
            currentFramesContainer.audioEndTimeStamp = endTimeStamp;
//        currentFramesContainer.pausedLocation = currentFramesContainer.container.size() - 1;
    }

    private class FramesContainer
    {
        private final ArrayList<Frame>container;
        private String outputFile = null;
        private int id;
        private final String selfWorkingDirectory, frameOutputPath;
        //        private int framesCounter;׳
        private int missingFrames;

        //        private int pausedLocation;
        private long audioStartTimeStamp = 0L, audioEndTimeStamp = 0L;

        private FramesContainer(int id)
        {
            container = new ArrayList<>(35);// 30 is usually the defualt fps, we do 35, just for case, so the os won't need to rebuild the array
            this.id = id;
            selfWorkingDirectory = workingDirectory + id+ "/";
            frameOutputPath = selfWorkingDirectory + FRAME_OUTPUT_FILE_NAME;
            new File(selfWorkingDirectory).mkdirs();
        }

        private void addFrame(Frame frame)
        {
            synchronized (this)
            {
                int i = container.size();
                for (; i > 0; i--)
                {
                    if (frame.timeStamp > container.get(i - 1).timeStamp)
                        break;
                }
                container.add(i, frame);
            }
        }

        private void close(boolean inFps)
        {
            System.out.println(">>>>ffmpeg>>close>>finalizing second<<<<start ffmpeg>>>>"+container.size()+">>>"+inFps);

            if(inFps)
            {
                int overFramesCount = container.size() - FPS;

                if (overFramesCount > 0)// too many frames
                {
                    while(overFramesCount > 0)
                    {
                        container.remove(container.size() - 1);
                        overFramesCount--;
                    }
                }
                else if (overFramesCount < 0)
                {
                    while(overFramesCount < 0)
                    {
                        container.add(copyFrame(container.get(container.size()-1), frameOutputPath, container.get(container.size()-1).timeStamp));
                        overFramesCount++;
                    }
                }

                // did pause occur in this container
                if(missingFrames != 0l)
                {
                    handleSyncForPause();
                }
            }
            else
            {
                if(missingFrames != 0l)
                {
                    handleSyncForPause();
                }
            }
//                    if(container.size() > FPS)
//                    {
//                    }

            for(int i=0;i<container.size();i++)
            {
                if(!container.get(i).isProcessed)
                    postProcess(container.get(i));
                if(i<10)
                    container.get(i).outputFile.renameTo(new File(frameOutputPath + "0" + i + FRAME_EXTENSION));
                else container.get(i).outputFile.renameTo(new File(frameOutputPath + i + FRAME_EXTENSION));
            }
            outputFile = workingDirectory + CONTAINER_OUTPUT_FILE_NAME + id + VIDEO_EXTENSION;

            System.out.println(">>>>ffmpeg>>>>finalizing second<<<<before ffmpeg>>>>"+container.size());
            try
            {
//                        int ret = FFMPEG.getInstance(mContext).renderImagesToVideo(frameOutputPath, FRAME_EXTENSION, outputFile, FPS);
                int ret = FFMPEG.getInstance(mContext).renderImagesToVideo(frameOutputPath, FRAME_EXTENSION, outputFile, container.size());
                System.out.println(">>>>ffmpeg>>>>finalizing second<<<<>>>>"+ret);
            }
            catch (Throwable err){err.printStackTrace();}

            System.out.println(">>>>ffmpeg>>>>finalizing second<<<<after ffmpeg>>>>"+container.size());

            Tic.tac(id);
            container.clear();
        }

//        private void close(boolean inFps)
//        {
//            if(inFps)
//            {
//                int overFramesCount = container.size() - FPS;
//                if (overFramesCount > 0)// too many frames
//                {
//                    int partIndex = container.size() / overFramesCount; // divide the frames to delete some of them
//                    if(partIndex == 0)
//                        partIndex = 1;
//                    int size = container.size() - 2;
//                    for (int i = size; i > -1; i -= partIndex)
//                    {
//                        container.remove(i);
//                    }
//                    while(container.size() > FPS)
//                        container.remove(container.size()-1);
//                }
//                else if (overFramesCount < 0)
//                {
//                    overFramesCount *= (-1);
//                    int partIndex = FPS / overFramesCount; // divide the frames to duplicate some of them
//                    if(partIndex <= 1)
//                        partIndex = 2;
//                    int size = container.size() - 2;
//                    for (int i = size; i > -1; i -= partIndex)
//                    {
//                        container.add(i, copyFrame(container.get(i), frameOutputPath, container.get(i).timeStamp));
//                    }
//                    while(container.size() < FPS)
//                        container.add(copyFrame(container.get(container.size() - 1), frameOutputPath, container.get(container.size() - 1).timeStamp));
//                }
//
//                // did pause occur in this container
//                if(pausedLocation != 0l || audioStartTimeStamp != 0l)
//                {
//                    handleSyncForPause();
//                }
//            }
//
//            for(int i=0;i<container.size();i++)
//            {
//                if(i<10)
//                    container.get(i).outputFile.renameTo(new File(frameOutputPath + "0" + i + FRAME_EXTENSION));
//                else container.get(i).outputFile.renameTo(new File(frameOutputPath + i + FRAME_EXTENSION));
//            }
//
//            outputFile = workingDirectory + CONTAINER_OUTPUT_FILE_NAME + id + VIDEO_EXTENSION;
//            try
//            {
//                FFMPEG.getInstance(mContext).renderImagesToVideo(frameOutputPath, FRAME_EXTENSION, outputFile, FPS);
//            }
//            catch (Throwable err){}
//
//            container.clear();
//        }
//
//        private void closeThread(final boolean inFps)
//        {
//            executorService.submit(new Runnable()
//            {
//                @Override
//                public void run()
//                {
//                    if(inFps)
//                    {
//                        int overFramesCount = container.size() - FPS;
//                        if (overFramesCount > 0)// too many frames
//                        {
//                            int partIndex = container.size() / overFramesCount; // divide the frames to delete some of them
//                            if(partIndex == 0)
//                                partIndex = 1;
//                            int size = container.size() - 2;
//                            for (int i = size; i > -1; i -= partIndex)
//                            {
//                                container.remove(i);
//                            }
//                            while(container.size() > FPS)
//                                container.remove(container.size()-1);
//                        }
//                        else if (overFramesCount < 0)
//                        {
//                            overFramesCount *= (-1);
//                            int partIndex = FPS / overFramesCount; // divide the frames to duplicate some of them
//                            if(partIndex <= 1)
//                                partIndex = 2;
//                            int size = container.size() - 2;
//                            for (int i = size; i > -1; i -= partIndex)
//                            {
//                                container.add(i, copyFrame(container.get(i), frameOutputPath, container.get(i).timeStamp));
//                            }
//                            while(container.size() < FPS)
//                                container.add(copyFrame(container.get(container.size() - 1), frameOutputPath, container.get(container.size() - 1).timeStamp));
//                        }
//
//                        // did pause occur in this container
//                        if(pausedLocation != 0l || audioStartTimeStamp != 0l)
//                        {
//                            handleSyncForPause();
//                        }
//                    }
//
//                    for(int i=0;i<container.size();i++)
//                    {
//                        if(i<10)
//                            container.get(i).outputFile.renameTo(new File(frameOutputPath + "0" + i + FRAME_EXTENSION));
//                        else container.get(i).outputFile.renameTo(new File(frameOutputPath + i + FRAME_EXTENSION));
//                    }
//
//                    outputFile = workingDirectory + CONTAINER_OUTPUT_FILE_NAME + id + VIDEO_EXTENSION;
//                    try
//                    {
//                        int ret = FFMPEG.getInstance(mContext).renderImagesToVideo(frameOutputPath, FRAME_EXTENSION, outputFile, FPS);
//                    }
//                    catch (Throwable err){err.printStackTrace();}
//
//                    container.clear();
//                    proceed(false);
//                }
//            });
//        }

//        private void closeThread(final boolean inFps)
//        {
//            executorService.submit(new Runnable()
//            {
//                @Override
//                public void run()
//                {
//                    System.out.println(">>>>ffmpeg>>>>finalizing second<<<<start ffmpeg>>>>"+container.size()+">>>"+inFps);
//
//                    if(inFps)
//                    {
//                        int overFramesCount = container.size() - FPS;
//
//                        if (overFramesCount > 0)// too many frames
//                        {
//                            while(overFramesCount > 0)
//                            {
//                                container.remove(container.size() - 1);
//                                overFramesCount--;
//                            }
//                        }
//                        else if (overFramesCount < 0)
//                        {
//                            while(overFramesCount < 0)
//                            {
//                                container.add(copyFrame(container.get(container.size()-1), frameOutputPath, container.get(container.size()-1).timeStamp));
//                                overFramesCount++;
//                            }
//                        }
//
//                        // did pause occur in this container
//                        if(missingFrames != 0l)
//                        {
//                            handleSyncForPause();
//                        }
//                    }
//                    else
//                    {
//                        if(missingFrames != 0l)
//                        {
//                            handleSyncForPause();
//                        }
//                    }
////                    if(container.size() > FPS)
////                    {
////                    }
//
//                    for(int i=0;i<container.size();i++)
//                    {
//                        if(!container.get(i).isProcessed)
//                            postProcess(container.get(i));
//                        if(i<10)
//                            container.get(i).outputFile.renameTo(new File(frameOutputPath + "0" + i + FRAME_EXTENSION));
//                        else container.get(i).outputFile.renameTo(new File(frameOutputPath + i + FRAME_EXTENSION));
//                    }
//                    outputFile = workingDirectory + CONTAINER_OUTPUT_FILE_NAME + id + VIDEO_EXTENSION;
//
//                    System.out.println(">>>>ffmpeg>>>>finalizing second<<<<before ffmpeg>>>>"+container.size());
//                    try
//                    {
////                        int ret = FFMPEG.getInstance(mContext).renderImagesToVideo(frameOutputPath, FRAME_EXTENSION, outputFile, FPS);
//                        int ret = FFMPEG.getInstance(mContext).renderImagesToVideo(frameOutputPath, FRAME_EXTENSION, outputFile, container.size());
//                        System.out.println(">>>>ffmpeg>>>>finalizing second<<<<>>>>"+ret);
//                    }
//                    catch (Throwable err){err.printStackTrace();}
//
//                    System.out.println(">>>>ffmpeg>>>>finalizing second<<<<after ffmpeg>>>>"+container.size());
//
//                    Tic.tac(id);
//                    container.clear();
////                    proceed(false);
//                }
//            });
//        }

        private void closeThread(final boolean inFps)
        {
            new Thread()
            {
                public void run()
                {
                    android.os.Process.setThreadPriority(THREAD_PRIORITY);

                    close(inFps);
                    proceed(false);
                }
            }.start();
        }


        private void handleSyncForPause()
        {
//            if(pausedLocation > container.size() - 1)// making sure that we didn't delete it before !
//                pausedLocation = container.size() - 1;

            if(currentFramesContainer != FramesContainer.this)
            {
                if(audioStartTimeStamp != 0l && audioEndTimeStamp != 0l)
                {
                    throw new RuntimeException("WTF"); // so we'll know that something wrong happened !!!!
                }

                else if (audioStartTimeStamp != 0l)
                {
                    if (missingFrames < 0) // audio started after the video
                    // We'll add frames to sync - adding frames to this video part
                    {
                        int looper = missingFrames * -1;
                        for(int i=0;i<looper;i++)
                        {
                            container.add(0, copyFrame(container.get(0), frameOutputPath, container.get(0).timeStamp));
//                            container.remove(0);
                        }
                    }
                    else// if (missingFrames > 0) // audio Ended before the video
                    // We'll deduct frames to sync - cutting frames from this video part
                    {
                        for(int i = missingFrames; i < 0; i++)
                        {
                            container.remove(0);
//                            container.add(0, copyFrame(container.get(0), frameOutputPath, container.get(0).timeStamp));
                        }
                    }
                }

                else if(audioEndTimeStamp != 0)
                {
                    if(missingFrames != 0)
                    {
                        if (missingFrames < 0) // audio Ended after the video
                        // We'll add frames to sync - adding frames to this video part
                        {
                            int looper = missingFrames * -1;
                            for(int i = looper; i < 0; i++)
                            {
                                container.add(container.size() - 1, copyFrame(container.get(container.size() - 1), frameOutputPath, container.get(container.size() - 1).timeStamp));
                            }
                        }
                        else// if (missingFrames > 0) // audio Ended before the video
                        // We'll deduct frames to sync - cutting frames from this video part
                        {
                            for(int i=0; i<missingFrames; i++)
                            {
                                container.remove(container.size() - 1);
                            }
                        }
                    }
                }

            }
        }
    }

    private byte[] loadBuffer(String framePath)
    {
        try
        {
            InputStream inputStream = new FileInputStream(framePath);
            byte[] dstBuffer = new byte[Camera1View.bufferSize];
            byte[] tmpBuffer = new byte[1024*50];//50k
            int len, index = 0;
            while((len = inputStream.read(tmpBuffer)) != -1)
            {
                System.arraycopy(tmpBuffer, 0, dstBuffer, index, len);
                index += len;
            }
            return dstBuffer;
        }
        catch (Throwable err)
        {
            err.printStackTrace();
            return null;
        }
    }

    private synchronized void postProcess(Frame unProcessedFrame)
    {
        byte[] frameBuffer = loadBuffer(unProcessedFrame.outputFile.getPath());

        Mat frameNV21 = new Mat(Camera1View.CAMERA_IN_HEIGHT_YUV, Camera1View.CAMERA_IN_WIDTH, CvType.CV_8UC1);
        frameNV21.put(0, 0, frameBuffer);

        Mat bgra = new Mat(Camera1View.cameraInSize, CvType.CV_8UC4);

        Imgproc.cvtColor(frameNV21, bgra, Imgproc.COLOR_YUV2RGBA_NV21, 4);
        frameNV21.release();

        ProcessImage.getInstance().multiDetection(bgra.nativeObj, unProcessedFrame.mFrameBackgroundPath, true, true, unProcessedFrame.outputFile.getPath(), false);

        bgra.release();

//        Mat loadedMat = new Mat(Camera1View.cameraInSize, CvType.CV_8UC4);
//
//        ProcessImage.getInstance().loadFrame(loadedMat.nativeObj, unProcessedFrame.outputFile.getPath());
//
//        ProcessImage.getInstance().multiDetection(loadedMat.nativeObj, unProcessedFrame.mFrameBackgroundPath, true, true, unProcessedFrame.outputFile.getPath());
//        loadedMat.release();
    }

    private Frame copyFrame(Frame frameToCopy, String filePath, long timeStamp)
    {
        File frameFile = new File(filePath + System.currentTimeMillis() + FRAME_EXTENSION);
        try
        {
            InputStream inputStream = new FileInputStream(frameToCopy.outputFile);
            OutputStream outputStream = new FileOutputStream(frameFile);
            byte[]buffer = new byte[102400];
            int ind;
            while( (ind = inputStream.read(buffer)) > 0 )
            {
                outputStream.write(buffer, 0, ind);
            }
            outputStream.flush();
            inputStream.close();
            outputStream.close();
        }
        catch (Throwable err)
        {
            err.printStackTrace();
        }
        return new Frame(frameFile, timeStamp, frameToCopy.isProcessed).setFrameBackgroundPath(frameToCopy.mFrameBackgroundPath);
    }

    private class Frame
    {
        private long timeStamp;
        private final File outputFile;
        private boolean isProcessed = true;
        private String mFrameBackgroundPath = null;

        private Frame(Bitmap frame, long timeStamp)
        {
            this.timeStamp = timeStamp;
            outputFile = new File(getFramePath(timeStamp));

            try
            {
                OutputStream outputStream = new FileOutputStream(outputFile);

                frame.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
                outputStream.flush();
                outputStream.close();
            }
            catch (Throwable err)
            {
                err.printStackTrace();
            }
        }

        private Frame(File iOutputFile, long iTimeStamp, boolean iIsProcessed)
        {
            this.timeStamp = iTimeStamp;
            this.outputFile = iOutputFile;
            this.isProcessed = iIsProcessed;
        }

        private Frame setFrameBackgroundPath(String frameBackgroundPath)
        {
            mFrameBackgroundPath = frameBackgroundPath;
            return this;
        }

    }

    private void deleteDir(File dirFile)
    {
        File[]files = dirFile.listFiles();
        for(int i=0; i<files.length;i++)
        {
            if(files[i].isDirectory() && !files[i].getName().equals("audio"))
                deleteDir(files[i]);
            files[i].delete();
        }
    }

    public void emptyWorkingFolder()
    {
        deleteDir(new File(workingDirectory));
        if(!new File(workingDirectory).exists())
            new File(workingDirectory).mkdirs();
    }


    public void setRecordSpeed(float speed)
    {
        this.mRecordSpeed = speed;
        secondInNano = (long) (speed * baseSecondInNano);
    }

    public interface OnDoneProcessingListener
    {
        void onDone();
    }

}