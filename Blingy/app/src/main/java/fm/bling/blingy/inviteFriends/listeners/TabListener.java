package fm.bling.blingy.inviteFriends.listeners;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/14/16.
 * History:
 * ***********************************
 */
public interface TabListener
{
    public void onTabSelected(String tabName, int index);
}