package fm.bling.blingy.profile;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;


import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.listeners.OnDoneLoadingListener;
import fm.bling.blingy.profile.fragments.BaseSearchFragment;
import fm.bling.blingy.profile.fragments.FollowersFragment;
import fm.bling.blingy.profile.fragments.FollowingFragment;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.utils.Constants;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/30/16.
 * History:
 * ***********************************
 */
public class UserConnectionsActivity2 extends BaseActivity implements OnDoneLoadingListener {

    /**
     * Main Views
     */
    private ImageView spinner;
    private FrameLayout loadingLayout;
    private ObjectAnimator anim;
    private ViewPager mViewPager;
    private UserConnectionsActivity2.PagerAdapter mPagerAdapter;

    private String mUserId;
    private int mNumOfFollowers;
    private int mNumOfFollowing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_tabs_layout);

        loadingLayout = (FrameLayout) findViewById(R.id.loading_layout);
        ((FrameLayout.LayoutParams)loadingLayout.getLayoutParams()).topMargin = 2 * App.TOOLBAR_HEIGHT;

        spinner = (ImageView) findViewById(R.id.spinner);
        loadAnimation();
        mViewPager = (ViewPager) findViewById(R.id.pager);
        EditText mSearch = (EditText) findViewById(R.id.search_edit_text);
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Bundle extras = getIntent().getExtras();
        mUserId = extras.getString(Constants.USER_ID);
        mNumOfFollowers = extras.getInt(Constants.FOLLOWERS_AMOUNT);
        mNumOfFollowing = extras.getInt(Constants.FOLLOWING_AMOUNT);
        int mSelectedTab = getIntent().getIntExtra(Constants.SELECTED_TAB, -1);


        TabLayout mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mTabLayout.addTab(mTabLayout.newTab().setText("FOLLOWING"));
        mTabLayout.addTab(mTabLayout.newTab().setText("FOLLOWERS"));
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ((BaseSearchFragment) (mPagerAdapter.instantiateItem(mViewPager, Constants.FOLLOWING))).onSearch(s.toString());
                ((BaseSearchFragment) (mPagerAdapter.instantiateItem(mViewPager, Constants.FOLLOWERS))).onSearch(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

        });

        mPagerAdapter = new UserConnectionsActivity2.PagerAdapter(getSupportFragmentManager(), mTabLayout.getTabCount());
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        if (mSelectedTab >= 0 && mSelectedTab <= 1)
            mViewPager.setCurrentItem(mSelectedTab);

//        mViewPager.setVisibility(View.INVISIBLE);

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    public void cancelAnimation() {
        if(anim != null)
            anim.cancel();
        loadingLayout.setVisibility(View.GONE);
    }

    private void loadAnimation() {
        loadingLayout.setVisibility(View.VISIBLE);
        anim = ObjectAnimator.ofFloat(spinner, "rotation", 0f, 359f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setDuration(1000);
        anim.setRepeatCount(Animation.INFINITE);
        anim.start();
    }

    public void updateFollowing(boolean increase, User friend){
        mNumOfFollowing = ((FollowingFragment) (mPagerAdapter.instantiateItem(mViewPager, Constants.FOLLOWING))).updateFollowing(increase, friend);
    }

    @Override
    public void onBackPressed() {
        if(mUserId.equalsIgnoreCase(Constants.ME)) {
            Intent intent = new Intent().putExtra(Constants.FOLLOWING_AMOUNT, mNumOfFollowing);
            setResult(Activity.RESULT_OK, intent);
        }
        finish();
    }

    @Override
    public void onDoneLoading() {
        mViewPager.setVisibility(View.VISIBLE);
        cancelAnimation();
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case Constants.FOLLOWERS:
                    return FollowersFragment.newInstance(mUserId, mNumOfFollowers, UserConnectionsActivity2.this);
                case Constants.FOLLOWING:
                    return FollowingFragment.newInstance(mUserId, mNumOfFollowing, UserConnectionsActivity2.this);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }
    }

}
