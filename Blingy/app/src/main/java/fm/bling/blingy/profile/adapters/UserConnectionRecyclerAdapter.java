package fm.bling.blingy.profile.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;

import fm.bling.blingy.R;
import fm.bling.blingy.database.handler.FollowingDataHandler;
import fm.bling.blingy.mashup.listeners.CameoInviteListener;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderListener;
import fm.bling.blingy.utils.imagesManagement.MultiImageLoader;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 30/11/16.
 * History:
 * ***********************************
 */
public class UserConnectionRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int TYPE_USER = 1;
    private static final int TYPE_LOADING = -1;

    private MultiImageLoader mMultiImageLoader;
    private Context mContext;
    private ArrayList<User> data;
    private Animation anim;

    private View.OnClickListener mItemClickListener;
    private View.OnClickListener mFollowClickListener;

    private boolean isCameo = false;

    public UserConnectionRecyclerAdapter(Context context, MultiImageLoader multiImageLoader, ArrayList<User> items,
                                         View.OnClickListener itemClickListener , View.OnClickListener followClickListener) {
        this.mContext = context;
        this.mMultiImageLoader = multiImageLoader;
        this.mItemClickListener = itemClickListener;
        this.mFollowClickListener = followClickListener;
        this.data = items;
        if(anim == null)
            this.anim = AnimationUtils.loadAnimation(mContext, R.anim.spinner_rotate);
    }

    public UserConnectionRecyclerAdapter(Context context, MultiImageLoader multiImageLoader, ArrayList<User> items,
                                         View.OnClickListener itemClickListener , View.OnClickListener followClickListener, boolean isCameo) {
        this.mContext = context;
        this.mMultiImageLoader = multiImageLoader;
        this.mItemClickListener = itemClickListener;
        this.mFollowClickListener = followClickListener;
        this.isCameo = isCameo;
        this.data = items;
        if(anim == null)
            this.anim = AnimationUtils.loadAnimation(mContext, R.anim.spinner_rotate);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case TYPE_USER:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_connection_item_view, parent, false);
                holder = new UserConnectionRecyclerAdapter.DataObjectHolder(view);
                break;
            case TYPE_LOADING:
                View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_list_layout, parent, false);
                holder = new UserConnectionRecyclerAdapter.SpinnerDataObjectHolder(view1);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if(holder instanceof DataObjectHolder) {
            User friend = data.get(position);
            DataObjectHolder dataObjectHolder = ((DataObjectHolder)holder);
            dataObjectHolder.textName.setText(friend.getFirstName() + " " + friend.getLastName());

            if(!friend.getUserId().contentEquals(Constants.ME) && !friend.getUserId().contentEquals(CAAUserDataSingleton.getInstance().getUserId())) {
                dataObjectHolder.addFriend.setVisibility(View.VISIBLE);
                Drawable d = null;
                if(isCameo)
                    d = friend.isCameoInvited() ? mContext.getResources().getDrawable(R.drawable.ic_followed_grey_24dp) : mContext.getResources().getDrawable(R.drawable.ic_follow_pink_24dp);
                else
                    d = iFollowYou(friend) ? mContext.getResources().getDrawable(R.drawable.ic_followed_grey_24dp) : mContext.getResources().getDrawable(R.drawable.ic_follow_green_24dp);

                dataObjectHolder.addFriend.setImageDrawable(d);
            }
            else{
                dataObjectHolder.addFriend.setVisibility(View.GONE);
            }
            dataObjectHolder.mItem.setTag(friend);
            dataObjectHolder.addFriend.setTag(friend);

            dataObjectHolder.imageProfile.setUrl(friend.getPhoto());
            dataObjectHolder.imageProfile.setImageLoader(mMultiImageLoader);
        }
        else{
            ((SpinnerDataObjectHolder)holder).mSpinnerView.setAnimation(anim);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).getFirstName() == null) {
            return TYPE_LOADING;
        } else {
            return TYPE_USER;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private boolean iFollowYou(User friend) {
        System.out.println( ">>>>>>ifollowu>>>" + FollowingDataHandler.getInstance().contains(friend.getUserId()));
        if(FollowingDataHandler.getInstance().contains(friend.getUserId()))
        {
            friend.setFollowedByMe("1");
            return true;
        }
        return false;
    }
    public class DataObjectHolder extends RecyclerView.ViewHolder{
        private LinearLayout mItem;
        private TextView textName;
        private ScaledImageView imageProfile;
        public ImageView addFriend;

        public DataObjectHolder(View itemView) {
            super(itemView);
            mItem = (LinearLayout)itemView.findViewById(R.id.connection_item);
            mItem.setBackgroundResource(R.drawable.notification_ripple_white_background);
            textName = (TextView)itemView.findViewById(R.id.text_user_name);
            imageProfile = (ScaledImageView)itemView.findViewById(R.id.image_view_profile);
            addFriend = (ImageView) itemView.findViewById(R.id.add_user_icon);

            imageProfile.setAnimResID(R.anim.fade_in);
            imageProfile.setIsRoundedImage(true);
            imageProfile.setKeepAspectRatioAccordingToWidth(true);
            imageProfile.setAutoShowRecycle(true);
            imageProfile.setImageLoader(mMultiImageLoader);

            addFriend.setOnClickListener(mFollowClickListener);
            mItem.setOnClickListener(mItemClickListener);
        }

    }

    public void setmMultiImageLoader(MultiImageLoader multiImageLoader){
        this.mMultiImageLoader = multiImageLoader;
    }

    public class SpinnerDataObjectHolder extends RecyclerView.ViewHolder {
        private ImageView mSpinnerView;
        public SpinnerDataObjectHolder(View itemView) {
            super(itemView);
            mSpinnerView = (ImageView) itemView.findViewById(R.id.page_spinner);
            mSpinnerView.setBackgroundResource(R.color.white);
        }
    }

    public void clearData() {
        int size = this.data.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.data.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }
}
