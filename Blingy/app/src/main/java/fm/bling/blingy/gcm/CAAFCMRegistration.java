package fm.bling.blingy.gcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;


import fm.bling.blingy.App;
import fm.bling.blingy.BuildConfig;
import fm.bling.blingy.R;
import fm.bling.blingy.gcm.model.CAAPutFCMSettings;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Chosen-pro on 6/22/15.
 */
public class CAAFCMRegistration {

//    private static final String TAG = "RegIntentService";

    public void onHandleIntent(final Context context) {

        new Thread() {
            public void run() {

                try {
                    Log.d("fcm", "registering");
                    String token = "";
                    SharedPreferences settings = context.getSharedPreferences(context.getResources().getString(R.string.user_shared_prefs), Context.MODE_MULTI_PROCESS);
                    final SharedPreferences.Editor editor = settings.edit();

                    token = FirebaseInstanceId.getInstance().getToken();
                    editor.putString(context.getResources().getString(R.string.gcm_token), token);
                    editor.apply();

                    CAAPutFCMSettings gcmSettings = new CAAPutFCMSettings(BuildConfig.VERSION_NAME, token, "fm.bling.blingy", Build.VERSION.RELEASE);

                    App.getUrlService().putFCMSettings(gcmSettings, new Callback<CAAPutFCMSettings>() {
                        @Override
                        public void success(CAAPutFCMSettings cAAGCMSettings, Response response) {
                            Log.d("fcm", "put success");
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.d("fcm", "put failure");
                        }
                    });

                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        }.start();

    }
}
