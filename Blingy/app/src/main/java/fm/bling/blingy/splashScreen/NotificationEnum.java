package fm.bling.blingy.splashScreen;

import java.util.HashMap;
import java.util.Map;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 4/19/16.
 * History:
 * ***********************************
 */
public enum NotificationEnum {
    BLINGY_DEFAULT("blingy_default",31),
    FOLLOW_AMBASSADOR("follow_ambassador",32),
    CONTENT_ADDED("content_added",33),
    TRENDING_VIDEO("trending_video",34),
    WATCH_VIDEO("watch_video",35),
    NEW_VIDEO_ADDED("new_videos_added",36),
    INTRIGUING("intriguing",37),
    NEW_FOLLOWER("new_follower",38),
    NEW_COMMENT_BACK("new_comment_back",39),
    REVIEW_VIDEO_READY("review_video_ready",40),
    D1_NOTIFICATION("D1_notification",41),
    NEW_COMMENT("new_comment",42),
    DAILY_PICK("daily_pick",43),
    NEW_PERFORMANCE("new_performance",44),
    NEW_LIKE("new_like",45),
    NEW_FOLLOW_BACK("new_follow_back",46),
    NEW_RESPONSE_FOLLOWER("new_response_follower",47),
    DAILY_PICKS_NEW("daily_picks_new",48),
    DAILY_PICKS_PLAYED("daily_picks_played",49),
    OTHER("other",50),
    NEW_RESPONSE("new_response",51),
    COMMENT_MKTG("comment_mktg",52),
    RECORD_VIDEO("record_video",53),
    VOTE_NOW("vote_now",54),
    COMMENT_NOW("comment_now",55),
    NEW_BOOT_FOLLOWER("new_bot_follower",56),
    SUGGESTED_FOLLOW("suggested_follow",57),
    USER_AMBASSADOR("user_ambassador",58),
    RECORD_AMBASSADOR("record_now",59),
    FEATURE_ME("featureme",60),
    ADD_FRIENDS("add_friends",61),
    PROFILE_AMBASSADOR("ambassador_profile",62),
    INTRO_INVITES("intro_invites",63),
    DISCOVER("discover",64),
    LEADER_CONGRATS("leaderboard_congrats",65),
    LEADER_WEEKLY("leaderboard_weekly",66),
    LEADER_MASS("leaderboard_mass",67),
    MASHUP("mashup",68),
    VIRAL_CAMEO_REQ("cameo_invite",69),
    CAMEO_CREATED("cameo_created", 70),
    DUET_SUGGESTION("duet_suggestion", 71),
    DUET_REMINDER("duet_reminder", 72);


    private String mType;
    private int mID;

    NotificationEnum(String type, int id){
        this.mType = type;
        this.mID = id;
    }

    private static Map<String, NotificationEnum> map = new HashMap<String, NotificationEnum>();

    static {
        for (NotificationEnum notificationEnum : NotificationEnum.values()) {
            map.put(notificationEnum.mType, notificationEnum);
        }
    }

    public static int getNotificationID(String type) {
        if(map.get(type) != null)
            return map.get(type).getID();
        else
            return BLINGY_DEFAULT.getID();
    }

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }

    public int getID() {
        return mID;
    }

    public void setID(int mID) {
        this.mID = mID;
    }
}
