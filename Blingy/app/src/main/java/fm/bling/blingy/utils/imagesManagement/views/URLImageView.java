package fm.bling.blingy.utils.imagesManagement.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import fm.bling.blingy.utils.imagesManagement.ImageLoaderBase;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderListener;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/10/16.
 * History:
 * ***********************************
 */
public class URLImageView extends ImageView
{
    public final static int NOT_SPECIFIED = -22;

    public String url;
    protected ImageLoaderBase mImageLoader;
    private int width = NOT_SPECIFIED;
    private int height = NOT_SPECIFIED;
    private int maxWidth = NOT_SPECIFIED;
    private boolean isClosed;
    private boolean isLocalImage;
    private boolean isRoundedImage;
    protected boolean isResourceImage;
    private boolean hasAlpha;
    private boolean isBlured;
    private boolean dontSaveToFileCache;
    private boolean dontUseExisting;
    private int placeHolder = 0;
    private int mBlurRadius = 15;
    private ImageLoaderListener mImageLoaderListener;
    private boolean keepAspectRatioAccordingToWidth = true;
    public boolean layoutChanged;

    private boolean isFixedSize;
    private boolean isAttached;

    private int mAnimResID = NOT_SPECIFIED;
    private boolean hasAnimation = false;
    private Animation mAnimation;

    private boolean isAutoShowRecycle;

    private boolean mHasMajorColorLayer = false;
    private Paint majorColorPaint;

    /**
     * Setting Res id of animation to perform.
     *
     * @param mAnimResID - pass @NOT_SPECIFIED to remove animation.
     */
    public void setAnimResID(int mAnimResID)
    {
        this.mAnimResID = mAnimResID;
        this.hasAnimation = mAnimResID != NOT_SPECIFIED;
        if (hasAnimation)
            mAnimation = AnimationUtils.loadAnimation(getContext(), mAnimResID);

    }

    public Animation getImageAnimation()
    {
        return mAnimation;
    }

    public boolean hasAnimation()
    {
        return hasAnimation;
    }

    public int getBlurRadius()
    {
        return mBlurRadius;
    }

    /**
     *
     * @param blurRadius at the moment the range is  0< radius <=25
     */
    public void setBlurRadius(int blurRadius)
    {
        isBlured = blurRadius != 0;
        this.mBlurRadius = blurRadius;
    }

    public ImageLoaderBase getImageLoaderManager()
    {
        return mImageLoader;
    }

    public ImageLoaderListener getImageLoaderListener()
    {
        return mImageLoaderListener;
    }

    public void setImageLoaderListener(ImageLoaderListener mImageLoaderListener)
    {
        this.mImageLoaderListener = mImageLoaderListener;
    }

    public URLImageView(Context context)
    {
        super(context);
    }

    public URLImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public URLImageView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

//    void init()
//    {
//       // setLayerType(LAYER_TYPE_SOFTWARE, null);
//    }

    public void setImageLoader(ImageLoaderBase imageLoader)
    {
        this.mImageLoader = imageLoader;
    }

    public void onDraw(Canvas can)
    {
        try
        {
            super.onDraw(can);
            if(mHasMajorColorLayer && majorColorPaint != null)
                can.drawRect(0, 0, can.getWidth(), can.getHeight(), majorColorPaint);
        }
        catch (Throwable e)
        {
            if (!isClosed && mImageLoader != null)
                mImageLoader.DisplayImage(this);
        }
    }

    public void draw(Canvas can)
    {
        try
        {
            super.draw(can);
            if(mHasMajorColorLayer && majorColorPaint != null)
                can.drawRect(0, 0, can.getWidth(), can.getHeight(), majorColorPaint);
        }
        catch (Throwable e)
        {
            if (!isClosed && mImageLoader != null)
                mImageLoader.DisplayImage(this);
        }
    }

    public void setImageBitmap(Bitmap imageBitmap)
    {
        isResourceImage = false;
        super.setImageBitmap(imageBitmap);
    }

    public void setImageURI(Uri imageUri)
    {
        throw new RuntimeException("Uri is not set here only through imageloader");
    }

    public void setImageResource(int resourceId)
    {
        isResourceImage = true;
        super.setImageResource(resourceId);
    }

//    @Override
//    public void setLayoutParams(ViewGroup.LayoutParams params)
//    {
//      //  width  = params.width;
//      //  height = params.height;
//        super.setLayoutParams(params);
//    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getImageHashName()
    {
        return url.hashCode() + width + (isRoundedImage ? 1 : 0) + (isBlured ? 1 : 0) + (dontUseExisting ? SystemClock.elapsedRealtimeNanos() : 0) + (dontSaveToFileCache ? 1 : 0) + "";
    }

    public void close()
    {
        isClosed = true;
        if (isResourceImage)
        {
            super.setImageDrawable(null);
        }
        else
        {
            if (getDrawable() != null)
            {
                try
                {
                    ((BitmapDrawable) getDrawable()).getBitmap().recycle();
                }
                catch (Throwable e)
                {
                }
            }
            super.setImageBitmap(null);
        }
        mImageLoaderListener = null;
    }

    public int getHeightInternal()
    {
        return height;
    }

    public void setHeightInternal(int height)
    {
        this.height = height;
    }

    public int getWidthInternal()
    {
        return width;
    }

    public void setWidthInternal(int width)
    {
        this.width = width;
    }

    public int getImageHeight()
    {
        if (getDrawable() instanceof BitmapDrawable)
        {
            return ((BitmapDrawable) getDrawable()).getBitmap().getHeight();
        }
        return getHeight();
    }

    public int getImageWidth()
    {
        if (getDrawable() instanceof BitmapDrawable)
        {
            return ((BitmapDrawable) getDrawable()).getBitmap().getWidth();
        }
        return getWidth();
    }

    public boolean isClosed()
    {
        return isClosed;
    }

//    public boolean isLocalImage()
//    {
//        return isLocalImage;
//    }

//    public void setIsLocalImage(boolean isLocalImage)
//    {
//        this.isLocalImage = isLocalImage;
//    }

    public boolean isRoundedImage()
    {
        return isRoundedImage;
    }

    public void setIsRoundedImage(boolean isRoundedImage)
    {
        this.isRoundedImage = isRoundedImage;
        this.hasAlpha = true;
    }

    public boolean hasAlpha()
    {
        return hasAlpha;
    }

    public void setHasAlpha(boolean hasAlpha)
    {
        this.hasAlpha = hasAlpha;
    }

    public boolean isBlured()
    {
        return isBlured;
    }

    public boolean isDontSaveToFileCache()
    {
        return dontSaveToFileCache;
    }

    public void setDontSaveToFileCache(boolean dontSaveToFileCache)
    {
        this.dontSaveToFileCache = dontSaveToFileCache;
    }

    public int getPlaceHolder()
    {
        return placeHolder;
    }

    /**
     * @param placeHolder -  pass 0 to remove or resourceID to activate
     */
    public void setPlaceHolder(int placeHolder)
    {
        this.placeHolder = placeHolder;
    }

    public int getMaxWidthInternal()
    {
        return maxWidth;
    }

    public void setMaxWidthInternal(int maxWidth)
    {
        this.maxWidth = maxWidth;
    }

    public boolean isKeepAspectRatioAccordingToWidth()
    {
        return keepAspectRatioAccordingToWidth;
    }

    public void setKeepAspectRatioAccordingToWidth(boolean keepAspectRatioAccordingToWidth)
    {
        this.keepAspectRatioAccordingToWidth = keepAspectRatioAccordingToWidth;
    }

    public void setLayoutWidthHeight(int width, int height)
    {
        getLayoutParams().width = width;
        getLayoutParams().height = height;
    }

    public void recycle()
    {
        if (getDrawable() != null)
        {
            try
            {
                ((BitmapDrawable) getDrawable()).getBitmap().recycle();
            }
            catch (Throwable e)
            {
            }
        }
    }

    public boolean isDontUseExisting()
    {
        return dontUseExisting;
    }

    public void setDontUseExisting(boolean dontUseExisting)
    {
        this.dontUseExisting = dontUseExisting;
    }

    public boolean isFixedSize()
    {
        return isFixedSize;
    }

    /**
     * if set to true it will strach the imaget to fit the requested width and height, by zooming in the center.
     * If the width fits, it will check if the height is at least in the size of the height that was set.
     *
     * @param fixedSize
     */
    public void setFixedSize(boolean fixedSize)
    {
        isFixedSize = fixedSize;
    }

    public void onImageLoaded(Bitmap bitmap)
    {
        mImageLoader.setImageView(this, bitmap);
        invalidate();
    }

    public boolean isAutoShowRecycle()
    {
        return isAutoShowRecycle;
    }

    public void setAutoShowRecycle(boolean autoShowRecycle)
    {
        isAutoShowRecycle = autoShowRecycle;
    }

    public void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        isAttached = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try
                {
                    Thread.sleep(100);
                    if(isAttached)
                        attcheImage();

                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void attcheImage()
    {
        if(mImageLoader != null)
            mImageLoader.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {

                    if (isAutoShowRecycle)
                    {
                        mImageLoader.DisplayImage(URLImageView.this);
                    }
                    if (majorColorPaint != null)
                        mHasMajorColorLayer = true;

                }
            });
    }

    public void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        isAttached = false;
        if(isAutoShowRecycle)
        {
            mImageLoader.recycleAndRemove(this);
        }
        mHasMajorColorLayer = false;
    }

    public void recycleOnDemand()
    {
        mImageLoader.recycleAndRemove(this);
    }

    public boolean isAttached()
    {
        return isAttached;
    }

    public void enableMajorColorLayer(boolean enable)
    {
        mHasMajorColorLayer = enable;
    }

    public void setMajorColorLayerColor(int color)
    {
        majorColorPaint = new Paint();
        majorColorPaint.setColor(color);
        majorColorPaint.setAlpha(75);
        majorColorPaint.setStyle(Paint.Style.FILL);
    }

    public boolean hasMajorColorLayer()
    {
        return mHasMajorColorLayer;
    }

}