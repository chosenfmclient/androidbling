package fm.bling.blingy.mashup.listeners;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 22/03/17.
 * History:
 * ***********************************
 */
public interface CameoInviteListener<T>  {

    void addToInvite(T contactInfo);
}
