package fm.bling.blingy.editVideo.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 9/14/16.
 * History:
 * ***********************************
 */
public class CAAEffect {

    @SerializedName("type")
    String type;

    @SerializedName("identifier")
    String identifier;

    @SerializedName("position")
    String position;

    public CAAEffect(String type, String identifier, String position){
        this.type = type;
        this.identifier = identifier;
        this.position = position;
    }

    public String getType() {
        return type;
    }

//    public void setType(String type) {
//        this.type = type;
//    }

    public String getIdentifier() {
        return identifier;
    }

//    public void setIdentifier(String identifier) {
//        this.identifier = identifier;
//    }

    public String getPosition() {
        return position;
    }

//    public void setPosition(String position) {
//        this.position = position;
//    }

    @Override
    public boolean equals(Object o) {
        return type.equalsIgnoreCase(((CAAEffect)o).getType()) &&
               identifier.equalsIgnoreCase(((CAAEffect)o).getIdentifier()) &&
               position.equalsIgnoreCase(((CAAEffect)o).getPosition());
    }
}
