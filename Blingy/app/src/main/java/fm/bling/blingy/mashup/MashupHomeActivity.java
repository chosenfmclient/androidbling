//package fm.bling.blingy.mashup;
//
//import android.app.Activity;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.res.Configuration;
//import android.graphics.Matrix;
//import android.graphics.SurfaceTexture;
//import android.media.MediaPlayer;
//import android.os.Bundle;
//import android.os.CountDownTimer;
//import android.support.v7.widget.GridLayoutManager;
//import android.support.v7.widget.PopupMenu;
//import android.support.v7.widget.RecyclerView;
//import android.util.TypedValue;
//import android.view.GestureDetector;
//import android.view.MenuItem;
//import android.view.MotionEvent;
//import android.view.Surface;
//import android.view.TextureView;
//import android.view.View;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.concurrent.TimeUnit;
//
//import fm.bling.blingy.App;
//import fm.bling.blingy.BaseActivity;
//import fm.bling.blingy.R;
//import fm.bling.blingy.dialogs.BaseDialog;
//import fm.bling.blingy.dialogs.LikeAnimationDialog;
//import fm.bling.blingy.dialogs.flag.FlagVideoDialog;
//import fm.bling.blingy.dialogs.listeners.FlagDialogListener;
//import fm.bling.blingy.dialogs.listeners.UpdateLikeTotal;
//import fm.bling.blingy.dialogs.model.CAAType;
//import fm.bling.blingy.dialogs.share.ShareDialogVideoHome;
//import fm.bling.blingy.discover.DiscoverActivity;
//import fm.bling.blingy.mashup.dialogs.MashupMapDialog;
//import fm.bling.blingy.mashup.listeners.StepsCallback;
//import fm.bling.blingy.mashup.model.MashupDetails;
//import fm.bling.blingy.mashup.model.MashupOBJ;
//import fm.bling.blingy.record.CreateVideoActivity;
//import fm.bling.blingy.record.PickMusicClipActivity;
//import fm.bling.blingy.record.Utils;
//import fm.bling.blingy.record.utils.GridSpacingItemDecoration;
//import fm.bling.blingy.rest.model.CAALike;
//import fm.bling.blingy.rest.model.CAAStatus;
//import fm.bling.blingy.singletones.CAAUserDataSingleton;
//import fm.bling.blingy.stateMachine.PermissionListener;
//import fm.bling.blingy.stateMachine.StateMachine;
//import fm.bling.blingy.stateMachine.StateMachineEvent;
//import fm.bling.blingy.tracking.TrackingManager;
//import fm.bling.blingy.utils.AdaptersDataTypes;
//import fm.bling.blingy.utils.Constants;
//import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
//import fm.bling.blingy.utils.imagesManagement.views.WhiteProgressBarView;
//import fm.bling.blingy.videoHome.VideoHomeActivity;
//import fm.bling.blingy.videoHome.VideoSocialDetailsActivity;
//import fm.bling.blingy.videoHome.model.CAAVideo;
//import retrofit.Callback;
//import retrofit.RetrofitError;
//import retrofit.client.Response;
//
///**
// * *********************************
// * Project: Blin.gy Android Application
// * Description:
// * Created by Oren Zakay on 12/18/16.
// * History:
// * ***********************************
// */
//public class MashupHomeActivity extends BaseActivity implements UpdateLikeTotal, FlagDialogListener, StepsCallback, PermissionListener, View.OnClickListener {
//
//    /**
//     * Main Views
//     */
////    private LinearLayout mTopBarContainer;
//    private ImageView mBackButton;
//    private TextView mTitlebar;
//    private ImageView mInviteFriends;
//
//    private TextView mDeletedText;
//
//    private ScaledImageView mUserPic;
//    private TextView mUserName;
//    private ImageView mMoreButton;
//
//    private FrameLayout mClipContainer;
//    private TextureView mClipTextureView;
//    private WhiteProgressBarView mProgressBar;
//    private MediaPlayer mMediaPlayer;
//    private ScaledImageView mClipImage;
//    private ImageView mPlayPauseButton;
//
//    private LinearLayout mDescContainer;
//    private TextView mTimerText;
//    private TextView mDescText;
//
//    private TextView mClipName;
//    private TextView mArtistName;
//    private TextView mShootNow;
//
//    private LinearLayout mSocialContainer;
//    private ImageView mLikeButton;
//    private TextView mLikesCount;
//    private ImageView mCommentButton;
//    private TextView mCommentsCount;
//    private ImageView mShareButton;
//    private TextView mSeeSimilar;
//
//
//    private RecyclerView mMashupRecycler;
//    private GridLayoutManager mGridLayoutManager;
//    private MashupRecyclerAdapter mMashupRecyclerAdapter;
//    private WhiteProgressBarView mProgressBarView;
//    private View mHeader;
//
//    private LikeAnimationDialog likeDialog;
//    private GestureDetector gestureDetector;
//
//    /**
//     * Utils
//     */
//    private Intent resultIntent;
//    private MashupMapDialog mMashupMap = null;
//    private CountDownTimer mCountDownTimer;
//    private MashupDetails mMashupDetails;
//    private MashupOBJ mMashupOBJ;
//    private String mMashupID;
//    private CAAVideo mRequesterVideo;
//    private CAAVideo mMashupVideo;
//    private ArrayList<CAAVideo> mVideos = new ArrayList<>();
//    private boolean isPlaying = false;
//    private boolean isPaused = false;
//    private boolean isPrepared = false;
//    private boolean isLoadingMusic = false;
//    private int orientation = Configuration.ORIENTATION_PORTRAIT;
//    private int mPos = -1;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.mashup_home);
//        mHeader = getLayoutInflater().inflate(R.layout.mashup_home_header, null);
//
//        mMashupID = getIntent().getStringExtra(Constants.MASHUP_ID);
//
//
//
//
//        mMashupDetails = StateMachine.getInstance(this).getMashupDetails(mMashupID);
//
//        if (getIntent().hasExtra(Constants.POS))
//            mPos = getIntent().getIntExtra(Constants.POS, -1);
//
//        if(getIntent().hasExtra(Constants.FROM)) {
//            switch (getIntent().getStringExtra(Constants.FROM)){
//                case Constants.INVITE_MASHUP_SCREEN:
//                    BaseDialog invitationsSent = new BaseDialog(this, getString(R.string.invitations_sent), getString(R.string.invitations_has_been_successfully_sent));
//                    invitationsSent.removeCancelbutton();
//                    invitationsSent.show();
//                    break;
//            }
//        }
//
//        resultIntent = new Intent();
//        init();
//        getMashupByID();
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//
//        if(mCountDownTimer != null)
//        mCountDownTimer.cancel();
//
//        mCountDownTimer = null;
//    }
//
//    private void getMashupByID() {
//        App.getUrlService().getMashup(mMashupID, new Callback<MashupOBJ>() {
//            @Override
//            public void success(MashupOBJ mashupOBJ, Response response) {
//                mMashupOBJ = mashupOBJ;
//                mMashupVideo = mashupOBJ.getMashupVideo();
//                mRequesterVideo = mashupOBJ.getRequesterVideo();
//                setup();
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        });
//    }
//
//    private void init() {
//        mProgressBarView = (WhiteProgressBarView) findViewById(R.id.progress_bar);mProgressBarView.updateLayout();
//        mMashupRecycler = (RecyclerView) findViewById(R.id.mashup_recycler);
//        mDeletedText = (TextView) findViewById(R.id.text_deleted_mashup);
////        mTopBarContainer = (LinearLayout) mHeader.findViewById(R.id.top_bar_container);
//        mProgressBar = (WhiteProgressBarView) mHeader.findViewById(R.id.progress_bar);mProgressBar.updateLayout();
//        mBackButton = (ImageView) mHeader.findViewById(R.id.bar_icon);
//        mTitlebar = (TextView) mHeader.findViewById(R.id.bar_title);
//        mInviteFriends = (ImageView) mHeader.findViewById(R.id.invite_friends);
//        mUserPic = (ScaledImageView) mHeader.findViewById(R.id.user_pic);
//        mUserName = (TextView) mHeader.findViewById(R.id.user_name);
//        mMoreButton = (ImageView) mHeader.findViewById(R.id.more_button);
//        mClipContainer = (FrameLayout) mHeader.findViewById(R.id.clip_container);
//        mClipTextureView = (TextureView) mHeader.findViewById(R.id.clip_texture_view);
//        mClipImage = (ScaledImageView) mHeader.findViewById(R.id.clip_image);
//        mPlayPauseButton = (ImageView) mHeader.findViewById(R.id.play_pause_button);
//        mDescContainer = (LinearLayout) mHeader.findViewById(R.id.desc_container);
//        mTimerText = (TextView) mHeader.findViewById(R.id.timer);
//        mDescText = (TextView) mHeader.findViewById(R.id.desc_text);
//        mClipName = (TextView) mHeader.findViewById(R.id.clip_name);
//        mArtistName = (TextView) mHeader.findViewById(R.id.artist_name);
//        mShootNow = (TextView) mHeader.findViewById(R.id.select_button);
//        mSocialContainer = (LinearLayout) mHeader.findViewById(R.id.social_container);
//        mLikeButton = (ImageView) mHeader.findViewById(R.id.like_button);
//        mLikesCount = (TextView) mHeader.findViewById(R.id.likes_count);
//        mCommentButton = (ImageView) mHeader.findViewById(R.id.comment_button);
//        mCommentsCount = (TextView) mHeader.findViewById(R.id.comments_count);
//        mShareButton = (ImageView) mHeader.findViewById(R.id.share_button);
//        mSeeSimilar = (TextView) mHeader.findViewById(R.id.see_similar);
//
//        mUserPic.setOnClickListener(this);
//        mUserName.setOnClickListener(this);
//        mClipName.setOnClickListener(this);
//        mArtistName.setOnClickListener(this);
//        mShootNow.setOnClickListener(this);
//        mLikeButton.setOnClickListener(this);
//        mCommentsCount.setOnClickListener(this);
//        mCommentButton.setOnClickListener(this);
//        mBackButton.setOnClickListener(this);
//        mInviteFriends.setOnClickListener(this);
//        mMoreButton.setOnClickListener(this);
//        mShareButton.setOnClickListener(this);
//        mSeeSimilar.setOnClickListener(this);
//    }
//
//    private void setup() {
//        mVideos.add(new CAAVideo());
//        mVideos.add(mRequesterVideo);
//        int columnPadding = (int) (15f * App.SCALE_Y);
//        mGridLayoutManager = new GridLayoutManager(this, 3);
//        mGridLayoutManager.setRecycleChildrenOnDetach(true);
//        mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(int position) {
//                return mMashupRecyclerAdapter.getItemViewType(position) == AdaptersDataTypes.HEADER ? mGridLayoutManager.getSpanCount() : 1;
//            }
//        });
//        mMashupRecycler.setHasFixedSize(true);
//        mMashupRecycler.setLayoutManager(mGridLayoutManager);
//        mMashupRecycler.addItemDecoration(new GridSpacingItemDecoration(3, columnPadding, false, true));
//        mMashupRecycler.setNestedScrollingEnabled(false);
//        if (mMashupOBJ.getVideos().size() > 0) {
//            mVideos.addAll(mMashupOBJ.getVideos());
//        } else {
//            Toast.makeText(this, "There is not videos", Toast.LENGTH_SHORT).show();
//        }
//
//        //This called only from push notification and this is a new mashup, we didn't have any details about that mashup.
//        if(mMashupDetails == null) {
//            mMashupDetails = new MashupDetails(mMashupID, 3, mVideos.size());
//            StateMachine.getInstance(this).insertMashup(mMashupID, 3, mVideos.size());
//        }
//
//        prepareBaseInfo();
//        switch (mMashupOBJ.getStatus()) {
//            default:
//            case Constants.STATUS_STARTED:
//                showTimer();
//                setUpRecycler();
//                break;
//            case Constants.STATUS_COMPLETED:
//                if (mMashupVideo == null) {
//                    setupProccessingMashup();
//                } else {
////                    mMashupVideo = mMashupOBJ.getMashupVideo();
//                    showVideo();
//                }
//                setUpRecycler();
//                break;
//            case Constants.STATUS_DELETED:
//                showDeleted();
//                break;
//        }
//
//        mProgressBarView.setVisibility(View.GONE);
//
//
//
//    }
//
//    public void setUpRecycler() {
//        mMashupRecyclerAdapter = new MashupRecyclerAdapter(mVideos, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CAAVideo video = ((CAAVideo) v.getTag());
//                Bundle bundle = new Bundle();
//                bundle.putParcelable(Constants.CAAVIDEO, video);
//                Intent songHome = new Intent(MashupHomeActivity.this, VideoHomeActivity.class);
//                songHome.putExtra(Constants.BUNDLE, bundle);
//                startActivity(songHome);
//            }
//        }, mHeader);
//
//        mMashupRecycler.setAdapter(mMashupRecyclerAdapter);
//        mMashupRecycler.setVisibility(View.VISIBLE);
//    }
//
//    private void showDeleted() {
//        setContentView(R.layout.mashup_deleted_layout);
//        ((TextView) findViewById(R.id.bar_title)).setText(mRequesterVideo.getSongName() + " Mashup");
//        findViewById(R.id.bar_icon).setOnClickListener(this);
//        new MashupMapDialog.MapBuilder(this, 1, this, false)
//                .setTitleRes(R.string.mashup_deleted_title)
//                .build()
//                .show();
//    }
//
//    private void prepareBaseInfo() {
//        mTitlebar.setText(mRequesterVideo.getSongName() + " Mashup");
//
//        mUserPic.setUrl(mRequesterVideo.getUser().getPhoto());
//        mUserPic.setAnimResID(R.anim.fade_in);
//        mUserPic.setIsRoundedImage(true);
//        mUserPic.setKeepAspectRatioAccordingToWidth(true);
//        mUserPic.setImageLoader(mImageLoaderManager);
//        mImageLoaderManager.DisplayImage(mUserPic);
//
//        mUserName.setText(mRequesterVideo.getUser().getFullName());
//        mClipName.setText(mRequesterVideo.getSongName());
//        mArtistName.setText(mRequesterVideo.getArtistName());
//
//        // Update the videos num.
//        mMashupDetails.setVideosNum(mVideos.size());
//        StateMachine.getInstance(this).updateVideos(mMashupID, mVideos.size());
//        if(mMashupOBJ.getStatus() == Constants.STATUS_STARTED) {
//            if (CAAUserDataSingleton.getInstance().isMe(mRequesterVideo.getUser().getUserId())) {
//                if (mVideos.size() - 1 >= 2) {
//                    mShootNow.setText(R.string.mash_up_now);
//                    // Update the step num.
//                    StateMachine.getInstance(this).updateStep(mMashupID, 4);
//                    mMashupDetails.setStep(4);
//                    mMashupMap = new MashupMapDialog.MapBuilder(this, mMashupDetails.getStep(), this, false)
//                            .setTitleRes(R.string.create_mashup)
//                            .build();
//                    mMashupMap.show();
//                } else {
//                    mShootNow.setText(R.string.add_your_video);
//                    mMashupMap = new MashupMapDialog.MapBuilder(this, mMashupDetails.getStep(), this, false)
//                            .setTitleRes(R.string.mashup_add_videos)
//                            .build();
//                    mMashupMap.show();
//                }
//            } else {
//                //Im coming from an invite and the mashup is not yet completed.
//                mMashupMap = new MashupMapDialog.MapBuilder(this, mMashupDetails.getStep(), this, false)
//                        .setTitleRes(R.string.mashup_add_videos)
//                        .setImageLoader(getMultiImageLodaer())
//                        .setImageUrl(mRequesterVideo.getUser().getPhoto())
//                        .build();
//                mMashupMap.show();
////                mMashupMap = new MashupMapDialog(this, mMashupDetails.getStep(), this, R.string.mashup_add_videos, false);
////                mMashupMap.setImageLoaderManager(getMultiImageLodaer());
////                mMashupMap.setProfileImageUrl(mRequesterVideo.getUser().getPhoto());
//
//            }
//        if((mVideos.size()-1>=3)){
//            forceCompleteMashup();
//        }
//        }
//
//    }
//
//    private void showVideo() {
//        new MashupMapDialog.MapBuilder(this, 1, this, false)
//                .setTitleRes(R.string.mashuo_is_gone_start_new)
//                .build()
//                .show();
//        mSocialContainer.setVisibility(View.VISIBLE);
//        gestureDetector = new GestureDetector(this, new GestureListener());
//
//        mClipTextureView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return gestureDetector.onTouchEvent(event);
//            }
//        });
//    }
//
//    private void showTimer() {
//        mInviteFriends.setVisibility(View.VISIBLE);
//        //Timer from server is in seconds, i need to convert it to Milliseconds
//        mCountDownTimer = new CountDownTimer(mMashupOBJ.getTimer() * 1000, 60 * 1000) {
//            @Override
//            public void onTick(long millisUntilFinished) {
//                long hr = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
//                long min = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished - TimeUnit.HOURS.toMillis(hr));
//                mTimerText.setText(String.format("%02d:%02d", hr, min));
//            }
//
//            @Override
//            public void onFinish() {
//                mTimerText.setText("00:00");
//                if(mVideos.size() >= 2)
//                    onFourthStepClicked();
//                else {
//                    //TODO ask Katya about
//                    App.getUrlService().deleteVideo(mMashupVideo.getVideoId(), new Callback<String>() {
//                        @Override
//                        public void success(String s, Response response) {
//                            resultIntent.putExtra(Constants.REMOVE_POS, mPos);
//                            setResult(Activity.RESULT_OK, resultIntent);
//                            finish();
//                        }
//
//                        @Override
//                        public void failure(RetrofitError error) {
//
//                        }
//                    });
//                }
//            }
//        }.start();
//    }
//
//    private void setupProccessingMashup() {
//        ((FrameLayout.LayoutParams) mDescContainer.getLayoutParams()).topMargin = 0;
//        ((FrameLayout.LayoutParams) mDescContainer.getLayoutParams()).bottomMargin = 0;
//        mDescContainer.getLayoutParams().height = App.landscapitemHeight;
//
//        mDescText.setText(R.string.processing_mashup);
//        mDescText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
//        mTimerText.setText(R.string.we_will_notify_you_when_ready);
//        mTimerText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
//        mShootNow.setText(R.string.shoot_now);
//        MashupMapDialog map = new MashupMapDialog.MapBuilder(this, 1, this, false)
//                                    .setTitleRes(R.string.mashuo_is_gone_start_new)
//                                    .build();
//        map.show();
//    }
//
//    @Override
//    public void onFinishFlagVideoDialog(boolean success) {
//        BaseDialog dialog = new BaseDialog(this, "Blingy", "Thank you for the report");
//        dialog.removeCancelbutton();
//        dialog.show();
//    }
//
//    @Override
//    public void onFirstStepClicked() {
//        Intent intent = new Intent(this, PickMusicClipActivity.class);
//        startActivity(intent);
//    }
//
//    @Override
//    public void onSecondStepClicked() {
//        moveToInviteFriends();
//    }
//
//    @Override
//    public void onThirdStepClicked() {
//        createVideo(true);
//    }
//
//    @Override
//    public void onFourthStepClicked() {
//        forceCompleteMashup();
//    }
//
//    @Override
//    public void onShareStepClicked() {
//
//    }
//
//    @Override
//    public void onPermissionGranted() {
//        switch (mMashupDetails.getStep()) {
//            case MashupMapDialog.INVITE_FRIENDS:
//                findViewById(R.id.invite_friends).callOnClick();
//                break;
//        }
//    }
//
//    @Override
//    public void onPermissionDenied() {
//
//    }
//
//    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
//
//        @Override
//        public boolean onSingleTapConfirmed(MotionEvent e) {
//            handleSingleTap();
//            return super.onSingleTapConfirmed(e);
//        }
//
//        @Override
//        public boolean onDown(MotionEvent e) {
//            return true;
//        }
//
//        @Override
//        public boolean onDoubleTap(MotionEvent e) {
//            handleDoubleTap();
//            return true;
//        }
//    }
//
//    private void handleSingleTap() {
//        if (!isLoadingMusic) {
//            if (mMediaPlayer != null) {
//                if (isPlaying) {
//                    isPlaying = false;
//                    isPaused = true;
//                    mMediaPlayer.pause();
//                    mPlayPauseButton.bringToFront();
//                    mPlayPauseButton.setVisibility(View.VISIBLE);
////                    if(orientation == Configuration.ORIENTATION_LANDSCAPE)
////                        handleContent(false);
//
//                } else {
////                    if(orientation == Configuration.ORIENTATION_LANDSCAPE)
////                        handleContent(true);
//                    if (isPrepared) {
//                        isPaused = false;
//                        isPlaying = true;
//                        mMediaPlayer.start();
//                        mClipImage.setVisibility(View.INVISIBLE);
//                        mPlayPauseButton.setVisibility(View.GONE);
//                    } else
//                        MusicPrepare();
//                }
//            } else {
//                MusicPrepare();
//            }
//        }
//    }
//
//    private void handleDoubleTap() {
//        int[] pos = new int[2];
//        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            pos[0] = App.HEIGHT / 2;
//            pos[1] = App.WIDTH / 2;
//        } else {
//            mClipTextureView.getLocationOnScreen(pos);
//        }
//
//        likeAnimation(pos);
//    }
//
//    private void likeAnimation(int[] pos) {
//        if (likeDialog != null)
//            likeDialog.dismiss();
//        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
//            pos[0] += (App.landscapItemWidth / 2);
//            pos[1] += (App.landscapitemHeight / 2);
//        } else {
//            pos[0] = (App.HEIGHT / 2);
//            pos[1] = (App.WIDTH / 2);
//        }
//        likeDialog = new LikeAnimationDialog(this, mMashupVideo, pos, this, -1);
//        likeDialog.show();
//    }
//
//    private void likeVideo(final CAAVideo video) {
//        CAAType type = new CAAType("home");
//        App.getUrlService().postLikeVideo(video.getVideoId(), type, new Callback<CAALike>() {
//            @Override
//            public void success(CAALike like, Response response) {
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                video.setLikedByMe(0);
//                video.setTotalLikes(video.getTotalLikes() - 1);
//            }
//        });
//    }
//
//    private void unLikeVideo(final CAAVideo video) {
//        App.getUrlService().deleteLikeVideo(video.getVideoId(), new Callback<String>() {
//            @Override
//            public void success(String s, Response response) {
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                video.setLikedByMe(1);
//                video.setTotalLikes(video.getTotalLikes() + 1);
//            }
//        });
//    }
//
//    private void followUser(String userID) {
//        App.getUrlService().putsUsersFollow(userID, "", new Callback<String>() {
//            @Override
//            public void success(String s, Response response) {
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//            }
//        });
//        StateMachine.getInstance(getApplicationContext()).insertEvent(
//                new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL,
//                        StateMachineEvent.EVENT_TYPE.FOLLOW_USER.ordinal(),
//                        Integer.parseInt(userID)
//                )
//        );
//    }
//
//    private void unFollowUser(String userID) {
//        App.getUrlService().deleteUsersFollow(userID, new Callback<String>() {
//            @Override
//            public void success(String s, Response response) {
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//            }
//        });
//    }
//
//    private void attachSurface() {
//        try {
//            if (mMediaPlayer == null) {
//                MusicPrepare();
//                return;
//            }
//
//            if (mClipTextureView.isAvailable()) {
//                mMediaPlayer.setSurface(new Surface(mClipTextureView.getSurfaceTexture()));
////            if (isPrepared && !isPlaying)
////                playMusic();
//            } else {
//                mClipTextureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
//                    @Override
//                    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
//                        if (mMediaPlayer != null) {
//                            mMediaPlayer.setSurface(new Surface(mClipTextureView.getSurfaceTexture()));
//                        }
//                    }
//
//                    @Override
//                    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
//                    }
//
//                    @Override
//                    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
//                        return false;
//                    }
//
//                    @Override
//                    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
//                    }
//                });
//            }
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }
//    }
//
//    private void adjustTextureViewSize() {
//        if (mClipTextureView == null || mMediaPlayer == null)
//            return;
//
//        float viewWidth = App.WIDTH;
//        float viewHeight = App.WIDTH * ((float) 3 / 4);
//        //Set it Top Centered.
//        int yoff = 0;
//
//        double aspectRatio;
//        try {
//            aspectRatio = ((double) mMediaPlayer.getVideoHeight()) / ((double) mMediaPlayer.getVideoWidth());
//        } catch (Throwable err) {
//            err.printStackTrace();
//            return;
//        }
//
//        final float newWidth, newHeight;
//
//        if (mMediaPlayer.getVideoWidth() == mMediaPlayer.getVideoHeight()) {
//            newWidth = viewWidth;
//            newHeight = (int) (viewWidth * aspectRatio);
//        } else if (mMediaPlayer.getVideoWidth() < mMediaPlayer.getVideoHeight()) {
//            // limited by narrow width; restrict height
//            newWidth = viewWidth;
//            newHeight = (int) (viewWidth * aspectRatio);
//        } else {
//            // limited by short height; restrict width
//            newWidth = (int) (viewHeight / aspectRatio);
//            newHeight = viewHeight;
//        }
//
//        int xoff = (int) ((viewWidth - newWidth) / 2f);
//        Matrix txform = new Matrix();
//        mClipTextureView.getTransform(txform);
//        txform.postTranslate(xoff, yoff);
//        txform.setScale(newWidth / viewWidth, newHeight / viewHeight, (int) ((viewWidth) / 2f), (int) ((viewHeight) / 2f)); // center crop
//
//        mClipTextureView.setTransform(txform);
//        mClipTextureView.requestLayout();
//
//    }
//
//    private void MusicPrepare() {
//
//        if (isLoadingMusic || !isActivityVisible || mMashupVideo == null)
//            return;
//
//        if (mMediaPlayer == null)
//            mMediaPlayer = new MediaPlayer();
//
//        TrackingManager.getInstance().videoPlay("video_home", mMashupVideo.getVideoId(), mMashupVideo.getArtistName(), mMashupVideo.getSongName(), "not_supported_by_server", -1);
//
//        isLoadingMusic = true;
//        isPrepared = false;
//        isPlaying = false;
//        // Set type to streaming
//        mProgressBar.setVisibility(View.VISIBLE);
//        mPlayPauseButton.setVisibility(View.GONE);
//
//        attachSurface();
//        // Listen for if the audio file can't be prepared
//        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
//            @Override
//            public boolean onError(MediaPlayer mp, int what, int extra) {
//                releaseMediaPlayer();
//                mProgressBar.setVisibility(View.GONE);
//                mClipImage.setVisibility(View.VISIBLE);
//                return false;
//            }
//        });
//        // Attach to when audio file is prepared for playing
//        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                isPrepared = true;
//                adjustTextureViewSize();
//                playMusic();
//            }
//        });
//
//        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mp) {
//                if (mp != null && isActivityVisible)
//                    mp.start();
//            }
//        });
//
//        mMediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
//            @Override
//            public boolean onInfo(MediaPlayer mp, int what, int extra) {
//                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
//                    mp.setOnInfoListener(null);
//                    if (mClipImage != null) {
//                        mClipImage.setVisibility(View.GONE);
////                        adjustTextureViewSize();
//                    }
//                    mProgressBar.setVisibility(View.GONE);
//                }
//                return true;
//            }
//        });
//
//        // Set the data source to the remote URL
//        // Trigger an async preparation which will file listener when completed
//        try {
////            mMediaPlayer.setLooping(true);
//            mMediaPlayer.setDataSource(mMashupVideo.getUrl());
//            mMediaPlayer.prepareAsync();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void releaseMediaPlayer() {
//        if (mMediaPlayer != null) {
//            mMediaPlayer.stop();
//            mMediaPlayer.reset();
//            mMediaPlayer = null;
//            isPlaying = false;
//            isLoadingMusic = false;
//            isPrepared = false;
//        }
//    }
//
//    private void playMusic() {
//        isPaused = false;
//        mMediaPlayer.start();
//        isLoadingMusic = false;
//        mClipImage.setVisibility(View.INVISIBLE);
//        mPlayPauseButton.setVisibility(View.GONE);
//        isPlaying = true;
//    }
//
//    @Override
//    public void onUpdateLikeTotal(int pos) {
//        mLikesCount.setText(mRequesterVideo.getTotalLikes() + "");
//        mLikeButton.setImageResource(R.drawable.ic_liked_24dp);
//    }
//
//    private void openMoreOption(View v) {
//        final String userID = mRequesterVideo.getUser().getUserId();
//        final boolean followedByMe = mRequesterVideo.getUser().getFollowedByMe().equalsIgnoreCase("1");
//
//        if (userID != null) {
//            PopupMenu popup = new PopupMenu(this, v);
//            popup.getMenuInflater().inflate(R.menu.video_home_popup_menu, popup.getMenu());
//            if (CAAUserDataSingleton.getInstance().isMe(userID)) {
//                popup.getMenu().findItem(R.id.action_flag_video).setVisible(false);
//                if (!mMashupVideo.getStatus().equalsIgnoreCase("secret")) {
//                    if (mMashupVideo.getStatus().equalsIgnoreCase("private"))
//                        popup.getMenu().findItem(R.id.action_private).setTitle("Make public").setVisible(true);
//                    else
//                        popup.getMenu().findItem(R.id.action_private).setTitle("Make private").setVisible(true);
//                }
//                popup.getMenu().findItem(R.id.action_delete).setVisible(true);
//                popup.getMenu().findItem(R.id.action_follow).setVisible(false);
//            } else {
//                if (followedByMe) {
//                    popup.getMenu().findItem(R.id.action_follow).setTitle("Unfollow " + mRequesterVideo.getUser().getFullName()).setVisible(true);
//                } else {
//                    popup.getMenu().findItem(R.id.action_follow).setTitle("Follow " + mRequesterVideo.getUser().getFullName()).setVisible(true);
//                }
//            }
//            popup.setOnMenuItemClickListener(
//                    new PopupMenu.OnMenuItemClickListener() {
//                        public boolean onMenuItemClick(MenuItem item) {
//                            switch (item.getItemId()) {
//                                case R.id.action_flag_video:
//                                    showFlagVideoDialog();
//                                    return true;
//                                case R.id.action_delete:
//                                    deleteVideo();
//                                    return true;
//                                case R.id.action_private:
//                                    if (mMashupVideo.getStatus().equalsIgnoreCase("public"))
//                                        uploadVideoStatus("private");
//                                    else
//                                        uploadVideoStatus("public");
//                                    return true;
//                                case R.id.action_follow:
//                                    if (followedByMe) {
//                                        mRequesterVideo.getUser().setFollowedByMe("0");
//                                        unFollowUser(userID);
//                                    } else {
//                                        mRequesterVideo.getUser().setFollowedByMe("1");
//                                        followUser(userID);
//                                    }
//                                    return true;
//                                default:
//                                    return true;
//                            }
//                        }
//                    }
//            );
//            popup.show();
//        }
//    }
//
//    private void showFlagVideoDialog() {
//        FlagVideoDialog flagVideoDialog = new FlagVideoDialog(this, mRequesterVideo.getVideoId(), this);
//        flagVideoDialog.show();
//    }
//
//    private void deleteVideo() {
//        BaseDialog dialog = new BaseDialog(this, "Are you sure?", "Are you sure you want to delete this " + mRequesterVideo.getMediaType() + "?");
//        dialog.setOKText("DELETE");
//        dialog.show();
//        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialogInterface) {
//                if (((BaseDialog) dialogInterface).getAnswer()) {
//                    App.getUrlService().deleteVideo(mMashupVideo.getVideoId(), new Callback<String>() {
//                        @Override
//                        public void success(String s, Response response) {
//                            resultIntent.putExtra(Constants.REMOVE_POS, mPos);
//                            setResult(Activity.RESULT_OK, resultIntent);
//                            finish();
//                        }
//
//                        @Override
//                        public void failure(RetrofitError error) {
//
//                        }
//                    });
//                }
//            }
//        });
//    }
//
//    private void uploadVideoStatus(final String status) {
//        App.getUrlService().putUploadVideoStatus(mMashupVideo.getVideoId(), new CAAStatus(status), new Callback<CAAVideo>() {
//            @Override
//            public void success(CAAVideo caaVideo, Response response) {
//                mMashupVideo.setStatus(caaVideo.getStatus());
//                resultIntent.putExtra(Constants.POS, mPos);
//                resultIntent.putExtra(Constants.STATUS, status);
//                setResult(Activity.RESULT_OK, resultIntent);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//            }
//        });
//    }
//
//    private void moveToSocial(boolean isLikeScreen){
//        Intent socialLike = new Intent(this, VideoSocialDetailsActivity.class);
//        socialLike.putExtra(Constants.VIDEO_ID, mMashupVideo.getVideoId());
//        socialLike.putExtra(Constants.CLIP_NAME, mMashupVideo.getSongName());
//        socialLike.putExtra(Constants.CLIP_THUMBNAIL, mMashupVideo.getFirstFrame());
//        socialLike.putExtra(Constants.MEDIA_TYPE, mMashupVideo.getMediaType());
//        socialLike.putExtra(Constants.SELECTED_TAB, isLikeScreen ? VideoSocialDetailsActivity.LIKES_POS : VideoSocialDetailsActivity.COMMENTS_POS);
//        startActivity(socialLike);
//    }
//
//    private void moveToInviteFriends(){
//        if(getContactsPermissions(this)) {
//            Intent mashupInvites = new Intent(this, InviteFriendsMashupActivity.class);
//            mashupInvites.putExtra(Constants.MASHUP_ID, mMashupID);
//            startActivity(mashupInvites);
//        }
//    }
//
//    private void createVideo(boolean isChild){
//        Intent intent = new Intent(this, CreateVideoActivity.class);
//        if(isChild)
//            intent.putExtra(Constants.PARENT_ID, mMashupOBJ.getParentID());
//        intent.putExtra(Constants.CLIP_URL, mRequesterVideo.getSongClipUrl());
//        intent.putExtra(Constants.CLIP_THUMBNAIL, mRequesterVideo.getSongImageUrl());
//        intent.putExtra(Constants.SONG_NAME, mRequesterVideo.getSongName());
//        intent.putExtra(Constants.ARTIST_NAME, mRequesterVideo.getArtistName());
//        intent.putExtra(Constants.MASHUP_ID,mMashupOBJ.getMashupID());
//        getRecordPermissions(intent);
//    }
//
//    private void forceCompleteMashup(){
//        setupProccessingMashup();
//        App.getUrlService().putMashupForceComplete(mMashupID, "",new Callback<String>() {
//            @Override
//            public void success(String s, Response response) {
//                Toast.makeText(MashupHomeActivity.this,"force completed succeed",Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Toast.makeText(MashupHomeActivity.this,"force completed failed ",Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    @Override
//    public void onClick(View view) {
//        if (Utils.canClick()) {
//            switch (view.getId()) {
//                case R.id.artist_name:
//                case R.id.clip_name:
//                case R.id.see_similar:
//                    Intent discover = new Intent(this, DiscoverActivity.class);
//                    discover.putExtra(Constants.IS_SONG_HOME, true);
//                    discover.putExtra(Constants.VIDEO_ID, mMashupVideo);
//                    startActivity(discover);
//                    break;
//                case R.id.share_button:
//                    ShareDialogVideoHome shareDialog = new ShareDialogVideoHome(this, this, mMashupVideo, false);
//                    shareDialog.show();
//                    break;
//                case R.id.comment_button:
//                case R.id.comments_count:
//                    moveToSocial(false);
//                    break;
//                case R.id.like_button:
//                    if (mMashupVideo.getLikedByMe() == 1) {
//                        mLikeButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_like_24dp));
//                        mMashupVideo.setLikedByMe(0);
//                        mMashupVideo.setTotalLikes(mMashupVideo.getTotalLikes() - 1);
//                        if (mLikesCount != null) {
//                            mLikesCount.setText(String.valueOf(mMashupVideo.getTotalLikes()));
//                        }
//                        unLikeVideo(mMashupVideo);
//                    } else {
//                        mLikeButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_liked_24dp));
//                        mMashupVideo.setLikedByMe(1);
//                        mMashupVideo.setTotalLikes(mMashupVideo.getTotalLikes() + 1);
//                        if (mLikesCount != null) {
//                            mLikesCount.setText(String.valueOf(mMashupVideo.getTotalLikes()));
//                        }
//                        likeVideo(mMashupVideo);
//                        int[] pos = new int[2];
//                        mClipTextureView.getLocationOnScreen(pos);
//                        likeAnimation(pos);
//                    }
//                    break;
//                case R.id.likes_count:
//                    moveToSocial(true);
//                    break;
//                case R.id.more_button:
//                    openMoreOption(view);
//                    break;
//                case R.id.invite_friends:
//                    moveToInviteFriends();
//                    break;
//                case R.id.bar_icon:
//                    onBackPressed();
//                    break;
//                case R.id.select_button:
//                    switch (mShootNow.getText().toString()) {
//                        case "+ Add Your Video":
//                            createVideo(true);
//                            break;
//                        case "Mash Up Now":
//                            forceCompleteMashup();
//                            break;
//                        case "Shoot Now":
//                            Intent intent = new Intent(this, PickMusicClipActivity.class);
//                            startActivity(intent);
//                            break;
//                        case "New Mashup":
//                            //TODO ask Katya
//                            break;
//                    }
//                    break;
//            }
//        }
//    }
//}
