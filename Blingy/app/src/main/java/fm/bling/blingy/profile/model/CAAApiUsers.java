package fm.bling.blingy.profile.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import fm.bling.blingy.rest.model.User;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAApiUsers
 * Description:
 * Created by Dawidowicz Nadav on 5/6/15.
 * History:
 * ***********************************
 */
public class CAAApiUsers {

    @SerializedName("data")
    private ArrayList<User> data;

    public ArrayList<User> getData() {
        return data;
    }

    public void setData(ArrayList<User> data) {
        this.data = data;
    }
}
