//package fm.bling.blingy.registration;
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.os.SystemClock;
//import android.util.Log;
//
//import fm.bling.blingy.App;
//import fm.bling.blingy.R;
//import fm.bling.blingy.registration.rest.model.CAAPostAnonymousToken;
//import fm.bling.blingy.registration.rest.model.CAAToken;
//import fm.bling.blingy.singletones.CAAUserDataSingleton;
//import fm.bling.blingy.utils.Constants;
//import retrofit.Callback;
//import retrofit.RetrofitError;
//import retrofit.client.Response;
//
///**
// * Created by Chosen-pro on 6/22/15.
// */
//public class CAAAnonymousLogin {
//    private static CAAUserDataSingleton userDataSingleton =  CAAUserDataSingleton.getInstance();
//    public static void AnonymousLogin(final Context context, final String uid, final CAALoginListener cAALogin)
//    {
//        //make API call and get token
//
//        CAAPostAnonymousToken postToken = new CAAPostAnonymousToken(uid, Constants.APP_ID, Constants.APP_SECRET);
//        postToken.setAuto(CAALogin.isAuto);
//        App.getUrlService().postAnonymousToken(postToken, new Callback<CAAToken>() {
//            @Override
//            public void success(CAAToken cAAToken, Response response) {
//
//                String accessToken = cAAToken.getAccessToken();
//                userDataSingleton.setAccessExpireTime(cAAToken.getExpiresIn());
//                updateSingleton(accessToken, uid, cAAToken.getUserId());
//                Log.d("CAAPostAnonymousToken", accessToken);
//                cAALogin.didFinishLogin(true, response.getStatus());
////                sendDataToMat(cAAToken.getUserId());
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                if (error.getResponse() != null) {
//                    cAALogin.didFinishLogin(false, error.getResponse().getStatus());
//                }
//            }
//        });
//    }
//
//    private static void updateSingleton(String accessToken, String uid, String userId)
//    {
//        CAAUserDataSingleton.getInstance().setAccessToken(accessToken);
//        if (!uid.isEmpty()) {
//            CAAUserDataSingleton.getInstance().setDeviceIdentifier(uid);
//        }
//        if (!userId.isEmpty()) {
//            CAAUserDataSingleton.getInstance().setUserId(userId);
//        }
//        CAAUserDataSingleton.getInstance().setAccountType("anonymous");
//    }
//}
