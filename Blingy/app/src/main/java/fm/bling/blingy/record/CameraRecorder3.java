package fm.bling.blingy.record;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.opencv.android.*;
import org.opencv.core.Mat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import fm.bling.blingy.App;
import fm.bling.blingy.ProcessImage;
import fm.bling.blingy.dialogs.messages.HelpUsFixItDialogII;
import fm.bling.blingy.record.audio.AudioPlayer;
import fm.bling.blingy.record.audio.AudioRecorder;
import fm.bling.blingy.record.video.VideoRecorder;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.FFMPEG;
import fm.bling.blingy.utils.Tic;

/**
 * *********************************
 * Project: Chosen Android Application. All rights reserved.
 * Description:
 * Created by Ben Levi on 3/13/16.
 * History:
 * ***********************************
 */
public class CameraRecorder3 extends View
{
    private static final String TAG = "CameraRecorder3";
    public final static int WIDTH = 480, HEIGHT = 360;// straight current resolution 29Dec2016, FPS  = 25;
    private String audioFileName;
    private final String videoOutputPath;
    private final String videoInputPath;

//    private String mFrameFileName = "";//App.VIDEO_WORKING_FOLDER + "video/" +"frame0001.jpg";
    private String mFrameFileName = "";//Environment.getExternalStorageDirectory().getPath() + "/videotest/frames/" +"frame0001.jpg";

    private CreateVideoActivity mContext;

    private long mVideoMaxLength;
    private long elpasedTime, lastTime;

    private JavaCameraView mOpenCvCameraView;
    private Camera2ViewInterface mCamera2View = null;
    private Camera1View mCamera1View = null;

    private Surface mSurface = null;

    private boolean isRecording;
    private boolean isFirstTimeAfterPause = true;
    private boolean isFirstTimeAfterStart;
    private boolean isPaused;
    public volatile boolean isStopped;

//    private ExecutorService saveFileExecutorService;

    private VideoRecorder videoRecorder;
    private AudioRecorder audioRecorder;
    private AudioPlayer audioPlayer;
    private VideoManager videoManager;

//    private ExecutorService oldExecutorService;

    private OnProcessingVideoListener mOnProcessingVideoListener;
    private AudioRecorder.OnDoneDownloadingListener mOnDoneDownloadingListener;
    private CreateVideoActivity.ActivateDeactivateButtonsDispatcher mActivateDeactivateButtons;

    public boolean goDetect;
    private boolean isDetectingNow, isTurningOffAdjustments;

    private String mSoundTrackUrl;

    private boolean isCamera2;

    private FrameLayout mParentView;

    private boolean useLastCreatedFile = false;

    public CameraRecorder3(CreateVideoActivity context, int videoMaxLength, String videoOutputPath, FrameLayout parentView, int cameraID, String soundTrackUrl, boolean useLastCreatedFile, OnProcessingVideoListener onProcessingVideoListener)
    {
        super(context);
        mContext = context;
        this.useLastCreatedFile = useLastCreatedFile;
        isCamera2 = FFMPEG.getInstance(context).getIsCamera2();

        mParentView = parentView;

        mVideoMaxLength = videoMaxLength - 1;
        mOnProcessingVideoListener = onProcessingVideoListener;
        this.videoOutputPath = videoOutputPath;

//        if (soundTrackUrl == null)
//            audioFileName = "audio.pcm";
//        else
            audioFileName = "audio.wav";

        videoInputPath = App.VIDEO_WORKING_FOLDER + "/frames/";
        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        init(parentView, soundTrackUrl, cameraID);
    }

    protected void init(FrameLayout parentView, String soundTrackUrl, int cameraID)
    {
        videoRecorder = new VideoRecorder(mContext);
        videoRecorder.emptyWorkingFolder();

        if(isCamera2)
            initCamera2(parentView, cameraID);
        else
        {
//            initCamera(parentView, cameraID);
            initCamera1(parentView, cameraID);
//            saveFileExecutorService = Executors.newFixedThreadPool(1);
        }



        mSoundTrackUrl = soundTrackUrl;
//        initAudioRecorder();

    }

    public void initAudioRecorder()
    {
        audioRecorder = new AudioRecorder(mContext, App.VIDEO_WORKING_FOLDER + audioFileName, mSoundTrackUrl, useLastCreatedFile, new AudioRecorder.OnDoneDownloadingListener()
        {
            @Override
            public void onDone(String downloadedFilePath, String downloadedFilePathFast, String downloadedFilePathSlow)
            {
                if (audioRecorder.errorCode != 0 || isSomethingWentWrong(audioRecorder.framesFilesPaths + "frame0001.jpg"))
                {
//                    try{System.out.println(audioRecorder.errorCode+">>>>ffmpeg>>"+ FFMPEG.getInstance(null).getError());}catch (Throwable err){}
                    mContext.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            HelpUsFixItDialogII helpUsFixItDialogII = new HelpUsFixItDialogII(mContext, new OnClickListener()
                            {
                                @Override
                                public void onClick(View view)
                                {
                                    TrackingManager.getInstance().trackCameraError(JavaCameraView.errorMessage);
                                }
                            });

                            helpUsFixItDialogII.setOnDismissListener(new DialogInterface.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    try
                                    {
                                        mContext.finish();
                                    }
                                    catch (Throwable err) { }
                                }
                            });

                            helpUsFixItDialogII.show();
                        }
                    });

                    return;
                }

                String answer = downloadedFilePath;
//                long duration = FFMPEG.getInstance(mContext).getFileDuration(downloadedFilePath, null);
//                mContext.setTimeSegments(duration);

                mContext.setTimeSegments(FFMPEG.getInstance(mContext).clipDuration * 1000);

                audioFileName = downloadedFilePath;
                mFrameFileName = audioRecorder.framesFilesPaths + "frame0001.jpg";

                if (audioPlayer == null)
                {
                    try
                    {
                        audioPlayer = new AudioPlayer(mContext, downloadedFilePath, downloadedFilePathFast, downloadedFilePathSlow);
                    }
                    catch (Throwable err)
                    {
                        err.printStackTrace();
                        answer = null;
                    }
                    initVideoManager(audioRecorder.framesFilesPaths);
                    if (mOnDoneDownloadingListener != null)
                        mOnDoneDownloadingListener.onDone(answer, downloadedFilePathFast, downloadedFilePathSlow);
                }
                else
                {
                    audioPlayer.setTracks(downloadedFilePath, downloadedFilePathFast, downloadedFilePathSlow);
                }
            }

            @Override
            public void onFirstFrameReady()
            {
                mFrameFileName = audioRecorder.framesFilesPaths + "frame0001.jpg";
            }

            @Override
            public void updateProgress(int progress)
            {
                if (mOnDoneDownloadingListener != null)
                    mOnDoneDownloadingListener.updateProgress(progress);
            }
        });
    }

    public void initVideoManager(String videoFilePath)
    {
        if(videoManager != null)
            return;

        videoManager = new VideoManager(videoFilePath, new VideoManager.VideoManagerListener()
        {
            @Override
            public void onFrameReady(String frameFileName) {   mFrameFileName = frameFileName;  }

            @Override
            public void onVideoReady(VideoManager videoManager) {}

            @Override
            public void onVideoError(String errMsg) {}

            @Override
            public void onVideoEnded()
            {
//                videoManager.reset();
//                videoManager.start();
            }

        });
//        mFrameFileName = audioRecorder.framesFilesPaths +"frame0001.jpg";
    }

    private int isValidBackground = ProcessImage.DetectionResponseSuccess;
    private int findChromaKeyCounter;
    private final static int numberOfFramesToFind = 45;
    private boolean isInFindChromaKeys = false;

    final int numberOfFramesBe4RunGc = 240;
    int framesToRunGcCounter = numberOfFramesBe4RunGc;

//    private synchronized void initCamera(FrameLayout parentView, int cameraID)
//    {
//        mOpenCvCameraView = new JavaCameraView(parentView.getContext(), cameraID);//JavaCameraView) findViewById(R.id.jcv);
//        mOpenCvCameraView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
////        mOpenCvCameraView.setMaxFrameSize(480, 640);
//        ProcessImage.getInstance().setCameraRotation(mOpenCvCameraView.getCameraRotation(), mOpenCvCameraView.isMirrored());
//        ProcessImage.getInstance().turnOnFaceDetection(mContext);
//
//        mOpenCvCameraView.setOnCameraStartingListener(new JavaCameraView.OnCameraStartingListener()
//        {
//            @Override
//            public void onCameraStarting(Camera camera) { }
//
//            @Override
//            public void onSetCameraRes(int cameraRes, int cameraWidth, int cameraHeight)
//            {
//                ProcessImage.getInstance().setCameraRes(cameraRes, FFMPEG.getInstance(mContext).isLiteVideo(), cameraHeight, cameraWidth);
//            }
//        });
//
//        mOpenCvCameraView.setErrorOnSpanClickListener(new OnClickListener()
//        {
//            @Override
//            public void onClick(View view)
//            {
//                TrackingManager.getInstance().trackCameraError(JavaCameraView.errorMessage);
//            }
//        });
//
//        mOpenCvCameraView.setCvCameraViewListener(new CameraBridgeViewBase.CvCameraViewListener2()
//        {
//                @Override
//                public void onCameraViewStarted(int width, int height) { }
//
//                @Override
//                public void onCameraViewStopped() { }
//
//                @Override
//                public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame)
//                {
//                    if(--framesToRunGcCounter == 0)
//                    {
//                        runGc();
//                    }
//                    Mat inM = inputFrame.rgba();
//
//                    if (isRecording)
//                    {
//                        ProcessImage.getInstance().multiDetection(inM.nativeObj, mFrameFileName, new File(mFrameFileName).exists(), false, "");//, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom);
//
//                        if (isFirstTimeAfterPause)
//                        {
//                            isFirstTimeAfterPause = false;
//                            lastTime = SystemClock.elapsedRealtime();
//
//                            try
//                            {
//                                startRecNow();
//                            }catch (Throwable err){err.printStackTrace();}
//                        }
//
//                        testFps();
//                        elpasedTime += (SystemClock.elapsedRealtime() - lastTime);
//                        if (elpasedTime > mVideoMaxLength)
//                        {
//                            stop();
//                        }
//                        lastTime = SystemClock.elapsedRealtime();
//
//
//                        Bitmap recordBitmap = Bitmap.createBitmap(inM.cols(), inM.rows(), Bitmap.Config.ARGB_8888);
//                        Utils.matToBitmap(inM, recordBitmap);
//                        saveFileExecutorService.execute(new SaveFileThread(recordBitmap, SystemClock.elapsedRealtimeNanos()));
//                    }
//                    else if (isFirstTimeAfterStart)
//                    {
//                        ProcessImage.getInstance().multiDetection(inM.nativeObj , mFrameFileName, mFrameFileName != null && mFrameFileName.length() > 0, false, "");//, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom);
//
//                        pauseRecNow();
//                        if (isStopped)
//                            return inM;
//                    }
//                    else if(goDetect && !isInFindChromaKeys)
//                    {
//                        if(isTurningOffAdjustments)
//                        {
//                            ProcessImage.getInstance().getResizedPicture(inM.nativeObj);//ProcessImage.getInstance().multiDetection(inM.nativeObj , mFrameFileName, mFrameFileName != null && mFrameFileName.length() > 0, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom);
//                            isTurningOffAdjustments = false;
//                            mOpenCvCameraView.turnOffImageAdjustments(new JavaCameraView.onImageAdjustmentsOff()
//                            {
//                                @Override
//                                public void onActionDone(int exposureCompensation, int minExposure, int maxExposure)
//                                {
//                                    isDetectingNow = true;
//                                }
//                            });
//                        }
//                        else if(isDetectingNow && !inLightingTest)
//                        {
//                            findChromaKeyCounter++;
////                            if(findChromaKeyCounter == 1) //  ProcessImage.getInstance().checkScreen(inM.getNativeObjAddr()) &&
////                            {
////                                inLightingTest = true;
////                                ProcessImage.getInstance().getResizedPicture(inM.nativeObj);
////                                mOpenCvCameraView.getExposureLevel(new JavaCameraView.ExposureListener()
////                                {
////                                    @Override
////                                    public void onExposureReceived(double exposureTime)
////                                    {
////                                        isValidBackground = isValidExposure(exposureTime);
////                                        inLightingTest = false;
////                                    }
////                                });
////                            }
////                            else if(findChromaKeyCounter == 2)
////                            {
////                                isInFindChromaKeys = true;
////                                isValidBackground = ProcessImage.getInstance().findChromaKeys(inM.nativeObj, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom);
////                                isInFindChromaKeys = false;
////
////                            }
//
////                            if(findChromaKeyCounter == 1)
////                            {
////                                isInFindChromaKeys = true;
////                                isValidBackground = ProcessImage.getInstance().findChromaKeys(inM.nativeObj, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom);
////                                isInFindChromaKeys = false;
////
////                            }
////                            else if(isValidBackground == ProcessImage.DetectionResponseSuccess && findChromaKeyCounter == 2) //  ProcessImage.getInstance().checkScreen(inM.getNativeObjAddr()) &&
////                            {
////                                inLightingTest = true;
////                                ProcessImage.getInstance().getResizedPicture(inM.nativeObj);
////                                mOpenCvCameraView.getExposureLevel(new JavaCameraView.ExposureListener()
////                                {
////                                    @Override
////                                    public void onExposureReceived(double exposureTime)
////                                    {
////                                        isValidBackground = isValidExposure(exposureTime);
////                                        inLightingTest = false;
////                                    }
////                                });
////                            }
//
//
//                            if(findChromaKeyCounter == 1)
//                            {
//                                isInFindChromaKeys = true;
//                                isValidBackground = ProcessImage.getInstance().findChromaKeys(inM.nativeObj);
//                                isInFindChromaKeys = false;
//
//                            }
////                            else if(isValidBackground == ProcessImage.DetectionResponseSuccess && findChromaKeyCounter == 2) //  ProcessImage.getInstance().checkScreen(inM.getNativeObjAddr()) &&
//                            else if(findChromaKeyCounter == 2) //  ProcessImage.getInstance().checkScreen(inM.getNativeObjAddr()) &&
//                            {
//
//                                isValidBackground = isValidBrightness(ProcessImage.getInstance().getImageBrightness(inM.nativeObj));
//
////                                inLightingTest = true;
////                                ProcessImage.getInstance().getResizedPicture(inM.nativeObj);
////                                mOpenCvCameraView.getExposureLevel(new JavaCameraView.ExposureListener()
////                                {
////                                    @Override
////                                    public void onExposureReceived(double exposureTime)
////                                    {
////                                        isValidBackground = isValidExposure(exposureTime);
////                                        inLightingTest = false;
////                                    }
////                                });
//                            }
//
////
////                            else if(isValidBackground == ProcessImage.DetectionResponseSuccess && findChromaKeyCounter == 2)
////                            {
////                                isInFindChromaKeys = true;
////                                isValidBackground = ProcessImage.getInstance().checkFaceLightness(inM.nativeObj, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom);
////                                isInFindChromaKeys = false;
////                            }
////                            else if(isValidBackground == ProcessImage.DetectionResponseSuccess && findChromaKeyCounter < numberOfFramesToFind)
//                            else if(findChromaKeyCounter < numberOfFramesToFind)
//                            {
//                                isInFindChromaKeys = true;
//                                isValidBackground = ProcessImage.getInstance().addChromaKeys(inM.nativeObj);
//                                isInFindChromaKeys = false;
//                            }
//                            else
//                            {
////                                if ( isValidBackground == ProcessImage.DetectionResponseSuccess)// && ProcessImage.getInstance().isValidChromaKeyAgainstSkinColor() )
////                        if( ! processImage.findChromaKeys(inM.nativeObj, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom) )
////                                {
//                                    mContext.onBackgroundDetected(isValidBackground);
//                                    ProcessImage.getInstance().multiDetection(inM.nativeObj , mFrameFileName, mFrameFileName != null && mFrameFileName.length() > 0, false, "");//, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom);
////                                }
////                                else
////                                {
////                                    mContext.onBackgroundDetected(isValidBackground);
////                                    ProcessImage.getInstance().getResizedPicture(inM.nativeObj);
////                                    mOpenCvCameraView.releaseExposure();
////                                    ProcessImage.getInstance().resetChromaKeys();
////                                }
//                                goDetect = false;
//                            }
//                        }
//                        else ProcessImage.getInstance().getResizedPicture(inM.nativeObj);
//                    }
//                    else if( ! ProcessImage.getInstance().multiDetection(inM.nativeObj , mFrameFileName, mFrameFileName != null && mFrameFileName.length() > 0, false, ""))//, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom) )
//                    {
//                        ProcessImage.getInstance().getResizedPicture(inM.nativeObj);
////                        isValidBrightness(ProcessImage.getInstance().getImageBrightness(inM.nativeObj));
//                    }
//
//                    return inM;
//                }
//            });
//
//        mOpenCvCameraView.enableView();
//        parentView.addView(mOpenCvCameraView);
//    }

    public static int incomingFramescounter = 0;
    private synchronized void initCamera2(FrameLayout parentView, int cameraID)
    {
        incomingFramescounter = 0;

        mCamera2View = new Camera2View(parentView.getContext());
        mCamera2View.setIsFrontCamera(cameraID == Camera.CameraInfo.CAMERA_FACING_FRONT);
//        mOpenCvCameraView = new JavaCameraView(parentView.getContext(), cameraID);//JavaCameraView) findViewById(R.id.jcv);
        mCamera2View.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
//        mOpenCvCameraView.setMaxFrameSize(480, 640);
//        ProcessImage.getInstance().setCameraRotation(mOpenCvCameraView.getCameraRotation(), mOpenCvCameraView.isMirrored());
        ProcessImage.getInstance().turnOnFaceDetection(mContext);

//        mOpenCvCameraView.setOnCameraStartingListener(new JavaCameraView.OnCameraStartingListener()
//        {
//            @Override
//            public void onCameraStarting(Camera camera) { }
//
//            @Override
//            public void onSetCameraRes(int cameraRes, int cameraWidth, int cameraHeight)
//            {
//                ProcessImage.getInstance().setCameraRes(cameraRes, FFMPEG.getInstance(mContext).isLiteVideo(), cameraHeight, cameraWidth);
//            }
//        });

//        mOpenCvCameraView.setErrorOnSpanClickListener(new OnClickListener()
//        {
//            @Override
//            public void onClick(View view)
//            {
//                TrackingManager.getInstance().trackCameraError(JavaCameraView.errorMessage);
//            }
//        });

        mCamera2View.setOnSurfaceReadyListener(new Camera2View.OnSurfaceReadyListener()
        {
            @Override
            public void onSurfaceReady(Surface surface)
            {
                mSurface = surface;
            }
        });

        mCamera2View.setOnFrameAvailableListener(new OnFrameAvailableListener()
        {
            boolean isInFindChromaKeys = false;
            int framesToSetSurface = 0;

            @Override
            public void onFrameAvailable(Mat currentFrame, long time)
            {
                if (isClosed) return;

                incomingFramescounter++;
//                if(mCamera2View.mSurface == null || !mCamera2View.mSurface.isValid())
                if (mSurface == null || !mSurface.isValid())
//                if(mCamera2View.isSurfaceValid())
                {
                    incomingFramescounter--;
                    return;
                }
//                if (--framesToRunGcCounter == 0) runGc();

                if (isRecording)
                {
                    if (isFirstTimeAfterPause)
                    {
                        isFirstTimeAfterPause = false;
                        lastTime = SystemClock.elapsedRealtime();

                        try
                        {
                            startRecNow();
                        }
                        catch (Throwable err)
                        {
                            err.printStackTrace();
                        }
                    }

                    testFps();
                    elpasedTime += (SystemClock.elapsedRealtime() - lastTime);
                    if (elpasedTime > mVideoMaxLength)
                    {
                        stop();
                    }
                    lastTime = SystemClock.elapsedRealtime();

                    long timeStamp = SystemClock.elapsedRealtimeNanos();
                    String framePath = videoRecorder.addFrame(timeStamp);

                    ProcessImage.getInstance().procSurface(currentFrame.nativeObj, mSurface, mFrameFileName, true, framePath);
                    if (isFirstFrame)
                        saveFirstFrameCamera2(framePath);
                }
                else if (isFirstTimeAfterStart)
                {
                    pauseRecNow();
                    if (isStopped)
                    {
                        incomingFramescounter--;
                        return;
                    }

                    ProcessImage.getInstance().procSurface(currentFrame.nativeObj, mSurface, mFrameFileName, false, null);
                }
                else if (goDetect)
                {
                    if (isInFindChromaKeys)
                    {
//                        ProcessImage.getInstance().getResizedPictureCameraII(currentFrame.nativeObj, mCamera2View.mSurface);
                        incomingFramescounter--;
                        return;
                    }

                    else if (isTurningOffAdjustments)
                    {
                        ProcessImage.getInstance().getResizedPictureCameraII(currentFrame.nativeObj, mSurface);
                        isDetectingNow = isTurningOffAdjustments = false;
                        mCamera2View.turnOffImageAdjustments(new Camera2View.OnImageAdjustmentsOff()
                        {
                            @Override
                            public void onActionDone(int exposureCompensation, int minExposure, int maxExposure)
                            {
                                isDetectingNow = true;
                            }

                        });
                    }
                    else if (isDetectingNow && !inLightingTest)
                    {
                        findChromaKeyCounter++;
//                            if(findChromaKeyCounter == 1) //  ProcessImage.getInstance().checkScreen(inM.getNativeObjAddr()) &&
//                            {
//                                inLightingTest = true;
//                                ProcessImage.getInstance().getResizedPicture(inM.nativeObj);
//                                mOpenCvCameraView.getExposureLevel(new JavaCameraView.ExposureListener()
//                                {
//                                    @Override
//                                    public void onExposureReceived(double exposureTime)
//                                    {
//                                        isValidBackground = isValidExposure(exposureTime);
//                                        inLightingTest = false;
//                                    }
//                                });
//                            }
//                            else if(findChromaKeyCounter == 2)
//                            {
//                                isInFindChromaKeys = true;
//                                isValidBackground = ProcessImage.getInstance().findChromaKeys(inM.nativeObj, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom);
//                                isInFindChromaKeys = false;
//
//                            }

//                            if(findChromaKeyCounter == 1)
//                            {
//                                isInFindChromaKeys = true;
//                                isValidBackground = ProcessImage.getInstance().findChromaKeys(inM.nativeObj, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom);
//                                isInFindChromaKeys = false;
//
//                            }
//                            else if(isValidBackground == ProcessImage.DetectionResponseSuccess && findChromaKeyCounter == 2) //  ProcessImage.getInstance().checkScreen(inM.getNativeObjAddr()) &&
//                            {
//                                inLightingTest = true;
//                                ProcessImage.getInstance().getResizedPicture(inM.nativeObj);
//                                mOpenCvCameraView.getExposureLevel(new JavaCameraView.ExposureListener()
//                                {
//                                    @Override
//                                    public void onExposureReceived(double exposureTime)
//                                    {
//                                        isValidBackground = isValidExposure(exposureTime);
//                                        inLightingTest = false;
//                                    }
//                                });
//                            }


                        if (findChromaKeyCounter == 1)
                        {
                            isInFindChromaKeys = true;
                            if (mSurface == null)
                                Log.e(TAG, "The surface is null >> ProcessImage.getInstance().findChromaKeysCamera2>>1");
                            isValidBackground = ProcessImage.getInstance().findChromaKeysCamera2(currentFrame.nativeObj, mSurface);
                            isInFindChromaKeys = false;

                        }
//                            else if(isValidBackground == ProcessImage.DetectionResponseSuccess && findChromaKeyCounter == 2) //  ProcessImage.getInstance().checkScreen(inM.getNativeObjAddr()) &&
                        else if (findChromaKeyCounter == 2) //  ProcessImage.getInstance().checkScreen(inM.getNativeObjAddr()) &&
                        {
                            isInFindChromaKeys = true;
                            isValidBackground = isValidBrightness(ProcessImage.getInstance().getImageBrightness(currentFrame.nativeObj));
                            isInFindChromaKeys = false;
//                                inLightingTest = true;
//                                ProcessImage.getInstance().getResizedPicture(inM.nativeObj);
//                                mOpenCvCameraView.getExposureLevel(new JavaCameraView.ExposureListener()
//                                {
//                                    @Override
//                                    public void onExposureReceived(double exposureTime)
//                                    {
//                                        isValidBackground = isValidExposure(exposureTime);
//                                        inLightingTest = false;
//                                    }
//                                });
                        }

//
//                            else if(isValidBackground == ProcessImage.DetectionResponseSuccess && findChromaKeyCounter == 2)
//                            {
//                                isInFindChromaKeys = true;
//                                isValidBackground = ProcessImage.getInstance().checkFaceLightness(inM.nativeObj, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom);
//                                isInFindChromaKeys = false;
//                            }
//                            else if(isValidBackground == ProcessImage.DetectionResponseSuccess && findChromaKeyCounter < numberOfFramesToFind)
                        else if (findChromaKeyCounter < numberOfFramesToFind)
                        {
                            isInFindChromaKeys = true;
                            isValidBackground = ProcessImage.getInstance().addChromaKeysCamera2(currentFrame.nativeObj, mSurface);
                            isInFindChromaKeys = false;
                        }
                        else
                        {
//                                if ( isValidBackground == ProcessImage.DetectionResponseSuccess)// && ProcessImage.getInstance().isValidChromaKeyAgainstSkinColor() )
//                        if( ! processImage.findChromaKeys(inM.nativeObj, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom) )
//                                {
                            goDetect = false;
                            mContext.onBackgroundDetected(isValidBackground);
                            ProcessImage.getInstance().procSurface(currentFrame.nativeObj, mSurface, mFrameFileName, false, null);
//                                }
//                                else
//                                {
//                                    mContext.onBackgroundDetected(isValidBackground);
//                                    ProcessImage.getInstance().getResizedPicture(inM.nativeObj);
//                                    mOpenCvCameraView.releaseExposure();
//                                    ProcessImage.getInstance().resetChromaKeys();
//                                }

                        }
                    }
//                    else ProcessImage.getInstance().getResizedPictureCameraII(currentFrame.nativeObj, mCamera2View.mSurface);
                }
                else if (!ProcessImage.getInstance().procSurface(currentFrame.nativeObj, mSurface, mFrameFileName, false, null))
                {
                    if (framesToSetSurface++ < 10)
                    {
                        ProcessImage.getInstance().setSurface(mSurface);
                    }

                    ProcessImage.getInstance().getResizedPictureCameraII(currentFrame.nativeObj, mSurface);
//                        isValidBrightness(ProcessImage.getInstance().getImageBrightness(inM.nativeObj));
                }

                incomingFramescounter--;

//
            }

            @Override
            public void onFrameDropped(Mat droppedFrame, long time)
            {

            }

            @Override
            public String shouldSaveDroppedFrame(long time)
            {
                return null;
            }

            @Override
            public boolean shouldCatchDroppedFrames()
            {
                return isRecording;
            }
        });

//        mOpenCvCameraView.enableView();
        parentView.addView((SurfaceView) mCamera2View, 0);
    }

    private synchronized void initCamera1(FrameLayout parentView, int cameraID)
    {
        try{mCamera1View = new Camera1View(parentView.getContext(), cameraID, mFrameFileName);}
        catch (Throwable err) { err.printStackTrace(); return;}
//        mCamera1View.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        ProcessImage.getInstance().turnOnFaceDetection(mContext);

        incomingFramescounter = 0;
        mCamera1View.setOnFrameAvailableListener(new OnFrameAvailableListener()
        {
            boolean isInFindChromaKeys = false;
            int framesToSetSurface = 0;

            @Override
            public void onFrameAvailable(Mat currentFrame, long time)
            {
                if(isClosed) return;

                if(--framesToRunGcCounter == 0)
                {
                    runGc();
                }

                incomingFramescounter++;

                if (isRecording)
                {
                    if (isFirstTimeAfterPause)
                    {
                        isFirstTimeAfterPause = false;
                        lastTime = SystemClock.elapsedRealtime();

                        try
                        {
                            startRecNow();
                        }catch (Throwable err){err.printStackTrace();}
//                        time = SystemClock.elapsedRealtimeNanos();
                    }

                    if(!audioPlayer.isPlaying)
                    {
                        ProcessImage.getInstance().multiDetection(currentFrame.nativeObj, mFrameFileName, mFrameFileName != null, false, "", true);
                        return;
                    }

                    testFps();
                    elpasedTime += (SystemClock.elapsedRealtime() - lastTime);
                    if (elpasedTime > mVideoMaxLength)
                    {
                        stop();
                    }
                    lastTime = SystemClock.elapsedRealtime();

                    String framePath = videoRecorder.addFrame(time);

                    ProcessImage.getInstance().multiDetection(currentFrame.nativeObj, mFrameFileName, mFrameFileName != null, true, framePath, true);
                    if(isFirstFrame)
                        saveFirstFrame(framePath);
                }
                else if (isFirstTimeAfterStart)
                {
                    pauseRecNow();
                    if (isStopped)
                    {
                        incomingFramescounter--;
                        ProcessImage.getInstance().getResizedPicture(currentFrame.nativeObj);
                        return;
                    }

                    ProcessImage.getInstance().multiDetection(currentFrame.nativeObj, mFrameFileName, mFrameFileName != null, false, "", true);
                }
                else if(goDetect)
                {
                    if(isInFindChromaKeys)
                    {
                        ProcessImage.getInstance().getResizedPicture(currentFrame.nativeObj);
                        incomingFramescounter--;
                        return;
                    }

                    else if(isTurningOffAdjustments)
                    {
                        ProcessImage.getInstance().getResizedPicture(currentFrame.nativeObj);
                        isTurningOffAdjustments = false;
                        mCamera1View.turnOffImageAdjustments(new Camera1View.OnImageAdjustmentsOff()
                        {
                            @Override
                            public void onActionDone()
                            {
                                isDetectingNow = true;
                            }
                        });
                    }
                    else if(isDetectingNow && !inLightingTest)
                    {
                        findChromaKeyCounter++;

                        if(findChromaKeyCounter == 1)
                        {
                            isInFindChromaKeys = true;
                            isValidBackground = ProcessImage.getInstance().findChromaKeys(currentFrame.nativeObj);
                            isInFindChromaKeys = false;

                        }
//                            else if(isValidBackground == ProcessImage.DetectionResponseSuccess && findChromaKeyCounter == 2) //  ProcessImage.getInstance().checkScreen(inM.getNativeObjAddr()) &&
                        else if(findChromaKeyCounter == 2) //  ProcessImage.getInstance().checkScreen(inM.getNativeObjAddr()) &&
                        {
                            isInFindChromaKeys = true;
                            isValidBackground = isValidBrightness(ProcessImage.getInstance().getImageBrightness(currentFrame.nativeObj));
//                            ProcessImage.getInstance().getResizedPicture(currentFrame.nativeObj);
                            isInFindChromaKeys = false;
//                                inLightingTest = true;
//                                ProcessImage.getInstance().getResizedPicture(inM.nativeObj);
//                                mOpenCvCameraView.getExposureLevel(new JavaCameraView.ExposureListener()
//                                {
//                                    @Override
//                                    public void onExposureReceived(double exposureTime)
//                                    {
//                                        isValidBackground = isValidExposure(exposureTime);
//                                        inLightingTest = false;
//                                    }
//                                });
                        }

//
//                            else if(isValidBackground == ProcessImage.DetectionResponseSuccess && findChromaKeyCounter == 2)
//                            {
//                                isInFindChromaKeys = true;
//                                isValidBackground = ProcessImage.getInstance().checkFaceLightness(inM.nativeObj, mOpenCvCameraView.faceLeft, mOpenCvCameraView.faceTop, mOpenCvCameraView.faceRight, mOpenCvCameraView.faceBottom);
//                                isInFindChromaKeys = false;
//                            }
//                            else if(isValidBackground == ProcessImage.DetectionResponseSuccess && findChromaKeyCounter < numberOfFramesToFind)
                        else if(findChromaKeyCounter < numberOfFramesToFind)
                        {
                            isInFindChromaKeys = true;
                            isValidBackground = ProcessImage.getInstance().addChromaKeys(currentFrame.nativeObj);
                            isInFindChromaKeys = false;
                        }
                        else
                        {
                            goDetect = false;
                            mContext.onBackgroundDetected(isValidBackground);
                            ProcessImage.getInstance().multiDetection(currentFrame.nativeObj, mFrameFileName, mFrameFileName != null, false, "", true);
                        }
                    }
                    else ProcessImage.getInstance().getResizedPicture(currentFrame.nativeObj);

                }
                else if( ! ProcessImage.getInstance().multiDetection(currentFrame.nativeObj, mFrameFileName, mFrameFileName != null, false, "", true) )
                {
                    ProcessImage.getInstance().getResizedPicture(currentFrame.nativeObj);
                }

                incomingFramescounter--;

            }

            @Override
            public void onFrameDropped(Mat droppedFrame, long time)
            {
                if(isClosed || !isRecording)
                    return;
                String framePath = videoRecorder.addDroppedFrame(time, mFrameFileName);
                ProcessImage.getInstance().saveFrame(droppedFrame.nativeObj, framePath);
            }

            @Override
            public String shouldSaveDroppedFrame(long time)
            {
                if(!isClosed && isRecording && audioPlayer.isPlaying)
                {
                    String framePath = videoRecorder.addDroppedFrame(time, mFrameFileName);
                    testFps();
                    return framePath;
                }
                return null;
            }

            @Override
            public boolean shouldCatchDroppedFrames()
            {
                return isRecording;
            }
        });

//        mOpenCvCameraView.enableView();
        parentView.addView(mCamera1View, 0);
    }

    private boolean isClosed = false;
    private final int CLOSING_CAMERA_MAX_WAIT_TIME = 500;
    private int closingTime;
    public void close()
    {
        if(isClosed)
            return;
        isClosed = true;

//        if(mCamera1View != null)
//            mCamera1View.setOnFrameAvailableListener(null);
////        if(isCamera2)
//        {
//            closingTime = CLOSING_CAMERA_MAX_WAIT_TIME;
//
//            while (incomingFramescounter != 0 && closingTime > 0)
//            {
//                try
//                {
//                    Thread.sleep(100);
//                    closingTime -= 100;
//                }
//                catch (Throwable err) { }
//            }
//        }
        try
        {
            audioRecorder.close();
        }
        catch (Throwable err)
        {
        }
        if (audioPlayer != null)
            try
            {
                audioPlayer.close();
            }
            catch (Throwable err)
            {
            }

//        if(!isCamera2)
//        {
//            try
//            {
//                shutdownExecutors();
//            }
//            catch (Throwable err) { }
//        }
//        else //

//        if(isCamera2)
//        {
//            try
//            {
//                mParentView.removeView((SurfaceView)mCamera2View);
//            }
//            catch (Throwable err) { }
//
//            try
//            {
//                mCamera2View.close(new Camera2ViewInterface.OnCameraClosedListener()
//                {
//                    @Override
//                    public void onCameraClosed()
//                    {
//                        CreateVideoActivity.isNowCanRun = true;
//                    }
//                });
//            }
//            catch (Throwable err) { }
//        }
//        else
//        {
//            mParentView.removeView(mCamera1View);
//            try
//            {
//                mCamera1View.close();
//            }
//            catch (Throwable err) { }
//        }
        try
        {
            setKeepScreenOn(false);
        }
        catch (Throwable err) {}
        isStopped = true;
    }

    public void releaseCamera(boolean dontBlockDontCare)
    {
        if(mCamera1View != null)
            mCamera1View.setOnFrameAvailableListener(null);
//        if(isCamera2)
//        Tic.tic(1100);

        {
            closingTime = CLOSING_CAMERA_MAX_WAIT_TIME;

            while (incomingFramescounter != 0 && closingTime > 0)
            {
                try
                {
                    Thread.sleep(100);
                    closingTime -= 100;
                }
                catch (Throwable err) { }
            }
        }
        incomingFramescounter = 0;
//        Tic.tac(1100);

        if(isCamera2)
        {
//            try
//            {
//                mParentView.removeView((SurfaceView)mCamera2View);
//            }
//            catch (Throwable err) { }

            try
            {
                mCamera2View.close(new Camera2ViewInterface.OnCameraClosedListener()
                {
                    @Override
                    public void onCameraClosed()
                    {
                        CreateVideoActivity.isNowCanRun = true;
                    }
                });
            }
            catch (Throwable err) { }
        }
        else
        {
//            Tic.tic(1200);

            try
            {
                mCamera1View.close(dontBlockDontCare);
            }
            catch (Throwable err) { }
//            Tic.tac(1200);

        }
    }

    public void removeCameraView()
    {
        if(isCamera2)
        {
            mParentView.removeView((SurfaceView) mCamera2View);
        }
        else
        {
            mParentView.removeView(mCamera1View);
        }
    }


//    private void shutdownExecutors()
//    {
//        if (!saveFileExecutorService.isShutdown())
//            saveFileExecutorService.shutdown();
//    }

    private void runGc()
    {
        new Thread()
        {
            public void run()
            {
                Runtime.getRuntime().gc();
                framesToRunGcCounter = numberOfFramesBe4RunGc;
            }
        }.start();
    }


//    private class ProcessData implements Runnable
//    {
//        private long timeStamp;
//
//        private ProcessData(long iTimeStamp)
//        {
//            timeStamp = iTimeStamp;
//        }
//
//        @Override
//        public void run()
//        {
//            try
//            {
//                if (isRecordingStarted)
//                {
//                    mIntrinsicYuvToRGB.forEach(mAllocationOut);
//                    mAllocationOut.syncAll(Allocation.USAGE_SHARED);
//
//                    if (isOverlaySet && faceDetected)
//                        mOverlay.draw();
//
////                    Bitmap recordBitmap = mOutputBitmap.copy(Bitmap.Config.RGB_565, false);
//                    Bitmap recordBitmap = BitmapFactory.decodeFile(mFrameFileName);
//
//                    try
//                    {
//                        process2(mOutputBitmap, recordBitmap, bufferField, hueTop, hueBot, luminanceTop, luminanceBottom, saturationTop, saturationBottom);//, mHuesBot, mHuesTop, mSaturationBot, mSaturationTop, mLuminanceBot, mLuminanceTop);
//                    }
//                    catch (Throwable err){}
//
//                    if (recordBitmap != null)
//                    {
//                        currentBitmap = recordBitmap;
//                        mContext.runOnUiThread(invalidateRunnable);
//                        saveFileExecutorService.execute(new SaveFileThread(recordBitmap, SystemClock.elapsedRealtimeNanos()));//timeStamp = SystemClock.elapsedRealtimeNanos();
//
//                    }
////                    lastFrameTimeStamp = timeStamp;
//                }
//                else
//                {
//                    mIntrinsicYuvToRGB.forEach(mAllocationOut);
//                    mAllocationOut.syncAll(Allocation.USAGE_SHARED);
//
//                    if(isInitChromaKey)
//                    {
//                        isInitChromaKey = false;
//                        initChromaKey(mOutputBitmap);
////                    initVideoManager();
//                    }
//
//                    if (isOverlaySet && faceDetected)
//                        mOverlay.draw();
//
////                    Bitmap recordBitmap = mOutputBitmap.copy(Bitmap.Config.RGB_565, false);
//
//                    Bitmap recordBitmap = BitmapFactory.decodeFile(mFrameFileName);
//
//                    try
//                    {
//                        process2(mOutputBitmap, recordBitmap, bufferField, hueTop, hueBot, luminanceTop, luminanceBottom, saturationTop, saturationBottom);//, mHuesBot, mHuesTop, mSaturationBot, mSaturationTop, mLuminanceBot, mLuminanceTop);
//
//                    }
//                    catch (Throwable err){}
//                    if (recordBitmap != null)
//                    {
//                        currentBitmap = recordBitmap;
//                        mContext.runOnUiThread(invalidateRunnable);
//                        saveFileExecutorService.execute(new RecycleBitmapThread(recordBitmap));
//                    }
//                }
//            }
//            catch (Throwable err)
//            {
//                err.printStackTrace();
//            }
////            if(faceDetectionOn)// runFaceDetector=!runFaceDetector)
////                detector.receiveFrame(new Frame.Builder().setRotation(faceDetectionRotation).setImageData(ByteBuffer.wrap(data, 0, data.length), WIDTH, HEIGHT, ImageFormat.NV21).build());
//        }
//    }
//
//    private class ProcessData2 implements Runnable
//    {
//        private long timeStamp;
//
//        private ProcessData2(long iTimeStamp)
//        {
//            timeStamp = iTimeStamp;
//        }
//
//        @Override
//        public void run()
//        {
//            try
//            {
//                if (isRecordingStarted)
//                {
//                    mIntrinsicYuvToRGB2.forEach(mAllocationOut2);
//                    mAllocationOut2.syncAll(Allocation.USAGE_SHARED);
//
//                    if (isOverlaySet && faceDetected)
//                        mOverlay2.draw();
//
////                    Bitmap recordBitmap = mOutputBitmap2.copy(Bitmap.Config.RGB_565, false);
//
//                    Bitmap recordBitmap = BitmapFactory.decodeFile(mFrameFileName);
//
//                    try
//                    {
//                        process2(mOutputBitmap2, recordBitmap, bufferField, hueTop, hueBot, luminanceTop, luminanceBottom, saturationTop, saturationBottom);//, mHuesBot, mHuesTop, mSaturationBot, mSaturationTop, mLuminanceBot, mLuminanceTop);
//                    }
//                    catch (Throwable err){}
//
//                    if (recordBitmap != null)
//                    {
//                        currentBitmap = recordBitmap;
//                        mContext.runOnUiThread(invalidateRunnable);
//                        saveFileExecutorService.execute(new SaveFileThread(recordBitmap, SystemClock.elapsedRealtimeNanos()));
//                    }
//                }
//                else
//                {
//                    mIntrinsicYuvToRGB2.forEach(mAllocationOut2);
//                    mAllocationOut2.syncAll(Allocation.USAGE_SHARED);
//
//                    if (isOverlaySet && faceDetected)
//                        mOverlay2.draw();
//
////                    Bitmap recordBitmap = mOutputBitmap2.copy(Bitmap.Config.RGB_565, false);
//
//                    Bitmap recordBitmap = BitmapFactory.decodeFile(mFrameFileName);
//                    try
//                    {
//                        process2(mOutputBitmap2, recordBitmap, bufferField, hueTop, hueBot, luminanceTop, luminanceBottom, saturationTop, saturationBottom);//, mHuesBot, mHuesTop, mSaturationBot, mSaturationTop, mLuminanceBot, mLuminanceTop);
//                    }
//                    catch (Throwable err){}
//                    if (recordBitmap != null)
//                    {
//                        currentBitmap = recordBitmap;
//                        mContext.runOnUiThread(invalidateRunnable);
//                        saveFileExecutorService.execute(new RecycleBitmapThread(recordBitmap));
//                    }
//                }
//            }
//            catch (Throwable err)
//            { err.printStackTrace();
//            }
//        }
//    }

//    private Runnable invalidateRunnable = new Runnable()
//    {
//        @Override
//        public void run()
//        {
//            invalidate();
//        }
//    };

//    private void setDetector()
//    {
//        detector = new FaceDetector.Builder(mContext)
//                .setTrackingEnabled(true)
//                .setLandmarkType(FaceDetector.NO_LANDMARKS)
//                .setClassificationType(FaceDetector.NO_CLASSIFICATIONS)
//                .setMode(FaceDetector.FAST_MODE)
//                .build();
//
//        detector.setProcessor(new Detector.Processor<Face>()
//        {
//            @Override
//            public void release()
//            {
//            }
//
//            @Override
//            public void receiveDetections(Detector.Detections<Face> detections)
//            {
//                        detections.getFrameMetadata().getRotation()+">>>"+faceDetectionRotation);
//
//                if(detections.getDetectedItems().size() > 0)
//                {
//                    mOverlay.updateLocations(detections.getDetectedItems().valueAt(0));
//                    mOverlay2.updateLocations(detections.getDetectedItems().valueAt(0));
//                    faceDetected = true;
//                    detections.getDetectedItems().clear();
//                }
//                else if(faceDetected) faceDetected = false;
//            }
//        });
//    }

//    private void releaseDetector()
//    {
//        if(detector != null)
//        {
//            detector.release();
//        }
//    }

    private boolean isFirstFrame = true;

    public void setFirstFramePath()
    {
        mFrameFileName = audioRecorder.framesFilesPaths +"frame0001.jpg";
    }

    //    private String firstFramePath;
//    private class SaveFileThread implements Runnable
//    {
//        private Bitmap outputBitmap;
//        private long timeStamp;
//
//        private SaveFileThread(Bitmap iOutputBitmap, long iTimeStamp)
//        {
//            outputBitmap = iOutputBitmap;
//            timeStamp = iTimeStamp;
//        }
//
//        public void run()
//        {
//            videoRecorder.addFrame(outputBitmap, timeStamp);
//            if(isFirstFrame)
//            {
//                saveFirstFrame(timeStamp);
//            }
//            recycleBitmap(outputBitmap);
//        }
//    }

//    private class RecycleBitmapThread implements Runnable
//    {
//        private Bitmap recycleBitmap;
//
//        private RecycleBitmapThread(Bitmap iRecycleBitmap)
//        {
//            recycleBitmap = iRecycleBitmap;
//        }
//
//        public void run()
//        {
//            recycleBitmap(recycleBitmap);
//        }
//    }

    private void saveFirstFrame(final String framePath)
    {
        isFirstFrame = false;
        new Thread()
        {
            public void run()
            {
                String firstFramePath = framePath;//videoRecorder.getFramePath(timeStamp);

                try
                {
                    OutputStream os = new FileOutputStream(Constants.thumbnail);
                    InputStream is = new FileInputStream(firstFramePath);

                    byte[]buffer = new byte[1024];
                    int count;
                    while( (count = is.read(buffer)) != -1 )
                        os.write(buffer, 0, count);
                    os.flush();
                    os.close();
                    is.close();

                    int cameraRotation = mCamera1View.getCameraRotation();
                    ProcessImage.getInstance().rotateImage(Constants.thumbnail, 90);//acording to AR_OUTPUT_IMAGE_ROTATION on the other clock way

                    ExifInterface exifInterface = new ExifInterface(Constants.thumbnail);
                    exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, 1 + "");//(getCameraRotation() == 90 ? ExifInterface.ORIENTATION_ROTATE_90 : ExifInterface.ORIENTATION_ROTATE_270) + "");
                    exifInterface.saveAttributes();

                }catch (Throwable err){err.printStackTrace();}
            }
        }.start();
    }

    private void saveFirstFrameCamera2(final String firstFramePath)
    {
        isFirstFrame = false;
        new Thread()
        {
            public void run()
            {
                try
                {
                    OutputStream os = new FileOutputStream(Constants.thumbnail);
                    InputStream is = new FileInputStream(firstFramePath);

                    byte[]buffer = new byte[1024];
                    int count;
                    while( (count = is.read(buffer)) != -1 )
                        os.write(buffer, 0, count);
                    os.flush();
                    os.close();
                    is.close();

                    ExifInterface exifInterface = new ExifInterface(Constants.thumbnail);
                    exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, 1 + "");//(getCameraRotation() == 90 ? ExifInterface.ORIENTATION_ROTATE_90 : ExifInterface.ORIENTATION_ROTATE_270) + "");
                    exifInterface.saveAttributes();

                }catch (Throwable err){err.printStackTrace();}
            }
        }.start();
    }

    private void recycleBitmap(Bitmap bitmapToRecycle)
    {
        bitmapToRecycle.recycle();
    }

    public void start()
    {
//        turnOffAutoExposure();

        isRecording = true;
        isFirstTimeAfterPause = true;
        isPaused = false;
        isStopped = false;
//        try {
//            startRecNow();
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }
    }

    private synchronized void startRecNow() throws Throwable
    {
        if (audioPlayer != null)
        {
            try
            {
                audioPlayer.resume();}
            catch (Throwable err)
            {
                err.printStackTrace();
            }
            videoManager.start();

        }

        videoRecorder.start();
        try
        {
            audioRecorder.start();
        }
        catch (Throwable err)
        {
        }
    }

    public void pause()
    {
        isRecording = false;
        isFirstTimeAfterStart = true;
//        pauseRecNow();
    }

    private void pauseRecNow()
    {
        if (!isPaused)
        {
            if (!isFirstTimeAfterStart) return;
            isFirstTimeAfterStart = false;

            isPaused = true;

//            isRecordingStarted = false;
            adjustAfterStop = true;
            stoppedTime = SystemClock.elapsedRealtime();

            audioRecorder.pause();
            videoRecorder.pause();

            if (audioPlayer != null)
            {
                audioPlayer.pause();
                videoManager.stop();
            }

//            if(!isCamera2)
//            {
//                oldExecutorService = saveFileExecutorService;
//                saveFileExecutorService = Executors.newFixedThreadPool(1);
//                oldExecutorService.shutdown();
//            }

            if (isStopped)recStopped();
//            else new finishForPause().execute();
        }
        else recStopped();
    }

    public synchronized void stop()
    {
        if (isStopped)
            return;
        isStopped = true;
        if (isPaused)
            recStopped();
        else
        {
            isFirstTimeAfterStart = true;
            isRecording = false;
        }
//        recStopped();
    }

    private void recStopped()
    {
        isStopped = true;
        audioRecorder.stop();
        if (audioPlayer != null)
            audioPlayer.stop();

        mContext.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                invalidate();
//                if(!isCamera2)
//                    shutdownExecutors();
                new finalize().execute();
            }
        });
    }

    public void setAudioSpeed(float speed)
    {
        if(audioPlayer != null)
        {
            if(speed == 2f) {
                videoManager.setVideoSpeed(2f);
                audioPlayer.setSpeed(audioPlayer.SPEED_FAST);
            }
            else if(speed < 0.9f) {
                videoManager.setVideoSpeed(0.5f);
                audioPlayer.setSpeed(audioPlayer.SPEED_SLOW);
            }
            else {
                videoManager.setVideoSpeed(1f);
                audioPlayer.setSpeed(audioPlayer.SPEED_NORMAL);
            }
//            audioPlayer.removeFilterType(AudioSpeedFilter.class);
//            if (speed != 1f)
//                audioPlayer.addFilter(new AudioSpeedFilter(speed));
        }
    }

    public void setRecordSpeed(float speed)
    {
        if (videoRecorder != null) {
            videoRecorder.setRecordSpeed(speed);
            this.mVideoMaxLength *= speed;
        }
    }

    private class finishForPause extends AsyncTask<Void, Void, Void>
    {
        long time;
//        LoadingDialog loadingDialog;

        @Override
        protected void onPreExecute()
        {
            time = System.currentTimeMillis();
//            loadingDialog = new LoadingDialog(mContext);
            if (mActivateDeactivateButtons != null)
                mActivateDeactivateButtons.deActivateButtons();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
//            terminateExecutor();
            return null;
        }

        @Override
        protected void onPostExecute(Void res)
        {
            if (isStopped)
                recStopped();
            else if (mActivateDeactivateButtons != null)
                mActivateDeactivateButtons.activateButtons();
        }
    }

//    private void terminateExecutor()
//    {
//
//    }

    private class finalize extends AsyncTask<Void, Void, Boolean>
    {
        @Override
        protected void onPreExecute()
        {
            mOnProcessingVideoListener.onStart();
            removeCameraView();

//            if(isCamera2)
//                mCamera2View.onPause();
        }

        @SuppressWarnings("WrongThread")
        @Override
        protected Boolean doInBackground(Void... params)
        {
            Tic.tic(1000);
            releaseCamera(false);
            Tic.tac(1000);

//            terminateExecutor();

//            if(!isCamera2)
//            {
//                try
//                {
//                    oldExecutorService.awaitTermination(40000, TimeUnit.MILLISECONDS);
//                }
//                catch (Throwable e) { }
//            }

            if (audioPlayer != null)
            {
                videoRecorder.setAudioTimeStamps(audioPlayer.getStartFrameTimeStamp(), audioPlayer.getEndFrameTimeStamp());
            }
//

//            if (new File(videoOutputPath).exists())
//                new File(videoOutputPath).delete();
            if(fpsCount == 0)
            {
                return false;
            }

            Tic.tic(1001);
            videoRecorder.finalize(fpsSum / fpsCount, new VideoRecorder.OnDoneProcessingListener()
            {
                @Override
                public void onDone()
                {
//                    new finalizePhase2().execute();
                    Tic.tac(1001);

                    finalizePhase2Threaded();
                }
            });
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success)
        {
            if(!success) // error
            {
                mOnProcessingVideoListener.onDone();
            }
        }
    }

    private void finalizePhase2Threaded()
    {
        new Thread()
        {
            public void run()
            {
                try
                {
                    Tic.tic(1010);

                    if (new File(App.VIDEO_WORKING_FOLDER + "output-007.mp4").exists())
                        new File(App.VIDEO_WORKING_FOLDER + "output-007.mp4").delete();

                    int rotation = FFMPEG.getInstance(mContext).getIsCamera2() ? 0 : 270;

                    Tic.tic(1002);


//                    FFMPEG.getInstance(mContext).concatVideosTogether(videoRecorder.getSubPaths(), App.VIDEO_WORKING_FOLDER + "output-007.mp4", videoRecorder.workingDirectory, rotation, videoRecorder.getFPS(), audioFileName);
                    FFMPEG.getInstance(mContext).concatVideosTogether(videoRecorder.getSubPaths(), videoOutputPath, videoRecorder.workingDirectory, rotation, videoRecorder.getFPS(), audioFileName);

                    System.out.println(">>>>>>ffmpeg>>>>"+FFMPEG.getInstance(null).getError());
                    Tic.tac(1002);

//                    if(audioRecorder.isSoundTrackSet())
//                    {
//
//                        FFMPEG.getInstance(mContext).coupleVideoAndAudioMp3(App.VIDEO_WORKING_FOLDER + "output-007.mp4", audioFileName, videoOutputPath, videoRecorder.getFPS());
//                    }
//                    else
//                    {
//                        FFMPEG.getInstance(mContext).coupleVideoAndAudioS16BE(App.VIDEO_WORKING_FOLDER + "output-007.mp4", audioRecorder.getOutputFilePath(), videoOutputPath, videoRecorder.getFPS());
//                    }
                }
                catch (Throwable throwable)
                {
                    throwable.printStackTrace();
                }

                if(mContext != null)
                    mContext.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if(mOnProcessingVideoListener != null)
                                mOnProcessingVideoListener.onDone();
                        }
                    });
                Tic.tac(1010);

            }
        }.start();
    }

//    private class finalizePhase2 extends AsyncTask<Void, Void, Void>
//    {
//        @Override
//        protected Void doInBackground(Void... params)
//        {
//            try
//            {
//                if (new File(App.VIDEO_WORKING_FOLDER + "output-007.mp4").exists())
//                    new File(App.VIDEO_WORKING_FOLDER + "output-007.mp4").delete();
//
//                int rotation = FFMPEG.getInstance(mContext).getIsCamera2() ? 0 : 270;
//                FFMPEG.getInstance(mContext).concatVideosTogether(videoRecorder.getSubPaths(), App.VIDEO_WORKING_FOLDER + "output-007.mp4", videoRecorder.workingDirectory, rotation, videoRecorder.getFPS(), audioFileName);
//
//                if(audioRecorder.isSoundTrackSet())
//                {
//                    FFMPEG.getInstance(mContext).coupleVideoAndAudioMp3(App.VIDEO_WORKING_FOLDER + "output-007.mp4", audioFileName, videoOutputPath, videoRecorder.getFPS());
//                }
//                else
//                {
//                    FFMPEG.getInstance(mContext).coupleVideoAndAudioS16BE(App.VIDEO_WORKING_FOLDER + "output-007.mp4", audioRecorder.getOutputFilePath(), videoOutputPath, videoRecorder.getFPS());
//                }
//            }
//            catch (Throwable throwable)
//            {
//                throwable.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void params)
//        {
//            mOnProcessingVideoListener.onDone();
//        }
//    }

    public interface OnProcessingVideoListener
    {
        void onStart();

        void onDone();
    }

    public void detect()
    {
        isTurningOffAdjustments = true;
        findChromaKeyCounter = 0;
        isDetectingNow = false;
        goDetect = true;
    }

//    public void reset()
//    {
//        try
//        {
//            audioRecorder.close();
//        }
//        catch (Throwable err) {}
//        if (audioPlayer != null)
//            try
//            {
//                audioPlayer.close();
//            }
//            catch (Throwable err) {}
//
////        if(!isCamera2)
////        {
////            try
////            {
////                shutdownExecutors();
////            }
////            catch (Throwable err) { }
////        }
//
//        isStopped = true;
//
////        videoRecorder = new VideoRecorder(mContext);
////        videoRecorder.emptyWorkingFolder();
//
////        if(!isCamera2)
////            saveFileExecutorService = Executors.newFixedThreadPool(1);
//
//    }

    /**
     * if set, the downloadedFilePath will be the file path or null if an error occured
     *
     * @param onDoneDownloadingListener - the listener to assign
     */
    public void setOnDoneDownloadingListener(AudioRecorder.OnDoneDownloadingListener onDoneDownloadingListener)
    {
        mOnDoneDownloadingListener = onDoneDownloadingListener;
        initAudioRecorder();
    }

    public void setOnCompleteListener(AudioPlayer.OnCompleteListener onCompleteListener)
    {
        if (audioPlayer != null)
            audioPlayer.setOnCompleteListener(onCompleteListener);
    }

    public void setActivateDeactivateButtonsDispatcher(CreateVideoActivity.ActivateDeactivateButtonsDispatcher activateDeactivateButtons)
    {
        mActivateDeactivateButtons = activateDeactivateButtons;
    }

    //////////////////////////////////////////////////////////////////

//    private Bitmap getBlurredBitmap(int radius)
//    {
//        Bitmap savedForRecycle = currentBitmap;
//        currentBitmap = savedForRecycle.copy(Bitmap.Config.ARGB_8888, false);
//        savedForRecycle.recycle();
//
//        Bitmap outputBitmap = Bitmap.createBitmap(currentBitmap.getWidth(), currentBitmap.getHeight(), currentBitmap.getConfig());
//        RenderScript rs = RenderScript.create(mContext);
//        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
//        Allocation tmpIn = Allocation.createFromBitmap(rs, currentBitmap);
//        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
//        theIntrinsic.setRadius(radius);
//        theIntrinsic.setInput(tmpIn);
//        theIntrinsic.forEach(tmpOut);
//        tmpOut.copyTo(outputBitmap);
//        savedForRecycle = currentBitmap;
//        currentBitmap = outputBitmap;
//        savedForRecycle.recycle();
//
////        RenderScript.releaseAllContexts();
//
//        return outputBitmap;
//    }

    //////////////////////////////////////////////////////////////////

    private long timeCount;
    private int frames;
    //    private String FPSLabel = "";
    private int fpsSum;
    private int fpsCount;
    private boolean isFirstTest = true;
    private long stoppedTime;
    private boolean adjustAfterStop;

    private void testFps()
    {
        if (isFirstTest)
        {
            timeCount = SystemClock.elapsedRealtime();
            isFirstTest = false;
        }
        else if (adjustAfterStop)
        {
            timeCount += (SystemClock.elapsedRealtime() - stoppedTime);
            adjustAfterStop = false;
        }

        if (SystemClock.elapsedRealtime() - timeCount > 999)
        {
//            try
//            {
//                FPSLabel = "Fps : " + (frames);///((System.currentTimeMillis() - timeCount)/1000));
//            }
//            catch (Throwable err){}
            timeCount = SystemClock.elapsedRealtime();
            fpsSum += frames;
            fpsCount++;
            frames = 0;
        }
        frames++;
    }

    private boolean inLightingTest = false;
//    private final double minimumExposure = 0.022;// recorded 0.0002; according to -20 to 20
//    private final double maximumExposure = 0.052; // recorded 0.066;
//    private int isValidExposure(double exposureTime)
//    {
//        android.widget.Toast.makeText(mContext, "Exposure time is : "+exposureTime, android.widget.Toast.LENGTH_LONG).show();
//
//        double minExposure = minimumExposure;// * ()
//        double maxExposure = maximumExposure + 0.1 * (20 / mOpenCvCameraView.maxExposure);
//        if(exposureTime < minimumExposure)
//        {
////            mOpenCvCameraView.releaseExposure();
////            ProcessImage.getInstance().resetChromaKeys();
//            return ProcessImage.DetectionResponseTooMuchLighting;
//        }
//        else if(exposureTime > maximumExposure)
//        {
////            mOpenCvCameraView.releaseExposure();
////            ProcessImage.getInstance().resetChromaKeys();
//            return ProcessImage.DetectionResponseNotEnoughLighting;
//        }
//        return ProcessImage.DetectionResponseSuccess;
//    }

    private final int minimumBrightness = 100;
    private final int maximumBrightness = 220;

    int brt;
    View serviceView;
    String serviceText = "";
    private int isValidBrightness(int brightness)
    {
//        if(serviceView == null)
//        {
//            serviceView = new View(mContext)
//            {
//                TextPaint textPaint;
//                public void onDraw(Canvas can)
//                {
//                    super.onDraw(can);
//
//                    if(textPaint == null)
//                    {
//                        textPaint = new TextPaint();
//                        textPaint.setTextSize(50);
//                        textPaint.setColor(android.graphics.Color.BLUE);
//                    }
//                    can.drawText(serviceText, 0, can.getHeight() - 100, textPaint);
//                }
//            };
//            serviceView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//            serviceView.setBackgroundColor(android.graphics.Color.argb(0,0,0,0));
//
//
//            mContext.runOnUiThread(new Runnable()
//            {
//                public void run()
//                {
//                    mParentView.addView(serviceView);
//                }
//            });
//        }
//
////        brt = brightness;
//        serviceText = "brightness : " + brightness;
//        serviceView.postInvalidate();

//        mContext.runOnUiThread(new Runnable()
//        {
//            @Override
//            public void run()
//            {
////                android.widget.Toast.makeText(mContext, "brightness is : " + brt, android.widget.Toast.LENGTH_LONG).show();
//                serviceText = "brightness : " + brt;
//            }
//        });

        if(brightness < minimumBrightness)
        {
//            mOpenCvCameraView.releaseExposure();
//            ProcessImage.getInstance().resetChromaKeys();
            return ProcessImage.DetectionResponseNotEnoughLighting;
        }
        else if(brightness > maximumBrightness)
        {
//            mOpenCvCameraView.releaseExposure();
//            ProcessImage.getInstance().resetChromaKeys();
            return ProcessImage.DetectionResponseTooMuchLighting;
        }
        return ProcessImage.DetectionResponseSuccess;
    }

    private boolean isSomethingWentWrong(String frameFileName)
    {
        try
        {
            Bitmap tempBitmap = BitmapFactory.decodeFile(frameFileName);
            if(tempBitmap != null)
            {
                tempBitmap.recycle();
                return false;
            }
            else return true;
        }
        catch (Throwable err)
        {
            return true;
        }
    }
//    public void onPause()
//    {
//        try
//        {
//            if(mCamera2View != null)
//            {
//                mCamera2View.onPause();
//            }
//        }
//        catch (Throwable err)
//        {
//        }
//    }
//
//    public void onResume()
//    {
//        try
//        {
//            if(mCamera2View != null)
//            {
//                mCamera2View.onResume();
//            }
//        }
//        catch (Throwable err)
//        {
//        }
//    }

}