package fm.bling.blingy.inviteFriends.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 31/01/17.
 * History:
 * ***********************************
 */
public class ContactsInviteeEmail extends CAAContactEmails {

    @SerializedName("video_id")
    private String videoId;

    public ContactsInviteeEmail(List<String> listContacts, String videoId) {
        super(listContacts);
        this.videoId = videoId;
    }
}
