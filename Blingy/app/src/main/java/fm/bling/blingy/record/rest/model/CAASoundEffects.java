package fm.bling.blingy.record.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 5/5/16.
 * History:
 * ***********************************
 */
public class CAASoundEffects
{
    @SerializedName("data")
    private ArrayList<CAAEffect> data;

    public ArrayList<CAAEffect> getData() {
        return data;
    }


    public  class CAAEffect{

        @SerializedName("name")
        String name;

        @SerializedName("url")
        String url;

        public String getName() {
            return name;
        }

        public String getUrl() {
            return url;
        }

    }

}
