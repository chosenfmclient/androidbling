//package fm.bling.blingy.mashup.listeners;
//
///**
// * *********************************
// * Project: Blin.gy Android Application
// * Description:
// * Created by Oren Zakay on 15/01/17.
// * History:
// * ***********************************
// */
//public interface StepsCallback {
//
//    void onFirstStepClicked();
//
//    void onSecondStepClicked();
//
//    void onThirdStepClicked();
//
//    void onFourthStepClicked();
//
//    void onShareStepClicked();
//}
