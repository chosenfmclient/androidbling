package fm.bling.blingy.record;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 24/04/2017.
 * History:
 * ***********************************
 */
public interface Camera2ViewInterface
{
    void setIsFrontCamera(boolean isFrontCamera);

    void setLayoutParams(android.view.ViewGroup.LayoutParams layoutParams);

    void setOnFrameAvailableListener(OnFrameAvailableListener onFrameAvailableListener);

    void setOnSurfaceReadyListener(Camera2View.OnSurfaceReadyListener onSurfaceReadyListener);

    boolean isSurfaceValid();

    void turnOffImageAdjustments(Camera2View.OnImageAdjustmentsOff onImageAdjustmentsOff);

    void close(OnCameraClosedListener onCameraClosedListener);

    interface OnCameraClosedListener
    {
        void onCameraClosed();
    }

}