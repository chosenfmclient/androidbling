package fm.bling.blingy.leaderboard;


import android.support.annotation.Nullable;

import java.util.ArrayList;

import fm.bling.blingy.rest.model.User;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 02/03/17.
 * History:
 * ***********************************
 */
public interface LeaderboardMVPR {

    interface View {

        void init();

        void openUserProfile(User user);

        void showLeaderboardData(ArrayList<User> users);

        void loadNextPage(@Nullable ArrayList<User> users);
    }

    interface Presenter {

        void setView(LeaderboardMVPR.View view);

        void getLeaderBoard(int page);

        void loadLeaderboardData(ArrayList<User> users);

        void loadNextPage(@Nullable ArrayList<User> users);
    }

    interface Model{

        void getLeaderBoard(int page);

        void loadLeaderboardData(ArrayList<User> users);

        void loadNextPage(@Nullable ArrayList<User> users);
    }

     interface Repository {

        void getLeaderBoard(int page);
    }
}
