package fm.bling.blingy.registration.rest.model;

import com.google.gson.annotations.SerializedName;


/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAToken
 * Description:
 * Created by Zach Gerstman on 6/7/15.
 * History:
 * ***********************************
 */
public class CAAEmailError {

    public CAAEmailErrorBody getEmail() {
        return email;
    }

    public void setEmail(CAAEmailErrorBody email) {
        this.email = email;
    }

    @SerializedName("email")
    private CAAEmailErrorBody email;


}
