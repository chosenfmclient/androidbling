package fm.bling.blingy.profile.model;

import com.google.gson.annotations.SerializedName;


import java.util.ArrayList;

import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.videoHome.model.CAAVideo;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 10/30/16.
 * History:
 * ***********************************
 */
public class UserProfile {

    @SerializedName("user")
    User user;

    @SerializedName("videos")
    ArrayList<CAAVideo> videos;

    public User getUser() {
        return user;
    }

    public ArrayList<CAAVideo> getVideos() {
        return videos;
    }
}
