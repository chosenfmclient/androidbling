package fm.bling.blingy.videoHome;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.io.IOException;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.database.handler.FollowingDataHandler;
import fm.bling.blingy.database.handler.LikesDataHandler;
import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.dialogs.LikeAnimationDialog;
import fm.bling.blingy.dialogs.flag.FlagVideoDialog;
import fm.bling.blingy.dialogs.listeners.FlagDialogListener;
import fm.bling.blingy.dialogs.listeners.UpdateLikeTotal;
import fm.bling.blingy.dialogs.model.CAAType;
import fm.bling.blingy.dialogs.share.ShareDialogVideoHome;

import fm.bling.blingy.duetHome.view.DuetHomeActivity;
import fm.bling.blingy.inviteFriends.InviteFriendsActivity;
import fm.bling.blingy.mashup.dialogs.CameoDialog;
import fm.bling.blingy.mashup.listeners.CreateCameoListener;
import fm.bling.blingy.profile.ProfileActivity;
import fm.bling.blingy.record.CreateVideoActivity;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.rest.model.CAALike;
import fm.bling.blingy.rest.model.CAAStatus;
import fm.bling.blingy.rest.model.PermissionLevel;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.songHome.SongHomeActivity;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.stateMachine.PermissionListener;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.views.ProgressBarView;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;
import fm.bling.blingy.utils.views.GlowRippleBackground;
import fm.bling.blingy.videoHome.model.CAAVideo;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/1/16.
 * History:
 * ***********************************
 */
public class VideoHomeActivity extends BaseActivity implements UpdateLikeTotal, FlagDialogListener, PermissionListener, CreateCameoListener {

    private static final String TAG = "VideoHomeActivity";
    private static final int PUBLIC_PERMISSION = 0;
    private static final int FRIENDS_ONLY_PERMISSION = 1;
    private static final int INVITED_ONLY_PERMISSION = 2;

    /**
     * Main Views
     */
    private Toolbar mToolbar;
    private View mSeparator;
    private FrameLayout mMainFrameLayout;
    private LinearLayout mUserDesc;
    private TextView mArtistName;
    private TextView mClipName;
    private TextView mShootNow;
    private TextView mDuetNow;
    private TextView mSeeSimilar;
    private TextView mLikesCount;
    private TextView mCommentsCount;
    private TextView mUserName;
    private TextView mDuetPermissionText;
    private ImageView mLikeButton;
    private ImageView mCommentButton;
    private ImageView mMoreButton;
    private ImageView mShareButton;
    private TextureView mClipTextureView;
    private ScaledImageView mClipImage;
    private ProgressBarView mProgressBar;
    private URLImageView mBlurImage;
    private ScaledImageView mUserPic;
    private FrameLayout mClipContainer;
    private FrameLayout mPermissionsFrame;
    private LinearLayout mSocialContainer;
    private LinearLayout mDetailsContainer;
    private LinearLayout mTileContainer;
    private LinearLayout mBottomLinearContainer;
    private MediaPlayer mMediaPlayer;
    private ImageView mPlayPauseButton;
    private ImageView mRightCornerIcon;
    private ImageView mCloseTutorialIcon;
    private LikeAnimationDialog likeDialog;
    private GlowRippleBackground mGlowRippleBackground;


    private GestureDetector gestureDetector;

    private CAAVideo mVideo;
    private String videoStatus;

    private boolean isME = false;
    private boolean isCreateCameo = false;
    private String mFrom;
    private boolean isPlaying = false;
    private boolean isPaused = false;
    private boolean isPrepared = false;
    private boolean isLoadingMusic = false;
    private boolean playDuetExplanation = false;
    //    private boolean newMashup = false;
    private boolean newCameo = false;
    private boolean fromInvite = false;
    private int orientation = Configuration.ORIENTATION_PORTRAIT;
    private int mPos = -1;


    private Animation slideDown, slideUp;
    private Animation fadeIn, fadeOut;
    private Animation slideActionButtonsAnim, slideInPermissionAnim;

    private Intent resultIntent;
    private BroadcastReceiver audioReceiver;
    private boolean isFirst = true;
    private boolean isAfterRecording = false;
    private boolean gotDuetPermissionData = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_home_layout);
        init();
        startMultiImageLoader();
        populateSong();
        UiChangeListener();
        resultIntent = getIntent();
        listenToAudioChanges();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        if (getIntent().hasExtra(Constants.POS))
            mPos = getIntent().getIntExtra(Constants.POS, -1);

        if (getIntent().hasExtra(Constants.FROM_INVITE))
            fromInvite = true;

        if (getIntent().hasExtra(Constants.FROM)) {
            String from = getIntent().getStringExtra(Constants.FROM);

            switch (from) {
                default:
                case Constants.VIDEO_PLAYER_ACTIVITY:
                    isAfterRecording = true;
                    videoStatus = getIntent().getStringExtra(Constants.STATUS);
                    if (!getIntent().getStringExtra(Constants.TYPE).equals(Constants.CAMEO_TYPE) && !videoStatus.equals(Constants.SECRET)) {
                        setupNewCameoButton();
                        //Read from Shared preferences to know the number of videos i uploaded
                        int videoCount = SharedPreferencesManager.getInstance().getInt(Constants.VIDEOS_CREATED, 0);
                        if (videoCount == 1 || (videoCount - 1) % 3 == 0) {
                            new CameoDialog(this, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (videoStatus.equals(Constants.PRIVATE)) {
                                        BaseDialog publishDialog = new BaseDialog(VideoHomeActivity.this, getString(R.string.video_is_private), getString(R.string.please_publish_your_video));
                                        publishDialog.setOKText(getString(R.string.publish));
                                        publishDialog.show();
                                        publishDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {
                                                if (((BaseDialog) dialog).getAnswer()) {
                                                    isCreateCameo = true;
                                                    uploadVideoStatus(Constants.PUBLIC);
                                                }
                                            }
                                        });
                                    } else {
                                        getContactsPermissions(VideoHomeActivity.this);
                                    }
                                }
                            }).show();
                        }
                    }

                    mFrom = Constants.VIDEO_PLAYER_ACTIVITY;

                    mClipImage.getLayoutParams().height = App.landscapitemHeight;
                    mClipImage.setMaxWidthInternal(App.WIDTH);
                    mClipImage.setUrl(Constants.thumbnail);
                    mClipImage.setDontSaveToFileCache(true);
                    mClipImage.setDontUseExisting(true);

                    mClipImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    mClipImage.setImageLoader(mMultiImageLoader);
                    mMultiImageLoader.DisplayImage(mClipImage);

                    mBlurImage.setUrl(Constants.thumbnail);
                    mBlurImage.setBlurRadius(60);
                    mBlurImage.setDontSaveToFileCache(true);
                    mBlurImage.setDontUseExisting(true);
                    mBlurImage.setMaxWidthInternal(App.WIDTH / 2);
                    mBlurImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    mBlurImage.setAnimResID(R.anim.fade_in);
                    mBlurImage.setPlaceHolder(R.drawable.game_thumb_placeholder);
                    mBlurImage.setImageLoader(mMultiImageLoader);
                    mBlurImage.enableMajorColorLayer(true);
                    mMultiImageLoader.DisplayImage(mBlurImage);

                    mUserPic.setUrl(CAAUserDataSingleton.getInstance().getPhotoUrl());
                    mUserPic.setAnimResID(R.anim.fade_in);
                    mUserPic.setIsRoundedImage(true);
                    mUserPic.setKeepAspectRatioAccordingToWidth(true);
                    mUserPic.setImageLoader(mMultiImageLoader);
                    mMultiImageLoader.DisplayImage(mUserPic);

                    mUserName.setText(CAAUserDataSingleton.getInstance().getFullName());
                    mShootNow.setVisibility(View.GONE);
                    mArtistName.setText(getIntent().getStringExtra(Constants.ARTIST_NAME));
                    mClipName.setText(getIntent().getStringExtra(Constants.CLIP_NAME));

                    if (getIntent().getStringExtra(Constants.STATUS).equals(Constants.SECRET)) {
                        getSupportActionBar().setTitle("Submission");
                    } else {
                        mDetailsContainer.setVisibility(View.VISIBLE);
                        mSocialContainer.setVisibility(View.VISIBLE);
                        getSupportActionBar().setTitle(getIntent().getStringExtra(Constants.CLIP_NAME));
                    }
                    break;
            }
        }

        fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        fadeIn.setFillAfter(true);
        fadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        fadeOut.setFillAfter(true);

        if (slideDown == null) {
            slideDown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
            slideDown.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mDetailsContainer.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
        if (slideUp == null) {
            slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
            slideUp.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mDetailsContainer.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    private void setupNewCameoButton() {
        newCameo = true;
        mShootNow.setText(R.string.invite_to_duet);
        mShootNow.setBackgroundResource(R.drawable.button_shape_pink);
        mShootNow.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_cameo_invite_24dp), null, null, null);
        mShootNow.setCompoundDrawablePadding((int) (25f * App.SCALE_X));
    }

    @Override
    protected void onStart() {
        super.onStart();
        hideNavBar();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            showLandScape();

        if (!isFirst && mUserPic != null) {
            mUserPic.setImageLoader(getMultiImageLodaer());
            mClipImage.setImageLoader(getImageLoaderManager());
            mBlurImage.setImageLoader(getMultiImageLodaer());
        }

        if (mVideo != null && !mVideo.getUrl().isEmpty() && !isPaused)
            handleSingleTap();
        else
            mClipImage.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if (playDuetExplanation)
        {
            mHandler.removeCallbacksAndMessages(null);
            playDuetExplanation = false;
            handleSingleTap();
            fadeInRightIcon();
            fadeOutCloseButton();
        }
        if (mMediaPlayer != null)
        {
            if (isPlaying)
            {
                mMediaPlayer.pause();
                isPlaying = false;
            }
            else
            {
                releaseMediaPlayer();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        isFirst = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (audioReceiver != null)
            unregisterReceiver(audioReceiver);
        releaseMediaPlayer();
    }

    private void init() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        mToolbar = getActionBarToolbar();
        if (mToolbar != null) {
            ((FrameLayout.LayoutParams) mToolbar.getLayoutParams()).topMargin = App.STATUS_BAR_HEIGHT;
            mToolbar.setBackgroundResource(R.color.transparent);
            mToolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        mMainFrameLayout = (FrameLayout) findViewById(R.id.home_tile_container);
        mArtistName = (TextView) findViewById(R.id.artist_name);
        mClipName = (TextView) findViewById(R.id.clip_name);
        mShootNow = (TextView) findViewById(R.id.select_button);
        mDuetNow = (TextView) findViewById(R.id.duet_button);
        mSeeSimilar = (TextView) findViewById(R.id.see_similar);
        mClipTextureView = (TextureView) findViewById(R.id.clip_texture_view);
        mProgressBar = (ProgressBarView) findViewById(R.id.progress_bar);
        mProgressBar.updateLayout();
        mClipImage = (ScaledImageView) findViewById(R.id.clip_image);
        mBlurImage = (URLImageView) findViewById(R.id.blured_image);
        mLikeButton = (ImageView) findViewById(R.id.like_button);
        mCommentButton = (ImageView) findViewById(R.id.comment_button);
        mLikesCount = (TextView) findViewById(R.id.likes_count);
        mCommentsCount = (TextView) findViewById(R.id.comments_count);
        mDuetPermissionText = (TextView) findViewById(R.id.permission_text);
        mUserPic = (ScaledImageView) findViewById(R.id.user_pic);
        mUserName = (TextView) findViewById(R.id.user_name);
        mMoreButton = (ImageView) findViewById(R.id.more_button);
        mShareButton = (ImageView) findViewById(R.id.share_button);
        mClipContainer = (FrameLayout) findViewById(R.id.clip_container);
        mPermissionsFrame = (FrameLayout) findViewById(R.id.permission_frame);
        mSocialContainer = (LinearLayout) findViewById(R.id.social_container);
        mDetailsContainer = (LinearLayout) findViewById(R.id.details_container);
        mBottomLinearContainer = (LinearLayout) findViewById(R.id.bottom_linear_container);
        mUserDesc = (LinearLayout) findViewById(R.id.user_line_container);
        mTileContainer = (LinearLayout) findViewById(R.id.home_tile_card);
        mPlayPauseButton = (ImageView) findViewById(R.id.play_pause_button);
        mRightCornerIcon = (ImageView) findViewById(R.id.right_corner_icon);
        mCloseTutorialIcon = (ImageView) findViewById(R.id.close_tutorial);
        mGlowRippleBackground = (GlowRippleBackground) findViewById(R.id.glow_ripple);
        mSeparator = findViewById(R.id.line_separator);

        ((FrameLayout.LayoutParams) mSeparator.getLayoutParams()).topMargin = App.STATUS_BAR_HEIGHT + App.TOOLBAR_HEIGHT;
        ((FrameLayout.LayoutParams) mTileContainer.getLayoutParams()).topMargin = App.STATUS_BAR_HEIGHT + App.TOOLBAR_HEIGHT;

        mMainFrameLayout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mClipTextureView.getLayoutParams().height = mClipImage.getLayoutParams().height = App.landscapitemHeight;
        mClipTextureView.getLayoutParams().width = mClipImage.getLayoutParams().width = App.WIDTH;

        mPlayPauseButton = new ImageView(this);
        FrameLayout.LayoutParams playParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        playParams.gravity = Gravity.CENTER;
        mPlayPauseButton.setLayoutParams(playParams);
        mPlayPauseButton.setImageResource(R.drawable.video_play);
        mPlayPauseButton.setVisibility(View.GONE);
        mClipContainer.addView(mPlayPauseButton, 2);

        if (slideActionButtonsAnim == null) {
            slideActionButtonsAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
            slideInPermissionAnim = AnimationUtils.loadAnimation(this,R.anim.slide_up);
        }

    }

    private void getVideo(String videoID) {
        App.getUrlService().getVideoPreview(videoID, new Callback<CAAVideo>() {
            @Override
            public void success(CAAVideo video, Response response) {
                mVideo = video;
                populateSong();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void getVideoMoreData(String videoId) {
        App.getUrlService().getVideoPreview(videoId, new Callback<CAAVideo>() {
            @Override
            public void success(CAAVideo video, Response response) {
                mVideo = video;
                showActionButtons();
            }

            @Override
            public void failure(RetrofitError error) {
                showActionButtons();
            }
        });
    }

    private void showActionButtons()
    {
        if (CAAUserDataSingleton.getInstance().isMe(mVideo.getUser().getUserId()))
        {
            isME = true;

            if (!mVideo.getType().equals(Constants.CAMEO_TYPE) && (getIntent().getStringExtra(Constants.TYPE) == null || !getIntent().getStringExtra(Constants.TYPE).equals(Constants.UPLOAD)) )
            {
                setPermissionText(mVideo.getPermissionLevel());
                mPermissionsFrame.setVisibility(View.VISIBLE);
                mPermissionsFrame.startAnimation(slideInPermissionAnim);
                mPermissionsFrame.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        openPermissionPopupMenu();
                    }
                });
            }

            if (!mVideo.getType().equals(Constants.CAMEO_TYPE))
                setupNewCameoButton();

            mShootNow.setVisibility(View.VISIBLE);
            mShootNow.startAnimation(slideActionButtonsAnim);
        }
        else
        {
            if (mVideo.isCameoPermission())
            {
                mDuetNow.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        onCreateCameo(mVideo);
                    }
                });

                mShootNow.setText(R.string.shoot);
                mShootNow.setVisibility(View.VISIBLE);
                mDuetNow .setVisibility(View.VISIBLE);
                mShootNow.startAnimation(slideActionButtonsAnim);
                mDuetNow .startAnimation(slideActionButtonsAnim);
            }
            else
            {
                mShootNow.setVisibility(View.VISIBLE);
                mShootNow.startAnimation(slideActionButtonsAnim);
            }
        }
    }

    private void populateSong() {
        Bundle bundle = getIntent().getBundleExtra(Constants.BUNDLE);
        if (mVideo == null) {
            if (bundle == null) {
                getVideo(getIntent().getStringExtra(Constants.VIDEO_ID));
                gotDuetPermissionData = true;
                return;
            } else {
                mVideo = bundle.getParcelable(Constants.CAAVIDEO);
                getVideoMoreData(mVideo.getVideoId());
                gotDuetPermissionData = false;
            }
        }

        videoStatus = mVideo.getStatus();

        if(gotDuetPermissionData)
            showActionButtons();

        if (mVideo.getType().equalsIgnoreCase(Constants.CAMEO_TYPE)) {
            mRightCornerIcon.setImageResource(R.drawable.ic_cameo_24dp);
            mRightCornerIcon.setVisibility(View.VISIBLE);
            mSeeSimilar.setText(R.string.view_other_duets);

        } else {
            mSeeSimilar.setText(R.string.more_like_this);
        }

        mShootNow.setPadding(App.MAIN_MARGIN, App.BLOCK_MARGIN, App.MAIN_MARGIN, App.BLOCK_MARGIN);


        if (mVideo != null && !mVideo.getUrl().isEmpty() && !isPaused)
            handleSingleTap();


        if (mMultiImageLoader == null)
            mMultiImageLoader = getMultiImageLodaer();


        if (!videoStatus.equalsIgnoreCase(Constants.SECRET)) {
            mDetailsContainer.setVisibility(View.VISIBLE);
            mSocialContainer.setVisibility(View.VISIBLE);
        }

        mArtistName.setText(mVideo.getArtistName());
        mClipName.setText(mVideo.getSongName());
        mUserName.setText(mVideo.getUser().getFullName());
        mLikesCount.setText(mVideo.getTotalLikes() == 0 && LikesDataHandler.getInstance().contains(mVideo.getVideoId()) ? "1" : mVideo.getTotalLikes() + "");
        mCommentsCount.setText(mVideo.getTotalComments() + "");

        if (videoStatus.equals(Constants.SECRET))
            getSupportActionBar().setTitle("Submission");
        else
            getSupportActionBar().setTitle(mVideo.getSongName());

        View.OnClickListener profileClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileIntent = new Intent(VideoHomeActivity.this, ProfileActivity.class);
                profileIntent.putExtra(Constants.USER_ID, mVideo.getUser().getUserId());
                startActivity(profileIntent);
            }
        };

        if (mFrom == null || !mFrom.equalsIgnoreCase(Constants.VIDEO_PLAYER_ACTIVITY)) {
            mUserPic.setUrl(mVideo.getUser().getPhoto());
            mUserPic.setAnimResID(R.anim.fade_in);
            mUserPic.setIsRoundedImage(true);
            mUserPic.setKeepAspectRatioAccordingToWidth(true);
            mUserPic.setImageLoader(mMultiImageLoader);
            getMultiImageLodaer().DisplayImage(mUserPic);

//            mClipImage.setFixedSize(true);
//            mClipImage.setDontUseExisting(true);
            mClipImage.setUrl(mVideo.getFirstFrame());
            mClipImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            mClipImage.setAspectRatio(4f / 3f);
            mClipImage.setImageLoader(mMultiImageLoader);
            mMultiImageLoader.DisplayImage(mClipImage);

            mBlurImage.setUrl(mVideo.getFirstFrame());
            mBlurImage.setBlurRadius(60);
//            mBlurImage.setDontUseExisting(true);
            mBlurImage.setMaxWidthInternal(App.WIDTH / 2);
            mBlurImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            mBlurImage.setAnimResID(R.anim.fade_in);
            mBlurImage.setPlaceHolder(R.drawable.game_thumb_placeholder);
            mBlurImage.setImageLoader(mMultiImageLoader);
            mBlurImage.enableMajorColorLayer(true);
            mMultiImageLoader.DisplayImage(mBlurImage);

        } else {
            mClipImage.setUrl(mVideo.getFirstFrame());
            mBlurImage.setUrl(mVideo.getFirstFrame());
        }

        mUserPic.setOnClickListener(profileClickListener);
        mUserName.setOnClickListener(profileClickListener);

        if (LikesDataHandler.getInstance().contains(mVideo.getVideoId()))
            mLikeButton.setImageResource(R.drawable.ic_liked_24dp);

        mLikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (LikesDataHandler.getInstance().contains(mVideo.getVideoId())) {
                    ((ImageView) view).setImageDrawable(getResources().getDrawable(R.drawable.ic_like_24dp));
                    mVideo.setLikedByMe(0);
                    mVideo.setTotalLikes(mVideo.getTotalLikes() - 1);
                    if (mLikesCount != null) {
                        mLikesCount.setText(String.valueOf(mVideo.getTotalLikes()));
                    }
                    unLikeVideo(mVideo);
                } else {
                    ((ImageView) view).setImageDrawable(getResources().getDrawable(R.drawable.ic_liked_24dp));
                    mVideo.setLikedByMe(1);
                    mVideo.setTotalLikes(mVideo.getTotalLikes() + 1);
                    if (mLikesCount != null) {
                        mLikesCount.setText(String.valueOf(mVideo.getTotalLikes()));
                    }
                    likeVideo(mVideo);
                    int[] pos = new int[2];
                    mClipTextureView.getLocationOnScreen(pos);
                    likeAnimation(pos);
                }
            }
        });


        mLikesCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent socialLike = new Intent(VideoHomeActivity.this, VideoSocialDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.CAAVIDEO, mVideo);
                socialLike.putExtra(Constants.BUNDLE, bundle);
                socialLike.putExtra(Constants.SELECTED_TAB, VideoSocialDetailsActivity.LIKES_POS);
                startActivityForResult(socialLike, Constants.REQ_VIDEO_UPDATE);
            }
        });

        View.OnClickListener commentClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent socialComment = new Intent(VideoHomeActivity.this, VideoSocialDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.CAAVIDEO, mVideo);
                bundle.putInt(Constants.SELECTED_TAB, VideoSocialDetailsActivity.COMMENTS_POS);
                socialComment.putExtra(Constants.BUNDLE, bundle);
                socialComment.putExtra(Constants.SELECTED_TAB, VideoSocialDetailsActivity.COMMENTS_POS);
                startActivityForResult(socialComment, Constants.REQ_VIDEO_UPDATE);
            }
        };

        mCommentButton.setOnClickListener(commentClick);
        mCommentsCount.setOnClickListener(commentClick);

        mShareButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (Utils.canClick())
                {
                    ShareDialogVideoHome shareDialog = new ShareDialogVideoHome(VideoHomeActivity.this, VideoHomeActivity.this, mVideo, false);
                    shareDialog.show();
                }
            }
        });

        mMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final String userID = mVideo.getUser().getUserId();
                final boolean followedByMe = FollowingDataHandler.getInstance().contains(userID);
//                App.getUrlService().getDuetPermission(userID, new Callback<DuetPermission>() {
//                    @Override
//                    public void success(DuetPermission duetPermission, Response response) {
////                            Constants.sFollowers.put(user_id, duetPermission.hasPermission());
                createAndShowMenu(view, userID, followedByMe, mVideo.isCameoPermission());
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
//                    }
//                });
            }
        });

        mShootNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newCameo) {
                    if (videoStatus.equals(Constants.PRIVATE)) {
                        BaseDialog publishDialog = new BaseDialog(VideoHomeActivity.this, getString(R.string.video_is_private), getString(R.string.please_publish_your_video));
                        publishDialog.setOKText(getString(R.string.publish));
                        publishDialog.show();
                        publishDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                if (((BaseDialog) dialog).getAnswer()) {
                                    isCreateCameo = true;
                                    uploadVideoStatus(Constants.PUBLIC);
                                }
                            }
                        });
                    } else {
                        getContactsPermissions(VideoHomeActivity.this);
                    }
                } else if (fromInvite) {
                    PopupMenu popup = new PopupMenu(VideoHomeActivity.this, v);
                    popup.getMenuInflater().inflate(R.menu.duet_popup_menu, popup.getMenu());
                    MenuItem makeCameo = popup.getMenu().findItem(R.id.create_cameo);
                    makeCameo.setTitle(makeCameo.getTitle() + " " + mVideo.getUser().getFullName());
                    popup.setOnMenuItemClickListener(
                            new PopupMenu.OnMenuItemClickListener() {
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.create_cameo:
                                            onCreateCameo(mVideo);
                                            String parentClipid = mVideo.getParent() == null ? mVideo.getClipId() : mVideo.getParent().getClipId();
                                            TrackingManager.getInstance().tapMakeCameo(mVideo.getVideoId(), parentClipid, mVideo.getArtistName(), mVideo.getSongName(), "invitee");
                                            return true;
                                        case R.id.record_your_video:
                                            recordVideo();
                                            return true;
                                        default:
                                            return true;
                                    }
                                }
                            }
                    );
                    popup.show();
                } else {
                    recordVideo();
                }
            }
        });

        View.OnClickListener moveToSonghomeListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.canClick()) {
                    Intent discover = new Intent(VideoHomeActivity.this, SongHomeActivity.class);
                    discover.putExtra(Constants.IS_SONG_HOME, true);
                    discover.putExtra(Constants.CLIP_ID, mVideo.getClipId());
                    startActivity(discover);
                }
            }
        };


        mArtistName.setOnClickListener(moveToSonghomeListener);
        mClipName.setOnClickListener(moveToSonghomeListener);
        if (mVideo.getType().equalsIgnoreCase(Constants.CAMEO_TYPE)) {
            mSeeSimilar.setText(R.string.view_other_duets);
            mSeeSimilar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.canClick()) {
                        Intent duetHome = new Intent(VideoHomeActivity.this, DuetHomeActivity.class);
                        duetHome.putExtra(Constants.VIDEO_ID, mVideo.getVideoId());
                        startActivity(duetHome);
                    }
                }
            });
        } else {
            mSeeSimilar.setOnClickListener(moveToSonghomeListener);
        }
        gestureDetector = new GestureDetector(this, new GestureListener());

        mClipTextureView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

        if (getIntent().hasExtra(Constants.IS_NOTIFICATION)) {
            getIntent().removeExtra(Constants.IS_NOTIFICATION);
            mCommentButton.callOnClick();
        }

        mRightCornerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mVideo.getType().equalsIgnoreCase(Constants.CAMEO_TYPE)) {
                    playDuetExplanation = true;
                    showTutorialImage();
                }
            }
        });

        mCloseTutorialIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClipImage.setBackgroundResource(0);
                mMultiImageLoader.DisplayImage(mClipImage);
                mHandler.removeCallbacksAndMessages(null);
                playDuetExplanation = false;
                handleSingleTap();
                fadeInRightIcon();
                fadeOutCloseButton();
            }
        });
    }


    private void openPermissionPopupMenu() {
        PopupMenu popup = new PopupMenu(VideoHomeActivity.this, findViewById(R.id.permission_arrow));
        popup.getMenuInflater().inflate(R.menu.duet_pemission_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(
                new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.duet_friendly:
                                postDuetPermissionLevel(PUBLIC_PERMISSION);
                                return true;
                            case R.id.duet_with_friends_only:
                                postDuetPermissionLevel(FRIENDS_ONLY_PERMISSION);
                                return true;
                            case R.id.duet_by_invite:
                                postDuetPermissionLevel(INVITED_ONLY_PERMISSION);
                                return true;

                            default:
                                return true;
                        }
                    }
                }
        );
        popup.show();
    }

    private void postDuetPermissionLevel(final int level) {
        setPermissionText(level);
        resultIntent.putExtra(Constants.POS, mPos);
        resultIntent.putExtra(Constants.PERMISSION_LEVEL, level);
        setResult(Activity.RESULT_OK, resultIntent);
        App.getUrlService().putDuetPermissionLevel(mVideo.getVideoId(), new PermissionLevel(level), new Callback<String>() {
            @Override
            public void success(String s, Response response) {

                mVideo.setPermissionLevel(level);
            }

            @Override
            public void failure(RetrofitError error) {
                setPermissionText(level);
            }
        });
    }

    private void setPermissionText(int level) {
        switch (level) {
            default:
            case PUBLIC_PERMISSION:
                mDuetPermissionText.setText(R.string.duet_friendly_video);
                break;
            case FRIENDS_ONLY_PERMISSION:
                mDuetPermissionText.setText(R.string.duet_with_friends_only);
                break;
            case INVITED_ONLY_PERMISSION:
                mDuetPermissionText.setText(R.string.duet_by_invite_only);
                break;
        }
    }

    private void createAndShowMenu(View view, final String userID, final boolean followedByMe, boolean hasPermission) {
        if (userID != null) {
            PopupMenu popup = new PopupMenu(VideoHomeActivity.this, view);
            popup.getMenuInflater().inflate(R.menu.video_home_popup_menu, popup.getMenu());
            if (mVideo.getType().equals(Constants.CAMEO_TYPE)) {
                popup.getMenu().findItem(R.id.view_original_video).setVisible(true);
            } else {
                if (!videoStatus.equals(Constants.SECRET)) {
                    if (isME) {
                        MenuItem createCameo = popup.getMenu().findItem(R.id.create_cameo);
                        String userName = "yourself";
                        createCameo.setTitle(createCameo.getTitle() + " with " + userName);
                        createCameo.setVisible(true);
                    } else {
                        if (hasPermission) {
                            MenuItem createCameo = popup.getMenu().findItem(R.id.create_cameo);
                            String userName = mVideo.getUser().getFullName();
                            createCameo.setTitle(createCameo.getTitle() + " with " + userName);
                            createCameo.setVisible(true);
                        }
                    }
                }
            }

            if (isME) {
                popup.getMenu().findItem(R.id.action_flag_video).setVisible(false);
                if (!videoStatus.equalsIgnoreCase(Constants.SECRET)) {
                    if (videoStatus.equalsIgnoreCase(Constants.PRIVATE))
                        popup.getMenu().findItem(R.id.action_private).setTitle("Make public").setVisible(true);
                    else
                        popup.getMenu().findItem(R.id.action_private).setTitle("Make private").setVisible(true);
                }
                popup.getMenu().findItem(R.id.action_delete).setVisible(true);
                popup.getMenu().findItem(R.id.action_follow).setVisible(false);
            } else {
                if (followedByMe) {
                    popup.getMenu().findItem(R.id.action_follow).setTitle("Unfollow " + mVideo.getUser().getFullName()).setVisible(true);
                } else {
                    popup.getMenu().findItem(R.id.action_follow).setTitle("Follow " + mVideo.getUser().getFullName()).setVisible(true);
                }
            }
            popup.setOnMenuItemClickListener(
                    new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.view_original_video:
                                    String parentClipid = mVideo.getParent() == null ? mVideo.getClipId() : mVideo.getParent().getClipId();
                                    TrackingManager.getInstance().tapViewOriginal(mVideo.getVideoId(), parentClipid, mVideo.getArtistName(), mVideo.getSongName());
                                    viewOriginalVideo(mVideo.getParent());
                                    return true;
                                case R.id.create_cameo:
                                    onCreateCameo(mVideo);
                                    String parentClipidSec = mVideo.getParent() == null ? mVideo.getClipId() : mVideo.getParent().getClipId();
                                    TrackingManager.getInstance().tapMakeCameo(mVideo.getVideoId(), parentClipidSec, mVideo.getArtistName(), mVideo.getSongName(), "other");
                                    return true;
                                case R.id.action_flag_video:
                                    showFlagVideoDialog();
                                    return true;
                                case R.id.action_delete:
                                    deleteVideo();
                                    return true;
                                case R.id.action_private:
                                    if (videoStatus.equalsIgnoreCase("public"))
                                        uploadVideoStatus("private");
                                    else
                                        uploadVideoStatus("public");
                                    return true;
                                case R.id.action_follow:
                                    if (followedByMe) {
                                        mVideo.getUser().setFollowedByMe("0");
                                        unFollowUser(userID);
                                    } else {
                                        mVideo.getUser().setFollowedByMe("1");
                                        followUser(userID);
                                    }
                                    return true;
                                default:
                                    return true;
                            }
                        }
                    }
            );
            popup.show();
        }
    }

    private void viewOriginalVideo(CAAVideo parent) {
        switch (parent.getStatus()) {
            case Constants.PUBLIC:
                Intent originalVideo = new Intent(VideoHomeActivity.this, VideoHomeActivity.class);
                originalVideo.putExtra(Constants.VIDEO_ID, parent.getVideoId());
                startActivity(originalVideo);
                break;
            case Constants.PRIVATE:
                BaseDialog privateDialog = new BaseDialog(VideoHomeActivity.this, getString(R.string.video_is_private), getString(R.string.original_video_is_private));
                privateDialog.show();
                privateDialog.removeCancelbutton();
                break;
            case Constants.DELETED:
                BaseDialog deletedDialog = new BaseDialog(VideoHomeActivity.this, getString(R.string.video_deleted_title), getString(R.string.original_video_deleted));
                deletedDialog.removeCancelbutton();
                deletedDialog.show();
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            showLandScape();
        } else {
            showPortrait();
        }
    }

//    private void createNewMashup(){
//        App.getUrlService().postMashup(new PostMashup(mVideo.getVideoId()), new Callback<MashupOBJ>() {
//            @Override
//            public void success(MashupOBJ mashupOBJ, Response response) {
//                mMashupOBJ = mashupOBJ;
//                if(mashupOBJ != null) {
//                    getContactsPermissions(VideoHomeActivity.this);
////                    movetoDuetInvites(mashupOBJ);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//            }
//        });
//    }

//    private void moveToMashupHome(String mashupID) {
//        Intent mashupHome = new Intent(this, MashupHomeActivity.class);
//        mashupHome.putExtra(Constants.MASHUP_ID, mashupID);
//        startActivity(mashupHome);
//    }

    private void movetoDuetInvites() {

        //The is me on first creation with first video.
//        StateMachine.getInstance(this).insertMashup(mashupOBJ.getMashupID(), 2, 1);
        Intent inviteCameo = new Intent(this, InviteFriendsActivity.class);
        inviteCameo.putExtra(Constants.TYPE, Constants.CAMEO_TYPE);
        inviteCameo.putExtra(Constants.VIDEO_ID, mVideo.getVideoId());
        startActivity(inviteCameo);

    }

    private void showLandScape() {
        if (!isPaused) {
            handleStatusbar(true);
            mDetailsContainer.setVisibility(View.GONE);
        }

        orientation = Configuration.ORIENTATION_LANDSCAPE;
        int videoWidth = (int) (((float) getResources().getDisplayMetrics().widthPixels) * (3f / 4f));
        mClipTextureView.getLayoutParams().height = mClipImage.getLayoutParams().height = App.WIDTH;
        mClipTextureView.getLayoutParams().width = mClipImage.getLayoutParams().width = videoWidth;

        mRightCornerIcon.setX(videoWidth / 2);

        mBottomLinearContainer.setVisibility(View.GONE);
//        mBottomBar.setVisibility(View.GONE);
        mUserDesc.setVisibility(View.GONE);

        if (mVideo != null && !videoStatus.equalsIgnoreCase(Constants.SECRET)) {
            ((ViewGroup) mDetailsContainer.getParent()).removeView(mDetailsContainer);
            mClipContainer.addView(mDetailsContainer);
            mDetailsContainer.setBackgroundResource(R.color.black_60);
            mSocialContainer.setVisibility(View.GONE);
            ((FrameLayout.LayoutParams) mDetailsContainer.getLayoutParams()).gravity = Gravity.BOTTOM;
        }


        mClipContainer.setBackgroundResource(R.color.transparent);
        mToolbar.setVisibility(View.GONE);
        mSeparator.setVisibility(View.GONE);
        FrameLayout.LayoutParams tileLayoutParams = ((FrameLayout.LayoutParams) mTileContainer.getLayoutParams());
        tileLayoutParams.topMargin = 0;
        tileLayoutParams.bottomMargin = 0;

    }

    private void showPortrait() {
        handleStatusbar(false);

        mBottomLinearContainer.setVisibility(View.VISIBLE);
//        mBottomBar.setVisibility(View.VISIBLE);
        orientation = Configuration.ORIENTATION_PORTRAIT;
        mToolbar.setVisibility(View.VISIBLE);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mRightCornerIcon != null)
                    mRightCornerIcon.setX(((float) (App.WIDTH - mRightCornerIcon.getWidth() - App.BLOCK_MARGIN)));
            }
        }, 100);

        if (mVideo != null && !videoStatus.equalsIgnoreCase(Constants.SECRET)) {
            mDetailsContainer.setVisibility(View.VISIBLE);
            mDetailsContainer.setBackgroundResource(R.color.transparent);
            ((ViewGroup) mDetailsContainer.getParent()).removeView(mDetailsContainer);
            mClipContainer.setBackgroundResource(R.color.black);
            mTileContainer.addView(mDetailsContainer, 2);
            mSocialContainer.setVisibility(View.VISIBLE);
        }

        mUserDesc.setVisibility(View.VISIBLE);
        mClipTextureView.getLayoutParams().height = mClipImage.getLayoutParams().height = App.landscapitemHeight;
        mClipTextureView.getLayoutParams().width = mClipImage.getLayoutParams().width = App.WIDTH;
        mSeparator.setVisibility(View.VISIBLE);
        FrameLayout.LayoutParams tileLayoutParams = ((FrameLayout.LayoutParams) mTileContainer.getLayoutParams());
        tileLayoutParams.topMargin = App.STATUS_BAR_HEIGHT + App.TOOLBAR_HEIGHT;
        tileLayoutParams.bottomMargin = App.TOOLBAR_HEIGHT;
    }

    private void showFlagVideoDialog() {
        FlagVideoDialog flagVideoDialog = new FlagVideoDialog(this, mVideo.getVideoId(), this);
        flagVideoDialog.show();
    }

    private void deleteVideo() {
        BaseDialog dialog = new BaseDialog(this, "Are you sure?", "Are you sure you want to delete this " + mVideo.getMediaType() + "?");
        dialog.setOKText("DELETE");
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (((BaseDialog) dialogInterface).getAnswer()) {
                    App.getUrlService().deleteVideo(mVideo.getVideoId(), new Callback<String>() {
                        @Override
                        public void success(String s, Response response) {
                            resultIntent.putExtra(Constants.REMOVE_POS, mPos);
                            setResult(Activity.RESULT_OK, resultIntent);
                            finish();
                        }

                        @Override
                        public void failure(RetrofitError error) {

                        }
                    });
                }
            }
        });
    }


    private void uploadVideoStatus(final String status)
    {
        App.getUrlService().putUploadVideoStatus(mVideo.getVideoId(), new CAAStatus(status), new Callback<CAAVideo>() {
            @Override
            public void success(CAAVideo caaVideo, Response response) {
                videoStatus = caaVideo.getStatus();
                mVideo.setStatus(caaVideo.getStatus());
                resultIntent.putExtra(Constants.POS, mPos);
                resultIntent.putExtra(Constants.STATUS, caaVideo.getStatus());
                setResult(Activity.RESULT_OK, resultIntent);
                if (isCreateCameo) {
                    isCreateCameo = false;
                    getContactsPermissions(VideoHomeActivity.this);
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    @Override
    public void onUpdateLikeTotal(int pos) {
        mLikesCount.setText(mVideo.getTotalLikes() + "");
        mLikeButton.setImageResource(R.drawable.ic_liked_24dp);
    }

    @Override
    public void onFinishFlagVideoDialog(boolean success) {
        BaseDialog dialog = new BaseDialog(this, "Blingy", "Thank you for the report");
        dialog.removeCancelbutton();
        dialog.show();
    }

    @Override
    public void onPermissionGranted() {
        movetoDuetInvites();
    }

    @Override
    public void onPermissionDenied() {
    }

    @Override
    public void onCreateCameo(CAAVideo video) {
        Intent recordVideo = new Intent(this, CreateVideoActivity.class);
        recordVideo.putExtra(Constants.PARENT_ID, video.getVideoId());
        recordVideo.putExtra(Constants.TYPE, Constants.CAMEO_TYPE);
        recordVideo.putExtra(Constants.CLIP_ID, video.getParent() == null ? video.getClipId() : video.getParent().getClipId());
        recordVideo.putExtra(Constants.CLIP_URL, video.getUrl());
        recordVideo.putExtra(Constants.CLIP_THUMBNAIL, video.getSongClipUrl());
        recordVideo.putExtra(Constants.SONG_NAME, video.getSongName());
        recordVideo.putExtra(Constants.ARTIST_NAME, video.getArtistName());

        if (isAfterRecording)
            recordVideo.putExtra(Constants.FROM, Constants.VIDEO_PLAYER_ACTIVITY);
        getRecordPermissions(recordVideo);
    }

    private void recordVideo() {
        Intent intent = new Intent(VideoHomeActivity.this, CreateVideoActivity.class);
        intent.putExtra(Constants.CLIP_URL, mVideo.getSongClipUrl());
        intent.putExtra(Constants.CLIP_THUMBNAIL, mVideo.getSongImageUrl());
        intent.putExtra(Constants.SONG_NAME, mVideo.getSongName());
        intent.putExtra(Constants.ARTIST_NAME, mVideo.getArtistName());
        intent.putExtra(Constants.CLIP_ID, mVideo.getParent() == null ? mVideo.getClipId() : mVideo.getParent().getClipId());
        TrackingManager.getInstance().tapShootNow("video_home", mVideo.getVideoId(), mVideo.getArtistName(), mVideo.getSongName(), 0);
        getRecordPermissions(intent);
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            handleSingleTap();
            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if (!mVideo.getStatus().equals(Constants.SECRET))
                handleDoubleTap();
            return true;
        }
    }

    private void handleSingleTap() {
        if (playDuetExplanation)
            return;

        if (!isLoadingMusic) {
            if (mMediaPlayer != null) {
                if (isPlaying) {
                    isPlaying = false;
                    isPaused = true;
                    mMediaPlayer.pause();
                    mPlayPauseButton.bringToFront();
                    mPlayPauseButton.setVisibility(View.VISIBLE);
                    if (orientation == Configuration.ORIENTATION_LANDSCAPE)
                        handleContent(false);

                } else {
                    if (orientation == Configuration.ORIENTATION_LANDSCAPE)
                        handleContent(true);
                    if (isPrepared) {
                        isPaused = false;
                        isPlaying = true;
                        mMediaPlayer.start();
                        mClipImage.setVisibility(View.INVISIBLE);
                        mPlayPauseButton.setVisibility(View.GONE);
                        gainAudioFocus();
                    } else
                        MusicPrepare();
                }
            } else {
                MusicPrepare();
            }
        }
    }

    private void handleDoubleTap() {
        int[] pos = new int[2];
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            pos[0] = App.HEIGHT / 2;
            pos[1] = App.WIDTH / 2;
        } else {
            mClipTextureView.getLocationOnScreen(pos);
        }

        likeAnimation(pos);
    }

    private void likeAnimation(int[] pos) {
        if (likeDialog != null)
            likeDialog.dismiss();
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            pos[0] += (App.landscapItemWidth / 2);
            pos[1] += (App.landscapitemHeight / 2);
        } else {
            pos[0] = (App.HEIGHT / 2);
            pos[1] = (App.WIDTH / 2);
        }
        likeDialog = new LikeAnimationDialog(this, mVideo, pos, this, -1);
        likeDialog.show();
    }

    private void likeVideo(final CAAVideo video) {
        CAAType type = new CAAType("video_home");
        LikesDataHandler.getInstance().add(video.getVideoId());
        TrackingManager.getInstance().tapLikeButton(video.getVideoId(), "video_home", true);
        App.getUrlService().postLikeVideo(video.getVideoId(), type, new Callback<CAALike>() {
            @Override
            public void success(CAALike like, Response response) {
            }

            @Override
            public void failure(RetrofitError error) {
                video.setLikedByMe(0);
                video.setTotalLikes(video.getTotalLikes() - 1);
            }
        });
    }

    private void unLikeVideo(final CAAVideo video) {
        LikesDataHandler.getInstance().remove(video.getVideoId());
        TrackingManager.getInstance().tapLikeButton(video.getVideoId(), "video_home", false);
        App.getUrlService().deleteLikeVideo(video.getVideoId(), new Callback<String>() {
            @Override
            public void success(String s, Response response) {
            }

            @Override
            public void failure(RetrofitError error) {
                video.setLikedByMe(1);
                video.setTotalLikes(video.getTotalLikes() + 1);
            }
        });
    }

    private void followUser(String userID) {
        FollowingDataHandler.getInstance().add(userID);
        App.getUrlService().putsUsersFollow(userID, "", new Callback<String>() {
            @Override
            public void success(String s, Response response) {
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
        StateMachine.getInstance(getApplicationContext()).insertEvent(
                new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL,
                        StateMachineEvent.EVENT_TYPE.FOLLOW_USER.ordinal(),
                        Integer.parseInt(userID)
                )
        );
    }

    private void unFollowUser(String userID) {
        FollowingDataHandler.getInstance().remove(userID);
        App.getUrlService().deleteUsersFollow(userID, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private void attachSurface()
    {
        try
        {
            if (mMediaPlayer == null)
            {
                MusicPrepare();
                return;
            }

            if (mClipTextureView.isAvailable())
            {
                mMediaPlayer.setSurface(new Surface(mClipTextureView.getSurfaceTexture()));
//            if (isPrepared && !isPlaying)
//                playMusic();
            }
            else
            {
                mClipTextureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener()
                {
                    @Override
                    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
                    {
                        if (mMediaPlayer != null)
                        {
                            mMediaPlayer.setSurface(new Surface(mClipTextureView.getSurfaceTexture()));
                        }
                    }

                    @Override
                    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height)
                    {
                    }

                    @Override
                    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface)
                    {
                        return false;
                    }

                    @Override
                    public void onSurfaceTextureUpdated(SurfaceTexture surface)
                    {
                    }
                });
            }
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
        }
    }

    private void MusicPrepare() {
        if (isLoadingMusic || !isActivityVisible)
            return;

        if (mMediaPlayer == null)
            mMediaPlayer = new MediaPlayer();

        TrackingManager.getInstance().videoPlay("video_home", mVideo.getVideoId(), mVideo.getArtistName(), mVideo.getSongName(), -1);


        isLoadingMusic = true;
        isPrepared = false;
        isPlaying = false;
        // Set type to streaming
        mProgressBar.setVisibility(View.VISIBLE);
        mPlayPauseButton.setVisibility(View.GONE);

        attachSurface();
        // Listen for if the audio file can't be prepared
        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                releaseMediaPlayer();
                mProgressBar.setVisibility(View.GONE);
                mClipImage.setVisibility(View.VISIBLE);
                return false;
            }
        });
        // Attach to when audio file is prepared for playing
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                isPrepared = true;
                adjustTextureViewSize();
                playMusic();
            }
        });

        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (mp != null && isActivityVisible)
                    mp.start();
            }
        });

        mMediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    mp.setOnInfoListener(null);
                    if (mClipImage != null) {
                        mClipImage.setVisibility(View.GONE);
//                        adjustTextureViewSize();
                    }
                    mProgressBar.setVisibility(View.GONE);
                }
                return true;
            }
        });

        // Set the data source to the remote URL
        // Trigger an async preparation which will file listener when completed
        try {
            mMediaPlayer.setDataSource(mVideo.getVideoUrl());
            mMediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void releaseMediaPlayer() {
        if (mMediaPlayer != null) {
            mProgressBar.setVisibility(View.GONE);
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer = null;
            isPlaying = false;
            isLoadingMusic = false;
            isPrepared = false;
        }
    }

    private void playMusic() {
        if (mMediaPlayer != null && !playDuetExplanation) {
            isPaused = false;
            mMediaPlayer.start();
            isLoadingMusic = false;
            mPlayPauseButton.setVisibility(View.GONE);
            isPlaying = true;
            gainAudioFocus();
        }
    }

    private void handleStatusbar(boolean hide) {
        if (hide) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }

    private void handleContent(boolean hide) {
        handleStatusbar(hide);
        if (!mVideo.getStatus().equalsIgnoreCase(Constants.SECRET)) {
            if (hide) {
                mDetailsContainer.startAnimation(slideDown);
            } else {
                mDetailsContainer.startAnimation(slideUp);
            }
        }
    }

    private void showTutorialImage() {
        releaseMediaPlayer();
        mClipImage.setVisibility(View.VISIBLE);
        mClipImage.setImageResource(R.drawable.duet_popup_image_full);

        fadeOutRightIcon();
        if (!SharedPreferencesManager.getInstance().getBoolean(Constants.FIRST_DUET_TUTORIAL, true))
            fadeInCloseButton();

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (playDuetExplanation) {
                    SharedPreferencesManager.getEditor().putBoolean(Constants.FIRST_DUET_TUTORIAL, false).apply();
                    playDuetExplanation = false;
                    mMultiImageLoader.DisplayImage(mClipImage);
                    handleSingleTap();
                    fadeInRightIcon();
                    fadeOutCloseButton();
                }
            }
        }, Constants.DUET_TURORIAL_TIME);
    }

    public void fadeOutRightIcon() {
        mRightCornerIcon.setClickable(false);
        mGlowRippleBackground.startAnimation(fadeOut);
        mRightCornerIcon.startAnimation(fadeOut);
    }

    public void fadeInRightIcon() {
        mRightCornerIcon.setClickable(true);
        mGlowRippleBackground.startAnimation(fadeIn);
        mRightCornerIcon.startAnimation(fadeIn);
        mGlowRippleBackground.startRippleAnimation();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mGlowRippleBackground != null)
                    mGlowRippleBackground.stopRippleAnimation();
            }
        }, 3000);
    }

    public void fadeOutCloseButton() {
        mCloseTutorialIcon.setClickable(false);
        mCloseTutorialIcon.startAnimation(fadeOut);
        mCloseTutorialIcon.setVisibility(View.GONE);
    }

    public void fadeInCloseButton() {
        mCloseTutorialIcon.setClickable(true);
        mCloseTutorialIcon.setVisibility(View.VISIBLE);
        mCloseTutorialIcon.startAnimation(fadeIn);
    }

    private void adjustTextureViewSize() {
        if (mClipTextureView == null || mMediaPlayer == null)
            return;

        float viewWidth = App.WIDTH;
        float viewHeight = App.WIDTH * ((float) 3 / 4);
        //Set it Top Centered.
        int yoff = 0;

        double aspectRatio;
        try {
            aspectRatio = ((double) mMediaPlayer.getVideoHeight()) / ((double) mMediaPlayer.getVideoWidth());
        } catch (Throwable err) {
            err.printStackTrace();
            return;
        }

        final float newWidth, newHeight;

        if (mMediaPlayer.getVideoWidth() == mMediaPlayer.getVideoHeight()) {
            newWidth = viewWidth;
            newHeight = (int) (viewWidth * aspectRatio);
        } else if (mMediaPlayer.getVideoWidth() < mMediaPlayer.getVideoHeight()) {
            // limited by narrow width; restrict height
            newWidth = viewWidth;
            newHeight = (int) (viewWidth * aspectRatio);
        } else {
            // limited by short height; restrict width
            newWidth = (int) (viewHeight / aspectRatio);
            newHeight = viewHeight;
        }
        int xoff = (int) ((viewWidth - newWidth) / 2f);
        Matrix txform = new Matrix();
        mClipTextureView.getTransform(txform);
        txform.postTranslate(xoff, yoff);
        txform.setScale(newWidth / viewWidth, newHeight / viewHeight, (int) ((viewWidth) / 2f), (int) ((viewHeight) / 2f)); // center crop

        mClipTextureView.setTransform(txform);
        mClipTextureView.requestLayout();

    }

    private void listenToAudioChanges() {
        IntentFilter filter = new IntentFilter(Constants.AUDIO_FOCUS_CHANGED);
        audioReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int focus = intent.getIntExtra(Constants.FOCUS, -1);
                switch (focus) {
                    case AudioManager.AUDIOFOCUS_LOSS:
                        if (mMediaPlayer != null && !isPaused && isPlaying)
                            handleSingleTap();
                        break;
                }

            }
        };
        registerReceiver(audioReceiver, filter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQ_VIDEO_UPDATE && resultCode == Activity.RESULT_OK) {
            //If i come from an empty screen , and its the first like or coment
            if (data.hasExtra(Constants.COMMENT_TEXT))
                mCommentsCount.setText("1");

            if (data.hasExtra(Constants.LIKES_UPDATE)) {
                mLikesCount.setText("1");
                mVideo.setLikedByMe(1);
                mLikeButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_liked_24dp));
            }
        }

    }


    public void hideNavBar()
    {
        boolean hasMenuKey = ViewConfiguration.get(getApplicationContext()).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        if (!hasMenuKey && !hasBackKey)
        {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public void UiChangeListener()
    {
        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
        {
            @Override
            public void onSystemUiVisibilityChange(int visibility)
            {
                if ((visibility) == 0)
                {
                    decorView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

                }
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    public int getSelfID() {
        return NO_BAR;
    }
}
