package fm.bling.blingy.utils.imagesManagement;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.view.ViewGroup;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fm.bling.blingy.ProcessImage;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;


/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 17/07/2016.
 * History:
 * ***********************************
 */
public class MultiImageLoader implements ImageLoaderBase
{
    private MemoryCache2 memoryCache;
    private FileCache2 fileCache;
    private HashMap<String, SharedBitmap> sharedBitmaps = new HashMap<>(); //  imageHashName  TO  shared Bitmap

    private Activity mActivity;
    private ExecutorService executorService;

    private Paint paint;
    private PorterDuffXfermode mPorterDuffXfermode;

    public MultiImageLoader(Activity activity)
    {
        mActivity = activity;
        fileCache = new FileCache2(activity);
        memoryCache = new MemoryCache2();
        executorService = Executors.newFixedThreadPool(10);
//        sharedBitmaps = new HashMap<>();

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        mPorterDuffXfermode = new PorterDuffXfermode(PorterDuff.Mode.SRC_IN);
    }

    @Override
    public synchronized void DisplayImage(URLImageView urlImageView)
    {
        if(urlImageView.getUrl() == null || sharedBitmaps == null)
        {
            if(urlImageView.getPlaceHolder() != 0)
                urlImageView.setBackgroundResource(urlImageView.getPlaceHolder());
            return;
        }

        String imageHashName = urlImageView.getImageHashName();
        synchronized (sharedBitmaps)
        {
            Bitmap bitmap = memoryCache.get(imageHashName);
            if (bitmap != null)
            {
                sharedBitmaps.get(imageHashName).addURLImageView(urlImageView);
                setImageView(urlImageView, bitmap);
            }
            else if (sharedBitmaps.get(imageHashName) != null)
            {
                sharedBitmaps.get(imageHashName).addURLImageView(urlImageView);
                urlImageView.setBackgroundResource(urlImageView.getPlaceHolder());
            }
            else
            {
                urlImageView.setBackgroundResource(urlImageView.getPlaceHolder());
                sharedBitmaps.put(imageHashName, new SharedBitmap(urlImageView));
                executorService.submit(new ImageLoader(imageHashName));
            }
        }
    }

    public void setImageView(URLImageView imageView, Bitmap bitmap)
    {
        imageView.setImageBitmap(bitmap);
        int width = imageView.getLayoutParams().width, height = imageView.getLayoutParams().height;

        if(width == ViewGroup.LayoutParams.WRAP_CONTENT)
        {
            width = bitmap.getWidth();
            imageView.layoutChanged = true;
        }

        if(imageView.isKeepAspectRatioAccordingToWidth() && width != ViewGroup.LayoutParams.MATCH_PARENT)
        {
            float scaleFactor = ((float)bitmap.getWidth()) / ((float)bitmap.getHeight());
            height = (int) (((float)width) / scaleFactor);
            imageView.layoutChanged = true;
        }
        else if(height == URLImageView.NOT_SPECIFIED || height == ViewGroup.LayoutParams.WRAP_CONTENT)
        {
            height = bitmap.getHeight();
            imageView.layoutChanged = true;
        }

        if(imageView.layoutChanged)
        {
            imageView.getLayoutParams().width  = width;
            imageView.getLayoutParams().height = height;
            imageView.requestLayout();
            imageView.layoutChanged = false;
        }

        if(imageView.getImageLoaderListener() != null)
        {
            imageView.getImageLoaderListener().onImageLoaded(imageView);
        }
        if(imageView.hasAnimation()){
            imageView.startAnimation(imageView.getImageAnimation());
        }
        imageView.invalidate();
    }

    @Override
    public void recycle(URLImageView urlImageView)
    {
        recycleAndRemove(urlImageView);
    }

    private boolean isInCheck = false;
    @Override
    public void recycleAndRemove(URLImageView urlImageView)
    {
        String imageHashName;

        if(urlImageView.getUrl() != null && sharedBitmaps != null)
        {
            synchronized (sharedBitmaps)
            {
                SharedBitmap sharedBitmap = sharedBitmaps.get(imageHashName = urlImageView.getImageHashName());
                if (sharedBitmap != null)
                {
                    if (sharedBitmap.removeURLImageView(urlImageView))
                    {
                        memoryCache.delete(imageHashName);
                        try{sharedBitmap.recycle();}catch (Throwable err){}
                        sharedBitmaps.remove(imageHashName);
                    }
                }
            }
        }
        else
        {
            try{((BitmapDrawable)urlImageView.getDrawable()).getBitmap().recycle();}catch (Throwable err){}
        }

        if(SharedBitmap.leftOver.size() > 0)
        {
            if(isInCheck) return;
            isInCheck = true;
            for(int l = SharedBitmap.leftOver.size() - 1; l > -1; l--)
            {
                if(!SharedBitmap.leftOver.get(l).isAttached())
                {
                    if(SharedBitmap.leftOver.get(l).getDrawable() == null)
                        SharedBitmap.leftOver.remove(l);
                    else
                    {
                        recycleAndRemove(SharedBitmap.leftOver.get(l));
                        SharedBitmap.leftOver.remove(l);
                    }
                }
            }
            isInCheck = false;
        }
    }

    @Override
    public void DisplayImageNowLight(URLImageView urlImageView)
    {

    }

    @Override
    public void clearCache()
    {
        memoryCache.clear();
    }

    @Override
    public void clearFiles()
    {

    }

    @Override
    public void clearImageViews()
    {
        if(sharedBitmaps == null)
            return;

        for(String key : sharedBitmaps.keySet())
            sharedBitmaps.get(key).recycle();
        sharedBitmaps.clear();
    }

    @Override
    public void close()
    {
        clearImageViews();
        clearCache();
//        for(SharedBitmap.leftOver :)

        SharedBitmap.leftOver.clear();
        sharedBitmaps.clear(); sharedBitmaps = null;
        mActivity = null;

        executorService.shutdownNow();
        executorService = null;

        paint = null;
        mPorterDuffXfermode = null;
    }

    class ImageLoader implements Runnable
    {
        String mImageHashName;

        ImageLoader(String imageHashName)
        {
            this.mImageHashName = imageHashName;
        }

        @Override
        public void run()
        {
            try
            {
                SharedBitmap sharedBitmap = sharedBitmaps.get(mImageHashName);
                if (sharedBitmap == null) return;

                Bitmap bitmap = getBitmap(sharedBitmap, mImageHashName);

                if (sharedBitmap.isClosed()) return;
                else
                {
                    memoryCache.put(mImageHashName, bitmap);
                    mActivity.runOnUiThread(new BitmapDisplayer(bitmap, sharedBitmap));
                }
            }
            catch (Throwable th)
            {
                th.printStackTrace();
            }
        }
    }

    private Bitmap getBitmap(SharedBitmap sharedBitmap, String imageHashName)
    {
        Bitmap bitmap = null;
        try
        {
            bitmap = fileCache.getFile(imageHashName);
            if(bitmap != null)
                return bitmap;

            if(sharedBitmap.getUrl().contains(Constants.HTTP))
            {
                bitmap = getRemoteBitmap(sharedBitmap.getUrl());
            }
            else
            {
                bitmap = getLocalBitmap(sharedBitmap.getUrl());
            }


//            if(sharedBitmap.isLocalImage())
//            {
//                bitmap = getLocalBitmap(sharedBitmap.getUrl());
//            }
//            else
//            {
//                bitmap = getRemoteBitmap(sharedBitmap.getUrl());
//            }

            if(sharedBitmap.isClosed())
            {
                try{bitmap.recycle();}catch (Throwable err){}
                return null;
            }

            if(sharedBitmap.hasMajorColorLayer())
                sharedBitmap.setMajorColorLayerColor(ProcessImage.getInstance().getMajorColor(bitmap));
            bitmap = getScaledBitmap(bitmap, sharedBitmap);
            bitmap = getFilteredBitmap(bitmap, sharedBitmap);

            if(!sharedBitmap.isDontSaveToFileCache())
            {
                saveBitmap(bitmap, imageHashName, sharedBitmap.hasAlpha());
            }
            return bitmap;
        }
        catch (Throwable ex)
        {
            ex.printStackTrace();

            try{bitmap.recycle();}catch (Throwable err){}
            if(ex instanceof OutOfMemoryError)
            {
                memoryCache.clear();
            }
            return null;
        }
    }

    private Bitmap getLocalBitmap(String imageUrl) throws Throwable
    {
        Bitmap bitmap;
        try
        {
            bitmap = BitmapFactory.decodeStream(mActivity.getContentResolver().openInputStream(android.net.Uri.parse(imageUrl)));
        }
        catch (Throwable x)
        {
            x.printStackTrace();
            bitmap = BitmapFactory.decodeFile(imageUrl);
        }
        return bitmap;
    }

    private Bitmap getRemoteBitmap(String imageUrl) throws Throwable
    {
        URL url = new URL(imageUrl);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setConnectTimeout(10000);
        conn.setReadTimeout(10000);
        conn.setInstanceFollowRedirects(true);
        InputStream is = conn.getInputStream();

        Bitmap bitmap = BitmapFactory.decodeStream(is);
        is.close();
        conn.disconnect();

        return bitmap;
    }

    private Bitmap getScaledBitmap(Bitmap temp, SharedBitmap sharedBitmap)
    {
        Bitmap bitmap;
        if(sharedBitmap.getWidthInternal() != URLImageView.NOT_SPECIFIED)
        {
            float scale = ((float) temp.getHeight()) / ((float) temp.getWidth());
            int width = sharedBitmap.getMaxWidthInternal() != URLImageView.NOT_SPECIFIED &&
                    sharedBitmap.getMaxWidthInternal() < sharedBitmap.getWidthInternal() ?
                    sharedBitmap.getMaxWidthInternal() : sharedBitmap.getWidthInternal();
            bitmap = Bitmap.createScaledBitmap(temp, (int) (((float)width) * memoryCache.resoulotionScaleFactor), (int) (((float) width) * scale * memoryCache.resoulotionScaleFactor), true);
            if (bitmap != temp)
                try
                {
                    temp.recycle();
                }
                catch (Throwable x)
                {
                    x.printStackTrace();
                }
        }
        else if(sharedBitmap.getMaxWidthInternal() != URLImageView.NOT_SPECIFIED && sharedBitmap.getMaxWidthInternal() < temp.getWidth())
        {
            float scale = ((float)temp.getHeight()) / ((float)temp.getWidth());
            int width = sharedBitmap.getMaxWidthInternal() < temp.getWidth() ?
                    sharedBitmap.getMaxWidthInternal() : temp.getWidth();
            bitmap = Bitmap.createScaledBitmap(temp, (int) (((float)width) * memoryCache.resoulotionScaleFactor), (int) (((float) width) * scale * memoryCache.resoulotionScaleFactor), true);
            if (bitmap != temp)
                try
                {
                    temp.recycle();
                }
                catch (Throwable x)
                {
                    x.printStackTrace();
                }
        }
        else if( memoryCache.resoulotionScaleFactor != 1f)
        {
            bitmap = Bitmap.createScaledBitmap(temp, (int) (((float)temp.getWidth()) * memoryCache.resoulotionScaleFactor), (int) (((float) temp.getHeight()) * memoryCache.resoulotionScaleFactor), true);
            if (bitmap != temp)
                try
                {
                    temp.recycle();
                }
                catch (Throwable x)
                {
                    x.printStackTrace();
                }
        }
        else bitmap = temp;

        if(sharedBitmap.getAspectRatio() != 1f)
        {
            bitmap = getBitmapInAspectRatio(bitmap, sharedBitmap.getAspectRatio());
        }
        else if(sharedBitmap.isFixedSize() && (bitmap.getWidth() != sharedBitmap.getWidth() || bitmap.getHeight() != sharedBitmap.getHeight()))
        {
            bitmap = getFitsBitmap(bitmap, sharedBitmap);
        }
        return bitmap;
    }

    private Bitmap getBitmapInAspectRatio(Bitmap bitmap, float aspectRatio)
    {
        float aspectRatio1 = ((float)bitmap.getWidth()) / ((float)bitmap.getHeight());
        if(aspectRatio1 == aspectRatio)
            return bitmap;

        Bitmap temp;// = Bitmap.createBitmap(urlImageView.getWidth(), urlImageView.getHeight(), Bitmap.Config.ARGB_8888);
        Matrix matrix = new Matrix();
        int newWidth, newHeight;
        int shiftX = 0, shiftY = 0;
        if(aspectRatio1 > aspectRatio) // bitmap is wider than needed
        {
            newHeight = bitmap.getHeight();
            newWidth = (int) (((float)bitmap.getHeight()) * aspectRatio);
            shiftX = - (bitmap.getWidth() - newWidth) / 2;
        }
        else
        {
            newWidth = bitmap.getWidth();
            newHeight = (int) (((float)bitmap.getWidth()) / aspectRatio);
            shiftY = - (bitmap.getHeight() - newHeight) / 2;
        }
        temp = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(temp);
        matrix.postTranslate(shiftX, shiftY);
        canvas.drawBitmap(bitmap, matrix, paint);

        try{ bitmap.recycle(); }catch (Throwable err){}

        return temp;
    }

    private Bitmap getFitsBitmap(Bitmap bitmap, SharedBitmap sharedBitmap)
    {
        if(bitmap.getWidth() == sharedBitmap.getWidth() && bitmap.getHeight() >= sharedBitmap.getHeight())
            return bitmap;
        else
        {
            Bitmap temp = Bitmap.createBitmap(sharedBitmap.getWidth(), sharedBitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(temp);
            Matrix matrix = new Matrix();
            float scale = ((float)sharedBitmap.getWidth()) / ((float)bitmap.getWidth());
            if(scale <  ((float)sharedBitmap.getHeight()) / ((float)bitmap.getHeight()))
            {
                scale = ((float)sharedBitmap.getHeight()) / ((float)bitmap.getHeight());
                matrix.setScale(scale, scale, bitmap.getWidth() / 2, bitmap.getHeight() / 2);
                matrix.postTranslate(0, (bitmap.getHeight() / 2)*(scale - 1));
            }
            else
            {
                matrix.setScale(scale, scale, bitmap.getWidth() / 2, bitmap.getHeight() / 2);
                matrix.postTranslate((bitmap.getWidth() / 2) * (scale - 1), 0);
            }


            canvas.drawBitmap(bitmap, matrix, paint);

            try{ bitmap.recycle(); }catch (Throwable err){}

            return temp;
        }
    }

    private Bitmap getFilteredBitmap(Bitmap bitmap, SharedBitmap sharedBitmap)
    {
        if(sharedBitmap.isRoundedImage())
        {
            bitmap = getRoundedBitmap(bitmap);
        }

        if(sharedBitmap.isBlured())
        {
            bitmap = getBluredBitmap(bitmap, sharedBitmap.getBlurRadius());
        }
        return bitmap;
    }

    //     * @return  true if the bitmap should be reloaded, false - don't reload the bitmap
//     *              always returns false

    /**
     *
     * @param bitmap
     * @param imageHashName
     * @param hasAlpha
     * @throws Throwable
     */
    private void saveBitmap(Bitmap bitmap, String imageHashName, boolean hasAlpha) throws Throwable
    {
        OutputStream os = new FileOutputStream(fileCache.getFileForIO(imageHashName));

        if (hasAlpha)
        {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
            os.flush();
            os.close();
//            return false;
        }
        else
        {
            bitmap.compress(Bitmap.CompressFormat.JPEG, memoryCache.pictureQuality, os);
            os.flush();
            os.close();
//            try
//            {
//                bitmap.recycle();
//            }
//            catch (Throwable x)
//            {
//                x.printStackTrace();
//            }
//            return false;
        }
    }

    private Bitmap getRoundedBitmap(Bitmap bitmap)
    {
        RectF rectF;
        Rect rectSrc, rectDest;
        Bitmap roundedBitmap;

        if(bitmap.getHeight() != bitmap.getWidth())
        {
            if(bitmap.getWidth() > bitmap.getHeight())
            {
                int dif = (bitmap.getWidth() - bitmap.getHeight())/2;
                rectSrc=new Rect(dif, 0, bitmap.getHeight() + dif, bitmap.getHeight());
                rectDest = new Rect(0, 0, bitmap.getHeight(), bitmap.getHeight());
                rectF=new RectF(0, 0, bitmap.getHeight(), bitmap.getHeight());
                roundedBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            }
            else
            {
                int dif = (bitmap.getHeight() - bitmap.getWidth())/2;
                rectSrc=new Rect(0, dif, bitmap.getWidth(), bitmap.getWidth() + dif);
                rectDest = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
                rectF=new RectF(0, 0, bitmap.getWidth(), bitmap.getWidth());
                roundedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
            }
        }
        else
        {
            rectDest = rectSrc =new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            rectF=new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight());
            roundedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas can = new Canvas(roundedBitmap);
        can.drawARGB(0, 0, 0, 0);


        can.drawOval(rectF, paint);
        paint.setXfermode(mPorterDuffXfermode);
        can.drawBitmap(bitmap, rectSrc, rectDest, paint);
        paint.setXfermode(null);

        bitmap.recycle();
        return roundedBitmap;
    }

    private class BitmapDisplayer implements Runnable
    {
        private Bitmap bitmap;
        private SharedBitmap mSharedBitmap;

        public BitmapDisplayer(Bitmap bitmap, SharedBitmap sharedBitmap)
        {
            this.bitmap = bitmap;
            this.mSharedBitmap = sharedBitmap;
        }

        public void run()
        {
            if(bitmap == null || mSharedBitmap.isClosed())
            {
                try{bitmap.recycle();}catch (Throwable err){}
            }
            else
            {
                mSharedBitmap.setBitmap(bitmap);
            }
            mSharedBitmap = null;
            bitmap = null;
        }
    }

    private Bitmap getBluredBitmap(Bitmap bitmap, int radius)
    {
        Bitmap bluredBitmap = bitmap.copy(bitmap.getConfig(), true);
        bitmap.recycle();
        try
        {
            int w = bluredBitmap.getWidth();
            int h = bluredBitmap.getHeight();

            int[] pix = new int[w * h];
            bluredBitmap.getPixels(pix, 0, w, 0, 0, w, h);

            int wm = w - 1;
            int hm = h - 1;
            int wh = w * h;
            int div = radius + radius + 1;

            int r[] = new int[wh];
            int g[] = new int[wh];
            int b[] = new int[wh];
            int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
            int vmin[] = new int[Math.max(w, h)];

            int divsum = (div + 1) >> 1;
            divsum *= divsum;
            int dv[] = new int[256 * divsum];
            for (i = 0; i < 256 * divsum; i++)
            {
                dv[i] = (i / divsum);
            }

            yw = yi = 0;

            int[][] stack = new int[div][3];
            int stackpointer;
            int stackstart;
            int[] sir;
            int rbs;
            int r1 = radius + 1;
            int routsum, goutsum, boutsum;
            int rinsum, ginsum, binsum;

            for (y = 0; y < h; y++)
            {
                rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
                for (i = -radius; i <= radius; i++)
                {
                    p = pix[yi + Math.min(wm, Math.max(i, 0))];
                    sir = stack[i + radius];
                    sir[0] = (p & 0xff0000) >> 16;
                    sir[1] = (p & 0x00ff00) >> 8;
                    sir[2] = (p & 0x0000ff);
                    rbs = r1 - Math.abs(i);
                    rsum += sir[0] * rbs;
                    gsum += sir[1] * rbs;
                    bsum += sir[2] * rbs;
                    if (i > 0)
                    {
                        rinsum += sir[0];
                        ginsum += sir[1];
                        binsum += sir[2];
                    }
                    else
                    {
                        routsum += sir[0];
                        goutsum += sir[1];
                        boutsum += sir[2];
                    }
                }
                stackpointer = radius;

                for (x = 0; x < w; x++)
                {
                    r[yi] = dv[rsum];
                    g[yi] = dv[gsum];
                    b[yi] = dv[bsum];

                    rsum -= routsum;
                    gsum -= goutsum;
                    bsum -= boutsum;

                    stackstart = stackpointer - radius + div;
                    sir = stack[stackstart % div];

                    routsum -= sir[0];
                    goutsum -= sir[1];
                    boutsum -= sir[2];

                    if (y == 0)
                    {
                        vmin[x] = Math.min(x + radius + 1, wm);
                    }
                    p = pix[yw + vmin[x]];

                    sir[0] = (p & 0xff0000) >> 16;
                    sir[1] = (p & 0x00ff00) >> 8;
                    sir[2] = (p & 0x0000ff);

                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];

                    rsum += rinsum;
                    gsum += ginsum;
                    bsum += binsum;

                    stackpointer = (stackpointer + 1) % div;
                    sir = stack[(stackpointer) % div];

                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];

                    rinsum -= sir[0];
                    ginsum -= sir[1];
                    binsum -= sir[2];

                    yi++;
                }
                yw += w;
            }
            for (x = 0; x < w; x++)
            {
                rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
                yp = -radius * w;
                for (i = -radius; i <= radius; i++)
                {
                    yi = Math.max(0, yp) + x;

                    sir = stack[i + radius];

                    sir[0] = r[yi];
                    sir[1] = g[yi];
                    sir[2] = b[yi];

                    rbs = r1 - Math.abs(i);

                    rsum += r[yi] * rbs;
                    gsum += g[yi] * rbs;
                    bsum += b[yi] * rbs;

                    if (i > 0)
                    {
                        rinsum += sir[0];
                        ginsum += sir[1];
                        binsum += sir[2];
                    }
                    else
                    {
                        routsum += sir[0];
                        goutsum += sir[1];
                        boutsum += sir[2];
                    }

                    if (i < hm)
                    {
                        yp += w;
                    }
                }
                yi = x;
                stackpointer = radius;
                for (y = 0; y < h; y++)
                {
                    // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                    pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                    rsum -= routsum;
                    gsum -= goutsum;
                    bsum -= boutsum;

                    stackstart = stackpointer - radius + div;
                    sir = stack[stackstart % div];

                    routsum -= sir[0];
                    goutsum -= sir[1];
                    boutsum -= sir[2];

                    if (x == 0)
                    {
                        vmin[y] = Math.min(y + r1, hm) * w;
                    }
                    p = x + vmin[y];

                    sir[0] = r[p];
                    sir[1] = g[p];
                    sir[2] = b[p];

                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];

                    rsum += rinsum;
                    gsum += ginsum;
                    bsum += binsum;

                    stackpointer = (stackpointer + 1) % div;
                    sir = stack[stackpointer];

                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];

                    rinsum -= sir[0];
                    ginsum -= sir[1];
                    binsum -= sir[2];

                    yi += w;
                }
            }

            bluredBitmap.setPixels(pix, 0, w, 0, 0, w, h);
        }catch (Throwable err){}
        return bluredBitmap;
    }

    public boolean runOnUiThread(Runnable runnable)
    {
        if(mActivity != null)
        {
            mActivity.runOnUiThread(runnable);
            return true;
        }
        else return false;
    }

}