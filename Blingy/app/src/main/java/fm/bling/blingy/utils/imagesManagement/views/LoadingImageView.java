package fm.bling.blingy.utils.imagesManagement.views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import fm.bling.blingy.R;
import fm.bling.blingy.utils.graphics.LoadingPlaceHolder;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 3/7/16.
 * History:
 * ***********************************
 */
public class LoadingImageView extends URLImageView
{
    private boolean isLoading;
    private LoadingPlaceHolder mLoadingPlaceHolder;

    public LoadingImageView(Context context)
    {
        super(context);
    }

    public LoadingImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public LoadingImageView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public void draw(Canvas can)
    {
        try
        {
            if(isLoading)
            {
                mLoadingPlaceHolder.draw(can);
            }
            else super.draw(can);
        }
        catch (Throwable e)
        {
            mImageLoader.DisplayImage(this);
        }
    }

    public void setLoadingPlaceHolder(LoadingPlaceHolder loadingPlaceHolder)
    {
        isLoading = true;
        mLoadingPlaceHolder = loadingPlaceHolder;
        mLoadingPlaceHolder.attachView(this);
    }

    public void stopLoading()
    {
        if(isLoading)
        {
            setVisibility(VISIBLE);
            isLoading = false;
            mLoadingPlaceHolder.detachView(this);
            mLoadingPlaceHolder = null;
            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
            animation.setAnimationListener(new Animation.AnimationListener()
            {
                @Override
                public void onAnimationStart(Animation animation)
                {
                }

                @Override
                public void onAnimationEnd(Animation animation)
                {
                    setVisibility(VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation)
                {
                }
            });
            startAnimation(animation);
        }
    }

}