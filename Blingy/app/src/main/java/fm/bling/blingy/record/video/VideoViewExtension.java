package fm.bling.blingy.record.video;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.VideoView;

import fm.bling.blingy.R;


/**
 * *********************************
 * Project: Chosen Android Application. All rights reserved.
 * Description:
 * Created by Ben Levi on 3/13/16.
 * History:
 * ***********************************
 */
public class VideoViewExtension extends VideoView
{
    private Bitmap forground        = null;
    private boolean isForgroundSet  = false;
    private Matrix matrix           = null;

    public VideoViewExtension(Context context)
    {
        super(context);
        setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    public VideoViewExtension(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    public VideoViewExtension(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    @Override
    public void draw(Canvas canvas)
    {
        super.draw(canvas);
        if(isForgroundSet)
            canvas.drawBitmap(forground, matrix, null);
    }

    public void setForgroundBitmap(Bitmap iForground)
    {
        if(this.forground != null)
        {
            this.forground.recycle();
        }
        this.forground = iForground;
        if(iForground == null)
        {
            isForgroundSet = false;
            matrix = null;
            return;
        }
        if(matrix == null)
        {
            matrix = new Matrix();
            matrix.setRectToRect(new RectF(0, 0, iForground.getWidth(), iForground.getHeight()), new RectF(0, 0, getWidth(), getHeight()), Matrix.ScaleToFit.FILL);
        }
        isForgroundSet = true;
    }

    public Bitmap getForgroundBitmap()
    {
        return forground;
    }



}