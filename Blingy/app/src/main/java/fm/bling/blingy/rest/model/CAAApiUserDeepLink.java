package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;

import fm.bling.blingy.videoHome.model.CAAVideo;


/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAApiUserDeepLink
 * Description:
 * Created by Dawidowicz Nadav on 5/6/15.
 * History:
 * ***********************************
 */
public class CAAApiUserDeepLink {
    @SerializedName("type")
    private String type = "";

    @SerializedName("page")
    private String page;

    @SerializedName("object_id")
    private String objectId;

    @SerializedName("share_id")
    private String shareId;

//    @SerializedName("tile")
//    private CAATile tile;

    @SerializedName("video")
    private CAAVideo video;

    public CAAVideo getVideo() {
        return video;
    }

    public void setVideo(CAAVideo video) {
        this.video = video;
    }

//    public CAATile getTile()
//    {
//        return tile;
//    }
//
//    public void setTile(CAATile tile) {
//        this.tile = tile;
//    }

    public String getType() {
        return type;
    }

//    public void setType(String type) {
//        this.type = type;
//    }

    public String getObjectId() {
        return objectId;
    }

//    public void setObjectId(String objectId) {
//        this.objectId = objectId;
//    }

    public String getShareId() {
        return shareId;
    }

//    public void setShareId(String shareId) {
//        this.shareId = shareId;
//    }

    public String getPage() {
        return page;
    }

//    public void setPage(String page) {
//        this.page = page;
//    }
}
