package fm.bling.blingy.leaderboard.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.leaderboard.LeaderboardMVPR;
import fm.bling.blingy.leaderboard.presenter.LeaderboardPresenter;
import fm.bling.blingy.profile.ProfileActivity;
import fm.bling.blingy.record.utils.SimpleDividerItemDecoration;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.utils.Constants;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 02/03/17.
 * History:
 * ***********************************
 */
public class LeaderboardActivity extends BaseActivity implements LeaderboardMVPR.View {

    private LeaderboardPresenter mPresenter;

    /**
     * Views
     */
    private Toolbar mToolbar;
    private RecyclerView mUsersRecycler;
    private LeaderboardRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private View.OnClickListener itemClickListener;

    private int noData = 0;
    private boolean calling = false;
    private boolean listInitialized = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leaderboard_layout);
        init();
        mPresenter = new LeaderboardPresenter();
        mPresenter.setView(this);
        if(Constants.mTopUsers == null)
            mPresenter.getLeaderBoard(0);
        else
            showLeaderboardData(Constants.mTopUsers);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(mAdapter != null) {
            mAdapter.setImageLoaderManager(getMultiImageLodaer());
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void init() {
        mToolbar = getActionBarToolbar();
        mToolbar.setBackgroundResource(R.color.transparent);
        mToolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mUsersRecycler = (RecyclerView)findViewById(R.id.leaderboard_recycler);
        itemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = ((User)v.getTag());
                openUserProfile(user);
            }
        };
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mUsersRecycler.setLayoutManager(mLayoutManager);
        mUsersRecycler.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext(), R.drawable.line_divider, true));
        mUsersRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //Next Page Section
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItem = ((LinearLayoutManager) mLayoutManager).findFirstVisibleItemPosition();
                if (listInitialized) {
                    final int lastItem = firstVisibleItem + visibleItemCount;
                    if (lastItem >= totalItemCount - 4 && !calling && !Constants.leaderEndReached) {
                        if (Constants.leaderPreviousLastItem != lastItem) { //to avoid multiple calls for last item
                            Constants.leaderPreviousLastItem = lastItem;
                            Constants.leaderPageNum++;
                            calling = true;
                            Constants.mTopUsers.add(new User());
                            mAdapter.notifyItemInserted(Constants.mTopUsers.size());
                            mPresenter.getLeaderBoard(Constants.leaderPageNum);
                        }
                    }
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_20));
            ((FrameLayout.LayoutParams)mToolbar.getLayoutParams()).topMargin = App.STATUS_BAR_HEIGHT;
            ((FrameLayout.LayoutParams)findViewById(R.id.main_content).getLayoutParams()).topMargin = App.TOOLBAR_HEIGHT + App.STATUS_BAR_HEIGHT;
        }
    }


    @Override
    public void openUserProfile(User user) {
        Intent profileIntent = new Intent(this, ProfileActivity.class);
        profileIntent.putExtra(Constants.USER_ID, user.getUserId());
        startActivity(profileIntent);
    }

    @Override
    public void showLeaderboardData(ArrayList<User> users) {
        if(users == null || users.size() <= 0)
            return;

        Constants.mTopUsers = users;
        mAdapter = new LeaderboardRecyclerAdapter(Constants.mTopUsers, itemClickListener);
        mAdapter.setImageLoaderManager(getMultiImageLodaer());
        mUsersRecycler.setAdapter(mAdapter);
        listInitialized = true;
    }

    @Override
    public void loadNextPage(@Nullable ArrayList<User> users) {
        try {

//            int startRange = mAdapter.getItemCount() - 1;
            Constants.mTopUsers.remove(Constants.mTopUsers.size() - 1);
            mAdapter.notifyItemRemoved(Constants.mTopUsers.size());

            if (users != null && users.size() != 0) {
                if (mAdapter.getItemCount() < 1) {
                    noData++;
                    if (noData >= 2) {
                        Constants.leaderEndReached = true;
                    }
                } else {
                    noData = 0;
                    int previousItemCount = mAdapter.getItemCount();
                    for (User user: users) {
                        Constants.mTopUsers.add(user);
                    }

//                    Constants.mTopUsers.add(new User());
                    mAdapter.notifyItemRangeInserted(previousItemCount, mAdapter.getItemCount() - previousItemCount);
                }
            } else {
                Constants.leaderEndReached = true;
            }
//            mAdapter.notifyItemRangeChanged(startRange, mAdapter.getItemCount(), null);
        } catch (Throwable err) {
            err.printStackTrace();
        }
        calling = false;
    }
}
