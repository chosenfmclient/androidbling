package fm.bling.blingy.utils.imagesManagement.views;

/**
 * Created by Processor on 1/31/2016.
 */
public interface CloseableView
{
    void close();

}