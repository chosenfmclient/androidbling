//package fm.bling.blingy.dialogs;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.view.View;
//
//import fm.bling.blingy.BaseActivity;
//import fm.bling.blingy.R;
//import fm.bling.blingy.record.PickMusicClipActivity;
//import fm.bling.blingy.tracking.TrackingManager;
//import fm.bling.blingy.upload.PickPhotoVideoActivity;
//
//
///**
// * Created by Chosen-pro on 4/25/16.
// */
//public class RecordButtonDialog extends ChosenChoiceDialog {
//    private Activity mActivity;
//    public RecordButtonDialog(Activity activity) {
//        super(activity, R.layout.recording_button_dialog);
//        this.mActivity = activity;
//
//        subMain.findViewById(R.id.upload_photo_or_video).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((BaseActivity)mActivity).checkUploadPermissions(new Intent(mActivity, PickPhotoVideoActivity.class));
//            }
//        });
//
//        subMain.findViewById(R.id.music_video).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismiss();
//                Intent intent = new Intent(mActivity, PickMusicClipActivity.class);
//                mActivity.startActivity(intent);
//            }
//        });
//    }
//
//    private void launchUpload() {
//        Intent intent = new Intent(mActivity, PickPhotoVideoActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        mActivity.startActivity(intent);
//    }
//}