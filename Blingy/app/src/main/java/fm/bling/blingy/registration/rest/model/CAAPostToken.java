package fm.bling.blingy.registration.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAPostToken
 * Description:
 * Created by Dawidowicz Nadav on 5/12/15.
 * History:
 * ***********************************
 */
public class CAAPostToken {
    public CAAPostToken(String email, String password, String appId, String secret) {
        this.email = email;
        this.password = password;
        this.appId = appId;
        this.secret = secret;
    }
    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    @SerializedName("app_id")
    private String appId;

    @SerializedName("secret")
    private String secret;

    @SerializedName("auto")
    private String auto;

    public String getAuto() {
        return auto;
    }

    public void setAuto(String auto) {
        this.auto = auto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
