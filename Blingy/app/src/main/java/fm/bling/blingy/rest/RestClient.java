package fm.bling.blingy.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import fm.bling.blingy.utils.Constants;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: RestClient
 * Description:
 * Created by Dawidowicz Nadav on 5/6/15.
 * History:
 * ***********************************
 */
public class RestClient {

    public static final String version = "1.1";
    private static final String BASE_URL = Constants.BASE_URL + Constants.API_BASE;
    private UrlService apiService;

    public RestClient() {
        OkHttpClient client = new OkHttpClient();
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        client.setCookieHandler(cookieManager);
        client.setReadTimeout(25, TimeUnit.SECONDS);
        client.setConnectTimeout(25, TimeUnit.SECONDS);
        client.interceptors().add(new QueueInterceptor());

        OkClient serviceClient = new OkClient(client);

        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .setConverter(new GsonConverter(gson))
                .setClient(serviceClient)
                .setRequestInterceptor(new BRequestInterceptor())
                .build();

        apiService = restAdapter.create(UrlService.class);
    }

    public UrlService getUrlService() {
        return apiService;
    }

}