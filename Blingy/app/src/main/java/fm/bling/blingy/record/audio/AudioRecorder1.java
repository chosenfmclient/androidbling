package fm.bling.blingy.record.audio;

import android.content.Context;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.audiofx.NoiseSuppressor;
import android.os.SystemClock;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import fm.bling.blingy.App;
import fm.bling.blingy.utils.FFMPEG;
import fm.bling.blingy.utils.Sonic;

/**
 * Created by ben on 06/06/2016.
 */
public class AudioRecorder1
{
    private final int SAMPLE_RATE = 44100, CHANNEL_CONFIG = android.media.AudioFormat.CHANNEL_CONFIGURATION_MONO, AUDIO_FORMAT = android.media.AudioFormat.ENCODING_PCM_16BIT;
    private final int minimumBufferSize;
    private AudioRecord audioRecord;
    private boolean isRecording, isDoneReadingData;
    private String mOutputFileName, internalOutputFileName, tempOutputFileName, internalOutputFastFileName, internalOutputSlowFileName;
    private DataOutputStream outputStream;
    private String mSoundTrackUrl;
    private long pausedTime;
    private boolean soundTrackSet;
    private OnDoneDownloadingListener mOnDoneDownloadingListener;
    private Context mContext;
    private boolean isSongSaved;
    private int numOfActionsBeforeProceed, counterOfActions;

    public AudioRecorder1(Context context, String outputFileName, String soundTrackUrl, OnDoneDownloadingListener onDoneDownloadingListener)
    {
        mOutputFileName = outputFileName;
        mOnDoneDownloadingListener = onDoneDownloadingListener;
        mContext = context;
        minimumBufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, CHANNEL_CONFIG, AUDIO_FORMAT);

        if ((mSoundTrackUrl = soundTrackUrl) == null)
        {
            try
            {
                outputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(mOutputFileName)));
            }
            catch (Throwable err)
            {
                err.printStackTrace();
                throw new RuntimeException();
            }
            audioRecord = new AudioRecord(MediaRecorder.AudioSource.CAMCORDER,
                    SAMPLE_RATE, CHANNEL_CONFIG, AUDIO_FORMAT, minimumBufferSize);
            NoiseSuppressor.create(audioRecord.getAudioSessionId());
        }
        else
        {
            if (!new File(App.VIDEO_WORKING_FOLDER + "audio/").exists())
                new File(App.VIDEO_WORKING_FOLDER + "audio/").mkdir();
            internalOutputFileName = App.VIDEO_WORKING_FOLDER + "audio/outputR.wav";
            internalOutputFastFileName = App.VIDEO_WORKING_FOLDER + "audio/outputF.wav";
            internalOutputSlowFileName = App.VIDEO_WORKING_FOLDER + "audio/outputS.wav";
            tempOutputFileName = App.VIDEO_WORKING_FOLDER + "audio/temp_output1.wav";
            soundTrackSet = true;
            saveSongFromUrlToFile();
        }
    }

    public void start() throws Throwable
    {
        if (soundTrackSet)
        {
            if (pausedTime != 0)
            {
                pausedTime = SystemClock.elapsedRealtime() - pausedTime;
                pausedTime = 0;
            }
        }
        else
        {
            audioRecord.startRecording();
            new Thread(new RecordAudioRunnable()).start();
        }
        isRecording = true;
    }

    public void pause()
    {
        isRecording = false;
        pausedTime = SystemClock.elapsedRealtime();
    }

    public void stop()
    {
        isRecording = false;
        if (!soundTrackSet)
        {
            while (!isDoneReadingData)
            {
                try
                {
                    Thread.sleep(50);
                }
                catch (Throwable err)
                {
                }
            }
            try
            {
                outputStream.flush();
                outputStream.close();
                outputStream = null;
            }
            catch (Throwable err)
            {
            }
        }
        else
        {
            if (pausedTime != 0)
            {
                pausedTime = SystemClock.elapsedRealtime() - pausedTime;
            }
        }
    }

    public String getOutputFilePath()
    {
        return mOutputFileName;
    }

//    08-11 15:05:26.831 14174-15544/chosen.fm.chosenandroid I/System.out: >>Tic>> Elapsed time since start : 11582 ms
//    08-11 15:32:16.561 14174-15516/chosen.fm.chosenandroid I/System.out: >>Tic>> Elapsed time since start : 11613 ms

    private void saveSongFromUrlToFile()
    {
        if (isSongSaved) return;
        isSongSaved = true;
        numOfActionsBeforeProceed = 3;//3 actions - 1 pitch, 2 fast track, 3 slow track

        if (new File(App.VIDEO_WORKING_FOLDER + "audio/" + mSoundTrackUrl.hashCode() + ".mp3").exists())
        {
            new Thread()
            {
                public void run()
                {
                    try
                    {
//                        Thread.sleep(1000);
//
                        DataInputStream dataInputStream = new DataInputStream(new FileInputStream(App.VIDEO_WORKING_FOLDER + "audio/" + mSoundTrackUrl.hashCode() + ".mp3"));

                        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(mOutputFileName));

                        byte[] buffer = new byte[1024 * 50]; // 50 kb
                        int count;

                        while (true)
                        {
                            count = dataInputStream.read(buffer);
                            if (count == -1)
                                break;
                            dataOutputStream.write(buffer, 0, count);
                        }
                        dataOutputStream.flush();
                        dataOutputStream.close();
                        dataInputStream.close();

                        if (new File(tempOutputFileName).exists())
                            new File(tempOutputFileName).delete();

                        FFMPEG.getInstance(mContext).convertMp3ToPCM(App.VIDEO_WORKING_FOLDER + "audio/" + mSoundTrackUrl.hashCode() + ".mp3", tempOutputFileName);

                        initTracks();
                    }
                    catch (Throwable err)
                    {
                        err.printStackTrace();
                    }
                }
            }.start();
        }
        else
        {
            new Thread()
            {
                public void run()
                {
                    try
                    {
                        URL url = new URL(mSoundTrackUrl);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setConnectTimeout(20000);
                        conn.setReadTimeout(20000);
                        conn.setInstanceFollowRedirects(true);

                        DataInputStream dataInputStream = new DataInputStream(conn.getInputStream());
                        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(mOutputFileName));
                        DataOutputStream cachingStream = new DataOutputStream(new FileOutputStream(App.VIDEO_WORKING_FOLDER + "audio/" + mSoundTrackUrl.hashCode() + ".mp3"));
                        byte[] buffer = new byte[1024 * 50]; // 50 kb
                        int count;

                        while (true)
                        {
                            count = dataInputStream.read(buffer);
                            if (count == -1)
                                break;
                            dataOutputStream.write(buffer, 0, count);
                            cachingStream.write(buffer, 0, count);
                        }
                        dataOutputStream.flush();
                        cachingStream.flush();
                        cachingStream.close();
                        dataOutputStream.close();
                        dataInputStream.close();

                        if (new File(tempOutputFileName).exists())
                            new File(tempOutputFileName).delete();
                        FFMPEG.getInstance(mContext).convertMp3ToPCM(mOutputFileName, tempOutputFileName);

                        initTracks();
                    }
                    catch (Throwable err)
                    {
                        err.printStackTrace();
                    }
                }
            }.start();
        }
    }

    private void initTracks()
    {
        new Thread()
        {
            public void run()
            {
                try{createFastTrack();}catch (Throwable err){}
                proceed();
            }
        }.start();

        new Thread()
        {
            public void run()
            {
                try{createSlowTrack();}catch (Throwable err){}
                proceed();
            }
        }.start();

        new Thread()
        {
            public void run()
            {
                try{changePitch();}catch (Throwable err){}
                proceed();
            }
        }.start();
    }

    private synchronized void proceed()
    {
//        counterOfActions++;
//        if(counterOfActions >= numOfActionsBeforeProceed)
        {
            if (mOnDoneDownloadingListener != null)
                mOnDoneDownloadingListener.onDone(internalOutputFileName, internalOutputFastFileName, internalOutputSlowFileName);
        }
    }

    private void changePitch()
    {
        try
        {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(tempOutputFileName));
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(internalOutputFileName));
            readAndWriteWavHeaderFile(dataInputStream, dataOutputStream);

            Sonic sonic;
            byte modifiedSamples[] = new byte[2048];
            sonic = new Sonic(sampleRate, 1);
            sonic.setPitch(1.0275f);

            byte[] buffer = new byte[minimumBufferSize];
            int count;
            while (true)
            {
                count = dataInputStream.read(buffer);
                if (count == -1)
                    break;
                sonic.putBytes(buffer, count);

                int available = sonic.availableBytes();
                if (available > 0)
                {
                    if (modifiedSamples.length < available)
                    {
                        modifiedSamples = new byte[available * 2];
                    }
                    sonic.receiveBytes(modifiedSamples, available);
                    dataOutputStream.write(modifiedSamples, 0, available);
                }
            }

            sonic.close();
            dataOutputStream.flush();
            dataOutputStream.close();
            dataInputStream.close();

        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
        }
    }

//    private void createFastSlowTracks()
//    {
//        try
//        {
//            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(tempOutputFileName));
//
//            DataOutputStream dataOutputStreamFast = new DataOutputStream(new FileOutputStream(internalOutputFastFileName));
//            DataOutputStream dataOutputStreamSlow = new DataOutputStream(new FileOutputStream(internalOutputSlowFileName));
//
////            dataInputStream.mark(1024000); // one mega
//            readAndWriteWavHeaderFile(dataInputStream, dataOutputStreamFast);
//            dataInputStream.close();
//
//            dataInputStream = new DataInputStream(new FileInputStream(tempOutputFileName));
//            readAndWriteWavHeaderFile(dataInputStream, dataOutputStreamSlow);
//
//            Sonic sonicFast, sonicSlow;
//
//            byte modifiedSamplesFast[] = new byte[2048];
//            byte modifiedSamplesSlow[] = new byte[2048];
//
//            sonicFast = new Sonic(44100, 1);
//            sonicSlow = new Sonic(44100, 1);
//
//            sonicSlow.setSpeed(0.33f);
////            sonicSlow.setSpeed(0.5f);
//            sonicFast.setSpeed(2);
//
//
//            byte[] buffer = new byte[minimumBufferSize];
////            byte[] bufferFast = new byte[minimumBufferSize];
//            int count, available;
//            while (true)
//            {
//                count = dataInputStream.read(buffer);
//                if (count == -1)
//                    break;
//
//                sonicFast.putBytes(buffer, count);
//                sonicSlow.putBytes(buffer, count);
//
//                available = sonicFast.availableBytes();
//                if (available > 0)
//                {
//                    if (modifiedSamplesFast.length < available)
//                    {
//                        modifiedSamplesFast = new byte[available * 2];
//                    }
//                    sonicFast.receiveBytes(modifiedSamplesFast, available);
//                    dataOutputStreamFast.write(modifiedSamplesFast, 0, available);
//                }
//
//                available = sonicSlow.availableBytes();
//                if (available > 0)
//                {
//                    if (modifiedSamplesSlow.length < available)
//                    {
//                        modifiedSamplesSlow = new byte[available * 2];
//                    }
//                    sonicSlow.receiveBytes(modifiedSamplesSlow, available);
//                    dataOutputStreamSlow.write(modifiedSamplesSlow, 0, available);
//                }
//            }
//
//            sonicFast.close();
//            sonicSlow.close();
//            dataInputStream.close();
//
//            dataOutputStreamFast.flush();
//            dataOutputStreamSlow.flush();
//
//            dataOutputStreamFast.close();
//            dataOutputStreamSlow.close();
//        }
//        catch (Throwable throwable)
//        {
//            throwable.printStackTrace();
//        }
//    }

    private void createFastTrack()
    {
        try
        {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(tempOutputFileName));
            DataOutputStream dataOutputStreamFast = new DataOutputStream(new FileOutputStream(internalOutputFastFileName));

            readAndWriteWavHeaderFile(dataInputStream, dataOutputStreamFast);

            byte modifiedSamplesFast[] = new byte[2048];

            Sonic sonicFast = new Sonic(sampleRate, 1);
            sonicFast.setSpeed(2);
            byte[] buffer = new byte[minimumBufferSize];
            int count, available;
            while (true)
            {
                count = dataInputStream.read(buffer);
                if (count == -1)
                    break;

                sonicFast.putBytes(buffer, count);

                available = sonicFast.availableBytes();
                if (available > 0)
                {
                    if (modifiedSamplesFast.length < available)
                    {
                        modifiedSamplesFast = new byte[available * 2];
                    }
                    sonicFast.receiveBytes(modifiedSamplesFast, available);
                    dataOutputStreamFast.write(modifiedSamplesFast, 0, available);
                }

            }

            sonicFast.close();
            dataInputStream.close();

            dataOutputStreamFast.flush();
            dataOutputStreamFast.close();
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
        }
    }

    private void createSlowTrack()
    {
        try
        {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(tempOutputFileName));
            DataOutputStream dataOutputStreamSlow = new DataOutputStream(new FileOutputStream(internalOutputSlowFileName));

            readAndWriteWavHeaderFile(dataInputStream, dataOutputStreamSlow);

            byte modifiedSamplesSlow[] = new byte[2048];

            Sonic sonicSlow = new Sonic(sampleRate, 1);
            sonicSlow.setSpeed(0.5f);
            byte[] buffer = new byte[minimumBufferSize];
            int count, available;
            while (true)
            {
                count = dataInputStream.read(buffer);
                if (count == -1)
                    break;

                sonicSlow.putBytes(buffer, count);

                available = sonicSlow.availableBytes();
                if (available > 0)
                {
                    if (modifiedSamplesSlow.length < available)
                    {
                        modifiedSamplesSlow = new byte[available * 2];
                    }
                    sonicSlow.receiveBytes(modifiedSamplesSlow, available);
                    dataOutputStreamSlow.write(modifiedSamplesSlow, 0, available);
                }
            }

            sonicSlow.close();
            dataInputStream.close();

            dataOutputStreamSlow.flush();
            dataOutputStreamSlow.close();
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
        }
    }

//    private void initialSetup()
//    {
//        try
//        {
//            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(tempOutputFileName));
//            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(internalOutputFileName));
//
//            DataOutputStream dataOutputStreamFast = new DataOutputStream(new FileOutputStream(internalOutputFastFileName));
//            DataOutputStream dataOutputStreamSlow = new DataOutputStream(new FileOutputStream(internalOutputSlowFileName));
//
//            readAndWriteWavHeaderFileMulti(dataInputStream, dataOutputStream, dataOutputStreamFast, dataOutputStreamSlow);
//
//            Sonic sonic;
//            byte modifiedSamples[] = new byte[2048];
//            sonic = new Sonic(44100, 1);
//            sonic.setPitch(1.0275f);
//
//            Sonic sonicFast, sonicSlow;
//
//            byte modifiedSamplesFast[] = new byte[2048];
//            byte modifiedSamplesSlow[] = new byte[2048];
//
//            sonicFast = new Sonic(44100, 1);
//            sonicSlow = new Sonic(44100, 1);
//
//            sonicSlow.setSpeed(0.33f);
////            sonicSlow.setSpeed(0.5f);
//            sonicFast.setSpeed(2);
//
//            byte[] buffer = new byte[minimumBufferSize];
//            int count, available;
//            while (true)
//            {
//                count = dataInputStream.read(buffer);
//                if (count == -1)
//                    break;
//
//                sonic.putBytes(buffer, count);
//                sonicFast.putBytes(buffer, count);
//                sonicSlow.putBytes(buffer, count);
//
//                available = sonic.availableBytes();
//                if (available > 0)
//                {
//                    if (modifiedSamples.length < available)
//                    {
//                        modifiedSamples = new byte[available * 2];
//                    }
//                    sonic.receiveBytes(modifiedSamples, available);
//                    dataOutputStream.write(modifiedSamples, 0, available);
//                }
//
//                available = sonicFast.availableBytes();
//                if (available > 0)
//                {
//                    if (modifiedSamplesFast.length < available)
//                    {
//                        modifiedSamplesFast = new byte[available * 2];
//                    }
//                    sonicFast.receiveBytes(modifiedSamplesFast, available);
//                    dataOutputStreamFast.write(modifiedSamplesFast, 0, available);
//                }
//
//                available = sonicSlow.availableBytes();
//                if (available > 0)
//                {
//                    if (modifiedSamplesSlow.length < available)
//                    {
//                        modifiedSamplesSlow = new byte[available * 2];
//                    }
//                    sonicSlow.receiveBytes(modifiedSamplesSlow, available);
//                    dataOutputStreamSlow.write(modifiedSamplesSlow, 0, available);
//                }
//            }
//
//            sonic.close();
//            sonicFast.close();
//            sonicSlow.close();
//
//            dataOutputStreamFast.flush();
//            dataOutputStreamSlow.flush();
//            dataOutputStream.flush();
//
//            dataOutputStreamFast.close();
//            dataOutputStreamSlow.close();
//            dataOutputStream.close();
//            dataInputStream.close();
//        }
//        catch (Throwable throwable)
//        {
//            throwable.printStackTrace();
//        }
//    }

    public boolean isSoundTrackSet()
    {
        return soundTrackSet;
    }

    private class RecordAudioRunnable implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                short[]buffer = new short[minimumBufferSize];
                while (isRecording)
                {
                    int bufferReadResult = audioRecord.read(buffer, 0, minimumBufferSize);
                    for (int i = 0; i < bufferReadResult; i++) {
                        outputStream.writeShort(buffer[i]);
                    }
                }
                audioRecord.stop();
                isDoneReadingData = true;
            }
            catch (Throwable t)
            {
                t.printStackTrace();
            }
        }
    }

    public void close()
    {
        mOnDoneDownloadingListener = null;
        mContext = null;
        if(outputStream != null)
            try{outputStream.close();}catch (Throwable err){}
        try
        {
            audioRecord.release();
            audioRecord = null;
            outputStream = null;
        }catch (Throwable err){}
    }

    public interface OnDoneDownloadingListener
    {
        void onDone(String downloadedFilePath, String downloadedFilePathFast, String downloadedFilePathSlow);
    }

    private int sampleRate;
    public void readAndWriteWavHeaderFile(InputStream wavInputStream, OutputStream wavOutputStream) throws Throwable
    {
        int HEADER_SIZE = 44;
        byte[]buffer = new byte[HEADER_SIZE];

        int readCount = wavInputStream.read(buffer);
        if(readCount != buffer.length)
            throw new Throwable("Something went wrong, the bytes read count is not the same length as the buffer size");
        wavOutputStream.write(buffer);
        if(sampleRate == 0)
            sampleRate = getIntFromBufferLittleIndian(buffer, 24); // at byte 24 we have the sample rate, part of the wave header

        int endOfHeaderFlag = getIntFromBufferLittleIndian(buffer, 36);
        int skipSize;
        boolean isFirst = true;

        while (endOfHeaderFlag != 0x61746164)
        {
            if(isFirst)
            {
                skipSize = getIntFromBufferLittleIndian(buffer, 40);
                isFirst = false;
            }
            else skipSize = getIntFromBufferLittleIndian(buffer, 4);

            buffer = new byte[skipSize];

            readCount = wavInputStream.read(buffer);

            if(readCount != buffer.length)
                throw new Throwable("Something went wrong, the bytes read count is not the same length as the buffer size");
            wavOutputStream.write(buffer);

            buffer = new byte[8]; // two ints :)

            readCount = wavInputStream.read(buffer);

            if(readCount != buffer.length)
                throw new Throwable("Something went wrong, the bytes read count is not the same length as the buffer size");
            wavOutputStream.write(buffer);
            endOfHeaderFlag = getIntFromBufferLittleIndian(buffer, 0);
        }
        wavOutputStream.flush();
    }

//    public void readAndWriteWavHeaderFileMulti(InputStream wavInputStream, OutputStream wavOutputStream, OutputStream wavOutputStreamFast, OutputStream wavOutputStreamSlow) throws Throwable
//    {
//        int HEADER_SIZE = 44;
//        byte[]buffer = new byte[HEADER_SIZE];
//
//        int readCount = wavInputStream.read(buffer);
//        if(readCount != buffer.length)
//            throw new Throwable("Something went wrong, the bytes read count is not the same length as the buffer size");
//        wavOutputStream.write(buffer);
//        wavOutputStreamFast.write(buffer);
//        wavOutputStreamSlow.write(buffer);
//
//        int endOfHeaderFlag = getIntFromBufferLittleIndian(buffer, 36);
//        int skipSize;
//        boolean isFirst = true;
//
//        while (endOfHeaderFlag != 0x61746164)
//        {
//            if(isFirst)
//            {
//                skipSize = getIntFromBufferLittleIndian(buffer, 40);
//                isFirst = false;
//            }
//            else skipSize = getIntFromBufferLittleIndian(buffer, 4);
//
//            buffer = new byte[skipSize];
//
//            readCount = wavInputStream.read(buffer);
//            if(readCount != buffer.length)
//                throw new Throwable("Something went wrong, the bytes read count is not the same length as the buffer size");
//            wavOutputStream.write(buffer);
//            wavOutputStreamFast.write(buffer);
//            wavOutputStreamSlow.write(buffer);
//
//            buffer = new byte[8]; // two ints :)
//
//            readCount = wavInputStream.read(buffer);
//            if(readCount != buffer.length)
//                throw new Throwable("Something went wrong, the bytes read count is not the same length as the buffer size");
//            wavOutputStream.write(buffer);
//            wavOutputStreamFast.write(buffer);
//            wavOutputStreamSlow.write(buffer);
//            endOfHeaderFlag = getIntFromBufferLittleIndian(buffer, 0);
//        }
//        wavOutputStream.flush();
//        wavOutputStreamFast.flush();
//        wavOutputStreamSlow.flush();
//    }


    private int getIntFromBufferLittleIndian(byte[] buffer, int startOffset)
    {
        int retInt = ((buffer[startOffset + 3] & 0xFF) << 24) | ((buffer[startOffset + 2] & 0xFF) << 16)
            | ((buffer[startOffset + 1] & 0xFF) << 8) | (buffer[startOffset] & 0xFF);
        return retInt;
    }

}