package fm.bling.blingy.discover.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import fm.bling.blingy.videoHome.model.CAAVideo;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/3/16.
 * History:
 * ***********************************
 */
public class Videos {

    @SerializedName("videos")
    ArrayList<CAAVideo> videos;

    public ArrayList<CAAVideo> getVideos() {
        return videos;
    }
}
