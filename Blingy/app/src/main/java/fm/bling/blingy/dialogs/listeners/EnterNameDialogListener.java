package fm.bling.blingy.dialogs.listeners;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 3/6/16.
 * History:
 * ***********************************
 */
public interface EnterNameDialogListener
{
    void onDoneClicked();

    void onCancel();

}