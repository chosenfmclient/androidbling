package fm.bling.blingy.editVideo.rest.model;

import com.google.gson.annotations.SerializedName;


/**
 * Created by Chosen-pro on 2/16/16.
 */
public class CAAPostTags {
    @SerializedName("tags")
    private String tags;

    public CAAPostTags(String tags) {
        this.tags = tags;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
