package fm.bling.blingy.record;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: ProgressListener
 * Description:
 * Created by Dawidowicz Nadav on 7/20/15.
 * History:
 * ***********************************
 */
public interface ProgressListener {
    void transferred(long num);
}
