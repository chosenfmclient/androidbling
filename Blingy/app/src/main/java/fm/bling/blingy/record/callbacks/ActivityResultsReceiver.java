package fm.bling.blingy.record.callbacks;

import android.content.Intent;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 3/3/16.
 * History:
 * ***********************************
 */
public interface ActivityResultsReceiver
{
    void onActivityResult(int requestCode, int resultCode, Intent data);

}