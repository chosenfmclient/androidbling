package fm.bling.blingy.videoHome;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.videoHome.fragments.CommentsFragment;
import fm.bling.blingy.videoHome.fragments.LikesFragment;
import fm.bling.blingy.videoHome.model.CAAVideo;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 8/30/16.
 * History:
 * ***********************************
 */
public class VideoSocialDetailsActivity extends BaseActivity {

    private ViewPager mViewPager;
    private ScaledImageView mBluredImage;

    public static final int COMMENTS_POS = 0;
    public static final int LIKES_POS = 1;
//    public static final int ACTIVITY_POS = 2;

    private CAAVideo mVideo;
    private String mVideoId;
    private String mMediaType;
//    public static boolean mActionComment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_social_details_layout);

        showWidget = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_20));
            ((FrameLayout.LayoutParams) findViewById(R.id.main_content).getLayoutParams()).topMargin = App.STATUS_BAR_HEIGHT;
        }
        String songName = "";
        String videoImage = "";
        int mSelectedTab;

        Bundle b = getIntent().getBundleExtra(Constants.BUNDLE);
        mVideo = b.getParcelable(Constants.CAAVIDEO);
        mVideoId = mVideo.getVideoId();
        mMediaType = mVideo.getMediaType();
        songName = mVideo.getSongName();
        videoImage = mVideo.getFirstFrame();
        mSelectedTab = b.getInt(Constants.SELECTED_TAB, -1);


        mBluredImage = (ScaledImageView) findViewById(R.id.blured_image);
        mBluredImage.setUrl(videoImage);
        mBluredImage.setBlurRadius(70);
        mBluredImage.setDontUseExisting(true);
        mBluredImage.setAnimResID(R.anim.fade_in);
        mBluredImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mBluredImage.setImageLoader(getMultiImageLodaer());
        getMultiImageLodaer().DisplayImage(mBluredImage);

        Bundle bundle = new Bundle();
        bundle.putString(Constants.VIDEO_ID, mVideoId);

        //Setting up the navigation icon.
        Toolbar mToolbar = getActionBarToolbar();
        mToolbar.setBackgroundResource(R.color.transparent);
        mToolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        mToolbar.setNavigationContentDescription("backClose");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(songName);

        TabLayout mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mTabLayout.addTab(mTabLayout.newTab().setText("COMMENTS"));
        mTabLayout.addTab(mTabLayout.newTab().setText("LIKES"));

        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter mPagerAdapter = new PagerAdapter(getSupportFragmentManager(), mTabLayout.getTabCount());
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        if (mSelectedTab >= 0 && mSelectedTab <= 1)
            mViewPager.setCurrentItem(mSelectedTab);

    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case COMMENTS_POS:
                    return CommentsFragment.newInstance(mVideoId, mMediaType);
                case LIKES_POS:
                    return LikesFragment.newInstance(mVideoId,mVideo,mMediaType);
//                case ACTIVITY_POS:
//                    return ActivitiesFragment.newInstance(mVideoId);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }
    }

}
