package fm.bling.blingy.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 28/07/2016.
 * History:
 * ***********************************
 */
public class TouchBlockerDialog extends Dialog
{
    public TouchBlockerDialog(Context context)
    {
        super(context, android.R.style.Theme_Panel);
        View v = new View(context);
        v.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        setContentView(v);
        setCancelable(false);
        show();
    }

}