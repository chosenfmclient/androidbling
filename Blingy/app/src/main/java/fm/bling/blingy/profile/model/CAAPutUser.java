package fm.bling.blingy.profile.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAPutUser
 * Description:
 * Created by Oren Zakay on 21/09/16.
 * History:
 * ***********************************
 */
public class CAAPutUser {

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("twitter_handle")
    private String twitterHandle;

    @SerializedName("instagram_handle")
    private String instagramHandle;

    @SerializedName("youtube_handle")
    private String youtubeHandle;

    @SerializedName("facebook_handle")
    private String facebookHandle;

    public CAAPutUser(String firstName, String lastName, String twitterHandle, String youtubeHandle , String instagramHandle, String facebookHandle) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.twitterHandle = twitterHandle;
        this.instagramHandle = instagramHandle;
        this.youtubeHandle = youtubeHandle;
        this.facebookHandle = facebookHandle;
    }

}
