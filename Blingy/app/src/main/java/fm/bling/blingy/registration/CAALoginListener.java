package fm.bling.blingy.registration;

/**
 * Created by Zach Gerstman on 6/9/15.
 */
public interface CAALoginListener {
    void didFinishLogin(Boolean success, int status);
}
