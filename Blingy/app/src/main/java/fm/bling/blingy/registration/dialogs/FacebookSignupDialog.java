package fm.bling.blingy.registration.dialogs;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import fm.bling.blingy.R;

/**
 * Created by Chosen-pro on 7/23/15.
 */
public class FacebookSignupDialog extends Fragment {

    public static FacebookSignupDialog newInstance() {
        return new FacebookSignupDialog();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.facebook_loading_dialogue, null);
        FrameLayout dialog = (FrameLayout)v.findViewById(R.id.facebook_loading) ;
        ProgressBar progressBar = (ProgressBar)v.findViewById(R.id.progressBar1);
        try {
            progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.main_blue), android.graphics.PorterDuff.Mode.MULTIPLY);
            dialog.setFocusable(true);
            dialog.setClickable(true);
        }catch (Throwable throwable){}
        return v;
    }
}
