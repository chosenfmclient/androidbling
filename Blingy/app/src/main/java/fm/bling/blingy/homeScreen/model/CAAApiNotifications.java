package fm.bling.blingy.homeScreen.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nadav on 5/6/15.
 */
public class CAAApiNotifications {

    @SerializedName("data")
    private ArrayList<CAANotification> data;

    @SerializedName("counters")
    private CAACounters counters;

    public ArrayList<CAANotification> getData() {
        return data;
    }

    public void setData(ArrayList<CAANotification> data) {
        this.data = data;
    }

    public CAACounters getCounters() {
        return counters;
    }

    public void setCounters(CAACounters counters) {
        this.counters = counters;
    }

}

