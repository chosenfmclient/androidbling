package fm.bling.blingy.registration.rest.model;

import com.google.gson.annotations.SerializedName;


/**
 * Created by Chosen-pro on 6/7/15.
 */
public class CAAEmailErrorBody {

    @SerializedName("recordFound")
    private String recordFound;

    @SerializedName("emailAddressInvalidHostname")
    private String emailAddressInvalidHostname;

    @SerializedName("hostnameInvalidHostname")
    private String hostnameInvalidHostname;

    @SerializedName("hostnameLocalNameNotAllowed")
    private String hostnameLocalNameNotAllowed;

    @SerializedName("emailAddressInvalidFormat")
    private String emailAddressInvalidFormat;

    @SerializedName("emailAddressDotAtom")
    private String emailAddressDotAtom;

    @SerializedName("emailAddressQuotedString")
    private String emailAddressQuotedString;

    @SerializedName("emailAddressInvalidLocalPart")
    private String emailAddressInvalidLocalPart;

    public String getRecordFound() {
        return recordFound;
    }

    public String getEmailAddressInvalidHostname() {
        return emailAddressInvalidHostname;
    }

    public String getHostnameInvalidHostname() {
        return hostnameInvalidHostname;
    }

    public String getHostnameLocalNameNotAllowed() {
        return hostnameLocalNameNotAllowed;
    }


    public String getEmailAddressInvalidFormat() {
        return emailAddressInvalidFormat;
    }

}
