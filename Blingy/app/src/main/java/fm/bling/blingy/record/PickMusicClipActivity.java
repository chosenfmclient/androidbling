package fm.bling.blingy.record;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.ScrollToTopBaseActivity;
import fm.bling.blingy.dialogs.listeners.NetworkChangedListener;
import fm.bling.blingy.discover.DiscoverActivity;
import fm.bling.blingy.record.adapters.PickMusicRecyclerAdapter;
import fm.bling.blingy.record.rest.model.CAAItunesMusicElement;
import fm.bling.blingy.record.rest.model.CAAiTunes;
import fm.bling.blingy.songHome.SongHomeActivity;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.views.ProgressBarView;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 9/7/16.
 * History:
 * ***********************************
 */
public class PickMusicClipActivity extends ScrollToTopBaseActivity implements NetworkChangedListener{

    private boolean isPlaying = false;
    private boolean isPrepared = false;
    private boolean isLoadingMusic = false;
    private static ArrayList<CAAItunesMusicElement> clips = null;
    private ArrayList<CAAItunesMusicElement> mDataSet = null;
    private Timer timer;

    /**
     * Views and utils
     */
    private LinearLayout mTopBar;
    private ImageView mBackButton;
    private ImageView mClear;
    private EditText mSearchEditText;
    private MediaPlayer mMediaPlayer;
    private FrameLayout mNoResults;
    private ProgressBarView mChosenProgerss;
    private RecyclerView mClipsRecycler;
    private PickMusicRecyclerAdapter mAdapter;
    private View lastView;
    private ImageView lastViewImage;
    private RecyclerView.LayoutManager mLayoutManager;

    private View.OnClickListener mSelectedClickListener;
    private View.OnClickListener mPlayPauseClickListener;
    private View.OnClickListener moveToSongHomeListener;

    private boolean controlsVisible = true;
    private boolean isFirst = true;
    private BroadcastReceiver audioReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pick_music_clip_layout);
        getWindow().setBackgroundDrawable(null);
        init();
        listenToAudioChanges();
        TrackingManager.getInstance().pageView(EventConstants.PICK_MUSIC_PAGE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_20));
        }

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mClipsRecycler.setLayoutManager(mLayoutManager);
        mClipsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstVisibleItem = ((LinearLayoutManager) mLayoutManager).findFirstCompletelyVisibleItemPosition();
                if(firstVisibleItem == -1)
                    return;
                if (firstVisibleItem == 0) {
                    if (!controlsVisible) {
                        //show search bar
                        mTopBar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
                        controlsVisible = true;
                    }
                } else {
                    if (controlsVisible) {
                        //hide search bar
                        mTopBar.animate().translationY(-mTopBar.getHeight() - App.STATUS_BAR_HEIGHT).setInterpolator(new AccelerateInterpolator(2));
                        controlsVisible = false;
                        closeKeyboard();
                    }
                }
            }

        });

        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getClips();
                mBackButton.setVisibility(View.GONE);
                mClear.setVisibility(View.INVISIBLE);
                ((LinearLayout.LayoutParams)mSearchEditText.getLayoutParams()).leftMargin = getResources().getDimensionPixelOffset(R.dimen.main_margin);
                mSearchEditText.setCompoundDrawablePadding(getResources().getDimensionPixelOffset(R.dimen.blocks_margin));
                mSearchEditText.setText("");
                mSearchEditText.clearFocus();
                closeKeyboard();
            }
        });

        mClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchEditText.setText("");
            }
        });

        mSearchEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP && Utils.canClick()){
                    ((LinearLayout.LayoutParams)mSearchEditText.getLayoutParams()).leftMargin = 0;
                    mSearchEditText.setCompoundDrawablePadding(0);
                    mBackButton.setVisibility(View.VISIBLE);
                    mClear.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });

        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                search(mSearchEditText.getText().toString());
                            }
                        });
                    }
                }, 600);
            }
        });

        mSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    mSearchEditText.clearFocus();
                    closeKeyboard();
                    return true;
                }
                return false;
            }
        });
        loadListeners();

        if(!getIntent().hasExtra(Constants.TERM)) {
            getClips();
        }
        else{
            search(getIntent().getStringExtra(Constants.TERM));
            getIntent().removeExtra(Constants.TERM);
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        if(!isFirst && mClipsRecycler != null && mAdapter != null) {
            mAdapter.setImageLoaderManager(getMultiImageLodaer());
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isFirst = false;
        if(lastView != null)
            lastView.setVisibility(View.VISIBLE);
        if(lastViewImage != null)
            lastViewImage.setVisibility(View.VISIBLE);
        lastView = null;
        releaseMediaPlayer();
//        if(mAdapter != null)
//            mAdapter.close();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(audioReceiver != null)
            unregisterReceiver(audioReceiver);
    }

    private void init(){
        mTopBar = (LinearLayout) findViewById(R.id.bar);
        mBackButton = (ImageView) findViewById(R.id.back_button);
        mClear = (ImageView) findViewById(R.id.clear_button);
        mSearchEditText = (EditText) findViewById(R.id.search_edit_text);
        mClipsRecycler = (RecyclerView) findViewById(R.id.clips_recycler);
        mChosenProgerss = (ProgressBarView) findViewById(R.id.progress_bar);mChosenProgerss.updateLayout();
        mNoResults = (FrameLayout) findViewById(R.id.empty_view);

        ((FrameLayout.LayoutParams)mTopBar.getLayoutParams()).topMargin = App.STATUS_BAR_HEIGHT;
        mChosenProgerss.setVisibility(View.VISIBLE);
        ((FrameLayout.LayoutParams)mNoResults.getLayoutParams()).topMargin = App.STATUS_BAR_HEIGHT + App.TOOLBAR_HEIGHT;
    }

    private void loadListeners() {
        mSelectedClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAAItunesMusicElement caaItunesMusicElement = ((CAAItunesMusicElement) v.getTag());
                Intent intent = new Intent(PickMusicClipActivity.this, CreateVideoActivity.class);
                intent.putExtra(Constants.CLIP_URL, caaItunesMusicElement.getPreviewUrl());
                intent.putExtra(Constants.CLIP_THUMBNAIL, caaItunesMusicElement.getArtworkUrl100());
                intent.putExtra(Constants.SONG_NAME, caaItunesMusicElement.getTrackName());
                intent.putExtra(Constants.ARTIST_NAME, caaItunesMusicElement.getArtistName());
                intent.putExtra(Constants.CLIP_ID, caaItunesMusicElement.getClipId());
                intent.putIntegerArrayListExtra(Constants.TIME_SEGMENTS, caaItunesMusicElement.getmTimeSegments());
                if(clips != null)
                    TrackingManager.getInstance().tapShootNow("seed_home",caaItunesMusicElement.getTrackId(), caaItunesMusicElement.getArtistName(), caaItunesMusicElement.getTrackName(), clips.indexOf(caaItunesMusicElement));
                getRecordPermissions(intent);
            }
        };

        moveToSongHomeListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAAItunesMusicElement caaItunesMusicElement = ((CAAItunesMusicElement) v.getTag());
                Intent intent = new Intent(PickMusicClipActivity.this, SongHomeActivity.class);
                intent.putExtra(Constants.IS_SONG_HOME, true);
                intent.putExtra(Constants.CLIP_ID, caaItunesMusicElement.getClipId());
                intent.putExtra(Constants.CLIP_THUMBNAIL, caaItunesMusicElement.getArtworkUrl100());
                intent.putExtra(Constants.SONG_NAME, caaItunesMusicElement.getTrackName());
                intent.putExtra(Constants.ARTIST_NAME, caaItunesMusicElement.getArtistName());
                intent.putIntegerArrayListExtra(Constants.TIME_SEGMENTS, caaItunesMusicElement.getmTimeSegments());
                startActivity(intent);
            }
        };

        mPlayPauseClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                try {
                    if (!isLoadingMusic) {
                        View view = ((View) view1.getTag());
                        FrameLayout item = ((FrameLayout) view.getTag());
                        TextureView textureView = ((TextureView) item.getTag());
                        ScaledImageView imageView = ((ScaledImageView) item.findViewById(R.id.clip_image));
                        ProgressBarView progressBar = ((ProgressBarView) textureView.getTag());
                        if (mMediaPlayer != null) {
                            if (mMediaPlayer.isPlaying()) {
                                if (view != lastView) {
                                    lastView.setVisibility(View.VISIBLE);
                                    lastViewImage.setVisibility(View.VISIBLE);
                                    lastView = view;
                                    lastViewImage = imageView;
                                    mMediaPlayer.reset();
                                    MusicPrepare(textureView, progressBar);
                                } else {
                                    isPlaying = false;
                                    lastView.setVisibility(View.VISIBLE);
                                    mMediaPlayer.pause();

                                }
                            } else {
                                if (view != lastView) {
                                    lastView = view;
                                    lastViewImage = imageView;
                                    mMediaPlayer.reset();
                                    MusicPrepare(textureView, progressBar);
                                } else {
                                    playMusic(progressBar);
                                }
                            }
                        } else {
                            lastView = view;
                            lastViewImage = imageView;
                            mMediaPlayer = new MediaPlayer();
                            MusicPrepare(textureView, progressBar);
                        }
                    }
                }catch (Throwable throwable){ throwable.printStackTrace(); }
            }
        };



        mClipsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isPlaying) {
                    int[] pos = new int[2];
                    lastViewImage.getLocationOnScreen(pos);
                    if (((pos[1] - App.STATUS_BAR_HEIGHT) <= ((lastViewImage.getHeight() * -1) + 50)) || pos[1] + 20 >= App.HEIGHT) {
                        isPlaying = false;
                        if (mMediaPlayer != null) {
                            mMediaPlayer.stop();
                            mMediaPlayer.reset();
                        }
                        if(lastView != null)
                            lastView.setVisibility(View.VISIBLE);
                        if (lastViewImage != null)
                            lastViewImage.setVisibility(View.VISIBLE);

                    }
                }
            }
        });
    }

    private void getClips() {
        if (clips == null) {
            App.getUrlService().getRecordClips(new Callback<CAAiTunes>() {
                @Override
                public void success(CAAiTunes caAiTunes, Response response) {
                    if(isActivityVisible) {
                        if (!caAiTunes.getResultCount().equalsIgnoreCase("") && Integer.parseInt(caAiTunes.getResultCount()) > 0) {
                            prepareData(caAiTunes.getResults());
                            clips = new ArrayList<>();
                            for (int i = 0; i < caAiTunes.getResults().size(); i++) {
                                clips.add(i, caAiTunes.getResults().get(i));
                            }
                        } else
                            mNoResults.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        } else {
            mDataSet = new ArrayList<>();
            for (int i = 0; i < clips.size(); i++) {
                mDataSet.add(i, clips.get(i));
            }
            prepareData(mDataSet);
        }
    }


    private void prepareData(ArrayList<CAAItunesMusicElement> data) {
        mChosenProgerss.setVisibility(View.GONE);
        mNoResults.setVisibility(View.GONE);
        mAdapter = new PickMusicRecyclerAdapter(data, getMultiImageLodaer(), mSelectedClickListener, mPlayPauseClickListener, moveToSongHomeListener);
        mClipsRecycler.setAdapter(mAdapter);
    }

    private void search(String newText) {
        if(newText.length() >= 3) {
            if (mAdapter != null)
                mAdapter.clearData();
            mNoResults.setVisibility(View.GONE);
            mChosenProgerss.setVisibility(View.VISIBLE);
            TrackingManager.getInstance().searchMusicClip(newText);
            App.getUrlService().getSearchVideo("clip", newText, new Callback<CAAiTunes>() {
                @Override
                public void success(CAAiTunes caAiTunes, Response response) {
                    if (isActivityVisible) {
                        if (caAiTunes != null && caAiTunes.getResults() != null && caAiTunes.getResults().size() > 0)
                            prepareData(caAiTunes.getResults());
                        else {
                            mChosenProgerss.setVisibility(View.GONE);
                            mNoResults.setVisibility(View.VISIBLE);
                            if (mAdapter != null && mAdapter.getItemCount() > 0)
                                mAdapter.clearData();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (isActivityVisible)
                        mChosenProgerss.setVisibility(View.GONE);
                }
            });
        }
    }

    private void attachSurface(final TextureView textureView, final ProgressBarView progressBar) {
        if (textureView.isAvailable()) {
            mMediaPlayer.setSurface(new Surface(textureView.getSurfaceTexture()));
            if (isPrepared && !isPlaying)
                playMusic(progressBar);
        } else {
            textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
                @Override
                public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                    mMediaPlayer.setSurface(new Surface(textureView.getSurfaceTexture()));
                    if (isPrepared && !isPlaying)
                        playMusic(progressBar);
                }

                @Override
                public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                }

                @Override
                public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                    return false;
                }

                @Override
                public void onSurfaceTextureUpdated(SurfaceTexture surface) {
                }
            });
        }
    }

    private void MusicPrepare(final TextureView textureView, final ProgressBarView progressBar) {
        closeKeyboard();
        isLoadingMusic = true;
        isPrepared = false;
        isPlaying = false;
        String url = ((CAAItunesMusicElement) progressBar.getTag()).getPreviewUrl();
        // Set type to streaming
        showProgressBar(progressBar);

        attachSurface(textureView, progressBar);

        // Listen for if the audio file can't be prepared
        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                releaseMediaPlayer();
                if(lastView != null)
                    lastView.setVisibility(View.GONE);
                if(lastViewImage != null)
                    lastViewImage.setVisibility(View.VISIBLE);
                return false;
            }
        });
        // Attach to when audio file is prepared for playing
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                isPrepared = true;
                adjustTextureViewSize(textureView);
                if (!isPlaying)
                    playMusic(progressBar);
            }
        });
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                if(lastView != null)
                    lastView.setVisibility(View.VISIBLE);
                if(lastViewImage != null)
                    lastViewImage.setVisibility(View.VISIBLE);
                isLoadingMusic = false;
            }
        });
        // Set the data source to the remote URL
        // Trigger an async preparation which will file listener when completed
        try {
            mMediaPlayer.setDataSource(url);
            mMediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private  void adjustTextureViewSize(TextureView textureView) {
        if (textureView == null || mMediaPlayer == null)
            return;

        float viewWidth = App.WIDTH;
        float viewHeight = App.landscapitemHeight;
        //Set it Top Centered.
        int yoff = 0;

        double aspectRatio;
        try
        {
            aspectRatio = ((double) mMediaPlayer.getVideoHeight()) / ((double) mMediaPlayer.getVideoWidth());
        }
        catch (Throwable err)
        {
            err.printStackTrace();
            return;
        }

        final float newWidth, newHeight;

        if(mMediaPlayer.getVideoWidth() == mMediaPlayer.getVideoHeight()){
            newWidth = viewWidth;
            newHeight = (int) (viewWidth * aspectRatio);
        }
        else if (mMediaPlayer.getVideoWidth() < mMediaPlayer.getVideoHeight())
        {
            // limited by narrow width; restrict height
            newWidth = viewWidth;
            newHeight = (int) (viewWidth * aspectRatio);
        }
        else
        {
            // limited by short height; restrict width
            newWidth = (int) (viewHeight / aspectRatio);
            newHeight = viewHeight;
        }

        int xoff = (int) ((viewWidth - newWidth) / 2f);
        Matrix txform = new Matrix();
        textureView.getTransform(txform);
        txform.postTranslate(xoff, yoff);
        txform.setScale(newWidth / viewWidth, newHeight / viewHeight, (int) ((viewWidth) / 2f), (int) ((viewHeight) / 2f)); // center crop

        textureView.setTransform(txform);
        textureView.requestLayout();

    }

    private void releaseMediaPlayer() {
        try {
            if (mMediaPlayer != null) {
                mMediaPlayer.stop();
                mMediaPlayer.reset();
                mMediaPlayer = null;
            }
        }catch (Throwable throwable){

        }
    }

    private void playMusic(ProgressBarView progressBar) {
        if(mMediaPlayer != null) {
            isPlaying = true;
            gainAudioFocus();
            hideProgressBar(progressBar);
            lastViewImage.setVisibility(View.GONE);
            mMediaPlayer.start();
            (lastView).setVisibility(View.GONE);
            isLoadingMusic = false;

        }
    }

    private void showProgressBar(ProgressBarView progressBar) {
        lastView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(ProgressBarView progressBar) {
        progressBar.setVisibility(View.GONE);
        lastView.setVisibility(View.VISIBLE);
    }

    @Override
    public void scrollToTop() {
        try {
            if (mClipsRecycler != null)
                mClipsRecycler.smoothScrollToPosition(0);
        }catch (Throwable throwable) { throwable.printStackTrace(); }
    }

    private void listenToAudioChanges() {
        IntentFilter filter = new IntentFilter(Constants.AUDIO_FOCUS_CHANGED);
        audioReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int focus = intent.getIntExtra(Constants.FOCUS, -1);
                switch (focus){
                    case AudioManager.AUDIOFOCUS_LOSS:
                        if(lastView != null && mMediaPlayer != null && isPlaying ) {
                            mMediaPlayer.pause();
                            isPlaying = false;
                            lastView.setVisibility(View.VISIBLE);
                        }
                        break;
                }

            }
        };
        registerReceiver(audioReceiver, filter);
    }

    @Override
    public int getSelfID() {
        return RECORD_SCREEN;
    }

    @Override
    public void onNetworkChangedListener(boolean isConnected) {
        if(!isConnected)
            mChosenProgerss.setVisibility(View.GONE);
    }
}
