package fm.bling.blingy.registration;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.net.HttpURLConnection;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
import fm.bling.blingy.registration.rest.model.CAAPostPassword;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Zach Gerstman on 6/3/15.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener, CAALoginListener {

    private String email = "";
    private TextView invalidText;
    private ImageView invalidImage;
    private Button loginButton;
    private Boolean recoverPasswordPage = false;
    private Boolean newPasswordPage = false;
    private LinearLayout mLoginContainer;
    private LinearLayout mForgotContainer;
    private LinearLayout mNewPasswordContainer;
    private String password = "";
    private Activity activity;
    private Toolbar mToolbar;

    private boolean canLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        mLoginContainer = (LinearLayout)findViewById(R.id.login_container);
        mForgotContainer = (LinearLayout)findViewById(R.id.forgot_password_container);
        mNewPasswordContainer = (LinearLayout)findViewById(R.id.new_password_container);
        Bundle b = getIntent().getExtras();
        TrackingManager.getInstance().pageView(EventConstants.LOGIN_PAGE);
        if (b != null && b.containsKey("email")) {
            email = b.getString("email");
        }
        activity = this;

        //Setting up the navigation icon.
        mToolbar = getActionBarToolbar();
        mToolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
        mToolbar.setNavigationContentDescription("backClose");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              onBackPressed();
            }
        });

        initializeLoginScreen();
    }

    @Override
    public void onBackPressed() {
        if (recoverPasswordPage) {
            initializeLoginScreen();
            recoverPasswordPage = false;
        } else if (newPasswordPage) {
            forgotPassword();
            recoverPasswordPage = true;
            newPasswordPage = false;
        } else {
            TrackingManager.getInstance().loginCancel();
            super.onBackPressed();
        }
    }

    private void initializeLoginScreen() {
        recoverPasswordPage = false;
        newPasswordPage = false;
        mToolbar.setTitle(R.string.Login);
        mLoginContainer.setVisibility(View.VISIBLE);
        mForgotContainer.setVisibility(View.GONE);
        mNewPasswordContainer.setVisibility(View.GONE);
        final EditText editEmail = (EditText) findViewById(R.id.emailRegistration_textField_email_login);
        final EditText editPassword = (EditText) findViewById(R.id.emailRegistration_textField_password);
        editEmail.setText(email);
        final Button loginButton = (Button)findViewById(R.id.emailRegistration_button_next);
        loginButton.setOnClickListener(this);
        if (!canLogin) {
            loginButton.setAlpha(0.3f);
            loginButton.setEnabled(false);
        }
        TextView joinNow = (TextView) findViewById(R.id.textView19);
        joinNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editEmail.getText().toString().trim().isEmpty() && !editPassword.getText().toString().trim().isEmpty()) {
                    canLogin = true;
                    loginButton.setAlpha(1f);
                    loginButton.setEnabled(true);
                }
            }
        });

        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editEmail.getText().toString().trim().isEmpty() && !editPassword.getText().toString().trim().isEmpty()) {
                    canLogin = true;
                    loginButton.setAlpha(1f);
                    loginButton.setEnabled(true);
                }
            }
        });

        invalidText = (TextView) findViewById(R.id.text_view_invalid_login);
        invalidText.setVisibility(View.INVISIBLE);
        TextView forgotPasswordText = (TextView) findViewById(R.id.emailRegistration_button_subButton);
        forgotPasswordText.setOnClickListener(this);
    }

    private void emailLogin() {
        invalidText.setVisibility(View.INVISIBLE);
        EditText editEmailText = (EditText) findViewById(R.id.emailRegistration_textField_email_login);
        email = editEmailText.getText().toString();
        EditText editPasswordText = (EditText) findViewById(R.id.emailRegistration_textField_password);
        password = editPasswordText.getText().toString();
        CAAEmailLogin.EmailLogin(email, password, this);
    }

    @Override
    public void didFinishLogin(Boolean success, int status) {
        CAALogin.isAuto = "0";
        if (success) {
            setResult(RESULT_OK, null);
            CAAUserDataSingleton.getInstance().setSentGcmToken(false);
            finish();
            //user is logged in
        } else {
            if (status == HttpURLConnection.HTTP_FORBIDDEN) {
                String yesText = "RESEND EMAIL";
                String cancelText = getApplicationContext().getResources().getString(R.string.ok);
                String title = "Unconfirmed Account";
                String text = "Your account has been created but needs to be confirmed. Please click the confirmation link sent to your email.";
                BaseDialog mDialog = new BaseDialog(this,title,text);
                mDialog.setOKText(yesText);
                mDialog.setCancelText(cancelText);
                mDialog.show();
                mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (((BaseDialog)dialog).getAnswer()) {
                            resendEmail();
                        }
                    }
                });
            } else {
                invalidText.setVisibility(View.VISIBLE);
            }
        }
    }

    private void forgotPassword() {

        mToolbar.setTitle(R.string.password_recovery);
        mForgotContainer.setVisibility(View.VISIBLE);
        mLoginContainer.setVisibility(View.GONE);
        mNewPasswordContainer.setVisibility(View.GONE);
        recoverPasswordPage = true;
        invalidImage = (ImageView)findViewById(R.id.image_view_invalid);
        invalidImage.setVisibility(View.GONE);
        invalidText = (TextView) findViewById(R.id.text_view_invalid);
        invalidText.setVisibility(View.INVISIBLE);
        final Button resendButton = (Button) findViewById(R.id.emailRegistration_button_nextResendPassword);
        resendButton.setAlpha(0.3f);
        resendButton.setEnabled(false);
        final EditText editEmail = (EditText) findViewById(R.id.emailRegistration_textField_email);

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editEmail.getText().toString().isEmpty()) {
                    resendButton.setEnabled(true);
                    resendButton.setAlpha(1f);
                }
            }
        });

        resendButton.setOnClickListener(this);
    }

    private void  sendPassword() {
        EditText editTextEmail = (EditText) findViewById(R.id.emailRegistration_textField_email);
        email = editTextEmail.getText().toString();

        CAAPostPassword postPassword = new CAAPostPassword(email, Constants.APP_ID, Constants.APP_SECRET);

        App.getUrlService().postPassword(postPassword, new Callback<String>() {
            @Override
            public void success(String pass, Response response) {
                Log.d("CAAPostPassword", response.getReason());
                getSupportActionBar().setTitle(R.string.new_password_sent);
                mNewPasswordContainer.setVisibility(View.VISIBLE);
                mLoginContainer.setVisibility(View.GONE);
                mForgotContainer.setVisibility(View.GONE);
                newPasswordPage = true;
                recoverPasswordPage = false;
                loginButton = (Button) findViewById(R.id.emailRegistration_label_registration);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loginButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                initializeLoginScreen();
                                EditText editEmailText = (EditText) findViewById(R.id.emailRegistration_textField_email);
                                editEmailText.setText(email);
                            }
                        });
                    }
                });

            }

            @Override
            public void failure(RetrofitError error) {
                invalidImage.setVisibility(View.VISIBLE);
                invalidText.setVisibility(View.VISIBLE);
            }
        });
    }

    private void resendEmail() {
        String title = "Blingy";
        String text = "Confirmation has been sent to your email. Please check your email to confirm your registration";
        BaseDialog mDialog = new BaseDialog(this,title,text);
        mDialog.removeCancelbutton();
        mDialog.show();

        CAAPostPassword putSignup = new CAAPostPassword(email, Constants.APP_ID, Constants.APP_SECRET);
        App.getUrlService().putsSignup(putSignup, new Callback<String>() {
            @Override
            public void success(String email, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.emailRegistration_button_next:
                if (NetworkConnectivityListener.isConnectedWithDialog(this)) {
                    emailLogin();
                }
                break;
            case R.id.emailRegistration_button_subButton:
                forgotPassword();
                break;
            case R.id.emailRegistration_button_nextResendPassword:
                if (NetworkConnectivityListener.isConnectedWithDialog(this)) {
                    sendPassword();
                }
                break;
            case R.id.emailRegistration_label_registration:
                setContentView(R.layout.login);
                EditText editEmailText = (EditText) findViewById(R.id.emailRegistration_textField_email);
                editEmailText.setText(email);
                break;
            default:
                break;
        }
    }
}
