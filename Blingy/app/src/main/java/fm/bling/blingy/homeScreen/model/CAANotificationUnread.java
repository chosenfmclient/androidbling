package fm.bling.blingy.homeScreen.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 4/21/16.
 * History:
 * ***********************************
 */
public class CAANotificationUnread {

    @SerializedName("unread")
    private String unread;

    public String getUnread() {
        return unread;
    }

    public void setUnread(String unread) {
        this.unread = unread;
    }
}
