package fm.bling.blingy.inviteFriends.model;

import com.google.gson.annotations.SerializedName;


/**
 * Created by Chosen-pro on 11/8/15.
 */
public class CAAEmailInvite {
    @SerializedName("email")
    private String email;

    public CAAEmailInvite(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @SerializedName("name")

    private String name;
}
