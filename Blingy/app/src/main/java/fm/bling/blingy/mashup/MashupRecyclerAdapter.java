package fm.bling.blingy.mashup;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.utils.AdaptersDataTypes;
import fm.bling.blingy.videoHome.model.CAAVideo;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 1/8/17.
 * History:
 * ***********************************
 */
public class MashupRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<CAAVideo> mDataSet;
    private final int tileWidthHeight;
    private RecyclerView.ViewHolder dataObjectHolder;
    private View.OnClickListener itemClickListener;
//    private boolean showPrivate = false;
    private View header;

    public MashupRecyclerAdapter(ArrayList<CAAVideo> dataset, View.OnClickListener itemClickListener, View header) {
        this.mDataSet = dataset;
        this.itemClickListener = itemClickListener;
//        this.showPrivate = showPrivate;
        this.header = header;
        this.tileWidthHeight = (int) (((App.WIDTH / 3f)));
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return AdaptersDataTypes.HEADER;
        else
            return AdaptersDataTypes.BASE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == AdaptersDataTypes.HEADER) {
            dataObjectHolder = new HeaderHolder(header);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.discover_recycler_item, parent, false);
            dataObjectHolder = new MashupRecyclerAdapter.DataObjectHolder(view);
        }
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case AdaptersDataTypes.BASE_ITEM:
                if(mDataSet != null) {
                    CAAVideo caaVideo = mDataSet.get(position);
                    ((DataObjectHolder) holder).rippleView.setTag(caaVideo);

                    if (caaVideo.getIntroUrl() == null) {
                        ((DataObjectHolder) holder).clipImage.setImageURI(caaVideo.getFirstFrame());
                    } else {
                        Uri uri = Uri.parse(caaVideo.getIntroUrl());
                        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                                .setResizeOptions(new ResizeOptions(tileWidthHeight, tileWidthHeight))
                                .build();

                        DraweeController controller = Fresco.newDraweeControllerBuilder()
                                .setUri(uri)
                                .setOldController(((DataObjectHolder) holder).clipImage.getController())
                                .setImageRequest(request)
                                .setAutoPlayAnimations(true)
                                .build();

                        ((DataObjectHolder) holder).clipImage.setController(controller);
                    }

//                if (showPrivate && (caaVideo.getStatus().equalsIgnoreCase(Constants.PRIVATE) || caaVideo.getStatus().equalsIgnoreCase(Constants.SECRET)))
//                    ((DataObjectHolder) holder).privateIcon.setVisibility(View.VISIBLE);
//                else
//                    ((DataObjectHolder) holder).privateIcon.setVisibility(View.GONE);
                }
                break;
            case AdaptersDataTypes.HEADER:
                break;

        }
    }


    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        View rippleView;
        SimpleDraweeView clipImage;
        ImageView privateIcon;

        public DataObjectHolder(View itemView) {
            super(itemView);
            rippleView = itemView.findViewById(R.id.ripple_view);
            rippleView.setOnClickListener(itemClickListener);

            clipImage = (SimpleDraweeView) itemView.findViewById(R.id.clip_image);
            clipImage.getLayoutParams().width = tileWidthHeight;
            clipImage.getLayoutParams().height = tileWidthHeight;

            itemView.getLayoutParams().width = tileWidthHeight;
            itemView.getLayoutParams().height = tileWidthHeight;

            privateIcon = (ImageView) itemView.findViewById(R.id.private_icon);
        }
    }

    public class HeaderHolder extends RecyclerView.ViewHolder {
        public HeaderHolder(View itemView) {
            super(itemView);
        }
    }

    public void close() {
        mDataSet.clear();
        mDataSet = null;
    }
}