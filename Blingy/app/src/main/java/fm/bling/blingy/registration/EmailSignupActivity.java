package fm.bling.blingy.registration;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import java.net.HttpURLConnection;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.registration.rest.model.CAAEmailError;
import fm.bling.blingy.registration.rest.model.CAAEmailSignup;
import fm.bling.blingy.registration.rest.model.CAAPostPassword;
import fm.bling.blingy.registration.rest.model.CAAPostSignup;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.utils.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Zach Gerstman on 6/3/15.
 */
public class EmailSignupActivity extends BaseActivity implements TextWatcher, View.OnClickListener, TextView.OnEditorActionListener, View.OnFocusChangeListener, CAALoginListener {

    private static String TAG = "EmailSignupActivity";
    private EditText editEmailText;
    private EditText editPasswordText;
    private EditText editFirstNameText;
    private EditText editLastNameText;
    private String email = "";
    private String password;
    private String firstName;
    private String lastName;
    private Boolean validEmail = false;
    private Boolean validPassword = false;
    private Boolean validFirstName = false;
    private Boolean validLastName = false;
    private Boolean emailExists = false;
    private Button nextButton;
    private Boolean emailSignUpPage = false;

    private ImageView firstNameValid;
    private ImageView lastNameValid;
    private ImageView passwordValid;
    private ImageView passwordInvalid;
    private TextView passwordInvalidText;
    private TextView nameValidText;
    private TextView emailInvalidText2;
    private TextView emailInvalidText1;
    private TextView loginText;
    private TextView resendConfirmationText;
    private ImageView emailInvalid;
    private ImageView emailValid;
    private ProgressBar emaillProgressBar;

    private ScrollView mRegistrationScroll;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout_edit);
        //Setting up the navigation icon.
        toolbar = getActionBarToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
        toolbar.setNavigationContentDescription("backClose");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        init();

        enterSignUp();
    }

    private void init(){
        mRegistrationScroll = (ScrollView) findViewById(R.id.registration_scroll);
        nextButton = (Button) findViewById(R.id.emailRegistration_button_next);
        editEmailText = (EditText) findViewById(R.id.emailRegistration_textField_email);
        editPasswordText = (EditText) findViewById(R.id.emailRegistration_textField_password);
        editFirstNameText = (EditText) findViewById(R.id.emailRegistration_textField_firstName);
        editLastNameText = (EditText) findViewById(R.id.emailRegistration_textField_lastName);
        firstNameValid = (ImageView) findViewById(R.id.image_view_first_name_valid);
        lastNameValid = (ImageView) findViewById(R.id.image_view_last_name_valid);
        passwordValid = (ImageView) findViewById(R.id.image_view_password_valid);
        passwordInvalid = (ImageView) findViewById(R.id.image_view_password_invalid);
        passwordInvalidText = (TextView) findViewById(R.id.text_login_info3);
        nameValidText = (TextView) findViewById(R.id.text_login_info4);
        emailInvalidText2 = (TextView) findViewById(R.id.text_login_info2);
        emailInvalidText1 = (TextView) findViewById(R.id.text_login_info);
        loginText = (TextView) findViewById(R.id.emailRegistration_button_login2);
        resendConfirmationText = (TextView) findViewById(R.id.emailRegistration_button_invalidEmailResend);
        emailInvalid = (ImageView) findViewById(R.id.image_view_email_invalid);
        emailValid = (ImageView) findViewById(R.id.image_view_email_valid);
        emaillProgressBar = (ProgressBar) findViewById(R.id.email_progressbar);
        emaillProgressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this,R.color.main_blue), android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    @Override
    public void onBackPressed() {
        if (emailSignUpPage) {
            setResult(RESULT_CANCELED);
            finish();
            emailSignUpPage = false;
        } else {
            super.onBackPressed();
        }
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == Constants.REQUEST_LOGIN) {
//            if (resultCode == RESULT_OK) {
//                setResult(RESULT_OK);
//                this.finish();
//            }
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }

//    private void emailLogin() {
//        Intent intent = new Intent(EmailSignupActivity.this, LoginActivity.class);
//        Bundle b = new Bundle();
//        if (email == null) {
//            email = "";
//        }
//        b.putString("email", email);
//        intent.putExtras(b);
//        startActivityForResult(intent, Constants.REQUEST_LOGIN);
//    }

    private void enterSignUp() {
        clear();
        emailSignUpPage = true;

        nextButton.setEnabled(false);
        nextButton.setAlpha((float) 0.5);
        nextButton.setOnClickListener(this);
        nextButton.setFocusable(false);

        editEmailText.setText("");
        editEmailText.setOnFocusChangeListener(this);

        editPasswordText.setText("");
        editPasswordText.setOnEditorActionListener(this);
        editPasswordText.setOnFocusChangeListener(this);
        editPasswordText.addTextChangedListener(this);

        editFirstNameText.setText("");
        editFirstNameText.setOnEditorActionListener(this);
        editFirstNameText.setOnFocusChangeListener(this);
        editFirstNameText.addTextChangedListener(this);

        editLastNameText.setText("");
        editLastNameText.setOnEditorActionListener(this);
        editLastNameText.setOnFocusChangeListener(this);
        editLastNameText.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s == editFirstNameText.getText()) {
            if (emailSignUpPage) {
                firstName = editFirstNameText.getText().toString().trim();
                if (firstName.isEmpty()) {
                    firstNameValid.setVisibility(View.INVISIBLE);
                    validFirstName = false;
                } else if (firstName.trim().length() < 2) {
                    firstNameValid.setImageResource(R.drawable.ic_register_field_error_24dp);
                    firstNameValid.setVisibility(View.VISIBLE);
                    nameValidText.setVisibility(View.VISIBLE);
                    emailInvalidText2.setVisibility(View.INVISIBLE);
                    emailInvalidText1.setVisibility(View.INVISIBLE);
                    passwordInvalidText.setVisibility(View.GONE);
                    validFirstName = false;
                } else {
                    firstNameValid.setImageResource(R.drawable.ic_register_field_ok_24dp);
                    firstNameValid.setVisibility(View.VISIBLE);
                    nameValidText.setVisibility(View.INVISIBLE);
                    validFirstName = true;
                }
            }
        } else if (s == editLastNameText.getText()) {
            if (emailSignUpPage) {
                lastName = editLastNameText.getText().toString().trim();
                if (lastName.isEmpty()) {
                    lastNameValid.setVisibility(View.INVISIBLE);
                    validLastName = false;
                } else if (lastName.length() < 2) {
                    lastNameValid.setImageResource(R.drawable.ic_register_field_error_24dp);
                    lastNameValid.setVisibility(View.VISIBLE);
                    nameValidText.setVisibility(View.VISIBLE);
                    emailInvalidText2.setVisibility(View.INVISIBLE);
                    emailInvalidText1.setVisibility(View.INVISIBLE);
                    passwordInvalidText.setVisibility(View.GONE);
                    validLastName = false;
                } else {
                    lastNameValid.setImageResource(R.drawable.ic_register_field_ok_24dp);
                    lastNameValid.setVisibility(View.VISIBLE);
                    nameValidText.setVisibility(View.INVISIBLE);
                    validLastName = true;
                }
            }
        } else if (s == editPasswordText.getText()) {
            if (emailSignUpPage) {
                password = editPasswordText.getText().toString();
                if ((password.length() > 7) && (password.matches(".*\\d+.*"))) {
                    //valid
                    passwordValid.setVisibility(View.VISIBLE);
                    passwordInvalid.setVisibility(View.GONE);
                    passwordInvalidText.setVisibility(View.GONE);
                    validPassword = true;
                } else if (password.isEmpty()) {
                    passwordValid.setVisibility(View.GONE);
                    passwordInvalid.setVisibility(View.GONE);
                    passwordInvalidText.setVisibility(View.GONE);
                    validPassword = false;
                } else {
                    //invalid
                    passwordValid.setVisibility(View.GONE);
                    passwordInvalid.setVisibility(View.VISIBLE);
                    emailInvalidText2.setVisibility(View.INVISIBLE);
                    emailInvalidText1.setVisibility(View.INVISIBLE);
                    nameValidText.setVisibility(View.INVISIBLE);
                    passwordInvalidText.setVisibility(View.VISIBLE);
                    validPassword = false;
                }
            }
        }
        canSignUp();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            switch (v.getId()) {
                case R.id.emailRegistration_textField_firstName:
                    if (emailSignUpPage) {
                        firstName = editFirstNameText.getText().toString().trim();
                        if (firstName.isEmpty()) {
                            firstNameValid.setVisibility(View.INVISIBLE);
                            validFirstName = false;
                        } else if (firstName.length() < 2) {
                            firstNameValid.setImageResource(R.drawable.ic_register_field_error_24dp);
                            firstNameValid.setVisibility(View.VISIBLE);
                            nameValidText.setVisibility(View.VISIBLE);
                            passwordInvalidText.setVisibility(View.GONE);
                            emailInvalidText1.setVisibility(View.INVISIBLE);
                            emailInvalidText2.setVisibility(View.INVISIBLE);
                            validFirstName = false;
                        } else {
                            firstNameValid.setImageResource(R.drawable.ic_register_field_ok_24dp);
                            firstNameValid.setVisibility(View.VISIBLE);
                            nameValidText.setVisibility(View.INVISIBLE);
                            validFirstName = true;
                        }
                    }
                    break;

                case R.id.emailRegistration_textField_lastName:
                    if (emailSignUpPage) {
                        lastName = editLastNameText.getText().toString().trim();
                        if (lastName.isEmpty()) {
                            lastNameValid.setVisibility(View.INVISIBLE);
                            validLastName = false;
                        } else if (lastName.length() < 2) {
                            lastNameValid.setImageResource(R.drawable.ic_register_field_error_24dp);
                            lastNameValid.setVisibility(View.VISIBLE);
                            nameValidText.setVisibility(View.VISIBLE);
                            passwordInvalidText.setVisibility(View.INVISIBLE);
                            emailInvalidText1.setVisibility(View.INVISIBLE);
                            emailInvalidText2.setVisibility(View.INVISIBLE);
                            validLastName = false;
                        } else {
                            lastNameValid.setImageResource(R.drawable.ic_register_field_ok_24dp);
                            lastNameValid.setVisibility(View.VISIBLE);
                            nameValidText.setVisibility(View.INVISIBLE);
                            validLastName = true;
                        }
                    }
                    break;

                case R.id.emailRegistration_textField_email:
                    if (emailSignUpPage) {
                        email = editEmailText.getText().toString().trim();
                        checkEmail(email);
                    }
                    break;

                case R.id.emailRegistration_textField_password:
                    if (emailSignUpPage) {
                        password = editPasswordText.getText().toString();
                        if ((password.length() > 7) && (password.matches(".*\\d+.*"))) {
                            //valid
                            passwordValid.setVisibility(View.VISIBLE);
                            passwordInvalid.setVisibility(View.INVISIBLE);
                            passwordInvalidText.setVisibility(View.INVISIBLE);
                            validPassword = true;
                        } else if (password.isEmpty()) {
                            passwordValid.setVisibility(View.INVISIBLE);
                            passwordInvalid.setVisibility(View.INVISIBLE);
                            passwordInvalidText.setVisibility(View.INVISIBLE);
                            validPassword = false;
                        } else {
                            //invalid
                            passwordValid.setVisibility(View.INVISIBLE);
                            passwordInvalid.setVisibility(View.VISIBLE);
                            emailInvalidText2.setVisibility(View.INVISIBLE);
                            emailInvalidText1.setVisibility(View.INVISIBLE);
                            passwordInvalidText.setVisibility(View.VISIBLE);
                            nameValidText.setVisibility(View.INVISIBLE);
                            validPassword = false;
                        }
                    }
                    break;
            }
            canSignUp();
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        switch (actionId) {
            case EditorInfo.IME_ACTION_DONE:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                v.clearFocus();
                editFirstNameText.requestFocus();
                return true;
            case EditorInfo.IME_ACTION_UNSPECIFIED:
                v.clearFocus();
                if (v.getId() == R.id.emailRegistration_textField_firstName) {
                    editLastNameText.requestFocus();
                } else {
                    editEmailText.requestFocus();
                }
                return true;
            default:
                return false;
        }
    }

    private void canSignUp() {
        if (emailExists) {
            loginText.setVisibility(View.VISIBLE);
            resendConfirmationText.setVisibility(View.VISIBLE);
            loginText.setOnClickListener(this);
            resendConfirmationText.setOnClickListener(this);
        } else {
            loginText.setVisibility(View.GONE);
            resendConfirmationText.setVisibility(View.GONE);
        }

        if (validFirstName && validLastName && validPassword && validEmail) {
            //Enable Next Button
            nextButton.setEnabled(true);
            nextButton.setAlpha((float) 1);
        } else {
            //Disable Next button
            nextButton.setEnabled(false);
            nextButton.setAlpha((float) 0.5);
        }
    }

    private void emailSignUp() {
        email = editEmailText.getText().toString();
        password = editPasswordText.getText().toString();
        firstName = editFirstNameText.getText().toString();
        lastName = editLastNameText.getText().toString();
        EmailSignup(email, password, firstName, lastName);
    }

    private void resendEmail() {
        String title = "Bligny";
        String text = "Confirmation has been sent to your email. Please check your email to confirm your registration";
        BaseDialog mDialog = new BaseDialog(this, title, text);
        mDialog.removeCancelbutton();
        mDialog.show();

        CAAPostPassword putSignup = new CAAPostPassword(email, Constants.APP_ID, Constants.APP_SECRET);
        App.getUrlService().putsSignup(putSignup, new Callback<String>() {
            @Override
            public void success(String email, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private boolean checkEmail(String email) {
        if (email.isEmpty()) {
            emailInvalid.setVisibility(View.GONE);
            emailValid.setVisibility(View.GONE);
            return false;
        }

        editEmailText = (EditText) findViewById(R.id.emailRegistration_textField_email);
        email = editEmailText.getText().toString();

        emailValid.setVisibility(View.GONE);
        emailInvalid.setVisibility(View.GONE);
        emaillProgressBar.setVisibility(View.VISIBLE);

        App.getUrlService().getEmailCheck(new CAAEmailSignup(email, Constants.APP_ID, Constants.APP_SECRET), new Callback<CAAEmailError>() {
            @Override
            public void success(CAAEmailError email, Response response) {
                validEmail = true;
                emailExists = false;
                TextView emailInvalidText2 = (TextView) findViewById(R.id.text_login_info2);
                emailInvalidText2.setVisibility(View.INVISIBLE);
                TextView emailInvalidText1 = (TextView) findViewById(R.id.text_login_info);
                emailInvalidText1.setVisibility(View.INVISIBLE);
                emaillProgressBar.setVisibility(View.GONE);
                emailInvalid.setVisibility(View.INVISIBLE);
                emailValid.setVisibility(View.VISIBLE);
                canSignUp();
            }

            @Override
            public void failure(RetrofitError error) {
                emaillProgressBar.setVisibility(View.GONE);
                validEmail = false;
                emailExists = false;
                try {
                    CAAEmailError body = (CAAEmailError) error.getBodyAs(CAAEmailError.class);
                    if (body != null) {
                        if (body.getEmail() != null && body.getEmail().getRecordFound() != null) {

                            emailInvalid.setVisibility(View.VISIBLE);
                            emailValid.setVisibility(View.GONE);
                            emailExists = true;
                            TextView emailInvalidText2 = (TextView) findViewById(R.id.text_login_info2);
                            emailInvalidText2.setVisibility(View.INVISIBLE);
                            TextView emailInvalidText1 = (TextView) findViewById(R.id.text_login_info);
                            emailInvalidText1.setVisibility(View.VISIBLE);
                            TextView passwordInvalidText = (TextView) findViewById(R.id.text_login_info3);
                            passwordInvalidText.setVisibility(View.INVISIBLE);
                            TextView nameInvalidText = (TextView) findViewById(R.id.text_login_info4);
                            nameInvalidText.setVisibility(View.INVISIBLE);

                        } else {
                            TextView emailInvalidText2 = (TextView) findViewById(R.id.text_login_info2);
                            emailInvalidText2.setVisibility(View.VISIBLE);
                            TextView emailInvalidText1 = (TextView) findViewById(R.id.text_login_info);
                            emailInvalidText1.setVisibility(View.INVISIBLE);
                            TextView passwordInvalidText = (TextView) findViewById(R.id.text_login_info3);
                            passwordInvalidText.setVisibility(View.INVISIBLE);
                            TextView nameInvalidText = (TextView) findViewById(R.id.text_login_info4);
                            nameInvalidText.setVisibility(View.INVISIBLE);

                            ImageView emailInvalid = (ImageView) findViewById(R.id.image_view_email_invalid);
                            emailInvalid.setVisibility(View.VISIBLE);
                            ImageView emailValid = (ImageView) findViewById(R.id.image_view_email_valid);
                            emailValid.setVisibility(View.INVISIBLE);
                        }
                    }
                }catch (Throwable throwable){
                    Log.d(TAG, throwable.toString());
                }
                canSignUp();
            }
        });
        return true;
    }

    public void onClick(View v) {
        if (Utils.canClick()) {
            switch (v.getId()) {
                case R.id.emailRegistration_button_next:
                    nextButton.setEnabled(false);
                    nextButton.setClickable(false);
                    closeKeyboard();
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (NetworkConnectivityListener.isConnectedWithDialog(EmailSignupActivity.this)) {
                                if (finalCheck()) {
                                    emailSignUpPage = false;
                                    emailSignUp();
                                } else {
                                    nextButton.setEnabled(true);
                                    nextButton.setClickable(true);
                                }

                            } else {
                                nextButton.setEnabled(false);
                                nextButton.setClickable(false);
                            }
                        }
                    }, 300);
                    break;
                case R.id.emailRegistration_button_invalidEmailResend:
//                case R.id.emailRegistration_button_resend:
                    if (NetworkConnectivityListener.isConnectedWithDialog(EmailSignupActivity.this))
                        resendEmail();
                    break;
                default:
                    break;
            }
        }
    }

    private void clear() {
        editEmailText.setText("");
        editPasswordText.setText("");
        editFirstNameText.setText("");
        editLastNameText.setText("");
        firstNameValid.setVisibility(View.INVISIBLE);
        lastNameValid.setVisibility(View.INVISIBLE);
        passwordValid.setVisibility(View.INVISIBLE);
        passwordInvalid.setVisibility(View.INVISIBLE);
        passwordInvalidText.setVisibility(View.INVISIBLE);
        emailInvalidText2.setVisibility(View.INVISIBLE);
        emailInvalidText1.setVisibility(View.INVISIBLE);
        emailInvalid.setVisibility(View.INVISIBLE);
        emailValid.setVisibility(View.INVISIBLE);
    }

    public boolean finalCheck() {
        boolean firstNameBool = true, lastNameBool = true, emailBool = true, passwordBool = true;
        if (emailSignUpPage) {
            firstName = editFirstNameText.getText().toString().trim();
            if (firstName.isEmpty()) {
                firstNameValid.setVisibility(View.INVISIBLE);
                validFirstName = false;
                firstNameBool = false;
            } else if (firstName.length() < 2) {
                firstNameValid.setImageResource(R.drawable.ic_register_field_error_24dp);
                firstNameValid.setVisibility(View.VISIBLE);
                validFirstName = false;
                firstNameBool = false;
            } else {
                firstNameValid.setImageResource(R.drawable.ic_register_field_ok_24dp);
                firstNameValid.setVisibility(View.VISIBLE);
                validFirstName = true;
                firstNameBool = true;
            }
        }

        if (emailSignUpPage) {
            lastName = editLastNameText.getText().toString().trim();
            if (lastName.isEmpty()) {
                lastNameValid.setVisibility(View.INVISIBLE);
                validLastName = false;
                lastNameBool = false;
            } else if (lastName.length() < 2) {
                lastNameValid.setImageResource(R.drawable.ic_register_field_error_24dp);
                lastNameValid.setVisibility(View.VISIBLE);
                validLastName = false;
                lastNameBool = false;
            } else {
                lastNameValid.setImageResource(R.drawable.ic_register_field_ok_24dp);
                lastNameValid.setVisibility(View.VISIBLE);
                validLastName = true;
                lastNameBool = true;
            }
        }

        if (emailSignUpPage) {
            email = editEmailText.getText().toString().trim();
            emailBool = checkEmail(email);
        }


        if (emailSignUpPage) {
            password = editPasswordText.getText().toString();
            if ((password.length() > 7) && (password.matches(".*\\d+.*"))) {
                //valid
                passwordValid.setVisibility(View.VISIBLE);
                passwordInvalid.setVisibility(View.INVISIBLE);
                passwordInvalidText.setVisibility(View.INVISIBLE);
                validPassword = true;
                passwordBool = true;
            } else if (password.isEmpty()) {
                passwordValid.setVisibility(View.INVISIBLE);
                passwordInvalid.setVisibility(View.INVISIBLE);
                passwordInvalidText.setVisibility(View.INVISIBLE);
                validPassword = false;
                passwordBool = false;
            } else {
                //invalid
                passwordValid.setVisibility(View.INVISIBLE);
                passwordInvalid.setVisibility(View.VISIBLE);
                emailInvalidText2.setVisibility(View.INVISIBLE);
                emailInvalidText1.setVisibility(View.INVISIBLE);
                passwordInvalidText.setVisibility(View.VISIBLE);
                validPassword = false;
                passwordBool = false;
            }

        }
        if (firstNameBool && lastNameBool && emailBool && passwordBool)
            return true;
        else
            return false;
    }

    private void EmailSignup(String email, String password, String firstName, String lastName) {

        CAAPostSignup postSignup = new CAAPostSignup(email, password, Constants.APP_ID, Constants.APP_SECRET, firstName, lastName);

        App.getUrlService().postSignup(postSignup, new Callback<User>() {
            @Override
            public void success(User cAAUser, Response response) {
                signInAndFinish();
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private void signInAndFinish() {
        CAAEmailLogin.EmailLogin(email, password, this);
    }

    @Override
    public void didFinishLogin(Boolean success, int status) {
        CAALogin.isAuto = "0";
        if (success) {
            setResult(RESULT_OK, null);
            CAAUserDataSingleton.getInstance().setSentGcmToken(false);
            finish();
            //user is logged in
        } else {
            if (status == HttpURLConnection.HTTP_FORBIDDEN) {
                String yesText = "RESEND EMAIL";
                String cancelText = getApplicationContext().getResources().getString(R.string.ok);
                String title = "Unconfirmed Account";
                String text = "Your account has been created but needs to be confirmed. Please click the confirmation link sent to your email.";
                BaseDialog mDialog = new BaseDialog(this,title,text);
                mDialog.setOKText(yesText);
                mDialog.setCancelText(cancelText);
                mDialog.show();
                mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (((BaseDialog)dialog).getAnswer()) {
                            resendEmail();
                        }
                    }
                });
            }
        }
    }
}
