package fm.bling.blingy.inviteFriends;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.Toast;

import fm.bling.blingy.App;
import fm.bling.blingy.R;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/14/16.
 * History:
 * ***********************************
 */
public class InviteTabContentBase extends TableLayout
{
    protected LinearLayout firstLine;
    protected Paint mPaint;
    private int strokLoc;
    private boolean isSeperateLineWidthPadding;
    final int padding;

    public InviteTabContentBase(Context context, Paint paint, boolean iIsSeperateLineWidthPadding)
    {
        super(context);
        padding = context.getResources().getDimensionPixelSize(R.dimen.main_margin);
        this.mPaint = paint;
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0.5f, context.getResources().getDisplayMetrics());
        this.mPaint.setStrokeWidth(px);
        this.isSeperateLineWidthPadding = iIsSeperateLineWidthPadding;
        int extraPadding = 0;
        if(isSeperateLineWidthPadding)
            extraPadding = (int) (10f * App.SCALE_X);
        strokLoc = (int) (140f * App.SCALE_X) - extraPadding;
        int tabHeight= (int) (140f * App.SCALE_X);

        firstLine = new LinearLayout(context)
        {
            public void draw(Canvas canvas)
            {
                super.draw(canvas);
                if(isSeperateLineWidthPadding)
                    canvas.drawLine(padding, strokLoc, App.WIDTH - padding, strokLoc, mPaint);
                else canvas.drawLine(0, strokLoc, App.WIDTH, strokLoc, mPaint);
            }
        };
        firstLine.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, tabHeight));
        firstLine.setBackgroundColor(context.getResources().getColor(R.color.transparent));
        firstLine.setPadding(extraPadding, extraPadding, extraPadding, extraPadding);

        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        setBackgroundColor(getResources().getColor(R.color.white));

        addView(firstLine);
    }

    public void close()
    {
        removeAllViews();destroyDrawingCache();
        try{firstLine.removeAllViews();firstLine.destroyDrawingCache();}catch(Throwable e){}firstLine = null;
        mPaint = null;
    }

    public void notifyDataSetChanged()
    {
    }

    protected void showNetworkErrorDialog()
    {
        Toast.makeText(getContext(), R.string.error_connecting_to_server, Toast.LENGTH_LONG).show();
    }

}