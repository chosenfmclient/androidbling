package fm.bling.blingy.profile.model;

import com.google.gson.annotations.SerializedName;

import java.net.URL;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAMethod
 * Description:
 * Created by Dawidowicz Nadav on 5/18/15.
 * History:
 * ***********************************
 */
public class CAAMethod {
    @SerializedName("method")
    private String method;

    @SerializedName("content_type")
    private String contentType;

    @SerializedName("signed_url")
    private URL signedUrl;

    public String getContentType() {
        return contentType;
    }

    public URL getSignedUrl() {
        return signedUrl;
    }

}
