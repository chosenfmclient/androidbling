package fm.bling.blingy.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaMetadataRetriever;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fm.bling.blingy.App;
import fm.bling.blingy.R;

/**
 * Created by Ben on 5/5/16.
 */
public class FFMPEG1
{
    private Context mContext;
    private volatile static FFMPEG1 ourInstance;
    private String execPath;
    private final int defaultBitrate = 15469;
    private InputStream errorStream;
    private static boolean initializing;

    public static FFMPEG1 getInstance(Context context)
    {
        while(initializing)
        {
            try{Thread.sleep(100);}catch (Throwable err){}
        }
        if(ourInstance == null)
        {
            initializing = true;
            ourInstance = new FFMPEG1(context.getApplicationContext());
            initializing = false;
        }
        return ourInstance;

    }

    private FFMPEG1(Context context)
    {
        mContext = context;
        initialize();
    }

    private void initialize()
    {
        File file = new File(App.MAIN_FOLDER_PATH, "ffmpeg");
        execPath = file.getPath();
        if(! file.exists())
        {
            try
            {
                createFile();
            }
            catch (Throwable err){err.printStackTrace();}
        }
    }

    private void createFile() throws Throwable
    {
        InputStream inputStream = mContext.getResources().openRawResource(R.raw.ffmpeg);
        OutputStream outputStream = mContext.openFileOutput("ffmpeg", 0);
        byte[] buffer = new byte[1024*1024];
        int count;
        while(true)
        {
            count = inputStream.read(buffer);
            if(count == -1)
                break;
            outputStream.write(buffer, 0, count);
        }
        inputStream.close();
        outputStream.flush();
        outputStream.close();
        File file = new File(App.MAIN_FOLDER_PATH, "ffmpeg");
        file.setExecutable(true, false);
    }

    /**
     * Run any compatible Command - See FFmpeg documentation for more info
     * @param command
     * @return
     * @throws Throwable
     */
    public int runCommand(String command) throws Throwable
    {
        if(command.indexOf("ffmpeg") != 0 )
            command = "ffmpeg " + command;
        return Runtime.getRuntime().exec(execPath + command).waitFor();
    }


    private synchronized int runCommandInternal(String command) throws Throwable
    {
        Process process = Runtime.getRuntime().exec(execPath + " " + command);
        errorStream = process.getErrorStream();
        return process.waitFor();
    }

    private synchronized int runCommandInternal(String[] command) throws Throwable
    {
        Process process = Runtime.getRuntime().exec(command);
        errorStream = process.getErrorStream();
        return process.waitFor();
    }

    /**
     * Render the video with an overlay
     *
     * @param videoInPath
     * @param imagePath
     * @param videoOutPath
     * @param x - starts from top left corner (0,0), can be complex as main_w and main_h (e.g: main_w - overlay_w)
     * @param y
     * @param bitRatInKiloBytes pass -1 for default, the lower the bitrate the lower the quality
     * @return the termination exit code
     * @throws Throwable
     */
    public int renderWithImage(String videoInPath, String imagePath, String videoOutPath, String x, String y, int bitRatInKiloBytes) throws Throwable
    {
        int bitrate = bitRatInKiloBytes == -1 ? defaultBitrate : bitRatInKiloBytes;
        String cmd = "-y -i " + videoInPath + " -i "+imagePath+" -strict experimental -filter_complex overlay="+x+":"+y+" -r 30 -b "+bitrate+"k -vcodec mpeg4 -ab 48000 -ac 2 -ar 22050 " + videoOutPath;
        return runCommandInternal(cmd);
    }

    /**
     * Render the video with an overlay
     *
     * @param videoInPath
     * @param imageBitmap
     * @param videoOutPath
     * @param x - starts from top left corner (0,0), can be complex as main_w and main_h (e.g: main_w - overlay_w)
     * @param y
     * @param bitRatInKiloBytes pass -1 for default, the lower the bitrate the lower the quality
     * @return the termination exit code
     * @throws Throwable
     */
    public int renderWithImageBitmap(String videoInPath, Bitmap imageBitmap, String videoOutPath, String x, String y, int bitRatInKiloBytes) throws Throwable
    {
        String imagePath = App.MAIN_FOLDER_PATH + File.separator + "temp_image_bitmap.jpg";
        File imageFile = new File(imagePath);
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(imageFile);
            imageBitmap.compress(Bitmap.CompressFormat.PNG,100,fOut);
            fOut.flush();
            fOut.close();
            imageFile.deleteOnExit();
            imageBitmap.recycle();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return renderWithImage( videoInPath, imagePath,  videoOutPath,  x,  y,  bitRatInKiloBytes);
    }

//    public int renderWithImageWithTranspose(String videoInPath, String imagePath, String videoOutPath, String x, String y, int bitRatInKiloBytes) throws Throwable
//    {
//        int bitrate = bitRatInKiloBytes == -1 ? defaultBitrate : bitRatInKiloBytes;
//        String cmd = "-y -i " + videoInPath +" -strict experimental -filter_complex movie="+imagePath+"[watermark];[watermark]transpose=1[transpose];[in][transpose]overlay="+x+":"+y+"[out]; -r 30 -b "+bitrate+"k -vcodec mpeg4 -ab 48000 -ac 2 -ar 22050 " + videoOutPath;
//        String cmd = "-y -i " + videoInPath + " -i "+imagePath+" -strict experimental -filter_complex [0:v][1:v]overlay[out] -map [out] " + videoOutPath;
//        return runCommandInternal(cmd);
//    }

    /**
     * Get the info of the file - metadata
     * this prints out the metadata
     * need to check what is better this method or MediaMetadataRetriever
     *
     * @param videoPath
     * @return the termination exit code
     * @throws Throwable
     */
    public int getVideoInfo(String videoPath) throws Throwable
    {
        String cmd = "-i " + videoPath;
        return runCommandInternal(cmd);
    }

    /**
     * Resize the video to the specified size
     * @param videoInPath
     * @param videoOutPath
     * @param width
     * @param height
     * @return the termination exit code
     * @throws Throwable
     */
    public int renderVideoToSize(String videoInPath, String videoOutPath, int width, int height) throws Throwable
    {
        String cmd = "-i " + videoInPath + " -s " + width + "x" + height + " -acodec mp3 -vcodec libx264 " + videoOutPath;
        return runCommandInternal(cmd);
    }

    /**
     *
     * @param videoInPath
     * @param videoOutPath
     * @param width
     * @param height
     * @param x - the x location, starting from top left (0,0) default is center
     * @param y - the y location, starting from top left (0,0) default is center
     * @return the termination exit code
     * @throws Throwable
     */
    public int cropVideo(String videoInPath, String videoOutPath, int width, int height, int x, int y) throws Throwable
    {
        String crop = "[in]crop="+width+":"+height+"[out]";
        String[] cmd = {execPath, "-y", "-i", videoInPath, "-vf", crop, "-r", "30", "-b", defaultBitrate + "k",
                "-vcodec", "mpeg4", "-ab", "48000", "-ac", "2", "-ar", "22050", videoOutPath};
        return runCommandInternal(cmd);
    }

    /**
     *
     * @param videoInPath
     * @param videoOutPath
     * @param widthHeight
     * @return the termination exit code
     * @throws Throwable
     */
    public int cropVideoCenterSquare(String videoInPath, String videoOutPath, int widthHeight) throws Throwable
    {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(videoInPath);
        int videoWidth = Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
        int videoHeight= Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
        int x, y;

        x = (videoWidth - widthHeight) / 2;
        y = (videoHeight - widthHeight) / 2;
        return cropVideo(videoInPath, videoOutPath, widthHeight, widthHeight, x, y);
    }

//    /**
//     * Concat the "videoInPaths" and deletes them, the full video will be in "videoOutPath"
//     * @param videoInPaths paths of videos to concat and delete
//     * @param videoOutPath path to the output file
//     * @param workingDirectory path to a working directory
//     * @return the termination exit code
//     * @throws Throwable
//     */
//    public int concatVideosTogether(String[] videoInPaths, String videoOutPath, String workingDirectory) throws Throwable
//    {
//        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
//        mediaMetadataRetriever.setDataSource(videoInPaths[0]);
//        int rotation = Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));
//        mediaMetadataRetriever.release();
//        String[] containers = new String[videoInPaths.length];
//        String concat="concat:";
//        String[] cmd;
//        for(int i=0;i<containers.length;i++)
//        {
//            containers[i]=workingDirectory+i+".ts";
//            concat+=containers[i];
//            if(i+1<containers.length) concat+="|";
//            cmd = new String[]{execPath, "-i", videoInPaths[i], "-c", "copy", "-f", "mpegts", containers[i]};
//            runCommandInternal(cmd);
//            new File(videoInPaths[i]).delete();
//        }
////        cmd = new String[]{execPath, "-y", "-i", concat, "-acodec", "copy", "-vcodec", "copy", "-bsf:a", "aac_adtstoasc", "-metadata:s:v:0", "rotate="+rotation, "-preset", "ultrafast", videoOutPath};
//        cmd = new String[]{execPath, "-y", "-i", concat, "-acodec", "copy", "-vcodec", "copy", "-bsf:a", "aac_adtstoasc", "-metadata:s:v:0", "rotate="+rotation, "-preset", "ultrafast", videoOutPath};//, "-r", "30"
//        int ret = runCommandInternal(cmd);
//        for(int i=0;i<containers.length;i++)
//            new File(containers[i]).delete();
//        return ret;
//    }

    /**
     * Concat the "videoInPaths" and deletes them, the full video will be in "videoOutPath"
     * @param videoInPaths paths of videos to concat and delete
     * @param videoOutPath path to the output file
     * @param workingDirectory path to a working directory
     * @param preSetRotation the pre set rotation in degrees or -1 to use the first video rotation as the rotation
     * @return the termination exit code
     * @throws Throwable
     */
    public int concatVideosTogether(String[] videoInPaths, String videoOutPath, String workingDirectory, int preSetRotation) throws Throwable
    {
        int rotation;

        if(preSetRotation == -1)
        {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(videoInPaths[0]);
            rotation = Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));
            mediaMetadataRetriever.release();
        }
        else rotation = preSetRotation;

        String[] containers = new String[videoInPaths.length];
        String concat="concat:";
        String[] cmd;

        for(int i=0;i<containers.length;i++)
        {
            containers[i]=workingDirectory+i+".ts";
            concat+=containers[i];
            if(i+1<containers.length) concat+="|";
            cmd = new String[]{execPath, "-i", videoInPaths[i], "-c", "copy", "-f", "mpegts", containers[i]};
            int ret = runCommandInternal(cmd);

            new File(videoInPaths[i]).delete();
        }

//        String outAllTs = workingDirectory + "all.ts";
//        cmd = new String[]{execPath, "-y", "-i", concat, "-c", "copy", outAllTs};
//        runCommandInternal(cmd);

        cmd = new String[]{execPath, "-y", "-i", concat, "-acodec", "copy", "-vcodec", "copy", "-bsf:a", "aac_adtstoasc", "-metadata:s:v:0", "rotate="+rotation, videoOutPath};
        int ret = runCommandInternal(cmd);

//        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
//        mediaMetadataRetriever.setDataSource(videoOutPath);
//        rotation = Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));

        for(int i=0;i<containers.length;i++)
            new File(containers[i]).delete();

        return ret;
    }

    /**
     *  Join video and audio together, considering that the audio and the video are already synced !!!
     *  Short 16bit Big endian
     * @param videoFilePath
     * @param audioFilePath
     * @param outputPath
     * @return the termination exit code
     * @throws Throwable
     */
    public int coupleVideoAndAudioS16BE(String videoFilePath, String audioFilePath, String outputPath, int fps) throws Throwable
    {
        String[]cmd = {execPath, "-i", videoFilePath, "-f", "s16be", "-ar", "44100", "-ac", "1", "-i", audioFilePath, "-vcodec", "copy", "-acodec", "aac", "-r", ""+fps, outputPath};// "-bsf:a", "aac_adtstoasc",
        return runCommandInternal(cmd);
    }

    public int coupleVideoAndAudioS16LE(String videoFilePath, String audioFilePath, String outputPath, int fps) throws Throwable
    {
        String[]cmd = {execPath, "-i", videoFilePath, "-f", "s16le", "-ar", "44100", "-ac", "1", "-i", audioFilePath, "-vcodec", "copy", "-acodec", "aac", "-r", ""+fps, outputPath};// "-bsf:a", "aac_adtstoasc",
        return runCommandInternal(cmd);
    }

    /**
     *  Join video and audio together, considering that the audio and the video are already synced !!!
     *  mp3
     * @param videoFilePath
     * @param audioFilePath
     * @param outputPath
     * @return the termination exit code
     * @throws Throwable
     */

//    public int coupleVideoAndAudioMp3(String videoFilePath, String audioFilePath, String outputPath, int fps) throws Throwable
//    {
//        if(new File(outputPath).exists())
//            new File(outputPath).delete();
////        String[]cmd = {execPath, "-i", videoFilePath, "-i", audioFilePath, "-codec", "copy", "-r", ""+fps, "-shortest", "-preset", "ultrafast", outputPath};// "-bsf:a", "aac_adtstoasc",
//        String[]cmd = {execPath, "-i", videoFilePath, "-i", audioFilePath, "-vcodec", "copy", "-acodec", "aac", "-shortest", outputPath};// "-bsf:a", "aac_adtstoasc",
//        return runCommandInternal(cmd);
//    }

    public int coupleVideoAndAudioMp3(String videoFilePath, String audioFilePath, String outputPath, int fps) throws Throwable
    {
        if(new File(outputPath).exists())
            new File(outputPath).delete();
//        String[]cmd = {execPath, "-i", videoFilePath, "-i", audioFilePath, "-codec", "copy", "-r", ""+fps, "-shortest", "-preset", "ultrafast", outputPath};// "-bsf:a", "aac_adtstoasc",
        String[]cmd = {execPath, "-i", videoFilePath, "-f", "s16le", "-ar", ""+sampleRate, "-ac", ""+channels, "-i", audioFilePath, "-vcodec", "copy", "-bsf:a", "aac_adtstoasc", "-acodec", "aac", "-r", ""+fps, "-shortest", outputPath};// "-bsf:a", "aac_adtstoasc",
        return runCommandInternal(cmd);
    }

    public int coupleVideoAndAudioWav(String videoFilePath, String audioFilePath, String outputPath, int fps) throws Throwable
    {
        if(new File(outputPath).exists())
            new File(outputPath).delete();
        String[]cmd = {execPath, "-i", videoFilePath, "-i", audioFilePath, "-acodec", "aac", "-shortest", outputPath};// "-bsf:a", "aac_adtstoasc",
        return runCommandInternal(cmd);
    }


    public int splitAudioTrack(String audioFileInPath, String audioFileOutPath, int time) throws Throwable
    {
        if(new File(audioFileOutPath).exists())
            new File(audioFileOutPath).delete();
        String[]cmd = {execPath, "-ss", "0", "-t", ""+time, "-i", audioFileInPath, "-codec", "copy", "-shortest", audioFileOutPath};// "-bsf:a", "aac_adtstoasc",
        return runCommandInternal(cmd);
    }

    public int convertMp3ToPCM(String mp3FilePath, String pcmOutputPath) throws Throwable
    {
        String[]cmd = {execPath, "-i", mp3FilePath, "-acodec", "pcm_s16le", "-ac", "" + 1, pcmOutputPath};
        return runCommandInternal(cmd);
    }

    /**
     *  Extract the first frame from the video and writes it to the output path
     * @param videoInPath
     * @param imageOutPath
     * @return the termination exit code
     * @throws Throwable
     */
    public int extractFrameFromVideo(String videoInPath, String imageOutPath) throws Throwable
    {
        String[]cmd = {execPath, "-i", videoInPath, "-frames:v", "1", imageOutPath};
        return runCommandInternal(cmd);
    }

//    /**
//     * Adds each image to a frame on the video as overlay
//     * @param videoInFileName the main video to attach the images
//     * @param imagesInFileName the images prefix name (e.g. "overlay.png" and the actual file names are overlay%d.png, where %d is a number with no leading zeros, in order)
//     * @param videoOutFileName the video out path
//     * @return the termination exit code
//     * @throws Throwable
//     */
//    public int renderWithImages(String videoInFileName, String imagesInFileName, String videoOutFileName)throws Throwable
//    {
//        String extension = imagesInFileName.substring(imagesInFileName.length()-4);
//        imagesInFileName = imagesInFileName.substring(0, imagesInFileName.length()-4);
//        String[] cmd = new String[]{execPath, "-y", "-r", CameraRecorder.sFps +"", "-i", videoInFileName, "-i", imagesInFileName + "%d" + extension, "-strict", "experimental", "-filter_complex", "overlay", "-preset", "ultrafast", "-vcodec", "libx264", "-ab", "48000", "-ac", "2", "-ar", "22050", "-b", defaultBitrate + "k", videoOutFileName};
//        return runCommandInternal(cmd);
//    }

    public int renderImagesToVideo(String framtOuputPath, String frame_extension, String outputFile, int fps) throws Throwable
    {
        String[] cmd = {execPath, "-f", "image2", "-r", fps +"", "-i", framtOuputPath +"%02d" + frame_extension, "-vcodec", "libx264", "-preset", "ultrafast", "-r", fps +"", outputFile};//, "-r", fps +"", "-b", bitrate +"", "-pix_fmt", "yuv420p"  - , "-pix_fmt", "yuv420p", "-bsf:a", "aac_adtstoasc"
        return runCommandInternal(cmd);
    }

//    public int renderImagesToVideo(String framtOuputPath, String frame_extension, String outputFile, int fps) throws Throwable
//    {
//        String[] cmd = {execPath, "-f", "image2", "-r", fps +"", "-i", framtOuputPath +"%02d" + frame_extension, "-vcodec", "libx264", "-preset", "ultrafast", "-r", fps +"", "-pix_fmt", "yuv420p", "-bsf:a", "aac_adtstoasc", outputFile};//, "-r", fps +"", "-b", bitrate +"", "-pix_fmt", "yuv420p"
//        return runCommandInternal(cmd);
//    }

//    public int renderImagesToVideo(String framtOuputPath, String frame_extension, String outputFile, int fps) throws Throwable
//    {
//        String[] cmd = {execPath, "-f", "image2", "-r", fps +"", "-i", framtOuputPath +"%02d" + frame_extension, "-vcodec", "libx264", "-preset", "ultrafast", "-r", fps +"", "-pix_fmt", "yuv420p", "-bsf:a", "aac_adtstoasc", outputFile};//, "-r", fps +"", "-b", bitrate +"", "-pix_fmt", "yuv420p"
//        return runCommandInternal(cmd);
//    }

//    public int renderImagesToVideo(String framtOuputPath, String frame_extension, String outputFile, int fps) throws Throwable
//    {
//        String[] cmd = {execPath, "-f", "image2", "-framerate", fps +"", "-i", framtOuputPath +"%02d" + frame_extension, "-vcodec", "libx264", "-preset", "ultrafast", "-r", fps +"", "-pix_fmt", "yuv420p", outputFile};
//        return runCommandInternal(cmd);
//    }

    /**
     * Get the error msg as String, NOTE: it doesn't have to be an error msg !!!
     * @return the error message
     * @throws Throwable
     */
    public String getError() throws Throwable
    {
        if(errorStream.available()>0)
        {
            return convertInputStreamToString(errorStream);
        }
        return null;
    }

    private String convertInputStreamToString(InputStream inputStream) {
        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            String str;
            StringBuilder sb = new StringBuilder();
            while ((str = r.readLine()) != null) {
                sb.append(str);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param videoInFileName  original file path.
     * @param startTime        time in seconds.
     * @param duration         time in seconds.
     * @param videoOutFileName video file output path
     * @return the termination exit code
     * @throws Throwable
     */
    public int getVideoFromTime(String videoInFileName, String startTime , String duration , String videoOutFileName) throws Throwable{
        String cmd = "-i " + videoInFileName + " -ss " + startTime + " -t " + duration + " -c copy " + videoOutFileName;
        return runCommandInternal(cmd);
    }

    public int bufferSize;
    public int bitrate = 128000;
    public int sampleRate = 44100;
    public int channels = 2;
    public AudioTrack getAudioTrack(InputStream wavStream) throws Throwable
    {
        int HEADER_SIZE = 44;

        ByteBuffer buffer = ByteBuffer.allocate(HEADER_SIZE);
        buffer.order(ByteOrder.LITTLE_ENDIAN);

        wavStream.read(buffer.array(), buffer.arrayOffset(), buffer.capacity());

        buffer.rewind();
        buffer.position(buffer.position() + 20);
        int format = buffer.getShort();
        channels = buffer.getShort();
        sampleRate = buffer.getInt();
        buffer.position(buffer.position() + 6);
        int bits = buffer.getShort();

        bitrate = sampleRate * channels * bits;

        int dataSize = 0;
        while (buffer.getInt() != 0x61746164)
        {
            int size = buffer.getInt();
            wavStream.skip(size);

            buffer.rewind();
            wavStream.read(buffer.array(), buffer.arrayOffset(), 8);
            buffer.rewind();
        }
        dataSize = buffer.getInt();

        int audioFormat = bits == 16 ? AudioFormat.ENCODING_PCM_16BIT : AudioFormat.ENCODING_PCM_8BIT;
        int channelConfig = channels == 2 ? AudioFormat.CHANNEL_OUT_STEREO : AudioFormat.CHANNEL_OUT_MONO;
        bufferSize = AudioTrack.getMinBufferSize(sampleRate, channelConfig, audioFormat);

        if(bufferSize < 0)
            bufferSize = AudioTrack.getMinBufferSize(44100, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);

        return new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, channelConfig, audioFormat, bufferSize * 2, AudioTrack.MODE_STREAM);
    }

}