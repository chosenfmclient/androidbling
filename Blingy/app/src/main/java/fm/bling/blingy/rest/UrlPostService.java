package fm.bling.blingy.rest;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.EncodedPath;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.mime.TypedFile;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: UrlPutService
 * Description:
 * Created by Dawidowicz Nadav on 5/11/15.
 * History:
 * ***********************************
 */
public interface UrlPostService
{
    @PUT("/{id}")
    public void putFileServer(@Header("Content-Type") String head, @EncodedPath("id") String value, @Body TypedFile photo, Callback<String> call);


    @PUT("/{id}")
    public String uploadFile(@Header("Content-Type") String head, @EncodedPath("id") String value, @Body TypedFile resource);


    @GET("{id}")
    public void getFileServer(@Path("id") String value, Callback<String> call);

    @POST("{id}")
    public void postFileServer(@Path("id") String value, Callback<String> call);



}
