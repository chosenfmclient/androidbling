package fm.bling.blingy.stateMachine;

/**
 * Created by Chosen-pro on 03/07/2016.
 */

public class StateMachineEvent {
    /**
     * Classes of events - NEW CLASSES MUST ALWAYS BE ADDED AT THE END...
     */
    public enum EVENT_CLASS {
        RECORDING, GAMES, SHARING, SOCIAL, STATE_MESSAGE
    }

    /**
     * Sub classes of events - NEW CLASSES MUST ALWAYS BE ADDED AT THE END...
     */
    public enum EVENT_TYPE {
        //RECORDING
        RECORD_VIDEO, RECORD_PHOTO, UPLOAD,

        //GAMES
        PLAY_GAME, CHECK_LEADERBOARD,

        //SOCIAL
        FOLLOW_USER, INVITE_FRIEND,

        //SHARING
        SHARE_FACEBOOK, SHARE_TWITTER, SHARE_SMS, SHARE_EMAIL, SHARE_OTHER,
    }

    /**
     * Messages - NEW MESSAGES MUST ALWAYS BE ADDED AT THE END...
     */
    public enum MESSAGE_TYPE {
        INVITE_THIS_FRIEND_SMS,
        INVITE_THIS_FRIEND_EMAIL,
        YOU_SHOULD_SIGN_UP,
        TAKE_A_PHOTO,
        TRY_UPLOAD,
        SUGGEST_FOLLOW,
        PLAY_NEW_GAME
    }

    private EVENT_CLASS eventClass;
    private int objectId = 0;
    private int eventDate = 0;
    private int eventOrdinal;

    public StateMachineEvent(EVENT_CLASS eventClass, int eventOrdinal, int objectId) {
        this.eventClass = eventClass;
        this.eventOrdinal = eventOrdinal;
        this.objectId = objectId;
        this.eventDate = StateMachine.timeInSeconds();
    }

    public StateMachineEvent(EVENT_CLASS eventClass, int eventOrdinal) {
        this.eventClass = eventClass;
        this.eventOrdinal = eventOrdinal;
        this.eventDate = StateMachine.timeInSeconds();
    }

    public EVENT_CLASS getEventClass() {
        return eventClass;
    }

    public int getEventDate() {
        return eventDate;
    }

    public int getObjectId() {
        return objectId;
    }

    public int getEventOrdinal() {
        return eventOrdinal;
    }

}
