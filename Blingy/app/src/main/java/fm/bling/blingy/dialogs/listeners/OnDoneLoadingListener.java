package fm.bling.blingy.dialogs.listeners;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 22/01/17.
 * History:
 * ***********************************
 */
public interface OnDoneLoadingListener {

    void onDoneLoading();
}
