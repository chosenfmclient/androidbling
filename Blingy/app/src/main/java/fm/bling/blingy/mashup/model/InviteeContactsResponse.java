package fm.bling.blingy.mashup.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.rest.model.UserInvited;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 1/9/17.
 * History:
 * ***********************************
 */
public class InviteeContactsResponse {

    @SerializedName("followings")
    private ArrayList<UserInvited> followingUsers;

    @SerializedName("emails_to_remove")
    private ArrayList<String> emailsToRemove;

    @SerializedName("emails")
    private ArrayList<EmailInvited> emailsInvited;

    @SerializedName("phones")
    private ArrayList<PhoneInvited> phonesInvited;

    public ArrayList<UserInvited> getFollowings() {
        return followingUsers;
    }

    public ArrayList<String> getEmailsToRemove() {
        return emailsToRemove;
    }

    public ArrayList<EmailInvited> getEmailsInvited() {
        return emailsInvited;
    }

    public ArrayList<PhoneInvited> getPhonesInvited() {
        return phonesInvited;
    }

    public class EmailInvited {

        @SerializedName("email")
        private String email;

        @SerializedName("has_invited")
        private boolean hasInvited;

        public String getEmail() {
            return email;
        }

        public boolean getHasInvited() {
            return hasInvited;
        }
    }

    public class PhoneInvited {

        @SerializedName("phone")
        private String phone;

        @SerializedName("has_invited")
        private boolean hasInvited;

        public String getPhone() {
            return phone;
        }

        public boolean getHasInvited() {
            return hasInvited;
        }
    }

}

