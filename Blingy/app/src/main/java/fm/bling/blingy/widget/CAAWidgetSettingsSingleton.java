package fm.bling.blingy.widget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fm.bling.blingy.widget.model.CAAInvitePrize;
import fm.bling.blingy.widget.model.CAAInviteWidgetSetting;
import fm.bling.blingy.widget.model.CAAInviteWidgetSettingsResponse;

/**
 * Created by Chosen-pro on 17/07/2016.
 */

public class CAAWidgetSettingsSingleton {
    private static CAAWidgetSettingsSingleton ourInstance;
    private HashMap<String, CAAInviteWidgetSetting> settings;
    private ArrayList<CAAInvitePrize> prizes;
    private int inviteWorth = 10;
    private int myPoints    = 0;
    private boolean widgetOn = true;
    private int inviteExclusionTime = 7 * 24 * 60 * 60;
//    private List<PointsObserver> observers = new ArrayList<>();
    private int oldPoints;
    private int countNotified;
    private String emailText = "";

    private CAAWidgetSettingsSingleton() {
    }

    public static CAAWidgetSettingsSingleton getInstance()
    {
        if (ourInstance == null) {
            ourInstance = new CAAWidgetSettingsSingleton();
            ourInstance.settings = new HashMap<>();
        }
        return ourInstance;
    }

    public void setSettings(CAAInviteWidgetSettingsResponse response) {
        this.settings.clear();
        for (CAAInviteWidgetSetting setting: response.getSettings()) {
            this.settings.put(setting.getType(), setting);
        }
        prizes              = response.getPrizes();
        inviteWorth         = response.getInviteWorth();
        myPoints            = response.getMyPoints();
        inviteExclusionTime = response.getExclusionTime();
        emailText = response.getEmailText();
        oldPoints = myPoints;
    }

    public HashMap<String, CAAInviteWidgetSetting> getSettings() {
        return settings;
    }

    public CAAInviteWidgetSetting getSettingByType(String type) {
        if (settings == null || settings.isEmpty() || !settings.containsKey(type)) {
            return null;
        }
        return settings.get(type);
    }

    public int getInviteWorth() {
        return inviteWorth;
    }
    public int getOldPoints() {
        return oldPoints;
    }

    public int getMyPoints() {
        return myPoints;
    }

    public void setMyPoints(int points) {
        myPoints = points;
//        if(myPoints != 0)
//            notifyAllObservers();
    }

    public ArrayList<CAAInvitePrize> getPrizes() {
        return prizes;
    }

    public void setWidgetOff()
    {
        widgetOn = false;
    }

    public boolean isWidgetOn()
    {
        return widgetOn;
    }

    public int getInviteExclusionTime() {
        return inviteExclusionTime;
    }

    public int getPointsAfterInvite(){
        return  myPoints + inviteWorth;
    }

//    public void attachPointsObserver(PointsObserver observer){
//        observers.add(observer);
//        countNotified++;
//    }
//
//    public void removeObserver(PointsObserver pointsObserver) {
//        try {
//            observers.remove(pointsObserver);
//            countNotified--;
//        }catch (Throwable t){
//            t.printStackTrace();
//        }
//    }

//    public void notifyAllObservers(){
//        for (PointsObserver observer : observers) {
//            if(observer != null)
//                observer.updatePoints();
//        }
//    }

    public void addPoints() {
        int sum = CAAWidgetSettingsSingleton.getInstance().getPointsAfterInvite();
        CAAWidgetSettingsSingleton.getInstance().setMyPoints(sum);
    }

    public void updateOldPoints() {
        this.oldPoints = myPoints;
    }

//    public int getCountNotified() {
//        return countNotified;
//    }

//    public void countDownNotified() {
//        if(observers != null && observers.size() > 0)
//            --(this.countNotified);
//    }

    public String getEmailText(){
        return emailText;
    }
}
