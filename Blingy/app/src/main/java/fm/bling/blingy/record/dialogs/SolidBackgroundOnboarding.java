package fm.bling.blingy.record.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
//import android.os.Handler;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
//import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
//import android.widget.LinearLayout;
//import android.widget.TextView;

import fm.bling.blingy.App;
import fm.bling.blingy.ProcessImage;
import fm.bling.blingy.R;
//import fm.bling.blingy.record.CameraRecorder3;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.record.utils.BackgroundDetected;
import fm.bling.blingy.utils.imagesManagement.views.WhiteProgressBarView;
import fm.bling.blingy.utils.views.TextViewRegular;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 9/12/16.
 * History:
 * ***********************************
 */
public class SolidBackgroundOnboarding extends Dialog implements BackgroundDetected {

    private LinearLayout mContainer;
    private FrameLayout mDetectContainer;
    private ImageView mDetectedBackground;
    private ImageView mDetectedIcon;

    private LinearLayout overLay;
//    private FrameLayout mainLay;

    private View mParentView;
    private Context mContext;

    public SolidBackgroundOnboarding(Context context, View parentView, boolean isFront) {
        super(context, R.style.FullScreen);
        this.mContext = context;
        this.mParentView = parentView;
        setCancelable(false);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        hideNavigation();
        setContentView(R.layout.green_screen_tutorial);
        init(isFront);
        show();
    }

    private void init(boolean isFront) {
        mContainer = (LinearLayout) findViewById(R.id.container);
        mDetectContainer = (FrameLayout) findViewById(R.id.get_in_solid_container);
        mDetectedBackground = (ImageView) findViewById(R.id.detect_tutorial);
        mDetectedIcon = (ImageView) findViewById(R.id.detected_circle);

        mDetectedBackground.getLayoutParams().width = App.WIDTH;
        mDetectedBackground.getLayoutParams().height = App.landscapitemHeight;
        if (!isFront)
            mDetectedBackground.setImageResource(R.drawable.silhouette_shadow_back);

        ((LinearLayout.LayoutParams) mDetectContainer.getLayoutParams()).height = App.landscapitemHeight;
        mDetectContainer.postInvalidate();
    }

    @Override
    public void onBackgroundDetected(int detectionResponseCode) {
        if (detectionResponseCode == ProcessImage.DetectionResponseSuccess)
            backgroundDetected();
        else
            wrongBackground(detectionResponseCode);
    }

    public void showSilhouette() {
        mDetectedBackground.setVisibility(View.VISIBLE);
        mDetectedIcon.setVisibility(View.GONE);
    }

    public void showSilhouetteSole() {
        mDetectedBackground.setVisibility(View.VISIBLE);
    }

    public void updateSilhouette(boolean isFront) {
        if (isFront)
            mDetectedBackground.setImageResource(R.drawable.silhouette_shadow_front);
        else
            mDetectedBackground.setImageResource(R.drawable.silhouette_shadow_back);
    }

    public void backgroundDetected() {
        mDetectedBackground.setVisibility(View.GONE);
        mDetectedIcon.setImageResource(R.drawable.extra_large_ic_ok);
        mDetectedIcon.setVisibility(View.VISIBLE);
        dismiss();
    }

    public void wrongBackground(int detectionResponseCode) {
        mDetectedBackground.setVisibility(View.GONE);
        showFailAnimation(detectionResponseCode);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mParentView.dispatchTouchEvent(event);
    }

    @Override
    public void show() {
        super.show();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        lp.dimAmount = 0.0f;
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);

        Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        mContainer.startAnimation(anim);
        anim.startNow();
    }

    public void dismiss() {
        if (mContext != null) {
            Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
            anim.setDuration(600);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    superDismiss();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            mContainer.startAnimation(anim);
        } else {
            superDismiss();
        }
    }

    public void superDismiss() {
        super.dismiss();
    }

    public void hideNavigation() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }


    private void showFailAnimation(int detectionResponseCode) {
//        mainLay = new FrameLayout(mContext);
//        mainLay.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
//        mainLay.setVisibility(View.INVISIBLE);
        overLay = new LinearLayout(mContext);

        overLay.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        overLay.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
        overLay.setOrientation(LinearLayout.VERTICAL);
        overLay.setVisibility(View.INVISIBLE);

        int bottomtopMaring = App.MAIN_MARGIN * 2;

        TextViewRegular tv = new TextViewRegular(mContext);
        LinearLayout.LayoutParams tvParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tvParams.bottomMargin = App.MAIN_MARGIN;
        tv.setLayoutParams(tvParams);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
        tv.setPadding(bottomtopMaring, App.MAIN_MARGIN, bottomtopMaring, App.MAIN_MARGIN);

        tv.setTextColor(Color.WHITE);
        tv.setBackgroundResource(R.drawable.button_shape_black_50_color);
        tv.setGravity(Gravity.CENTER);

        overLay.addView(tv);
        switch (detectionResponseCode) {
            case ProcessImage.DetectionResponseNotEnoughLighting:
            case ProcessImage.DetectionResponseBadLighting:
                tv.setText("Protip: Try brighter, natural lighting!");
                break;

            case ProcessImage.DetectionResponseTooMuchLighting:
                tv.setText("Protip: Try lower lighting!");
                break;

            case ProcessImage.DetectionResponseTooManyEdges:
            case ProcessImage.DetectionResponseTooManyColors:
                tv.setText("Protip: Try a totally blank wall!");
                break;

            case ProcessImage.DetectionResponseNoFaceDetection:
                tv.setText("Protip: Move into the frame");

                break;
            case ProcessImage.DetectionResponseNoChromaColors:
            case ProcessImage.DetectionResponseFailure:
            default:
                tv.setText("Please try again");
                break;
        }

        Animation bounceAnimation = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        bounceAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                overLay.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

//        mainLay = null;

        mDetectContainer.addView(overLay);
        overLay.startAnimation(bounceAnimation);

        ((Activity) mContext).runOnUiThread(new Runnable()
//                mDetectContainer.post(new Runnable()
        {
            @Override
            public void run() {
                Animation fadeOutAnimation = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
                fadeOutAnimation.setStartOffset(3000);
                fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mDetectContainer.removeView(overLay);
                        if (overLay != null) {
                            overLay.removeAllViews();
                            overLay = null;
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });

                overLay.startAnimation(fadeOutAnimation);
            }
        });
    }
}