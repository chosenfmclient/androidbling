package fm.bling.blingy.stateMachine.mashup;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 16/01/17.
 * History:
 * ***********************************
 */
public class MashupContract {

    /** Table **/
    private static final String TABLE_NAME_MASHUP = "mashup_table";

    /** Columns **/
    private static final String COLUMN_MASHUP_ID = "mashup_id"; // (String)
    public  static final String COLUMN_STEP = "step"; // (int)
    public  static final String COLUMN_VIDEOS_NUM = "videos_num"; // (int)

    /** Queries **/
    // Create
    public static final String QUERY_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_MASHUP + " (" + COLUMN_MASHUP_ID + " TEXT UNIQUE, " +
                                                                                                               COLUMN_STEP +" INT, " +
                                                                                                               COLUMN_VIDEOS_NUM +" INT" + ");";
    // Insert
    public static final String QUERY_INSERT_MASHUP = "INSERT INTO " + TABLE_NAME_MASHUP + " (" + COLUMN_MASHUP_ID +
                                                                                            ", " + COLUMN_STEP +
                                                                                            ", " + COLUMN_VIDEOS_NUM + ") " + "VALUES (%s,%d,%d);";

    // Update
    public static final String QUERY_UPDATE_STEP = "UPDATE " + TABLE_NAME_MASHUP + " SET " + COLUMN_STEP + "=%d WHERE " + COLUMN_MASHUP_ID + "=\'%s\';";
    public static final String QUERY_UPDATE_VIDEOS_NUM = "UPDATE " + TABLE_NAME_MASHUP + " SET " + COLUMN_VIDEOS_NUM + "=%d WHERE " + COLUMN_MASHUP_ID + "=\'%s\';";

    // Delete
    public static final String QUERY_DELETE_MASHUP = "DELETE FROM " + TABLE_NAME_MASHUP + " WHERE " + COLUMN_MASHUP_ID + "=\'%s\';";

    // Select
    public static final String QUERY_GET_MASHUP_DETAILS = "SELECT * FROM " + TABLE_NAME_MASHUP + " WHERE " + COLUMN_MASHUP_ID + "=\'%s\';";

}
