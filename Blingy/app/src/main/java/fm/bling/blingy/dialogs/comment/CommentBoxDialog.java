package fm.bling.blingy.dialogs.comment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.comment.model.CAACommentText;
import fm.bling.blingy.rest.model.CAACommentParams;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/************************************
 * Project: Chosen Android Application
 * FileName: CommentBox
 * Description:
 * Created by Dawidowicz Nadav on 12/01/2016.
 * History:
 *************************************/

public class CommentBoxDialog extends DialogFragment {

    private ScaledImageView commentProfileImage;
    private EditText commentEditText;
    private FrameLayout commentBoxRelative;
    private ImageButton commentSendButton;
    private ProgressBar commentProgressBar;
    private ImageLoaderManager mImageLoaderManager;


    public static CommentBoxListener commentBoxListener;
    private String commentId;
    private String commentText;
    private boolean newCommentMode = false;
    // private CAACommentParams commentParams;
    private String videoId;
//    private String mCallingActivity = Constants.GAME_ACTIVITY;

    public interface CommentBoxListener {
        void onFinishCommentVideoDialog(boolean success, String commentId, String text);
    }

    public CommentBoxDialog() {
    }

//    public static CommentBoxDialog newInstance(CAACommentParams cAACommentParams) {
//        CommentBoxDialog fragment = new CommentBoxDialog();
//        Bundle args = new Bundle();
//        args.putString(Constants.COMMENT_TEXT, cAACommentParams.getText());
//        args.putString(Constants.COMMENT_ID, cAACommentParams.getCommentId());
//        args.putString(Constants.COMMENT_IMAGE, cAACommentParams.getUser().getPhoto());
//        fragment.setArguments(args);
//        return fragment;
//    }

    public static CommentBoxDialog newInstance(CAACommentParams cAACommentParams, CommentBoxListener callback) {
        CommentBoxDialog fragment = new CommentBoxDialog();
        Bundle args = new Bundle();
        args.putString(Constants.COMMENT_TEXT, cAACommentParams.getText());
        args.putString(Constants.COMMENT_ID, cAACommentParams.getCommentId());
        args.putString(Constants.COMMENT_IMAGE, cAACommentParams.getUser().getPhoto());
        fragment.setArguments(args);
        commentBoxListener = callback;
        return fragment;
    }

    public static CommentBoxDialog newInstance(String videoId, CommentBoxListener callback) {
        Log.d("Comment", "New comment frag builder");
        CommentBoxDialog fragment = new CommentBoxDialog();
        Bundle args = new Bundle();
        args.putString(Constants.COMMENT_TEXT, "");
        args.putString(Constants.COMMENT_ID, "");
        args.putString(Constants.COMMENT_IMAGE, CAAUserDataSingleton.getInstance().getPhotoUrl());
        args.putBoolean(Constants.COMMENT_NEW, true);
        args.putString(Constants.VIDEO_ID, videoId);
        fragment.setArguments(args);
        commentBoxListener = callback;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.comment_box_dialog, null);
        commentProfileImage = (ScaledImageView) v.findViewById(R.id.comment_image_profile);
        commentEditText = (EditText) v.findViewById(R.id.comment_edit_text);
        commentBoxRelative = (FrameLayout) v.findViewById(R.id.comment_box_relative);
        commentSendButton = (ImageButton) v.findViewById(R.id.comment_send_button);
        commentProgressBar = (ProgressBar) v.findViewById(R.id.sendProgress);

        mImageLoaderManager = new ImageLoaderManager(getActivity());
        commentProfileImage.setIsRoundedImage(true);
        commentProfileImage.setKeepAspectRatioAccordingToWidth(true);
        commentProfileImage.setAnimResID(R.anim.fade_in);
        commentProfileImage.setImageLoader(mImageLoaderManager);


        if (getArguments() != null) {
            commentId = getArguments().getString(Constants.COMMENT_ID);
            commentText = getArguments().getString(Constants.COMMENT_TEXT);
            String photoUrl = getArguments().getString(Constants.COMMENT_IMAGE);
            if (photoUrl != null && !photoUrl.isEmpty())
                commentProfileImage.setUrl(photoUrl);

            mImageLoaderManager.DisplayImage(commentProfileImage);
            newCommentMode = getArguments().containsKey(Constants.COMMENT_NEW) && getArguments().getBoolean(Constants.COMMENT_NEW);
            videoId = getArguments().containsKey(Constants.VIDEO_ID) ? getArguments().getString(Constants.VIDEO_ID) : "";
        }
        if (newCommentMode) {
            commentBoxRelative.setBackground(getResources().getDrawable(R.color.transparent));
        }
        commentEditText.setText(commentText);
        commentEditText.setSelection(commentText.length());
        commentEditText.requestFocus();
        commentSendButton.setEnabled(false);
        commentEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0 && s.toString().trim().length() > 0 && !commentSendButton.isEnabled()) {
                    commentSendButton.setClickable(true);
                    commentSendButton.setEnabled(true);
                    commentSendButton.setImageResource(R.drawable.standard_icon_game_send);
                } else if (s.length() == 0 && commentSendButton.isEnabled()) {
                    commentSendButton.setClickable(false);
                    commentSendButton.setEnabled(false);
                    commentSendButton.setImageResource(R.drawable.standard_icon_game_send_disabled);

                }
            }
        });


        commentSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CAACommentText text = new CAACommentText(commentEditText.getText().toString());
                commentSendButton.setEnabled(false);
                commentSendButton.setVisibility(View.GONE);
                commentProgressBar.setVisibility(View.VISIBLE);
                if (!newCommentMode) {
                    if (!commentEditText.getText().toString().contentEquals(commentText)) {
                        App.getUrlService().putEditComment(commentId, text, new Callback<String>() {
                            @Override
                            public void success(String s, Response response) {
                                dismiss();
                                commentBoxListener.onFinishCommentVideoDialog(true, commentId, text.getText());
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                dismiss();
                                commentBoxListener.onFinishCommentVideoDialog(false, commentId, text.getText());
                            }
                        });
                    } else {
                        commentBoxListener.onFinishCommentVideoDialog(false, commentId, text.getText());
                        dismiss();
                    }
                } else {
                    App.getUrlService().postLeaveComment(videoId, new CAACommentText(text.getText()), new Callback<CAACommentParams>() {
                        @Override
                        public void success(CAACommentParams s, Response response) {
                            commentBoxListener.onFinishCommentVideoDialog(true, s.getCommentId(), text.getText());
                            newCommentMode = false; //On stop doesn't neeed to be called if the comment is sent
                            try {
                                dismiss();
                            } catch (Throwable t) {
                                t.printStackTrace();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            dismiss();
                            commentBoxListener.onFinishCommentVideoDialog(false, "", text.getText());
                            newCommentMode = false; //On stop doesn't neeed to be called if the comment is sent
                        }
                    });
                }
            }
        });

        return v;
    }

    @Override
    public void onDestroy() {
        if (newCommentMode) {
            commentBoxListener.onFinishCommentVideoDialog(false, "", "");
        }
        mImageLoaderManager.clearImageViews();
        super.onDestroy();
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final FrameLayout root = new FrameLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.game_button_bg);
        return dialog;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

    }
}