package fm.bling.blingy.registration.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chosen-pro on 6/30/15.
 */
public class CAACFacebookSignup {
    public String getFacebookAccessToken() {
        return facebookAccessToken;
    }

    public void setFacebookAccessToken(String facebookAccessToken) {
        this.facebookAccessToken = facebookAccessToken;
    }

    @SerializedName("facebook_access_token")
    private  String facebookAccessToken;

    public CAACFacebookSignup(String facebookAccessToken) {
        this.facebookAccessToken = facebookAccessToken;
    }
}
