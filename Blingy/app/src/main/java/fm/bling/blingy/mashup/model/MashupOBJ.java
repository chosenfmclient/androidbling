package fm.bling.blingy.mashup.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.videoHome.model.CAAVideo;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 1/8/17.
 * History:
 * ***********************************
 */
public class MashupOBJ {


    @SerializedName("mashup_id")
    private String mashupID;

    @SerializedName("parent_id")
    private String parentID;

    @SerializedName("output")
    private CAAVideo outputVideo;

    @SerializedName("video")
    private CAAVideo video;

    @SerializedName("mashup")
    private String mashup;

    @SerializedName("timer")
    private long timer;

    @SerializedName("videos")
    private ArrayList<CAAVideo> videos;

    @SerializedName("status")
    private int status;

    public String getMashupID() {
        return mashupID;
    }

    public String getParentID() {
        return parentID;
    }

    public CAAVideo getMashupVideo() {
        return outputVideo;
    }

    public CAAVideo getRequesterVideo() {
        return video;
    }

    public String getMashup() {
        return mashup;
    }

    public long getTimer() {
        return timer;
    }

    public ArrayList<CAAVideo> getVideos() {
        return videos;
    }

    public int getStatus() {
        return status;
    }

    public User getUser(){
        return video.getUser();
    }
}
