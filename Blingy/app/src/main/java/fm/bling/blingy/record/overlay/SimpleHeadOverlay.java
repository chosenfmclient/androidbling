package fm.bling.blingy.record.overlay;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;

/**
 * Created by ben on 02/06/2016.
 */
public class SimpleHeadOverlay extends OverlayBase
{
    private Bitmap overlayBitmap;
    private int overlayWidth, overlayWidthCenter, overlayHeight;
    private Matrix overlayMatrix;
    private int paddingTop, faceID;
    private float fl[]=new float[2];
    private boolean isFirst = true;
    private float scaleFactor;

    public SimpleHeadOverlay(Bitmap overlayBitmap, int cameraRotation, Bitmap outputBitmap, boolean shouldRecycle)
    {
        super(cameraRotation, outputBitmap);
        overlayMatrix = new Matrix();
        setOverlay(overlayBitmap, shouldRecycle);
        setCameraRotation(cameraRotation);
    }

    @Override
    public void draw()
    {
        canvas.drawBitmap(overlayBitmap, overlayMatrix, mPaint);

        canvas.drawCircle(fl[0], fl[1], 30, mPaint);
        if(cameraRotation == 90)
            canvas.drawRect(fl[0], fl[1], fl[0] + height, fl[1] - width, mPaint);
        else canvas.drawRect(fl[0], fl[1], fl[0] - height, fl[1] - width, mPaint);
    }

    @Override
    public void updateLocations(RectF face)
    {

    }


    private float width, height;
//    @Override
//    public void updateLocations(Face face)
//    {
////        overlayMatrix.setTranslate(face.getPosition().x - overlayWidth, face.getPosition().y - overlayHeight);
//
//        if(faceID != face.getId())
//        {
//            boolean found = false;
//            for(int i=0; i<face.getLandmarks().size(); i++)
//            {
//                if (face.getLandmarks().get(i).getType() == Landmark.LEFT_EYE || face.getLandmarks().get(i).getType() == Landmark.RIGHT_EYE)
//                {
//                    found = true;
//                    fl[0]=face.getLandmarks().get(i).getPosition().x;
//                    fl[1]=face.getLandmarks().get(i).getPosition().y;
//                    converterMatrix.mapPoints(fl);
//                    float eyeLocation = fl[0];
//
//                    fl[0]= face.getPosition().x;
//                    fl[1]= face.getPosition().y;
//                    converterMatrix.mapPoints(fl);
//
//                    paddingTop = (int) ((fl[0] - eyeLocation) / 2f);
//                }
//            }
//            if(found)
//                faceID = face.getId();
//        }
//        else
//        {
//            fl[0] = face.getPosition().x;
//            fl[1] = face.getPosition().y;
//            converterMatrix.mapPoints(fl);
//        }
//        height = face.getHeight();
//        width = face.getWidth();
//
//        switch(cameraRotation)
//        {
//            case 90:
////                fl[1] += width;
//                fl[0] -= paddingTop;
//
//                overlayMatrix.setTranslate(fl[0] - overlayHeight, fl[1] - overlayWidth);
//                overlayMatrix.preRotate(360 - cameraRotation, overlayHeight, overlayWidth);
//                overlayMatrix.preScale(scaleFactor = (face.getWidth() / overlayWidth), scaleFactor, 0, overlayWidth);
//                overlayMatrix.postTranslate(0, -overlayWidth);
//                break;
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//            case 270:
//                fl[1] += width;
//                fl[0] -= paddingTop;
//
//                overlayMatrix.setTranslate(fl[0] - overlayHeight, fl[1] - overlayWidth);
//                overlayMatrix.preRotate(360 - cameraRotation, overlayHeight, overlayWidth);
//                overlayMatrix.preScale(scaleFactor = (face.getWidth() / overlayWidth), scaleFactor, overlayHeight, overlayWidth);
//                break;
//        }
////        fl[1] += (width = face.getWidth());
////        fl[0] -= paddingTop;
////        height = face.getHeight();
////
////        overlayMatrix.setTranslate(fl[0] - overlayHeight, fl[1] - overlayWidth);
////        overlayMatrix.preRotate(360 - cameraRotation, overlayHeight, overlayWidth);
////        overlayMatrix.preScale(scaleFactor = (face.getWidth() / overlayWidth), scaleFactor, overlayHeight, overlayWidth);//, overlayWidth / 2, overlayHeight);//, overlayLastX + overlayWidthCenter, overlayLastY + overlayHeight);
//    }

    @Override
    public void setOverlay(Bitmap overlay, boolean shouldRecycle)
    {
        if(overlayBitmap != null)
            try{overlayBitmap.recycle();}catch (Throwable err){}
        overlayBitmap = overlay;
        if(overlay != null)
        {
            overlayWidthCenter = overlay.getWidth() / 2;
            overlayHeight = overlay.getHeight();
            overlayWidth = overlay.getWidth();
            setConverter();
        }
    }

    @Override
    public void close()
    {
        if(overlayBitmap != null)
            try{overlayBitmap.recycle();}catch (Throwable err){}
        overlayBitmap = null;
        mPaint = null;
        overlayMatrix = null;
    }

}