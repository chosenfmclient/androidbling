package fm.bling.blingy.dialogs.share.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chosen-pro on 9/3/15.
 */
public class CAANativeShare {
    @SerializedName("video_id")
    private String videoId;

    @SerializedName("facebook_token")
    private String facebookToken;

    @SerializedName("text")
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

//    public String getFacebookToken() {
//        return facebookToken;
//    }
//
//    public void setFacebookToken(String facebookToken) {
//        this.facebookToken = facebookToken;
//    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String photo) {
        this.videoId = videoId;
    }


    public CAANativeShare(String videoId, String facebookToken, String text) {
        this.videoId = videoId;
        this.facebookToken = facebookToken;
        this.text = text;
    }
}
