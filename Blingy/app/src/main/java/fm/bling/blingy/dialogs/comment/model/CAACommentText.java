package fm.bling.blingy.dialogs.comment.model;

import com.google.gson.annotations.SerializedName;

/************************************
 * Project: Chosen Android Application
 * FileName: CAACommentText
 * Description:
 * Created by Dawidowicz Nadav on 10/01/2016.
 * History:
 *************************************/
public class CAACommentText {

    @SerializedName("text")
    private String text;

    public CAACommentText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
