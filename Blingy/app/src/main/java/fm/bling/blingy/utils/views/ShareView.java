package fm.bling.blingy.utils.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import fm.bling.blingy.App;
import fm.bling.blingy.R;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/9/16.
 * History:
 * ***********************************
 */
public class ShareView extends LinearLayout {

    private ImageView icon;
    private TextView text;
    private Context mContext;

    private Animation anim_out;
    private Animation anim_in;

    public ShareView(Context context, int iconResID, int textResID) {
        super(context);
        this.mContext = context;
        this.setLayoutParams(new LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.33f));
        this.setOrientation(VERTICAL);
        this.setGravity(Gravity.CENTER_HORIZONTAL);
        this.setBackgroundResource(R.color.transparent);

        icon = new ImageView(context);
        LinearLayout.LayoutParams iconParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        iconParams.bottomMargin = ((int)(15f * App.SCALE_Y));
        icon.setLayoutParams(iconParams);
        icon.setImageResource(iconResID);

        text = new TextView(context);
        text.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        text.setText(textResID);
        text.setTextSize(13f);
        text.setTextColor(ContextCompat.getColor(context, R.color.black_70));

        this.addView(icon);
        this.addView(text);
    }

    public void setIcon(final int iconResID){
        if(anim_out == null)
            anim_out = AnimationUtils.loadAnimation(mContext, android.R.anim.fade_out);
        if(anim_in == null)
            anim_in  = AnimationUtils.loadAnimation(mContext, android.R.anim.fade_in);
        anim_out.setDuration(150);
        anim_in.setDuration(130);
        anim_out.setAnimationListener(new Animation.AnimationListener()
        {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation)
            {
                icon.setImageResource(iconResID);
                anim_in.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        anim_out = null;
                        anim_in = null;
                    }
                });
                icon.startAnimation(anim_in);
            }
        });
        icon.startAnimation(anim_out);
    }

    public  void setText(int textResID){
        text.setText(textResID);
    }

}
