package fm.bling.blingy.registration.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAPostPassword
 * Description:
 * Created by Dawidowicz Nadav on 5/12/15.
 * History:
 * ***********************************
 */
public class CAAPostPassword {
    public CAAPostPassword(String email, String appId, String secret) {
        this.email = email;
        this.appId = appId;
        this.secret = secret;
    }
    @SerializedName("email")
    private String email;

    @SerializedName("app_id")
    private String appId;

    @SerializedName("secret")
    private String secret;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
