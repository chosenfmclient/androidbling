package fm.bling.blingy.record.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 10/10/16.
 * History:
 * ***********************************
 */
public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {
    private int spanCount;
    private int spacing;
    private boolean includeEdge;
    private boolean hasHeader;

    public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
        this.spanCount = spanCount;
        this.spacing = spacing;
        this.includeEdge = includeEdge;
    }

    public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge, boolean hasHeader) {
        this.spanCount = spanCount;
        this.spacing = spacing;
        this.includeEdge = includeEdge;
        this.hasHeader = hasHeader;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        int position = parent.getChildAdapterPosition(view) - (hasHeader ? 1 : 0); // item position
        int measuredSpace = spacing / spanCount;
        if (position >= 0) {
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * measuredSpace;
                outRect.right = (column + 1) * measuredSpace;

                if (position < spanCount) { // top edge
                    outRect.top = measuredSpace;
                }
                outRect.bottom = measuredSpace; // item bottom
            } else {
                outRect.left = column * measuredSpace;
                outRect.right = spacing - (column + 1) * measuredSpace;
                if (position >= spanCount) {
                    outRect.top = measuredSpace; // item top
                }
            }
        } else {
            outRect.left = 0;
            outRect.right = 0;
            outRect.top = 0;
            outRect.bottom = 0;
        }
    }

//    @Override
//    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
//    {
//        int position = parent.getChildAdapterPosition(view); // item position
//        int column = position % spanCount; // item column
//
//        if(column>0)
//            outRect.left = spacing;// / spanCount; // column * ((1f / spanCount) * spacing)
//        if (position >= spanCount)
//        {
//            outRect.top = spacing; // item top
//        }
//    }
}
