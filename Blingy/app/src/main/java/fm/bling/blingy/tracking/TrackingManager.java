package fm.bling.blingy.tracking;

import com.amplitude.api.Amplitude;

import com.appsee.Appsee;
import com.flurry.android.FlurryAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fm.bling.blingy.utils.Constants;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * In this class we do the tracking, this class provides a method for each action
 * <p>
 * <p>
 * Created by Ben Levi on 3/8/16.
 * History:
 * ***********************************
 */
public class TrackingManager {
    private static TrackingManager ourInstance = new TrackingManager();

    public static TrackingManager getInstance() {
        return ourInstance;
    }

    public void pageView(String pageView) {
        Amplitude.getInstance().logEvent(pageView);
    }

    public void appseeStartScreen(String screen){
        Appsee.startScreen(screen);
    }

    /**
     * Registration
     */
    public void tapSkip() {
        Amplitude.getInstance().logEvent(EventConstants.TAP_SKIP);
    }

    public void tapCanelRegister() {
        Amplitude.getInstance().logEvent(EventConstants.REGISTRATION_CANCEL);
    }

    public void registerFaild(int faildLogins) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.FAILED_LOGINS, faildLogins);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().setUserProperties(params);
        Amplitude.getInstance().logEvent(EventConstants.REGISTRATION_FAILED);
    }

    public void registerSubmit(String authType) {
        Amplitude.getInstance().logEvent(EventConstants.REGISTRATION_SUBMIT);

        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.AUTH_TYPE, authType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().setUserProperties(params);
    }

    public void inviteSend(String inviteOption, int inviteSent) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.INVITE_OPTION, inviteOption);
            params.put(EventConstants.INVITES_SENT, inviteSent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.TAP_SEND, params);


        JSONObject sent_invite_user = new JSONObject();
        try {
            sent_invite_user.put(EventConstants.INVITES_SENT, "yes");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().setUserProperties(sent_invite_user);
    }

    /**
     * Login
     */
    public void loginCancel() {
        Amplitude.getInstance().logEvent(EventConstants.LOGIN_CANCEL);
    }

    public void loginFailed(int faildLogins) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.FAILED_LOGINS, faildLogins);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().setUserProperties(params);
        Amplitude.getInstance().logEvent(EventConstants.LOGIN_FAILED);
    }

    public void loginSubmit(String authType) {
        Amplitude.getInstance().logEvent(EventConstants.LOGIN_SUBMIT);
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.AUTH_TYPE, authType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().setUserProperties(params);

        HashMap<String, String> map = new HashMap<>();
        map.put(EventConstants.AUTH_TYPE, authType);
        FlurryAgent.logEvent(EventConstants.LOGIN_SUBMIT, map);
    }

    public void loginFacebookSubmit() {
        Amplitude.getInstance().logEvent(EventConstants.LOGIN_FACEBOOK_SUBMIT);
        this.register(EventConstants.FACEBOOK);
    }

    public void loginFacebookFailed() {
        Amplitude.getInstance().logEvent(EventConstants.LOGIN_FACEBOOK_FAILED);
    }

    public void register(String type){
        switch (type){
            case EventConstants.GOOGLE_PLUS:
                Appsee.addEvent(EventConstants.REGISTRATION_GOOGLE_PLUS);
                break;
            case EventConstants.FACEBOOK:
                Appsee.addEvent(EventConstants.REGISTRATION_FACEBOOK);
                break;
            case EventConstants.EMAIL:
                Appsee.addEvent(EventConstants.REGISTRATION_EMAIL);
                break;
            case EventConstants.LOGIN:
                Appsee.addEvent(EventConstants.REGISTRATION_LOGIN);
                break;
        }
    }

    /**
     * Record
     */
    public void tapShootNow(String pageName, String objectID, String artistName, String videoName, int pos) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.PAGE_NAME, pageName);
            params.put(EventConstants.OBJECT_ID, objectID);
            params.put(EventConstants.ARTIST_NAME, artistName);
            params.put(EventConstants.VIDEO_NAME, videoName);
            params.put(EventConstants.POS, pos);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.RECORD_SHOOT_NOW, params);
    }

    public void camerView(long duration_sec) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.DURATION_SEC, duration_sec);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.RECORD_CAMERA, params);
    }

    public void detectStart() {
        Amplitude.getInstance().logEvent(EventConstants.RECORD_TAP_DETECT);
        Appsee.addEvent(EventConstants.RECORD_TAP_DETECT);
    }

    public void detectCompleted(long duration_sec) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.DURATION_SEC, duration_sec);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.RECORD_DETECTED, params);
    }

    public void recordCancel(String pageName) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.PAGE_NAME, pageName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.RECORD_CANCEL, params);
    }

    public void recordStart(String clipId, String artistName, String videoName) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.CLIP_ID, clipId);
            params.put(EventConstants.ARTIST_NAME, artistName);
            params.put(EventConstants.VIDEO_NAME, videoName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.RECORD_START,params);
        Appsee.addEvent(EventConstants.RECORD_START);
    }

    public void recordPause(String clipId, String artistName, String videoName) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.CLIP_ID, clipId);
            params.put(EventConstants.ARTIST_NAME, artistName);
            params.put(EventConstants.VIDEO_NAME, videoName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.RECORD_PAUSE,params);
        Appsee.addEvent(EventConstants.RECORD_PAUSE);
    }

    public void recordStop(String clipId, String artistName, String videoName) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.CLIP_ID, clipId);
            params.put(EventConstants.ARTIST_NAME, artistName);
            params.put(EventConstants.VIDEO_NAME, videoName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.RECORD_STOP,params);
        Appsee.addEvent(EventConstants.RECORD_STOP);
    }

    public void recordReview() {
        Amplitude.getInstance().logEvent(EventConstants.RECORD_REVIEW);
    }

    public void recordSubmit() {
        Amplitude.getInstance().logEvent(EventConstants.RECORD_SUBMIT);
        Appsee.addEvent(EventConstants.RECORD_SUBMIT);
    }

    public void recordCompleted(String status, String objectID, String artistName, String videoName, ArrayList<String> effects, int videosCreated) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.STATUS, status);
            params.put(EventConstants.OBJECT_ID, objectID);
            params.put(EventConstants.ARTIST_NAME, artistName);
            params.put(EventConstants.VIDEO_NAME, videoName);
            params.put(EventConstants.EFFECTS, effects);
            params.put(EventConstants.VIDEOS_CREATED, videosCreated);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.RECORD_COMPLETED, params);
        Appsee.addEvent(EventConstants.RECORD_COMPLETED);
    }

    public void tapShare(String pageName, String objectID) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.PAGE_NAME, pageName);
            params.put(EventConstants.OBJECT_ID, objectID);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.TAP_SHARE, params);

        Appsee.addEvent(EventConstants.TAP_SHARE);
    }

    public void sharePlatform(String pageName, String objectID, String artistName, String videoName, String shareID, String platform) {
        JSONObject amplitude_params = new JSONObject();
        try {
            amplitude_params.put(EventConstants.PAGE_NAME, pageName);
            amplitude_params.put(EventConstants.OBJECT_ID, objectID);
            amplitude_params.put(EventConstants.ARTIST_NAME, artistName);
            amplitude_params.put(EventConstants.VIDEO_NAME, videoName);
            amplitude_params.put(EventConstants.SHARE_ID, shareID);
            amplitude_params.put(EventConstants.PLATFORM, platform);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.TAP_SHARE_PLATFORM, amplitude_params);

        Map<String, Object> appsee_params = new HashMap<>();
        appsee_params.put(EventConstants.PLATFORM, platform);
        Appsee.addEvent(EventConstants.TAP_SHARE_PLATFORM, appsee_params);
    }

    public void shareCompleted() {
        Amplitude.getInstance().logEvent(EventConstants.TAP_SHARE_COMPLETED);
    }

    /**
     * Edit Profile
     */
    public void editProfileCancel() {
        Amplitude.getInstance().logEvent(EventConstants.EDIT_PROFILE_CANCEL);
    }

    public void editProfileSubmit(JSONObject editedPatams) {
        Amplitude.getInstance().logEvent(EventConstants.EDIT_PROFILE_SUMBIT);
        Amplitude.getInstance().setUserProperties(editedPatams);
    }

    public void editPic() {
        Amplitude.getInstance().logEvent(EventConstants.EDIT_PROFILE_EDIT_PIC);
    }

    public void editPicSubmit() {
        Amplitude.getInstance().logEvent(EventConstants.EDIT_PROFILE_SUBMIT_PIC);

        JSONObject pic_set = new JSONObject();
        try {
            pic_set.put(EventConstants.PROFILE_PIC_SET, "yes");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().setUserProperties(pic_set);
    }

    /**
     * Follow
     */
    public void tapFollow(String followedID, String pageName, int pos, int following) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.FOLLOWED_ID, followedID);
            params.put(EventConstants.PAGE_NAME, pageName);
            params.put(EventConstants.POS, pos);
            params.put(EventConstants.FOLLOWING_COUNT, following);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.TAP_FOLLOW, params);
    }

    public void tapUnfollow(int following, String followedID, String pageName, String pos) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.UNFOLLOWED_ID, followedID);
            params.put(EventConstants.PAGE_NAME, pageName);
            params.put(EventConstants.POS, pos);
            params.put(EventConstants.FOLLOWING_COUNT, following);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.TAP_UNFOLLOW, params);
    }

    /**
     * Like
     */
    public void tapLikeButton(String video_id, String pageName, boolean isLike) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.OBJECT_ID, video_id);
            params.put(EventConstants.PAGE_NAME, pageName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(isLike)
            Amplitude.getInstance().logEvent(EventConstants.TAP_LIKE, params);
        else
            Amplitude.getInstance().logEvent(EventConstants.TAP_UNLIKE, params);
    }

    /**
     * Notifications
     */
    public void tapNotification(int pos, String notificationID, String type) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.POS, pos);
            params.put(EventConstants.NOTIFICATION_ID, notificationID);
            params.put(EventConstants.NOTIFICATION_TYPE, type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.TAP_NOTIFICATION, params);
    }

    public void receivedPushNotification(String notificationType, String text, String source, String notification_id, String notificationMethod) {
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.NOTIFICATION_TYPE, notificationType);
            params.put(EventConstants.TEXT, text);
            params.put(EventConstants.SOURCE, source);
            params.put(EventConstants.NOTIFICATION_ID, notification_id);
            params.put(EventConstants.NOTIFICATION_METHOD, notificationMethod);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.NOTIFICATION_PUSH_RECEIVED, params);
    }

    /**
     * Video
     */
    public void videoPlay(String pageName, String objectID, String artistName, String videoName, int pos){
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.PAGE_NAME, pageName);
            params.put(EventConstants.OBJECT_ID, objectID);
            params.put(EventConstants.ARTIST_NAME, artistName);
            params.put(EventConstants.VIDEO_NAME, videoName);
            params.put(EventConstants.POS, pos);
//            params.put(EventConstants.RELATIONSHIP, relationship);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.VIDEO_VIEW, params);
    }

    /**
     * Search
     */
    public void searchMusicClip(String text){
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.SEARCH_TEXT, text);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.PICK_MUSIC_SEARCH, params);
    }

    public void searchDiscover(String type, String text){
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.SEARCH_TYPE, type);
            params.put(EventConstants.SEARCH_TEXT, text);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.DISCOVER_SEARCH, params);
    }

    /**
     * Camera Error
     * @param cameraErrorMsg - the error msg.
     */
    public void trackCameraError(String cameraErrorMsg) {
        Map<String, Object> appsee_params = new HashMap<>();
        appsee_params.put(EventConstants.ERROR, cameraErrorMsg);
        Appsee.addEvent(EventConstants.CAMERA_ERROR, appsee_params);
    }

    /**
     * Cameo
     */

    public void tapInviteFriendsCameo(int invites_sent){
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.INVITES_SENT, invites_sent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.CAMEO_INVITE_SENT, params);
    }

    public void tapMakeCameo(String objectId, String clipID, String artistName,String songName, String type){
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.OBJECT_ID, objectId);
            params.put(Constants.CLIP_ID, clipID);
            params.put(EventConstants.ARTIST_NAME, artistName);
            params.put(EventConstants.VIDEO_NAME, songName);
            params.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.CAMEO_MAKE, params);
    }

    public void tapCameoCompleted(String objectId, String clipID, String artistName,String songName,String status){
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.OBJECT_ID, objectId);
            params.put(Constants.CLIP_ID, clipID);
            params.put(EventConstants.ARTIST_NAME, artistName);
            params.put(EventConstants.VIDEO_NAME, songName);
            params.put(EventConstants.STATUS, status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.CAMEO_COMPLETED, params);
    }

    public void tapViewOriginal(String objectId, String clipID, String artistName,String songName){
        JSONObject params = new JSONObject();
        try {
            params.put(EventConstants.OBJECT_ID, objectId);
            params.put(Constants.CLIP_ID, clipID);
            params.put(EventConstants.ARTIST_NAME, artistName);
            params.put(EventConstants.VIDEO_NAME, songName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Amplitude.getInstance().logEvent(EventConstants.CAMEO_VIEW_ORIGINAL, params);
    }


    /**
     * Preparing Error
     * @param errorMsg - the error msg.
     */
    public void trackPreparingError(String errorMsg) {
        Map<String, Object> appsee_params = new HashMap<>();
        appsee_params.put(EventConstants.ERROR, errorMsg);
        Appsee.addEvent(EventConstants.PREPARING_CLIP_ERROR, appsee_params);
    }

}