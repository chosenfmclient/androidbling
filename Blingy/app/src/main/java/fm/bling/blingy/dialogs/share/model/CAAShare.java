package fm.bling.blingy.dialogs.share.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAShare
 * Description:
 * Created by Zach on 08/30/15.
 * History:
 * ***********************************
 */
public class CAAShare {
    @SerializedName("data")
    private CAAPostShare data;

    public CAAPostShare getData() {
        return data;
    }

    public void setData(CAAPostShare data) {
        this.data = data;
    }

    public CAAShare(CAAPostShare data) {
        this.data = data;
    }

}
