package fm.bling.blingy.database.handler;

import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.Iterator;

import fm.bling.blingy.database.contract.DuetContract;
import fm.bling.blingy.mashup.model.InviteeContact;

public class DuetInvitedHandler extends BaseHandler<InviteeContact> {

    private static DuetInvitedHandler mDuetInvitedDataHandler;

    private DuetInvitedHandler(){

    }

    public static DuetInvitedHandler getInstance(){
        if(mDuetInvitedDataHandler == null)
            mDuetInvitedDataHandler = new DuetInvitedHandler();
        return mDuetInvitedDataHandler;
    }

    @Override
    public long add(@NonNull InviteeContact object) {
        return 0;
    }

    @Override
    public long addMany(@NonNull ArrayList<InviteeContact> inviteeContacts) {
        if(inviteeContacts.size() > 0) {
            StringBuilder query = new StringBuilder(DuetContract.INSERT_MANY);
            Iterator<InviteeContact> iterator = inviteeContacts.iterator();
            while (iterator.hasNext()) {
                InviteeContact contact = iterator.next();
                query.append("(").append(contact.toSqlValues()).append(")").append(iterator.hasNext() ? ",\n" : ";");
            }
            DataBaseManager.excQuery(query.toString());
            return inviteeContacts.size();
        }
        return 0;
    }

    @Override
    public boolean remove(@NonNull InviteeContact object) {
        return false;
    }

    @Override
    public boolean update(@NonNull InviteeContact object) {
        return false;
    }

    @Override
    public InviteeContact get(@NonNull InviteeContact object) {
        return null;
    }

    @Override
    public ArrayList<InviteeContact> getAll(String video_id) {
        return DataBaseManager.getAllInvted(DuetContract.COLUMN_VIDEO_ID + "=" + video_id);
    }

    public void removeAll(){
        DataBaseManager.excQuery(DuetContract.DELETE_TABLE);
        DataBaseManager.excQuery(DuetContract.CREATE_TABLE);
    }
}
