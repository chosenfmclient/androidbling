package fm.bling.blingy.inviteFriends;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.inviteFriends.listeners.InviteDataReadyListener;
import fm.bling.blingy.inviteFriends.model.CAAContactEmails;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.inviteFriends.model.CAAApiContactsResponse;
import fm.bling.blingy.inviteFriends.model.ContactInfo;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.stateMachine.StateMachine;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Chosen-pro on 12/07/2016.
 */

public class InviteHandler {
    private InviteDataReadyListener onReadyListener;
    private ArrayList<ContactInfo> searchBaseContacts;
    private List<List<String>> mListsEmails;
    private List<ArrayList<ContactInfo>> contactsLists = new ArrayList<>();
    private Context mContext;
    private int nCalls = 0;
    private List<User> receivedData;
    private List<String> excludedNumbers;
    private List<String> excludedEmails;
    private ArrayList<Integer> contactIdsForPhoneList;

    private static List<String> emailsToRemove;
    public static List<String> emails;
    public static ArrayList<ContactInfo> contacts;
    public static List<User> chosenContacts;
    private boolean alphabetize;

    public List<User> getChosenContacts() {
        return chosenContacts;
    }

    public ArrayList<ContactInfo> getEmailContacts() {
        ArrayList<ContactInfo> emailContacts = new ArrayList<>();
        ContactInfo contactInfo;
        for (int i = 0; i < contacts.size(); i++) {
            contactInfo = contacts.get(i);
            if (contactInfo.email != null && !excludedEmails.contains(contactInfo.email) && !contactIdsForPhoneList.contains(contactInfo.id)) {
                emailContacts.add(contactInfo);
            }
        }
        return emailContacts;
    }

    public ArrayList<ContactInfo> getPhoneContacts() {
        ArrayList<ContactInfo> phoneContacts = new ArrayList<>();
        ContactInfo contactInfo;
        for (int i = 0; i < contacts.size(); i++) {
            contactInfo = contacts.get(i);
            if (contactInfo.isPhoneNumberSet() && !excludedNumbers.contains(contactInfo.phone())) {
                phoneContacts.add(contactInfo);
            }
        }
        return phoneContacts;
    }

    public InviteHandler(Context mContext, InviteDataReadyListener onReadyListener, boolean alphabetize) {
        if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_CONTACTS))
            return ;

        this.mContext = mContext;
        this.onReadyListener = onReadyListener;
        this.alphabetize = alphabetize;
        new GetContactsTask().execute();
    }

    private class GetContactsTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            getContactsToInvite();
            return null;
        }
    }

    private void getContactsToInvite() {
        excludedNumbers = StateMachine.getInstance(mContext.getApplicationContext()).getTemporarilyExcluded(StateMachine.INVITE_EXCLUDE_TYPE_PHONE);
        excludedEmails = StateMachine.getInstance(mContext.getApplicationContext()).getTemporarilyExcluded(StateMachine.INVITE_EXCLUDE_TYPE_EMAIL);
        contacts = new ArrayList<>();
        searchBaseContacts = new ArrayList<>();
        emails = new ArrayList<>();
        contactIdsForPhoneList = new ArrayList<>();

        String[] PROJECTION =
                {
                        ContactsContract.CommonDataKinds.Identity.CONTACT_ID,
                        ContactsContract.Contacts.DISPLAY_NAME,
                        ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
                        ContactsContract.CommonDataKinds.Email.ADDRESS,
                };

        ContentResolver cr = mContext.getContentResolver();
        Cursor cursorStarred = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, ContactsContract.Contacts.STARRED + "=1", null, ContactsContract.Contacts.DISPLAY_NAME + " COLLATE NOCASE DESC");
        Cursor cursorNotStarred = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, ContactsContract.Contacts.STARRED + "!=1", null, ContactsContract.Contacts.DISPLAY_NAME + " COLLATE NOCASE DESC");
        getEmailContactsWithCursor(cursorStarred);
        getEmailContactsWithCursor(cursorNotStarred);


        //    int countryCode = -1;
        String countryRegion = null;
        try {
            TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            countryRegion = tm.getNetworkCountryIso();
            if (countryRegion == null || (countryRegion != null && countryRegion.isEmpty()))
                countryRegion = tm.getSimCountryIso();
            if (countryRegion != null) {
                countryRegion = countryRegion.toUpperCase();
            }
        } catch (Throwable xz) {
            xz.printStackTrace();
        }
        if (countryRegion != null) {
            String[] PROJECTION2 =
                    {
                            ContactsContract.CommonDataKinds.Identity.CONTACT_ID,
                            ContactsContract.Contacts.DISPLAY_NAME,
                            ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
                            ContactsContract.CommonDataKinds.Phone.NUMBER,
                            ContactsContract.CommonDataKinds.Phone.TYPE
                    };

            cursorStarred = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION2, ContactsContract.Contacts.STARRED + "=1", null, ContactsContract.Contacts.DISPLAY_NAME + " COLLATE NOCASE DESC");
            cursorNotStarred = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION2, ContactsContract.Contacts.STARRED + "!=1", null, ContactsContract.Contacts.DISPLAY_NAME + " COLLATE NOCASE DESC");
            getSmsContactsWithCursor(cursorStarred, countryRegion);
            getSmsContactsWithCursor(cursorNotStarred, countryRegion);

            nCalls = 0;
            if (chosenContacts == null && emails.size() > 0) {
                mListsEmails = Utils.partition(emails, 500);
                getFriendsOnChosen();
            } else {
                setContactsArray();
            }
        }
    }

    private void getEmailContactsWithCursor(Cursor cur) {
        final int emailIdColumnIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Identity.CONTACT_ID);
        final int emailNameColumnIndex = cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        final int emailPhotoColumnIndex = cur.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI);
        final int emailEmailColumnIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS);

        ArrayList<ContactInfo> contactsList = new ArrayList<>();
        int id;
        String name, photoUri, email;
        // ContactInfo ci;
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                id = cur.getInt(emailIdColumnIndex);
                name = cur.getString(emailNameColumnIndex);
                photoUri = cur.getString(emailPhotoColumnIndex);
                email = cur.getString(emailEmailColumnIndex);
                contactsList.add(new ContactInfo(id, name, photoUri, email, false));//, phoneNumber
                emails.add(email);
            }
        }
        cur.close();
        contactsLists.add(contactsList);
    }

    private void getSmsContactsWithCursor(Cursor cur, String countryRegion) {
        if (cur.getCount() > 0) {
            final int phoneIdColumnIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Identity.CONTACT_ID);
            final int phoneNameColumnIndex = cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
            final int phonePhotoColumnIndex = cur.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI);
            final int phoneNumberColumnIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            final int phoneTypeColumnIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);

            ArrayList<ContactInfo> contactsList = new ArrayList<>();
            int id;
            String name, photoUri, phone;
            PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber phoneNumber = new Phonenumber.PhoneNumber();
            int phoneTypeOrdinal;

            while (cur.moveToNext()) {
                phone = cur.getString(phoneNumberColumnIndex);
                id = cur.getInt(phoneIdColumnIndex);

                try {
                    phone = getOnlyDigits(phone);
                    phoneNumberUtil.parse(phone, countryRegion, phoneNumber);
                    PhoneNumberUtil.PhoneNumberType phoneNumberType = phoneNumberUtil.getNumberType(phoneNumber);
                    phoneTypeOrdinal = phoneNumberType.ordinal();

                    if (phoneTypeOrdinal == PhoneNumberUtil.PhoneNumberType.MOBILE.ordinal()) {
                        phone = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                    } else if (phoneTypeOrdinal == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE.ordinal()) {
                        if (cur.getInt(phoneTypeColumnIndex) == (ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)) {
                            phone = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                        } else {
                            continue;
                        }
                    } else if (phoneTypeOrdinal == PhoneNumberUtil.PhoneNumberType.UNKNOWN.ordinal()) {
                        try {
                            if (phone.charAt(0) == '*')
                                continue;
                            if (phone.charAt(0) != '+')
                                phone = '+' + phone;

                            String countryRegion2 = phoneNumberUtil.getRegionCodeForCountryCode(phoneNumberUtil.parse(phone, "").getCountryCode());
                            phoneNumberUtil.parse(phone, countryRegion2, phoneNumber);

                            if (phone.length() < 7)
                                continue;

                            if (phoneTypeOrdinal == PhoneNumberUtil.PhoneNumberType.MOBILE.ordinal()) {
                                phone = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                            } else {
                                if (cur.getInt(phoneTypeColumnIndex) == (ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)) {
                                    phone = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                                } else {
                                    continue;
                                }
                            }
                        } catch (Throwable err) {
                            err.printStackTrace();
                            continue;
                        }
                    } else {
                        continue;
                    }
                } catch (Throwable err) {
                    err.printStackTrace();
                    continue;
                }
                name = cur.getString(phoneNameColumnIndex);
                photoUri = cur.getString(phonePhotoColumnIndex);

                contactIdsForPhoneList.add(id);
                contactsList.add(new ContactInfo(id, name, photoUri, phone, true));
            }
            contactsLists.add(contactsList);
        }
        cur.close();
    }

    private String getOnlyDigits(String phone) {
        String result = "";
        for (int i = 0; i < phone.length(); i++) {
            if (Character.isDigit(phone.charAt(i))) {
                result += phone.charAt(i);
            }
        }
        return result;
    }

    private void getFriendsOnChosen() {
        App.getUrlService().postFindFriends(new CAAContactEmails(mListsEmails.get(nCalls)), new Callback<CAAApiContactsResponse>() {
            @Override
            public void success(CAAApiContactsResponse s, Response response) {
                if (receivedData == null)
                    receivedData = new ArrayList<>();
                receivedData.addAll(s.getData());

                if (emailsToRemove == null)
                    emailsToRemove = new ArrayList<>();
                emailsToRemove.addAll(s.getEmailsToRemove());

                nCalls++;
                if (nCalls <= mListsEmails.size() - 1) {
                    getFriendsOnChosen();
                } else {
                    if (chosenContacts == null) {
                        chosenContacts = new ArrayList<>();
                    }
                    if (receivedData.size() != 0) {

                        User caaUser;
                        for (int i = 0, j; i < receivedData.size(); i++) {
                            caaUser = receivedData.get(i);
                            for (j = 0; j < chosenContacts.size(); j++) {
                                if (caaUser.getFullName().compareToIgnoreCase(chosenContacts.get(j).getFullName()) < 0)
                                    break;
                            }
                            if (j > chosenContacts.size())
                                chosenContacts.add(caaUser);
                            else chosenContacts.add(j, caaUser);
                        }
                    }
                    setContactsArray();
                }
            }

            @Override
            public void failure(RetrofitError error) {
//                Log.d("Invite handler", "find friends API failed");
            }
        });

    }

    private void setContactsArray() {
        int i;
        boolean skip = false;
        int comparisonResult;
        for (ArrayList<ContactInfo> contactsList : contactsLists) {
            ArrayList<ContactInfo> mContacts = new ArrayList<>();

            for (ContactInfo contactInfo : contactsList) {
                if (contactInfo.email != null && emailsToRemove.contains(contactInfo.email))
                    continue;

                for (i = 0; i < mContacts.size(); i++) {
                    if ((comparisonResult = contactInfo.name.compareToIgnoreCase(mContacts.get(i).name)) < 0)
                        break;
                    else if (comparisonResult == 0)
                        skip = true;
                    break;
                }
                if (skip) {
                    skip = false;
                } else {
                    searchBaseContacts.add(i, contactInfo);
                    mContacts.add(i, contactInfo);
                }
            }
            contacts.addAll(mContacts);
        }
        if (!alphabetize) {
            Collections.shuffle(contacts, new Random(System.nanoTime()));
        }
        else{
            Collections.sort(contacts, new Comparator<ContactInfo>() {
                @Override
                public int compare(ContactInfo contact1, ContactInfo contact2) {
                    return contact1.name.toLowerCase().compareTo(contact2.name.toLowerCase());
                }
            });
        }
//        TabContentInviteFriend2.contacts = contacts;
//        TabContentInviteFriend2.chosenContacts = chosenContacts;
        onReadyListener.onInviteDataReady();
    }
}
