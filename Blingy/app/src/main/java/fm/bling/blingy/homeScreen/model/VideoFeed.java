package fm.bling.blingy.homeScreen.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import fm.bling.blingy.videoHome.model.CAAVideo;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 10/6/16.
 * History:
 * ***********************************
 */
public class VideoFeed {
    @SerializedName("video_feed")
    ArrayList<CAAVideo> videoFeed;

    public ArrayList<CAAVideo> getVideoFeed() {
        return videoFeed;
    }
}
