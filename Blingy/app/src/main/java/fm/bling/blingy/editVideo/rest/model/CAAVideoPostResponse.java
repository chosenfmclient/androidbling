package fm.bling.blingy.editVideo.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.videoHome.model.CAAVideo;


/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAVideoPostResponse
 * Description:
 * Created by Dawidowicz Nadav on 5/18/15.
 * History:
 * ***********************************
 */

public class CAAVideoPostResponse {

    @SerializedName("video_id")
    private String videoId;

    @SerializedName("user")
    private User user;

    @SerializedName("parent")
    private CAAVideo parent;

    @SerializedName("type")
    private String type;

    @SerializedName("status")
    private String status;

    @SerializedName("date")
    private String date;

    @SerializedName("song_name")
    private String songName;

    @SerializedName("artist_name")
    private String artistName;

    @SerializedName("genre")
    private String genre;

    @SerializedName("thumbnail")
    private CAAMethod thumbnail;

    @SerializedName("url")
    private CAAMethod url;

    @SerializedName("video_url")
    private String videoUrl;

    @SerializedName("score")
    private String score;

    @SerializedName("total_views")
    private String totalViews;

    @SerializedName("game_views")
    private String gameViews;

    @SerializedName("total_shares")
    private String totalShares;

    @SerializedName("total_votes")
    private String totalVotes;

    @SerializedName("total_responses")
    private String totalResponses;

    @SerializedName("rank")
    private String rank;

    @SerializedName("clip")
    private ArrayList<String> clip;

    @SerializedName("clip_url")
    private String clipUrl;

    @SerializedName("audio_filters")
    private ArrayList<String> audioFilters;

    @SerializedName("video_filters")
    private ArrayList<String> videoFilters;

    @SerializedName("song")
    private CAASongInfo song;

    @SerializedName("question")
    private String question;

    public CAAMethod getUrl() {
        return url;
    }

    public void setUrl(CAAMethod url) {
        this.url = url;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getArtistName() {
        return artistName;
    }


    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public CAAMethod getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(CAAMethod thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public CAAVideo getParent() {
        return parent;
    }

    public void setParent(CAAVideo parent) {
        this.parent = parent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
//    public void setArtistName(String artistName) {
//        this.artistName = artistName;
//    }

//    public String getVideoUrl() {
//        return videoUrl;
//    }
//
//    public void setVideoUrl(String videoUrl) {
//        this.videoUrl = videoUrl;
//    }
//
//    public String getScore() {
//        return score;
//    }
//
//    public void setScore(String score) {
//        this.score = score;
//    }
//
//    public String getTotalViews() {
//        return totalViews;
//    }
//
//    public void setTotalViews(String totalViews) {
//        this.totalViews = totalViews;
//    }
//
//    public String getGameViews() {
//        return gameViews;
//    }
//
//    public void setGameViews(String gameViews) {
//        this.gameViews = gameViews;
//    }
//
//    public String getTotalShares() {
//        return totalShares;
//    }
//
//    public void setTotalShares(String totalShares) {
//        this.totalShares = totalShares;
//    }
//
//    public String getTotalVotes() {
//        return totalVotes;
//    }
//
//    public void setTotalVotes(String totalVotes) {
//        this.totalVotes = totalVotes;
//    }
//
//    public String getTotalResponses() {
//        return totalResponses;
//    }
//
//    public void setTotalResponses(String totalResponses) {
//        this.totalResponses = totalResponses;
//    }
//
//    public String getRank() {
//        return rank;
//    }
//
//    public void setRank(String rank) {
//        this.rank = rank;
//    }
//
//    public ArrayList<String> getClip() {
//        return clip;
//    }
//
//    public void setClip(ArrayList<String> clip) {
//        this.clip = clip;
//    }
//
//    public String getClipUrl() {
//        return clipUrl;
//    }
//
//    public void setClipUrl(String clipUrl) {
//        this.clipUrl = clipUrl;
//    }
//
//    public ArrayList<String> getAudioFilters() {
//        return audioFilters;
//    }
//
//    public void setAudioFilters(ArrayList<String> audioFilters) {
//        this.audioFilters = audioFilters;
//    }

//    public ArrayList<String> getVideoFilters() {
//        return videoFilters;
//    }
//
//    public void setVideoFilters(ArrayList<String> videoFilters) {
//        this.videoFilters = videoFilters;
//    }
//
//    public CAASongInfo getSong() {
//        return song;
//    }
//
//    public void setSong(CAASongInfo song) {
//        this.song = song;
//    }
//
//    public String getQuestion() {
//        return question;
//    }
//
//    public void setQuestion(String question) {
//        this.question = question;
//    }
}

