package fm.bling.blingy.profile.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAPhoto
 * Description:
 * Created by Dawidowicz Nadav on 7/16/15.
 * History:
 * ***********************************
 */
public class CAAPhoto {
    @SerializedName("photo")
    private String photo;

    public CAAPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto(){
        return photo;
    }

}
