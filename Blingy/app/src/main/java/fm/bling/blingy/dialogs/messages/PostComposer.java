package fm.bling.blingy.dialogs.messages;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import fm.bling.blingy.R;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 9/20/16.
 * History:
 * ***********************************
 */
public class PostComposer extends Dialog {

    private String title;
    private String text;
    private String yesText;
    private String cancelText;
    private Boolean answer = false;
    public EditText dialogText;

    public PostComposer (Context context) {
        super(context);
    }

    /**
     * Create ChosenPostComposer
     * @param context - parent context
     * @param title - the title text
     * @param text - the default text - can be empty
     * @param cancelText - the cancel button text (if null then the button won't appear)
     * @param yesText - the yes button text (if null then the button won't appear)
     *                - if both are null yesText defaults to "OK"
     * @return ChosenPostComposer
     */
    public static PostComposer newInstance(Context context, String title, String text, String cancelText, String yesText) {
        PostComposer dialog = new PostComposer(context);
        dialog.title = title;
        dialog.text = text;
        dialog.yesText = yesText;
        dialog.cancelText = cancelText;
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.post_composer_dialog);
        dialogText = (EditText) findViewById(R.id.dialogText);
        dialogText.setText(text);
        TextView dialogTitle = (TextView) findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        TextView yesTextView = (TextView) findViewById(R.id.text_view_yes);
        yesTextView.setText(yesText);
        TextView cancelTextView = (TextView) findViewById(R.id.text_view_cancel);
        cancelTextView.setText(cancelText);


        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer = false;
                dismiss();
            }
        });

        yesTextView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                answer = true;
                dismiss();
            }
        });
    }

    public Boolean getAnswer() {
        return answer;
    }

    public String getInputText() {
        if (dialogText != null) {
            return dialogText.getText().toString();
        } else {
            return "";
        }
    }
}
