package fm.bling.blingy.dialogs.messages;

import android.content.Context;
import android.net.NetworkInfo;
import android.view.View;

import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 9/25/16.
 * History:
 * ***********************************
 */
public class NetworkDialog extends BaseDialog {

    public NetworkDialog(Context context) {
        super(context,"No internet", "You are not connected to the internet!");
        mCancelButton.setVisibility(View.GONE);
    }

    public NetworkDialog(Context context, String info) {
        super(context,"No internet", info);
        mCancelButton.setVisibility(View.GONE);
    }

    @Override
    public void dismiss() {
        NetworkConnectivityListener.mNetworkDialog = null;
        super.dismiss();
    }
}
