package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


/************************************
 * Project: Chosen Android Application
 * FileName: CAALikeResponse
 * Description:
 * Created by Dawidowicz Nadav on 10/01/2016.
 * History:
 *************************************/

public class CAALikeResponse {

    @SerializedName("total_likes")
    private String totalLikes;

    @SerializedName("data")
    private ArrayList<CAALikeParams> data;

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public ArrayList<CAALikeParams> getData() {
        return data;
    }

    public void setData(ArrayList<CAALikeParams> data) {
        this.data = data;
    }
}
