package fm.bling.blingy.database.contract;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 08/02/17.
 * History:
 * ***********************************
 */
public abstract class BaseContract {

    static final String CREATE_IF_NOT_EXISTS = "CREATE TABLE IF NOT EXISTS ";
    static final String TEXT_TYPE = " TEXT";
    static final String INT_TYPE = " INTEGER";
    static final String NOT_NULL = " NOT NULL";
    static final String PRIMARY_KEY = " PRIMARY KEY";
    static final String UNIQUE = " UNIQUE";
    static final String COMMA_SEP = ", ";

    public static final int STATUS_PENDING = 0;
    public static final int STATUS_SENT = 1;

    static final String OR_REPLACE = "OR REPLACE ";


}
