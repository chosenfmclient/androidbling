//package fm.bling.blingy.record.fragments;
//
//import android.media.AudioManager;
//import android.media.MediaPlayer;
//import android.os.Bundle;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import java.io.IOException;
//import java.util.ArrayList;
//
//import fm.bling.blingy.App;
//import fm.bling.blingy.R;
//import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
//import fm.bling.blingy.record.AddSoundActivity;
//import fm.bling.blingy.record.adapters.ItunesRecyclerAdapter;
//import fm.bling.blingy.record.rest.RestClientiTunes;
//import fm.bling.blingy.record.rest.model.CAAItunesMusicElement;
//import fm.bling.blingy.record.rest.model.CAAiTunes;
//import fm.bling.blingy.record.utils.SimpleDividerItemDecoration;
//import fm.bling.blingy.tracking.EventConstants;
//import fm.bling.blingy.tracking.TrackingManager;
//import fm.bling.blingy.utils.Constants;
//import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
//import fm.bling.blingy.utils.imagesManagement.views.ProgressBarView;
//import retrofit.Callback;
//import retrofit.RetrofitError;
//import retrofit.client.Response;
//
///**
// * *********************************
// * Project: Chosen Android Application
// * Description:
// * Created by Oren Zakay on 4/12/16.
// * History:
// * ***********************************
// */
//public class SoundTrackFragment extends BaseAudioFragment {
//
//    private RecyclerView mRecycleTrackList;
//    private ProgressBarView mChosenProgerss;
//    private TextView mNoResults;
//    private SoundTrackListener mCallBack;
//    private MediaPlayer mMediaPlayer;
//
//    private ArrayList<CAAItunesMusicElement> songs = null;
//    private ArrayList<CAAItunesMusicElement> mDataSet = null;
//    private ItunesRecyclerAdapter mItunesRecyclerAdapter;
//
//    private boolean isVisible = false;
//    private boolean isPrepared = false;
//    private boolean isLoadingMusic = false;
//
//    private View.OnClickListener mItemClickListener;
//    private View.OnClickListener mPlayPauseClickListener;
//
//    private View lastView;
//    private boolean isPassedOnCreate = true;
//    private int retries = 0;
//
//    public static SoundTrackFragment getInstance(ArrayList<CAAItunesMusicElement> items) {
//        SoundTrackFragment fragment = new SoundTrackFragment();
//        fragment.songs = items;
//        return fragment;
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        isLoadingMusic = false;
//        retries = 0;
//
//        mItemClickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (lastView != null && mMediaPlayer != null && mMediaPlayer.isPlaying())
//                    ((ImageView) lastView).setImageResource(R.drawable.sound_play_36);
//                CAAItunesMusicElement itunesElement = ((CAAItunesMusicElement) view.getTag());
//                ((AddSoundActivity) getActivity()).saveSoundtrackResult(itunesElement.getTrackName(),
//                        itunesElement.getArtistName(),
//                        itunesElement.getPreviewUrl());
//                mCallBack.onSongClicked();
//            }
//        };
//
//        mPlayPauseClickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!isLoadingMusic) {
//                    CAAItunesMusicElement caaItunesMusicElement = ((CAAItunesMusicElement) ((LinearLayout) view.getTag()).getTag());
//                    ProgressBar progressBar = ((ProgressBar) ((LinearLayout) view.getTag()).findViewById(R.id.progress_bar));
//                    if (mMediaPlayer != null) {
//                        if (mMediaPlayer.isPlaying()) {
//                            if (view == lastView) {
//                                ((ImageView) view).setImageResource(R.drawable.sound_play_36);
//                                mMediaPlayer.pause();
//                            } else {
//                                ((ImageView) lastView).setImageResource(R.drawable.sound_play_36);
//                                lastView = view;
//                                mMediaPlayer.reset();
//                                MusicPrepare(caaItunesMusicElement.getPreviewUrl(), progressBar);
//                            }
//
//                        } else {
//                            if (view == lastView) {
//                                mMediaPlayer.start();
//                                ((ImageView) view).setImageResource(R.drawable.sound_pause_36);
//                            } else {
//                                lastView = view;
//                                mMediaPlayer.reset();
//                                MusicPrepare(caaItunesMusicElement.getPreviewUrl(), progressBar);
//                            }
//                        }
//                    } else {
//                        lastView = view;
//                        mMediaPlayer = new MediaPlayer();
//                        MusicPrepare(caaItunesMusicElement.getPreviewUrl(), progressBar);
//                    }
//                }
//            }
//        };
//        if (songs == null) {
//            getSongs();
//            isPrepared = true;
//        }
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View v = inflater.inflate(R.layout.soundtrack_fragment, null);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
//        mRecycleTrackList = (RecyclerView) v.findViewById(R.id.recycler_view);
//        mRecycleTrackList.setLayoutManager(mLayoutManager);
//        mRecycleTrackList.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext(), R.drawable.line_divider));
//
//        mChosenProgerss = (ProgressBarView) v.findViewById(R.id.progress_bar);
//        mChosenProgerss.updateLayout();
//        mChosenProgerss.setVisibility(View.VISIBLE);
//
//        mNoResults = (TextView) v.findViewById(R.id.no_search_results);
//
//        if (!isPrepared) {
//            mDataSet = new ArrayList<>();
//            for (int i = 0; i < songs.size(); i++) {
//                mDataSet.add(i, songs.get(i));
//            }
//            prepareData(mDataSet);
//        }
//        mCallBack = (SoundTrackListener) getActivity();
//        return v;
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        isVisible = true;
//        if (!isPassedOnCreate)
//            mItunesRecyclerAdapter.notifyDataSetChanged();
//        else
//            isPassedOnCreate = false;
//
//    }
//
//    @Override
//    public void onStop() {
//        isVisible = false;
//        if (mMediaPlayer != null) {
//            mMediaPlayer.stop();
//            mMediaPlayer.release();
//            mMediaPlayer = null;
//        }
////        lastView = null;
//        super.onStop();
//    }
//
//    private void getSongs() {
//
//        App.getUrlService().getSoundtracks(new Callback<CAAiTunes>() {
//            @Override
//            public void success(CAAiTunes caAiTunes, Response response) {
//                retries = 0;
//                if (!caAiTunes.getResultCount().equalsIgnoreCase("") && Integer.parseInt(caAiTunes.getResultCount()) > 0) {
//                    prepareData(caAiTunes.getResults());
//                    songs = new ArrayList<>();
//                    for (int i = 0; i < caAiTunes.getResults().size(); i++) {
//                        songs.add(i, caAiTunes.getResults().get(i));
//                    }
//                } else
//                    mNoResults.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
////                if (getActivity() != null)
////                {
////                    if (retries < 4)
////                    {
////                        getSongs();
////                        retries++;
////                    }
////                    else
////                    {
////                        try
////                        {
////                            CAANetworkErrors networkErrors = new CAANetworkErrors();
////                            ChosenAlertDialog dialog = networkErrors.getDialog(getActivity(), error.getResponse());
////                            if (dialog.shouldBeShown())
////                            {
////                                dialog.show();
////                            }
////                            getActivity().finish();
////                        }
////                        catch (Throwable throwable)
////                        {
////                        }
////                    }
////                }
//            }
//        });
//    }
//
//    @Override
//    public void onQueryTextSubmit(String query) {
//    }
//
//    @Override
//    public void onSearch(final String text) {
//        if (text.length() >= 3) {
//            if (mItunesRecyclerAdapter != null)
//                mItunesRecyclerAdapter.clearData();
//            mNoResults.setVisibility(View.GONE);
//            mChosenProgerss.setVisibility(View.VISIBLE);
//            RestClientiTunes.getInstance().getUrlService().searchiTunes(text, "music", new Callback<CAAiTunes>() {
//                @Override
//                public void success(CAAiTunes caAiTunes, Response response) {
//                    retries = 0;
//                    if (!caAiTunes.getResultCount().equalsIgnoreCase("") && Integer.parseInt(caAiTunes.getResultCount()) > 0)
//                        prepareData(caAiTunes.getResults());
//                    else if (isVisible) {
//                        mChosenProgerss.setVisibility(View.GONE);
//                        mNoResults.setVisibility(View.VISIBLE);
//                        if (mItunesRecyclerAdapter != null && mItunesRecyclerAdapter.getItemCount() > 0)
//                            mItunesRecyclerAdapter.clearData();
//                    }
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                    if (retries < 4) {
//                        onSearch(text);
//                        retries++;
//                    } else {
//                        try {
//                            NetworkConnectivityListener.isConnectedWithDialog(getActivity());
//                            getActivity().finish();
//                        } catch (Throwable throwable) {
//                        }
//                    }
//                }
//            });
//        }
//    }
//
//    @Override
//    public void onSearchCollapse(boolean isCollapse) {
//        if (isCollapse) {
//            mNoResults.setVisibility(View.GONE);
//            if (songs != null && songs.size() > 0) {
//                mItunesRecyclerAdapter.clearData();
//                if (mDataSet == null)
//                    mDataSet = new ArrayList<>();
//                mDataSet.clear();
//                for (int i = 0; i < songs.size(); i++) {
//                    mDataSet.add(i, songs.get(i));
//                }
//                prepareData(mDataSet);
//            } else {
//                getSongs();
//            }
//        }
//    }
//
//    @Override
//    public void stopMediaPlayer() {
//        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
//            mMediaPlayer.stop();
//            mMediaPlayer.reset();
//            mMediaPlayer = null;
//            if (lastView != null)
//                ((ImageView) lastView).setImageResource(R.drawable.sound_play_36);
//            lastView = null;
//        }
//    }
//
//    @Override
//    public String getFragmentTag() {
//        return Constants.SOUNDTRACK_FRAGMENT;
//    }
//
//    public interface SoundTrackListener {
//        void onSongClicked();
//    }
//
//    private void prepareData(ArrayList<CAAItunesMusicElement> data) {
//        if (isVisible) {
//            mChosenProgerss.setVisibility(View.GONE);
//            mNoResults.setVisibility(View.GONE);
//            mItunesRecyclerAdapter = new ItunesRecyclerAdapter(data, getImageLoader(), mItemClickListener, mPlayPauseClickListener);
//            mRecycleTrackList.setAdapter(mItunesRecyclerAdapter);
//        }
//    }
//
//    private ImageLoaderManager getImageLoader() {
//        return ((AddSoundActivity) getActivity()).getImageLoaderManager();
//    }
//
//    public void MusicPrepare(String url, final ProgressBar progressBar) {
//        isLoadingMusic = true;
//        // Set type to streaming
//        showProgressBar(progressBar);
//        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//        // Listen for if the audio file can't be prepared
//        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
//            @Override
//            public boolean onError(MediaPlayer mp, int what, int extra) {
//                // ... react appropriately ...
//                // The MediaPlayer has moved to the Error state, must be reset!
//                return false;
//            }
//        });
//        // Attach to when audio file is prepared for playing
//        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                hideProgressBar(progressBar);
//                mp.start();
//                ((ImageView) lastView).setImageResource(R.drawable.sound_pause_36);
//                isLoadingMusic = false;
//            }
//        });
//        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mediaPlayer) {
//                ((ImageView) lastView).setImageResource(R.drawable.sound_play_36);
//                isLoadingMusic = false;
//            }
//        });
//        // Set the data source to the remote URL
//        try {
//            mMediaPlayer.setDataSource(url);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        // Trigger an async preparation which will file listener when completed
//        mMediaPlayer.prepareAsync();
//    }
//
//    public void showProgressBar(ProgressBar progressBar) {
//        lastView.setVisibility(View.INVISIBLE);
//        progressBar.setVisibility(View.VISIBLE);
//    }
//
//    public void hideProgressBar(ProgressBar progressBar) {
//        progressBar.setVisibility(View.GONE);
//        lastView.setVisibility(View.VISIBLE);
//    }
//}
