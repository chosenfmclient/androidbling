package fm.bling.blingy.utils.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;

import java.util.ArrayList;

import fm.bling.blingy.R;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/6/16.
 * History:
 * ***********************************
 */
public class WhiteLoadingPlaceHolder {
    private Bitmap bitmap;
    private Paint paint;
    private int centerX, centerY;
    private Matrix matrix;
    private ArrayList<View> attachedViews;
    private boolean run;
    private final long fps; // frames per second
    private final int rotationDegrees;


    /**
     * @param context
     * @param width   the width  of the progress bitmap
     * @param height  the Height of the progress bitmap
     * @param locX    X Location in the parent
     * @param locY    Y Location in the parent
     *                //     * @param fps       Frames per second ( max 30  min 10)
     */
    public WhiteLoadingPlaceHolder(Context context, int width, int height, int locX, int locY) {
        int fps = 30;

        this.fps = 1000 / fps;
        rotationDegrees = ((fps + 10) / 2); // -20 = -30 + 10(minimum fps)

        if (width == 0 || height == 0)
            width = height = 150;

        Bitmap temp = BitmapFactory.decodeResource(context.getResources(), R.drawable.extra_large_ic_loading);
        bitmap = Bitmap.createScaledBitmap(temp, width, height, true);
        if (bitmap != temp)
            temp.recycle();

        paint = new Paint();
        paint.setAntiAlias(true);

        matrix = new Matrix();
        matrix.setRectToRect(new RectF(0, 0, width, height), new RectF(locX, locY, locX + width, locY + height), Matrix.ScaleToFit.CENTER);

        this.centerX = locX + (width / 2);
        this.centerY = locY + (height / 2);
        attachedViews = new ArrayList<>();
    }

    public void draw(Canvas canvas) {
        try {
            canvas.drawBitmap(bitmap, matrix, paint);
        } catch (Throwable er) {

        }
    }

    public void attachView(View viewToAttach) {
        synchronized (attachedViews) {
            attachedViews.add(viewToAttach);
            if (!run)
                new WhiteLoadingPlaceHolder.refreshThread();
        }
    }

    public void detachView(View viewToDetach) {
        try {
            synchronized (attachedViews) {
                attachedViews.remove(viewToDetach);
                if (attachedViews.size() == 0)
                    run = false;
            }
        } catch (Throwable err) {
        }
    }

    public void close() {
        run = false;
        try {
            bitmap.recycle();
        } catch (Throwable er) {
        }
        bitmap = null;
        paint = null;
        matrix = null;
        try {
            attachedViews.clear();
        } catch (Throwable er) {
        }
        attachedViews = null;
    }

    private class refreshThread extends Thread {

        public refreshThread() {
            run = true;
            start();
        }

        public void run() {
            try {
                while (run) {
                    for (View loopView : attachedViews) {
                        loopView.postInvalidate();
                    }
                    matrix.postRotate(rotationDegrees, centerX, centerY);
                    Thread.sleep(fps);
                }
            } catch (Throwable err) {
            }
        }
    }

}