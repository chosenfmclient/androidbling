package fm.bling.blingy.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;

import fm.bling.blingy.R;
import fm.bling.blingy.WebViewActivity;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.views.ClickableSpanExtension;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 05/01/16.
 * History:
 * ***********************************
 */
public class AcceptPrivacyDialog extends Dialog
{
    private TableLayout main;
    private Context mContext;
    private TextView title;
    private TextView content;
    private Button iAgreeButton;

    public AcceptPrivacyDialog(Context context)
    {
        super(context, android.R.style.Theme_Panel);
        mContext = context;

        int padding = context.getResources().getDimensionPixelSize(R.dimen.blocks_margin_card_side);

        title = new TextView(context);
        title.setLayoutParams(new ViewGroup.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
        title.setText("Terms of use");
        title.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(R.dimen.font_title_medium) );
        title.setTextColor(context.getResources().getColor(R.color.black));
        title.setPadding(padding, padding, padding, padding);
        title.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);
        title.setGravity(Gravity.CENTER);
        title.setIncludeFontPadding(false);

        content = new TextView(context);
        content.setLayoutParams(new ViewGroup.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
        content.setText(R.string.terms_and_condition, TextView.BufferType.EDITABLE);
        content.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(R.dimen.font_description));
        content.setTextColor(context.getResources().getColor(R.color.black));
        content.setPadding(padding, padding, padding, padding*2);
        content.setGravity(Gravity.LEFT);
        content.setIncludeFontPadding(false);
        content.setMovementMethod(LinkMovementMethod.getInstance());
        content.setHighlightColor(Color.TRANSPARENT);

        SpannableStringBuilder ssb = (SpannableStringBuilder) content.getEditableText();
        String str = ssb.toString();

        int ind;
        ssb.setSpan(new ClickableSpanExtension()
        {
            @Override
            public void onClick(View widget)
            {
                startTermsOfService();
            }
        }, ind = str.indexOf("Terms of Service"), ind + "Terms of Service".length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.main_blue)), str.indexOf("Terms of Service"), ind + "Terms of Service".length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);

        ssb.setSpan(new ClickableSpanExtension()
        {
            @Override
            public void onClick(View widget)
            {
                startPrivacy();
            }
        }, ind = str.indexOf("Privacy Policy"), ind + "Privacy Policy".length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.main_blue)), str.indexOf("Privacy Policy"), ind + "Privacy Policy".length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);

        iAgreeButton = new Button(context);
        iAgreeButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        iAgreeButton.setGravity(Gravity.CENTER);
        iAgreeButton.setText("I agree");
        iAgreeButton.setBackgroundResource(R.drawable.button_dialog_blue);//Color(mContext.getResources().getColor(R.color.main_blue));
        iAgreeButton.setTextColor(Color.WHITE);
        iAgreeButton.setPadding(padding * 3, 0, padding*3, 0);
        iAgreeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onIAgreeClicked();
            }
        });

        main = new TableLayout(context);
        main.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        main.setBackgroundColor(context.getResources().getColor(R.color.white));
        main.setPadding(padding, padding * 2, padding, padding * 2);
        main.addView(title);
        main.addView(content);
        main.addView(iAgreeButton);
        main.setGravity(Gravity.CENTER);

        setContentView(main);
    }

    private void startTermsOfService()
    {
        Intent intent = new Intent(mContext, WebViewActivity.class);
        intent.putExtra("title", "Terms Of Service");
        intent.putExtra("url", mContext.getResources().getString(R.string.terms_url));
        mContext.startActivity(intent);
    }

    private void startPrivacy()
    {
        Intent intent = new Intent(mContext, WebViewActivity.class);
        intent.putExtra("title", "Privacy Policy");
        intent.putExtra("url", mContext.getResources().getString(R.string.privacy_url));
        mContext.startActivity(intent);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        System.exit(0);
    }

    private void onIAgreeClicked()
    {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(mContext.getResources().getString(R.string.user_shared_prefs), Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Constants.PRIVACY_SHOWN, true);
        editor.apply();

        dismiss();
    }

}