package fm.bling.blingy.homeScreen.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.homeScreen.model.CAANotification;
import fm.bling.blingy.utils.AdaptersDataTypes;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderListener;
import fm.bling.blingy.utils.imagesManagement.MultiImageLoader;
import fm.bling.blingy.utils.imagesManagement.views.ProgressBarView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;


/**
 * Created by Ben Levi on 03/17/2016.
 */
public class NotificationsRecyclerAdapter extends RecyclerView.Adapter<NotificationsRecyclerAdapter.DataObjectHolder> {





    public static final int TYPE_VIDEO = 2;
    public static final int TYPE_PHOTO = 1;
    public static final int TYPE_USER = 0;
    public static final int TYPE_LOADING = -1;

    private List<CAANotification> baseList;
    private Activity mContext;
    private MultiImageLoader multiImageLoader;
    private ImageLoaderListener imageLoaderListener;
    private View.OnClickListener mOnClickListener;
    private ProgressBarView chosenProgressBar;

    public NotificationsRecyclerAdapter(Activity context, List<CAANotification> items, View.OnClickListener onClickListener) {
        baseList = items;
        mContext = context;
        multiImageLoader = new MultiImageLoader(context);
        mOnClickListener = onClickListener;
        init();
    }

    public NotificationsRecyclerAdapter(Activity context, View.OnClickListener onClickListener) {
        mContext = context;
        multiImageLoader = new MultiImageLoader(context);
        mOnClickListener = onClickListener;
        init();
    }

    private void init() {
        imageLoaderListener = new ImageLoaderListener() {
            @Override
            public void onImageLoaded(URLImageView urlImageView) {
                try {
                    if (urlImageView.getParent() instanceof LinearLayout)
                        ((LinearLayout) urlImageView.getParent()).postInvalidateDelayed(500);
                    else if (urlImageView.getParent() instanceof FrameLayout)
                        ((FrameLayout) urlImageView.getParent()).postInvalidateDelayed(500);
                    urlImageView.postInvalidateDelayed(500);
                } catch (Throwable err) {
                }
            }
        };
    }

    public void setData(List<CAANotification> items) {
        baseList = items;
        notifyDataSetChanged();
    }

    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return baseList == null ? 0 : baseList.size();
    }

    public int getItemViewTypeInternal(int position) {
        return baseList.get(position).getAdapterType();
    }

    @Override
    public NotificationsRecyclerAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int position) {
        int type = getItemViewTypeInternal(position);
        DataObjectHolder holder;
        switch (type) {
            case TYPE_VIDEO: {
                LinearLayout rootView = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.video_preview_item2, parent, false);
                rootView.setTag(position);
                rootView.setOnClickListener(mOnClickListener);

                holder = new DataObjectHolder(rootView);
                holder.type = type;


                holder.textTitle = (TextView) rootView.findViewById(R.id.text_view_video_info_2);
                holder.textTitle.setMaxLines(3);

                holder.textInfoPrimary = (TextView) rootView.findViewById(R.id.text_view_video_info_3);

                holder.profileImage = (URLImageView) rootView.findViewById(R.id.videoHome_button_playPause);
                holder.profileImage.setImageLoaderListener(imageLoaderListener);
                holder.profileImage.setAnimResID(R.anim.fade_in);
                holder.profileImage.setAutoShowRecycle(true);

                break;
            }
            case TYPE_PHOTO: {
                LinearLayout rootView = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.video_preview_item2, parent, false);
                rootView.setTag(position);
                rootView.setOnClickListener(mOnClickListener);

                holder = new DataObjectHolder(rootView);
                holder.type = type;

                holder.textTitle = (TextView) rootView.findViewById(R.id.text_view_video_info_2);
                holder.textTitle.setMaxLines(3);

                holder.textInfoPrimary = (TextView) rootView.findViewById(R.id.text_view_video_info_3);

                holder.profileImage = (URLImageView) rootView.findViewById(R.id.videoHome_button_playPause);
                holder.profileImage.setImageLoaderListener(imageLoaderListener);
                holder.profileImage.setAutoShowRecycle(true);

                holder.profileImage.setAnimResID(R.anim.fade_in);
                break;
            }
            case TYPE_USER: {
                LinearLayout rootView = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.user_type_preview_item2, parent, false);

                rootView.setTag(position);
                rootView.setOnClickListener(mOnClickListener);

                holder = new DataObjectHolder(rootView);
                holder.type = type;

                holder.textInfoPrimary = (TextView) rootView.findViewById(R.id.text_view_user_info_primary);

                holder.textTitle = (TextView) rootView.findViewById(R.id.text_view_user_info_secondary);
                holder.textTitle.setMaxLines(3);

                holder.profileImage = (URLImageView) rootView.findViewById(R.id.image_view_profile);
                holder.profileImage.setImageLoaderListener(imageLoaderListener);
                holder.profileImage.setAnimResID(R.anim.fade_in);
                holder.profileImage.setAutoShowRecycle(true);


                break;
            }
            case TYPE_LOADING: {
                LinearLayout rootView = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.user_type_preview_item2, null);
                holder = new DataObjectHolder(rootView);
                holder.type = type;

                holder.textInfoPrimary = (TextView) rootView.findViewById(R.id.text_view_user_info_primary);

                holder.textTitle = (TextView) rootView.findViewById(R.id.text_view_user_info_secondary);
                holder.textTitle.setMaxLines(3);

                holder.profileImage = (URLImageView) rootView.findViewById(R.id.image_view_profile);
                holder.profileImage.setImageLoaderListener(imageLoaderListener);
                holder.profileImage.setAnimResID(R.anim.fade_in);
                holder.profileImage.setAutoShowRecycle(true);

                break;
            }
            default: {
                LinearLayout rootView = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.user_type_preview_item2, null);
                holder = new DataObjectHolder(rootView);
                holder.type = type;

                holder.textInfoPrimary = (TextView) rootView.findViewById(R.id.text_view_user_info_primary);

                holder.textTitle = (TextView) rootView.findViewById(R.id.text_view_user_info_secondary);
                holder.textTitle.setMaxLines(3);

                holder.profileImage = (URLImageView) rootView.findViewById(R.id.image_view_profile);
                holder.profileImage.setImageLoaderListener(imageLoaderListener);
                holder.profileImage.setAnimResID(R.anim.fade_in);
                holder.profileImage.setAutoShowRecycle(true);

                break;
            }
            // anim = AnimationUtils.loadAnimation(getContext(), R.anim.spinner_rotate);
            // convertView = inflater.inflate(R.layout.loading_list_layout, null);
        }
        holder.profileImage.setImageLoader(multiImageLoader);
        return holder;
    }

    @Override
    public void onBindViewHolder(NotificationsRecyclerAdapter.DataObjectHolder holder, int position) {
        int type = getItemViewTypeInternal(position);
        if (type != holder.type)//ActivitiesListAdapter.TYPE_LOADING && position != baseList.size())
        {
            new notifyThread(position);
            return;
        }
        switch (type) {

            case TYPE_VIDEO: {
                holder.profileImage.setIsRoundedImage(false);
                holder.profileImage.setPlaceHolder(R.drawable.placeholder_thumb);
                holder.profileImage.setKeepAspectRatioAccordingToWidth(false);
                break;
            }
//            case TYPE_PHOTO: {
//                holder.profileImage.setIsRoundedImage(false);
//                holder.profileImage.setPlaceHolder(R.drawable.placeholder_thumb);
//                holder.profileImage.setKeepAspectRatioAccordingToWidth(false);
//                break;
//            }
            case TYPE_USER: {
                holder.profileImage.setIsRoundedImage(true);
                holder.profileImage.setKeepAspectRatioAccordingToWidth(false);
                holder.profileImage.setPlaceHolder(R.drawable.extra_large_userpic_placeholder);
                break;
            }
        }

        if (holder.type >= 0) {
            CAANotification notification = baseList.get(position);
            if (notification.getStatus().equalsIgnoreCase(AdaptersDataTypes.READ_STATUS) || notification.getStatus().equalsIgnoreCase(AdaptersDataTypes.UNREAD_STATUS)) {
                if (isPinkBackground(notification.getType())) {
                    holder.rootView.setBackground(mContext.getResources().getDrawable(R.drawable.notification_ripple_light_pink_background));

                } else {
                    holder.rootView.setBackground(mContext.getResources().getDrawable(R.drawable.notification_ripple_gray_background));
                }
            } else {
                holder.rootView.setBackground(mContext.getResources().getDrawable(R.drawable.notification_ripple_white_background));
            }

            holder.textTitle.setText(baseList.get(position).getAdapterText());
            if (holder.profileImage != null)
                holder.profileImage.setTag(baseList.get(position).getAdapterText().toString());

            holder.textInfoPrimary.setTypeface(App.ROBOTO_REGULAR);


                holder.textInfoPrimary.setTextColor(mContext.getResources().getColor(R.color.grey_font));



            setUpTime(holder, System.currentTimeMillis() / 1000 - Long.valueOf(notification.getDate()));

            if (holder.type != TYPE_LOADING) {
                String url = notification.getImage();
                if(url.contains("t=")){
                    int index = url.lastIndexOf('t');
                    if(index != -1){
                        url = url.substring(0, index - 1);
                    }
                }
                holder.profileImage.setUrl(url);
//                holder.profileImage.setUrl(notification.getImage());
                multiImageLoader.DisplayImage(holder.profileImage);
            }
        } else if (holder.type == TYPE_LOADING) {
            holder.rootView.setBackgroundColor(Color.parseColor("#FFFFFF"));
            holder.rootView.removeAllViews();
            holder.rootView.setGravity(Gravity.CENTER);
            holder.rootView.setMinimumHeight(mContext.getResources().getDimensionPixelSize(R.dimen.userpic_big));
            if (holder.rootView.getLayoutParams() == null)
                holder.rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            else holder.rootView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            if (chosenProgressBar == null) {
                chosenProgressBar = new ProgressBarView(mContext);
                chosenProgressBar.updateLayout();
            }
            if (chosenProgressBar.getParent() != null) {
                ((LinearLayout) chosenProgressBar.getParent()).removeView(chosenProgressBar);
            }
            holder.rootView.addView(chosenProgressBar);
            holder.rootView.requestLayout();
        }
    }

    private boolean isPinkBackground(String type) {
        return  type.equals(Constants.CAMEO_INVITE) ||
                type.equals(Constants.DUET_SUGGESTION) ||
                type.equals(Constants.DUET_REMINDER);
    }

    public void closeProgressBar() {
        try {
            chosenProgressBar.close();
        } catch (Throwable er) {
        }
        chosenProgressBar = null;
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        LinearLayout rootView;
        TextView textTitle;
        TextView textInfoPrimary;
        URLImageView profileImage;
        int type;

        public DataObjectHolder(LinearLayout itemView) {
            super(itemView);
            rootView = itemView;
        }
    }

    private void setUpTime(DataObjectHolder holder, long timeSpan) {
        int hour = (int) (timeSpan / 60 / 60);
        int minutes = (int) ((timeSpan / 60) % 60);
        int days = hour / 24;
        int weeks = hour / 24 / 7;
        if ((timeSpan) > 60) {
            if (hour / 24 == 0) {
                if (hour == 0) {
                    if (minutes == 1) holder.textInfoPrimary.setText(minutes + " min" + " ago");
                    else holder.textInfoPrimary.setText(minutes + " mins" + " ago");
                } else {
                    if (hour == 1) holder.textInfoPrimary.setText(hour + " hour" + " ago");
                    else holder.textInfoPrimary.setText(hour + " hours" + " ago");
                }
            } else {
                if (days <= 6) {
                    if (days == 1) holder.textInfoPrimary.setText(days + " day" + " ago");
                    else holder.textInfoPrimary.setText(days + " days" + " ago");
                } else {
                    if (weeks == 1) holder.textInfoPrimary.setText(weeks + " week" + " ago");
                    else holder.textInfoPrimary.setText(weeks + " weeks" + " ago");
                }
            }
        }
    }

    @Override
    public void onViewRecycled(DataObjectHolder holder) {
//        if(holder.profileImage != null)
//            mImageLoader.recycle(holder.profileImage);//holder.profileImage.recycle();//close();
//        if(holder.type == TYPE_LOADING)
//        {
//            holder.rootView.removeAllViews();
//        }
    }

    public int size() {
        return baseList.size();
    }


    public void close() {
        closeProgressBar();
        baseList = null;
        mContext = null;
        multiImageLoader.close();
        multiImageLoader = null;
        imageLoaderListener = null;
        mOnClickListener = null;
    }

    private class notifyChange implements Runnable {
        final int position;

        private notifyChange(int iposition) {
            position = iposition;
        }

        @Override
        public void run() {
            notifyItemChanged(position);
        }
    }

    private class notifyThread extends Thread {
        final int position;

        private notifyThread(int iposition) {
            position = iposition;
            start();
        }

        public void run() {
            try {
                Thread.sleep(200);
            } catch (Throwable err) {
            }
            mContext.runOnUiThread(new notifyChange(position));
        }

    }

}