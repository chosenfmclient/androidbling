package fm.bling.blingy.widget.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chosen-pro on 17/07/2016.
 */

public class CAAInvitePrize {
    @SerializedName("points")
    int points;

    @SerializedName("text")
    String text;

    public int getPoints() {
        return points;
    }

    public String getText() {
        return text;
    }
}
