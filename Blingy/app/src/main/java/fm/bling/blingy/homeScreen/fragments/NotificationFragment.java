package fm.bling.blingy.homeScreen.fragments;

import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.homeScreen.HomeScreenActivity;
import fm.bling.blingy.homeScreen.adapters.NotificationsRecyclerAdapter;
import fm.bling.blingy.homeScreen.model.CAAApiNotifications;
import fm.bling.blingy.homeScreen.model.CAANotification;

import fm.bling.blingy.inviteFriends.InviteFriendsActivity;
import fm.bling.blingy.leaderboard.view.LeaderboardActivity;

import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
import fm.bling.blingy.profile.ProfileActivity;
import fm.bling.blingy.record.rest.model.CAAItunesMusicElement;
import fm.bling.blingy.record.utils.SimpleDividerItemDecoration;
import fm.bling.blingy.rest.model.CAACommentParams;
import fm.bling.blingy.rest.model.CAAStatus;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.songHome.SongHomeActivity;
import fm.bling.blingy.songHome.fragments.ScrollToTopFragment;
import fm.bling.blingy.stateMachine.PermissionListener;
import fm.bling.blingy.videoHome.VideoHomeActivity;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.AdaptersDataTypes;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.videoHome.VideoSocialDetailsActivity;
import fm.bling.blingy.videoHome.model.CAAVideo;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ben Levi on March, 17, 2016
 */
public class NotificationFragment extends ScrollToTopFragment implements PermissionListener{
    private List<CAANotification> notifications;
    private int pageNumber = 0;
    private Boolean listInitialized = false;
    private Boolean calling = false;
    private Boolean endReached = false;

    private RecyclerView notificationList;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayout listViewEmpty;
    private NotificationsRecyclerAdapter notificationAdapter;

    private int previousLastItem = 0;
    private int noData = 0;
    private Handler mHandler;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mHandler == null)
            mHandler = new Handler();
        getNotification();
        sendNotificationsUpdated();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.notifications, null);

        synchronized (NotificationFragment.this) {
            notificationList = (RecyclerView) rootView.findViewById(R.id.list_view_notifications);
            linearLayoutManager = new LinearLayoutManager(NotificationFragment.this.getActivity(), LinearLayoutManager.VERTICAL, false);
            notificationList.setLayoutManager(linearLayoutManager);
            listViewEmpty = (LinearLayout) rootView.findViewById(R.id.notification_empty);
            notificationList.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext(), R.drawable.line_divider, false));

            if (notifications == null) {
                notificationAdapter = new NotificationsRecyclerAdapter(NotificationFragment.this.getActivity(), getClickListener());
                notificationList.setAdapter(notificationAdapter);
                listViewEmpty.setVisibility(View.GONE);
                notificationList.setVisibility(View.VISIBLE);
            } else if (notifications.size() == 0) {
                listViewEmpty.setVisibility(View.VISIBLE);
                notificationList.setVisibility(View.GONE);
            } else {
                notificationAdapter = new NotificationsRecyclerAdapter(NotificationFragment.this.getActivity(), notifications, getClickListener());
                notificationList.setAdapter(notificationAdapter);
                listViewEmpty.setVisibility(View.GONE);
                notificationList.setVisibility(View.VISIBLE);
            }
        }

        notificationList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (listInitialized) {
                    try {
                        final int lastItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (lastItem >= notificationAdapter.size() - 10 && !calling && !endReached) {
                            if (previousLastItem != lastItem) {
                                previousLastItem = lastItem;
                                getNextPage();
                            }
                        }
                    } catch (Throwable throwable) {
                    }
                }
            }
        });
        return rootView;
    }

    public View.OnClickListener getClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (int) view.getTag();
                final CAANotification notification = notifications.get(position);
                TrackingManager.getInstance().tapNotification(position, notification.getNotificationId(), notification.getType());
                if (NetworkConnectivityListener.isConnected())
                    sendSingleNotificationViewed(notification, new CAAStatus(AdaptersDataTypes.VIEWED_STATUS));

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Gson g = new Gson();
                        switch (notification.getType()) {
                            case Constants.USER_TYPE:
                                User newObj = null;
                                String userID = null;
                                try {
                                    newObj = g.fromJson(notification.getObject(), User.class);
                                    if (newObj != null)
                                        userID = newObj.getUserId();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if (userID == null)
                                    return;

                                Intent profileIntent = new Intent(NotificationFragment.this.getActivity(), ProfileActivity.class);
                                profileIntent.putExtra(Constants.USER_ID, userID);
                                startActivity(profileIntent);
                                break;
                            case Constants.CAMEO_INVITE:
                                CAAVideo video = null;
                                try {
                                    video = g.fromJson(notification.getObject(), CAAVideo.class);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                String videoID = video.getVideoId();
                                userID = video.getUser().getUserId();
                                Intent cameoInvite = new Intent(NotificationFragment.this.getActivity(), VideoHomeActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putParcelable(Constants.CAAVIDEO, video);

                                cameoInvite.putExtra(Constants.BUNDLE, bundle);
                                cameoInvite.putExtra(Constants.VIDEO_TYPE_EXTRA, video.getType());
                                cameoInvite.putExtra(Constants.USER_ID, userID);
                                cameoInvite.putExtra(Constants.VIDEO_ID, videoID);
                                cameoInvite.putExtra(Constants.FROM_INVITE, true);
                                startActivity(cameoInvite);

                                break;
                            case Constants.VIDEO_TYPE:
                                CAAVideo cv = null;
                                try {
                                    cv = g.fromJson(notification.getObject(), CAAVideo.class);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                String videoId = cv.getVideoId();
                                userID = cv.getUser().getUserId();
                                Intent videoHomeIntent = new Intent(NotificationFragment.this.getActivity(), VideoHomeActivity.class);

                                Bundle bundle1 = new Bundle();
                                bundle1.putParcelable(Constants.CAAVIDEO, cv);

                                videoHomeIntent.putExtra(Constants.BUNDLE, bundle1);

                                videoHomeIntent.putExtra(Constants.VIDEO_TYPE_EXTRA, cv.getType());
                                videoHomeIntent.putExtra(Constants.USER_ID, userID);
                                videoHomeIntent.putExtra(Constants.VIDEO_ID, videoId);


                                startActivity(videoHomeIntent);
                                break;
                            case Constants.COMMENT_TYPE:
                                CAACommentParams comment = null;
                                try {
                                    comment = g.fromJson(notification.getObject(), CAACommentParams.class);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                Intent videoHome = new Intent(getActivity(), VideoHomeActivity.class);
                                Intent videoComments = new Intent(getActivity(), VideoSocialDetailsActivity.class);
                                Intent homeScreenIntent = new Intent(getActivity(), HomeScreenActivity.class);

                                CAAVideo cvideo = comment.getVideo();

                                Bundle bundle2 = new Bundle();
                                bundle2.putParcelable(Constants.CAAVIDEO, cvideo);
                                videoComments.putExtra(Constants.BUNDLE, bundle2);

//                                videoComments.putExtra(Constants.CLIP_THUMBNAIL, cvideo.getFirstFrame());
//                                videoComments.putExtra(Constants.CLIP_NAME, cvideo.getSongName());
//                                videoComments.putExtra(Constants.VIDEO_ID, comment.getVideoId());
//                                videoComments.putExtra(Constants.MEDIA_TYPE, Constants.VIDEO_TYPE);

                                videoHome.putExtra(Constants.BUNDLE, bundle2);



                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity())
                                        .addNextIntent(homeScreenIntent)
                                        .addNextIntent(videoHome)
                                        .addNextIntent(videoComments);
                                stackBuilder.startActivities();
                                break;
                            case Constants.ADD_SOUND_TYPE:
                                CAAItunesMusicElement clip = null;
                                try {
                                    clip = g.fromJson(notification.getObject(), CAAItunesMusicElement.class);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Intent discover = new Intent(getActivity(), SongHomeActivity.class);
                                discover.putExtra(Constants.IS_SONG_HOME, true);
                                discover.putExtra(Constants.CLIP_URL, clip.getPreviewUrl());
                                discover.putExtra(Constants.CLIP_ID, clip.getClipId());
                                discover.putExtra(Constants.SONG_NAME, clip.getTrackName());
                                discover.putExtra(Constants.ARTIST_NAME, clip.getArtistName());
                                discover.putExtra(Constants.CLIP_THUMBNAIL, clip.getArtworkUrl100().replace("100X100", "600X600"));
                                discover.putExtra(Constants.TIME_SEGMENTS, clip.getmTimeSegments());
                                startActivity(discover);
                                break;
                            case Constants.LEADERBOARD:
                                Intent leaderIntent = new Intent(getActivity(), LeaderboardActivity.class);
                                startActivity(leaderIntent);
                                break;
                            case Constants.INVITE_TYPE :
                                if(((BaseActivity)getActivity()).getContactsPermissions(NotificationFragment.this))
                                    startActivity(new Intent(getActivity(), InviteFriendsActivity.class));
                                break;
                        }
                    }
                }, 250);
            }
        };
    }

    private synchronized void getNextPage() {
        if (calling)
            return;
        calling = true;
        pageNumber++;
        new Thread() {
            public void run() {
                App.getUrlService().getNotificationsPage(pageNumber, new Callback<CAAApiNotifications>() {
                    @Override
                    public void success(CAAApiNotifications caaApiNotifications, Response response) {
                        try {
                            int startRange = notifications.size();
//                            notifications.remove(startRange);
//                            notificationAdapter.notifyItemRemoved(startRange);

                            if (response.getStatus() != HttpURLConnection.HTTP_NO_CONTENT) {
                                if (caaApiNotifications.getData().size() < 1) {
                                    noData++;
                                    if (noData >= 2) {
                                        endReached = true;
                                    }
                                } else {
                                    noData = 0;
                                    setTypeToList(caaApiNotifications.getData());
                                    int previousItemCount = notifications.size();
                                    for (CAANotification notification : caaApiNotifications.getData()) {
                                        notifications.add(notification);
                                    }
//                                    CAANotification caaNotification;
//                                    notifications.add(caaNotification = new CAANotification());
//                                    caaNotification.setAdapterType(NotificationsListAdapter.TYPE_LOADING);
                                    notificationAdapter.notifyItemRangeInserted(previousItemCount, caaApiNotifications.getData().size());
                                }
                            } else {
                                endReached = true;
                                notificationAdapter.closeProgressBar();
                            }
                            notificationAdapter.notifyItemRangeChanged(startRange, notifications.size(), null);
                        } catch (Throwable err) {
                            err.printStackTrace();
                        }
                        calling = false;
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        calling = false;
                    }
                });
            }
        }.start();

    }

    private void setTypeToList(ArrayList<CAANotification> baseList) {
        for (CAANotification caaNotification : baseList) {
            caaNotification.setAdapterText(Html.fromHtml(caaNotification.getText()));

            if (caaNotification.getType() == null) {
                caaNotification.setAdapterType(NotificationsRecyclerAdapter.TYPE_LOADING);

            } else
                if (caaNotification.getType().equalsIgnoreCase(Constants.VIDEO_TYPE)) {
                    caaNotification.setAdapterType(NotificationsRecyclerAdapter.TYPE_VIDEO);
                }
                else if (caaNotification.getType().equalsIgnoreCase("rounded")) {
                caaNotification.setAdapterType(NotificationsRecyclerAdapter.TYPE_USER);
//                CAAVideo video = null;
//                Gson g = new Gson();
//                try {
//                    video = g.fromJson(caaNotification.getObject(), CAAVideo.class);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                if (video != null && video.getMediaType().equalsIgnoreCase(Constants.PHOTO_TYPE)) {
//                    caaNotification.setAdapterType(NotificationsRecyclerAdapter.TYPE_PHOTO);
//                } else {
//                    caaNotification.setAdapterType(NotificationsRecyclerAdapter.TYPE_VIDEO);
//                }
            }
        }
    }

    public void sendNotificationsUpdated() {
        App.getUrlService().putsUsersNotifications("", new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                if (NotificationFragment.this.getActivity() != null)
                    NotificationFragment.this.getActivity().setResult(android.app.Activity.RESULT_OK);
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private void sendSingleNotificationViewed(final CAANotification notification, CAAStatus status) {
        if (!notification.getStatus().equalsIgnoreCase(AdaptersDataTypes.VIEWED_STATUS)) {
            App.getUrlService().putsNotifications(notification.getNotificationId(), status, new Callback<String>() {

                @Override
                public void success(String s, Response response) {
                    try {
                        notification.setStatus(AdaptersDataTypes.VIEWED_STATUS);
                        notificationAdapter.notifyDataSetChanged();
                    } catch (Throwable throwable) {

                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }

            });
        }
    }

    private void getNotification() {
        App.getUrlService().getNotifications(new Callback<CAAApiNotifications>() {
            @Override
            public void success(CAAApiNotifications caaApiNotifications, Response response) {
                try {
                    if (isVisible()) {
                        synchronized (NotificationFragment.this) {
                            if (caaApiNotifications != null) {
                                setTypeToList(caaApiNotifications.getData());

                                notifications = caaApiNotifications.getData();
                                NotificationFragment.this.getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        synchronized (NotificationFragment.this) {
                                            if (notificationList != null) {
                                                if (notifications.size() == 0) {
                                                    listViewEmpty.setVisibility(View.VISIBLE);
                                                    notificationList.setVisibility(View.GONE);
                                                } else {
                                                    notificationAdapter.setData(notifications);
                                                    listViewEmpty.setVisibility(View.GONE);
                                                    notificationList.setVisibility(View.VISIBLE);
                                                    notificationList.invalidate();
                                                    notificationList.requestLayout();
                                                }
                                            }
                                            listInitialized = true;
                                        }
                                    }
                                });
                            } else {
                                if (notificationList != null) {
                                    listViewEmpty.setVisibility(View.VISIBLE);
                                    notificationList.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    @Override
    public void onDestroyView() {
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        super.onDestroyView();
    }

    public void close() {
        notificationList = null;
        listViewEmpty = null;
        linearLayoutManager = null;
        try {
            notificationAdapter.close();
        } catch (Throwable err) {
        }
        notificationAdapter = null;
    }

    @Override
    public void scrollToTop() {
        try {
            if (notificationList != null)
                notificationList.smoothScrollToPosition(0);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void onPermissionGranted() {
        startActivity(new Intent(getActivity(), InviteFriendsActivity.class));
    }

    @Override
    public void onPermissionDenied() {
    }
}
