package fm.bling.blingy.inviteFriends.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import fm.bling.blingy.rest.model.User;


/**
 * Created by Kiril on 7/1/2015.
 */
public class CAAApiContactsResponse {
    @SerializedName("data")
    private List<User> data = new ArrayList<User>();

    @SerializedName("emails_to_remove")
    private ArrayList<String> emailsToRemove;

    @SerializedName("twitter_ids_to_remove")
    private ArrayList<String> twitterIdsToRemove;

    public ArrayList<String> getEmailsToRemove() {
        return emailsToRemove;
    }

    public void setEmailsToRemove(ArrayList<String> emailsToRemove) {
        this.emailsToRemove = emailsToRemove;
    }


    public List<User> getData() {
        return data;
    }


    public void setData(List<User> data) {
        this.data = data;
    }

    public ArrayList<String> getTwitterIdsToRemove() {
        return twitterIdsToRemove;
    }

    public void setTwitterIdsToRemove(ArrayList<String> twitterIdsToRemove) {
        this.twitterIdsToRemove = twitterIdsToRemove;
    }
}
