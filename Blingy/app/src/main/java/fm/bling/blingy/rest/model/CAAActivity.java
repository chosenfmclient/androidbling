package fm.bling.blingy.rest.model;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import java.net.URL;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAActivity
 * Description:
 * Created by Dawidowicz Nadav on 5/6/15.
 * History:
 * ***********************************
 */
public class CAAActivity {

    @SerializedName("activity_id")
    private String activityId;

    @SerializedName("type")
    private String type;

    @SerializedName("date")
    private String date;

    @SerializedName("title")
    private String title;

    @SerializedName("text")
    private String text;

    @SerializedName("object")
    private JsonElement object;

    @SerializedName("image")
    private URL image;

    @SerializedName("image_type")
    private String imageType;

    private int adapterType;

    private CharSequence adapterText;

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityIOd(String activityId) {
        this.activityId = activityId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public URL getImage() {
        return image;
    }

    public void setImage(URL image) {
        this.image = image;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public JsonElement getObject() {
        return object;
    }

    public void setObject(JsonElement object) {
        this.object = object;
    }

    public int getAdapterType()
    {
        return adapterType;
    }

    public void setAdapterType(int adapterType)
    {
        this.adapterType = adapterType;
    }

    public CharSequence getAdapterText()
    {
        if(adapterText == null)
            return "";
        return adapterText;
    }

    public void setAdapterText(CharSequence adapterText)
    {
        this.adapterText = adapterText;
    }
}
