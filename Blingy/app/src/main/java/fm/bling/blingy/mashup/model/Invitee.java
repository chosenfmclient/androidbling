package fm.bling.blingy.mashup.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 1/9/17.
 * History:
 * ***********************************
 */
public class Invitee {

    @SerializedName("invitees")
    private ArrayList<InviteeContact> contacts;

    @SerializedName("video_id")
    private String videoID;

    public Invitee(ArrayList<InviteeContact> contacts, String videoID){
        this.contacts = contacts;
        this.videoID = videoID;
    }


}
