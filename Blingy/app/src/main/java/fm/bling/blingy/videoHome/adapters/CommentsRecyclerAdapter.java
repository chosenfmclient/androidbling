package fm.bling.blingy.videoHome.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fm.bling.blingy.R;
import fm.bling.blingy.rest.model.CAACommentParams;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.utils.imagesManagement.MultiImageLoader;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 9/6/16.
 * History:
 * ***********************************
 */
public class CommentsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<CAACommentParams> mDataset;
    private MultiImageLoader multiImageLoader;
    private View.OnClickListener mItemClickListener;
    private View.OnLongClickListener mItemLongClickListener;

    private final int TYPE_USER = 0;
    private final int TYPE_LOADING = -1;
    private Animation anim;

    public CommentsRecyclerAdapter(ArrayList<CAACommentParams> myDataset, View.OnClickListener itemClickListener,View.OnLongClickListener itemLongClickListener, Activity activity) {
        mDataset = myDataset;
        multiImageLoader = new MultiImageLoader(activity);
        mItemClickListener = itemClickListener;
        mItemLongClickListener = itemLongClickListener;
        anim = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.spinner_rotate);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
            switch (viewType) {
                case TYPE_LOADING:
                    View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_list_layout, parent, false);
                    holder = new SpinnerDataObjectHolder(view1);
                    break;
                case TYPE_USER:
                    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_type_item, parent, false);
                    holder = new DataObjectHolder(view);
                    break;
            }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof DataObjectHolder) {
            CAACommentParams caaCommentParams = mDataset.get(position);
            ((DataObjectHolder)holder).mItem.setTag(position);
            String username;
            if (caaCommentParams.getUser().getUserId().equalsIgnoreCase(CAAUserDataSingleton.getInstance().getUserId())) {
                username = "You";
            } else {
                username = caaCommentParams.getUser().getFullName();
            }
            ((DataObjectHolder)holder).mUserName.setText(username);
            ((DataObjectHolder)holder).mCommentText.setText(caaCommentParams.getText());

            ((DataObjectHolder)holder).mUserPic.setId(position);
            ((DataObjectHolder)holder).mUserPic.setUrl(caaCommentParams.getUser().getPhoto());
            ((DataObjectHolder)holder).mUserPic.setKeepAspectRatioAccordingToWidth(true);
            ((DataObjectHolder)holder).mUserPic.setIsRoundedImage(true);
            ((DataObjectHolder)holder).mUserPic.setImageLoader(multiImageLoader);
        }
        else{
            ((SpinnerDataObjectHolder)holder).mSpinnerView.setAnimation(anim);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mDataset.get(position).getCommentId() == null) {
            return TYPE_LOADING;
        } else {
            return TYPE_USER;//position;
        }
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        private LinearLayout mItem;
        private ScaledImageView mUserPic;
        private TextView mUserName;
        private TextView mCommentText;

        public DataObjectHolder(View itemView) {
            super(itemView);
            mItem = (LinearLayout)itemView.findViewById(R.id.user_type_item);

            mItem.setBackgroundResource(R.drawable.rectangle_transparent_white_ripple);

            mUserName = (TextView)itemView.findViewById(R.id.text_user_name);
            mCommentText = (TextView)itemView.findViewById(R.id.text_comment);

            mUserName.setTextColor(Color.WHITE);
            mCommentText.setTextColor(Color.WHITE);

            mUserPic = (ScaledImageView)itemView.findViewById(R.id.image_view_profile);
            mUserPic.setAnimResID(R.anim.fade_in);
            mUserPic.setAutoShowRecycle(true);
            mUserPic.setImageLoader(multiImageLoader);

            mItem.setOnClickListener(mItemClickListener);
            mItem.setOnLongClickListener(mItemLongClickListener);

        }
    }

    public class SpinnerDataObjectHolder extends RecyclerView.ViewHolder {
        private ImageView mSpinnerView;
        public SpinnerDataObjectHolder(View itemView) {
            super(itemView);
            mSpinnerView = (ImageView) itemView.findViewById(R.id.page_spinner);
        }
    }
}
