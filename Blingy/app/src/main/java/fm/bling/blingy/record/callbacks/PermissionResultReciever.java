package fm.bling.blingy.record.callbacks;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Zach on 6/3/16.
 * History:
 * ***********************************
 */
public interface PermissionResultReciever
{
    void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults);
}