package fm.bling.blingy.gcm;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.iid.FirebaseInstanceIdService;

import fm.bling.blingy.R;
import fm.bling.blingy.singletones.CAAUserDataSingleton;


/**
 * Created by Chosen-pro on 6/22/15.
 */
public class CAAGCMInstanceIDListenerService extends FirebaseInstanceIdService {

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    @Override
    public void onTokenRefresh() {

        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        SharedPreferences settings = getApplicationContext().getSharedPreferences(getApplicationContext().getResources().getString(R.string.user_shared_prefs), Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(getApplicationContext().getResources().getString(R.string.gcm_token), "");
        editor.apply();
        CAAUserDataSingleton.getInstance().setSentGcmToken(false);

        new CAAFCMRegistration().onHandleIntent(this);
    }

}
