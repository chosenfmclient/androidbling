package fm.bling.blingy.networkConnectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import fm.bling.blingy.dialogs.messages.NetworkDialog;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.utils.Constants;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 9/22/16.
 * History:
 * ***********************************
 */
public class NetworkConnectivityListener {
    private static final String TAG = "NetConnectivityListener";

    private Context mStartedContext;

    private Context mCurrentContext;

    private static String mState;
    private static String mReason;

    public static boolean mListening;
    private boolean mIsFailover;

    /** Network connectivity information */
    private static NetworkInfo mNetworkInfo;

    /**
     * In case of a Disconnect, the connectivity manager may have
     * already established, or may be attempting to establish, connectivity
     * with another network. If so, {@code mOtherNetworkInfo} will be non-null.
     */
    private NetworkInfo mOtherNetworkInfo;

    private ConnectivityBroadcastReceiver mReceiver;

    public static NetworkDialog mNetworkDialog;

    private class ConnectivityBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (!action.equals(ConnectivityManager.CONNECTIVITY_ACTION) || !mListening) {
                return;
            }

            try {
                mNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                mOtherNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);
            }catch (Throwable throwable){
                Log.d(TAG, throwable.toString());
            }

            mReason = intent.getStringExtra(ConnectivityManager.EXTRA_REASON);

            mIsFailover = intent.getBooleanExtra(ConnectivityManager.EXTRA_IS_FAILOVER, false);

            boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);

            if (noConnectivity) {
                mState = Constants.NOT_CONNECTED;
                showNotConnectionDialog();
            } else {
                mState = Constants.CONNECTED;
            }
        }
    }

    /**
     * Create a new NetworkConnectivityListener.
     */
    public NetworkConnectivityListener() {
        mState = Constants.UNKNOWN;
        mReceiver = new ConnectivityBroadcastReceiver();
    }

    /**
     * This method starts listening for network connectivity state changes.
     * @param context
     */
    public synchronized void startListening(Context context) {
        if (!mListening) {
            mStartedContext = context;
            IntentFilter filter = new IntentFilter();
            filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            context.getApplicationContext().registerReceiver(mReceiver, filter);
            mListening = true;
        }
    }

    /**
     * This method stops this class from listening for network changes.
     */
    public synchronized void stopListening() {
        if (mListening) {
            mStartedContext.getApplicationContext().unregisterReceiver(mReceiver);
            mStartedContext = null;
            mCurrentContext = null;
            mNetworkInfo = null;
            mOtherNetworkInfo = null;
            mIsFailover = false;
            mReason = null;
            mListening = false;
        }
    }

//    /**
//     * This methods registers a Handler to be called back onto with the specified what code when
//     * the network connectivity state changes.
//     *
//     * @param target The target handler.
//     * @param what The what code to be used when posting a message to the handler.
//     */
//    public void registerHandler(Handler target, int what) {
//        mHandlers.put(target, what);
//    }

//    /**
//     * This methods unregisters the specified Handler.
//     * @param target
//     */
//    public void unregisterHandler(Handler target) {
//        mHandlers.remove(target);
//    }

    public static synchronized String getState() {
        return mState;
    }

    /**
     * Return the NetworkInfo associated with the most recent connectivity event.
     * @return {@code NetworkInfo} for the network that had the most recent connectivity event.
     */
    public  NetworkInfo getNetworkInfo() {
        return mNetworkInfo;
    }

    /**
     * If the most recent connectivity event was a DISCONNECT, return
     * any information supplied in the broadcast about an alternate
     * network that might be available. If this returns a non-null
     * value, then another broadcast should follow shortly indicating
     * whether connection to the other network succeeded.
     *
     * @return NetworkInfo
     */
    public NetworkInfo getOtherNetworkInfo() {
        return mOtherNetworkInfo;
    }

    /**
     * Returns true if the most recent event was for an attempt to switch over to
     * a new network following loss of connectivity on another network.
     * @return {@code true} if this was a failover attempt, {@code false} otherwise.
     */
    public boolean isFailover() {
        return mIsFailover;
    }

    /**
     * An optional reason for the connectivity state change may have been supplied.
     * This returns it.
     * @return the reason for the state change, if available, or {@code null}
     * otherwise.
     */
    public String getReason() {
        return mReason;
    }


    public static boolean isConnected(){
        return getState() == Constants.CONNECTED;
    }

    private void showNotConnectionDialog(){
        if(mCurrentContext == null)
            return;

        if(Utils.isAirplaneModeOn(mCurrentContext)){
            if(mNetworkDialog == null || !mNetworkDialog.isShowing()) {
                mNetworkDialog = new NetworkDialog(mCurrentContext, "Please turn off airplane mode and try again.");
                mNetworkDialog.show();
            }
        }
        else{
            if(mNetworkDialog == null || !mNetworkDialog.isShowing()) {
                mNetworkDialog = new NetworkDialog(mCurrentContext);
                mNetworkDialog.show();
            }
        }
    }

    public void setCurrentContext(Context currentContext){
            this.mCurrentContext = currentContext;
    }

    public static boolean isConnectedWithDialog(Context context) {
        if(mListening && mNetworkInfo != null && !isConnected()){
            if(context == null)
                return false;

            if(Utils.isAirplaneModeOn(context)){
                if(mNetworkDialog == null || !mNetworkDialog.isShowing()) {
                    mNetworkDialog = new NetworkDialog(context, "Please turn off airplane mode and try again.");
                    mNetworkDialog.show();
                }
            }
            else{
                if(mNetworkDialog == null || !mNetworkDialog.isShowing()) {
                    mNetworkDialog = new NetworkDialog(context);
                    mNetworkDialog.show();
                }
            }
        }
        return true;
    }

}
