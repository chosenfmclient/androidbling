package fm.bling.blingy.inviteFriends;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.inviteFriends.listeners.InviteDataReadyListener;
import fm.bling.blingy.profile.fragments.BaseSearchFragment;
import fm.bling.blingy.profile.fragments.FollowingFragment;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 12/5/16.
 * History:
 * ***********************************
 */
public class InviteFriendsActivity extends BaseActivity implements InviteDataReadyListener {

    /**
     * Main Views
     */
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private InviteFriendsActivity.PagerAdapter mPagerAdapter;
    private ImageView spinner;
    private FrameLayout loadingLayout;
    private ObjectAnimator anim;

    private String mUserId;
    private int mNumOfFollowing;
    private boolean showContactsInilaized = false;

    private final int FOLLOWING = 0;
    private final int INVITE    = 1;

    private boolean isCameo = false;
    private String mVideoID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_tabs_layout);

        if(getIntent().hasExtra(Constants.TYPE))
            isCameo = getIntent().getStringExtra(Constants.TYPE).equals(Constants.CAMEO_TYPE);
        if(getIntent().hasExtra(Constants.VIDEO_ID))
            mVideoID = getIntent().getStringExtra(Constants.VIDEO_ID);

        if (InviteHandler.contacts == null)
                new InviteHandler(getApplicationContext(), this, true);
        else
            showContactsInilaized = true;

        loadingLayout = (FrameLayout) findViewById(R.id.loading_layout);
        ((FrameLayout.LayoutParams)loadingLayout.getLayoutParams()).topMargin = 2 * App.TOOLBAR_HEIGHT;

        spinner = (ImageView) findViewById(R.id.spinner);
        loadAnimation();
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        EditText mSearch = (EditText) findViewById(R.id.search_edit_text);
        ImageView backButton = (ImageView) findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if(isCameo) {
            TrackingManager.getInstance().pageView(EventConstants.CAMEO_INVITE_PAGE);
            backButton.setImageResource(R.drawable.ic_close_white_24dp);
            ((FrameLayout)(findViewById(R.id.main_container))).removeView(findViewById(R.id.main_bottom_bar));
            ((FrameLayout.LayoutParams)(findViewById(R.id.main_content)).getLayoutParams()).bottomMargin = 0;
        }

        mUserId = CAAUserDataSingleton.getInstance().getUserId();

        mTabLayout.addTab(mTabLayout.newTab().setText(isCameo ? "FOLLOWING" : "FOLLOW"));
        mTabLayout.addTab(mTabLayout.newTab().setText(isCameo ? "CONTACTS" : "INVITE"));
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mPagerAdapter != null) {
                    ((BaseSearchFragment) (mPagerAdapter.instantiateItem(mViewPager, FOLLOWING))).onSearch(s.toString());
                    ((BaseSearchFragment) (mPagerAdapter.instantiateItem(mViewPager, INVITE))).onSearch(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

        });


        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        if(showContactsInilaized)
            onInviteDataReady();
    }

    @Override
    public void onBackPressed() {
        if(mUserId.equalsIgnoreCase(Constants.ME)) {
            Intent intent = new Intent().putExtra(Constants.FOLLOWING_AMOUNT, mNumOfFollowing);
            setResult(Activity.RESULT_OK, intent);
        }
        finish();
    }

    public void cancelAnimation() {
        anim.cancel();
        loadingLayout.setVisibility(View.GONE);

    }

    private void loadAnimation() {
        loadingLayout.setVisibility(View.VISIBLE);
        anim = ObjectAnimator.ofFloat(spinner, "rotation", 0f, 359f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setDuration(1300);
        anim.setRepeatCount(Animation.INFINITE);
        anim.start();
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case FOLLOWING:
                    return FollowingFragment.newInstance(mUserId, !isCameo, InviteHandler.chosenContacts, isCameo, mVideoID);
                case INVITE:
                    return InviteFragment.newInstance(isCameo, mVideoID);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }
    }

    @Override
    public void onInviteDataReady() {
        if(InviteHandler.contacts != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (InviteHandler.chosenContacts != null)
                        mNumOfFollowing = InviteHandler.chosenContacts.size();

                    cancelAnimation();
                    mViewPager.setVisibility(View.VISIBLE);
                    mPagerAdapter = new InviteFriendsActivity.PagerAdapter(getSupportFragmentManager(), mTabLayout.getTabCount());
                    mViewPager.setAdapter(mPagerAdapter);
                }
            });
        }
    }

}
