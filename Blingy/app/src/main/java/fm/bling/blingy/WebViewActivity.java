package fm.bling.blingy;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import fm.bling.blingy.utils.dialogs.LoadingDialog;


/**
 * Created by Ben on 29/02/2016.
 */
public class WebViewActivity extends Activity
{
    private WebView webView;
    private LoadingDialog loadingDialog;
    private boolean isFirst;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        String url = getIntent().getStringExtra("url");

        if(url.indexOf("mailto") == 0)
        {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
            startActivity(Intent.createChooser(emailIntent, "Send Email"));
            finish();
            return;
        }

        webView = new WebView(this);
        webView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);

        webView.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                if(url.indexOf("mailto") == 0)
                {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
                    startActivity(Intent.createChooser(emailIntent, "Send Email"));
                    finish();
                    return true;
                }
                else return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {

                    super.onPageStarted(view, url, favicon);
                if(isFirst)
                {
                    loadingDialog = new LoadingDialog(WebViewActivity.this,false,false);
                    isFirst = false;
                }
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                if(url.indexOf("mailto") != 0)
                    super.onPageFinished(view, url);
                if (loadingDialog != null)
                {
                    loadingDialog.dismiss();
                    loadingDialog = null;
                }
            }
        });
        if (getIntent().getStringExtra("title") != null)
            setTitle(getIntent().getStringExtra("title"));

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);

        setContentView(webView);
    }

}