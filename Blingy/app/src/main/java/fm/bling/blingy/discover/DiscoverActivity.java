package fm.bling.blingy.discover;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.ScrollToTopBaseActivity;
import fm.bling.blingy.discover.fragments.DiscoverFragment;
import fm.bling.blingy.discover.fragments.SearchFragment;
import fm.bling.blingy.discover.fragments.SearchPeopleFragment;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 10/10/16.
 * History:
 * ***********************************
 */
public class DiscoverActivity extends ScrollToTopBaseActivity {

    /**
     * Main Views
     */
    private LinearLayout searchBarContainer;
    private EditText searchEditText;
    public URLImageView backgroundUrlImageView;

    /**
     * Menu Items
     **/
    private boolean isInDiscoverSearch = false;
    private String discoverSearchText = "";
    private SearchFragment searchFragment;
    private SearchPeopleFragment searchPeopleFragment;
    private DiscoverFragment discoverFragment;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private PagerAdapter pagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discovery_activity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_20));
        }
        getWindow().setBackgroundDrawable(null);
        init();
        moveToDiscoverFrame();
    }

    @Override
    public void onStart() {
        super.onStart();
        getMultiImageLodaer();

        if (backgroundUrlImageView != null)
            backgroundUrlImageView.setImageLoader(mMultiImageLoader);

        TrackingManager.getInstance().pageView(EventConstants.DISCOVER_PAGE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeKeyboard();
    }

    @Override
    public void onBackPressed() {
        closeKeyboard();
        if (isInDiscoverSearch || mTabLayout.getVisibility() == View.VISIBLE) {
            moveToDiscoverFrame();
            mTabLayout.setVisibility(View.GONE);
            searchBarContainer.setVisibility(View.GONE);
            pagerAdapter.notifyDataSetChanged();
        } else {
            super.onBackPressed();
        }
    }

    private void init() {
        searchBarContainer = (LinearLayout) findViewById(R.id.search_bar_container);
        searchEditText = (EditText) findViewById(R.id.search_edit_text);
        ((LinearLayout.LayoutParams) findViewById(R.id.search_bar_container).getLayoutParams()).topMargin = App.STATUS_BAR_HEIGHT;

        backgroundUrlImageView = (URLImageView) findViewById(R.id.background);

        backgroundUrlImageView.setBlurRadius(70);
        backgroundUrlImageView.setDontSaveToFileCache(true);
        backgroundUrlImageView.setDontUseExisting(true);
        backgroundUrlImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        backgroundUrlImageView.setPlaceHolder(R.drawable.game_thumb_placeholder);
        backgroundUrlImageView.setAnimResID(R.anim.fade_in);
        backgroundUrlImageView.setImageLoader(mMultiImageLoader);
        backgroundUrlImageView.setAutoShowRecycle(true);
        backgroundUrlImageView.enableMajorColorLayer(true);


        findViewById(R.id.cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditText.setText("");
                searchEditText.clearFocus();
                onBackPressed();
            }
        });

        findViewById(R.id.clear_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditText.setText("");
                openKeyboard(searchEditText);
            }
        });

        searchEditText.addTextChangedListener(new android.text.TextWatcher() {
            Timer timer;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {

                        String text = searchEditText.getText().toString();
                        if (text.length() > 2) {

                            searchFragment.onSearch(text);
                            searchPeopleFragment.onSearch(text);
                        }
                    }
                }, 600);

            }
        });

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchEditText.clearFocus();
                    DiscoverActivity.this.closeKeyboard();
                    return true;
                }

                return false;
            }
        });

        findViewById(R.id.cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        searchEditText.clearFocus();
        searchEditText.setFocusable(false);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);

        mTabLayout.addTab(mTabLayout.newTab().setText("VIDEOS"));
        mTabLayout.addTab(mTabLayout.newTab().setText("PEOPLE"));
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });


        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), 2);
        mViewPager.setAdapter(pagerAdapter);

        searchFragment = SearchFragment.newInstance();
        searchPeopleFragment = SearchPeopleFragment.newInstance();
    }

    private void moveToDiscoverFrame() {
        boolean shouldNotify = isInDiscoverSearch;

        isInDiscoverSearch = false;
        discoverSearchText = "";
        searchEditText.setText("");
        searchEditText.clearFocus();
        searchEditText.setFocusable(false);

        closeKeyboard();


        if (shouldNotify) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pagerAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void moveToDiscoverSearchFrame() {
        isInDiscoverSearch = true;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pagerAdapter.notifyDataSetChanged();
                searchFragment.onSearch(discoverSearchText);
            }
        });
    }

    @Override
    public void scrollToTop() {
        try {
            if (discoverFragment != null)
                discoverFragment.scrollToTop();
            if (searchFragment != null)
                searchFragment.scrollToTop();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(android.support.v4.app.FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            if (mTabLayout.getVisibility() != View.VISIBLE) {
//                if (discoverFragment == null)
                discoverFragment = DiscoverFragment.newInstance(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onSearchClicked();
                    }
                });
                return discoverFragment;
            }

            switch (position) {
                case 1:
//                    if (blingyUsers == null) {
//                        searchPeopleFragment = null;
//                        return //FollowingFragment.newInstance(CAAUserDataSingleton.getInstance().getUserId(), -1, true, null);
//                            searchPeopleFragment = FollowingFragment.newInstance(CAAUserDataSingleton.getInstance().getUserId(), -1, true, blingyUsers, false, mClipID);
//                    }
                    return searchPeopleFragment;

                case 0:
                    if (isInDiscoverSearch)
                        return searchFragment;
                    else {
                        if (discoverFragment == null)
                            discoverFragment = DiscoverFragment.newInstance(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    onSearchClicked();
                                }
                            });
                        return discoverFragment;
                    }

                default:
                    return null;

            }
        }

        @Override
        public int getCount() {
            if (mTabLayout.getVisibility() == View.VISIBLE)
                return mNumOfTabs;
            else return 1;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }
    }

    private void onSearchClicked() {
        if (backgroundUrlImageView.getUrl() == null && discoverFragment != null && discoverFragment.getClipThumbnail() != null
                && discoverFragment.getClipThumbnail().length() > 0) {
            backgroundUrlImageView.setUrl(discoverFragment.getClipThumbnail());
            mMultiImageLoader.DisplayImage(backgroundUrlImageView);
        }
        searchEditText.setFocusableInTouchMode(true);
        searchEditText.requestFocus();

        mTabLayout.setVisibility(View.VISIBLE);
        searchBarContainer.setVisibility(View.VISIBLE);

        moveToDiscoverSearchFrame();

        openKeyboard(searchEditText);
    }

    public int getSelfID() {
        return DISCVOER_SCREEN;
    }

}