package fm.bling.blingy.record.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.record.rest.model.CAAItunesMusicElement;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderListener;
import fm.bling.blingy.utils.imagesManagement.MultiImageLoader;
import fm.bling.blingy.utils.imagesManagement.views.LoadingImageView;
import fm.bling.blingy.utils.graphics.LoadingPlaceHolder;
import fm.bling.blingy.utils.imagesManagement.views.ProgressBarView;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 9/7/16.
 * History:
 * ***********************************
 */
public class PickMusicRecyclerAdapter extends RecyclerView.Adapter<PickMusicRecyclerAdapter.DataObjectHolder> {
    private ArrayList<CAAItunesMusicElement> mDataset;
    private MultiImageLoader mImageLoaderManager;
    private SparseArray<DataObjectHolder> holders;
    private View.OnClickListener clipSelectedOnClickListener, playPauseOnClickListener, moveToSongHomeListener;

    public PickMusicRecyclerAdapter(ArrayList<CAAItunesMusicElement> myDataset, MultiImageLoader imageLoaderManager,
                                    View.OnClickListener clipSelectedOnClickListener, View.OnClickListener playPauseOnClickListener, View.OnClickListener moveToSongHomeListener) {

        mDataset = myDataset;
        mImageLoaderManager = imageLoaderManager;
        holders = new SparseArray<>();

        this.clipSelectedOnClickListener = clipSelectedOnClickListener;
        this.playPauseOnClickListener = playPauseOnClickListener;
        this.moveToSongHomeListener = moveToSongHomeListener;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewIndex) {
        DataObjectHolder dataObjectHolder = holders.get(viewIndex);
        if (dataObjectHolder == null) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pick_layout_item, parent, false);
            dataObjectHolder = new DataObjectHolder(view);
            holders.put(viewIndex, dataObjectHolder);
        }
        return dataObjectHolder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        CAAItunesMusicElement caaItunesMusicElement = mDataset.get(position);

        if(position == 0){
            ((FrameLayout.LayoutParams)holder.mTileContainer.getLayoutParams()).topMargin = App.STATUS_BAR_HEIGHT + App.TOOLBAR_HEIGHT;
        }
        else{
            ((FrameLayout.LayoutParams)holder.mTileContainer.getLayoutParams()).topMargin = 0;
        }

        holder.mArtistName.setText(caaItunesMusicElement.getArtistName());
        holder.mClipName.setText(caaItunesMusicElement.getTrackName());

//        String image = get400X400Image(caaItunesMusicElement.getArtworkUrl100());
        String image = caaItunesMusicElement.getArtworkUrl100();

        holder.mClipImage.setUrl(image);
        holder.mClipImage.setTag(holder.mPlayPause);
        holder.mClipImage.setImageLoader(mImageLoaderManager);

        holder.mBluredImage.setUrl(image);
        holder.mBluredImage.setImageLoader(mImageLoaderManager);

        holder.mClipContainer.setTag(holder.mPlayPause);
        holder.mPlayPause.setTag(holder.itemView);
        holder.itemView.setTag(holder.mClipTextureView);
        holder.mClipTextureView.setTag(holder.mProgressBar);
        holder.mProgressBar.setTag(caaItunesMusicElement);
        holder.mSelectButton.setTag(caaItunesMusicElement);
        holder.mArtistName.setTag(caaItunesMusicElement);
        holder.mClipName.setTag(caaItunesMusicElement);

    }

    private String get400X400Image(String artworkUrl100) {
        return artworkUrl100.replace("100x100", "400x400");
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public void onViewRecycled(DataObjectHolder holder) {

    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView mArtistName;
        TextView mClipName;
        FrameLayout mClipContainer;
        TextureView mClipTextureView;
        ScaledImageView mClipImage;
        ScaledImageView mBluredImage;
        TextView mSelectButton;
        ImageView mPlayPause;
        ProgressBarView mProgressBar;
        LinearLayout mTileContainer;

        public DataObjectHolder(View itemView) {
            super(itemView);
            mArtistName = (TextView) itemView.findViewById(R.id.artist_name);
            mClipName = (TextView) itemView.findViewById(R.id.clip_name);
            mSelectButton = (TextView) itemView.findViewById(R.id.select_button);
            mSelectButton.setOnClickListener(clipSelectedOnClickListener);
            mClipImage = (ScaledImageView) itemView.findViewById(R.id.clip_image);
            mBluredImage = (ScaledImageView) itemView.findViewById(R.id.blured_image);
            mClipTextureView = (TextureView) itemView.findViewById(R.id.clip_texture_view);
            mProgressBar = (ProgressBarView) itemView.findViewById(R.id.progress_bar);
            mPlayPause = (ImageView) itemView.findViewById(R.id.play_pause);
            mClipContainer = (FrameLayout)itemView.findViewById(R.id.clip_container);
            mTileContainer = (LinearLayout) itemView.findViewById(R.id.home_tile_card);

            mProgressBar.updateLayout();

            mClipContainer.setOnClickListener(playPauseOnClickListener);

            mClipTextureView.getLayoutParams().height = App.landscapitemHeight;
            mClipImage.getLayoutParams().height = App.landscapitemHeight;

            mClipImage.setMaxWidthInternal(App.WIDTH);
            mClipImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            mClipImage.setAspectRatio(4f / 3f);
            mClipImage.setAnimResID(R.anim.fade_in);
            mClipImage.setAutoShowRecycle(true);

            mBluredImage.setBlurRadius(70);
            mBluredImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            mBluredImage.setAspectRatio(4f / 3f);
            mBluredImage.setPlaceHolder(R.drawable.game_thumb_placeholder);
            mBluredImage.enableMajorColorLayer(true);
            mBluredImage.setAutoShowRecycle(true);

            mClipName.setOnClickListener(moveToSongHomeListener);
            mArtistName.setOnClickListener(moveToSongHomeListener);
        }
    }

    public void setImageLoaderManager(MultiImageLoader multiImageLoader){
        this.mImageLoaderManager = multiImageLoader;
    }

    public void clearData() {
        int size = this.mDataset.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.mDataset.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

//    public void close() {
//        for (int i = 0; i < holders.size(); i++) {
//            holders.get(i).mClipImage.close();
//            holders.get(i).mBluredImage.close();
//            mImageLoaderManager.clearImageViews();
//        }
//    }
}
