package fm.bling.blingy.dialogs.listeners;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 8/1/16.
 * History:
 * ***********************************
 */
public interface FlagDialogListener {
    void onFinishFlagVideoDialog(boolean success);
}
