package fm.bling.blingy.inviteFriends.views;

import android.content.Context;
import android.graphics.Paint;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import fm.bling.blingy.App;
import fm.bling.blingy.inviteFriends.listeners.TabListener;
import fm.bling.blingy.inviteFriends.views.TabView;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/14/16.
 * History:
 * ***********************************
 */
public class TabsHost2 extends LinearLayout
{
    private TabListener mTabListener;
    private TabView[] tabs;

    public TabsHost2(Context context, String[]tabsName, Paint paint, int tabColor, TabListener tabListener)
    {
        super(context);
        mTabListener = tabListener;
        setBackgroundColor(tabColor);
        int tabWidth = App.WIDTH / 3;
        int tabHeight= (int) (140f * App.SCALE_X);//context.getResources().getDimensionPixelSize(R.dimen.font_title_medium) * 2;//
        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, tabHeight));
        tabs = new TabView[tabsName.length];
        for(int i=0; i < tabsName.length; i++)
        {
            addView( tabs[i] = new TabView(context, tabWidth, tabHeight, tabsName[i], i, paint, new TabListener()
            {
                @Override
                public void onTabSelected(String tabName, int index)
                {
                    if(tabs[index].isTabSelected())
                        return;
                    for(int i=0;i<tabs.length;i++)
                    {
                        tabs[i].setSelected(false);
                        tabs[i].invalidate();
                    }
                    tabs[index].setSelected(true);
                    tabs[index].invalidate();
                    mTabListener.onTabSelected(tabName, index);
                }
            }));
        }
        tabs[1].setSelected(true);
    }

    public void close()
    {
        removeAllViews();destroyDrawingCache();
        for(int i = 0; i < tabs.length; i++)
            try{tabs[i].close();tabs[i] = null;}catch(Throwable e){}tabs = null;
        mTabListener = null;
    }

    public void setTitle(String tabName, String newTitle)
    {
        for(int i=0; i<tabs.length; i++)
        {
            if(tabs[i].getTabName().equals(tabName))
            {
                tabs[i].setText(newTitle);
                break;
            }
        }
    }

}