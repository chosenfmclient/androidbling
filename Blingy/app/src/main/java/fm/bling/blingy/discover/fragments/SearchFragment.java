package fm.bling.blingy.discover.fragments;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Timer;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.discover.adapters.VideoRecyclerAdapter;
import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
import fm.bling.blingy.record.rest.model.CAAItunesMusicElement;
import fm.bling.blingy.record.rest.model.CAAiTunes;
import fm.bling.blingy.record.utils.SimpleDividerItemDecoration;
import fm.bling.blingy.songHome.SongHomeActivity;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.MultiImageLoader;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/3/16.
 * History:
 * ***********************************
 */
public class SearchFragment extends ScrollToTopFragment {

    /**
     * Main Views
     */
    private FrameLayout mEmptyFrame;
    private TextView mNoResult;
    private View mLine;
    private View.OnClickListener mItemClickListener;
    private Handler mHandler;
    private ImageView spinner;
    private ObjectAnimator anim;

    /**
     * Recycler
     */
    private RecyclerView mRecycleTrackList;
    private VideoRecyclerAdapter mRecyclerAdapter;

    private boolean isVisible = false;


    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        isVisible = true;
        if (mRecyclerAdapter != null && mRecycleTrackList != null)
            mRecyclerAdapter.notifyDataSetChanged();
        if (mHandler == null)
            mHandler = new Handler();
    }

    @Override
    public void onStop() {
        super.onStop();
        isVisible = false;
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.search_layout, null);
        init(v);
        return v;
    }

    public void init(View mainView) {
        mEmptyFrame = (FrameLayout) mainView.findViewById(R.id.empty_view);
        spinner = (ImageView) mainView.findViewById(R.id.spinner);
        mNoResult = (TextView) mainView.findViewById(R.id.no_search_results);
        mLine = mainView.findViewById(R.id.line);

        mRecycleTrackList = (RecyclerView) mainView.findViewById(R.id.search_recycler);
        mRecycleTrackList.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        mRecycleTrackList.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext(), R.drawable.line_divider));

        loadListeners();

    }

    private void loadListeners() {
        mItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CAAItunesMusicElement video = ((CAAItunesMusicElement) view.getTag());
                Intent discover = new Intent(getActivity(), SongHomeActivity.class);
                discover.putExtra(Constants.IS_SONG_HOME, true);
                discover.putExtra(Constants.CLIP_URL, video.getPreviewUrl());
                discover.putExtra(Constants.CLIP_THUMBNAIL, video.getArtworkUrl100());
                discover.putExtra(Constants.SONG_NAME, video.getTrackName());
                discover.putExtra(Constants.ARTIST_NAME, video.getArtistName());
                discover.putIntegerArrayListExtra(Constants.TIME_SEGMENTS, video.getmTimeSegments());
                getActivity().startActivity(discover);
            }
        };
    }

    private void prepareData(ArrayList<CAAItunesMusicElement> data) {
        if (isVisible) {
            showResults();
            mRecyclerAdapter = new VideoRecyclerAdapter(data, ((BaseActivity) getActivity()).getImageLoaderManager(), mItemClickListener);
            mRecycleTrackList.setAdapter(mRecyclerAdapter);
        }
    }

    public boolean onSearch(final String text) {
        if (text.length() >= 3 && getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mRecyclerAdapter != null)
                        mRecyclerAdapter.clearData();
                    showProgressBar();
                    TrackingManager.getInstance().searchDiscover("video", text);
                    App.getUrlService().getSearchVideo("clip", text, new Callback<CAAiTunes>() {
                        @Override
                        public void success(CAAiTunes caAiTunes, Response response) {
                            if (caAiTunes != null && caAiTunes.getResults() != null && caAiTunes.getResults().size() > 0)
                                prepareData(caAiTunes.getResults());
                            else if (isVisible) {
                                showNoResult();
                                if (mRecyclerAdapter != null && mRecyclerAdapter.getItemCount() > 0)
                                    mRecyclerAdapter.clearData();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            try {
                                NetworkConnectivityListener.isConnectedWithDialog(getActivity());
                                showNoResult();
                            } catch (Throwable throwable) {
                            }
                        }
                    });
                }
            });

            return true;
        } else return false;

    }

    private void showResults() {
        mRecycleTrackList.setVisibility(View.VISIBLE);
        mEmptyFrame.setVisibility(View.GONE);
    }

    private void showNoResult() {
        mRecycleTrackList.setVisibility(View.GONE);
        mEmptyFrame.setVisibility(View.VISIBLE);
        spinner.setVisibility(View.GONE);
        mNoResult.setVisibility(View.VISIBLE);
        mLine.setVisibility(View.VISIBLE);
        canelAnimation();

    }

    private void showProgressBar() {
        loadAnimation();
        mRecycleTrackList.setVisibility(View.GONE);
        mEmptyFrame.setVisibility(View.VISIBLE);
        mNoResult.setVisibility(View.GONE);
        mLine.setVisibility(View.GONE);
    }

    public void canelAnimation() {
        anim.cancel();
    }

    private void loadAnimation() {
        spinner.setVisibility(View.VISIBLE);
        anim = ObjectAnimator.ofFloat(spinner, "rotation", 0f, 359f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setDuration(1300);
        anim.setRepeatCount(Animation.INFINITE);
        anim.start();

    }

    @Override
    public void scrollToTop() {
        try {
            if (mRecycleTrackList != null)
                mRecycleTrackList.smoothScrollToPosition(0);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

}