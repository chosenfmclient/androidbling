package fm.bling.blingy.editVideo.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAPostVideos
 * Description:
 * Created by Dawidowicz Nadav on 5/12/15.
 * History:
 * ***********************************
 */
public class CAAPostVideos {

    public CAAPostVideos(String type, String parentId, String thumbnail, String url, String songID,
                          String songName, ArrayList<String> clip, String artistName, String frameId, ArrayList<CAAEffect> effects, String songClipUrl, String songImageUrl) {
        this.type = type;
        this.parentId = parentId;
        this.thumbnail = thumbnail;
        this.url = url;
        this.songID = songID;
        this.songName = songName;
        this.clip = clip;
        this.artistName = artistName;
        this.frameId = frameId;
        this.effects = effects;
        this.songClipUrl = songClipUrl;
        this.songImageUrl = songImageUrl;
    }

    public CAAPostVideos(String type, String parentId, String thumbnail, String url, String songID) {
        this.type = type;
        this.parentId = parentId;
        this.thumbnail = thumbnail;
        this.url = url;
        this.songID = songID;
    }



    @SerializedName("type")
    private String type;

    @SerializedName("parent_id")
    private String parentId;

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("url")
    private String url;

    @SerializedName("song_id")
    private String songID;

    @SerializedName("song_name")
    private String songName;

    @SerializedName("artist_name")
    private String artistName;

    @SerializedName("status")
    private String status;

    @SerializedName("effects")
    private ArrayList<CAAEffect> effects;

    @SerializedName("song_clip_url")
    private String songClipUrl; // The previewUrl of the iTunes clip's video file

    @SerializedName("song_image_url")
    private String songImageUrl; // The artworkUrl100 of the iTunes clip's image file



    /**
     *   0 - No frame
         1 - Jungle
         2 - Paws
         3 - Savanna
     */
    @SerializedName("frame")
    private String frameId;

    @SerializedName("clip")
    private ArrayList<String> clip;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSongName() {
        return songName;
    }

    public ArrayList<String> getClip() {
        return clip;
    }

    public String getArtistName() {
        return artistName;
    }

}
