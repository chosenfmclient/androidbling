package fm.bling.blingy.songHome.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fm.bling.blingy.R;
import fm.bling.blingy.record.rest.model.CAAItunesMusicElement;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/3/16.
 * History:
 * ***********************************
 */
public class VideoRecyclerAdapter extends RecyclerView.Adapter<VideoRecyclerAdapter.DataObjectHolder> {

    private ArrayList<CAAItunesMusicElement> mDataset;
    private ImageLoaderManager mImageLoaderManager;
    private SparseArray<VideoRecyclerAdapter.DataObjectHolder> mHolders;
    private View.OnClickListener mItemClickListener;

    public VideoRecyclerAdapter(ArrayList<CAAItunesMusicElement> myDataset, ImageLoaderManager imageLoaderManager, View.OnClickListener itemClickListener) {
        mDataset = myDataset;
        mImageLoaderManager = imageLoaderManager;
        mHolders = new SparseArray<>();
        mItemClickListener = itemClickListener;
    }

    @Override
    public VideoRecyclerAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewIndex) {
        VideoRecyclerAdapter.DataObjectHolder dataObjectHolder = mHolders.get(viewIndex);
        if(dataObjectHolder == null) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_video_recycler_item, parent, false);
            dataObjectHolder = new VideoRecyclerAdapter.DataObjectHolder(view);
            mHolders.put(viewIndex, dataObjectHolder);
        }
        return dataObjectHolder;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    @Override
    public void onBindViewHolder(VideoRecyclerAdapter.DataObjectHolder holder, int position) {
        CAAItunesMusicElement caaItunesMusicElement = mDataset.get(position);
        holder.mItem.setTag(caaItunesMusicElement);
        holder.mArtistName.setText(caaItunesMusicElement.getArtistName());
        holder.mSongName.setText(caaItunesMusicElement.getTrackName());
        holder.mArtistPic.setId(position);
        holder.mArtistPic.setUrl(caaItunesMusicElement.getArtworkUrl100());
        holder.mArtistPic.setScaleType(ImageView.ScaleType.CENTER_CROP);
        holder.mArtistPic.setFixedSize(true);
        mImageLoaderManager.DisplayImage(holder.mArtistPic);
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        private LinearLayout mItem;
        private TextView mArtistName;
        private TextView mSongName;
        private ScaledImageView mArtistPic;

        public DataObjectHolder(View itemView) {
            super(itemView);
            mItem = (LinearLayout)itemView.findViewById(R.id.song_item);
            mArtistPic = (ScaledImageView) itemView.findViewById(R.id.artist_pic);
            mArtistName = (TextView)itemView.findViewById(R.id.artist_name);
            mSongName = (TextView)itemView.findViewById(R.id.song_name);

            mArtistPic.setAnimResID(R.anim.fade_in);
            mItem.setOnClickListener(mItemClickListener);

        }
    }

    public void clearData() {
        int size = this.mDataset.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.mDataset.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void close() {
        for(int i = 0 ;i < mHolders.size(); i++)
            mHolders.get(i).mArtistPic.close();
        mDataset = null;
        mImageLoaderManager = null;
    }
}
