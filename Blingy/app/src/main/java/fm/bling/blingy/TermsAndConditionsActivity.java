package fm.bling.blingy;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;



/**
 * Created by LeadTheWay on 7/1/15.
 */
public class TermsAndConditionsActivity extends BaseActivity {

    TextView termsBody;

//    private WebView mWebView;
//    private Boolean printing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms);
        termsBody = (TextView)findViewById(R.id.termsBody);

        termsBody.setMovementMethod(LinkMovementMethod.getInstance());

        //Setting up the navigation icon.
        Toolbar toolbar = getActionBarToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
        toolbar.setNavigationContentDescription("backClose");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        MenuItem printAction = menu.findItem(R.id.action_print);
//        if (android.os.Build.VERSION.SDK_INT < 19) {
//            printAction.setVisible(false);
//        }
//        return super.onPrepareOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_privacy, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle item selection
//        switch (item.getItemId()) {
//            case R.id.action_print:
//                if (!printing) {
//                    doPrint();
//                    return true;
//                }
//                return false;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

//    private void doPrint() {
//        printing = true;
//        WebView webView = new WebView(this);
//        webView.setWebViewClient(new WebViewClient() {
//
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                return false;
//            }
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                printing = false;
//                createWebPrintJob(view);
//                mWebView = null;
//            }
//        });
//
//        // Generate an HTML document on the fly:
//        String htmlDocument = "<html><body>" + termsBody.getText() + "</body></html>";
//        webView.loadDataWithBaseURL(null, htmlDocument, "text/HTML", "UTF-8", null);
//
//        // Keep a reference to WebView object until you pass the PrintDocumentAdapter
//        // to the PrintManager
//        mWebView = webView;
//    }

//    private void createWebPrintJob(WebView webView) {
//
//        // Get a PrintManager instance
//        PrintManager printManager = (PrintManager) this
//                .getSystemService(Context.PRINT_SERVICE);
//
//        // Get a print adapter instance
//        PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();
//
//        // Create a print job with name and adapter instance
//        String jobName = getString(R.string.app_name) + " Document";
//        PrintJob printJob = printManager.print(jobName, printAdapter,
//                new PrintAttributes.Builder().build());
//
//        // Save the job object for later status checking
////        mPrintJobs.add(printJob);
//    }
}
