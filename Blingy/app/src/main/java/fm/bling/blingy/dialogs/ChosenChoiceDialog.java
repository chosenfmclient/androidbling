package fm.bling.blingy.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;


/**
 * Created by Zach.
 */
public abstract class ChosenChoiceDialog extends Dialog
{

    protected Context mContext;
    protected LinearLayout subMain;
    protected boolean dismissed = false;
    protected Handler mHandler;

    public ChosenChoiceDialog(Context context, int layout) {
        super(context, android.R.style.Theme_Panel);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        subMain = (LinearLayout) layoutInflater.inflate(layout, null);
        this.mHandler = new Handler();
        setContentView(subMain);
    }

    @Override
    public void show()
    {

        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        if(mContext instanceof BaseActivity)
            ((BaseActivity)mContext).setShowWidget(false);
        Animation slideIn = AnimationUtils.loadAnimation(mContext, R.anim.slide_up);
        slideIn.setDuration(150);
        slideIn.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                subMain.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }
        });

        super.show();
        subMain.startAnimation(slideIn);
    }

    public boolean onTouchEvent(MotionEvent me)
    {
        if (!dismissed)
        {
            if (me.getAction() == MotionEvent.ACTION_DOWN)
            {
                if (me.getY() < subMain.getTop())
                {
                    dismiss();
                }
            }
        }
        return true;
    }

    public void dismiss()
    {
        if (dismissed)
            return;
        dismissed = true;
        Animation slideOut = AnimationUtils.loadAnimation(mContext, R.anim.slide_down);
        slideOut.setDuration(150);
        slideOut.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                if(mContext instanceof BaseActivity)
                    ((BaseActivity)mContext).setShowWidget(true);
                subMain.setVisibility(View.INVISIBLE);
                superDismiss();
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }
        });
        subMain.startAnimation(slideOut);
    }

    protected void superDismiss()
    {
        super.dismiss();
    }

}
