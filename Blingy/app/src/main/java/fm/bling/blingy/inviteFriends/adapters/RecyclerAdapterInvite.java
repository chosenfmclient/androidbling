package fm.bling.blingy.inviteFriends.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.inviteFriends.listeners.ErrorListener;
import fm.bling.blingy.inviteFriends.listeners.InvalidateNotificationListener;
import fm.bling.blingy.inviteFriends.model.CAAEmailInvite;
import fm.bling.blingy.inviteFriends.model.CAAEmailInvites;
import fm.bling.blingy.inviteFriends.model.CAASmsInvite;
import fm.bling.blingy.inviteFriends.model.CAASmsInvites;
import fm.bling.blingy.inviteFriends.model.ContactInfo;
import fm.bling.blingy.mashup.listeners.CameoInviteListener;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.ClickableView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;
import fm.bling.blingy.widget.CAAWidgetSettingsSingleton;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/8/16.
 * History:
 * ***********************************
 */
public class RecyclerAdapterInvite extends RecyclerView.Adapter<RecyclerAdapterInvite.DataObjectHolder> {
    private ArrayList<ContactInfo> mDataset;
    private ImageLoaderManager mImageLoaderManager;
    private SparseArray<DataObjectHolder> holders;
    private View.OnClickListener tileMainOnClickListener;
    private InvalidateNotificationListener mInvalidateNotificationListener;

    private int imageWidthHeight = -1;
    private int statusImageWidthHeight;
    private Context mContext;
    private ErrorListener mErrorListener;
    private CameoInviteListener<ContactInfo> mCameoInviteListener;
    private boolean transparent;
    private boolean isCameo = false;

    public RecyclerAdapterInvite(Context context, ArrayList<ContactInfo> myDataset,
                                 ImageLoaderManager imageLoaderManager, InvalidateNotificationListener invalidateNotificationListener,
                                 ErrorListener errorListener, boolean transparent) {
        mDataset = myDataset;
        mContext = context;
        mErrorListener = errorListener;
        mImageLoaderManager = imageLoaderManager;
        holders = new SparseArray<>();
        mInvalidateNotificationListener = invalidateNotificationListener;

        imageWidthHeight = (int) (120f * App.SCALE_X);
        statusImageWidthHeight = (int) (80f * App.SCALE_X);
        tileMainOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inviteFriend(((ContactInfo) v.getTag()));
            }
        };
        this.transparent = transparent;
    }

    public RecyclerAdapterInvite(Context context, ArrayList<ContactInfo> myDataset, ImageLoaderManager imageLoaderManager, InvalidateNotificationListener invalidateNotificationListener, CameoInviteListener<ContactInfo> cameoInviteListener) {
        this.mDataset = myDataset;
        this.mContext = context;
        this.isCameo = cameoInviteListener != null;
        this.mCameoInviteListener = cameoInviteListener;
        this.mImageLoaderManager = imageLoaderManager;
        this.holders = new SparseArray<>();
        this.mInvalidateNotificationListener = invalidateNotificationListener;
        this.imageWidthHeight = (int) (120f * App.SCALE_X);
        this.statusImageWidthHeight = (int) (80f * App.SCALE_X);
        this.tileMainOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inviteFriend(((ContactInfo) v.getTag()));
            }
        };
        this.mErrorListener = null;
        this.transparent = false;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewIndex) {
        DataObjectHolder dataObjectHolder = holders.get(viewIndex);
        if (dataObjectHolder == null) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.invite_friend_contact_list_item, parent, false);
            dataObjectHolder = new DataObjectHolder(view);
            holders.put(viewIndex, dataObjectHolder);
        }
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        ContactInfo contactInfo = mDataset.get(position);
        holder.Name.setText(contactInfo.name);
        holder.mainView.setTag(contactInfo);
        if (contactInfo.isPhoneNumberSet())
            holder.address.setText(contactInfo.phone());
        else if (contactInfo.email != null)
            holder.address.setText(contactInfo.email);

        if (contactInfo.photoUrl != null) {
            holder.contactImage.setUrl(contactInfo.photoUrl);
            mImageLoaderManager.DisplayImage(holder.contactImage);
        } else {
            holder.contactImage.setImageResource(holder.contactImage.getPlaceHolder());
        }

        if(isCameo){
            if(contactInfo.cameoInvited == ContactInfo.NOT_INVITED)
                holder.statusImage.setBackgroundResource(R.drawable.ic_invite_friend_pink_24dp);
            else
                holder.statusImage.setBackgroundResource(R.drawable.ic_invite_friend_grey_24dp);
        }
        else{
            if (contactInfo.status == ContactInfo.NOT_INVITED) {

                holder.statusImage.setBackgroundResource(R.drawable.ic_invited_friend_green_24dp);
            } else {
                holder.statusImage.setBackgroundResource(R.drawable.ic_invite_friend_grey_24dp);
            }
        }


        if (transparent) {
            holder.address.setVisibility(View.GONE);
            holder.Name.setTextColor(mContext.getResources().getColor(R.color.white));
        }

    }

    public void setData(ArrayList<ContactInfo> data) {
        mDataset = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        View mainView;
        URLImageView contactImage;
        TextView Name, address;
        ClickableView statusImage;

        public DataObjectHolder(View itemView) {
            super(itemView);
            this.mainView = itemView;
            contactImage = (URLImageView) itemView.findViewById(R.id.invite_friend_contact_list_item_contact_image);
            Name = (TextView) itemView.findViewById(R.id.invite_friend_contact_list_item_contact_name);
            address = (TextView) itemView.findViewById(R.id.invite_friend_contact_list_item_contact_address);
            statusImage = (ClickableView) itemView.findViewById(R.id.invite_friend_contact_list_item_contact_status);
            mainView.setOnClickListener(tileMainOnClickListener);

            Name.setTypeface(App.ROBOTO_REGULAR);
            Name.getLayoutParams().width = App.WIDTH / 2;

            address.setTypeface(App.ROBOTO_REGULAR);
            address.getLayoutParams().width = App.WIDTH / 2;

            statusImage.setLayoutParams(new LinearLayout.LayoutParams(statusImageWidthHeight, statusImageWidthHeight));

            contactImage.setWidthInternal(imageWidthHeight);
            contactImage.setImageLoader(mImageLoaderManager);


            contactImage.setIsRoundedImage(true);
            contactImage.setLayoutWidthHeight(imageWidthHeight, imageWidthHeight);
            contactImage.setPlaceHolder(R.drawable.extra_large_userpic_placeholder);
        }
    }

    public void inviteFriend(final ContactInfo contactInfo) {
        if (contactInfo.status != ContactInfo.NOT_INVITED)
            return;

        if(isCameo){
            contactInfo.cameoInvited = ContactInfo.INVITED;
            if(mCameoInviteListener != null)
                mCameoInviteListener.addToInvite(contactInfo);
            if (mInvalidateNotificationListener != null)
                mInvalidateNotificationListener.invalidateNotification();
            showToastInvited(contactInfo.getName());
        }
        else{
            if (contactInfo.isPhoneNumberSet()) {
                ArrayList<CAASmsInvite> caaSmsInviteArrayList = new ArrayList<>();
                caaSmsInviteArrayList.add(new CAASmsInvite(contactInfo.phone(), contactInfo.getName()));
                CAASmsInvites caaSmsInvites = new CAASmsInvites(caaSmsInviteArrayList);
                App.getUrlService().postSmsInvite(caaSmsInvites, new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {
                        StateMachine.getInstance(mContext.getApplicationContext()).excludeFromInvites(contactInfo.phone(), StateMachine.INVITE_EXCLUDE_TYPE_PHONE);
                        contactInfo.status = ContactInfo.INVITED;
                        if (mInvalidateNotificationListener != null)
                            mInvalidateNotificationListener.invalidateNotification();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        mErrorListener.onError();
                    }
                });
            } else {
                CAAEmailInvite caaEmailInvite = new CAAEmailInvite(contactInfo.email, contactInfo.name);
                ArrayList<CAAEmailInvite> caaEmailInviteArrayList = new ArrayList<>();
                caaEmailInviteArrayList.add(caaEmailInvite);
                CAAEmailInvites caaEmailInvites = new CAAEmailInvites(caaEmailInviteArrayList, CAAUserDataSingleton.getInstance().getFullName());
                App.getUrlService().postEmailInvite(caaEmailInvites, new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {
                        StateMachine.getInstance(mContext.getApplicationContext()).excludeFromInvites(contactInfo.email, StateMachine.INVITE_EXCLUDE_TYPE_EMAIL);
                        contactInfo.status = ContactInfo.INVITED;
                        if (mInvalidateNotificationListener != null)
                            mInvalidateNotificationListener.invalidateNotification();
                        CAAWidgetSettingsSingleton.getInstance().addPoints();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                    }
                });
//            TrackingManager.getInstance().tapInviteInviteFriends("email");
            }
            //update invites count.
            int invteSent = SharedPreferencesManager.getInstance().getInt(EventConstants.INVITES_SENT, 0);
            invteSent++;
            SharedPreferencesManager.getEditor().putInt(EventConstants.INVITES_SENT, invteSent);

            TrackingManager.getInstance().inviteSend("selected", invteSent);
            StateMachine.getInstance(mContext.getApplicationContext()).insertEvent(
                    new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL,
                            StateMachineEvent.EVENT_TYPE.INVITE_FRIEND.ordinal()
                    )
            );
        }
    }

    private void showToastInvited(String name) {
        if(isCameo){
            Toast.makeText(mContext, "Invited " + name + " to your Duet!", Toast.LENGTH_SHORT).show();
        }
        else{
            //TODO show other msg
        }

    }

    @Override
    public void onViewRecycled(DataObjectHolder holder) {
        super.onViewRecycled(holder);
    }

    public void close() {
        for (int i = 0; i < holders.size(); i++) {
            holders.get(i).contactImage.close();
        }
        mDataset = null;
        mImageLoaderManager = null;
        holders = null;
        tileMainOnClickListener = null;
        mInvalidateNotificationListener = null;
    }

}