package fm.bling.blingy.utils.imagesManagement.views;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by Ben Levi on 1/25/2016.
 */
public class ClickableImageView extends ImageView
{
    private boolean hasShadow, pressed;
    private ShadowUtil shadowUtil;


    public ClickableImageView(Context context, int resourceID)
    {
        super(context);
        setBackgroundResource(resourceID);
    }

    public ClickableImageView(Context context)
    {
        super(context);
    }

    public ClickableImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public ClickableImageView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public boolean onTouchEvent(MotionEvent me)
    {
        if(!isClickable())return false;

        if(me.getAction() == MotionEvent.ACTION_DOWN)
        {
            select();
        }
        else if(me.getAction() == MotionEvent.ACTION_MOVE)
            ;
        else
        {
            if(me.getAction() == MotionEvent.ACTION_UP)
            {
                if( me.getX() > 0 && me.getX() < getWidth() && me.getY() > 0 && me.getY() < getHeight() )
                    performClick();
            }
            deSelect();
        }
        return true;
    }

    private void select()
    {
        Drawable drawable = getBackground();
        if(drawable != null)
        {
            drawable.setColorFilter(new PorterDuffColorFilter(ClickableView.lightenColor, PorterDuff.Mode.MULTIPLY));
            pressed = true;
        }
    }

    private void deSelect()
    {
        Drawable drawable = getBackground();
        if(drawable != null)
        {
            drawable.setColorFilter(null);
            pressed = false;
        }
    }

    public void close()
    {
        //try{((BitmapDrawable)getDrawable()).getBitmap().recycle();}catch (Throwable xa){}
        setBackgroundResource(0);
        setImageDrawable(null);
        destroyDrawingCache();
    }

    public void draw(Canvas canvas)
    {
        if(hasShadow)
        {
            if(!pressed)
                canvas.drawOval(shadowUtil.ovalRect, shadowUtil.paint);
            canvas.scale(shadowUtil.scale, shadowUtil.scale, 0, 0);
            super.draw(canvas);
        }
        else super.draw(canvas);
    }

    /**
     * adds shadow layer to the imageView, this adds a shadow view to the whole view
     * at the moment it supports only circles.
     * @param shiftX            the amount to shift on the x axis   (ignored)
     * @param shiftY            the amount to shift on the y axis
     * @param color             the color of the shadow
     * @param imageViewSize     the size of the view
     */
    public void setShadow(int shiftX, int shiftY, int color, int imageViewSize)
    {
        if(shiftY == 0)
            shiftY = 1;
        shadowUtil = new ShadowUtil(shiftX, shiftY, color, imageViewSize);
        this.hasShadow = true;
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    private class ShadowUtil
    {
        private float scale;
        private Paint paint;
        private RectF ovalRect;

        private ShadowUtil(int shiftX, int shiftY, int color, float imageViewSize)
        {
            scale = ((float)shiftY) / imageViewSize;
            paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(color);
            paint.setMaskFilter(new BlurMaskFilter((shiftX > shiftY ? shiftX : shiftY), BlurMaskFilter.Blur.NORMAL));//(shiftX > shiftY ? shiftX : shiftY)
            if(shiftX == 0)
                scale = 1f - (((float)shiftY) / imageViewSize);
            else if(shiftY == 0)
                scale = 1f - (((float)shiftX) / imageViewSize);
            else if(shiftX < shiftY)
                scale = 1f - (((float)shiftY) / imageViewSize);
            else
                scale = 1f - (((float)shiftX) / imageViewSize);
            ovalRect = new RectF(shiftX, shiftY, (imageViewSize * scale), (imageViewSize * scale));    //+ shiftX    + shiftY
        }

    }

}