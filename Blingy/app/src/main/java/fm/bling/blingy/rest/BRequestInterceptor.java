package fm.bling.blingy.rest;

import android.os.Build;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.FacebookException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.Scope;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import fm.bling.blingy.App;
import fm.bling.blingy.BuildConfig;
import fm.bling.blingy.R;
import fm.bling.blingy.registration.rest.model.CAAGoogleToken;
import fm.bling.blingy.registration.rest.model.CAAPostToken;

import fm.bling.blingy.registration.rest.model.CAAToken;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.utils.Constants;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Chosen-pro on 18/06/15.
 * Updated by Oren Zakay on 26/02/17.
 */
public class BRequestInterceptor implements RequestInterceptor {

    public static boolean waitingForAccess = false;
    private static final String TAG = "BRequestInterceptor";


    @Override
    public void intercept(RequestFacade request) {

        /* This filed are shouldn't be removed **/
        request.addHeader("User-Agent", "BlingyAndroidApp/" + BuildConfig.VERSION_NAME + "; " + Build.VERSION.RELEASE + "; " + Build.MODEL + "; " + timeZone());
        request.addQueryParam("access_token", CAAUserDataSingleton.getInstance().getAccessToken());
        request.addQueryParam("v", RestClient.version);

        if (isTokenExpired()) {

            if (waitingForAccess)
                return;

            waitingForAccess = true;
            switch (CAAUserDataSingleton.getInstance().getAccountType()) {
                case Constants.GOOGLE_PLUS_ACC:
                    String googleToken = "";
                    try {
                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                                .requestId()
                                .requestIdToken(App.getAppContext().getResources().getString(R.string.server_client_id))
                                .build();

                        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(App.getAppContext())
                                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                                .build();

                        OptionalPendingResult<GoogleSignInResult> optionalPendingResult = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);

                        GoogleSignInResult result = optionalPendingResult.get();
                        googleToken = result.getSignInAccount().getIdToken();

                        App.getUrlService().postGoogleToken(new CAAGoogleToken(googleToken, Constants.APP_ID, Constants.APP_SECRET), new Callback<CAAToken>() {
                            @Override
                            public void success(CAAToken caaToken, Response response) {
                                CAAUserDataSingleton.getInstance().setAccessToken(caaToken);
                                waitingForAccess = false;
//                                Log.d(TAG, "Login>>onNext>>google_token>>>" + caaToken.getAccessToken());
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                waitingForAccess = false;
//                                Log.d(TAG, "Login>>onCompleted>>google_token>>>");
                            }
                        });
                    } catch (Throwable throwable) {
//                        Log.d(TAG, "Login>>catch>>google_token>>>" + googleToken + "\n Excep :" + throwable.toString());
                    }
                    break;
                case Constants.FACEBOOK_ACC:
                    AccessToken.refreshCurrentAccessTokenAsync(new AccessToken.AccessTokenRefreshCallback() {
                        @Override
                        public void OnTokenRefreshed(AccessToken accessToken) {
                            AccessToken.setCurrentAccessToken(accessToken);
                            CAAUserDataSingleton.getInstance().setAccessToken(accessToken.getToken());
                            waitingForAccess = false;
//                            Log.d(TAG, "Login>>>>updatedFacebooktoken>>>" + CAAUserDataSingleton.getInstance().getAccessToken());
                        }

                        @Override
                        public void OnTokenRefreshFailed(FacebookException exception) {
                            waitingForAccess = false;
//                            Log.d(TAG, "Login>>>>updatedFacebooktoken>>>Failed>>>" + CAAUserDataSingleton.getInstance().getAccessToken());
                        }
                    });
                    break;
                case Constants.EMAIL_ACC:

                    String email = SharedPreferencesManager.getInstance().getString(Constants.EMAIL, "");
                    String pass = SharedPreferencesManager.getInstance().getString(Constants.PASSWORD, "");

                    App.getUrlService().postToken(new CAAPostToken(email, pass, Constants.APP_ID, Constants.APP_SECRET), new Callback<CAAToken>() {
                        @Override
                        public void success(CAAToken caaToken, Response response) {
                            CAAUserDataSingleton.getInstance().setAccessToken(caaToken);
                            waitingForAccess = false;
//                            Log.d(TAG, "Login>>onNext>>email>>>" + caaToken.getAccessToken());
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            waitingForAccess = false;
//                            Log.d(TAG, "Login>>onCompleted>>email>>>");
                        }
                    });
                    break;
            }
            return;
        }
    }

    private static String timeZone() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault());
        String timeZone = new SimpleDateFormat("Z").format(calendar.getTime());
        return timeZone.substring(0, 3) + timeZone.substring(3, 5);
    }

    private boolean isTokenExpired() {
        return CAAUserDataSingleton.getInstance().isAccessTokenExpired();
    }
}
