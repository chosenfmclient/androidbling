package fm.bling.blingy.record.callbacks;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 12/29/16.
 * History:
 * ***********************************
 */
public interface DetectBackgroundCallback {

    void doDetectBackground();
}
