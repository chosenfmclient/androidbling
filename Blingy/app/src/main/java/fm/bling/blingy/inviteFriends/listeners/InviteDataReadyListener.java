package fm.bling.blingy.inviteFriends.listeners;

/**
 * Created by Chosen-pro on 12/07/2016.
 */


public interface InviteDataReadyListener {

    void onInviteDataReady();
}
