package fm.bling.blingy.myVideos.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fm.bling.blingy.R;
import fm.bling.blingy.utils.AdaptersDataTypes;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;
import fm.bling.blingy.videoHome.model.CAAVideo;

/**
 * Created by Kiril on 5/18/2015.
 */
public class MyPerformancesAdapter extends ArrayAdapter<CAAVideo> {
    private List<CAAVideo> baseList;
    private LayoutInflater inflater;
    private Animation anim;
    private ImageLoaderManager mImageLoaderManager;


    public MyPerformancesAdapter(Activity context, List<CAAVideo> items) {
        super(context, 0, items);
        inflater = LayoutInflater.from(context);
        baseList = items;
        mImageLoaderManager = new ImageLoaderManager(context);

    }

    @Override
    public int getItemViewType(int position) {
        if (baseList.get(position).getType() == null) {
            return AdaptersDataTypes.TYPE_LOADING;
        } else {
            if (baseList.get(position).getMediaType().equalsIgnoreCase(Constants.PHOTO_TYPE)) {
                return AdaptersDataTypes.TYPE_PHOTO;
            } else {
                return AdaptersDataTypes.TYPE_VIDEO;
            }
        }
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int cellType = getItemViewType(position);
        if (convertView == null) {
            ViewHolder holder = new ViewHolder();

            if (cellType == AdaptersDataTypes.TYPE_VIDEO || AdaptersDataTypes.TYPE_PHOTO == cellType) {
                convertView = inflater.inflate(R.layout.my_performances_video_item, null);
                holder.videoInfoPrimary = (TextView) convertView.findViewById(R.id.text_view_video_info1);
                holder.videoInfoSecondary = (TextView) convertView.findViewById(R.id.text_view_video_info2);
                holder.videoStatus = (TextView) convertView.findViewById(R.id.text_view_video_status);
                holder.videoImage = (URLImageView) convertView.findViewById(R.id.videoHome_button_playPause);
                holder.playImage = (ImageView) convertView.findViewById(R.id.play_pause_image_view);
            } else {
                anim = AnimationUtils.loadAnimation(getContext(), R.anim.spinner_rotate);
                convertView = inflater.inflate(R.layout.loading_list_layout, null);
                holder.mSpinnerView = (ImageView) convertView.findViewById(R.id.page_spinner);
            }
            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();

        if (cellType == AdaptersDataTypes.TYPE_VIDEO || cellType == AdaptersDataTypes.TYPE_PHOTO) {
            final CAAVideo video = baseList.get(position);
            String videoType = video.getType();
            switch (videoType) {
//                case Constants.BIO_TYPE:
//                    holder.videoInfoPrimary.setText("Bio #" + (position + 1));
//                    break;
                case Constants.COVER_TYPE:
//                case Constants.KARAOKE_TYPE:
                case Constants.DANCE_OFF_TYPE:
//                case Constants.LIPSYNC_TYPE:
                    holder.videoInfoPrimary.setText(video.getSongName());
                    holder.videoStatus.setText("Status: " + video.getStatus());
                    holder.videoInfoSecondary.setVisibility(View.VISIBLE);
                    if(!isFirstNameAndLastNameEmpty(video.getUser().getFirstName(), video.getUser().getLastName()))
                        holder.videoInfoSecondary.setText("By: " + video.getUser().getFirstName() + " " + video.getUser().getLastName());
                    else
                        holder.videoInfoSecondary.setVisibility(View.GONE);
//                    break;
//                case Constants.RESPONSE_TYPE:
//                    holder.videoInfoPrimary.setText(video.getUser().getFirstName() + " " + video.getUser().getLastName());
//                    holder.videoStatus.setText("Status: " + video.getStatus());
//                    holder.videoInfoSecondary.setVisibility(View.VISIBLE);
//                    if(!isSongNameEmpty(video.getSongName()))
//                        holder.videoInfoSecondary.setText("\"" + video.getSongName() + "\"");
//                    else
//                        holder.videoInfoSecondary.setVisibility(View.GONE);
//                    break;
                case Constants.ORIGINAL_TYPE:
                case Constants.UPLOAD_TYPE:
                default:
                    holder.videoInfoPrimary.setText(video.getSongName());
                    holder.videoStatus.setText("Status: " + video.getStatus());
                    break;
            }
            holder.videoImage.setUrl(video.getThumbnail());
            holder.videoImage.setKeepAspectRatioAccordingToWidth(false);
            holder.videoImage.setImageLoader(mImageLoaderManager);
            mImageLoaderManager.DisplayImage(holder.videoImage);

            if (cellType == AdaptersDataTypes.TYPE_PHOTO) {
                holder.playImage.setVisibility(View.GONE);
            }
        } else {
            holder.mSpinnerView.setAnimation(anim);
        }

        return convertView;
    }

    private boolean isSongNameEmpty(String songName){
        if(songName.isEmpty() || songName.equalsIgnoreCase("") || songName.equalsIgnoreCase(" "))
            return true;
        else
            return false;
    }

    private boolean isFirstNameAndLastNameEmpty(String firstName, String lastName){
        if((firstName.isEmpty() || firstName.equalsIgnoreCase("") || firstName.equalsIgnoreCase(" "))
           && (lastName.isEmpty() || lastName.equalsIgnoreCase("") || lastName.equalsIgnoreCase(" ")))
            return true;
        else
            return false;
    }


    public class ViewHolder {

        TextView videoInfoPrimary;
        TextView videoInfoSecondary;
        TextView videoStatus;
        URLImageView videoImage;
        ImageView mSpinnerView;
        ImageView playImage;


    }
}
