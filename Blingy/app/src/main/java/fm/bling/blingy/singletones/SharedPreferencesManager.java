package fm.bling.blingy.singletones;

import android.content.Context;
import android.content.SharedPreferences;

import fm.bling.blingy.App;
import fm.bling.blingy.R;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 5/10/16.
 * History:
 * ***********************************
 */
public class SharedPreferencesManager {

    private static SharedPreferences mSharedPreferences = null;
    private static SharedPreferences.Editor mEditor = null;

    private SharedPreferencesManager(){

    }

    public static SharedPreferences getInstance() {
        if (mSharedPreferences == null)
            mSharedPreferences = App.getAppContext().getSharedPreferences(App.getAppContext().getResources().getString(R.string.user_shared_prefs), Context.MODE_PRIVATE);
        return mSharedPreferences;
    }

    public static SharedPreferences.Editor getEditor(){
        if(mEditor == null)
            mEditor = getInstance().edit();
        return mEditor;
    }
}
