package fm.bling.blingy.widget.dialogs;


import android.content.Intent;
import android.view.View;

import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.registration.SignupActivity;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.stateMachine.StateMachineMessageListener;
import fm.bling.blingy.upload.PickPhotoVideoActivity;
import fm.bling.blingy.widget.CAAWidgetSettingsSingleton;
import fm.bling.blingy.widget.model.CAAInviteWidgetSetting;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 7/13/16.
 * History:
 * ***********************************
 */
public class ButtonTitleDialog extends InviteWidgetBaseDialog {

    public ButtonTitleDialog(StateMachineMessageListener onReadyListener, final BaseActivity context, final StateMachineEvent.MESSAGE_TYPE messageType, String buttonText, String mainTitle) {
        super(onReadyListener, context, messageType);
        CAAInviteWidgetSetting settings = CAAWidgetSettingsSingleton.getInstance().getSettingByType(messageType.name().toLowerCase());
        if (settings != null) mainTitle = settings.getText();

        mButtonText.setText(buttonText);
        mMainText.setText(mainTitle);

        mButtonFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (messageType)
                {
                    case YOU_SHOULD_SIGN_UP:
                        context.startActivity(new Intent(context, SignupActivity.class));
                        break;
                    case TRY_UPLOAD:
                        context.checkUploadPermissions(new Intent(context, PickPhotoVideoActivity.class));
                        break;
//                    case TAKE_A_PHOTO:
//                        Intent intent = new Intent(context, CreateVideoActivity.class);
//                        intent.putExtra(Constants.PHOTO_MODE,true);
//                        context.getRecordPermissions(intent);
//                        break;
                    default:
                        break;
                }
                dismiss();
            }
        });
    }
}
