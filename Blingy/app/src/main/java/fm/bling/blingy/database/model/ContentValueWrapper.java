package fm.bling.blingy.database.model;

import android.content.ContentValues;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 09/02/17.
 * History:
 * ***********************************
 */
public class ContentValueWrapper {

    private String mTableName;
    private String mWhereClause;

    protected Map<String, String> mValuesString;
    protected Map<String, Integer> mValuesIntegers;

    public ContentValueWrapper() {
        mValuesString = new HashMap<>();
        mValuesIntegers = new HashMap<>();
    }

    public void addIntegerValue(String key, int value) {
        mValuesIntegers.put(key, value);
    }

    public void addStringValue(String key, String value) {
        mValuesString.put(key, value);
    }

    public ContentValues getContentValue() {
        ContentValues contentValues = new ContentValues();

        if (mValuesString.size() > 0) {
            Set stringSet = mValuesString.entrySet();
            Iterator iterator = stringSet.iterator();

            while (iterator.hasNext()) {
                Map.Entry entry = (Map.Entry) iterator.next();
                contentValues.put((String) entry.getKey(), (String) entry.getValue());
            }
        }

        if (mValuesIntegers.size() > 0) {
            Set longSet = mValuesIntegers.entrySet();
            Iterator iterator = longSet.iterator();

            while (iterator.hasNext()) {
                Map.Entry entry = (Map.Entry) iterator.next();
                contentValues.put((String) entry.getKey(), (Integer) entry.getValue());
            }
        }

        return contentValues;
    }

    public void setTableName(String mTableName) {
        this.mTableName = mTableName;
    }

    public void setWhereClause(String mWhereClause) {
        this.mWhereClause = mWhereClause;
    }

    public String getTableName() {
        return mTableName;
    }

    public String getWhereClause() {
        return mWhereClause;
    }
}
