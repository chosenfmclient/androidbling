package fm.bling.blingy.videoHome.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAVideoList
 * Description:
 * Created by Dawidowicz Nadav on 5/6/15.
 * History:
 * ***********************************
 */
public class CAAVideoList {

    @SerializedName("data")
    private ArrayList<CAAVideo> data;

    @SerializedName("sort_by")
    private CAAVideoListSortBy sortBy;

    @SerializedName("background")
    private String background;

    @SerializedName("icon")
    private String icon;

    @SerializedName("points")
    private String points;

    public CAAVideoListSortBy getSortBy() {
        return sortBy;
    }

    public void setSortBy(CAAVideoListSortBy sortBy) {
        this.sortBy = sortBy;
    }

    public ArrayList<CAAVideo> getData() {
        return data;
    }

    public void setData(ArrayList<CAAVideo> data) {
        this.data = data;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
