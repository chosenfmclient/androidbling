package fm.bling.blingy.registration;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.TermsAndConditionsActivity;
import fm.bling.blingy.database.handler.FollowingDataHandler;
import fm.bling.blingy.database.handler.LikesDataHandler;
import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.dialogs.EmailSignupDialog;
import fm.bling.blingy.homeScreen.HomeScreenActivity;
import fm.bling.blingy.homeScreen.model.VideoFeed;
import fm.bling.blingy.inviteFriends.InviteTabContentInvite;
import fm.bling.blingy.inviteFriends.model.CAAContactEmails;
import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.registration.listeners.RegistrationButtonListener;
import fm.bling.blingy.registration.rest.model.CAAGooglePlusSignup;
import fm.bling.blingy.registration.rest.model.CAAToken;

import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.inviteFriends.model.CAAApiContactsResponse;
import fm.bling.blingy.inviteFriends.model.ContactInfo;
import fm.bling.blingy.splashScreen.views.SplashScreenView;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.dialogs.LoadingDialog;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.WhiteProgressBarView;
import fm.bling.blingy.widget.CAAWidgetSettingsSingleton;
//import fm.bling.blingy.widget.PointsObserver;
//import fm.bling.blingy.widget.dialogs.PointsViewDialog;
//import fm.bling.blingy.widget.dialogs.UserProgressMapDialog;
import fm.bling.blingy.widget.model.CAAInviteWidgetSettingsResponse;
//import fm.bling.blingy.widget.views.PointsView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Chosen-pro on 4/10/16.
 */
public class RegistrationActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener, RegistrationButtonListener {

    private boolean friendsListReached = false;
    private FrameLayout parentLayout;
    private FrameLayout uiFrame;
    private FrameLayout registrationLayout;
    private FrameLayout signInLayout;
    private FrameLayout inviteLayout;
    private FrameLayout inviteFrame;
    private LinearLayout connectLayout;
    private FrameLayout emailButton;
    private FrameLayout facebookButton;
    private FrameLayout googleButton;
    private ImageButton connectButton;
    private FrameLayout goToHomeFrame;
    private Button playButton;
    private TextView dontLikeText;
    private TextView skipButton;
    private TextView terms;
    private WhiteProgressBarView mProgressBar;

    private CallbackManager mCallbackManager;
    private LoginManager loginManager;
    private List<String> permissionNeeds;
    private boolean mustConnect = false;

    private SplashScreenView splashView;
    private Animation fadeOut;
    private Animation fadeIn;
    private Animation slideOut;
    private Animation slideIn;
    private Handler handler;
    private LoadingDialog loadingDialog;
    private int EMAIL_REQUEST_CODE = 777;

    private ArrayList<ContactInfo> contacts, searchBaseContacts;
    private List<User> chosenContacts;
    private List<String> emails;
    private List<List<String>> mListsEmails;
    private List<User> receivedData;
    private List<String> emailsToRemove;
    private HashMap<Integer, ContactInfo> contactsMap;
    private int nCalls = 0;

    private boolean deeplink;
    private boolean deeplinkduet = false;

    private GoogleApiClient mGoogleApiClient;

    private ImageLoaderManager mImageLoaderManager;

    private int faildLogins = 1;

    private TextureView firstVideo;
    private MediaPlayer firstMediaPlayer;
    private boolean isPassedOnCreate = false;
    private boolean firstTime = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_fragment);
        inflateViews();
        isPassedOnCreate = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }

        deeplink = getIntent().getBooleanExtra("deeplink", false);

        if (getIntent().hasExtra("duet"))
            deeplinkduet = getIntent().getBooleanExtra("duet", false);

        handler = new Handler();
        TrackingManager.getInstance().pageView(EventConstants.REGISRATION_PAGE);

        setUpAnimations();

        /** First Video **/
        initilizeFirstVideo();

        splashView = new SplashScreenView(getApplicationContext(), false);
        parentLayout.addView(splashView);

        setUpLoginButtons();

        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hasPermission()) {
                    moveToFriendsList();
                } else {
                    getPermission();
                }
            }
        });

        if (splashView != null)
            splashView.startAnimation(fadeOut);

        uiFrame.setVisibility(View.VISIBLE);
        uiFrame.setAnimation(fadeIn);
        fadeIn.start();

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrackingManager.getInstance().tapSkip();
                goToHome();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mImageLoaderManager = new ImageLoaderManager(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!isPassedOnCreate)
            reinitCurrentPlayer();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isPassedOnCreate = false;
        try {
            releaseCurrentPlayer();
        } catch (Throwable err) {
        }
        try {
            releaseCurrentPlayer();
        } catch (Throwable err) {
        }
        mImageLoaderManager.close();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        close();
    }

    private void inflateViews() {
        parentLayout = (FrameLayout) findViewById(R.id.parentLayout);
        uiFrame = (FrameLayout) findViewById(R.id.ui_frame);
        registrationLayout = (FrameLayout) findViewById(R.id.registration_layout);
        signInLayout = (FrameLayout) findViewById(R.id.sign_in_layout);
        inviteLayout = (FrameLayout) findViewById(R.id.invite_layout);
        inviteFrame = (FrameLayout) findViewById(R.id.inviteFrame);
        connectLayout = (LinearLayout) findViewById(R.id.connect_layout);
        emailButton = (FrameLayout) findViewById(R.id.login_email);
        facebookButton = (FrameLayout) findViewById(R.id.login_facebook);
        googleButton = (FrameLayout) findViewById(R.id.login_google);
        connectButton = (ImageButton) findViewById(R.id.connect_button);
        goToHomeFrame = (FrameLayout) findViewById(R.id.go_to_home);
        playButton = (Button) findViewById(R.id.letsPlay);
        dontLikeText = (TextView) findViewById(R.id.i_dont_like_text);
        skipButton = (TextView) findViewById(R.id.skipButton);
        terms = (TextView) findViewById(R.id.terms);
        ImageView mBlingyLogo = (ImageView) findViewById(R.id.blingy_logo);
        ((FrameLayout.LayoutParams) mBlingyLogo.getLayoutParams()).bottomMargin = (int) (250f * App.SCALE_X);
        mProgressBar = (WhiteProgressBarView) findViewById(R.id.progress_bar);
        mProgressBar.updateLayout();
    }


    @Override
    public void onBackPressed() {
        if (friendsListReached)
            goToHome();
        else {
            TrackingManager.getInstance().tapCanelRegister();
            super.onBackPressed();
        }
    }

    private void setUpLoginButtons() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PROFILE))
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestScopes(new Scope(Scopes.PLUS_ME))
                .requestId()
                .requestProfile()
                .requestEmail()
                .requestIdToken(getResources().getString(R.string.server_client_id))
                .requestServerAuthCode(getResources().getString(R.string.server_client_id))
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Plus.API)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EmailSignupDialog dialog = new EmailSignupDialog(RegistrationActivity.this, RegistrationActivity.this);
                dialog.show();
            }
        });


        mCallbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();
        loginManager.registerCallback(mCallbackManager, mCallback);
        permissionNeeds = Arrays.asList("email");
        facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.canClick()) {
                    TrackingManager.getInstance().loginFacebookSubmit();
                    if (NetworkConnectivityListener.isConnectedWithDialog(RegistrationActivity.this)) {
                        loginManager.logInWithReadPermissions(RegistrationActivity.this, permissionNeeds);
                    }
                }
            }
        });

        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.canClick()) {
                    TrackingManager.getInstance().register(EventConstants.GOOGLE_PLUS);
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, Constants.GOOGLE_SIGN_IN);
                }

            }
        });

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.canClick()) {
                    Intent i = new Intent(RegistrationActivity.this, TermsAndConditionsActivity.class);
                    startActivity(i);
                }
            }
        });
    }

    private FacebookCallback<LoginResult> mCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            showLoading();
            callFacebookSignupApi(loginResult.getAccessToken());
            TrackingManager.getInstance().registerSubmit("facebook");
        }

        @Override
        public void onCancel() {
            TrackingManager.getInstance().loginFacebookFailed();
        }

        @Override
        public void onError(FacebookException e) {
            TrackingManager.getInstance().loginFacebookFailed();
            TrackingManager.getInstance().registerFaild(faildLogins++);
        }
    };

    @Override
    public void goToRegistration() {
        Intent i = new Intent(RegistrationActivity.this, EmailSignupActivity.class);
        startActivityForResult(i, EMAIL_REQUEST_CODE);
    }

    @Override
    public void goToLogin() {
        Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
        startActivityForResult(i, EMAIL_REQUEST_CODE);
    }

    private void callFacebookSignupApi(final AccessToken token) {
        CAAFacebookLogin.updateFacebookUserSingleton(token);
        handler.postDelayed(new Runnable() {
            public void run() {
                CAAUserDataSingleton.getInstance().setSentGcmToken(false);
                hideLoading();
                userSignedIn();
            }
        }, 2000);
    }

    private void userSignedIn() {
        getHomeScreen();
        SharedPreferencesManager.getEditor().putLong(Constants.INSTALL_DATE, System.currentTimeMillis()).apply();
        SharedPreferencesManager.getEditor().putBoolean(Constants.FIRST_LAUNCH, false).apply();
        App.getUrlService().getWidgetSettings(new Callback<CAAInviteWidgetSettingsResponse>() {
            @Override
            public void success(CAAInviteWidgetSettingsResponse response, Response response2) {
                if (response2.getStatus() == 204) {
                    CAAWidgetSettingsSingleton.getInstance().setWidgetOff();
                } else {
                    CAAWidgetSettingsSingleton.getInstance().setSettings(response);
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

        getAllLikesFromServer();
        getAllFollowingFromServer();

        if (deeplinkduet) {
            goToHome();
        } else {

            if (!hasPermission() && signInLayout != null) {
                mustConnect = true;
                signInLayout.startAnimation(slideOut);
            } else {
                mustConnect = false;
                moveToFriendsList();
            }
        }
    }

    private void getAllLikesFromServer() {
        App.getUrlService().getUserLikes(new Callback<ArrayList<String>>() {
            @Override
            public void success(ArrayList<String> likes, Response response) {
                if (likes != null)
                    LikesDataHandler.getInstance().addMany(likes);
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private void getAllFollowingFromServer() {
        App.getUrlService().getUserFollowing(new Callback<ArrayList<String>>() {
            @Override
            public void success(ArrayList<String> userIds, Response response) {
                if (userIds != null)
                    FollowingDataHandler.getInstance().addMany(userIds);
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED)
            return;

        if (requestCode == EMAIL_REQUEST_CODE && resultCode == RESULT_OK) {
            TrackingManager.getInstance().registerSubmit("email");
            userSignedIn();
        } else if (requestCode == Constants.GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void getHomeScreen() {
        App.getUrlService().getHomeScreen(0, new Callback<VideoFeed>() {
            @Override
            public void success(VideoFeed videoFeed, Response response) {
                if (Constants.mVideoFeedData == null && videoFeed != null)
                    Constants.mVideoFeedData = videoFeed.getVideoFeed();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result != null && result.isSuccess()) {
            showLoading();
            setResult(Activity.RESULT_OK);
            CAAUserDataSingleton.getInstance().setSentGcmToken(false);
            final GoogleSignInAccount acct = result.getSignInAccount();

            String personNickName = "";
            String personGooglePlusProfile = "";
            String birthday = "";
            int personGender = -1;
            String gender = "";

            if (mGoogleApiClient.hasConnectedApi(Plus.API)) {
                if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                    Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                    if (currentPerson.hasNickname())
                        personNickName = currentPerson.getNickname();
                    if (currentPerson.hasUrl())
                        personGooglePlusProfile = currentPerson.getUrl();
                    if (currentPerson.hasGender())
                        personGender = currentPerson.getGender();
                    if (currentPerson.hasBirthday())
                        birthday = currentPerson.getBirthday();
                }
                if (personGender != -1) {
                    gender = personGender == 0 ? "male" : "female";
                }

            }
            String token = acct.getIdToken();
            SharedPreferencesManager.getEditor().putString(Constants.GOOGLE_TOKEN_ID, token).commit();
            callGooglePlusSignupApi(token, personGooglePlusProfile, personNickName, birthday, gender);
        } else {
            TrackingManager.getInstance().registerFaild(faildLogins++);
        }
    }

    private void callGooglePlusSignupApi(final String token, final String googleProfile, final String Nickname, final String birthday, final String gender) {
        CAAGooglePlusSignup googlePlusSignup = new CAAGooglePlusSignup(token, googleProfile, Nickname, birthday, gender, Constants.APP_ID, Constants.APP_SECRET);

        App.getUrlService().postGooglePlusSignup(googlePlusSignup, new Callback<CAAToken>() {
            @Override
            public void success(CAAToken caaToken, Response response) {
                CAAGooglePlusLogin.updateGooglePlusUserSingleton(caaToken);
                hideLoading();
                TrackingManager.getInstance().registerSubmit("google+");
                userSignedIn();
            }

            @Override
            public void failure(RetrofitError error) {
                TrackingManager.getInstance().registerFaild(faildLogins++);
                hideLoading();
                setResult(Activity.RESULT_OK);
                if (error.getResponse() != null && error.getResponse().getStatus() == 403) {
                    String errorMsg = "Please try login with email.";
                    BaseDialog dialog = new BaseDialog(RegistrationActivity.this, "Login Error", errorMsg);
                    dialog.removeCancelbutton();
                    dialog.show();
                }
            }
        });
    }

    private void setUpAnimations() {
        /**
         * Cards
         */
        Animation.AnimationListener mAnimListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                try {
                    removeCard();
                } catch (Throwable throwable) {
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };

        fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        fadeOut.setDuration(500);
        fadeOut.setAnimationListener(mAnimListener);


        fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        fadeIn.setDuration(500);
        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                uiFrame.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                try {
                    fadeIn.setAnimationListener(null);
                    fadeIn = null;
                } catch (Throwable throwable) {
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fadeIn.setDuration(800);
        slideOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out);
        slideOut.setDuration(300);
        slideOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (firstTime) {
                    firstTime = false;
                    if(emailButton != null)
                        emailButton.setOnClickListener(null);
                    if(facebookButton != null)
                        facebookButton.setOnClickListener(null);
                    if(googleButton != null)
                        googleButton.setOnClickListener(null);

                    if(registrationLayout != null) {
                        registrationLayout.removeView(googleButton);
                        registrationLayout.removeView(emailButton);
                        registrationLayout.removeView(facebookButton);
                    }
                }

                if (mustConnect && signInLayout != null) {
                    signInLayout.setVisibility(View.GONE);
                    connectLayout.setVisibility(View.VISIBLE);
                    skipButton.setVisibility(View.VISIBLE);
                    registrationLayout.removeView(signInLayout);
                    connectLayout.startAnimation(slideIn);
                    signInLayout = null;
                    TrackingManager.getInstance().appseeStartScreen(EventConstants.LOGIN_PERMISSIONS_FRIENDS);

                } else {
                    if(terms != null)
                        terms.setOnClickListener(null);
                    parentLayout.removeView(terms);
                    parentLayout.removeView(registrationLayout);
                    parentLayout.removeView(connectLayout);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        slideIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_right);
        slideIn.setDuration(300);
    }

    private void removeCard() {
        try {
            if (splashView != null) {
                splashView.setVisibility(View.GONE);
                parentLayout.removeView(splashView);
            }
        } catch (Throwable throwable) {
        }
    }

    private void moveToFriendsList()
    {
        try
        {
            friendsListReached = true;
            registrationLayout.startAnimation(slideOut);
            skipButton.setVisibility(View.GONE);
        }
        catch (Throwable throwable) { }
        getContactsToInvite();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.CRITICAL_PERMISSION) {
            for (int i = 0; i < grantResults.length; i++) {
                if (permissions[i].equals(Manifest.permission.READ_CONTACTS)) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        moveToFriendsList();
                        break;
                    } else {
                        permissionsDialog();
                    }
                }
            }
        }
    }

    public void permissionsDialog() {
        BaseDialog dialog = new BaseDialog(this, "Permissions", getResources().getString(R.string.contacts_permission_dialog));
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (((BaseDialog) dialogInterface).getAnswer()) {
                    getPermission();
                }
            }
        });
    }

    private boolean hasPermission() {
        return ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED;
    }

    private void getPermission() {
        ActivityCompat.requestPermissions(RegistrationActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, Constants.CRITICAL_PERMISSION);
    }

    private void getContactsToInvite() {
        if (!hasPermission()) {
            getPermission();
            return;
        }

        try {
            loadingDialog = new LoadingDialog(this, false, false);
            contacts = new ArrayList<>();
            searchBaseContacts = new ArrayList<>();
            emails = new ArrayList<>();

            contactsMap = new HashMap<>();

            String[] PROJECTION =
                    {
                            ContactsContract.CommonDataKinds.Identity.CONTACT_ID,
                            ContactsContract.Contacts.DISPLAY_NAME,
                            ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
                            ContactsContract.CommonDataKinds.Email.ADDRESS,
                    };

            ContentResolver cr = getApplicationContext().getContentResolver();
            Cursor cur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");//;
            final int emailIdColumnIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Identity.CONTACT_ID);
            final int emailNameColumnIndex = cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
            final int emailPhotoColumnIndex = cur.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI);
            final int emailEmailColumnIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS);

            int id;
            String name, photoUri, email;
            // ContactInfo ci;
            if (cur.getCount() > 0) {
                while (cur.moveToNext()) {
                    id = cur.getInt(emailIdColumnIndex);
                    name = cur.getString(emailNameColumnIndex);
                    photoUri = cur.getString(emailPhotoColumnIndex);
                    email = cur.getString(emailEmailColumnIndex);
                    contactsMap.put(id, new ContactInfo(id, name, photoUri, email, false));//, phoneNumber
                    emails.add(email);
                }
            }
            cur.close();

            String countryRegion = null;
            try {
                TelephonyManager tm = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
                countryRegion = tm.getNetworkCountryIso();
                if (countryRegion == null || (countryRegion != null && countryRegion.isEmpty()))
                    countryRegion = tm.getSimCountryIso();
                if (countryRegion != null) {
                    countryRegion = countryRegion.toUpperCase();
                }
            } catch (Throwable xz) {
//            xz.printStackTrace();
            }
            if (countryRegion != null) {
                String[] PROJECTION2 =
                        {
                                ContactsContract.CommonDataKinds.Identity.CONTACT_ID,
                                ContactsContract.Contacts.DISPLAY_NAME,
                                ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                ContactsContract.CommonDataKinds.Phone.TYPE
                        };

                cur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION2, null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");

                if (cur.getCount() > 0) {
                    final int phoneIdColumnIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Identity.CONTACT_ID);
                    final int phoneNameColumnIndex = cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                    final int phonePhotoColumnIndex = cur.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI);
                    final int phoneNumberColumnIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    final int phoneTypeColumnIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);

                    String phone;
                    PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
                    Phonenumber.PhoneNumber phoneNumber = new Phonenumber.PhoneNumber();
                    ContactInfo contactInfo;
                    int phoneTypeOrdinal;

                    while (cur.moveToNext()) {
                        id = cur.getInt(phoneIdColumnIndex);
                        contactInfo = contactsMap.get(id);
                        if (contactInfo != null && contactInfo.isPhoneNumberSet()) {
                            continue;
                        }

                        phone = cur.getString(phoneNumberColumnIndex);

                        try {
                            phone = getOnlyDigits(phone);
                            phoneNumberUtil.parse(phone, countryRegion, phoneNumber);
                            PhoneNumberUtil.PhoneNumberType phoneNumberType = phoneNumberUtil.getNumberType(phoneNumber);
                            phoneTypeOrdinal = phoneNumberType.ordinal();

                            if (phoneTypeOrdinal == PhoneNumberUtil.PhoneNumberType.MOBILE.ordinal()) {
                                phone = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                            } else if (phoneTypeOrdinal == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE.ordinal()) {
                                if (cur.getInt(phoneTypeColumnIndex) == (ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)) {
                                    phone = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                                } else {
                                    continue;
                                }
                            } else if (phoneTypeOrdinal == PhoneNumberUtil.PhoneNumberType.UNKNOWN.ordinal()) {
                                try {
                                    if (phone.charAt(0) == '*')
                                        continue;
                                    if (phone.charAt(0) != '+')
                                        phone = '+' + phone;

                                    String countryRegion2 = phoneNumberUtil.getRegionCodeForCountryCode(phoneNumberUtil.parse(phone, "").getCountryCode());
                                    phoneNumberUtil.parse(phone, countryRegion2, phoneNumber);

                                    if (phone.length() < 7)
                                        continue;

                                    if (phoneTypeOrdinal == PhoneNumberUtil.PhoneNumberType.MOBILE.ordinal()) {
                                        phone = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                                    } else// if(phoneTypeOrdinal == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE.ordinal())
                                    {
                                        if (cur.getInt(phoneTypeColumnIndex) == (ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)) {
                                            phone = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                                        } else {
                                            continue;
                                        }
                                    }
                                } catch (Throwable err) {
//                                err.printStackTrace();
                                    continue;
                                }
                            } else {
                                continue;
                            }
                        } catch (Throwable err) {
//                        err.printStackTrace();
                            continue;
                        }
                        name = cur.getString(phoneNameColumnIndex);
                        photoUri = cur.getString(phonePhotoColumnIndex);

                        if (contactInfo != null) {
                            contactInfo.setPhoneNumber(phone);
                        } else {
                            contactsMap.put(id, new ContactInfo(id, name, photoUri, phone, true));
                        }
                    }
                }
                cur.close();

                nCalls = 0;
                if (emails.size() > 0) {
                    mListsEmails = Utils.partition(emails, 500);
                    getFriendsOnChosen();
                } else {
                    setContacts();
                }
            }
        }catch (Throwable throwable){
            goToHome();
        }
    }

    private String getOnlyDigits(String phone) {
        String result = "";
        for (int i = 0; i < phone.length(); i++) {
            if (Character.isDigit(phone.charAt(i))) {
                result += phone.charAt(i);
            }
        }
        return result;
    }

    private void getFriendsOnChosen() {
        App.getUrlService().postFindFriends(new CAAContactEmails(mListsEmails.get(nCalls)), new Callback<CAAApiContactsResponse>() {
            @Override
            public void success(CAAApiContactsResponse s, Response response) {
                if (receivedData == null)
                    receivedData = new ArrayList<>();
                receivedData.addAll(s.getData());

                if (emailsToRemove == null)
                    emailsToRemove = new ArrayList<>();
                emailsToRemove.addAll(s.getEmailsToRemove());

                nCalls++;
                if (nCalls <= mListsEmails.size() - 1) {
                    getFriendsOnChosen();
                } else {
                    if (chosenContacts == null) {
                        chosenContacts = new ArrayList<>();
                    }

                    User caaUser;
                    for (int i = 0, j; i < receivedData.size(); i++) {
                        caaUser = receivedData.get(i);
                        for (j = 0; j < chosenContacts.size(); j++) {
                            if (caaUser.getFullName().compareToIgnoreCase(chosenContacts.get(j).getFullName()) < 0)
                                break;
                        }
                        if (j > chosenContacts.size())
                            chosenContacts.add(caaUser);
                        else chosenContacts.add(j, caaUser);
                    }

                    int i;
                    boolean skip = false;
                    int comparisonResult;
                    if (contactsMap != null) {
                        for (ContactInfo contactInfo : contactsMap.values()) {
                            if (contactInfo.email != null && emailsToRemove.contains(contactInfo.email))
                                continue;
                            for (i = 0; i < contacts.size(); i++) {
                                if ((comparisonResult = contactInfo.name.compareToIgnoreCase(contacts.get(i).name)) < 0) {
                                    break;
                                } else if (comparisonResult == 0) {
                                    skip = true;
                                    break;
                                }
                            }
                            if (skip) {
                                skip = false;
                            } else {
                                searchBaseContacts.add(i, contactInfo);
                                contacts.add(i, contactInfo);
                            }
                        }
                        setContacts();
                    } else getContactsToInvite();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                setContacts();

            }
        });

    }

    private void setContacts() {
        contactsMap.clear();
        contactsMap = null;
        emailsToRemove = null;
        receivedData = null;


        Paint paint = new Paint();
        paint.setColor(getApplicationContext().getResources().getColor(R.color.white_50));
        paint.setStyle(Paint.Style.STROKE);
        InviteTabContentInvite inviteList = new InviteTabContentInvite(RegistrationActivity.this, paint, contacts, new ImageLoaderManager(RegistrationActivity.this), null, true);
        inviteFrame.addView(inviteList, 0);

        goToHomeFrame.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                goToHome();
                return false;
            }
        });

        TextView letsPlay = (TextView) findViewById(R.id.letsPlay);
        letsPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToHome();
            }
        });

        if (loadingDialog != null) loadingDialog.dismiss();
        inviteLayout.setVisibility(View.VISIBLE);
    }

    private void showLoading() {
        if (mProgressBar != null)
            mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        mProgressBar.setVisibility(View.GONE);
    }

    private void goToHome() {
        SharedPreferencesManager.getEditor().putLong(Constants.INSTALL_DATE, System.currentTimeMillis()).apply();
        SharedPreferencesManager.getEditor().putBoolean(Constants.FIRST_LAUNCH, false).apply();
        if (!deeplink) {
            Intent i = new Intent(RegistrationActivity.this, HomeScreenActivity.class);
            startActivity(i);
            close();
        }
        finish();
    }

    public void letsPlay() {
        dontLikeText.setVisibility(View.GONE);
        playButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    private void close() {
        fadeIn = null;
        slideOut = null;
        slideIn = null;
        handler = null;
        emailButton = null;
        facebookButton = null;
        googleButton = null;
        signInLayout = null;
        registrationLayout = null;
        connectLayout = null;
        terms = null;
    }

    private void reinitCurrentPlayer() {
        initilizeFirstVideo();
    }

    private void releaseCurrentPlayer() {
        if (firstMediaPlayer != null) {
            firstMediaPlayer.stop();
            firstMediaPlayer.reset();
        }
    }

    private void initilizeFirstVideo() {
        Uri mFirstUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.intro_video);
        firstMediaPlayer = new MediaPlayer();
        try {
            firstMediaPlayer.setDataSource(RegistrationActivity.this, mFirstUri);
            firstMediaPlayer.prepare();
            firstMediaPlayer.setLooping(true);
            firstMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    if (firstVideo == null)
                        firstVideo = (TextureView) findViewById(R.id.video_one);
                    firstVideo.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
                        @Override
                        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                            adjustTextureViewSize(firstVideo, firstMediaPlayer);
                            firstMediaPlayer.setSurface(new Surface(firstVideo.getSurfaceTexture()));
                        }

                        @Override
                        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                        }

                        @Override
                        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                            return false;
                        }

                        @Override
                        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
                        }
                    });
                    firstMediaPlayer.start();
                }
            });
            firstMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    initilizeFirstVideo();
                    return true;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void adjustTextureViewSize(TextureView textureView, MediaPlayer mediaPlayer) {
        if ((((float) App.WIDTH / App.HEIGHT)) != (((float) 16 / 9))) {
            if (textureView == null || mediaPlayer == null)
                return;
            float viewWidth = App.WIDTH;
            float viewHeight = textureView.getHeight();

            //Set it Top Centered.
            int yoff = 0;
            double aspectRatio;
            textureView.setBackgroundColor(Color.CYAN);
            try {
                aspectRatio = ((double) mediaPlayer.getVideoHeight()) / ((double) mediaPlayer.getVideoWidth());
            } catch (Throwable err) {
                aspectRatio = (9 / 16);
//                err.printStackTrace();
//
            }

            float newWidth, newHeight;
            newWidth = viewWidth;
            newHeight = (int) (viewWidth * aspectRatio);

            int xoff = (int) ((viewWidth - newWidth) / 2f);
            Matrix txform = new Matrix();
            textureView.getTransform(txform);
            txform.setScale(newWidth / viewWidth, newHeight / viewHeight);
            txform.postTranslate(xoff, yoff);
            textureView.setTransform(txform);
        }
    }

}
