package fm.bling.blingy.widget.views;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import fm.bling.blingy.App;
import fm.bling.blingy.R;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 8/15/16.
 * History:
 * ***********************************
 */
public class PointsItem extends FrameLayout {

    private Paint paint,paint1;
    private RectF mRect,mRect1;
    private boolean isFirst = true;
    private boolean isSelected = true;
    private int padding;
    private boolean isSetLayoutParams = false;
    private boolean gotWidthHeight = false;

    private int width, height;

    public PointsItem(Context context) {
        super(context);
        preparePaint();
    }

    public PointsItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        preparePaint();
    }

    public PointsItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        preparePaint();
    }

    private void preparePaint(){
        setLayerType(LAYER_TYPE_SOFTWARE, null);
        setSelected(false);
        padding = (int) (20f * App.SCALE_X);
    }

    @Override
    public void draw(Canvas canvas) {
        if(!gotWidthHeight){
            gotWidthHeight = true;
            ViewGroup.LayoutParams params = getLayoutParams();
            width  = params.width;
            height = params.height;
        }

        if (isFirst) {
            isFirst = false;
            mRect1.set(padding, padding, width + padding, height + padding);
            if(isSelected) {
                mRect.set(padding, padding, width + padding, height + padding);
            }
        }

        if(!isSetLayoutParams){
            ViewGroup.LayoutParams params = getLayoutParams();
            isSetLayoutParams = true;
            params.height += (padding * 2);
            params.width  += (padding * 2);
            setLayoutParams(params);
            requestLayout();
            postInvalidate();
        }

        if(isSelected)
            canvas.drawRoundRect(mRect, 100f, 100f, paint);
        canvas.drawRoundRect(mRect1, 100f, 100f, paint1);

        super.draw(canvas);
    }

    public void setSelected(boolean isSelected){
        if(this.isSelected != isSelected) {
            this.isSelected = isSelected;
            this.isFirst = true;
            if (isSelected) {
                mRect1 = new RectF();
                paint1 = new Paint();
                paint1.setStyle(Paint.Style.FILL);
                paint1.setAntiAlias(true);
                paint1.setShader(new LinearGradient(0, 0, 0, 150f, getResources().getColor(R.color.main_blue), getResources().getColor(R.color.main_blue_accent), Shader.TileMode.CLAMP));

                mRect = new RectF();
                paint = new Paint();
                paint = new Paint();
                paint.setStyle(Paint.Style.FILL);
                paint.setAntiAlias(true);
                paint.setColor(getResources().getColor(R.color.orange));
                paint.setMaskFilter(new BlurMaskFilter(23f, BlurMaskFilter.Blur.OUTER));
            } else {
                mRect1 = new RectF();
                paint1 = new Paint();
                paint1.setStyle(Paint.Style.FILL);
                paint1.setAntiAlias(true);
                paint1.setColor(getResources().getColor(R.color.white_30));
            }
            invalidate();
        }
    }

}

