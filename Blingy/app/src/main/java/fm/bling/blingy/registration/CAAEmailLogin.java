package fm.bling.blingy.registration;

import android.content.SharedPreferences;

import fm.bling.blingy.App;
import fm.bling.blingy.registration.rest.model.CAAPostToken;
import fm.bling.blingy.registration.rest.model.CAAToken;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAFacebookLogin
 * Description: Gets Email login info and sets CAAUserDataSingleton
 * Created by Zach Gerstman on 5/20/15.
 * History:
 * ***********************************
 */
public class CAAEmailLogin {

    private static CAAUserDataSingleton userDataSingleton =  CAAUserDataSingleton.getInstance();

    public static void EmailLogin(final String email, final String password, final CAALoginListener cAAlogin) {
        //make API call and get token

        CAAPostToken postToken = new CAAPostToken(email, password, Constants.APP_ID, Constants.APP_SECRET);
        postToken.setAuto(CAALogin.isAuto);
        App.getUrlService().postToken(postToken, new Callback<CAAToken>() {
            @Override
            public void success(CAAToken cAAToken, Response response) {
                String accessToken = cAAToken.getAccessToken();
                String firstName = cAAToken.getFirstName();
                String lastName = cAAToken.getLastName();
                String userId = cAAToken.getUserId();
                String photoUrl = cAAToken.getPhoto();

                if (firstName == null)
                    firstName = "";
                if(lastName == null)
                    lastName = "";
                userDataSingleton.setAccessExpireTime(cAAToken.getExpiresIn());
                updateEmailUserSingleton(accessToken, email, password, firstName, lastName, userId, photoUrl);
                SavePreferences(email, password);
                TrackingManager.getInstance().loginSubmit("email");
                cAAlogin.didFinishLogin(true, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                TrackingManager.getInstance().loginFailed(++CAALogin.failedLogins);
                if(error.getResponse() != null)
                    cAAlogin.didFinishLogin(false, error.getResponse().getStatus());
            }
        });
    }

//    public static void RefreshAccessToken(final Context context, String email, String password, final CAALoginListener cAALogin) {
//        //make API call and get token
//        CAAPostToken postToken = new CAAPostToken(email, password, Constants.APP_ID, Constants.APP_SECRET);
//        postToken.setAuto(CAALogin.isAuto);
//        App.getUrlService().postToken(postToken, new Callback<CAAToken>() {
//            @Override
//            public void success(CAAToken cAAToken, Response response) {
//                userDataSingleton.setAccessExpireTime(cAAToken.getExpiresIn());
//                CAAUserDataSingleton.getInstance().setAccessToken(cAAToken.getAccessToken());
//                CAAUserDataSingleton.getInstance().setUserId(context, cAAToken.getUserId());
//                cAALogin.didFinishLogin(true, response.getStatus());
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                if(error.getResponse() != null)
//                    cAALogin.didFinishLogin(false, error.getResponse().getStatus());
//            }
//        });
//    }

    /**
     * Updates user data singleton for email sign up users
     * @param accessToken token - can be null (won't update)
     * @param email string - can be null (won't update)
     * @param password string - can be null (won't update)
     */
    public static void updateEmailUserSingleton(String accessToken, String email, String password, String firstName, String lastName, String userId, String photoUrl) {
//        CAAUserDataSingleton.getInstance().setContext(context);
        CAAUserDataSingleton userDataSingleton = CAAUserDataSingleton.getInstance();
        if (!email.isEmpty()) {
            userDataSingleton.setEmail(email);
        }
        if (!password.isEmpty()) {
            userDataSingleton.setPassword(password);
        }
        if (!accessToken.isEmpty()) {
            userDataSingleton.setAccessToken(accessToken);
        }
        if (!lastName.isEmpty()) {
            userDataSingleton.setLastName(lastName);
        }
        if (!firstName.isEmpty()) {
            userDataSingleton.setFirstName(firstName);
        }
        if (!userId.isEmpty()) {
            userDataSingleton.setUserId(userId);
        }
        if (!photoUrl.isEmpty()) {
            userDataSingleton.setPhotoUrl(photoUrl);
        }
        userDataSingleton.setAccountType("email");
    }

    /**
     * Saves user data to shared preferences
     * @param email string
     * @param password string

     */
    public static void SavePreferences(String email, String password) {
        SharedPreferences settings = SharedPreferencesManager.getInstance();
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Constants.EMAIL, email);
        editor.putString(Constants.PASSWORD, password);
        editor.putString(Constants.ACCOUNT_TYPE, "email");
        editor.apply();
    }
}
