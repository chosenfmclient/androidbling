package fm.bling.blingy.mashup.listeners;

import java.util.ArrayList;

import fm.bling.blingy.mashup.model.InviteeContact;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 1/10/17.
 * History:
 * ***********************************
 */
public interface SelectedCallback {

    void onSelected(InviteeContact contact);

    void onRemove(InviteeContact contact);

    void onSelectedAll(ArrayList<InviteeContact> contacts);

}
