package fm.bling.blingy.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import fm.bling.blingy.App;
import fm.bling.blingy.R;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 9/25/16.
 * History:
 * ***********************************
 */
public class BaseDialog extends Dialog {

    private TextView mOkButton;
    protected TextView mCancelButton;

    private boolean mAnswer = false;

    public BaseDialog(Context context, String title, String message) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.base_dialog);
        TextView mTitle = (TextView) findViewById(R.id.dialog_title);
        TextView mMessage = (TextView) findViewById(R.id.dialog_info);
        mOkButton = (TextView) findViewById(R.id.ok_button);
        mCancelButton = (TextView)findViewById(R.id.cancel_button);

        mTitle.setText(title);
        mTitle.setTypeface(App.ROBOTO_MEDIUM);

        mMessage.setText(message);
        mMessage.setTypeface(App.ROBOTO_REGULAR);

        mCancelButton.setTag(this);

        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAnswer = true;
                dismiss();
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAnswer = false;
                dismiss();
            }
        });

    }

    public void setOKText(String text){
        mOkButton.setText(text);
    }

    public void setCancelText(String text){
        mCancelButton.setText(text);
    }

    public void setCancelOnclickListener(View.OnClickListener onclickListener){
        this.mCancelButton.setOnClickListener(onclickListener);
    }

    public void removeOKbutton(){
        mCancelButton.setVisibility(View.GONE);
    }

    public void removeCancelbutton(){
        mCancelButton.setVisibility(View.GONE);
    }

    public boolean getAnswer() {
        return mAnswer;
    }
}
