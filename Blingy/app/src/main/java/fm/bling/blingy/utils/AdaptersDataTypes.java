package fm.bling.blingy.utils;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 9/18/16.
 * History:
 * ***********************************
 */
public class AdaptersDataTypes {

    public static final int TYPE_LOADING = -1;
    public static final int TYPE_USER  = 0;
    public static final int TYPE_VIDEO = 1;
    public static final int TYPE_PHOTO = 2;
    public static final int FIRST_ITEM = 3;
    public static final int CONTACT    = 4;

    //notifications statuses
    public static final String UNREAD_STATUS  = "unread";
    public static final String READ_STATUS = "read";
    public static final String VIEWED_STATUS = "viewed";

    //item types
    public static final int HEADER = 1;
    public static final int BASE_ITEM = 2;

    public static final int TYPE_SECTION = 11;
}
