package fm.bling.blingy.utils;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.videoHome.model.CAAVideo;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 14/09/2016.
 * History:
 * ***********************************
 */
public class Constants
{
    //prod
     public static final String BASE_URL = "https://blingy-prod.appspot.com";
     public static final String APP_SECRET = "B42xvdYcREKQMLMX";
     public static final String APP_ID = "1";
     public static final String FLURRY_KEY = "82TMZM23KMCPG3P3KKY2";

    //stage
//    public static final String BASE_URL = "https://blingy-stage.appspot.com";
//    public static final String APP_SECRET = "fXX6FDAdV6fPPeBW";
//    public static final String APP_ID = "2";
//    public static final String FLURRY_KEY = "93YG5TFC3PSY7N2J9KFY";

    //testing
//    public static final String BASE_URL = "https://blingy-test.appspot.com";
//    public static final String APP_SECRET = "JAtjfpL9WdhYTG8F";
//    public static final String APP_ID = "3";
//    public static final String FLURRY_KEY = "93YG5TFC3PSY7N2J9KFY";

    //development
//    public static final String BASE_URL = "https://blingy-dev.appspot.com";
//    public static final String APP_SECRET = "tsGv8bbRbuutytYU";
//    public static final String APP_ID = "4";
//    public static final String FLURRY_KEY = "93YG5TFC3PSY7N2J9KFY";

    //demo
//    public static final String BASE_URL = "https://blingy-demo.appspot.com";
//    public static final String APP_SECRET = "PZNTeEjwqvrLUU79";
//    public static final String APP_ID = "6";
//    public static final String FLURRY_KEY = "93YG5TFC3PSY7N2J9KFY";

    //iTunes base url
    public static final String ITUNES_BASE_URL = "https://itunes.apple.com";

    public static final String API_BASE = "/api/v1/";

    //Utils
//    public static final String MASHUP_ID = "mashup_id";
//    public static final String MASHUP_TYPE = "mashup";

    public static final String ME = "me";
    public static final String USER_ID = "user_id_key";
    public static final String VIDEO_ID = "video_id";
    public static final String PARENT_ID = "parent_id";
    public static final String CLIP_ID = "clip_id";
    public static final String SHARE_ID = "share_id";
    public static final String VIDEO_TYPE = "video";
    public static final String CAMEO_TYPE = "cameo";
    public static final String CAMEO_INVITE = "cameo_invite";
    public static final String PHOTO_TYPE = "photo";
    public static final String USER_TYPE = "user";
    public static final String SONG_NAME = "song_name";
    public static final String ARTIST_NAME = "artist_name";
    public static final String USER_NAME = "user_name";
    public static final String PHOTO_MODE = "PM";
    public static final String SELECTED_TAB = "selected_tab";
    public static final String COMMENT_TYPE = "comment";
    public static final String INVITE_TYPE = "invite";
    public static final String ADD_SOUND_TYPE = "sound";
    public static final String TYPE = "type";
    public static final String UPLOAD = "upload";
//    public static final String COMMENT_ACTION = "comment_action";
//    public static final String LIKES_COUNT = "like_c";
//    public static final String COMMENTS_COUNT = "comment_c";
//    public static final String USER_PIC = "user_pic";
//    public static final String LIKE_BY_ME = "like_by_me";
    public static final String FROM_INVITE ="from_invite";
    public static final String CAAVIDEO = "CAAVideo";
    public static final String BUNDLE = "bundle";
    public static final String TIME_SEGMENTS = "TS";
    public static final String POS = "P";
    public static final String REMOVE_POS = "RP";
    public static final String IS_SONG_HOME = "SH";
    public static final String TERM = "TR";
    public static final String FIRST_SNAP = "FS";
    public static final String HOME_LAST_POS = "LP";
    public static final String PRIVATE = "private";
    public static final String DELETED = "deleted";
    public static final String PUBLIC = "public";
    public static final String SECRET = "secret";
    public static final String STATUS = "ST";
    public static final String FOLLOW_BY_ME = "FBM";
    public static final String HTTP = "http";
    public static final String FOCUS = "focus";
    public static final String PERMISSION_LEVEL = "permission_level";


    //Notifications types
    public static final String LEADERBOARD = "leaderboard";
    public static final String DUET_SUGGESTION = "duet_suggestion";
    public static final String DUET_REMINDER = "duet_reminder";



    //Permission granted
    public static final int RECORD_PERMISSION = 225;
    public static final int OPTIONAL_PERMISSION = 4;
    public static final int CRITICAL_PERMISSION = 6;
    public static final int STATE_MACHINE_PERMISSION_REQUEST = 214;


    //Activity Requests
    public static final int REQUEST_LOGIN = 5;
    public static final int GOOGLE_SIGN_IN = 9001;
    public static final int PICK_VIDEO_REQUEST = 66;
    public static final int TWITTER_VER_HANDLE_LOGIN = 5780;
    public static final int REQ_PROFILE_FOLLOWING = 324;
    public static final int REQ_VIDEO_UPDATE = 325;
    public static final int BLINGY_FAKE_DOWN_TIME = -234;

    //Time
    public static final int DUET_TURORIAL_TIME = 7000;

    //Intent filters
    public static final String AUDIO_FOCUS_CHANGED = "audio_focus_changed";


    //Twitter fileds
    public static final String TWITTER_ACCOUNT_TYPE = "com.twitter.android.auth.login";
    public static String mTwitterVerion = "";

    //Save External setting
    public static final int SAVE_EXTERNAL = 1;
    public static final int SAVE_INTERNAL = 0;
    public static final int NO_EXTERNAL_SETTING = -1;
    public static final String SAVE_SETTING = "save_external";
    public static final String RENDER_SERVICE = "RC";

    public static final String VIDEO_TYPE_EXTRA = "VT";
    public static final String FRONT_CAMERA = "FC";


    public static final String CLIP_URL = "CU";
    public static final String CLIP_NAME = "CN";
    public static final String CLIP_THUMBNAIL = "CT";

//    public static final String SOUNDTRACK_FRAGMENT = "SF";
//    public static final String SOUND_EFFECT_FRAGMENT = "SEF";

    public static final String SHARE_TAG = "share_tag";


    public static final String PORTRAIT = "portrait";

    /**
     * UserConnectionsActivity2
     */
    public static final int FOLLOWING = 0;
    public static final int FOLLOWERS = 1;
    public static final int CONTACTS = 2;
    public static final String FOLLOWING_AMOUNT = "FGSA";
    public static final String FOLLOWERS_AMOUNT = "FRSA";

    //Media types
    public static final String MEDIA_TYPE = "media_type";
    public static final String DANCE_OFF_TYPE = "dance_off";
    public static final String COVER_TYPE = "cover_freestyle";
    public static final String ORIGINAL_TYPE = "original_freestyle";
    public static final String UPLOAD_TYPE = "original_upload";
    public static final String PERFORMANCES = "performances";

    //RateApp keys
    public static final String FIRST_LAUNCH = "first_launch";
    public static final String INSTALL_DATE = "install_date";
    public static final String RATE_AFTER_UPLOAD_VIDEO = "rate_after_upload_video";

    //Discover keys
    public static final String DISCOVER_FRAGMENT = "d_fragment";
    public static final String SEARCH_FRAGMENT = "s_fragment";


    //Login type.
    public static final String GOOGLE_PLUS_ACC = "google_plus";
    public static final String FACEBOOK_ACC = "facebook";
    public static final String EMAIL_ACC = "email";

    /**
     * Edit Profile
     */
    public static final String FACEBOOK_LINK = "fb_link";
    public static final String TWITTER_LINK = "twi_link";
    public static final String INSTAGRAM_LINK = "inst_link";
    public static final String YOUTUBE_LINK = "yout_link";
    public static final String PHOTO_PATH = "ph_path";
    public static final String URI_PATH = "uri_path";

    /**
     * Shared Preferences keys
     */
    public static final String ACCESS_TOKEN = "access_token";
    public static final String ACCESS_EXPIRE_TIME = "access_expire_time";
    public static final String ACCOUNT_TYPE = "account_type";
    public static final String GOOGLE_TOKEN_ID = "google_token_id";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String PRIVACY_SHOWN = "privacy_shown";
    public static final String BACKGROUND_TUTORIAL = "background_tutorial";
    public static final String VIDEOS_CREATED = "Uploads";
    public static final String FIRST_DUET_TUTORIAL = "first_duet_tutorial";
    public static final String PLAY_DUET_TUTORIAL = "duet_tutorial";
    public static final String INVITED_TO_DUET = "invited_to_duet";
    public static final String ADDED_VIDEO_TO_10 = "added_video_10";
    public static final String DLIB_EXIST = "dlib_exist";


//    /**
//     * MapPoints
//     */
//    public static final String MAP_POINTS = "map_points";

    /**
     * Flags
     */
    public static final String IS_NOTIFICATION = "is_notification";
//    public static final String IS_AFTER_REC = "is_after_recording";

//    /**
//     * Paths
//     */
//    public static final String ORIGINAL_VIDEO_PATH = "original_video_path";
//    public static final String FRAME_1X1_PATH      = "frame_1x1_path";
//    public static final String LOGO_1X1_PATH       = "logo_1x1_path";

    /**
     * Comments
     */
    public static final String COMMENT_ID = "comment_id";
    public static final String COMMENT_TEXT = "comment_text";
    public static final String COMMENT_IMAGE = "comment_image";
    public static final String COMMENT_NEW = "comment_new";

    /**
     * Likes
     */
    public static final String LIKES_UPDATE = "likes_update";

    /**
     * SQLite
     */
    public static final String BLINGY_EVENT_SQLITE_DB = "blingy_events_sqlite_db";

    /**
     * Connectivity status
     */
    public static final String UNKNOWN = "UK";
    public static final String CONNECTED = "CN";
    public static final String NOT_CONNECTED = "NCN";

    /**
     * Calling activity
     */
    public static final String FROM = "F";
    public static final String VIDEO_PLAYER_ACTIVITY = "VP";
    public static final String PROFILE_ACTIVITY = "PROF_ACT";
//    public static final String COMMENTS_ACTIVITY = "CommentsActivity";
//    public static final String INVITE_MASHUP_SCREEN = "InviteFriendsMashup";
//    public static final String VIDEO_HOME_ACTIVITY = "VideoHomeActivity";


    public static String mNotificationCount;
    public static int mLastBarID = -1;
    public final static String thumbnailName = "Thumb_Blingy.jpg";
    public final static String thumbnail = App.VIDEO_WORKING_FOLDER + thumbnailName;
    public static boolean isFFmpegIsDoneProcessing = false;
    public static boolean isBackCamera;
    public static int uploadVideoProgressBar = 101;

    /**
     * Home screen feed
     **/
    public static ArrayList<CAAVideo> mVideoFeedData;
    public static int lastPosition = 0;
    public static int pageNum = 0;
    public static int previousLastItem = 0;

    /** Leaderboard **/
    public static ArrayList<User> mTopUsers;
    public static int leaderPageNum = 0;
    public static int leaderPreviousLastItem = 0;
    public static boolean leaderEndReached = false;



}
