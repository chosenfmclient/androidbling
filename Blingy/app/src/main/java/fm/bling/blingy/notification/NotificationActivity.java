package fm.bling.blingy.notification;

import android.os.Bundle;
import android.view.View;

import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.ScrollToTopBaseActivity;
import fm.bling.blingy.homeScreen.fragments.NotificationFragment;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/10/16.
 * History:
 * ***********************************
 */
public class NotificationActivity extends ScrollToTopBaseActivity {

    private NotificationFragment notificationFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_activity);
        TrackingManager.getInstance().pageView(EventConstants.NOTIFICATION_PAGE);
        notificationFragment = new NotificationFragment();
        getFragmentManager().beginTransaction().add(R.id.fragment_container, notificationFragment ).commit();
        setNotifCount("");
        getActionBarToolbar().setTitle(R.string.notifications);
        getWindow().setBackgroundDrawable(null);
    }

    @Override
    public void scrollToTop() {
        try {
            if (notificationFragment != null)
                notificationFragment.scrollToTop();
        }catch (Throwable throwable) { throwable.printStackTrace(); }
    }

    @Override
    public int getSelfID() {
        return NOTIFICATION_SCREEN;
    }
}
