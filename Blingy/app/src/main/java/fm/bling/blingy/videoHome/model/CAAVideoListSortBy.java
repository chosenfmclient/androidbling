package fm.bling.blingy.videoHome.model;

import com.google.gson.annotations.SerializedName;


import java.util.ArrayList;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAVideoListSortBy
 * Description:
 * Created by Dawidowicz Nadav on 5/10/15.
 * History:
 * ***********************************
 */
public class CAAVideoListSortBy {

    @SerializedName("date_added")
    private ArrayList<String> dateAdded;

    @SerializedName("most_viewed")
    private ArrayList<String> mostViewed;

    @SerializedName("rank_high_to_low")
    private ArrayList<String> rankHighToLow;

    public ArrayList<String> getDateAdded() {
        return dateAdded;
    }

    public ArrayList<String> getRankHighToLow() {
        return rankHighToLow;
    }

    public void setRankHighToLow(ArrayList<String> rankHighToLow) {
        this.rankHighToLow = rankHighToLow;
    }

    public ArrayList<String> getMostViewed() {
        return mostViewed;
    }

    public void setMostViewed(ArrayList<String> mostViewed) {
        this.mostViewed = mostViewed;
    }

    public void setDateAdded(ArrayList<String> dateAdded) {
        this.dateAdded = dateAdded;

    }
}

