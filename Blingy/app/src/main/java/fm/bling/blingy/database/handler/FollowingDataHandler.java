package fm.bling.blingy.database.handler;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import fm.bling.blingy.database.contract.FollowingContract;
import fm.bling.blingy.database.model.ContentValueWrapper;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 14/02/17.
 * History:
 * ***********************************
 */
public class FollowingDataHandler extends BaseHandler<String> {

    private final String TAG = "FollowingDataHandler";
    private static FollowingDataHandler mFollowingDataHandler;
    private static HashMap<String,Integer> mFollowingMap = new HashMap<>();

    private FollowingDataHandler(){

    }

    public static FollowingDataHandler getInstance(){
        if(mFollowingDataHandler == null)
            mFollowingDataHandler = new FollowingDataHandler();
        return mFollowingDataHandler;
    }

    @Override
    public long add(@NonNull String user_id) {
        mFollowingMap.put(user_id,1);
        ContentValueWrapper cvw = new ContentValueWrapper();
        cvw.setTableName(FollowingContract.TABLE_NAME);
        cvw.addStringValue(FollowingContract.COLUMN_USER_ID, user_id);
        return DataBaseManager.insertToDB(cvw);
    }

    @Override
    public long addMany(@NonNull ArrayList<String> user_ids) {
        if(user_ids.size() > 0) {
            StringBuilder query = new StringBuilder(FollowingContract.INSERT_MANY);
            Iterator<String> iterator = user_ids.iterator();
            while (iterator.hasNext()) {
                String user_id = iterator.next();
                mFollowingMap.put(user_id, 1);
                query.append("(").append(user_id).append(")").append(iterator.hasNext() ? ",\n" : ";");
            }
            DataBaseManager.excQuery(query.toString());
            return user_ids.size();
        }
        return 0;
    }

    @Override
    public boolean remove(@NonNull String user_id) {
        mFollowingMap.remove(user_id);
        ContentValueWrapper cvw = new ContentValueWrapper();
        cvw.setTableName(FollowingContract.TABLE_NAME);
        cvw.setWhereClause(FollowingContract.COLUMN_USER_ID + " LIKE \'" + user_id + "\'");
        return DataBaseManager.deleteFromDB(cvw);
    }

    @Override
    public boolean update(@NonNull String user_id) {
        ContentValueWrapper cvw = new ContentValueWrapper();
        cvw.addStringValue(FollowingContract.COLUMN_USER_ID, user_id);
        return DataBaseManager.updateDB(cvw);
    }

    @Override
    public String get(@NonNull String user_id) {
        if(mFollowingMap.get(user_id) != null)
            return user_id;
        else
            return DataBaseManager.getFollowing(user_id);
    }

    @Override
    public ArrayList<String> getAll(String filter) {
        ArrayList<String> ids = DataBaseManager.getAllFollowing(filter);
        for (String id : ids) {
            mFollowingMap.put(id, 1);
        }
        return ids;
    }

    public boolean contains(String user_id){
//        System.out.println(user_id+">>>>ifollowu>>>"+mFollowingMap.size());
        return mFollowingMap.containsKey(user_id);
    }

    public int size(){
        if(mFollowingMap == null)
            return 0;
        else
            return mFollowingMap.size();
    }

    public void clear()
    {
        ArrayList<String> keys = new ArrayList(mFollowingMap.keySet());
        for(int i = 0; i < keys.size(); i++)
        {
            remove(keys.get(i));
        }
    }

}
