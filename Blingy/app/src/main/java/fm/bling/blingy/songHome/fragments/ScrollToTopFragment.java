package fm.bling.blingy.songHome.fragments;

import android.app.Fragment;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 11/01/17.
 * History:
 * ***********************************
 */
public abstract class ScrollToTopFragment extends Fragment{

    public abstract void scrollToTop();
}
