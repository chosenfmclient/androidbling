package fm.bling.blingy.widget.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import fm.bling.blingy.rest.model.User;


/**
 * Created by Chosen-pro on 17/07/2016.
 */

public class CAAInviteWidgetSettingsResponse {
    @SerializedName("settings")
    private ArrayList<CAAInviteWidgetSetting> settings;

    @SerializedName("me")
    private User me;

    @SerializedName("points")
    private int myPoints;

    @SerializedName("action_worth")
    private int inviteWorth;

    @SerializedName("points_settings")
    private ArrayList<CAAInvitePrize> prizes;

    @SerializedName("invite_exclusion_time")
    private int exclusionTime = 7 * 24 * 60 * 60;

    @SerializedName("email_content")
    private String emailText;


    public ArrayList<CAAInviteWidgetSetting> getSettings() {
        return settings;
    }

    public void setSettings(ArrayList<CAAInviteWidgetSetting> settings) {
        this.settings = settings;
    }

    public User getMe() {
        return me;
    }

    public void setMe(User me) {
        this.me = me;
    }

    public int getMyPoints() {
        return myPoints;
    }

    public int getInviteWorth() {
        return inviteWorth;
    }

    public ArrayList<CAAInvitePrize> getPrizes() {
        return prizes;
    }

    public void setPrizes(ArrayList<CAAInvitePrize> prizes) {
        this.prizes = prizes;
    }

    public int getExclusionTime() {
        return exclusionTime;
    }

    public String getEmailText() {
        return emailText;
    }

    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }
}
