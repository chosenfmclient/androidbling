package fm.bling.blingy.dialogs.share.model;

import com.google.gson.annotations.SerializedName;


/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAANativeVideo
 * Description:
 * Created by Zach Gerstman on 8/31/2015.
 * History:
 * ***********************************
 */
public class CAANativeVideo {
    public String getNativeVideo() {
        return nativeVideo;
    }

//    public void setNativeVideo(String nativeVideo) {
//        this.nativeVideo = nativeVideo;
//    }

    @SerializedName("native_video")
    private String nativeVideo;
}
