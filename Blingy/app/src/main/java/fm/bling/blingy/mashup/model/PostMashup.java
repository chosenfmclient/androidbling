package fm.bling.blingy.mashup.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 1/8/17.
 * History:
 * ***********************************
 */
public class PostMashup {

    @SerializedName("video_id")
    private String videoID;

    @SerializedName("parent_id")
    private String parentID;

    public PostMashup(String videoID){
        this.videoID = videoID;
        this.parentID = null;
    }

    public PostMashup(String videoID, String parentID){
        this.videoID = videoID;
        this.parentID = parentID;
    }
}
