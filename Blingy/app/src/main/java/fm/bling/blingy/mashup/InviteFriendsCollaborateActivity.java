//package fm.bling.blingy.mashup;
//
//import android.Manifest;
//import android.animation.ObjectAnimator;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.TypedValue;
//import android.view.Gravity;
//import android.view.View;
//import android.view.ViewTreeObserver;
//import android.view.animation.Animation;
//import android.view.animation.LinearInterpolator;
//import android.widget.EditText;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//
//import java.util.ArrayList;
//import java.util.Timer;
//import java.util.TimerTask;
//
//import fm.bling.blingy.App;
//import fm.bling.blingy.BaseActivity;
//import fm.bling.blingy.R;
//import fm.bling.blingy.dialogs.BaseDialog;
//import fm.bling.blingy.discover.DiscoverActivity;
//import fm.bling.blingy.inviteFriends.InviteHandler;
//import fm.bling.blingy.inviteFriends.listeners.InviteDataReadyListener;
//import fm.bling.blingy.inviteFriends.model.ContactInfo;
//import fm.bling.blingy.inviteFriends.model.ContactsInviteeEmail;
//import fm.bling.blingy.mashup.listeners.SelectedCallback;
//import fm.bling.blingy.mashup.model.Invitee;
//import fm.bling.blingy.mashup.model.InviteeContact;
//import fm.bling.blingy.mashup.model.InviteeContactsResponse;
//import fm.bling.blingy.record.utils.SimpleDividerItemDecoration;
//import fm.bling.blingy.rest.model.User;
//import fm.bling.blingy.tracking.EventConstants;
//import fm.bling.blingy.tracking.TrackingManager;
//import fm.bling.blingy.utils.Constants;
//import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
//import fm.bling.blingy.utils.views.CountDownItem;
//import fm.bling.blingy.utils.views.TextViewRegular;
//import retrofit.Callback;
//import retrofit.RetrofitError;
//import retrofit.client.Response;
//
///**
// * *********************************
// * Project: Blin.gy Android Application
// * Description:
// * Created by Oren Zakay on 1/4/17.
// * History:
// * ***********************************
// */
//public class InviteFriendsCollaborateActivity extends BaseActivity implements InviteDataReadyListener, SelectedCallback, View.OnClickListener {
//
//    /**
//     * Main Views
//     */
////    private ImageView mBackButton;
////    private TextView mInviteButton;
//    private TextView mTitle;
//    private EditText mEditSearch;
//    private ImageView mSearchButton;
//    private FrameLayout mMainContent;
//    private ImageView spinner;
//    private ObjectAnimator anim;
//    private LinearLayout mImagesGroup;
//
//    private RecyclerView mInviteMasheupRecycler;
//    private InviteMashupAdapter mInviteMasheupAdapter;
//    private LinearLayoutManager mLayoutManager;
//
//    private String mVideoID;
////    private String mMashupID;
//    private ArrayList<InviteeContact> mContactsList = new ArrayList<>();
//    private ArrayList<InviteeContact> mInviteeContacts = new ArrayList<>();
//    private ArrayList<InviteeContact> mGroupContacts = new ArrayList<>();
//
//    private int groupImgWithHeight = (int)(120f * App.SCALE_X);
//    private int margin = (int)(20f * App.SCALE_X);
//    private int maxGroupImages = 5;
//    private int lastSearchLength;
//    private CountDownItem mCountImage;
//    private Timer timer;
//    private int followingSize, contactsSize;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.invite_friends_mashup);
//        TrackingManager.getInstance().pageView(EventConstants.CAMEO_INVITE_PAGE);
//        if(getIntent().hasExtra(Constants.VIDEO_ID))
//            mVideoID = getIntent().getStringExtra(Constants.VIDEO_ID);
//        else
//            finish();
//
//        init();
//        loadAnimation();
//
//        if (InviteHandler.contacts == null && PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS))
//            new InviteHandler(getApplicationContext(), this, true);
//        else
//            onInviteDataReady();
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        closeKeyboard();
//    }
//
//    public void init(){
////        mBackButton = (ImageView) findViewById(R.id.back_button);
////        mInviteButton = (TextView) findViewById(R.id.inivite);
//        mTitle = (TextView) findViewById(R.id.bar_title);
//        mEditSearch = (EditText) findViewById(R.id.bar_search);
//        mSearchButton = (ImageView) findViewById(R.id.search_button);
//        mMainContent = (FrameLayout) findViewById(R.id.main_content);
//        spinner = (ImageView)findViewById(R.id.spinner);
//        mInviteMasheupRecycler = (RecyclerView)findViewById(R.id.recycler_view);
//        mImagesGroup = (LinearLayout) findViewById(R.id.images_group);
//
//        mSearchButton.setOnClickListener(this);
//        findViewById(R.id.back_button).setOnClickListener(this);
//        findViewById(R.id.inivite).setOnClickListener(this);
//        mEditSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (timer != null) {
//                    timer.cancel();
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                timer = new Timer();
//                timer.schedule(new TimerTask() {
//                    @Override
//                    public void run() {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                onSearch(mEditSearch.getText().toString());
//                            }
//                        });
//                    }
//                }, 600);
//            }
//
//        });
//
//        mLayoutManager = new LinearLayoutManager(getApplicationContext());
//        mInviteMasheupRecycler.setLayoutManager(mLayoutManager);
//        mInviteMasheupRecycler.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext(), R.drawable.line_divider, true));
//
//        mImagesGroup.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                maxGroupImages = mImagesGroup.getWidth() / (groupImgWithHeight + margin);
//                mImagesGroup.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//            }
//        });
//
//        mCountImage = new CountDownItem(this,"", R.drawable.circle_shape_grey);
//        mCountImage.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
//        mCountImage.setTextColor(R.color.grey_font);
//        mCountImage.setVisibility(View.GONE);
//        mCountImage.getLayoutParams().width = mCountImage.getLayoutParams().height = groupImgWithHeight;
//        mImagesGroup.addView(mCountImage, 0);
//        ((LinearLayout.LayoutParams)mCountImage.getLayoutParams()).rightMargin = margin;
//
//        mGroupContacts.add(new InviteeContact(null,null,null,null, -1 ,false));
//    }
//
//    private void cancelAnimation() {
//        anim.cancel();
//        spinner.setVisibility(View.GONE);
//
//    }
//
//    private void loadAnimation() {
//        spinner.setVisibility(View.VISIBLE);
//        anim = ObjectAnimator.ofFloat(spinner, "rotation", 0f, 359f);
//        anim.setInterpolator(new LinearInterpolator());
//        anim.setDuration(600);
//        anim.setRepeatCount(Animation.INFINITE);
//        anim.start();
//    }
//
//    private void getFollowingAndContacts(){
//        App.getUrlService().postFollowingEmailToRemove(new ContactsInviteeEmail(InviteHandler.emails, mVideoID), new Callback<InviteeContactsResponse>() {
//            @Override
//            public void success(InviteeContactsResponse inviteeContactsResponse, Response response) {
//                if(InviteHandler.contacts == null || (InviteHandler.contacts != null && InviteHandler.contacts.size() < 1) && inviteeContactsResponse.getFollowings().size() < 1)
//                    showEmptyLayout();
//                else {
//                    followingSize = 0;
//                    if (inviteeContactsResponse != null) {
//
//                        ArrayList<InviteeContactsResponse.EmailInvited> invitedEmails = inviteeContactsResponse.getEmailsInvited();
//                        ArrayList<InviteeContactsResponse.PhoneInvited> invitedPhones = inviteeContactsResponse.getPhonesInvited();
//
//                        // Add the following
//                        if (inviteeContactsResponse.getFollowings() != null && inviteeContactsResponse.getFollowings().size() > 0) {
//                            mInviteeContacts.add(new InviteeContact(Constants.FOLLOWING, (followingSize = inviteeContactsResponse.getFollowings().size()) + " Following"));
//                            for (int i = 0; i < inviteeContactsResponse.getFollowings().size(); i++) {
//                                User user = inviteeContactsResponse.getFollowings().get(i).getUser();
//                                boolean isInvited = inviteeContactsResponse.getFollowings().get(i).getHasInvited();
//                                mInviteeContacts.add(new InviteeContact(user.getFullName(), user.getUserId(), user.getPhoto(), Constants.FOLLOWING, isInvited));
//                            }
//                            mContactsList.addAll(mInviteeContacts.subList(1, mInviteeContacts.size()));
//                        }
//
//
//                        // Add the contacts
//                        contactsSize = 0;
//                        if (InviteHandler.contacts != null && InviteHandler.contacts.size() > 0) {
//                            ArrayList<InviteeContact> contacts = new ArrayList<>();
//                            ArrayList<String> emailsToemove = inviteeContactsResponse.getEmailsToRemove();
//                            if (emailsToemove != null && emailsToemove.size() > 0) {
//                                for (int i = 0; i < InviteHandler.contacts.size(); i++) {
//                                    ContactInfo user = InviteHandler.contacts.get(i);
//                                    if (!emailsToemove.contains(user.getEmail())) {
//                                        boolean isInvited = false;
//                                        for(int j  = 0; j < invitedEmails.size();j++){
//                                            if(invitedEmails.get(j).getEmail().equals(user.getEmail())) {
//                                                isInvited = true;
//                                                break;
//                                            }
//                                        }
//                                        if(!isInvited) {
//                                            for (int k = 0; k < invitedPhones.size(); k++){
//                                                if(user.phone() != null && invitedPhones.get(k).getPhone().equals(user.phone())) {
//                                                    isInvited = true;
//                                                    break;
//                                                }
//                                            }
//                                        }
//                                        contacts.add(new InviteeContact(user.getName(), user.phone(), user.getEmail(), user.getPhotoUrl(), Constants.CONTACTS, isInvited));
//                                    }
//                                }
//                                mInviteeContacts.add(new InviteeContact(Constants.CONTACTS, (contactsSize = contacts.size()) + " Contacts"));
//                                mInviteeContacts.addAll(contacts);
//                                mContactsList.addAll(contacts);
//                            } else {
//                                mInviteeContacts.add(new InviteeContact(Constants.CONTACTS, (contactsSize = InviteHandler.contacts.size()) + " Contacts"));
//                                for (int i = 0; i < InviteHandler.contacts.size(); i++) {
//                                    ContactInfo user = InviteHandler.contacts.get(i);
//                                    boolean isInvited = false;
//                                    for(int j  = 0; j < invitedEmails.size();j++){
//                                        if(invitedEmails.get(j).getEmail().equals(user.getEmail())) {
//                                            isInvited = true;
//                                            break;
//                                        }
//                                    }
//                                    if(!isInvited) {
//                                        for (int k = 0; k < invitedPhones.size(); k++){
//                                            if(user.phone() != null && invitedPhones.get(k).getPhone().equals(user.phone())) {
//                                                isInvited = true;
//                                                break;
//                                            }
//                                        }
//                                    }
//                                    mInviteeContacts.add(new InviteeContact(user.getName(), user.phone(), user.getEmail(), user.getPhotoUrl(), Constants.CONTACTS, isInvited));
//                                }
//                                mContactsList.addAll(mInviteeContacts.subList(followingSize + 1, mInviteeContacts.size()));
//                            }
//                        }
//
//
//
//                        mInviteMasheupAdapter = new InviteMashupAdapter(getImageLoaderManager(), mInviteeContacts, InviteFriendsCollaborateActivity.this);
//                        mInviteMasheupAdapter.setContactsSize(contactsSize);
//                        mInviteMasheupAdapter.setFollowingSize(followingSize);
//                        mInviteMasheupRecycler.setAdapter(mInviteMasheupAdapter);
//                        cancelAnimation();
//                        mMainContent.setVisibility(View.VISIBLE);
//                        findViewById(R.id.bottom_shadow).setVisibility(View.VISIBLE);
//                        findViewById(R.id.bottom_invite_bar).setVisibility(View.VISIBLE);
//                        mSearchButton.setVisibility(View.VISIBLE);
//                    }
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Toast.makeText(InviteFriendsCollaborateActivity.this,"Error invite mashup", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    @Override
//    public void onInviteDataReady() {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    getFollowingAndContacts();
//                }
//            });
//    }
//
//    public void showEmptyLayout(){
//        View emptyView = getLayoutInflater().inflate(R.layout.empty_fragment_social, null);
//        TextView text = (TextView)emptyView.findViewById(R.id.bios_nothing);
//        text.setText("Find some people to follow");
//        text.setOnClickListener(this);
//        mMainContent.removeView(mInviteMasheupRecycler);
//        mMainContent.removeView(findViewById(R.id.bottom_invite_bar));
//        mMainContent.removeView(findViewById(R.id.bottom_shadow));
//        mMainContent.addView(emptyView, 1);
//        ((FrameLayout.LayoutParams)emptyView.getLayoutParams()).topMargin = App.TOOLBAR_HEIGHT;
//        cancelAnimation();
//    }
//
//    private View prepareContactImage(InviteeContact contact){
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(groupImgWithHeight, groupImgWithHeight);
//        params.rightMargin = margin;
//
//        if(contact.getPhotoUrl() != null) {
//        ScaledImageView imageView = new ScaledImageView(this);
//        imageView.setLayoutParams(params);
//        imageView.setImageLoader(getImageLoaderManager());
//            imageView.setUrl(contact.getPhotoUrl());
//            getImageLoaderManager().DisplayImage(imageView);
//            return imageView;
//        }
//        else {
//            String firstLetter = "";
//            if(contact.getName() != null && !contact.getName().isEmpty())
//                firstLetter = contact.getName().substring(0,1).toUpperCase();
//            TextViewRegular contactLetter = new TextViewRegular(this);
//            contactLetter.setLayoutParams(params);
//            contactLetter.setText(firstLetter);
//            contactLetter.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
//            contactLetter.setTextColor(Color.WHITE);
//            contactLetter.setGravity(Gravity.CENTER);
//            contactLetter.setBackgroundResource(R.drawable.circle_shape_main_blue);
//            return contactLetter;
//        }
//
//    }
//
//    private void addContactToGrouop(InviteeContact contact) {
//        if(contact != null && !mGroupContacts.contains(contact)){
//            mGroupContacts.add(1, contact);
//            if(mImagesGroup.getChildCount() - 1 >= maxGroupImages - 1){
//                mCountImage.updateText("+" + (mGroupContacts.size() - maxGroupImages));
//                if(mCountImage.getVisibility() == View.GONE) {
//                    mCountImage.setVisibility(View.VISIBLE);
//                }
//                mImagesGroup.removeViewAt(maxGroupImages - 1);
//            }
//            mImagesGroup.addView(prepareContactImage(contact), 1);
//        }
//    }
//
//    private void removeContactFromGroup(InviteeContact contact){
//        if(contact != null && mGroupContacts.contains(contact)){
//            int index = mGroupContacts.indexOf(contact);
//            mGroupContacts.remove(contact);
//            if (mCountImage.getVisibility() == View.VISIBLE) {
//                if(index >= 1 && index < maxGroupImages){// the index is visible to the user
//
//                    if(mGroupContacts.size() == maxGroupImages){//we can put the whole list visible to the user
//                        mCountImage.setVisibility(View.GONE);
//                        mCountImage.updateText("");
//                        mImagesGroup.removeViewAt(index);
//                        mImagesGroup.addView(prepareContactImage(mGroupContacts.get(mGroupContacts.size() - 1)));
//                    }else{
//                        //we have more contacts than max images, remove the index and get the next contacts to show.
//                        mImagesGroup.removeViewAt(index);
//                        mImagesGroup.addView(prepareContactImage(mGroupContacts.get(maxGroupImages - 1)));
//                        mCountImage.updateText("+" + (mGroupContacts.size() - maxGroupImages));
//                    }
//                }
//                else{
//                    if(mGroupContacts.size() == maxGroupImages) {//we can put the whole list visible to the user
//                        mCountImage.setVisibility(View.GONE);
//                        mCountImage.updateText("");
//                    }
//                    //the index is not visible to the user so we just need to update the text.
//                    mCountImage.updateText("+" + (mGroupContacts.size() - maxGroupImages));
//                }
//            } else {
//                mImagesGroup.removeViewAt(index);
//            }
//        }
//
//    }
//
//    private void openSearch(){
//        mTitle.setVisibility(View.GONE);
//        mSearchButton.setVisibility(View.INVISIBLE);
//        mEditSearch.setVisibility(View.VISIBLE);
//        openKeyboard(mEditSearch);
//    }
//
//    private void invitePeople(){
//        if(mGroupContacts.size() > 1){
//            ArrayList<InviteeContact> sendList = new ArrayList<>(mGroupContacts.subList(1, mGroupContacts.size()));
//            mInviteMasheupAdapter.notifyDataSetChanged();
//            TrackingManager.getInstance().tapInviteFriendsCameo(sendList.size());
//            App.getUrlService().postCameoInvite(new Invitee(sendList, mVideoID), new Callback<String>() {
//                @Override
//                public void success(String s, Response response) {
//                    BaseDialog invitationsSent = new BaseDialog(InviteFriendsCollaborateActivity.this, getString(R.string.invitations_sent), getString(R.string.invitations_has_been_successfully_sent));
//                    invitationsSent.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                        @Override
//                        public void onDismiss(DialogInterface dialog) {
//                            finish();
//                        }
//                    });
//                    invitationsSent.removeCancelbutton();
//                    invitationsSent.show();
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                }
//            });
//        }
//        else{
//            Toast.makeText(this,"Please select friends first.", Toast.LENGTH_SHORT).show();
//        }
//    }
//
////    private void moveToMashupHome(){
////        StateMachine.getInstance(this).updateStep(mMashupID, 3);
////        Intent mashupHome = new Intent(this, MashupHomeActivity.class);
////        mashupHome.putExtra(Constants.MASHUP_ID, mMashupID);
////        mashupHome.putExtra(Constants.FROM, Constants.INVITE_MASHUP_SCREEN);
////        startActivity(mashupHome);
////        finish();
////    }
//
//    private void openDiscover(){
//        Intent discover = new Intent(this, DiscoverActivity.class);
//        startActivity(discover);
//    }
//
//    @Override
//    public void onSelected(InviteeContact contact) {
//        addContactToGrouop(contact);
//    }
//
//    @Override
//    public void onRemove(InviteeContact contact) {
//        removeContactFromGroup(contact);
//    }
//
//    @Override
//    public void onSelectedAll(ArrayList<InviteeContact> contacts) {
//        if(contacts == null)
//            return;
//        for (int i = 0; i < contacts.size(); i++) {
//            addContactToGrouop(contacts.get(i));
//        }
//    }
//
//    public void onSearch(String text) {
//        if(mInviteeContacts == null || mInviteMasheupAdapter == null)
//            return;
//
//        String search = text.toLowerCase();
//        if(lastSearchLength < search.length())
//        {
//            for(int i = mInviteeContacts.size() - 1; i > -1; i--)
//            {
//                if(mInviteeContacts.get(i).getName() != null && !mInviteeContacts.get(i).getName().toLowerCase().contains(search))
//                {
//                    mInviteeContacts.remove(i);
//                }
//            }
//        }
//        else
//        {
//            if(search.length() > 0)
//            {
//                if(followingSize > 1)
//                    mInviteeContacts.subList(1, contactsSize > 0 ? mInviteMasheupAdapter.getHeaderPositionByType(Constants.CONTACTS) : mInviteeContacts.size()).clear();
//                if(contactsSize > 1)
//                    mInviteeContacts.subList(mInviteMasheupAdapter.getHeaderPositionByType(Constants.CONTACTS) + 1, mInviteeContacts.size()).clear();
//
//                for(int i=0; i < mContactsList.size(); i++)
//                {
//                    if(!mInviteeContacts.contains(mContactsList.get(i))){
//                        switch (mContactsList.get(i).getType()) {
//                            case Constants.FOLLOWING:
//                                if (mContactsList.get(i).getName().toLowerCase().contains(search)) {
//                                    if (contactsSize > 1)
//                                        mInviteeContacts.add(mInviteMasheupAdapter.getHeaderPositionByType(Constants.CONTACTS), mContactsList.get(i));
//                                    else
//                                        mInviteeContacts.add(mContactsList.get(i));
//                                }
//                                break;
//                            case Constants.CONTACTS:
//                                if (mContactsList.get(i).getName().toLowerCase().contains(search))
//                                    mInviteeContacts.add(mContactsList.get(i));
//                                break;
//                        }
//                    }
//
//
//                }
//            }
////            else
////            {
////                for(int i=0; i < mContactsList.size(); i++)
////                {
////                    mMashupContacts.add(mContactsList.get(i));
////                }
////            }
//        }
//        mInviteMasheupAdapter.notifyDataSetChanged();
//        lastSearchLength = search.length();
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.back_button:
//                onBackPressed();
//                break;
//            case R.id.search_button:
//                openSearch();
//                break;
//            case R.id.inivite:
//                invitePeople();
//                break;
//            case R.id.linear_container:
//                openDiscover();
//                break;
//        }
//    }
//}
