//package fm.bling.blingy.profile;
//
//
//import android.animation.ObjectAnimator;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//import android.view.View;
//import android.view.animation.Animation;
//import android.view.animation.LinearInterpolator;
//import android.widget.AbsListView;
//import android.widget.AdapterView;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import com.flurry.android.FlurryAgent;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//
//import fm.bling.blingy.App;
//import fm.bling.blingy.BaseActivity;
//import fm.bling.blingy.R;
//import fm.bling.blingy.profile.adapters.UserConnectionListAdapter;
//import fm.bling.blingy.profile.model.CAAApiUsers;
//import fm.bling.blingy.rest.model.User;
//import fm.bling.blingy.tracking.FlurryConstants;
//import fm.bling.blingy.tracking.TrackingManager;
//import fm.bling.blingy.utils.Constants;
//import retrofit.Callback;
//import retrofit.RetrofitError;
//import retrofit.client.Response;
//
///**
// * Followers / Following window.
// */
//public class UserConnectionsActivity extends BaseActivity {
//
//    private ImageView spinner;
//    private ListView listView;
//    private FrameLayout loadingLayout;
//    private FrameLayout listViewEmpty;
//    private TextView listHeader;
//
//    public static final int FOLLOWERS = 1;
//    public static final int FOLLOWING = 2;
//    private final int TYPE_LOADING = -1;
//    private ArrayList<User> items;
//    private ObjectAnimator anim;
//    private String userID;
//    private int typeOfConnection;
//    private int numberOfConnections = 0;
//    private int previousLastItem = 0;
//    private Boolean endPages = false;
//    private Boolean calling = false;
//    private Boolean listInitialized = false;
//    private int pageNumber = 0;
//    private UserConnectionListAdapter adapter;
//
//    private boolean isInFront = false;
//
//    public static final String CONNECTIONSTYPE = "ConnectionsType";
//    public static final String CONNECTIONSAMOUNT = "ConnectionsAmout";
//
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.fragment_user_connections);
//        init();
//    }
//
//    private void init(){
//        spinner = (ImageView) findViewById(R.id.spinner);
//        listView = (ListView) findViewById(R.id.list_view_user_connections);
//        loadingLayout = (FrameLayout) findViewById(R.id.loading_layout);
//        listViewEmpty = (FrameLayout) findViewById(R.id.activities_empty);
//        listHeader = (TextView) findViewById(R.id.text_view_list_header);
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        loadAnimation();
//        listInitialized = false;
//        if(adapter != null && adapter.getCount() > 0) {
//            pageNumber = 0;
//            items.clear();
//            adapter.clear();
//            adapter.notifyDataSetChanged();
//            listView.postInvalidate();
//        }
//        if (getIntent().getExtras() != null) {
//            Bundle extras = getIntent().getExtras();
//            userID = extras.getString(Constants.USER_ID);
//            typeOfConnection = extras.getInt(CONNECTIONSTYPE);
//            numberOfConnections = extras.getInt(CONNECTIONSAMOUNT);
//        }
//
//        if (typeOfConnection == FOLLOWERS) {
//            getFollowers();
//            TrackingManager.getInstance().pageViewFollowers();
//        } else if (typeOfConnection == FOLLOWING) {
//            getFollowing();
//            TrackingManager.getInstance().pageViewFollowing();
//        }
//
//        listViewEmpty.setVisibility(View.INVISIBLE);
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                if(adapter != null && adapter.getItemViewType(position) != TYPE_LOADING){
//                    Intent i = new Intent(UserConnectionsActivity.this, ProfileActivity.class);
//                    String user_id =items.get(position).getUserId();
//                    i.putExtra(Constants.USER_ID, user_id);
//                    startActivity(i);
//                    Map<String, String> userParams = new HashMap<String, String>();
//                    userParams.put(FlurryConstants.POS_NUM_FLURRY,"" + position);
//                    userParams.put(FlurryConstants.USER_ID_FLURRY,user_id);
//
//                    if(typeOfConnection == FOLLOWERS) {
//                        FlurryAgent.logEvent(FlurryConstants.FOLLOWERS_PAGE_TAP_USER_FLURRY,userParams);
//                    } else if (typeOfConnection == FOLLOWING) {
//                        FlurryAgent.logEvent(FlurryConstants.FOLLOWING_PAGE_TAP_USER_FLURRY,userParams);
//                    }
//                }
//
//
//            }
//        });
//
//        Toolbar mToolBar = getActionBarToolbar();
//        if(mToolBar != null) {
//            if (typeOfConnection == FOLLOWERS) {
//                getSupportActionBar().setTitle("Followers");
//            } else if (typeOfConnection == FOLLOWING) {
//                getSupportActionBar().setTitle("Following");
//            }
//            mToolBar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
//            mToolBar.setNavigationContentDescription("backClose");
//            mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    onBackPressed();
//                }
//            });
//
//        }
//
//
//
//        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                if (listInitialized) {
//                    final int lastItem = firstVisibleItem + visibleItemCount;
//                    if (lastItem >= totalItemCount - 4 && !calling && !endPages) {
//                        if (previousLastItem != lastItem) { //to avoid multiple calls for last item
//                            Log.d("Scroll", "Last:" + lastItem);
//                            previousLastItem = lastItem;
//                            getNextPage();
//                        }
//                    }
//                }
//            }
//        });
//    }
//
//    @Override
//    public void onBackPressed() {
//        if(typeOfConnection == FOLLOWERS)
//            TrackingManager.getInstance().tapBack(FlurryConstants.FOLLOWERS_PAGE_FLURRY);
//        else
//            TrackingManager.getInstance().tapBack(FlurryConstants.FOLLOWING_PAGE_FLURRY);
//        super.onBackPressed();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        isInFront = true;
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        isInFront = false;
//    }
//
//    public void cancelAnimation() {
//        anim.cancel();
//        loadingLayout.setVisibility(View.GONE);
//
//    }
//
//    private void loadAnimation() {
//        loadingLayout.setVisibility(View.VISIBLE);
//        anim = ObjectAnimator.ofFloat(spinner, "rotation", 0f, 359f);
//        anim.setInterpolator(new LinearInterpolator());
//        anim.setDuration(600);
//        anim.setRepeatCount(Animation.INFINITE);
//        anim.start();
//    }
//
//    private void getFollowers() {
//        App.getUrlService(this).getSpecificUserFollowersPage(userID, pageNumber, new Callback<CAAApiUsers>() {
//            @Override
//            public void success(CAAApiUsers caaApiUsers, Response response) {
//                if (isInFront) {
//                    cancelAnimation();
//                    items = caaApiUsers.getData();
//                    if (items.size() < 1 || items.size() == numberOfConnections) {
//                        endPages = true;
//                    } else {
//                        items.add(new User());
//                    }
//                    listView.setEmptyView(listViewEmpty);
//                    adapter = new UserConnectionListAdapter(UserConnectionsActivity.this, items, typeOfConnection);
//                    listHeader.setText("Followed by " + numberOfConnections + " people");
//                    listView.setAdapter(adapter);
//                    listInitialized = true;
//                }
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        });
//
//    }
//
//    private void getFollowing() {
//        App.getUrlService(this).getSpecificUserFollowingPage(userID, pageNumber, new Callback<CAAApiUsers>() {
//            @Override
//            public void success(CAAApiUsers caaApiUsers, Response response) {
//                if (isInFront) {
//                    cancelAnimation();
//                    items = caaApiUsers.getData();
//                    if (items.size() < 1 || items.size() == numberOfConnections) {
//                        endPages = true;
//                    } else {
//                        items.add(new User());
//                    }
//                    listView.setEmptyView(listViewEmpty);
//                    adapter = new UserConnectionListAdapter(UserConnectionsActivity.this, items, typeOfConnection);
//                    listView.setAdapter(adapter);
//                    listHeader.setText("Following " + numberOfConnections + " people");
//                    listInitialized = true;
//                }
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        });
//
//    }
//
//    private void getNextPage() {
//        pageNumber++;
//        calling = true;
//        if (typeOfConnection == FOLLOWERS) {
//            getFollowersPage();
//        } else if (typeOfConnection == FOLLOWING) {
//            getFollowingPage();
//        }
//    }
//
//    private void getFollowersPage() {
//        App.getUrlService(this).getSpecificUserFollowersPage(userID, pageNumber, new Callback<CAAApiUsers>() {
//            @Override
//            public void success(CAAApiUsers caaApiUsers, Response response) {
//                if (isInFront) {
//                    items.remove(items.size() - 1);
//                    if (caaApiUsers.getData().size() < 1) {
//                        endPages = true;
//                    } else {
//                        for (User user : caaApiUsers.getData()) {
//                            items.add(user);
//                        }
//                        if (items.size() == numberOfConnections) {
//                            endPages = true;
//                        } else {
//                            items.add(new User());
//                        }
//                    }
//                    adapter.notifyDataSetChanged();
//                }
//                calling = false;
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        });
//
//    }
//
//    private void getFollowingPage() {
//        App.getUrlService(this).getSpecificUserFollowingPage(userID, pageNumber, new Callback<CAAApiUsers>() {
//            @Override
//            public void success(CAAApiUsers caaApiUsers, Response response) {
//                if (isInFront) {
//                    items.remove(items.size() - 1);
//                    if (caaApiUsers.getData().size() < 1) {
//                        endPages = true;
//                    } else {
//                        for (User user : caaApiUsers.getData()) {
//                            items.add(user);
//                        }
//                        if (items.size() == numberOfConnections) {
//                            endPages = true;
//                        } else {
//                            items.add(new User());
//                        }
//                    }
//                    adapter.notifyDataSetChanged();
//                }
//                calling = false;
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        });
//
//    }
//}
