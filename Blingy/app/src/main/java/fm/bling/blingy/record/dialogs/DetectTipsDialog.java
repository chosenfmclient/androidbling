package fm.bling.blingy.record.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;

import fm.bling.blingy.R;
import fm.bling.blingy.record.callbacks.DetectBackgroundCallback;


/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 12/29/16.
 * History:
 * ***********************************
 */
public class DetectTipsDialog extends Dialog {
    private DetectBackgroundCallback mCallDetect;
    public DetectTipsDialog(Context context, DetectBackgroundCallback calldetect) {
        super(context, R.style.FullScreen);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.mCallDetect = calldetect;
        setContentView(R.layout.detect_tips_dialog);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        findViewById(R.id.detect_main_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallDetect.doDetectBackground();
                dismiss();
            }
        });
        show();
    }
}
