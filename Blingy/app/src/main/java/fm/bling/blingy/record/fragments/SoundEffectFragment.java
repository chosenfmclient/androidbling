//package fm.bling.blingy.record.fragments;
//
//import android.media.AudioManager;
//import android.media.MediaPlayer;
//import android.os.Bundle;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import java.io.IOException;
//import java.util.ArrayList;
//
//import fm.bling.blingy.App;
//import fm.bling.blingy.R;
//import fm.bling.blingy.record.AddSoundActivity;
//import fm.bling.blingy.record.adapters.SoundEffectRecyclerAdapter;
//import fm.bling.blingy.record.utils.SimpleDividerItemDecoration;
//import fm.bling.blingy.record.rest.model.CAASoundEffects;
//import fm.bling.blingy.tracking.TrackingManager;
//import fm.bling.blingy.utils.Constants;
//import fm.bling.blingy.utils.imagesManagement.views.ProgressBarView;
//import retrofit.Callback;
//import retrofit.RetrofitError;
//import retrofit.client.Response;
//
///**
// * *********************************
// * Project: Chosen Android Application
// * Description:
// * Created by Oren Zakay on 5/5/16.
// * History:
// * ***********************************
// */
//public class SoundEffectFragment extends BaseAudioFragment{
//
//    private RecyclerView mRecycleTrackList;
//    private ProgressBarView mChosenProgerss;
//    private TextView mNoResults;
//    private SoundEffectListener mCallBack;
//    private MediaPlayer mMediaPlayer;
//
//    private ArrayList<CAASoundEffects.CAAEffect> searchDataSet = null;
//    private ArrayList<CAASoundEffects.CAAEffect> songs = null;
//    private SoundEffectRecyclerAdapter adapter;
//
//    private boolean isVisible = false;
//    private boolean isPrepared = false;
//
//    private View.OnClickListener mItemClickListener;
//    private View.OnClickListener mPlayPauseClickListener;
//
//    private View lastView;
//    private int lastSearchLength;
//    private boolean mDataFetched = false;
//
//    private boolean isLoadingMusic = false;
//
//    public static SoundEffectFragment getInstance(ArrayList<CAASoundEffects.CAAEffect> items) {
//        SoundEffectFragment fragment = new SoundEffectFragment();
//        fragment.songs = items;
//        fragment.searchDataSet = new ArrayList<CAASoundEffects.CAAEffect>();
//        if(items != null)
//            for (int i = 0; i < items.size(); i++)
//                fragment.searchDataSet.add(items.get(i));
//        return fragment;
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        isLoadingMusic = false;
//        mItemClickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(lastView != null && mMediaPlayer != null && mMediaPlayer.isPlaying())
//                    ((ImageView) lastView).setImageResource(R.drawable.sound_play_36);
//                CAASoundEffects.CAAEffect effectElement = ((CAASoundEffects.CAAEffect)view.getTag());
//                ((AddSoundActivity)getActivity()).saveSoundEffecttrackResult(effectElement.getName(), effectElement.getUrl());
//                mCallBack.onSongClicked();
//            }
//        };
//
//        mPlayPauseClickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!isLoadingMusic) {
//                    CAASoundEffects.CAAEffect effectElement = ((CAASoundEffects.CAAEffect) ((LinearLayout) view.getTag()).getTag());
//                    ProgressBar progressBar = ((ProgressBar) ((LinearLayout) view.getTag()).findViewById(R.id.progress_bar));
//                    if (mMediaPlayer != null) {
//                        if (mMediaPlayer.isPlaying()) {
//                            if (view == lastView) {
//                                ((ImageView) view).setImageResource(R.drawable.sound_play_36);
//                                mMediaPlayer.pause();
//                            } else {
//                                ((ImageView) lastView).setImageResource(R.drawable.sound_play_36);
//                                lastView = view;
//                                mMediaPlayer.reset();
//                                MusicPrepare(effectElement.getUrl(), progressBar);
//                            }
//
//                        } else {
//                            if (view == lastView) {
//                                mMediaPlayer.start();
//                                ((ImageView) view).setImageResource(R.drawable.sound_pause_36);
//                            } else {
//                                lastView = view;
//                                mMediaPlayer.reset();
//                                MusicPrepare(effectElement.getUrl(), progressBar);
//                            }
//                        }
//                    } else {
//                        lastView = view;
//                        mMediaPlayer = new MediaPlayer();
//                        MusicPrepare(effectElement.getUrl(), progressBar);
//                    }
//                }
//            }
//        };
//
//        if(songs == null) {
//            getSongs();
//            isPrepared = true;
//        }
//        else
//            mDataFetched = true;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View v = inflater.inflate(R.layout.soundtrack_fragment, null);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
//        mRecycleTrackList = (RecyclerView) v.findViewById(R.id.recycler_view);
//        mRecycleTrackList.setLayoutManager(mLayoutManager);
//        mRecycleTrackList.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext(), R.drawable.line_divider));
//
//        mChosenProgerss = (ProgressBarView)v.findViewById(R.id.progress_bar);
//        mChosenProgerss.updateLayout();
//        mChosenProgerss.setVisibility(View.VISIBLE);
//
//        mNoResults = (TextView)v.findViewById(R.id.no_search_results);
//
//        if(!isPrepared)
//            prepareData(songs);
//        mCallBack = (SoundEffectListener) getActivity();
//        return v;
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        isVisible = true;
//    }
//
//    @Override
//    public void onStop() {
//        isVisible = false;
//        if (mMediaPlayer != null) {
//            mMediaPlayer.stop();
//            mMediaPlayer.release();
//            mMediaPlayer = null;
//        }
//        super.onStop();
//    }
//
//    private void getSongs() {
//        App.getUrlService().getSoundEffects(new Callback<CAASoundEffects>() {
//            @Override//getRestClient().getUrlService()
//            public void success(CAASoundEffects CAASoundEffects, Response response) {
//                if(CAASoundEffects.getData() != null && CAASoundEffects.getData().size() > 0) {
//                    prepareData(CAASoundEffects.getData());
//                    mDataFetched = true;
//                }
//                else
//                    mNoResults.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Toast.makeText(getActivity(),"Failed to fetch data, please try again later.",Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//
//    @Override
//    public void onSearch(String text) {
//        if(mDataFetched) {
//            String search = text.toString().toLowerCase();
//            if (lastSearchLength < search.length()) {
//                for (int i = searchDataSet.size() - 1; i > -1; i--) {
//                    if (searchDataSet.get(i).getName().toLowerCase().indexOf(search) == -1) {
//                        searchDataSet.remove(i);
//                    }
//                }
//                if(searchDataSet.size() == 0)
//                    mNoResults.setVisibility(View.VISIBLE);
//                else
//                    mNoResults.setVisibility(View.GONE);
//            } else {
//                searchDataSet.clear();
//                if (search.length() > 0) {
//                    for (int i = 0; i < songs.size(); i++) {
//                        if (songs.get(i).getName().toLowerCase().indexOf(search) != -1)
//                            searchDataSet.add(songs.get(i));
//                    }
//                    if(searchDataSet.size() == 0)
//                        mNoResults.setVisibility(View.VISIBLE);
//                    else
//                        mNoResults.setVisibility(View.GONE);
//                } else {
//                    mNoResults.setVisibility(View.GONE);
//                    for (int i = 0; i < songs.size(); i++) {
//                        searchDataSet.add(songs.get(i));
//                    }
//                }
//            }
//            adapter.notifyDataSetChanged();
//            lastSearchLength = search.length();
//        }
//    }
//
//    @Override
//    public void stopMediaPlayer() {
//        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
//            mMediaPlayer.stop();
//            mMediaPlayer.reset();
//            mMediaPlayer = null;
//            if(lastView != null)
//                ((ImageView) lastView).setImageResource(R.drawable.sound_play_36);
//            lastView = null;
//        }
//    }
//
//    @Override
//    public String getFragmentTag() {
//        return Constants.SOUND_EFFECT_FRAGMENT;
//    }
//
//    public interface SoundEffectListener {
//        void onSongClicked();
//    }
//
//    private void prepareData(ArrayList<CAASoundEffects.CAAEffect> data){
//        if(isVisible){
//            mChosenProgerss.setVisibility(View.GONE);
//            songs = data;
//            if(data != null)
//                for (int i = 0; i < data.size(); i++)
//                    searchDataSet.add(data.get(i));
//            adapter = new SoundEffectRecyclerAdapter(searchDataSet, mItemClickListener, mPlayPauseClickListener);
//            mRecycleTrackList.setAdapter(adapter);
//        }
//    }
//
//    public void MusicPrepare(String url,final ProgressBar progressBar) {
//        // Set type to streaming
//        isLoadingMusic = true;
//        showProgressBar(progressBar);
//        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//        // Listen for if the audio file can't be prepared
//        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
//            @Override
//            public boolean onError(MediaPlayer mp, int what, int extra) {
//                // ... react appropriately ...
//                // The MediaPlayer has moved to the Error state, must be reset!
//                return false;
//            }
//        });
//        // Attach to when audio file is prepared for playing
//        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                hideProgressBar(progressBar);
//                mp.start();
//                ((ImageView)lastView).setImageResource(R.drawable.sound_pause_36);
//                isLoadingMusic = false;
//            }
//        });
//        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mediaPlayer) {
//                ((ImageView)lastView).setImageResource(R.drawable.sound_play_36);
//                isLoadingMusic = false;
//            }
//        });
//        // Set the data source to the remote URL
//        try {
//            mMediaPlayer.setDataSource(url);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        // Trigger an async preparation which will file listener when completed
//        mMediaPlayer.prepareAsync();
//    }
//
//    public void showProgressBar(ProgressBar progressBar){
//        lastView.setVisibility(View.INVISIBLE);
//        progressBar.setVisibility(View.VISIBLE);
//    }
//    public void hideProgressBar(ProgressBar progressBar){
//        progressBar.setVisibility(View.GONE);
//        lastView.setVisibility(View.VISIBLE);
//    }
//}
