package fm.bling.blingy.record;

import android.content.Context;
import android.os.SystemClock;

import java.io.File;

import fm.bling.blingy.utils.FFMPEG;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 05/09/2016.
 * History:
 * ***********************************
 */
public class VideoManager
{
    private int FPS = 25;

    private float videoSpeed = 1f;


    private String mFilePrefix, mVideoPath, mAudioOutPath, mFramesPath;
    private VideoManagerListener mVideoManagerListener;
    private Context mContext;
    private boolean play;

//    public VideoManager(Context context, String videoPath, String audioPath, String framesPath, VideoManagerListener videoManagerListener)
//    {
//        mContext = context;
//        this.mVideoManagerListener = videoManagerListener;
//        mVideoPath = videoPath;
//        mAudioOutPath = audioPath;
//        mFilePrefix = framesPath + "frame%04d.jpg";
//        mFramesPath = framesPath;
//
//        prepareVideo();
//    }

    public VideoManager(String framesPath, VideoManagerListener videoManagerListener)
    {
        mFramesPath = framesPath;
        this.mVideoManagerListener = videoManagerListener;
    }

//    private void prepareVideo()
//    {
//
//        new Thread()
//        {
//            public void run()
//            {
//                try
//                {
//                    int ret = FFMPEG.getInstance(mContext).splitAudioAndVideo(mVideoPath, mAudioOutPath, mFilePrefix, FPS);
//                    if(mVideoManagerListener != null)
//                        mVideoManagerListener.onVideoReady(VideoManager.this);
//                }
//                catch (Throwable throwable)
//                {
//                    throwable.printStackTrace();
//                    if(mVideoManagerListener != null)
//                        try{mVideoManagerListener.onVideoError(FFMPEG.getInstance(mContext).getError());}catch (Throwable err){err.printStackTrace();}
//                }
//            }
//        }.start();
//    }

    public void reset()
    {
        lastIndex = 1;
    }

    private int lastIndex = 1;
    public void start()
    {
        play = true;

        if(lastIndex == 1)
            setIndexAccordingToVideoLag();

        new Thread()
        {
            public void run()
            {
                File currentFile;
                int fileIndex = lastIndex;
                String fileIndexStr;
                String framePath = mFramesPath + "frame";

                int frameTiming = (int) ((1000f / ((float)FPS)) / videoSpeed);
                long timeStarted;

                while(play)
                {
                    timeStarted = SystemClock.elapsedRealtime();

                    if(fileIndex < 10)
                        fileIndexStr = "000"+fileIndex;
                    else if(fileIndex < 100)
                        fileIndexStr = "00"+fileIndex;
                    else if(fileIndex < 1000)
                        fileIndexStr = "0"+fileIndex;
                    else fileIndexStr = fileIndex + "";

                    currentFile = new File(framePath + fileIndexStr + ".jpg");

                    if(currentFile.exists())
                        mVideoManagerListener.onFrameReady(currentFile.getPath());
                    else return;//throw new RuntimeException("this file does not exist : " + new File(framePath + fileIndexStr + ".jpg").getPath());//break;
                    fileIndex++;

                    timeStarted = frameTiming - (SystemClock.elapsedRealtime() - timeStarted);

                    if(timeStarted > 0)
                        try{ Thread.sleep(timeStarted); }
                        catch (Throwable er) { er.printStackTrace(); }
                }
                if(play)
                    mVideoManagerListener.onVideoEnded();
                lastIndex = fileIndex;
            }
        }.start();
    }

    private void setIndexAccordingToVideoLag()
    {
        FPS = FFMPEG.getInstance(null).loadedFps;

        // We can ask only once if FFMPEG.getInstance(null).isFirstStreamIsVideo
        if(!FFMPEG.getInstance(null).isFirstStreamIsVideo && FFMPEG.getInstance(null).stream0StartTime < 0)
        {
            FFMPEG.getInstance(null).stream0StartTime *= -1;
            lastIndex = (int) (FPS * FFMPEG.getInstance(null).stream0StartTime);
        }
        else if(FFMPEG.getInstance(null).isFirstStreamIsVideo && FFMPEG.getInstance(null).stream0StartTime > 0)
        {
            lastIndex = (int) (FPS * FFMPEG.getInstance(null).stream0StartTime);
        }
        if(lastIndex <= 0)
            lastIndex = 1;

    }

    public void stop()
    {
        play = false;
    }

    public float getVideoSpeed()
    {
        return videoSpeed;
    }

    public void setVideoSpeed(float videoSpeed)
    {
        this.videoSpeed = videoSpeed;
    }

    public interface VideoManagerListener
    {
        void onFrameReady(String frameFileName);

        void onVideoReady(VideoManager videoManager);

        void onVideoError(String errMsg);

        void onVideoEnded();
    }

}