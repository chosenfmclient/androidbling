package fm.bling.blingy.stateMachine;

import fm.bling.blingy.widget.dialogs.InviteWidgetBaseDialog;

/**
 * Created by Chosen-pro on 06/07/2016.
 */

public interface StateMachineMessageListener {
    void onWidgetReady(InviteWidgetBaseDialog dialog);
}
