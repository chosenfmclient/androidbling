package fm.bling.blingy.mashup.listeners;

import fm.bling.blingy.videoHome.model.CAAVideo;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 20/03/17.
 * History:
 * ***********************************
 */
public interface  CreateCameoListener {

    void onCreateCameo(CAAVideo video);
}
