package fm.bling.blingy.registration.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 10/6/16.
 * History:
 * ***********************************
 */
public class CAAEmailSignup {

    @SerializedName("email")
    private String email;

    @SerializedName("app_id")
    private String appId;

    @SerializedName("secret")
    private String secret;

    public CAAEmailSignup(String email, String appId, String secret){
        this.email = email;
        this.appId = appId;
        this.secret = secret;
    }
}
