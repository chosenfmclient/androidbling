package fm.bling.blingy.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.database.handler.LikesDataHandler;
import fm.bling.blingy.dialogs.listeners.UpdateLikeTotal;
import fm.bling.blingy.dialogs.model.CAAType;
import fm.bling.blingy.rest.model.CAALike;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.videoHome.VideoHomeActivity;
import fm.bling.blingy.videoHome.model.CAAVideo;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 10/9/16.
 * History:
 * ***********************************
 */
public class LikeAnimationDialog extends Dialog {

    private Context mContext;
    private FrameLayout mFrame;
    private ImageView heart0;
    private ImageView heart1;
    private ImageView heart2;

    public LikeAnimationDialog(Context context,final CAAVideo video, int[] location, UpdateLikeTotal updateLikeTotal, int pos) {
        super(context, android.R.style.Theme_Panel);
        setContentView(R.layout.like_animation_dialog);
        this.mContext = context;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(context ,R.color.black_20));
        }

        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.dimAmount = 0;
        getWindow().setAttributes(lp);

        mFrame = (FrameLayout)findViewById(R.id.like_anim_frame);
        heart0 = (ImageView)findViewById(R.id.like_anim1);
        heart1 = (ImageView)findViewById(R.id.like_anim2);
        heart2 = (ImageView)findViewById(R.id.like_anim3);
        int frameWidthHeight = (context.getResources().getDimensionPixelSize(R.dimen.size_100dp) / 2);
        mFrame.setX(location[0] - frameWidthHeight);
        mFrame.setY(location[1] - frameWidthHeight);
        mFrame.requestLayout();

        likeAnimation();
        if(video.getLikedByMe() == 0) {
            video.setLikedByMe(1);
            video.setTotalLikes(video.getTotalLikes() + 1);
            CAAType type = new CAAType(mContext instanceof VideoHomeActivity ? "video_home" : "home");
            LikesDataHandler.getInstance().add(video.getVideoId());
            TrackingManager.getInstance().tapLikeButton(video.getVideoId(),mContext instanceof VideoHomeActivity ? "video_home" : "home_screen",true);
            App.getUrlService().postLikeVideo(video.getVideoId(), type, new Callback<CAALike>() {
                @Override
                public void success(CAALike like, Response response) {
                }

                @Override
                public void failure(RetrofitError error) {
                    video.setLikedByMe(0);
                    video.setTotalLikes(video.getTotalLikes() - 1);
                }
            });
            updateLikeTotal.onUpdateLikeTotal(pos);
        }
    }

    private void likeAnimation() {
        final ArrayList<ImageView> hearts = new ArrayList<>();
        hearts.add(heart0);
        hearts.add(heart1);
        hearts.add(heart2);
        Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.like_anim);
        final ArrayList<Animation> anims = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            anims.add(anim);
        }

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 3; i++) {
                        try {
                            final int finalI = i;
                            heart0.post(new Runnable() {
                                @Override
                                public void run() {
                                    hearts.get(finalI).setAlpha((float)1);
                                    hearts.get(finalI).startAnimation(anims.get(finalI));
                                    if(finalI < 2){
                                        anims.get(finalI).setAnimationListener(new Animation.AnimationListener() {
                                            @Override
                                            public void onAnimationStart(Animation animation) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animation animation) {
                                                hearts.get(finalI).setAlpha((float)0);
                                            }

                                            @Override
                                            public void onAnimationRepeat(Animation animation) {

                                            }
                                        });
                                    }
                                    else{
                                        anims.get(finalI).setAnimationListener(new Animation.AnimationListener() {
                                            @Override
                                            public void onAnimationStart(Animation animation) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animation animation) {
                                                hearts.get(finalI).setAlpha((float)0);
                                                heart2.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                     dismiss();
                                                    }
                                                },10);
                                            }

                                            @Override
                                            public void onAnimationRepeat(Animation animation) {

                                            }
                                        });
                                    }
                                }
                            });
                        } catch (Exception ex) {
//                            Log.d("runOnUI exception", ex.getMessage());
                        }
                        sleep(50);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }
}
