package fm.bling.blingy.duetHome.presenter;



import fm.bling.blingy.duetHome.DuetHomeMVPR;

import fm.bling.blingy.duetHome.model.DuetHomeFeed;
import fm.bling.blingy.duetHome.model.DuetHomeModel;

public class DuetHomePresenter implements DuetHomeMVPR.Presenter{

    private DuetHomeMVPR.View mView;
    private DuetHomeMVPR.Model mModel;

    public DuetHomePresenter(){
        mModel = new DuetHomeModel(this);
    }

    @Override
    public void setView(DuetHomeMVPR.View view) {
        this.mView = view;
    }

    @Override
    public void getDuetHome(int page) {
        mModel.getDuetHome(page);
    }

    @Override
    public void loadDuetHomeData(DuetHomeFeed data) {
        if(data != null) {
            if(data.getFeaturedClip() != null)
                mView.setupFeatured(data.getFeaturedClip());

            if(data.getTrendingVideos() != null)
                mView.showDuetData(data.getTrendingVideos());

        }
    }

    @Override
    public void loadDataFailed() {
        mView.loadDataFailed();
    }
}
