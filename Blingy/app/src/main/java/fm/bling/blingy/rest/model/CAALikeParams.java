package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;


/************************************
 * Project: Chosen Android Application
 * FileName: CAALikeParams
 * Description:
 * Created by Dawidowicz Nadav on 10/01/2016.
 * History:
 *************************************/
public class CAALikeParams {
    @SerializedName("like_id")
    private String commentId;

    @SerializedName("user")
    private User user;

    @SerializedName("video_id")
    private String videoId;

    @SerializedName("date")
    private String date;

    public CAALikeParams(){}

    public CAALikeParams(User user, String videoId, String liked_id){
        this.user = user;
        this.videoId = videoId;
        this.commentId = liked_id;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
