package fm.bling.blingy.record.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 5/16/16.
 * History:
 * ***********************************
 */
public class CAAFaceDetection {

    @SerializedName("faces")
    ArrayList<CAAFaceDetectionObject> faces;

    @SerializedName("masks")
    ArrayList<CAAFaceDetectionObject> masks;

    public ArrayList<CAAFaceDetectionObject> getMasks() {
        return masks;
    }

    public void setMasks(ArrayList<CAAFaceDetectionObject> masks) {
        this.masks = masks;
    }

    public ArrayList<CAAFaceDetectionObject> getFaces() {
        return faces;
    }

    public void setFaces(ArrayList<CAAFaceDetectionObject> faces) {
        this.faces = faces;
    }

    public class CAAFaceDetectionObject{

        @SerializedName("preview_image")
        String previewImage;

        @SerializedName("name")
        String name;

        @SerializedName("elements")
        ArrayList<CAAFaceDetectionElements> elements;

        public String getPreviewImage() {
            return previewImage;
        }

        public String getName() { return name; }

        public void setName(String name) { this.name = name; }

        public ArrayList<CAAFaceDetectionElements> getElements() {
            return elements;
        }

    }

    public class CAAFaceDetectionElements {

        /** type options
         *  1 - mouth
         *  2 - mustache
         *  3 - eyes
         *  4 - left eye
         *  5 - right eye
         *  6 - face
         *  7 - on top of face
         * */
        @SerializedName("type")
        int type;

        @SerializedName("image")
        String image;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }

}
