package fm.bling.blingy.registration.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 4/17/16.
 * History:
 * ***********************************
 */
public class CAAGoogleToken {

    @SerializedName("google_access_token")
    private  String googleAccessToken;

    @SerializedName("app_id")
    private String appId;

    @SerializedName("secret")
    private String secret;

    @SerializedName("auto")
    private String auto;

    public CAAGoogleToken(String googleAccessToken, String appId, String secret){
        this.googleAccessToken = googleAccessToken;
        this.appId = appId;
        this.secret = secret;
    }

    public String getAuto() {
        return auto;
    }

    public void setAuto(String auto) {
        this.auto = auto;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getGoogleAccessToken() {
        return googleAccessToken;
    }

    public void setGoogleAccessToken(String googleAccessToken) {
        this.googleAccessToken = googleAccessToken;
    }
}
