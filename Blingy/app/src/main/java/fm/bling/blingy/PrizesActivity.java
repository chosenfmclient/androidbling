package fm.bling.blingy;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;

import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
import fm.bling.blingy.tracking.TrackingManager;


/**
 * Created by Ivan on 6/9/2015.
 */
public class PrizesActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);
        //Setting up the navigation icon.
        Toolbar toolbar = getActionBarToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
        toolbar.setNavigationContentDescription("backClose");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String url ="http://blin.gy/prizes";
        WebView view=(WebView) this.findViewById(R.id.prizes_webview);

        view.getSettings().setJavaScriptEnabled(true);
        if (NetworkConnectivityListener.isConnectedWithDialog(this)) {
            view.loadUrl(url);
        }
//        else {
//            showNoConnectionDialog();
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

//    @Override
//    protected int getSelfNavDrawerItem() {
//        return NavDrawerActivity.NAVDRAWER_ITEM_PRIZES;
//    }
}
