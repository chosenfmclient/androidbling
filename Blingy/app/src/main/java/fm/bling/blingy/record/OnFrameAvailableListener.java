package fm.bling.blingy.record;

import org.opencv.core.Mat;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 24/05/2017.
 * History:
 * ***********************************
 */
public interface OnFrameAvailableListener
{
    void onFrameAvailable(Mat currentFrame, long time);

    void onFrameDropped(Mat droppedFrame, long time);

    /**
     *
     * @param time
     * @return the frame path
     */
    String shouldSaveDroppedFrame(long time);

    boolean shouldCatchDroppedFrames();
}