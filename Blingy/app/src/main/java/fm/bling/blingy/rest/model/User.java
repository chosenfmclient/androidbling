
package fm.bling.blingy.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class User implements Parcelable{
    @SerializedName("user_id")
    private String userId;

    @SerializedName("user_name")
    private String username;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("photo")
    private String photo;

    @SerializedName("total_performances")
    private String totalPerformances;

    @SerializedName("total_followers")
    private String totalFollowers;

    @SerializedName("total_following")
    private String totalFollowing;

    @SerializedName("followed_by_me")
    private String followedByMe;

    @SerializedName("twitter_handle")
    private String twitterHandle;

    @SerializedName("instagram_handle")
    private String instagramHandle;

    @SerializedName("youtube_handle")
    private String youtubeHandle;

    @SerializedName("facebook_handle")
    private String facebookHandle;

    @SerializedName("rank")
    private String rank;

    @SerializedName("likes")
    private String likes;

    private int cameoInvited = 0;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getFullName(){
        return firstName + " " + lastName;
    }

    public String getTotalPerformances() {
        return totalPerformances;
    }

    public String setTotalPerformances(String totalPerformances){
       return this.totalPerformances = totalPerformances;
    }
    public String getTotalFollowers() {
        return totalFollowers;
    }

    public String getTotalFollowing() {
        return totalFollowing;
    }

    public String getFollowedByMe() {
        return followedByMe;
    }

    public String getTwitterHandle() {
        return twitterHandle;
    }

    public String getInstagramHandle() {
        return instagramHandle;
    }

    public String getYoutubeHandle() {
        return youtubeHandle;
    }

    public String getFacebookHandle() {
        return facebookHandle;
    }

    public void setFollowedByMe(String followedByMe) {
        this.followedByMe = followedByMe;
    }

    public void setTotalFollowing(String totalFollowing) {
        this.totalFollowing = totalFollowing;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank){
        this.rank = rank;
    }

    public String getLikes() {
        return likes;
    }

    public void setCameoInvited(int invited){
        this.cameoInvited = invited;
    }

    public boolean isCameoInvited(){
        return this.cameoInvited == 1;
    }

    public User(){}

    public User(String userId, String firstName, String lastName, String photo){
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.photo = photo;
    }

    protected User(Parcel in) {
        userId = in.readString();
        username = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        photo = in.readString();
        totalPerformances = in.readString();
        totalFollowers = in.readString();
        totalFollowing = in.readString();
        followedByMe = in.readString();
        twitterHandle = in.readString();
        instagramHandle = in.readString();
        youtubeHandle = in.readString();
        facebookHandle = in.readString();
        likes = in.readString();
        rank = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(username);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(photo);
        dest.writeString(totalPerformances);
        dest.writeString(totalFollowers);
        dest.writeString(totalFollowing);
        dest.writeString(followedByMe);
        dest.writeString(twitterHandle);
        dest.writeString(instagramHandle);
        dest.writeString(youtubeHandle);
        dest.writeString(facebookHandle);
        dest.writeString(rank);
        dest.writeString(likes);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public String toString() {
        return userId + " " + username + " " + lastName;
    }
}
