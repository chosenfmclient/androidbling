package fm.bling.blingy.leaderboard;

import fm.bling.blingy.App;
import fm.bling.blingy.rest.model.LeaderboardData;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 02/03/17.
 * History:
 * ***********************************
 */
public class LeaderboardRepository  implements LeaderboardMVPR.Repository {


    private LeaderboardMVPR.Model mModel;

    public LeaderboardRepository(LeaderboardMVPR.Model model){
        this.mModel = model;
    }

    @Override
    public void getLeaderBoard(final int page) {
        App.getUrlService().getLeaderboard(page, new Callback<LeaderboardData>() {
            @Override
            public void success(LeaderboardData leaderboardData, Response response) {
                if(page == 0)
                    mModel.loadLeaderboardData(leaderboardData != null ? leaderboardData.getUsers() : null);
                else
                    mModel.loadNextPage(leaderboardData != null ? leaderboardData.getUsers() : null);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
