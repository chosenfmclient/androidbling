package fm.bling.blingy.utils.imagesManagement.views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import fm.bling.blingy.App;
import fm.bling.blingy.utils.graphics.WhiteLoadingPlaceHolder;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/6/16.
 * History:
 * ***********************************
 */
public class WhiteProgressBarView extends View
{
    private static WhiteLoadingPlaceHolder loadingPlaceHolder = null;
    private int widthHeight = (int) (250f * App.SCALE_X);

    public WhiteProgressBarView(Context context) {
        super(context);
        init(context);
    }

    public WhiteProgressBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public WhiteProgressBarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){

        if(widthHeight == 0) {
            widthHeight = (int) (250f * App.SCALE_X); // portrait 1920 X 1080
        }

        this.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));

        if(loadingPlaceHolder == null)
            while(true)
            {
                try
                {
                    loadingPlaceHolder = new WhiteLoadingPlaceHolder(context, widthHeight, widthHeight, 0, 0);
                    break;
                }
                catch (Throwable err)
                {
                    widthHeight -= 10; //decreasing the size of the widthHeight which is the size of the image of the loader, so
                    // it won't claim too much memory in case of OOM .
                    if(widthHeight <= 0)
                        return;
                }
            }
        loadingPlaceHolder.attachView(this);
    }

    public void setWidthHeight(int widthHeight)
    {
        this.widthHeight = widthHeight;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        try{loadingPlaceHolder.draw(canvas);}
        catch (Throwable err){err.printStackTrace();}
    }

    public void updateLayout()
    {
        if(getLayoutParams() == null)
            setLayoutParams(new ViewGroup.LayoutParams(widthHeight, widthHeight));
        else getLayoutParams().width = getLayoutParams().height = widthHeight;
    }

    public void close(){
        try
        {
            loadingPlaceHolder.detachView(this);//close();
        }
        catch (Throwable err)
        {}
//        loadingPlaceHolder = null;
    }
}
