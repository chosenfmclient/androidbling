package fm.bling.blingy.record.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 4/12/16.
 * History:
 * ***********************************
 */
public class CAAItunesMusicElement {

    @SerializedName("wrapperType")
    private String wrraperType;

    @SerializedName("trackId")
    private String trackId;

    @SerializedName("clip_id")
    private String clipId;

    @SerializedName("kind")
    private String kind;

    @SerializedName("collectionName")
    private String collectionName;

    @SerializedName("artistName")
    private String artistName;

    @SerializedName("trackName")
    private String trackName;

    @SerializedName("previewUrl")
    private String previewUrl;

    @SerializedName("artworkUrl100")
    private String artworkUrl100;

    @SerializedName("time_segments")
    private ArrayList<Integer> mTimeSegments;

    @SerializedName("label")
    private String lable;

    @SerializedName("color")
    private String color;

    @SerializedName("primaryGenreName")
    private String genre;

    public CAAItunesMusicElement(String artistName, String trackName, String previewUrl, String artworkUrl100, ArrayList<Integer> segments){
        this.artistName = artistName;
        this.trackName = trackName;
        this.previewUrl = previewUrl;
        this.artworkUrl100 = artworkUrl100;
        this.mTimeSegments = segments;

    }

    public ArrayList<Integer> getmTimeSegments() {
        return mTimeSegments;
    }

    public String getTrackId() {
        return trackId;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public String getArtworkUrl100() {
        return artworkUrl100;
    }

    public String getLable() {
        return lable;
    }

    public String getColor() {
        return color;
    }

    public String getGenre(){ return genre; }

    public String getClipId() {
        return clipId;
    }
}
