package fm.bling.blingy.record.rest;

import fm.bling.blingy.record.rest.model.CAAiTunes;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 5/3/16.
 * History:
 * ***********************************
 */
public interface UrlServiceiTunes
{

    @GET("/search")
    void searchiTunes(@Query("term") String term, @Query("media") String kind, Callback<CAAiTunes> callback);

}
