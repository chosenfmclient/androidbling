package fm.bling.blingy.dialogs.listeners;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 12/11/16.
 * History:
 * ***********************************
 */
public interface NetworkChangedListener {
    void onNetworkChangedListener(boolean isConnected);
}
