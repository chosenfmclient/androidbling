package fm.bling.blingy.utils.imagesManagement.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

import fm.bling.blingy.R;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/28/16.
 * History:
 * ***********************************
 */
public class ScaledImageView extends URLImageView
{
    private float scaleX = 1f, scaleY = 1f;
    private boolean firstRun = true, fitToParent = false;
    private int centerX , centerY;
    private boolean override = true;
    private float aspectRatio = 1f;

    public ScaledImageView(Context context)
    {
        super(context);
    }

    public ScaledImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public ScaledImageView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

//    public void setScale(float scaleX, float scaleY)
//    {
//        this.scaleX = scaleX;
//        this.scaleY = scaleY;
//    }

    public void setFitToParent()
    {
        fitToParent = true;
        setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    public void draw(Canvas canvas)
    {
        if(firstRun)
        {
            firstRun(canvas);
        }

        if(override)
            super.draw(canvas);
        else
        {
            canvas.save();
            canvas.scale(scaleX, scaleY, centerX, centerY);
            super.draw(canvas);
            canvas.restore();
        }
    }

    private void firstRun(Canvas canvas)
    {
        if(isResourceImage) return;
        try
        {
            centerX = canvas.getWidth() / 2;
            centerY = canvas.getHeight() / 2;

            Drawable drawable;
            if (fitToParent)
            {
                if(Build.VERSION.SDK_INT == 21)
                {
                    setScaleType(ImageView.ScaleType.FIT_XY);
                    fitToParent = false;
                    firstRun = false;
                    override = true;
                }
                else if ((drawable = getDrawable()) instanceof BitmapDrawable && ((BitmapDrawable) drawable).getBitmap() != null)
                {
                    setScaleType(ImageView.ScaleType.CENTER);
                    scaleX = (((float) canvas.getWidth())  / ((float) ((BitmapDrawable) drawable).getBitmap().getWidth())) * 1.1f;
                    scaleY = (((float) canvas.getHeight()) / ((float) ((BitmapDrawable) drawable).getBitmap().getHeight())) * 1.1f;
                    if (scaleX > scaleY)
                        scaleY = scaleX;
                    else scaleX = scaleY;

                    fitToParent = false;
                    firstRun = false;
                    override = false;
                }
            }
            else firstRun = false;
        }
        catch (Throwable err) {err.printStackTrace();}
    }

    public float getAspectRatio()
    {
        return aspectRatio;
    }

    public void setAspectRatio(float aspectRatio)
    {
        this.aspectRatio = aspectRatio;
    }
}