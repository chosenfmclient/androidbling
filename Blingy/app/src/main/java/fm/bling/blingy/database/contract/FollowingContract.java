package fm.bling.blingy.database.contract;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 14/02/17.
 * History:
 * ***********************************
 */
public class FollowingContract extends BaseContract{

    /** Table Name **/
    public static final String TABLE_NAME = "following";

    /** Columns Name **/

    /**
     * String value :
     * example : "123"
     **/
    public static final String COLUMN_USER_ID = "user_id";

//    /**
//     * int value:
//     * 0 - not following
//     * 1 - following
//     **/
//    public static final String COLUMN_IS_FOLLOWING = "is_following";
//
//    /**
//     * int value:
//     * 0 - pending
//     * 1 - sent
//     **/
//    public static final String COLUMN_STATUS = "status";

    /** Queries **/

    //Create
    public static final String CREATE_TABLE =
            CREATE_IF_NOT_EXISTS + TABLE_NAME + " (" +
            COLUMN_USER_ID + TEXT_TYPE + PRIMARY_KEY + UNIQUE + NOT_NULL + ")";
//            COLUMN_IS_FOLLOWING + INT_TYPE + NOT_NULL + COMMA_SEP +
//            COLUMN_STATUS + INT_TYPE + NOT_NULL + ")";

    //Delete
    public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    //Insert many
    public static final String INSERT_MANY = "INSERT "+ OR_REPLACE + "INTO " + TABLE_NAME + " (" +
            COLUMN_USER_ID + ")" +
//            + COMMA_SEP +
//            COLUMN_TYPE + COMMA_SEP +
//            COLUMN_IS_LIKED + COMMA_SEP +
//            STATUS_SENT + ")" +
            " VALUES ";
}
