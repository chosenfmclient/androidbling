package fm.bling.blingy;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;

import java.util.HashMap;
import java.util.Map;

import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;

/**
 * Created by Kiril on 5/12/2015.
 * History :
 * Edited by : Oren Zakay since October 6th 2015.
 */
public class SettingsActivity extends BaseActivity {

    private LinearLayout openPrivacy;
    private LinearLayout openTerms;
    private TextView versionNumber;
    private Switch saveExternalToggle;
//    private Switch mPlayTutorialVideo;
    private Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        init();
        context = this;
        versionNumber.setText(BuildConfig.VERSION_NAME);

        int storageSetting = SharedPreferencesManager.getInstance().getInt(Constants.SAVE_SETTING, 0);
        if (storageSetting == 1) {
            saveExternalToggle.setChecked(true);
        }
//        mPlayTutorialVideo.setChecked(SharedPreferencesManager.getInstance(this).getBoolean(Constants.SHOW_BACKGROUND_TUTORIAL, true));
        //Setting up the navigation icon.
        Toolbar toolbar = getActionBarToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
        toolbar.setNavigationContentDescription("backClose");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        openPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PrivacyActivity.class);
                startActivity(i);
            }
        });

        openTerms.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, TermsAndConditionsActivity.class);
                startActivity(i);
            }
        });

        saveExternalToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    checkExternalStoragePermission();
                } else {
                    SharedPreferencesManager.getEditor().putInt(Constants.SAVE_SETTING, Constants.SAVE_INTERNAL).apply();
                }
            }
        });

//        mPlayTutorialVideo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                SharedPreferencesManager.getEditor(SettingsActivity.this).putBoolean(Constants.SHOW_BACKGROUND_TUTORIAL,b).commit();
//            }
//        });

    }

    private void init() {
        openPrivacy = (LinearLayout)findViewById(R.id.openPrivacy);
        openTerms = (LinearLayout)findViewById(R.id.openTerms);
        versionNumber = (TextView) findViewById(R.id.textViewVersionNumber);
        saveExternalToggle = (Switch)findViewById(R.id.saveExternalToggle);
//        mPlayTutorialVideo = (Switch)findViewById(R.id.play_tutorial_toggle);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == Constants.OPTIONAL_PERMISSION) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED &&
                        (permissions[i].equalsIgnoreCase(Manifest.permission.WRITE_EXTERNAL_STORAGE) || permissions[i].equalsIgnoreCase(Manifest.permission.READ_EXTERNAL_STORAGE))) {
                    SharedPreferencesManager.getEditor().putInt(Constants.SAVE_SETTING, Constants.SAVE_INTERNAL).apply();
                    saveExternalToggle.setChecked(false);
                    showPermissionDialog();
                    return;
                }
            }
            SharedPreferencesManager.getEditor().putInt(Constants.SAVE_SETTING, Constants.SAVE_EXTERNAL).apply();
            saveExternalToggle.setChecked(true);
        }
    }

    private void checkExternalStoragePermission() {
        String[] permissions = new String[] {"", ""};
        if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(SettingsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            permissions[0] = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        }
        if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(SettingsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            permissions[1] = Manifest.permission.READ_EXTERNAL_STORAGE;
        }
        if ((!permissions[0].isEmpty() || !permissions[1].isEmpty())) {
            ActivityCompat.requestPermissions(SettingsActivity.this, permissions,
                    Constants.OPTIONAL_PERMISSION);
        } else {
            SharedPreferencesManager.getEditor().putInt(Constants.SAVE_SETTING, Constants.SAVE_EXTERNAL).apply();
            saveExternalToggle.setChecked(true);
        }
    }

    private void showPermissionDialog() {
        BaseDialog dialog = new BaseDialog(this,"Permission", getResources().getString(R.string.record_save_permission_dialog));
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (((BaseDialog)dialogInterface).getAnswer()) {
                    checkExternalStoragePermission();
                }
            }
        });
    }

//    @Override
//    protected int getSelfNavDrawerItem() {
//        return NavDrawerActivity.NAVDRAWER_ITEM_SETTINGS;
//    }
}
