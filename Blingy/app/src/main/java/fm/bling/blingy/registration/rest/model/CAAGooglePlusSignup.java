package fm.bling.blingy.registration.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 4/17/16.
 * History:
 * ***********************************
 */
public class CAAGooglePlusSignup {

    @SerializedName("google_access_token")
    private  String googleAccessToken;

    @SerializedName("google_profile")
    private  String google_profile;

    @SerializedName("nickname")
    private  String nickname;

    @SerializedName("birthday")
    private  String birthday;

    @SerializedName("gender")
    private  String gender;

    @SerializedName("app_id")
    private String appId;

    @SerializedName("secret")
    private String secret;

    public CAAGooglePlusSignup(String googleAccessToken ,String google_profile, String nickname, String birthday, String gender, String appID, String secret) {
        this.googleAccessToken = googleAccessToken;
        this.google_profile = google_profile;
        this.nickname = nickname;
        this.birthday = birthday;
        this.gender = gender;
        this.appId = appID;
        this.secret = secret;
    }

    public String getGoogle_profile() {
        return google_profile;
    }

    public void setGoogle_profile(String google_profile) {
        this.google_profile = google_profile;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGoogleAccessToken() {
        return googleAccessToken;
    }

    public void setGoogleAccessToken(String googleAccessToken) {
        this.googleAccessToken = googleAccessToken;
    }

}
