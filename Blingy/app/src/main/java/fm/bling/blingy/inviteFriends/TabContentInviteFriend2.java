package fm.bling.blingy.inviteFriends;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
//import android.util.TypedValue;
//import android.view.Gravity;
import android.view.View;
//import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
//import android.widget.ImageView;
import android.widget.LinearLayout;
//import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.inviteFriends.listeners.InvalidateNotificationListener;
import fm.bling.blingy.inviteFriends.listeners.InviteDataReadyListener;
import fm.bling.blingy.inviteFriends.listeners.SearchListener;
import fm.bling.blingy.inviteFriends.listeners.TabListener;
import fm.bling.blingy.inviteFriends.listeners.TabTitleListener;
import fm.bling.blingy.inviteFriends.views.InviteTabContentParkingView;

import fm.bling.blingy.inviteFriends.views.TabContentBase;
import fm.bling.blingy.inviteFriends.views.TabsHost2;
import fm.bling.blingy.registration.CAALogin;
import fm.bling.blingy.inviteFriends.model.ContactInfo;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.graphics.LoadingPlaceHolder;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
//import fm.bling.blingy.widget.CAAWidgetSettingsSingleton;
//import fm.bling.blingy.widget.PointsObserver;
//import fm.bling.blingy.widget.dialogs.PointsViewDialog;
//import fm.bling.blingy.widget.dialogs.UserProgressMapDialog;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/14/16.
 * History:
 * ***********************************
 */
public class TabContentInviteFriend2 extends TabContentBase implements /* PointsObserver,*/ InviteDataReadyListener {
    private ImageLoaderManager mImageLoaderManager;
    private LinearLayout mainView, container;
    private TabsHost2 mTabHost;
    private InviteTabContentBase[]tabs;
    private BaseActivity mContext;
//    private LinearLayout bottomStrip;
//    private TextView myPoints;
//    private PointsViewDialog pointsViewDialog;
    int mAccentColor;
    int mTabColor;

    private LoadingPlaceHolder loadingPlaceHolder;

    private InviteHandler inviteHandler;

    public static ArrayList<ContactInfo> contacts;
    public static List<User> chosenContacts;
    private static StateMachine.OnStateMachineUpdated onStateMachineUpdated;

    public TabContentInviteFriend2(BaseActivity context, ImageLoaderManager imageLoaderManager, int tabColor, int accentColor)
    {
        super(context);
        setOrientation(VERTICAL);
        this.mImageLoaderManager = imageLoaderManager;
        mContext = context;
        mAccentColor = accentColor;
        mTabColor = tabColor;

        if (!hasPermission())
        {
            addView(new InviteTabContentParkingView(mContext, new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    checkInviteTabPermission();
                }
            }));
        }
        else
        {
            setLoadingPlaceHolder();
            inviteHandler = new InviteHandler(context, this, true);
        }

    }

    private void setLoadingPlaceHolder()
    {
        removeAllViews();
        loadingPlaceHolder = new LoadingPlaceHolder(mContext, App.WIDTH/ 6, App.WIDTH / 6, (App.WIDTH/2) - (App.WIDTH / 12), (App.WIDTH/2) - (App.WIDTH / 6));
        mainView = new LinearLayout(mContext)
        {
            public void draw(Canvas canvas)
            {
                super.draw(canvas);
                loadingPlaceHolder.draw(canvas);
            }
        };
        mainView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        mainView.setBackgroundColor(Color.WHITE);
        loadingPlaceHolder.attachView(mainView);
        addView(mainView);
    }

    private void unSetLoadingPlaceHolder()
    {
        if (loadingPlaceHolder != null)
        {
            try{loadingPlaceHolder.detachView(mainView);}catch (Throwable err){}
            loadingPlaceHolder.close();
        }
    }

    @Override
    public void onInviteDataReady()
    {
        contacts = inviteHandler.getPhoneContacts();
        ArrayList<ContactInfo> contactsEmail = inviteHandler.getEmailContacts();
        ContactInfo contactInfo;

        for(int i = 0, j = 0; i < contactsEmail.size(); i++)
        {
            contactInfo = contactsEmail.get(i);
            for(;j<contacts.size();j++)
            {
                if(contactInfo.name.compareToIgnoreCase(contacts.get(j).name) < 0)
                    break;
            }
            contacts.add(j, contactInfo);
        }

        chosenContacts = inviteHandler.getChosenContacts();
        if(onStateMachineUpdated == null)
        {
            onStateMachineUpdated = new StateMachine.OnStateMachineUpdated()
            {
                @Override
                public void onContactUpdate(String phoneEmail)
                {
                    boolean found = false;
                    ContactInfo contactInfo = null;
                    for(ContactInfo ci : contacts)
                    {
                        if(ci != null) {
                            if (ci.isPhoneNumberSet()) {
                                if (ci.phone().equals(phoneEmail)) {
                                    contactInfo = ci;
                                    break;
                                }
                            } else {
                                if (ci.email != null && ci.email.equals(phoneEmail)) {
                                    contactInfo = ci;
                                    break;
                                }
                            }
                        }
                    }
                    if(contactInfo != null && tabs != null)
                    {
//                        contacts.remove(contactInfo);
                        contactInfo.status = ContactInfo.INVITED;
                        if(mContext!=null)
                        {
                            mContext.runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    if(tabs != null)
                                    {
                                        if(tabs[1] != null)
                                            ((InviteTabContentInvite) tabs[1]).setData(contacts);
                                        if(tabs[2] != null)
                                            ((InviteTabContentSearch) tabs[2]).setData(contacts, chosenContacts);
                                    }
                                }
                            });
                        }
                    }
                }
            };
            StateMachine.addOnStateMachineUpdated(onStateMachineUpdated);
        }

        mContext.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                init();
            }
        });
    }

    private void init()
    {
        Paint paint = new Paint();
        paint.setColor(mContext.getResources().getColor(R.color.list_separator_color));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth((int) (5f * App.SCALE_X));

        Paint tabsPaint = new Paint();
        tabsPaint.setColor(mAccentColor);
        tabsPaint.setStyle(Paint.Style.STROKE);
        tabsPaint.setStrokeWidth((int) (10f * App.SCALE_X));

        mTabHost = new TabsHost2(mContext, new String[]{mContext.getString(R.string.follow), mContext.getString(R.string.invite), mContext.getString(R.string.search)}, tabsPaint, mTabColor, new TabListener()
        {
            private int currentSelected = 1;
            @Override
            public void onTabSelected(String tabName, int index)
            {
                trackTabPressed(index);
                if(index < currentSelected)
                {
                    transitionLeft(currentSelected, index);
                }
                else transitionRight(currentSelected, index);
                currentSelected = index;
            }
        });

        tabs = new InviteTabContentBase[3];
        tabs[0] = new InviteTabContentFollow(mContext, paint, contacts, chosenContacts, mImageLoaderManager, new TabTitleListener()
        {
            @Override
            public void onTitleChanged(String newTitle)
            {
                mTabHost.setTitle(mContext.getString(R.string.follow), newTitle);
            }
        }, new InvalidateNotificationListener()
        {
            @Override
            public void invalidateNotification()
            {
                try
                {
                    tabs[2].post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            tabs[2].notifyDataSetChanged();
                        }
                    });
                }catch (Throwable zx){}
            }
        });
        tabs[1] = new InviteTabContentInvite(mContext, paint, contacts, mImageLoaderManager, new InvalidateNotificationListener()
        {
            @Override
            public void invalidateNotification()
            {
                try
                {
                    tabs[2].post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            tabs[2].notifyDataSetChanged();
                        }
                    });
                }catch (Throwable zx){}
            }
        }, false);

        tabs[2] = new InviteTabContentSearch(mContext, paint, contacts, chosenContacts, mImageLoaderManager, new SearchListener()
        {
            @Override
            public boolean followUnfollow(String id)
            {
                if (CAALogin.userIsAnonymous())
                {
                    ((InviteTabContentFollow) tabs[0]).startSignUpScreen();
                    return false;
                }
                for (User user : chosenContacts)
                {
                    if (user.getUserId().equals(id))
                    {
                        if (user.getFollowedByMe().equals("1"))
                        {
                            ((InviteTabContentFollow) tabs[0]).unFollowFriend(id);
                            return false;
                        }
                        else
                        {
                            ((InviteTabContentFollow) tabs[0]).followFriend(id);
                            return true;
                        }
                    }
                }
                return false;
            }

            @Override
            public void invite(ContactInfo contactInfo)
            {
                ((InviteTabContentInvite) tabs[1]).invite(contactInfo);
            }
        });

        try{unSetLoadingPlaceHolder();}catch (Throwable err){}

        mainView = new LinearLayout(mContext);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams.weight = 0.1f;
        mainView.setLayoutParams(layoutParams);
        mainView.addView(tabs[1]);

//        bottomStrip = new LinearLayout(mContext);
//        bottomStrip.setOrientation(VERTICAL);
//        bottomStrip.setBackgroundColor(Color.TRANSPARENT);
//        layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//        layoutParams.weight = 0.9f;
//        bottomStrip.setLayoutParams(layoutParams);
//        createBottomStrip();

        container = new LinearLayout(mContext);
        container.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        container.setOrientation(VERTICAL);
        container.addView(mainView);
//        container.addView(bottomStrip);

        removeAllViews();
        addView(mTabHost);
        addView(container);
        setBackgroundColor(Color.WHITE);

//        CAAWidgetSettingsSingleton.getInstance().attachPointsObserver(this);
    }

//    private void createBottomStrip() {
//        int shadowHeight = ((int)(10f * App.SCALE_X));
//        View shadowView = new View(mContext);
//        shadowView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, shadowHeight));
//        shadowView.setBackgroundResource(R.drawable.shadow_point_strip);
//
//        LinearLayout pointsLinear  = new LinearLayout(mContext);
//        pointsLinear.setBackgroundResource(R.color.white);
//        LinearLayout.LayoutParams pointsLinearParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//        pointsLinear.setLayoutParams(pointsLinearParams);
//        pointsLinear.setOrientation(HORIZONTAL);
//        pointsLinear.setGravity(Gravity.CENTER);
//
//        ImageView star1 = new ImageView(mContext);
//        star1.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//        star1.setImageResource(R.drawable.point_star);
//
//        ImageView star2 = new ImageView(mContext);
//        star2.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//        star2.setImageResource(R.drawable.point_star);
//
//        int padding = ((int)(15f * App.SCALE_X));
////        myPoints = new TextView(mContext);
////        myPoints.setTypeface(App.ROBOTO_MEDIUM);
////        myPoints.setTextColor(mContext.getResources().getColor(R.color.black));
////        myPoints.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.font_title_medium));
////        myPoints.setPadding(padding, 0, padding, 0);
////        myPoints.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
////        myPoints.setText(String.valueOf(CAAWidgetSettingsSingleton.getInstance().getMyPoints()));
//
////        myPoints.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                UserProgressMapDialog dialog = new UserProgressMapDialog(mContext, mImageLoaderManager);
////                dialog.show();
////            }
////        });
//
////        pointsLinear.addView(star1);
////        pointsLinear.addView(myPoints);
////        pointsLinear.addView(star2);
//
//        bottomStrip.addView(shadowView);
//        bottomStrip.addView(pointsLinear);
//    }

    private void transitionLeft(int currentSelected, int index)
    {
        tabs[currentSelected].startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.translate_left_out));
        tabs[index].startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.translate_left_in));
        mainView.removeView(tabs[currentSelected]);
        mainView.addView(tabs[index]);
    }

    private void transitionRight(int currentSelected, int index)
    {
        tabs[currentSelected].startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.translate_right_out));
        tabs[index].startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.translate_right_in));
        mainView.removeView(tabs[currentSelected]);
        mainView.addView(tabs[index]);
    }

    public boolean hasPermission()
    {
        return PackageManager.PERMISSION_DENIED != ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_CONTACTS);
    }

    public void checkInviteTabPermission()
    {
        ActivityCompat.requestPermissions(mContext, new String[]{Manifest.permission.READ_CONTACTS}, Constants.CRITICAL_PERMISSION);
    }

    @Override
    public void close()
    {
//        CAAWidgetSettingsSingleton.getInstance().removeObserver(this);
        removeAllViews();destroyDrawingCache();
        try{mainView.removeAllViews();mainView.destroyDrawingCache();}catch(Throwable e){}mainView = null;
        if(tabs != null)
        {
            for (int i = 0; i < tabs.length; i++)
                try
                {
                    tabs[i].close();
                    tabs[i] = null;
                }
                catch (Throwable e)
                {
                }
            tabs = null;
            mTabHost.close();
            mTabHost = null;
        }
        else if(getChildAt(0) instanceof InviteTabContentParkingView)
        {
            ((InviteTabContentParkingView)getChildAt(0)).close();
        }
//        mContext  = null;
        mImageLoaderManager = null;
    }

    @Override
    public View getOnboardingView() {
        return null;
    }

    public void permissionGranted()
    {
        setLoadingPlaceHolder();
        inviteHandler = new InviteHandler(mContext, this, true);
    }

    public void hideKeyboard()
    {
        if (tabs != null) {
            ((InviteTabContentSearch) tabs[2]).hideKeyboard();
        }
    }

    private void trackTabPressed(int tabIndex)
    {
        switch(tabIndex)
        {
            case 0 : // follow
                break;
            case 1 : // invite
                break;
            case 2 : // search
                break;
        }
    }

//    @Override
//    public void updatePoints() {
//        if (pointsViewDialog != null) {
//            pointsViewDialog.dismiss();
//            pointsViewDialog = null;
//        }
//
//        if(!mContext.getIsWidgetVisible()) {
//            pointsViewDialog = new PointsViewDialog(mContext, false, ((int) (120f * App.SCALE_Y)));
//            pointsViewDialog.showPointsBigLine(" Friend Invited!!");
//        }
//
//        mContext.addPointsInvite(myPoints);
//    }
}
