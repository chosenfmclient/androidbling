package fm.bling.blingy.inviteFriends.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 1/5/17.
 * History:
 * ***********************************
 */
public class CAASmsInvite {

    public CAASmsInvite(String phone, String name) {
        this.phone = phone;
        this.name = name;
    }

    @SerializedName("phone")
    private String phone;

    @SerializedName("name")
    private String name;
}
