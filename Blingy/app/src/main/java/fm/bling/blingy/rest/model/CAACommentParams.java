package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;

import fm.bling.blingy.videoHome.model.CAAVideo;


/************************************
 * Project: Chosen Android Application
 * FileName: CAACommentParams
 * Description:
 * Created by Dawidowicz Nadav on 10/01/2016.
 * Updated by Oren Zakay.
 * History:
 *************************************/

public class CAACommentParams {
    @SerializedName("comment_id")
    private String commentId;

    @SerializedName("user")
    private User user;

    @SerializedName("video_id")
    private String videoId;

    @SerializedName("text")
    private String text;

    @SerializedName("date")
    private String date;

    @SerializedName("video")
    CAAVideo video;

    public CAAVideo getVideo() {
        return video;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
