package fm.bling.blingy.splashScreen.views;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.splashScreen.SplashScreenActivity;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/3/16.
 * History: Updated by Oren Zakay.
 * ***********************************
 */
public class SplashScreenView extends LinearLayout
{
    private View logo;
    private float width = 500;
    public static boolean videoFinished = false;

    public SplashScreenView(final Context context, boolean animate)
    {
        super(context);
        setBackgroundResource(R.drawable.intro_bg);
        this.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                setHeights();
                SplashScreenView.this.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        float scaleX = ((float)context.getResources().getDisplayMetrics().widthPixels) / 1080f ;
        logo = new View(context);
        logo.setLayoutParams(new LayoutParams((int) ( width = width * scaleX), (int)width));
        logo.setBackgroundResource(R.drawable.intro_logo);
        logo.setVisibility(View.INVISIBLE);

        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

        setGravity(Gravity.CENTER);
        addView(logo);
        if (animate) {
            postDelayed(new Runnable() {
                @Override
                public void run() {
                    ScaleAnimation sa = new ScaleAnimation(0f, 1f, 0f, 1f, width / 2, width / 2);
                    sa.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            logo.setVisibility(View.VISIBLE);
                            videoFinished = true;
                            ((SplashScreenActivity) context).launch();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    sa.setInterpolator(new DecelerateInterpolator());
                    sa.setDuration(400);
                    logo.startAnimation(sa);

                    logo.invalidate();
                }
            }, 100);
        } else {
            logo.setVisibility(View.VISIBLE);
        }

    }

    private void setHeights() {
        if(App.STATUS_BAR_HEIGHT <= 0)
            App.STATUS_BAR_HEIGHT = getResources().getDisplayMetrics().heightPixels - SplashScreenView.this.getHeight();

        if(App.HEIGHT <= 0) {
            App.HEIGHT = SplashScreenView.this.getHeight();
            App.SCALE_Y =((float)App.HEIGHT) / 1920f;
        }
    }
}