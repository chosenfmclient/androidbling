package fm.bling.blingy;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Movie;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.File;

import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.dialogs.LoadingDialog;


/**
 * Created by Oren Zakay on 10/7/15.
 */
public class SupportActivity extends BaseActivity {


    private WebView mWebView;
    private LoadingDialog loadingDialog;
    private ValueCallback<Uri> mFilePathCallback;
    private boolean isFirst;
    //    private boolean pageLoaded = false;
    private final int REQUEST_FILE_PICKER = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);
        mWebView = (WebView) findViewById(R.id.prizes_webview);

        //Setting up the navigation icon.
        Toolbar toolbar = getActionBarToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
        toolbar.setNavigationContentDescription("backClose");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String url = "http://blingy.zendesk.com/";
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                return super.onShowFileChooser(webView, filePathCallback, fileChooserParams);
            }

            public void openFileChooser(ValueCallback<Uri> filePathCallback) {
                mFilePathCallback = filePathCallback;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("*/*");
                startActivityForResult(Intent.createChooser(i, "File Chooser"), REQUEST_FILE_PICKER);
            }

            public void openFileChooser(ValueCallback<Uri> filePathCallback, String acceptType) {
                openFileChooser(filePathCallback);
            }

            public void openFileChooser(ValueCallback<Uri> filePathCallback, String acceptType, String capture) {
                openFileChooser(filePathCallback);
            }
        });
        WebSettings mWebSettings = mWebView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setBlockNetworkImage(false);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setDomStorageEnabled(true);
        mWebSettings.setAllowFileAccessFromFileURLs(true);
        mWebSettings.setAllowUniversalAccessFromFileURLs(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            mWebSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

        if (NetworkConnectivityListener.isConnectedWithDialog(this)) {
            mWebView.loadUrl(url);
//            pageLoaded = true;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_FILE_PICKER) {
            if (mFilePathCallback != null) {
                Uri result = intent == null || resultCode != Activity.RESULT_OK ? null : intent.getData();
                if (result != null) {
                    String path = Utils.getPathFromMediaUri(getApplicationContext(), result);
                    Uri uri = Uri.fromFile(new File(path));
                    mFilePathCallback.onReceiveValue(uri);
                } else {
                    mFilePathCallback.onReceiveValue(null);
                }
            }

            mFilePathCallback = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (pageLoaded) {
//            finish();
//        }
    }

}
