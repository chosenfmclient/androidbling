package fm.bling.blingy.dialogs.share;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
//import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.BuildConfig;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.dialogs.messages.PostComposer;
import fm.bling.blingy.dialogs.share.model.CAANativeShare;
import fm.bling.blingy.dialogs.share.model.CAANativeVideo;
import fm.bling.blingy.dialogs.share.model.CAAPostPlatformAndTag;
import fm.bling.blingy.dialogs.share.model.CAAPostShare;
import fm.bling.blingy.dialogs.share.model.CAAShare;
import fm.bling.blingy.dialogs.share.workers.RenderVideoWithWatermark;
import fm.bling.blingy.dialogs.share.workers.listeners.onWorkerFinishedListener;
import fm.bling.blingy.editVideo.VideoPlayerActivity;
import fm.bling.blingy.homeScreen.HomeScreenActivity;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.record.callbacks.ActivityResultsReceiver;
import fm.bling.blingy.record.callbacks.PermissionResultReciever;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.dialogs.LoadingDialog;
import fm.bling.blingy.utils.views.ShareView;
import fm.bling.blingy.videoHome.VideoHomeActivity;
import fm.bling.blingy.videoHome.model.CAAVideo;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/************************************
 * Project: Chosen Android Application
 * FileName: ShareDialog
 * Description:
 * All sharing goes through this class, extend it to handle the content that is sent to the chosen platform
 * Created by Ben
 * History:
 *************************************/

public abstract class ShareDialogBase extends Dialog implements ActivityResultsReceiver, PermissionResultReciever, onWorkerFinishedListener {
    private int TWITTER_REQUEST_CODE = 101;
    private int SHARE_INTENT = 102;

    public final String SMS = "sms", EMAIL = "email", FACEBOOK = "facebook",
            TWITTER = "twitter", MMS = "mms", CLIP_BOARD = "copy_link",
            SNAPCHAT = "snapchat", INSTAGRAM = "instagram", MUSICALLY = "musically", MORE = "more", SAVE_TO_GALLERY = "save_to_gallery";


    private boolean isNative;

//    private ShareView facebookShare;
//    private ShareView twitterShare;
//    private ShareView mailShare;
//    private ShareView smsShare;
    protected ShareView mInstagramItem;
    private ShareView copyLink;
    private ShareView snapchatShare;
    private ShareView musicalyShare;
    private ShareView moreChooserShare;
    private ShareView saveToCameraRollShare; // only if the video is mine.

    private LinearLayout mainLayout;
    protected LinearLayout mFirstLine;
    private LinearLayout mSecondLine;

    private String subject;
    protected String content;
    protected String type;
    private String contentID;
    private String mShareTag;
    protected String mFilePath = null;
    protected String mInstagramFilePath = null;

    protected boolean mIsFacebookClicked = false;
    protected boolean isInstagramClicked = false;
    protected boolean isAfterRecording = false;

    protected CAAVideo mVideo;
    protected String videoType;
    protected String songName;
    protected String videoID;
    protected String userId;
    protected String artistName;
    private boolean isDuet = false;

    private CallbackManager mFacebookCallbackManager;
    private com.facebook.share.widget.ShareDialog shareDialog;

    protected boolean dismissed;
    protected String chosenPlatform;
    protected String shareID;
    protected Activity mContext;
    private LoadingDialog loadingDialog;
    private ShareDialogCallbackSetter mShareDialogCallbackSetter;

    private String[] accountManagerPermission;
    private final int ACCOUNT_MANAGER_PERMISSION = 17;
    private boolean accountDialogShown = false;
    private String shareUrl;

    protected String fileUserName;
    protected boolean isPhoto = false;
    private boolean moreClicked = false;


    public ShareDialogBase(Activity context, ShareDialogCallbackSetter shareDialogCallbackSetter, CAAVideo video) {
        super(context, android.R.style.Theme_Panel);
        this.mVideo = video;
        this.songName = video.getSongName();
        this.videoID = video.getVideoId();
        this.userId = video.getUser().getUserId();
        this.videoID = video.getVideoId();
        this.videoType = video.getMediaType();
        this.fileUserName = video.getUser().getFullName();
        this.isDuet = video.getType().equals(Constants.CAMEO_TYPE);
        init(context, shareDialogCallbackSetter);
        if (context instanceof BaseActivity)
            ((BaseActivity) context).setShowWidget(false);
    }


    public ShareDialogBase(Activity context, ShareDialogCallbackSetter shareDialogCallbackSetter, String userId, boolean isDuet) {
        super(context, android.R.style.Theme_Panel);
        this.userId = userId;
        this.isDuet = isDuet;
        init(context, shareDialogCallbackSetter);
        if (context instanceof BaseActivity)
            ((BaseActivity) context).setShowWidget(false);
    }

    private void init(Activity context, ShareDialogCallbackSetter shareDialogCallbackSetter) {
        mContext = context;
        mShareDialogCallbackSetter = shareDialogCallbackSetter;
        shareDialogCallbackSetter.setActivityResultsReceiver(this);
        shareDialogCallbackSetter.setPermissionResultReiever(this);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mainLayout = (LinearLayout) layoutInflater.inflate(R.layout.share_dialog_3, null);
        mFirstLine = (LinearLayout) mainLayout.findViewById(R.id.first_line);
        mSecondLine = (LinearLayout) mainLayout.findViewById(R.id.second_line);

        //FirstRow
        /** Instagram is created on its own dialog. see @{@link ShareDialogInstagram} **/
        snapchatShare = new ShareView(context, R.drawable.large_share_snapchat, R.string.snapchat_share);
        musicalyShare = new ShareView(context, R.drawable.large_share_musically, R.string.musicaly_share);

        //SecondRow
        copyLink = new ShareView(context, R.drawable.large_share_copy_link, R.string.copy_link);
        saveToCameraRollShare = new ShareView(context, R.drawable.large_share_save, R.string.save);
        moreChooserShare = new ShareView(context, R.drawable.large_share_more, R.string.share_more);

//        smsShare = new ShareView(context, R.drawable.large_share_sms, R.string.send_by_sms);
//        mailShare = new ShareView(context, R.drawable.large_share_mail, R.string.send_by_mail);
//        facebookShare = new ShareView(context, R.drawable.large_share_more, R.string.share_more);

        //ThirdRow
//        twitterShare = new ShareView(context, R.drawable.large_share_twitter, R.string.Twitter);


        /** Instagram is added on its own dialog. see @{@link ShareDialogInstagram} **/
        mFirstLine.addView(snapchatShare);
        mFirstLine.addView(musicalyShare);

        mSecondLine.addView(copyLink);
        if(Utils.userIsMe(userId)) mSecondLine.addView(saveToCameraRollShare);
        else saveToCameraRollShare = null;
        mSecondLine.addView(moreChooserShare);

//        facebookShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!Utils.canClick()) return;
//                onShareProccessStart();
//                TrackingManager.getInstance().tapShare(getNameByActivity(), videoID, artistName, songName, genre, shareID, FACEBOOK);
//                TrackingManager.getInstance().sharePlatform(getNameByActivity(), videoID, artistName, songName, genre, shareID, FACEBOOK);
//                if (moreClicked) {
//                    if (mContext instanceof VideoPlayerActivity) {
//                        mIsFacebookClicked = true;
//                        if (writeExternalStoragePermissions()) {
//                            if (!isFileExsist(mFilePath)) {
//                                if (loadingDialog == null)
//                                    loadingDialog = new LoadingDialog(mContext, false, false);
//                                ((VideoPlayerActivity) mContext).moveVideoToExternal();
//                            } else {
//                                onFacebookClicked();
//                            }
//                        }
//                    } else {
//                        onFacebookClicked();
//                    }
//                } else {
//                    extendTheDialog();
//                }
//            }
//
//        });

        if(saveToCameraRollShare != null){
            saveToCameraRollShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!Utils.canClick()) return;
                    onShareProccessStart();
                    TrackingManager.getInstance().sharePlatform(getNameByActivity(), videoID, artistName, songName, shareID, SAVE_TO_GALLERY);
                        chosenPlatform = SAVE_TO_GALLERY;
                        if (writeExternalStoragePermissions()) {
                            if (mFilePath != null && isFileExsist(mFilePath) && mFilePath.contains(Environment.getExternalStorageDirectory().getPath())) {
                                showSaveToGallery();
                            } else {
                                if (loadingDialog == null)
                                    loadingDialog = new LoadingDialog(mContext, false, false);
                                String pathWithWatermark = Environment.getExternalStorageDirectory().getPath() + "/Blingy/videos/" + songName.replace(" ", "_") + "_" + videoID.hashCode() + ".mp4";
                                if(new File(pathWithWatermark).exists()){
                                    mFilePath = pathWithWatermark;
                                    final String tempPath = mFilePath + "_temp.mp4";
                                    File tempFile = new File(tempPath);
                                    tempFile.deleteOnExit();
                                    File originalfile = new File(pathWithWatermark);
                                    Utils.copyFile(mFilePath, tempPath);
                                    originalfile.delete();
                                    Uri contentUri = Uri.fromFile(originalfile);
                                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
                                    mContext.sendBroadcast(mediaScanIntent);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Utils.copyFile(tempPath, mFilePath);
                                            showSaveToGallery();
                                        }
                                    },1000);
                                }
                                else
                                    new RenderVideoWithWatermark(mContext, isDuet, mVideo != null ? mVideo.getSharedUrl() : null, mFilePath, pathWithWatermark, ShareDialogBase.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        }

                }
            });
        }


        copyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.canClick()) return;
                onShareProccessStart();
                TrackingManager.getInstance().sharePlatform(getNameByActivity(), videoID, artistName, songName, shareID, CLIP_BOARD);
                chosenPlatform = CLIP_BOARD;
                getUrl();
            }
        });

        snapchatShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.canClick()) return;
                onShareProccessStart();
                TrackingManager.getInstance().sharePlatform(getNameByActivity(), videoID, artistName, songName, shareID, SNAPCHAT);
                if (checkAppInstalled(mContext.getString(R.string.snapchat_package_name))) {
                    chosenPlatform = SNAPCHAT;
                    if (writeExternalStoragePermissions()) {
                        if (mFilePath != null && isFileExsist(mFilePath) && mFilePath.contains(Environment.getExternalStorageDirectory().getPath())) {
                            File f = new File(mFilePath);
                            Uri contentUri = Uri.fromFile(f);
                            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
                            mContext.sendBroadcast(mediaScanIntent);
                            sendSnapchat();
                        } else {
                            if (loadingDialog == null)
                                loadingDialog = new LoadingDialog(mContext, false, false);
                            String pathWithWatermark = Environment.getExternalStorageDirectory().getPath() + "/Blingy/videos/" + songName.replace(" ", "_") + "_" + videoID.hashCode() + ".mp4";
                            if(new File(pathWithWatermark).exists()){
                                mFilePath = pathWithWatermark;
                                sendSnapchat();
                            }
                            else
                            new RenderVideoWithWatermark(mContext, isDuet, mVideo != null ? mVideo.getSharedUrl() : null, mFilePath, pathWithWatermark, ShareDialogBase.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                } else
                    Toast.makeText(mContext, "You must first install Snapchat. Visit the Google Play to download.", Toast.LENGTH_LONG).show();
            }

        });

        musicalyShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.canClick()) return;
                onShareProccessStart();
                TrackingManager.getInstance().sharePlatform(getNameByActivity(), videoID, artistName, songName, shareID, MUSICALLY);
                if (checkAppInstalled(mContext.getString(R.string.musicaly_package_name))) {
                    chosenPlatform = MUSICALLY;
                    if (writeExternalStoragePermissions()) {
                        if (mFilePath != null && isFileExsist(mFilePath) && mFilePath.contains(Environment.getExternalStorageDirectory().getPath())) {
                            File f = new File(mFilePath);
                            Uri contentUri = Uri.fromFile(f);
                            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
                            mContext.sendBroadcast(mediaScanIntent);
                            sendMusicaly();
                        } else {
                            if (loadingDialog == null)
                                loadingDialog = new LoadingDialog(mContext, false, false);
                            String pathWithWatermark = Environment.getExternalStorageDirectory().getPath() + "/Blingy/videos/" + "aaa_" + songName.replace(" ", "_") + "_m_" + videoID.hashCode() + ".mp4";
                            if(new File(pathWithWatermark).exists()){
                             mFilePath = pathWithWatermark;
                             sendMusicaly();
                            }
                            else
                                new RenderVideoWithWatermark(mContext, isDuet,MUSICALLY ,mVideo != null ? mVideo.getSharedUrl() : null, mFilePath, pathWithWatermark, ShareDialogBase.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                } else
                    Toast.makeText(mContext, "Install Musical.ly to enable Musical.ly sharing. Visit the Google Play Store to download.", Toast.LENGTH_LONG).show();
            }

        });
        moreChooserShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.canClick()) return;
                onShareProccessStart();
                TrackingManager.getInstance().sharePlatform(getNameByActivity(), videoID, artistName, songName, shareID, MORE);
                chosenPlatform = MORE;
                if (writeExternalStoragePermissions()) {
                    if (mFilePath != null && isFileExsist(mFilePath) && mFilePath.contains(Environment.getExternalStorageDirectory().getPath())) {
                        File f = new File(mFilePath);
                        Uri contentUri = Uri.fromFile(f);
                        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
                        mContext.sendBroadcast(mediaScanIntent);
                        openChooser();
                    } else {
                        if (loadingDialog == null)
                            loadingDialog = new LoadingDialog(mContext, false, false);
                        String pathWithWatermark = Environment.getExternalStorageDirectory().getPath() + "/Blingy/videos/" + songName.replace(" ", "_") + "_" + videoID.hashCode() + ".mp4";
                        if(new File(pathWithWatermark).exists()){
                            mFilePath = pathWithWatermark;
                            openChooser();
                        }
                        else
                            new RenderVideoWithWatermark(mContext, isDuet, mVideo != null ? mVideo.getSharedUrl() : null, mFilePath, pathWithWatermark, ShareDialogBase.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }

            }
        });


//        twitterShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onShareProccessStart();
//                TrackingManager.getInstance().tapShare(getNameByActivity(), videoID, artistName, songName, genre, shareID, TWITTER);
//                TrackingManager.getInstance().sharePlatform(getNameByActivity(), videoID, artistName, songName, genre, shareID, TWITTER);
//                onTwitterClicked();
//            }
//        });
//
//        mailShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onShareProccessStart();
//                TrackingManager.getInstance().tapShare(getNameByActivity(), videoID, artistName, songName, genre, shareID, EMAIL);
//                TrackingManager.getInstance().sharePlatform(getNameByActivity(), videoID, artistName, songName, genre, shareID, EMAIL);
//                onMailClicked();
//            }
//        });
//
//        smsShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onShareProccessStart();
//                TrackingManager.getInstance().tapShare(getNameByActivity(), videoID, artistName, songName, genre, shareID, SMS);
//                TrackingManager.getInstance().sharePlatform(getNameByActivity(), videoID, artistName, songName, genre, shareID, SMS);
//                onSmsClicked();
//            }
//        });

        setContentView(mainLayout);
        TrackingManager.getInstance().tapShare(getNameByActivity(), videoID);
        show();
    }

    protected void onShareProccessStart() {
        if (this.mShareDialogCallbackSetter != null && this.mShareDialogCallbackSetter instanceof ShareDialogProccessCalback)
            ((ShareDialogProccessCalback) this.mShareDialogCallbackSetter).onStartShareProcess();
    }

    protected void onShareProccessEnd() {
        if (this.mShareDialogCallbackSetter != null && this.mShareDialogCallbackSetter instanceof ShareDialogProccessCalback)
            ((ShareDialogProccessCalback) this.mShareDialogCallbackSetter).onEndShareProcess();
    }

    private void showSaveToGallery(){
        dismissLoadingDialog();
        File f = new File(mFilePath);
        Uri contentUri = Uri.fromFile(f);
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
        mContext.sendBroadcast(mediaScanIntent);
        Toast.makeText(mContext,"Your video has been saved to your gallery",Toast.LENGTH_SHORT).show();
        onShareProccessEnd();
    }

    private void sendSnapchat() {
        try {
            dismissLoadingDialog();
            if (SharedPreferencesManager.getInstance().getBoolean(Constants.FIRST_SNAP, true)) {
                SharedPreferencesManager.getEditor().putBoolean(Constants.FIRST_SNAP, false).commit();
                BaseDialog snapDialog = new BaseDialog(mContext, mContext.getString(R.string.video_save_to_gallery), mContext.getString(R.string.you_can_share_throw_memories));
                snapDialog.setOKText(mContext.getString(R.string.go_to_snap));
                snapDialog.setCancelText(mContext.getString(R.string.maybe_later));
                snapDialog.setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        onShareProccessEnd();
                        if (((BaseDialog) dialogInterface).getAnswer()) {
                            Intent intent = mContext.getPackageManager().getLaunchIntentForPackage(mContext.getString(R.string.snapchat_package_name));
                            mContext.startActivity(intent);
                            TrackingManager.getInstance().shareCompleted();
                        }
                    }
                });
                snapDialog.show();

            } else {
                onShareProccessEnd();
                TrackingManager.getInstance().shareCompleted();
                Intent intent = mContext.getPackageManager().getLaunchIntentForPackage(mContext.getString(R.string.snapchat_package_name));
                mContext.startActivity(intent);
            }

            CAAPostPlatformAndTag caaPostPlatformAndTag = new CAAPostPlatformAndTag(mShareTag, chosenPlatform);
            App.getUrlService().postVideoShare(contentID, caaPostPlatformAndTag, new Callback<CAAShare>() {
                @Override
                public void success(CAAShare caaShare, Response response) {

                }

                @Override
                public void failure(RetrofitError error) {

                }
            });

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void sendMusicaly() {
        try {
            dismissLoadingDialog();
            BaseDialog snapDialog = new BaseDialog(mContext, mContext.getString(R.string.video_save_to_gallery), mContext.getString(R.string.musicaly_shre_info));
            snapDialog.setOKText(mContext.getString(R.string.go_to_musicaly));
            snapDialog.setCancelText(mContext.getString(R.string.maybe_later));
            snapDialog.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    onShareProccessEnd();
                    if (((BaseDialog) dialogInterface).getAnswer()) {
                        Intent intent = mContext.getPackageManager().getLaunchIntentForPackage(mContext.getString(R.string.musicaly_package_name));
                        mContext.startActivity(intent);
                        TrackingManager.getInstance().shareCompleted();
                    }
                }
            });
            snapDialog.show();

            CAAPostPlatformAndTag caaPostPlatformAndTag = new CAAPostPlatformAndTag(mShareTag, chosenPlatform);
            App.getUrlService().postVideoShare(contentID, caaPostPlatformAndTag, new Callback<CAAShare>() {
                @Override
                public void success(CAAShare caaShare, Response response) {

                }

                @Override
                public void failure(RetrofitError error) {

                }
            });

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void openChooser(){

        // Create the new Intent using the 'Send' action.
        dismissLoadingDialog();
        try {
            chosenPlatform = MORE;
//            Intent share = new Intent(Intent.ACTION_SEND);
//            // Set the MIME type
//            share.setType("video/mp4");
//            // Create the URI from the media
//            File media = new File(mFilePath);
//            Uri uri = Uri.fromFile(media);
//            // Add the URI to the Intent.
//            share.putExtra(Intent.EXTRA_STREAM, uri);
//            share.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
//            // Broadcast the Intent.
////            mContext.startActivity(share);
//            mContext.startActivityForResult(Intent.createChooser(share, "Share Your Video"), SHARE_INTENT);
            createChooser();
//            CAAPostPlatformAndTag caaPostPlatformAndTag = new CAAPostPlatformAndTag(mShareTag, chosenPlatform);
//            App.getUrlService().postVideoShare(contentID, caaPostPlatformAndTag, new Callback<CAAShare>() {
//                @Override
//                public void success(CAAShare caaShare, Response response) {
//
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//
//                }
//            });


        } catch (Throwable e) {
            Log.e("ShareBase" , e.getMessage());
        }
    }

    public void createChooser() {

        List<Intent> shareIntentsLists = new ArrayList<>();
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("video/mp4");
        File media = new File(mFilePath);
        Uri uri = Uri.fromFile(media);
        List<ResolveInfo> resInfos = mContext.getPackageManager().queryIntentActivities(shareIntent, 0);
        if (!resInfos.isEmpty()) {
            for (ResolveInfo resInfo : resInfos) {
                String packageName = resInfo.activityInfo.packageName;
                if (!packageName.equals(mContext.getString(R.string.instagram_package_name))) {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("video/mp4");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    intent.setPackage(packageName);
                    shareIntentsLists.add(intent);
                }
            }
            if (!shareIntentsLists.isEmpty()) {
                Intent chooserIntent = Intent.createChooser(shareIntentsLists.remove(0), "Share Your Video");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, shareIntentsLists.toArray(new Parcelable[]{}));
                chooserIntent.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
                mContext.startActivityForResult(chooserIntent, SHARE_INTENT);
            } else
                Log.e("Error", "No Apps can perform your task");

        }
    }

//    private void extendTheDialog() {
//        moreClicked = true;
//        facebookShare.setIcon(R.drawable.large_share_facebook);
//        facebookShare.setText(R.string.Facebook);
//        ValueAnimator progressAnimation = ValueAnimator.ofInt(mainLayout.getHeight(), mainLayout.getHeight() + mSecondLine.getHeight()).setDuration(300);
//        progressAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
//        progressAnimation.start();
//        progressAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public synchronized void onAnimationUpdate(ValueAnimator valueAnimator) {
//                int val = (Integer) valueAnimator.getAnimatedValue();
//                mainLayout.getLayoutParams().height = val;
//            }
//
//        });
//        progressAnimation.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animator) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animator) {
//                addThirdRow();
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animator) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animator) {
//
//            }
//        });
//    }

//    private void addThirdRow() {
//        int padding = mContext.getResources().getDimensionPixelOffset(R.dimen.main_margin);
//        LinearLayout thirdRow = new LinearLayout(mContext);
//        thirdRow.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//        thirdRow.setOrientation(LinearLayout.HORIZONTAL);
//        thirdRow.setPadding(0, 0, 0, padding);
//        thirdRow.setWeightSum(1f);
//        thirdRow.setBackgroundResource(R.color.white);
//        thirdRow.addView(twitterShare);
//        thirdRow.addView(copyLink);
//        thirdRow.addView(moreChooserShare);
//        mainLayout.addView(thirdRow, mainLayout.getChildCount());
//    }

    public abstract void onInstagramClicked();

    public abstract void onFacebookClicked();

    public abstract void onTwitterClicked();

    public abstract void onSmsClicked();

    public abstract void onMailClicked();

    @Override
    public void show() {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        Animation slideIn = AnimationUtils.loadAnimation(mContext, R.anim.slide_up);
        slideIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (mShareDialogCallbackSetter != null && mShareDialogCallbackSetter instanceof ShareDialogVisibilityCallback)
                    ((ShareDialogVisibilityCallback) mShareDialogCallbackSetter).onShareDialogShow();
                mainLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        super.show();
        mainLayout.startAnimation(slideIn);
    }

    public void dismiss() {
        if (dismissed)
            return;

        if (mContext instanceof BaseActivity)
            ((BaseActivity) mContext).setShowWidget(true);

        if (mShareDialogCallbackSetter != null && mShareDialogCallbackSetter instanceof ShareDialogVisibilityCallback)
            ((ShareDialogVisibilityCallback) mShareDialogCallbackSetter).onShareDialogDismissed();
        dismissed = true;
        Animation slideOut = AnimationUtils.loadAnimation(mContext, R.anim.slide_down);
        slideOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mainLayout.setVisibility(View.INVISIBLE);
                superDismiss();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        mainLayout.startAnimation(slideOut);
    }

    public boolean onTouchEvent(MotionEvent me) {
        if (!dismissed) {
            if (me.getAction() == MotionEvent.ACTION_DOWN) {
                if (me.getY() < mainLayout.getTop())    //me.getX() < mainLayout.getLeft() || me.getX() > mainLayout.getRight() ||
                {
                    dismiss();
                }
            }
        }
        return true;
    }

    private void superDismiss() {
        super.dismiss();
    }

    @Override
    public void onStop() {
        super.onStop();
        mainLayout.removeAllViews();
        mainLayout.destroyDrawingCache();
        mainLayout = null;

//        facebookShare.removeAllViews();
//        facebookShare.destroyDrawingCache();
//        facebookShare = null;
//        twitterShare.removeAllViews();
//        twitterShare.destroyDrawingCache();
//        twitterShare = null;
//        mailShare.removeAllViews();
//        mailShare.destroyDrawingCache();
//        mailShare = null;
//        smsShare.removeAllViews();
//        smsShare.destroyDrawingCache();
//        smsShare = null;

        musicalyShare.removeAllViews();
        musicalyShare.destroyDrawingCache();
        musicalyShare = null;

        snapchatShare.removeAllViews();
        snapchatShare.destroyDrawingCache();
        snapchatShare = null;

        if(saveToCameraRollShare != null) {
            saveToCameraRollShare.removeAllViews();
            saveToCameraRollShare.destroyDrawingCache();
            saveToCameraRollShare = null;
        }

        copyLink.removeAllViews();
        copyLink.destroyDrawingCache();
        copyLink = null;
    }

    public void setFilePath(String path) {
        this.mFilePath = path;
    }

    protected void shareToInstagram(String type) {
        // Create the new Intent using the 'Send' action.
        try {
            onShareProccessEnd();
            chosenPlatform = INSTAGRAM;
            Intent share = new Intent(Intent.ACTION_SEND);

            // Set the MIME type
            share.setType(type);

            // Create the URI from the media
            File media = new File(mInstagramFilePath);
            Uri uri = Uri.fromFile(media);

            // Add the URI to the Intent.
            share.putExtra(Intent.EXTRA_STREAM, uri);

            share.setPackage(mContext.getResources().getString(R.string.instagram_package_name));
            share.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);

            // Broadcast the Intent.
            mContext.startActivity(share);
            CAAPostPlatformAndTag caaPostPlatformAndTag = new CAAPostPlatformAndTag(mShareTag, chosenPlatform);
            App.getUrlService().postVideoShare(contentID, caaPostPlatformAndTag, new Callback<CAAShare>() {
                @Override
                public void success(CAAShare caaShare, Response response) {

                }

                @Override
                public void failure(RetrofitError error) {

                }
            });


        } catch (Throwable e) {
        }
    }

    protected final void sendSms()//String content)
    {
        if (!Utils.canClick()) return;

        chosenPlatform = SMS;
        //this.content   = content + " ";
        getUrl();
    }

    private void sendSmsInternal() {
        onShareProccessEnd();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("smsto:"));
        intent.putExtra("sms_body", content);// + shareUrl
        intent.putExtra("exit_on_sent", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void copyLink() {
        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(shareUrl, shareUrl);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(mContext, "Link copied to clipboard", Toast.LENGTH_SHORT).show();
        onShareProccessEnd();
    }

    protected final void sendEmail()//String subject, String content)
    {
        if (!Utils.canClick()) return;

        chosenPlatform = EMAIL;
        getUrl();
    }

    private void sendEmailInternal() {
        onShareProccessEnd();
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));//emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, content); // + shareUrl
        emailIntent.setType("message/rfc822");
        mContext.startActivity(Intent.createChooser(emailIntent, "Send an email using :"));
    }

    protected void sendFacebook() {
        if (mFacebookCallbackManager == null) {
            mFacebookCallbackManager = CallbackManager.Factory.create();
        }
        chosenPlatform = FACEBOOK;

        if (type.equals(Constants.VIDEO_TYPE) || type.equals(Constants.PHOTO_TYPE)) {
            if (CAAUserDataSingleton.getInstance().getNativeShareSetting() == null) {
                getNativeShareSettings();
            } else {
                if (CAAUserDataSingleton.getInstance().getNativeShareSetting()) {
                    facebookNativeShareSettings();
                } else {
                    getUrl();
                }
            }
        } else {
            getUrl();
        }
    }

    private void facebookNativeShareSettings() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        if (accessToken != null) {
            if (accessToken.isExpired()) {
                AccessToken.refreshCurrentAccessTokenAsync(new AccessToken.AccessTokenRefreshCallback() {
                    @Override
                    public void OnTokenRefreshed(AccessToken accessToken) {
                        AccessToken.setCurrentAccessToken(accessToken);
                        facebookNativeShareSettings();
                    }

                    @Override
                    public void OnTokenRefreshFailed(FacebookException exception) {
                        failedPostingToFacebook();
                    }
                });
                return;
            }

            Set<String> permissions = accessToken.getPermissions();
            if (permissions.contains("publish_actions")) {
                isNative = true;
                dismiss();
                goTo();
            } else {
                facebookPublishLogin();
                return;
            }
        } else {
            facebookPublishLogin();
            return;
        }
    }

    private void sendFacebookPhaseTwo() {
        shareDialog = new com.facebook.share.widget.ShareDialog(mContext);
        shareDialog.registerCallback(mFacebookCallbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                TrackingManager.getInstance().shareCompleted();
                addStateMachinShareEvent(chosenPlatform);
                onShareProccessEnd();
//                MATEvent event = new MATEvent("share").withContentType("facebook");
//                MobileAppTracker.getInstance().measureEvent(event);
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException e) {
            }
        });

        ShareLinkContent ShareLinkContent = new ShareLinkContent.Builder()
                .setContentTitle(content)
                .setContentUrl(Uri.parse(shareUrl))
                .build();
        shareDialog.show(ShareLinkContent);
    }

    protected final void sendTwitter()//String subject, String content)
    {
        if (!Utils.canClick()) return;

        chosenPlatform = TWITTER;

        if (Utils.userHasTwitterApp(mContext.getApplicationContext())) {
            if (accountManagerPermissions()) {
                AccountManager am = AccountManager.get(mContext);
                Account[] accounts = am.getAccountsByType(Constants.TWITTER_ACCOUNT_TYPE);

                Constants.mTwitterVerion = Constants.mTwitterVerion.replace(".", "");
                int twitterInstalledVersion = Integer.parseInt(Constants.mTwitterVerion);
                if (accounts.length > 0 || twitterInstalledVersion >= Constants.TWITTER_VER_HANDLE_LOGIN) {
                    getUrl();
                } else {
                    if (twitterInstalledVersion < Constants.TWITTER_VER_HANDLE_LOGIN) {
                        Toast toast = Toast.makeText(mContext, R.string.sign_in_twitter_to_share, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
            }
        } else {
            Toast toast = Toast.makeText(mContext, R.string.twitter_no_app, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

//    private void sendTwitterInternal() {
//        Intent intent = new TweetComposer.Builder(mContext)
//                .text(content)
//                .createIntent();
//        mContext.startActivityForResult(intent, TWITTER_REQUEST_CODE);
//    }

    protected boolean writeExternalStoragePermissions() {
        String[] permissions = new String[]{"", ""};
        if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            permissions[0] = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        }
        if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            permissions[1] = Manifest.permission.READ_EXTERNAL_STORAGE;
        }
        if ((!permissions[0].isEmpty() || !permissions[1].isEmpty())) {
            ActivityCompat.requestPermissions(mContext, permissions, Constants.OPTIONAL_PERMISSION);
            return false;
        } else {
            return true;
        }
    }

    private boolean accountManagerPermissions() {
        accountManagerPermission = new String[]{""};
        if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(mContext, Manifest.permission.GET_ACCOUNTS)) {
            accountManagerPermission[0] = Manifest.permission.GET_ACCOUNTS;
        }
        if (!accountManagerPermission[0].isEmpty()) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(mContext, Manifest.permission.GET_ACCOUNTS)) && !accountDialogShown) {
                accountDialogShown = true;
                accountManagerDialog();
            } else {
                ActivityCompat.requestPermissions(mContext, accountManagerPermission,
                        ACCOUNT_MANAGER_PERMISSION);
            }
        } else {
            return true;
        }
        return false;
    }

    private void accountManagerDialog() {
        BaseDialog dialog = new BaseDialog(mContext, "Permissions", mContext.getResources().getString(R.string.account_manager_permission_dialog));
        dialog.show();
        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                accountDialogShown = false;
                if (((BaseDialog) dialogInterface).getAnswer()) {
                    accountManagerPermission = new String[]{""};
                    if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(mContext, Manifest.permission.GET_ACCOUNTS)) {
                        accountManagerPermission[0] = Manifest.permission.GET_ACCOUNTS;
                    }
                    ActivityCompat.requestPermissions(mContext, accountManagerPermission,
                            ACCOUNT_MANAGER_PERMISSION);
                }
            }
        });
    }

    protected boolean checkAppInstalled(String packageName) {
        PackageManager pm = mContext.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void getNativeShareSettings() {
        App.getUrlService().getNativeVideo(new Callback<CAANativeVideo>() {
            @Override
            public void success(CAANativeVideo caaNativeVideo, Response response) {
                Boolean nativeShareSetting = caaNativeVideo.getNativeVideo().equalsIgnoreCase("1");
                CAAUserDataSingleton.getInstance().setNativeShareSetting(nativeShareSetting);
                sendFacebook();
            }

            @Override
            public void failure(RetrofitError error) {
                CAAUserDataSingleton.getInstance().setNativeShareSetting(true);
                sendFacebook();
            }

        });
    }

    private void facebookPublishLogin() {
        LoginManager loginManager = LoginManager.getInstance();
        loginManager.registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken.setCurrentAccessToken(loginResult.getAccessToken());
                facebookNativeShareSettings();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException e) {
                failedPostingToFacebook();
            }

        });
        List<String> permissionNeeds = Arrays.asList("publish_actions");
        loginManager.logInWithPublishPermissions(mContext, permissionNeeds);
    }

    private void openPostFacebookVideoDialog() {
        if (mContext != null) {
//            String message = isPhoto ? mContext.getString(R.string.updload_to_facebook_would_you_photo) : mContext.getString(R.string.updload_to_facebook_would_you_video);
            String message =  mContext.getString(R.string.updload_to_facebook_would_you_video);
            BaseDialog mDialog = new BaseDialog(mContext, mContext.getResources().getString(R.string.updload_to_facebook), message);
            mDialog.show();
            mDialog.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    mIsFacebookClicked = false;
                    if (((BaseDialog) dialog).getAnswer()) {
                        nativeFacebookShare();
                    } else {
                        dismissLoadingDialog();
                    }
                }
            });
        }
    }

    private void nativeFacebookShare() {
        final PostComposer postComposer = PostComposer.newInstance(mContext, "Facebook", content, mContext.getResources().getString(R.string.cancel), "POST");

        postComposer.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (postComposer.getAnswer()) {
                    if (ShareDialogBase.this.mFilePath != null) {
                        try {
                            if(AccessToken.getCurrentAccessToken() != null && AccessToken.getCurrentAccessToken().isExpired())
                                AccessToken.refreshCurrentAccessTokenAsync();
                        }catch (Throwable throwable){
//                            if(BuildConfig.DEBUG)
                        }
                        if (loadingDialog == null)
                            loadingDialog = new LoadingDialog(mContext, false, false);
                        if (isPhoto)
                            uploadPhoto(postComposer.getInputText());
                        else uploadVideo(postComposer.getInputText());
                    } else {
                        App.getUrlService().postNativeVideo(new CAANativeShare(contentID, AccessToken.getCurrentAccessToken().getToken(), postComposer.getInputText()), new Callback<CAANativeVideo>() {
                            @Override
                            public void success(CAANativeVideo caaNativeVideo, Response response) {
                                try {
                                    if (mContext != null && !mContext.isFinishing()) {
                                        BaseDialog successDialog = new BaseDialog(mContext, "Success", songName + " has been Uploaded!");
                                        successDialog.removeCancelbutton();
                                        successDialog.show();
                                        successDialog.setOnDismissListener(new OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {
                                                onShareProccessEnd();
                                            }
                                        });
                                    }
                                } catch (Throwable err) {
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }
                } else
                    dismissLoadingDialog();
            }
        });

        postComposer.show();
        postComposer.dialogText.callOnClick();
    }

    private void uploadPhoto(String postComposerText) {
        if (ShareDialogBase.this.mFilePath != null) {
            if (loadingDialog == null)
                loadingDialog = new LoadingDialog(mContext, false, false);
            GraphRequest request = GraphRequest.newPostRequest(AccessToken.getCurrentAccessToken(), "me/photos", null, new GraphRequest.Callback() {
                @Override
                public void onCompleted(GraphResponse response) {
                    dismissLoadingDialog();
                    if (response.toString().contains("responseCode: 200")) {
                        TrackingManager.getInstance().shareCompleted();
                        try {
                            if (mContext != null && !mContext.isFinishing()) {
                                BaseDialog successDialog = new BaseDialog(mContext, "Success", songName + " has been Uploaded!");
                                successDialog.removeCancelbutton();
                                successDialog.show();
                                successDialog.setOnDismissListener(new OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        onShareProccessEnd();
                                    }
                                });
                            }
                        } catch (Throwable err) {
                        }
                    }
                    if (loadingDialog != null)
                        loadingDialog.dismiss();
                }
            });

            Bundle params = request.getParameters();
            try {
                byte[] data = readBytes(ShareDialogBase.this.mFilePath);
                params.putByteArray("photo.jpg", data);
                params.putString("title", songName);
                params.putString("caption", postComposerText);
                request.setParameters(params);
                request.executeAsync();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private FacebookUploadData facebookUploadData;

    private void uploadVideo(String postComposerText) {
        GraphRequest request = GraphRequest.newPostRequest(AccessToken.getCurrentAccessToken(), "me/videos", null, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse response) {
                if (response.toString().contains("responseCode: 200")) {
                    try {
                        if (facebookUploadData.isFinished()) {
                            TrackingManager.getInstance().shareCompleted();
                            loadingDialog.dismiss();
                            loadingDialog = null;
                            facebookUploadData.close();
                            facebookUploadData = null;

                            if (mContext != null && !mContext.isFinishing()) {
                                BaseDialog successDialog = new BaseDialog(mContext, "Success", songName + " has been Uploaded!");
                                successDialog.removeCancelbutton();
                                successDialog.show();
                                successDialog.setOnDismissListener(new OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        onShareProccessEnd();
                                    }
                                });
                            }
                        } else {
                            facebookUploadData.chunkSent(response);
                            facebookUploadData.sendChunk();
                        }
                    } catch (Throwable err) {
                        err.printStackTrace();
                    }
                } else if (loadingDialog != null) {
                    loadingDialog.dismiss();
                    Toast.makeText(mContext, "An error occured while uploading the file, please try again", Toast.LENGTH_LONG).show();
                    try {
                        facebookUploadData.close();
                    } catch (Throwable wee) {
                    }
                    facebookUploadData = null;
                }
            }
        });

        try {
            facebookUploadData = new FacebookUploadData(request, postComposerText);
        } catch (Throwable err) {
            err.printStackTrace();
        }
        facebookUploadData.sendChunk();
    }

    private class FacebookUploadData {
        private final int chunkSize = 1024 * 1024;
        private String sessionId;
        private int videoId;
        final int fileSize;
        private boolean isFinished;
        private String content;

        int startOffeset;
        int endOffset;
        FileInputStream fis;
        GraphRequest request;
        FileChannel fc;
        Bundle params;
        byte phase = 0; // 0 - start ; 1 - progress; 2 - ended;

        private FacebookUploadData(GraphRequest graphRequest, String postComposerText) throws Throwable {
            File file = new File(ShareDialogBase.this.mFilePath);
            fis = new FileInputStream(file);
            request = graphRequest;
            fc = fis.getChannel();
            fileSize = (int) file.length();
            content = postComposerText;

            params = request.getParameters();
        }

        private byte[] getNextChunk() throws Throwable {
            if (fc.position() != startOffeset) {
                fc.position(startOffeset);
            }
            int count = endOffset - startOffeset;
            byte[] chunk = new byte[count];
            fis.read(chunk, 0, count);
            return chunk;
        }

        private String getUploadPhase() throws Throwable {
            if (phase == 0)
                return "start";
            if (phase == 2)
                return "finish";
            return "transfer";

        }

        private void chunkSent(GraphResponse response) throws Throwable {
            endOffset = response.getJSONObject().getInt("end_offset");

            startOffeset = response.getJSONObject().getInt("start_offset");
            if (phase == 0) {
                phase = 1;
                sessionId = response.getJSONObject().getString("upload_session_id");
                videoId = response.getJSONObject().getInt("video_id");
                params.clear();
                params.putString("upload_session_id", sessionId);
            } else if (startOffeset == endOffset) {
                params.clear();
                params.putString("upload_session_id", sessionId);
                params.putString("title", songName);
                params.putString("description", content);

                phase = 2;
            }
        }

        private void sendChunk() {
            try {
                if (phase == 1) {
                    byte[] data = getNextChunk();
                    params.putByteArray("video_file_chunk", data);

                    params.putInt("start_offset", startOffeset);
                } else if (phase == 0) {
                    params.putInt("file_size", fileSize);
                } else
                    isFinished = true;
                params.putString("upload_phase", getUploadPhase());
                request.setParameters(params);
                request.executeAsync();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }

        private boolean isFinished() {
            return isFinished;
        }

        private void close() {
            try {
                fis.close();
            } catch (Throwable err) {
            }
            try {
                fc.close();
            } catch (Throwable err) {
            }

            request = null;
            fc = null;
            fis = null;
        }

    }

    public byte[] readBytes(String dataPath) throws IOException {

        InputStream inputStream = new FileInputStream(dataPath);
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];

        int len;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        return byteBuffer.toByteArray();
    }

    /**
     * for creating games we need the tile id and the share id
     */
    private synchronized void getUrl() {
        if (loadingDialog == null)
            loadingDialog = new LoadingDialog(mContext, false, false);

//        dismiss();
        CAAPostPlatformAndTag caaPostPlatformAndTag = new CAAPostPlatformAndTag(mShareTag, chosenPlatform);
        switch (type) {
            case Constants.VIDEO_TYPE:
                App.getUrlService().postVideoShare(contentID, caaPostPlatformAndTag, new Callback<CAAShare>() {
                    @Override
                    public void success(CAAShare caaShare, Response response) {
                        shareID = caaShare.getData().getShareId();
                        shareUrl = Constants.BASE_URL + "/v/" + contentID + "_" + shareID;
                        content = caaShare.getData().getContent();
                        subject = caaShare.getData().getSubject();
                        dismissLoadingDialog();
                        goTo();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        dismissLoadingDialog();
                    }
                });
                break;
            case Constants.USER_TYPE:
                App.getUrlService().postUsersShare(contentID, caaPostPlatformAndTag, new Callback<CAAShare>() {
                    @Override
                    public void success(CAAShare caaShare, Response response) {
                        shareID = caaShare.getData().getShareId();
                        shareUrl = String.format(Constants.BASE_URL + "/u/%1s_%2s", contentID, shareID);
                        content = caaShare.getData().getContent();
                        subject = caaShare.getData().getSubject();
                        dismissLoadingDialog();
                        goTo();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        dismissLoadingDialog();
                    }
                });
                break;
            case Constants.PHOTO_TYPE:
                App.getUrlService().postVideoShare(contentID, caaPostPlatformAndTag, new Callback<CAAShare>() {
                    @Override
                    public void success(CAAShare caaShare, Response response) {
                        shareID = caaShare.getData().getShareId();
                        shareUrl = String.format(Constants.BASE_URL + "/p/%1s_%2s", contentID, shareID);
                        content = caaShare.getData().getContent();
                        subject = caaShare.getData().getSubject();
                        dismissLoadingDialog();
                        goTo();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        dismissLoadingDialog();
                    }
                });
                break;
        }
    }

    private void goTo() {
        switch (chosenPlatform) {
            case SMS:
                sendSmsInternal();
                callShareApi("sms");
                addStateMachinShareEvent(chosenPlatform);
//                MATEvent smsEvent = new MATEvent("share").withContentType("sms");
//                MobileAppTracker.getInstance().measureEvent(smsEvent);
                break;
            case EMAIL:
                sendEmailInternal();
                callShareApi("email");
                addStateMachinShareEvent(chosenPlatform);
//                MATEvent mailEvent = new MATEvent("share").withContentType("email");
//                MobileAppTracker.getInstance().measureEvent(mailEvent);
                break;
            case FACEBOOK:
                if (isNative)
                    openPostFacebookVideoDialog();
                else
                    sendFacebookPhaseTwo();
                break;
//            case TWITTER:
//                sendTwitterInternal();
//                break;
            case CLIP_BOARD:
                copyLink();
//            case MMS:
//                sendMms();
//                callShareApi("mms");
//                doneLoading();
        }
    }

    private void addStateMachinShareEvent(String platform) {
        StateMachineEvent.EVENT_TYPE eventType;
        switch (platform) {
            case SMS:
                eventType = StateMachineEvent.EVENT_TYPE.SHARE_SMS;
                break;
            case EMAIL:
                eventType = StateMachineEvent.EVENT_TYPE.SHARE_EMAIL;
                break;
            case FACEBOOK:
                eventType = StateMachineEvent.EVENT_TYPE.SHARE_FACEBOOK;
                break;
            case TWITTER:
                eventType = StateMachineEvent.EVENT_TYPE.SHARE_TWITTER;
                break;
            default:
                eventType = StateMachineEvent.EVENT_TYPE.SHARE_OTHER;
                break;
        }

        StateMachine.getInstance(mContext.getApplicationContext()).insertEvent(
                new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SHARING, eventType.ordinal())
        );
    }

    private void callShareApi(String platForm) {
        CAAPostShare postShare = new CAAPostShare(platForm, shareID, content);
        switch (type) {
            case Constants.VIDEO_TYPE:
            case Constants.PHOTO_TYPE:
                App.getUrlService().putVideoShare(contentID, postShare, new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {
                    }

                    @Override
                    public void failure(RetrofitError error) {
                    }
                });
                break;
            case Constants.USER_TYPE:
                App.getUrlService().putUserShare(contentID, postShare, new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {
                    }

                    @Override
                    public void failure(RetrofitError error) {
                    }
                });
                break;
        }
    }

    private void dismissLoadingDialog() {
        try {
            if(loadingDialog != null)
                loadingDialog.dismiss();
        } catch (Throwable err) {
            err.printStackTrace();
        }
        loadingDialog = null;
    }

    private void lateDoneLoading() {
        switch (chosenPlatform) {
            case FACEBOOK:
                callShareApi("facebook");
                break;
            case TWITTER:
                callShareApi("twitter");
                break;
        }
    }

    private void failedPostingToFacebook() {
        Toast.makeText(mContext, R.string.failed_post_to_fb, Toast.LENGTH_LONG).show();
    }

    protected void setContentId(String contentID, String type) {
        this.type = type;
        this.contentID = contentID;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == ACCOUNT_MANAGER_PERMISSION) {
            accountManagerPermission = new String[]{""};
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED &&
                        permissions[i].equalsIgnoreCase(Manifest.permission.GET_ACCOUNTS)) {
                    accountManagerPermission[i] = permissions[i];
                }
            }

            //If permissions denied
            if (!accountManagerPermission[0].isEmpty()) {
                accountDialogShown = true;
                accountManagerDialog();
            } else {
                accountDialogShown = false;
                sendTwitter();//null, null);
            }
        }

        if (requestCode == Constants.OPTIONAL_PERMISSION) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED &&
                        (permissions[i].equalsIgnoreCase(Manifest.permission.WRITE_EXTERNAL_STORAGE) || permissions[i].equalsIgnoreCase(Manifest.permission.READ_EXTERNAL_STORAGE))) {
                    SharedPreferencesManager.getEditor().putInt(Constants.SAVE_SETTING, Constants.SAVE_INTERNAL).apply();
                    return;
                }
            }
//            if (mIsFacebookClicked) {
//                this.facebookShare.callOnClick();
//            }
//            else {
                if (((ShareDialogInstagram) this).getIsInstagramClicked()) {
                    ((ShareDialogInstagram) this).callInstagramClicked();
                }
                else{
                    switch (chosenPlatform){
                        case INSTAGRAM :
                            mInstagramItem.callOnClick();
                        case SNAPCHAT:
                            snapchatShare.callOnClick();
                            break;
                        case MUSICALLY:
                            musicalyShare.callOnClick();
                            break;
                        case MORE:
                            moreChooserShare.callOnClick();
                            break;
                        case SAVE_TO_GALLERY:
                            saveToCameraRollShare.callOnClick();
                            break;
                    }
                }
            }
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_CANCELED) {
            onShareProccessEnd();
            return;
        }

        if (resultCode == Activity.RESULT_OK) {
            if (mFacebookCallbackManager != null) {
                lateDoneLoading();
                mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
            }
            else if (requestCode == TWITTER_REQUEST_CODE) {
                    lateDoneLoading();
                    addStateMachinShareEvent(chosenPlatform);
            }
            else if (requestCode == SHARE_INTENT){
                    onShareProccessEnd();
            }
        }
    }
    /**
     * isFFmpegIsDoneProcessing
     *
     * @param fileName
     * @return true if the file exsists and the processing is done - this is good for uploadig video to facebook
     */
    protected boolean isFileExsist(String fileName) {
        return Constants.isFFmpegIsDoneProcessing && new File(fileName).exists();
    }

    @Override
    public void onWorkerFinished(boolean result, String filePath) {
        dismissLoadingDialog();
        if (result) {
            this.mFilePath = filePath;
            if (mFilePath != null) {
                File f = new File(mFilePath);
                Uri contentUri = Uri.fromFile(f);
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
                mContext.sendBroadcast(mediaScanIntent);
            }

            switch (chosenPlatform) {
                case INSTAGRAM:
                    onInstagramClicked();
                    break;
                case SNAPCHAT:
                    sendSnapchat();
                    break;
                case MUSICALLY:
                    sendMusicaly();
                    break;
                case MORE:
                    openChooser();
                    break;
                case SAVE_TO_GALLERY:
                    showSaveToGallery();
                    break;
                default:
                    break;
            }

        } else {
            isInstagramClicked = false;
        }
    }

    protected String getNameByActivity() {
        if (mContext instanceof HomeScreenActivity)
            return "home_screen";
        else if (mContext instanceof VideoHomeActivity)
            return "video_home";
        else if (mContext instanceof VideoPlayerActivity)
            return "uploading";
        else return "share";
    }
}