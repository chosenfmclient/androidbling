package fm.bling.blingy.inviteFriends.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Chosen-pro on 11/8/15.
 */
public class CAAEmailInvites
{
    @SerializedName("emails")
    private ArrayList<CAAEmailInvite> emails;
    @SerializedName("name")
    private String name;

    public ArrayList<CAAEmailInvite> getEmails() {
        return emails;
    }

    public void setEmails(ArrayList<CAAEmailInvite> emails) {
        this.emails = emails;
    }

    public CAAEmailInvites(ArrayList<CAAEmailInvite> emails, String name)
    {
        this.emails = emails;
        this.name   = name;
    }

}
