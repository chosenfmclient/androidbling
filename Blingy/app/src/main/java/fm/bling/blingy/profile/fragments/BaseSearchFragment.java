package fm.bling.blingy.profile.fragments;

import android.support.v4.app.Fragment;

import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.tracking.EventConstants;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/30/16.
 * History:
 * ***********************************
 */
public class BaseSearchFragment extends Fragment {

    public void onSearch(String text) {
    }

    public int updatedFollowing(int add) {
        int following = SharedPreferencesManager.getInstance().getInt(EventConstants.FOLLOWING_COUNT, 1) + add;
        SharedPreferencesManager.getEditor().putInt(EventConstants.FOLLOWING_COUNT, following).commit();
        return following;
    }

}
