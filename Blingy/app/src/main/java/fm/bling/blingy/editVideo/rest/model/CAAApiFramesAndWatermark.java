package fm.bling.blingy.editVideo.rest.model;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import fm.bling.blingy.App;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 3/28/16.
 * History:
 * ***********************************
 */
public class CAAApiFramesAndWatermark
{
    @SerializedName("data")
    private ArrayList<FrameData> framesData;

    public String watermarkPath;

    private ArrayList<FrameDataInternal>frames;

    public ArrayList<FrameDataInternal> getFramesData()
    {
        return frames;
    }

    public class FrameData
    {
        @SerializedName("frame_id")
        private int frameId;

        @SerializedName("name")
        private String name;

        @SerializedName("texture")
        private String textureUrl;

        @SerializedName("frames")
        private JsonElement jsonElement;

        @SerializedName("watermark")
        private String watermarkUrl;

    }

    public void setFramesAndWaterMarkCashing(android.content.Context context)
    {
        FrameData frameData;

        File framesDir = new File(App.VIDEO_WORKING_FOLDER, "/frames");
        if(!framesDir.exists())
            framesDir.mkdirs();

        File watermarkDir = new File(App.VIDEO_WORKING_FOLDER, "/watermark");
        if(!watermarkDir.exists())
            watermarkDir.mkdirs();

        frames = new ArrayList<>();

        FrameDataInternal blankFrame = new FrameDataInternal();
        frames.add(blankFrame);
        blankFrame.name = "None";

        for(int i=0; i < framesData.size(); i++)
        {
            frameData = framesData.get(i);

            if(i==0)
            {
                try
                {
                    if (frameData.watermarkUrl == null)
                        continue;

                    File watermarkFile = new File(watermarkDir, frameData.watermarkUrl.hashCode()+"");
                    watermarkPath = watermarkFile.getPath();

                    if (!watermarkFile.exists())
                    {
                        File[] files = watermarkDir.listFiles();
                        for (int j = 0; j < files.length; j++)
                        {
                            files[j].delete();
                        }

                        getFile(frameData.watermarkUrl, watermarkPath);
                    }
                }
                catch (Throwable err)
                {
                    err.printStackTrace();
                }
            }
            else
            {
                try
                {
                    FrameDataInternal frameDataInternal = new FrameDataInternal();
                    frameDataInternal.frameId = frameData.frameId+"";
                    frameDataInternal.name = frameData.name;

                    String url = frameData.textureUrl;
                    File frame = new File(framesDir, url.hashCode() + "");
                    frameDataInternal.urlTexture = frame.getPath();
                    if (!frame.exists()) getFile(url, frameDataInternal.urlTexture);

                    JsonObject jsonObject = frameData.jsonElement.getAsJsonObject();

                    url = jsonObject.get("1x1").getAsString();
                    frame = new File(framesDir, url.hashCode() + "");
                    frameDataInternal.url1x1 = frame.getPath();
                    if (!frame.exists()) getFile(url, frameDataInternal.url1x1);

                    url = jsonObject.get("3x4").getAsString();
                    frame = new File(framesDir, url.hashCode() + "");
                    frameDataInternal.url3x4 = frame.getPath();
                    if (!frame.exists()) getFile(url, frameDataInternal.url3x4);

                    url = jsonObject.get("4x3").getAsString();
                    frame = new File(framesDir, url.hashCode() + "");
                    frameDataInternal.url4x3 = frame.getPath();
                    if (!frame.exists()) getFile(url, frameDataInternal.url4x3);

                    url = jsonObject.get("9x16").getAsString();
                    frame = new File(framesDir, url.hashCode() + "");
                    frameDataInternal.url9x16 = frame.getPath();
                    if (!frame.exists()) getFile(url, frameDataInternal.url9x16);

                    url = jsonObject.get("10x16").getAsString();
                    frame = new File(framesDir, url.hashCode() + "");
                    frameDataInternal.url10x16 = frame.getPath();
                    if (!frame.exists()) getFile(url, frameDataInternal.url10x16);

                    url = jsonObject.get("16x9").getAsString();
                    frame = new File(framesDir, url.hashCode() + "");
                    frameDataInternal.url16x9 = frame.getPath();
                    if (!frame.exists()) getFile(url, frameDataInternal.url16x9);

                    url = jsonObject.get("16x10").getAsString();
                    frame = new File(framesDir, url.hashCode() + "");
                    frameDataInternal.url16x10 = frame.getPath();
                    if (!frame.exists()) getFile(url, frameDataInternal.url16x10);

                    frames.add(frameDataInternal);

                }
                catch(Throwable err)
                {
                    err.printStackTrace();
                }
            }
        }
        framesData.clear();
        framesData = null;
    }

    public class FrameDataInternal
    {
        public String url1x1, url3x4, url4x3, url9x16, url10x16, url16x9, url16x10;
        public String name;
        public String urlTexture;
        public String frameId;
    }

    private void getFile(String sUrl, String outputPath) throws Throwable
    {
        URL url = new URL(sUrl);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setConnectTimeout(20000);
        conn.setReadTimeout(20000);
        conn.setInstanceFollowRedirects(true);
        InputStream is = conn.getInputStream();

        FileOutputStream fos = new FileOutputStream(outputPath);

        byte[]buffer = new byte[1024 * 10];
        int count;

        while(true)
        {
            count = is.read(buffer);
            if(count == -1)
                break;
            fos.write(buffer, 0, count);
        }
        fos.flush();
        fos.close();
        is.close();
    }

}