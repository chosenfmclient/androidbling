package fm.bling.blingy.rest.model;

/**
 * Created by Zach on 7/7/15.
 */
public class CAAExperienceOverlay {
    private String added;
    private String level;
    private String earned;
    private String requiredToLevelUp;

    public CAAExperienceOverlay(String added, String level, String earned, String requiredToLevelUp) {
        this.added = added;
        this.level = level;
        this.earned = earned;
        this.requiredToLevelUp = requiredToLevelUp;
    }

    public String getRequiredToLevelUp() {
        return requiredToLevelUp;
    }

    public void setRequiredToLevelUp(String requiredToLevelUp) {
        this.requiredToLevelUp = requiredToLevelUp;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getEarned() {
        return earned;
    }

    public void setEarned(String earned) {
        this.earned = earned;
    }
}
