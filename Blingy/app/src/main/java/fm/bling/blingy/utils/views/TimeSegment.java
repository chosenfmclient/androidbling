package fm.bling.blingy.utils.views;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;

import fm.bling.blingy.App;
import fm.bling.blingy.R;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/15/16.
 * History:
 * ***********************************
 */
public class TimeSegment extends View {

    public TimeSegment(Context context, int xPos) {
        super(context);
        setLayoutParams(new FrameLayout.LayoutParams((int)(10f * App.SCALE_X), context.getResources().getDimensionPixelSize(R.dimen.padding_5)));
        setX(xPos);
        setBackgroundResource(R.color.white);
    }
}
