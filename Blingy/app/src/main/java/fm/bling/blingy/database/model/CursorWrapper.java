package fm.bling.blingy.database.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 14/02/17.
 * History:
 * ***********************************
 */
public class CursorWrapper {

    private String mTableName;
    private String mWhereClause;
    private String mSQLQuery;

    private List<String> lstColumns;
    private boolean isAllCols = true;

    public CursorWrapper(String tableName) {
        this.setTableName(tableName);
        this.lstColumns = new ArrayList<>();
    }

    public String[] getColumns() {

        if (lstColumns.size() == 0)
            return new String[0];

        Object[] objColumns = lstColumns.toArray();
        String[] arrColumns = Arrays.copyOf(objColumns, objColumns.length, String[].class);

        return arrColumns;
    }

    public boolean isAllCols(){
        return isAllCols;
    }

    public CursorWrapper addColumn(String val) {
        isAllCols = false;
        lstColumns.add(val);
        return this;
    }

    public CursorWrapper addAllCols(String val) {
        lstColumns.add(val);
        return this;
    }

    public String getTableName() {
        return mTableName;
    }

    public CursorWrapper setTableName(String tableName) {
        this.mTableName = tableName;
        return this;
    }

    public String getWhereClause() {
        return mWhereClause;
    }

    public CursorWrapper setWhereClause(String mWhereClause) {
        this.mWhereClause = mWhereClause;
        return this;
    }

    public String getSQLQuery() {
        return mSQLQuery;
    }

    public CursorWrapper setmQLQuery(String mSQLQuery) {
        this.mSQLQuery = mSQLQuery;
        return this;
    }
}
