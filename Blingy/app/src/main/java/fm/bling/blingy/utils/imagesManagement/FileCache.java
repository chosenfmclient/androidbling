package fm.bling.blingy.utils.imagesManagement;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Ben levi on 1/29/2016.
 */
public class FileCache
{
    private File cacheDir;

    public FileCache(Context context)
    {
        cacheDir = context.getDir("Blingy", Context.MODE_PRIVATE);//new File(android.os.Environment.getExternalStorageDirectory(),"/Chosen/");
        if(cacheDir == null)
        {
            cacheDir = context.getCacheDir();
        }
        if(!cacheDir.exists())
        {
            cacheDir.mkdirs();
        }
    }

    public Bitmap getFile(String url, int width)
    {
        //Identify images by hashcode
        File imageFile = new File(cacheDir,  String.valueOf(url.hashCode()) + width);
        if(imageFile.exists())
            return decodeFile(imageFile);

        return null;
    }

    private Bitmap decodeFile(File f)
    {
        try
        {
            FileInputStream fileInputStream = new FileInputStream(f);
            Bitmap bitmap = BitmapFactory.decodeStream(fileInputStream, null, null);
            fileInputStream.close();
            return bitmap;
        }
        catch (FileNotFoundException e){}
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public void clear()
    {
        File[] files=cacheDir.listFiles();
        if(files==null)return;
        for(File f:files)
            f.delete();
    }

    public File getFileForIO(String url, int width)
    {
        return new File(cacheDir,  String.valueOf(url.hashCode()) + width );
    }

    public long getFolderSize()
    {
        return getFolderSizeInternal(cacheDir);
    }

    public long getFolderSizeInternal(File dirToCheck)
    {
        long folderSize = 0;
        File[] files = dirToCheck.listFiles();
        for(int i=0; i<files.length; i++)
        {
            if(files[i].isDirectory())
                folderSize  += getFolderSizeInternal(files[i]);
            else folderSize += files[i].length();
        }
        return folderSize;
    }

}