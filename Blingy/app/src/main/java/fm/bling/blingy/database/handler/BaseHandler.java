package fm.bling.blingy.database.handler;

import android.support.annotation.NonNull;

import java.util.ArrayList;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 12/02/17.
 * History:
 * ***********************************
 */
public abstract class BaseHandler<T> {

    /**
     * Add an object to the db.
     * @param object - the object to add.
     * @return the row ID of the newly inserted row, or -1 if an error occurred
     */
    public abstract long add(@NonNull T object);

    public abstract long addMany(@NonNull ArrayList<T> object);

    public abstract boolean remove(@NonNull T object);

    public abstract boolean update(@NonNull T object);

    public abstract T get(@NonNull T object);

    public abstract ArrayList<T> getAll(String filter);
}
