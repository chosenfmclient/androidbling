package fm.bling.blingy.leaderboard.presenter;

import android.support.annotation.Nullable;

import java.util.ArrayList;

import fm.bling.blingy.leaderboard.LeaderboardMVPR;
import fm.bling.blingy.leaderboard.model.LeaderboardModel;
import fm.bling.blingy.rest.model.User;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 02/03/17.
 * History:
 * ***********************************
 */
public class LeaderboardPresenter implements LeaderboardMVPR.Presenter {

    private LeaderboardMVPR.View mView;
    private LeaderboardMVPR.Model mModel;

    public LeaderboardPresenter(){
        this.mModel = new LeaderboardModel(this);
    }

    @Override
    public void setView(LeaderboardMVPR.View view) {
        this.mView = view;
    }

    @Override
    public void getLeaderBoard(int page) {
        mModel.getLeaderBoard(page);
    }

    @Override
    public void loadLeaderboardData(ArrayList<User> users) {
        mView.showLeaderboardData(users);
    }

    @Override
    public void loadNextPage(@Nullable ArrayList<User> users) {
        mView.loadNextPage(users);
    }
}
