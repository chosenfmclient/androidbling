package fm.bling.blingy.widget.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.Gravity;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
//import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.stateMachine.StateMachineMessageListener;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;
import fm.bling.blingy.widget.CAAWidgetSettingsSingleton;
//import fm.bling.blingy.widget.PointsObserver;
//import fm.bling.blingy.widget.views.PointsView;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 7/11/16.
 * History:
 * ***********************************
 */
public class InviteWidgetBaseDialog extends Dialog /* implements PointsObserver */{

    protected BaseActivity mContext;
    protected StateMachineEvent.MESSAGE_TYPE messageType;
    private boolean dismissed;
//    private boolean isPointsTutorialVisible = false;
    protected int worthPoints  = 0;
    protected String myPoints  = "0";
    protected LinearLayout dialogLayout;
//    protected LinearLayout mPointsLayout;
//    protected PointsView mPointsTutorial;
//    protected TextView mPoints;
    protected TextView mMainText;
    protected FrameLayout mButtonFrame;
    protected TextView mButtonText;
    protected ImageView mButtonSocialPic;
    protected URLImageView mButtonUserPic;
    protected TextView mSkip;
    protected TextView mBack;
    protected ImageLoaderManager mImageLoaderManager;
    protected AlphaAnimation fadeOut = new AlphaAnimation(1f, 0f);
    protected AlphaAnimation fadeIn = new AlphaAnimation(0f, 1f);
    protected StateMachineMessageListener onReadyListener;
    protected LinearLayout widgetContainer;

    public InviteWidgetBaseDialog(StateMachineMessageListener onReadyListener, BaseActivity context, StateMachineEvent.MESSAGE_TYPE messageType) {
        super(context, android.R.style.Theme_Panel);
        this.mContext = context;
        this.messageType = messageType;
        this.mImageLoaderManager = context.getImageLoaderManager();
        this.onReadyListener = onReadyListener;
//        CAAWidgetSettingsSingleton.getInstance().attachPointsObserver(this);

        this.worthPoints  = CAAWidgetSettingsSingleton.getInstance().getInviteWorth();
        this.myPoints     = String.valueOf(CAAWidgetSettingsSingleton.getInstance().getMyPoints());

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dialogLayout     = (LinearLayout) layoutInflater.inflate(R.layout.invite_widget, null);
        widgetContainer = (LinearLayout) dialogLayout.findViewById(R.id.widget_container);
        mMainText        = (TextView)dialogLayout.findViewById(R.id.main_text);
//        mPoints          = (TextView)dialogLayout.findViewById(R.id.points);
//        mPointsLayout    = (LinearLayout)dialogLayout.findViewById(R.id.total_points);
        mButtonFrame     = (FrameLayout) dialogLayout.findViewById(R.id.main_button);
        mButtonText      = (TextView)dialogLayout.findViewById(R.id.button_text);
        mButtonUserPic   = (URLImageView) dialogLayout.findViewById(R.id.button_pic);
        mButtonSocialPic = (ImageView) dialogLayout.findViewById(R.id.social_pic);
        mSkip            = (TextView) dialogLayout.findViewById(R.id.skip);
        mBack            = (TextView) dialogLayout.findViewById(R.id.back);

//        mPointsTutorial = new PointsView(mContext, false, false);
//        mPointsTutorial.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//        mPointsTutorial.setVisibility(View.INVISIBLE);
//        dialogLayout.addView(mPointsTutorial, 0);

        mButtonUserPic.setIsRoundedImage(true);
        mButtonUserPic.setAnimResID(R.anim.fade_in);
        mButtonUserPic.setKeepAspectRatioAccordingToWidth(true);
        mButtonUserPic.setImageLoader(mImageLoaderManager);

//        mPoints.setText(myPoints);
//        mPointsLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                TrackingManager.getInstance().tapPoints("open");
//                UserProgressMapDialog dialog = new UserProgressMapDialog(mContext, mImageLoaderManager);
//                dialog.show();
//            }
//        });
        setContentView(dialogLayout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(context.getResources().getColor(R.color.main_blue));
            getWindow().setNavigationBarColor(Color.BLACK);
        }

//        mPointsView.setVisibility();
    }

//    private void showPointsDescription() {
//        isPointsTutorialVisible = true;
//            mPointsTutorial.removeAllLines();
//
//        mPointsTutorial.setVisibility(View.INVISIBLE);
//
//        ArrayList<CAAInvitePrize> prizes = CAAWidgetSettingsSingleton.getInstance().getPrizes();
//        if(prizes.size() > 0) {
//            for (CAAInvitePrize prize : prizes) {
//                mPointsTutorial.addLineView(true, false, prize.getPoints() + " = " + prize.getText());
//            }
//            mPointsTutorial.addLineView(false, false ,mContext.getString(R.string.prizes_redeem));
//        }
//        mPointsTutorial.setVisibility(View.VISIBLE);
//
//    }

//    private void closePointsDescription() {
//        isPointsTutorialVisible = false;
//        mPointsTutorial.setVisibility(View.INVISIBLE);
//    }

    public boolean onTouchEvent(MotionEvent me)
    {
        if (!dismissed)
        {
            if (me.getAction() == MotionEvent.ACTION_DOWN)
            {
//                int topLocation = mPointsTutorial.getHeightInternal();
//                if(topLocation == 0)
//                {
//                    int[] loc;
//                    widgetContainer.getLocationOnScreen(loc = new int[2]);
//                    topLocation = loc[1];
//                }
//                if(me.getRawY() < topLocation)
                   dismiss();
            }
        }
        return true;
    }

    @Override
    public void show()
    {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        Animation slideIn = AnimationUtils.loadAnimation(mContext, R.anim.slide_up);
        slideIn.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                dialogLayout.setVisibility(View.VISIBLE);
                hideNavBar();
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }
        });

        super.show();
        dialogLayout.startAnimation(slideIn);
    }

    public void dismiss()
    {
        if (dismissed)
            return;
        mContext.setIsWidgetVisible(false);
//        CAAWidgetSettingsSingleton.getInstance().removeObserver(this);
        dismissed = true;
        Animation slideOut = AnimationUtils.loadAnimation(mContext, R.anim.slide_down);
        slideOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                dialogLayout.setVisibility(View.INVISIBLE);
                showNavBar();
                superDismiss();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        dialogLayout.startAnimation(slideOut);
    }

    private void superDismiss() {
        super.dismiss();
    }


    @Override
    public void onStop() {
        super.onStop();

        if(fadeIn !=null)
            fadeIn = null;
        if(fadeOut != null)
            fadeOut = null;

        mButtonUserPic.close();
        mButtonUserPic = null;

        dialogLayout.removeAllViews();
        dialogLayout.destroyDrawingCache();
        dialogLayout = null;


    }

    public void reShowDialog() {
        if (dialogLayout.getVisibility() == View.VISIBLE) {
            return;
        }
        Animation slideIn = AnimationUtils.loadAnimation(mContext, R.anim.slide_up);
        slideIn.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                dialogLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }
        });
        dialogLayout.startAnimation(slideIn);
    }

    public void hideDialog() {
        if (dialogLayout.getVisibility() != View.VISIBLE) {
            return;
        }
        Animation slideOut = AnimationUtils.loadAnimation(mContext, R.anim.slide_down);
        slideOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                dialogLayout.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        dialogLayout.startAnimation(slideOut);
    }

//    protected void showNexPointsPopUp(String text){
//        mPointsTutorial.removeAllLines();
//        mPointsTutorial.addBigLineView(mContext, "+" + worthPoints + text);
//        mPointsTutorial.setVisibility(View.VISIBLE);
//
//        new Thread(){
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(2000);
//                    mContext.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Animation fadeOut = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
//                            fadeOut.setFillAfter(false);
//                            fadeOut.setAnimationListener(new Animation.AnimationListener() {
//                                @Override
//                                public void onAnimationStart(Animation animation) {
//
//                                }
//
//                                @Override
//                                public void onAnimationEnd(Animation animation) {
//                                    mPointsTutorial.setVisibility(View.INVISIBLE);
//                                }
//
//                                @Override
//                                public void onAnimationRepeat(Animation animation) {
//
//                                }
//                            });
//                            mPointsTutorial.startAnimation(fadeOut);
//                        }
//                    });
//
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }.start();
//
//    }

    private void hideNavBar()
    {
        boolean hasMenuKey = ViewConfiguration.get(mContext.getApplicationContext()).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        if (!hasMenuKey && !hasBackKey)
        {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(
                              View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private void showNavBar(){

        boolean hasMenuKey = ViewConfiguration.get(mContext.getApplicationContext()).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        if (!hasMenuKey && !hasBackKey)
        {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

    }

//    @Override
//    public void updatePoints() {
//        mContext.addPointsInvite(mPoints);
//    }

}
