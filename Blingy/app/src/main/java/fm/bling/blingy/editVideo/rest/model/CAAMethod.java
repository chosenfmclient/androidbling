package fm.bling.blingy.editVideo.rest.model;

import com.google.gson.annotations.SerializedName;

import java.net.URL;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAMethod
 * Description:
 * Created by Dawidowicz Nadav on 5/18/15.
 * History:
 * ***********************************
 */
public class CAAMethod {
    @SerializedName("method")
    private String method;

    @SerializedName("content_type")
    private String contentType;

    @SerializedName("signed_url")
    private URL signedUrl;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public URL getSignedUrl() {
        return signedUrl;
    }

    public void setSignedUrl(URL signedUrl) {
        this.signedUrl = signedUrl;
    }
}
