package fm.bling.blingy.singletones;

import android.content.Context;
import android.os.SystemClock;

import com.amplitude.api.Amplitude;
import com.appsee.Appsee;
import com.facebook.AccessToken;
import com.flurry.android.FlurryAgent;

import java.io.DataInputStream;
import java.io.DataOutputStream;

import fm.bling.blingy.registration.rest.model.CAAToken;
import fm.bling.blingy.utils.Constants;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAUserDataSingleton
 * Description:
 * Created by Dawidowicz Nadav on 5/12/15.
 * History:
 * ***********************************
 */
public class CAAUserDataSingleton {

    private static CAAUserDataSingleton ourInstance;
    private String accessToken;
    private long accessExpireTime;
    private String email;
    private String photoUrl;
    private String password;
    private String appVersion;
    private String deviceIdentifier;
    private String accountType;

    private String firstName;
    private String lastName;
    private String userId;
    private String facebookId;
    private String totalFollowing;

    private String facebookLink;
    private String twitterLink;
    private String instagramLink;
    private String youtubeLink;

    private boolean sentGcmToken;
    private Boolean nativeShareSetting;

    public static CAAUserDataSingleton getInstance() {
        if (ourInstance == null) {
            synchronized (CAAUserDataSingleton.class) {
                if (ourInstance == null) {
                    ourInstance = new CAAUserDataSingleton();
                }
            }
        }
        return ourInstance;
    }

    private CAAUserDataSingleton() {
        accessToken = SharedPreferencesManager.getInstance().getString(Constants.ACCESS_TOKEN, "");
        accountType = SharedPreferencesManager.getInstance().getString(Constants.ACCOUNT_TYPE, "");
        accessExpireTime = SharedPreferencesManager.getInstance().getLong(Constants.ACCESS_EXPIRE_TIME, -1);
        email = "";
        photoUrl = "";
        password = "";
        deviceIdentifier = "";
        firstName = "";
        lastName = "";
        userId = "";
        facebookId = "";
        sentGcmToken = false;
        appVersion = "";
        nativeShareSetting = null;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getInstagramLink() {
        return instagramLink;
    }

    public void setInstagramLink(String instagramLink) {
        this.instagramLink = instagramLink;
    }

    public String getYoutubeLink() {
        return youtubeLink;
    }

    public void setYoutubeLink(String youtubeLink) {
        this.youtubeLink = youtubeLink;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Boolean getNativeShareSetting() {
        return nativeShareSetting;
    }

    public void setNativeShareSetting(Boolean nativeShareSetting) {
        this.nativeShareSetting = nativeShareSetting;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getDeviceIdentifier() {
        return deviceIdentifier;
    }

    public void setDeviceIdentifier(String deviceIdentifier) {
        this.deviceIdentifier = deviceIdentifier;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserId()
    {
        if(userId == null)
        {
            userId = SharedPreferencesManager.getInstance().getString("user_id", "");
        }
        return userId;
    }

    public long getAccessExpireTime() {
        return accessExpireTime;
    }

    public void setAccessExpireTime(String accessExpireTime) {
        this.accessExpireTime = SystemClock.elapsedRealtime() + (Long.parseLong(accessExpireTime) * 1000);
        SharedPreferencesManager.getEditor().putLong(Constants.ACCESS_EXPIRE_TIME, this.accessExpireTime).commit();
    }

    public void setUserId(String userId) {
        this.userId = userId;
        SharedPreferencesManager.getEditor().putString("user_id", userId).commit();
        Appsee.setUserId(userId);
        Amplitude.getInstance().setUserId(userId);
        FlurryAgent.setUserId(userId);
    }

    public void setSentGcmToken(boolean sentGcmToken) {
        this.sentGcmToken = sentGcmToken;
    }

    public boolean getSentGcmToken() {
        return sentGcmToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        SharedPreferencesManager.getEditor().putString(Constants.ACCESS_TOKEN, accessToken).commit();
        this.accessToken = accessToken;
    }

    public String getTotalFollowing() {
        return totalFollowing;
    }

    public void setTotalFollowing(String totalFollowing) {
        this.totalFollowing = totalFollowing;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public void saveName(Context context) {
        try {
            DataOutputStream dos = new DataOutputStream(context.openFileOutput("user.name", Context.MODE_PRIVATE));
            dos.writeUTF(firstName);
            dos.flush();
            dos.close();
        } catch (Throwable xz) {
        }
    }

    public String getName(Context context) {
        try {
            DataInputStream dis = new DataInputStream(context.openFileInput("user.name"));
            String name = dis.readUTF();
            dis.close();
            return name;
        } catch (Throwable xz) {

        }
        return "";
    }

    public boolean isAccessTokenExpired() {
        try {
            if (getAccountType() == null || getAccessToken() == null || getAccountType().isEmpty() || getAccessToken().isEmpty())
                return false;

            if (getAccountType().equals(Constants.FACEBOOK_ACC))
                return AccessToken.getCurrentAccessToken() != null && AccessToken.getCurrentAccessToken().isExpired();
            else
                return (getAccessExpireTime() != -1 && getAccessExpireTime() < SystemClock.elapsedRealtime());
        } catch (Throwable throwable) {
            return true;
        }
    }

    public boolean isMe(String user_id)
    {
        return user_id.equalsIgnoreCase(userId) || user_id.equalsIgnoreCase(Constants.ME);
    }

    public void setAccessToken(CAAToken caaToken) {
        if (caaToken != null) {
            setAccessToken(caaToken.getAccessToken());
            setAccessExpireTime(caaToken.getExpiresIn());
        }
    }
}
