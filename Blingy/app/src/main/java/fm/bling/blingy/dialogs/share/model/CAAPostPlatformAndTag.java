package fm.bling.blingy.dialogs.share.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 4/4/16.
 * History:
 * ***********************************
 */
public class CAAPostPlatformAndTag
{
    @SerializedName("tag")
    private String tag;

    @SerializedName("platform")
    private String platform;

    public CAAPostPlatformAndTag(String tag, String platform)
    {
        this.tag = tag;
        this.platform = platform;
    }

}