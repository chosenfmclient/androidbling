package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAType
 * Description:
 * Created by Dawidowicz Nadav on 7/2/15.
 * History:
 * ***********************************
 */
public class CAALike{
    @SerializedName("type")
    private String type;

    @SerializedName("video_id")
    private String videoId;

    private int isLiked = 0;

    public CAALike(String type) {
        this.type = type;
    }

    public CAALike(String type, String videoId, int isLiked) {
        this.type = type;
        this.videoId = videoId;
        this.isLiked = isLiked;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVideoId() {
        return videoId;
    }

    public int getIsLiked()  {
        return isLiked;
    }

    public void setIsLiked(int isLiked) {
        this.isLiked = isLiked;
    }
}
