//package fm.bling.blingy.myVideos;
//
//import android.animation.ObjectAnimator;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.widget.Toolbar;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.animation.Animation;
//import android.view.animation.LinearInterpolator;
//import android.widget.AbsListView;
//import android.widget.AdapterView;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import com.flurry.android.FlurryAgent;
//import com.google.android.gms.common.api.GoogleApiClient;
//
//import java.net.HttpURLConnection;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import fm.bling.blingy.App;
//import fm.bling.blingy.BaseActivity;
//import fm.bling.blingy.R;
//import fm.bling.blingy.myVideos.adapters.MyPerformancesAdapter;
//import fm.bling.blingy.myVideos.dialogs.FilterDialog;
//import fm.bling.blingy.myVideos.model.VideosFilter;
//import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
//import fm.bling.blingy.record.CreateVideoActivity;
//import fm.bling.blingy.record.Utils;
//import fm.bling.blingy.singletones.SharedPreferencesManager;
//import fm.bling.blingy.videoHome.VideoHomeActivity;
//import fm.bling.blingy.tracking.EventConstants;
//import fm.bling.blingy.tracking.TrackingManager;
//import fm.bling.blingy.utils.Constants;
////import fm.bling.blingy.videoHome.VideoHomeActivity2;
//import fm.bling.blingy.videoHome.model.CAAVideo;
//import fm.bling.blingy.videoHome.model.CAAVideoList;
//import retrofit.Callback;
//import retrofit.RetrofitError;
//import retrofit.client.Response;
//
//
//public class MyVideosActivity extends BaseActivity implements FilterDialog.FilterSelectedListener {
//    private boolean listInitialized = false;
//
//    private int previousLastItem;
//    private int pageNumber = 0;
//    //    private boolean switching;
//    //menu
//    private Menu myVideoMenu;
//
//    private final int ALL = 0;
//    private final int PUBLIC = 1;
//    private final int PRIVATE = 2;
//
//
//    private TextView showOffButton;
//    private TextView listHeader;
//    //    private TextView textViewPerf;
////    private TextView textViewReviews;
//    private ImageView spinner;
//    private FrameLayout loadingLayout;
//    private ListView list;
//    private FrameLayout listViewEmpty;
//    private ImageView imageEmpty;
//    private TextView textEmpty;
//
//    private int selectedFilter = 0;
//    private VideosFilter filterModel;
//    private VideosFilter filter = new VideosFilter("All", "Date Added");
//
//    private ObjectAnimator anim;
//
//    //    private List<CAAVideo> videoItems;
////    private boolean isBioTab = false;
////    private boolean isReviewTab = false;
//    private boolean isPerformanceTab = true;
//
//    private boolean calling = false;
//    private boolean endReached = false;
//
////    private String type = Constants.PERFORMANCES;
//
//    private MyPerformancesAdapter adapter;
//    private List<CAAVideo> mainItems = new ArrayList<>();
//    /**
//     * ATTENTION: This was auto-generated to implement the App Indexing API.
//     * See https://g.co/AppIndexing/AndroidStudio for more information.
//     */
//    private GoogleApiClient client;
////    private List<CAAVideo> currentPerf = new ArrayList<>();
////    private List<CAAVideo> currentRev = new ArrayList<>();
////    private List<CAAVideo> bioItems;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.my_videos);
//        init();
//
//        //Setting up the navigation icon.
//        Toolbar toolbar = getActionBarToolbar();
//        toolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
//        toolbar.setNavigationContentDescription("backClose");
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
////        setUpPerformanceButton();
//        showOffButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(MyVideosActivity.this, CreateVideoActivity.class);
//                getRecordPermissions(i);
//            }
//        });
//        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                if (Utils.canClick()) {
//                    CAAVideo video = mainItems.get(position);
//                    Intent i = new Intent(MyVideosActivity.this, VideoHomeActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putParcelable(Constants.CAAVIDEO, video);
//                    i.putExtra(Constants.BUNDLE, bundle);
////                    i.putExtra(Constants.VIDEO_TYPE_EXTRA, video.getType());
////                    i.putExtra(Constants.USER_ID, video.getUser().getUserId());
////                    i.putExtra(Constants.VIDEO_ID, video_id);
//                    startActivity(i);
//                }
//            }
//        });
//
////        textViewPerf.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                FlurryAgent.logEvent(FlurryConstants.MY_VIDEOS_TAP_PERFORMANCE_TAB_FLURRY);
////                if (!isPerformanceTab) {
////                    lockButtons();
//////                    isBioTab = false;
//////                    isReviewTab = false;
////                    isPerformanceTab = true;
//////                    type = Constants.PERFORMANCES;
////                    setUpPerformanceButton();
////                    myVideoMenu.findItem(R.id.action_filter).setVisible(true);
////                    getVideos();
////                }
////            }
////        }
//
////        );
////        textViewReviews.setOnClickListener(new View.OnClickListener()
////           {
////               @Override
////               public void onClick(View v) {
////                   if (!isReviewTab) {
////                       lockButtons();
////                       isBioTab = false;
////                       isReviewTab = true;
////                       isPerformanceTab = false;
////                       type = Constants.RESPONSES;
////                       setUpReviewsButton();
////                       myVideoMenu.findItem(R.id.action_filter).setVisible(true);
////                       getVideos();
////                   }
////               }
////           }
////
////        );
//        list.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                if (listInitialized) {
//                    final int lastItem = firstVisibleItem + visibleItemCount;
//                    if (lastItem >= totalItemCount - 4 && !calling && !endReached) {
//                        if (previousLastItem != lastItem) { //to avoid multiple calls for last item
//                            previousLastItem = lastItem;
//                            getNextPage();
//                        }
//                    }
//                }
//            }
//        });
//        String filterType = SharedPreferencesManager.getInstance(getApplicationContext()).getString(Constants.KEY_FILTER, "all");
//        String sortType = SharedPreferencesManager.getInstance(getApplicationContext()).getString(Constants.KEY_SORT, "date added");
//        // init default lest selection
//        this.filterModel = new VideosFilter(filterType, sortType);
//    }
//
//    private void init() {
//        showOffButton = (TextView) findViewById(R.id.myVideos_button_record);
//        listHeader = (TextView) findViewById(R.id.text_view_list_header);
////     textViewPerf = (TextView)findViewById(R.id.myVideos_button_performances);
////     textViewReviews = (TextView)findViewById(R.id.myVideos_button_reviews);
//        spinner = (ImageView) findViewById(R.id.spinner);
//        loadingLayout = (FrameLayout) findViewById(R.id.loading_layout);
//        list = (ListView) findViewById(R.id.testing_list);
//        listViewEmpty = (FrameLayout) findViewById(R.id.empty_view);
//        imageEmpty = (ImageView) findViewById(R.id.image_view_empty);
//        textEmpty = (TextView) findViewById(R.id.text_view_empty);
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_my_videos, menu);
//        myVideoMenu = menu;
////        mFilterAll = myVideoMenu.findItem(R.id.all_filter);
////        mFilterPublic = myVideoMenu.findItem(R.id.public_filter);
////        mFilterPrivate = myVideoMenu.findItem(R.id.private_filter);
////        mSortDate = myVideoMenu.findItem(R.id.date_sort);
////        mSortMovstViewed = myVideoMenu.findItem(R.id.moset_viewed_sort);
////        mSortRank = myVideoMenu.findItem(R.id.rank_sort);
//
////        switch (filterModel.getFilterType()){
////            case "all":
////                mFilterAll.setChecked(true);
////                break;
////            case "public":
////                mFilterPublic.setChecked(true);
////                break;
////            case "private":
////                mFilterPrivate.setChecked(true);
////                break;
////        }
////
////        switch (filterModel.getSortType()){
////            case "date added":
////                mSortDate.setChecked(true);
////                break;
////            case "most viewed":
////                mSortMovstViewed.setChecked(true);
////                break;
////            case "rank high to low":
////                mSortRank.setChecked(true);
////                break;
////        }
//
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle item selection
//        switch (item.getItemId()) {
//            case R.id.action_filter:
//                new FilterDialog(this, this, selectedFilter);
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
//
//
//    private void getNextPage() {
////        addFooter();
//        if (!calling) {
//            calling = true;
//            pageNumber++;
//            String sort = filter.getSortType().toLowerCase().replaceAll(" ", "_");
//            String status = filter.getFilterType().toLowerCase().replaceAll(" ", "_");
////            if (sort.isEmpty() || type.equals(Constants.BIOS)) {
////                sort = "date_added";
////            }
////            if (status.isEmpty() || type.equals(Constants.BIOS)) {
////                status = "all";
////            }
//            App.getUrlService(this).getSpecificUserVideosPage(Constants.ME, Constants.PERFORMANCES, status, sort, pageNumber, new Callback<CAAVideoList>() {
//                @Override
//                public void success(CAAVideoList caaVideoList, Response response) {
//                    if (mainItems.size() > 0) {
//                        mainItems.remove(mainItems.size() - 1);
//                    }
//                    if (response.getStatus() != HttpURLConnection.HTTP_NO_CONTENT) {
//                        for (CAAVideo video : caaVideoList.getData()) {
//                            mainItems.add(video);
//                        }
//                        mainItems.add(new CAAVideo());
//                    } else {
//                        endReached = true;
//                    }
//                    calling = false;
//                    adapter.notifyDataSetChanged();
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                    calling = false;
//                }
//            });
//        }
//    }
//
//    private void setUpPerformanceButton() {
////        textViewPerf.setActivated(true);
////        textViewPerf.setEnabled(false);
//
////        textViewReviews.setEnabled(true);
////        textViewReviews.setActivated(false);
//    }
//
////    private void setUpReviewsButton() {
////        textViewPerf.setEnabled(true);
////        textViewPerf.setActivated(false);
////
////        textViewReviews.setEnabled(false);
////        textViewReviews.setActivated(true);
////    }
//
//    private void getVideos() {
////        if (!switching) {
//        if (NetworkConnectivityListener.isConnectedWithDialog(this)) {
//            listInitialized = false;
//            pageNumber = 0;
//            previousLastItem = 0;
//            endReached = false;
//            loadAnimation();
//            String sort = filter.getSortType().toLowerCase().replaceAll(" ", "_");
//            String status = filter.getFilterType().toLowerCase().replaceAll(" ", "_");
////                if (sort.isEmpty() || type.equals(Constants.BIOS)) {
////                    sort = "date_added";
////                }
////                if (status.isEmpty() || type.equals(Constants.BIOS)) {
////                    status = "all";
////                }
//            App.getUrlService(this).getSpecificUserVideos(Constants.ME, Constants.PERFORMANCES, status, sort, new Callback<CAAVideoList>() {
//                @Override
//                public void success(CAAVideoList caaVideoList, Response response) {
//                    mainItems.clear();
//                    if (response.getStatus() != HttpURLConnection.HTTP_NO_CONTENT) {
//                        mainItems.addAll(caaVideoList.getData());
//                        mainItems.add(new CAAVideo());
//                    }
//                    adapter = new MyPerformancesAdapter(MyVideosActivity.this, mainItems);
//                    list.setAdapter(adapter);
//                    listViewEmpty.setVisibility(View.GONE);
//
//                    if (adapter.isEmpty()) {
//                        endReached = true;
////                            if (type.equals(Constants.BIOS)) {
////                                displayEmptyBio();
////                            }
////                            else
//                        if (!filter.getFilterType().equalsIgnoreCase("all")) {
//                            displayEmptyFilter();
//                        } else
////                            if (type.equals(Constants.RESPONSES)) {
////                                displayEmptyReviews();
////                            }
////                            else
////                            if (type.equals(Constants.PERFORMANCES)) {
//                            displayEmptyPerformances();
////                            }
//                    } else {
//                        listInitialized = true;
//                    }
////                        unlockButtons();
//                    cancelAnimation();
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                    cancelAnimation();
//                }
//            });
//        }
//
//
//    }
//
////    public void lockButtons() {
////        textViewPerf.setClickable(false);
////        textViewReviews.setClickable(false);
////    }
//
////    public void unlockButtons() {
////        textViewPerf.setClickable(true);
////        textViewReviews.setClickable(true);
////    }
//
//    private void onReloadFilters(VideosFilter filterModel, Boolean reload) {
//        if (!filter.equals(filterModel) && reload) {
//            filter.setFilterType(filterModel.getFilterType());
//            filter.setSortType(filterModel.getSortType());
//            getVideos();
//            String sort = filter.getSortType();
//            String[] sortWords = sort.split(" ");
//            sort = "";
//            for (String word : sortWords) {
//                sort = sort + " " + Character.toUpperCase(word.charAt(0)) + word.substring(1);
//            }
//            String status = filter.getFilterType();
//            String[] statusWords = status.split(" ");
//            status = "";
//            for (String word : statusWords) {
//                status = status + " " + Character.toUpperCase(word.charAt(0)) + word.substring(1);
//            }
//            if (filter.getFilterType().equalsIgnoreCase("all")) {
//                listHeader.setText("Sorted by \"" + sort.substring(1) + "\"");
//            } else {
//                listHeader.setText("Sorted by \"" + sort.substring(1) + "\", Filtered by \"" + status.substring(1) + "\"");
//            }
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//        if (getApplicationContext() != null) {
//            SharedPreferencesManager.getEditor(getApplicationContext()).putString(Constants.KEY_SORT, null);
//            SharedPreferencesManager.getEditor(getApplicationContext()).putString(Constants.KEY_FILTER, null);
//        }
//        super.onDestroy();
//    }
//
//    public void cancelAnimation() {
//        anim.cancel();
//        loadingLayout.setVisibility(View.GONE);
////        if (!type.equals(Constants.BIOS)) {
////            listHeader.setVisibility(View.VISIBLE);
////        }
//    }
//
//    private void loadAnimation() {
//        listHeader.setVisibility(View.GONE);
//        listViewEmpty.setVisibility(View.GONE);
//        loadingLayout.setVisibility(View.VISIBLE);
//        mainItems.clear();
//        adapter = new MyPerformancesAdapter(MyVideosActivity.this, mainItems);
//        list.deferNotifyDataSetChanged();
//
//        anim = ObjectAnimator.ofFloat(spinner, "rotation", 0f, 359f);
//        anim.setInterpolator(new LinearInterpolator());
//        anim.setDuration(600);
//        anim.setRepeatCount(Animation.INFINITE);
//        anim.start();
//    }
//
//    private void displayEmptyPerformances() {
//        listViewEmpty.setVisibility(View.VISIBLE);
//        imageEmpty.setImageResource(R.drawable.no_data_performances);
//        showOffButton.setVisibility(View.VISIBLE);
//        textEmpty.setText("Nothing here yet");
//        showOffButton.setText("Show off and record");
//    }
//
////    private void displayEmptyReviews() {
////
////        listViewEmpty.setVisibility(View.VISIBLE);
////        imageEmpty.setImageResource(R.drawable.no_data_reviews);
////        showOffButton.setVisibility(View.INVISIBLE);
////        textEmpty.setText("Nothing here yet");
////
////    }
//
////    private void displayEmptyBio() {
////        listViewEmpty.setVisibility(View.VISIBLE);
////        imageEmpty.setImageResource(R.drawable.no_data_bio);
////        showOffButton.setVisibility(View.VISIBLE);
////        showOffButton.setText("Create your bio video!");
////    }
//
//    private void displayEmptyFilter() {
//        listViewEmpty.setVisibility(View.VISIBLE);
//        imageEmpty.setImageResource(R.drawable.no_data_reviews);
//        showOffButton.setVisibility(View.INVISIBLE);
//        textEmpty.setText("No data for these filters");
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();// ATTENTION: This was auto-generated to implement the App Indexing API.
//// See https://g.co/AppIndexing/AndroidStudio for more information.
//        client.connect();
//        loadAnimation();
//        listHeader.setText("Sorted by \"" + filter.getSortType() + "\"");
//        getVideos();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//    }
//
////    @Override
////    protected int getSelfNavDrawerItem() {
////        return NavDrawerActivity.NAVDRAWER_ITEM_MY_VIDEOS;
////    }
//
//    @Override
//    public void onFilterSelceted(int selected) {
//        if (selectedFilter != selected) {
//            Map<String, String> filterParams = new HashMap<String, String>();
//            switch (selected) {
//                case ALL:
//                    selectedFilter = selected;
//                    filterModel.setFilterType("all");
//                    SharedPreferencesManager.getEditor(getApplicationContext()).putString(Constants.KEY_FILTER, "all");
//                    onReloadFilters(filterModel, true);
//                    selectedFilter = ALL;
//                    break;
//                case PUBLIC:
//                    filterModel.setFilterType("public");
//                    SharedPreferencesManager.getEditor(getApplicationContext()).putString(Constants.KEY_FILTER, "public");
//                    onReloadFilters(filterModel, true);
//
//                    selectedFilter = PUBLIC;
//                    break;
//                case PRIVATE:
//                    filterModel.setFilterType("private");
//                    SharedPreferencesManager.getEditor(getApplicationContext()).putString(Constants.KEY_FILTER, "private");
//                    onReloadFilters(filterModel, true);
//
//                    selectedFilter = PRIVATE;
//                    break;
//            }
//        }
//    }
//}
