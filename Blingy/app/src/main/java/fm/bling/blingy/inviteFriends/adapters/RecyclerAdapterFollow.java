package fm.bling.blingy.inviteFriends.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.inviteFriends.listeners.FollowListener;

import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.ClickableImageView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/8/16.
 * History:
 * ***********************************
 */
public class RecyclerAdapterFollow extends RecyclerView.Adapter<RecyclerAdapterFollow.DataObjectHolder>
{
    private List<User> mDataset;
    private ImageLoaderManager mImageLoaderManager;
    private SparseArray<DataObjectHolder>holders;
    private View.OnClickListener followOnClickListener;
    private FollowListener mFollowListener;

    private int imageWidthHeight;
    private int statusImageWidthHeight;

    public RecyclerAdapterFollow(List<User> myDataset, ImageLoaderManager imageLoaderManager, FollowListener followListener)
    {
        mDataset = myDataset;
        mImageLoaderManager = imageLoaderManager;
        mFollowListener = followListener;
        holders = new SparseArray<>();

        imageWidthHeight        = (int) (120f * App.SCALE_X);
        statusImageWidthHeight  = (int) (80f * App.SCALE_X);

        followOnClickListener = new View.OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                v.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        User user = ((User) v.getTag());
                        if (user.getFollowedByMe().equals("0"))
                        {
                            mFollowListener.follow(user.getUserId());
                        }
                        else
                        {
                            mFollowListener.unFollow(user.getUserId());
                        }
                    }
                });
            }
        };
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewIndex)
    {
        DataObjectHolder dataObjectHolder = holders.get(viewIndex);
        if(dataObjectHolder == null)
        {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.invite_friend_contact_list_item_follow, parent, false);

            dataObjectHolder = new DataObjectHolder(view);

            holders.put(viewIndex, dataObjectHolder);
        }
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        User tile = mDataset.get(position);
        holder.mainView.setTag(tile);
        holder.mainView.setId(position);
        holder.Name.setText(tile.getFullName());
        if(tile.getPhoto() != null)
        {
//            if(tile.getPhoto().indexOf("content") >= 0)
//            {
//                holder.contactImage.setIsLocalImage(true);
//            }
//            else holder.contactImage.setIsLocalImage(false);
            holder.contactImage.setUrl(tile.getPhoto());
            mImageLoaderManager.DisplayImage(holder.contactImage);
        }
        else
        {
            holder.contactImage.setImageResource(holder.contactImage.getPlaceHolder());
        }
        if(tile.getFollowedByMe().equals("0"))
        {
            holder.statusImage.setBackgroundResource(R.drawable.ic_follow_green_24dp);
        }
        else
        {
            holder.statusImage.setBackgroundResource(R.drawable.ic_followed_grey_24dp);
        }
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public void setData(List<User> data)
    {
        mDataset = data;
        notifyDataSetChanged();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder
    {
        View mainView;
        URLImageView contactImage;
        TextView Name;
        ClickableImageView statusImage;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            this.mainView   = itemView;
            contactImage    = (URLImageView) itemView.findViewById(R.id.invite_friend_contact_list_item_contact_image);
            Name            = (TextView)     itemView.findViewById(R.id.invite_friend_contact_list_item_contact_name);
            statusImage     = (ClickableImageView)itemView.findViewById(R.id.invite_friend_contact_list_item_contact_status);
            mainView.setOnClickListener(followOnClickListener);

            Name.setTypeface(App.ROBOTO_REGULAR);
            Name.getLayoutParams().width = App.WIDTH / 2;

            statusImage.setLayoutParams(new LinearLayout.LayoutParams(statusImageWidthHeight, statusImageWidthHeight));
            contactImage.setWidthInternal(imageWidthHeight);
            contactImage.setIsRoundedImage(true);
            contactImage.setLayoutWidthHeight(imageWidthHeight, imageWidthHeight);
            contactImage.setImageLoader(mImageLoaderManager);
            contactImage.setPlaceHolder(R.drawable.extra_large_userpic_placeholder);
        }
    }

    @Override
    public void onViewRecycled(DataObjectHolder holder)
    {
        super.onViewRecycled(holder);
    }

    public void close()
    {
        for(int i=0;i<holders.size();i++)
        {
            holders.get(i).contactImage.close();
        }
        holders.clear();holders = null;
        mDataset= null;
        mImageLoaderManager = null;
        followOnClickListener = null;
        mFollowListener = null;
    }

}