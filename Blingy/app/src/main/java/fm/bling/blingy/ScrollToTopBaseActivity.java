package fm.bling.blingy;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 11/01/17.
 * History:
 * ***********************************
 */
public abstract class ScrollToTopBaseActivity extends BaseActivity {

    public abstract void scrollToTop();
}
