package fm.bling.blingy.profile.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import fm.bling.blingy.R;


public class EditPhotoDialog extends DialogFragment {
    OnDialogFinished mListener;
    public interface OnDialogFinished {
        void onSelectImageFromGallery();
        void onTakeImageWithCamera();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (OnDialogFinished) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.edit_photo_dialog_fragment, null);
        builder.setView(v);

        Dialog dialog = builder.create();
        TextView cameraText = (TextView)v.findViewById(R.id.Take_Photo);
        TextView galleryText = (TextView)v.findViewById(R.id.Choose_Existing_Photo);
        TextView cancelText = (TextView)v.findViewById(R.id.cancel_dialog);
        cancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        cameraText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                mListener.onTakeImageWithCamera();
            }
        });
        galleryText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                mListener.onSelectImageFromGallery();
            }
        });


        return dialog;
    }


}
