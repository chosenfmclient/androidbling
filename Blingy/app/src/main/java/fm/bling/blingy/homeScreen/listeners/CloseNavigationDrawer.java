package fm.bling.blingy.homeScreen.listeners;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 4/24/16.
 * History:
 * ***********************************
 */
public interface CloseNavigationDrawer {
   void onCloseNavigation(int id);
}
