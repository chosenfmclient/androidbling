package fm.bling.blingy.dialogs.flag;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import fm.bling.blingy.App;
import fm.bling.blingy.dialogs.adapters.CustomDialogAdapter;
import fm.bling.blingy.dialogs.listeners.FlagDialogListener;
import fm.bling.blingy.dialogs.model.CAAType;
import fm.bling.blingy.record.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Kiril on 6/4/2015.
 * Updated by Oren Zakay.
 */
public class FlagVideoDialog extends FlagBaseDialog {

    private String videoID;
    private CustomDialogAdapter adapter;
    private String[] flagsConstans   = new String[]{"sexual", "violent", "hateful", "harmful", "infringing"};

    public FlagVideoDialog(Context context, String videoID, FlagDialogListener flagDialogListener) {
        super(context,flagDialogListener);
        this.videoID = videoID;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        for (int i = 0; i < firstDialogList.length; i++) {
            map.put(flagsDialogList[i], flagsConstans[i]);
        }
    }

    @Override
    protected void onStart() {
        adapter = new CustomDialogAdapter(mContext, firstDialogList);
        listView.setAdapter(adapter);
        Utils.setListViewHeightBasedOnChildren(listView);
        super.onStart();
    }

    @Override
    protected void sendReport() {
        String formatedString = "";
        if (map.get(flagType) != null) {
            formatedString = map.get(flagType);
        } else {
            formatedString = "spam";
        }

        CAAType type = new CAAType(formatedString);

        Log.d("TAG", formatedString);


        App.getUrlService().putVideoFlag(videoID, type, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                dismiss();//handle toast msg in activity
                mFlagDialogListener.onFinishFlagVideoDialog(true);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
