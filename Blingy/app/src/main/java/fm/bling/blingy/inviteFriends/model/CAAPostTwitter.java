//package fm.bling.blingy.inviteFriends.model;
//
//import com.google.gson.annotations.SerializedName;
//
//import java.util.List;
//
///**
// * *********************************
// * Project: Chosen Android Application
// * FileName: CAAPostTwitter
// * Description:
// * Created by Dawidowicz Nadav on 6/29/15.
// * History:
// * ***********************************
// */
//public class CAAPostTwitter {
//    @SerializedName("twitter_ids")
//    private List <String> listTwitter;
//
//    public CAAPostTwitter(List<String> listTwitter) {
//        this.listTwitter = listTwitter;
//    }
//
//    public List<String> getListTwitter() {
//        return listTwitter;
//    }
//
//    public void setListTwitter(List<String> listTwitter) {
//        this.listTwitter = listTwitter;
//    }
//}
