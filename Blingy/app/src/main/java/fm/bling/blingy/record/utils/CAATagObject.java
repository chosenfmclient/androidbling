package fm.bling.blingy.record.utils;

/**
 * Created by Chosen-pro on 2/15/16.
 */
public class CAATagObject {
    String tag;
    String hint;
    boolean locked = false;

    public CAATagObject(String tag, String hint) {
        this.tag = tag;
        this.hint = hint;
    }
    public CAATagObject(String tag, String hint, boolean locked) {
        this.tag = tag;
        this.hint = hint;
        this.locked = locked;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }
}
