package fm.bling.blingy.dialogs.share.workers.listeners;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 3/30/16.
 * History:
 * ***********************************
 */
public interface onWorkerFinishedListener {
    void onWorkerFinished(boolean result, String filePath);
}
