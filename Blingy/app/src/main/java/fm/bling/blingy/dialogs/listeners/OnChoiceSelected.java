package fm.bling.blingy.dialogs.listeners;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/14/16.
 * History:
 * ***********************************
 */
public interface OnChoiceSelected {
    void onChoiceSelected(int choice);
}
