//
//package fm.bling.blingy.widget.views;
//
//import android.content.Context;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//
//import fm.bling.blingy.App;
//import fm.bling.blingy.R;
//import fm.bling.blingy.widget.CAAWidgetSettingsSingleton;
//import fm.bling.blingy.widget.model.CAAInvitePrize;
//
//
///**
// * *********************************
// * Project: Chosen Android Application
// * Description:
// * Created by Oren Zakay on 7/17/16.
// * History:
// * ***********************************
// */
//public class PointsView extends LinearLayout {
//
//    private LayoutInflater mLayoutInflater;
//    private LinearLayout mRoundedBox;
//    private ImageView mArrowDown;
//
//    public PointsView(Context context, boolean isArrowUp, boolean disableArrow) {
//        super(context);
//        init(context,isArrowUp, disableArrow);
//
//    }
//
//    private void init(Context context, boolean isArrowUp,boolean mDisableArrow){
//        LayoutParams mainParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        mainParams.gravity = Gravity.CENTER_HORIZONTAL;
//        this.setLayoutParams(mainParams);
//        this.setOrientation(VERTICAL);
//
//        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        LayoutParams boxParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        boxParams.gravity = Gravity.CENTER_HORIZONTAL;
//        mRoundedBox = new LinearLayout(context);
//        int main_margin = (int)context.getResources().getDimensionPixelOffset(R.dimen.main_margin);
//        mRoundedBox.setPadding(main_margin,main_margin,main_margin,main_margin);
//        mRoundedBox.setOrientation(LinearLayout.VERTICAL);
//        mRoundedBox.setBackgroundResource(R.drawable.black_rect_rounded);
//        mRoundedBox.setLayoutParams(boxParams);
//
//        if(!mDisableArrow) {
//            LayoutParams arrowParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            arrowParams.gravity = Gravity.CENTER_HORIZONTAL;
//            mArrowDown = new ImageView(context);
//            mArrowDown.setImageResource(R.drawable.tutorial_black_arrow);
//            mArrowDown.setLayoutParams(arrowParams);
//        }
//
//        if(isArrowUp){
//            if(!mDisableArrow) {
//                mArrowDown.setRotation(180f);
//                this.addView(mArrowDown);
//            }
//            this.addView(mRoundedBox);
//        }
//        else{
//            this.addView(mRoundedBox);
//            if(!mDisableArrow)
//                this.addView(mArrowDown);
//        }
//    }
//
//    public void addLineView(boolean showStar,boolean isBold ,String text){
//        LinearLayout line = (LinearLayout) mLayoutInflater.inflate(R.layout.points_item_view, null);
//        if(!showStar)
//            line.findViewById(R.id.star).setVisibility(GONE);
//        ((TextView)line.findViewById(R.id.text)).setText(text);
//
//        if(isBold)
//            ((TextView)line.findViewById(R.id.text)).setTypeface(App.ROBOTO_MEDIUM);
//        else
//            ((TextView)line.findViewById(R.id.text)).setTypeface(App.ROBOTO_REGULAR);
//
//        mRoundedBox.addView(line);
//    }
//
//    public void addBigLineView(Context context,String text){
//        LinearLayout line = (LinearLayout) mLayoutInflater.inflate(R.layout.points_item_view,null);
//        line.findViewById(R.id.star).setVisibility(GONE);
//        ((TextView)line.findViewById(R.id.text)).setText(text);
//        ((TextView)line.findViewById(R.id.text)).setTextAppearance(context,R.style.title_card);
//        mRoundedBox.addView(line);
//    }
//
//    public void removeAllLines(){
//        mRoundedBox.removeAllViews();
//    }
//
//    public int getHeightInternal() {
//
//        if (this.getVisibility() == INVISIBLE)
//            return 0;
//        else {
//            int[] loc;
//            getLocationOnScreen(loc = new int[2]);
//            return loc[1];
//        }
//    }
//
//    public void addPrizesDescription(Context context) {
//        this.addLineView(false , true, context.getString(R.string.earn_prizes));
//        ArrayList<CAAInvitePrize> prizes = CAAWidgetSettingsSingleton.getInstance().getPrizes();
//        if(prizes != null && prizes.size() > 0) {
//            for (CAAInvitePrize prize : prizes) {
//                this.addLineView(true, false, prize.getPoints() + " = " + prize.getText());
//            }
//            this.addLineView(false, false, context.getString(R.string.prizes_redeem));
//        }
//    }
//
//    public void close(){
//        this.removeAllViews();
//    }
//}