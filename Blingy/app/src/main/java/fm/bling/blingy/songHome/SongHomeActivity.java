package fm.bling.blingy.songHome;

import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;

import java.util.ArrayList;

import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.songHome.fragments.SongHomeFragment;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 10/10/16.
 * History:
 * ***********************************
 */
public class SongHomeActivity extends BaseActivity
{
    /**
     * Menu Items
     **/
    //    private String mVideoID;
    private String mClipID;
    private String mSongClipUrl;
    private String mClipThumbnail;
    private String mArtistName;
    private String mSongName;
    private ArrayList<Integer> mSegments;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.song_home_activity);
        TrackingManager.getInstance().pageView(EventConstants.SONG_HOME_PAGE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_20));
        }

        mClipID = getIntent().getStringExtra(Constants.CLIP_ID);
        if (mClipID == null)
        {
            mSongClipUrl = getIntent().getStringExtra(Constants.CLIP_URL);
            mSongName = getIntent().getStringExtra(Constants.SONG_NAME);
            mArtistName = getIntent().getStringExtra(Constants.ARTIST_NAME);
            mClipThumbnail = getIntent().getStringExtra(Constants.CLIP_THUMBNAIL);
            mSegments = getIntent().getIntegerArrayListExtra(Constants.TIME_SEGMENTS);
        }

        moveToSongHome();
    }

    private void moveToSongHome()
    {
        Fragment discoverFragment = SongHomeFragment.newInstance(mSongClipUrl, mSongName, mArtistName, mClipThumbnail, mSegments, mClipID);
        getFragmentManager().beginTransaction().add(R.id.main_container, discoverFragment, Constants.DISCOVER_FRAGMENT).commit();
    }

}