package fm.bling.blingy.inviteFriends.model;

import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/15/16.
 * History:
 * ***********************************
 */
public class ContactInfo
{
    public final static byte NOT_INVITED = 0, INVITED = 1, FOLLOWING = 3, NOT_FOLLOWING =4;
    public final static int NOT_FROM_CONTACT_ID = -15;

    public final int id;
    public final String name;
    public String email;
    public final String photoUrl;
    public byte  status;
    public byte  cameoInvited;
    public boolean isOnChosen;
    public String chosenId;
    private String phoneNumber;

    public ContactInfo(int id, String name, String photoUrl, String emailOrPhone, boolean isPhoneNumber)//, String phoneNumber
    {
        this.id          = id;
        this.name        = name;
        this.photoUrl    = photoUrl;
        if(isPhoneNumber)
            phoneNumber  = emailOrPhone;
        else
            this.email   = emailOrPhone;
        isOnChosen       = false;
        chosenId         = null;
    }

    public ContactInfo(int id, String name, String photoUrl, String email, byte status, boolean isOnChosen, String chosenId)
    {
        this.id          = id;
        this.name        = name;
        this.photoUrl    = photoUrl;
        this.email       = email;
        this.status      = status;
        this.isOnChosen  = isOnChosen;
        this.chosenId    = chosenId;
    }

    @Override
    public boolean equals(Object obj)
    {
        return obj instanceof ContactInfo && ((ContactInfo)obj).id == id;
    }

 /*   public static ArrayList<String> getEmails(ArrayList<ContactInfo> contacts)
    {
        ArrayList<String>ret = new ArrayList<>();
        ContactInfo ci;
        for(int i=0; i<contacts.size(); i++)
        {
            if((ci = contacts.get(i)).email != null)
                ret.add(ci.email);
        }
        return ret;
    }*/

//    public void saveContactInfo(DataOutputStream dos) throws  Throwable
//    {
//        dos.writeInt(id);
//        dos.writeUTF(name);
//        dos.writeUTF(photoUrl);
//        dos.writeUTF(email);
//        dos.writeByte(status);
//        dos.writeBoolean(isOnChosen);
//        dos.writeUTF(chosenId);
//    }

    public static ContactInfo readContactInfo(DataInputStream dis) throws  Throwable
    {
        return new ContactInfo(dis.readInt(), dis.readUTF(), dis.readUTF(), dis.readUTF(), dis.readByte(), dis.readBoolean(), dis.readUTF());
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public boolean isPhoneNumberSet()
    {
        return phoneNumber != null;
    }

    public String phone()
    {
        return phoneNumber;
    }

    public String getName(){
        return this.name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }
}