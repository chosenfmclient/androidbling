package fm.bling.blingy.utils.views;

import android.content.Context;
import android.util.AttributeSet;

import fm.bling.blingy.utils.views.listeners.SwipeViewDropListenter;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 24/06/2016.
 * History:
 * ***********************************
 */
public class SwipeViewToSide extends SwipeViewBase
{
    private final float finalDegree = 45f;
    private float maxXBeforeDrop;
    private float difX, difY, lastX, lastY;
    private float degrees;
    private float width, height;
    private SwipeViewDropListenter mOnViewDropListener;
    private boolean inTouchMode, isDropped;

    public SwipeViewToSide(Context context)
    {
        super(context);
    }

    public SwipeViewToSide(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public SwipeViewToSide(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public void setOnViewDropListener(SwipeViewDropListenter onViewDropListener)
    {
        mOnViewDropListener = onViewDropListener;
    }

    public boolean isDropped()
    {
        return isDropped;
    }

    @Override
    public void actionMove(float x, float y)
    {
        if (isDropped) return;
        setLocation(lastX = x - difX, lastY = y - difY);
        degrees = finalDegree * (lastX / width);
        setRotation(degrees);
    }

    @Override
    public void actionDown(float x, float y)
    {
        if (isDropped) return;
        inTouchMode = true;
        difX = x - lastX;
        difY = y - lastY;
        width = getWidth();
        height = getHeight();
        maxXBeforeDrop = width / 2f;
    }

    @Override
    public void actionUp(float x, float y)
    {
        if (isDropped) return;
        inTouchMode = false;
        if (lastX > maxXBeforeDrop || lastX < -maxXBeforeDrop)
        {
            isDropped = true;
            new DropViewAnimation().start();
        }
        else
        {
            new backToPlaceAnimation().start();
        }
    }

    @Override
    public void actionCancel(float x, float y)
    {
        if (isDropped) return;
        inTouchMode = false;
        new backToPlaceAnimation().start();
    }

    private class backToPlaceAnimation extends Thread
    {
        public void run()
        {
            if (isDropped) return;
            float stepX, stepY;
            int finalStep;
            if (Math.abs(lastX) > Math.abs(lastY))
            {
                stepY = 1f;
                stepX = Math.abs(lastX) / Math.abs(lastY);
                finalStep = (int) Math.abs(lastY);
            }
            else
            {
                stepX = 1f;
                stepY = Math.abs(lastY) / Math.abs(lastX);
                finalStep = (int) Math.abs(lastX);
            }
            while (finalStep > 0 && !inTouchMode)
            {
                if (lastX < 0)
                    lastX += stepX;
                else lastX -= stepX;
                if (lastY < 0)
                    lastY += stepY;
                else lastY -= stepY;
                degrees = finalDegree * (lastX / width);
                post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        setRotation(degrees);
                        setLocation(lastX, lastY);
                    }
                });
                finalStep--;
                try
                {
                    Thread.sleep(0, 150000);
                }
                catch (Throwable err)
                {
                }
            }
            if (!inTouchMode)
            {
                post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        setRotation(degrees = 0);
                        setLocation(lastX = 0, lastY = 0);
                    }
                });
            }
        }

    }

    private class DropViewAnimation extends Thread
    {
        public void run()
        {
            float stepX, stepY;
            int finalStep;
            if (Math.abs(width - lastX) > Math.abs(height - lastY))
            {
                stepY = 1f;
                finalStep = (int) Math.abs(height - lastY);
                stepX = Math.abs(width - lastX) / finalStep;

            }
            else
            {
                stepY = 1f;
                finalStep = (int) Math.abs(height - lastY);
                stepX = Math.abs(width + lastX) / finalStep;

            }
            while (finalStep > 0)
            {
                if (lastX > 0)
                    lastX += stepX;
                else lastX -= stepX;
                lastY += stepY;
                degrees = finalDegree * (lastX / width);
                post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        setRotation(degrees);
                        setLocation(lastX, lastY);
                    }
                });
                finalStep--;
                try
                {
                    Thread.sleep(0, 150000);
                }
                catch (Throwable err)
                {
                }
            }
            if (mOnViewDropListener != null)
                mOnViewDropListener.onViewDropped();
        }

    }

}