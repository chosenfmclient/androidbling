package fm.bling.blingy.upload;

import android.os.Environment;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 04/07/2016.
 * History:
 * ***********************************
 */
public class FileManager
{
    public static final byte PHOTO = 0, VIDEO = 1;
    private final int minimumFileSize = 1024 * 35;

    private String[] mExtensions;
    private File[] mFolders;
    private final byte mType;

    private HashMap<Integer, FolderManager> mFoldersManager; // folder name hashcode to FolderManager

    public FileManager(byte type)
    {
        mType = type;
        getExtensions(type);
        init();
    }

    private void getExtensions(byte type)
    {
        switch (type)
        {
            case PHOTO:
                mExtensions = new String[]{".jpg", ".png"};
                break;
            case VIDEO:
                mExtensions = new String[]{".avi", ".mp4", ".mpeg"};
                break;
        }
    }

    private void init()
    {
        ArrayList<File> folders = new ArrayList<>();
        File rootDir = Environment.getExternalStorageDirectory();

        File[] files = rootDir.listFiles();
        for (int i = 0; i < files.length; i++)
        {
            if (files[i].isDirectory() && notFiltering(files[i].getName()))
                addDir(files[i], folders);
        }

        mFoldersManager = new HashMap<>();
        for (int i = folders.size() - 1; i >= 0 ; i--)
        {
            FolderManager folderManager = new FolderManager(folders.get(i), mType);
            if(folderManager.hasFiles()) {
                mFoldersManager.put(folders.get(i).getPath().hashCode(), folderManager);
            }
            else
                folders.remove(i);
        }
        mFolders = folders.toArray(new File[folders.size()]);
//        mExtensions = null;
    }

    private void addDir(File dir, ArrayList<File> folders)
    {
        boolean isAdded = false;
        File[] files = dir.listFiles();
        if(files == null)
            return;
        for (File file : files)
        {
            if (file.isDirectory())
            {
                if (notFiltering(file.getName())) // Not hidden
                    addDir(file, folders);
            }
            else if (!isAdded && file.length() > minimumFileSize && isInExtensions(file.getName()))
            {
                int index = 0;
                for(int i=0;i<folders.size();i++)
                {
                    if(folders.get(i).getName().compareToIgnoreCase(dir.getName()) > 0)
                        break;
                    index++;
                }
                folders.add(index, dir);
                isAdded = true;
            }
        }
    }

    private boolean notFiltering(String str)
    {
        return
                str.charAt(0) != '.' &&
                !str.equalsIgnoreCase("android")&&
                !str.equalsIgnoreCase("Data") &&
                !str.equalsIgnoreCase("files");
    }

    public boolean isInExtensions(String name)
    {
        if(name.charAt(0) == '.')
            return false;
        for (String ext : mExtensions)
        {
            try
            {
                if (name.substring(name.length() - ext.length()).equals(ext))
                    return true;
            }
            catch (Throwable err)
            {
            }
        }
        return false;
    }

    public File[] getFolders()
    {
        return mFolders;
    }

    public byte getType()
    {
        return mType;
    }

    public FolderManager getFolderManager(String folderPath)
    {
        return mFoldersManager.get(folderPath.hashCode());
    }

    public void close()
    {
        mExtensions = null;
        mFolders = null;
        try{mFoldersManager.clear();}catch (Throwable err){}
        mFoldersManager = null;
    }

}