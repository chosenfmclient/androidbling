package fm.bling.blingy.database.contract;


public class DuetContract extends BaseContract {

    /** Table Name **/
    public static final String TABLE_NAME = "duet_invited";


    /** Columns **/
    public static final String COLUMN_VIDEO_ID = "video_id";

    public static final String COLUMN_USER_ID = "user_id";

    public static final String COLUMN_USER_NAME = "user_name";

    public static final String COLUMN_EMAIL = "user_email";

    public static final String COLUMN_PHOTO = "photo";

    public static final String COLUMN_PHONE = "user_phone";

    /** Querires **/
    //Create
    public static final String CREATE_TABLE =
            CREATE_IF_NOT_EXISTS + TABLE_NAME + " (" +
                    COLUMN_VIDEO_ID + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    COLUMN_USER_ID + TEXT_TYPE + COMMA_SEP +
                    COLUMN_USER_NAME + TEXT_TYPE  + COMMA_SEP +
                    COLUMN_EMAIL + TEXT_TYPE  + COMMA_SEP +
                    COLUMN_PHOTO + TEXT_TYPE  + COMMA_SEP +
                    COLUMN_PHONE + TEXT_TYPE  + ")";

    //Delete
    public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    //Insert many
    public static final String INSERT_MANY = "INSERT "+ OR_REPLACE + "INTO " + TABLE_NAME + " (" +
            COLUMN_VIDEO_ID + COMMA_SEP +
            COLUMN_USER_ID + COMMA_SEP +
            COLUMN_USER_NAME + COMMA_SEP +
            COLUMN_EMAIL + COMMA_SEP +
            COLUMN_PHOTO + COMMA_SEP +
            COLUMN_PHONE + ")" + " VALUES ";

}
