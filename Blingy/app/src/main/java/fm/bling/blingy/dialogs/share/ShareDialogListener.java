package fm.bling.blingy.dialogs.share;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/29/16.
 * History:
 * ***********************************
 */
public interface ShareDialogListener
{
    void onShareTypeSelected(int shareType);
}