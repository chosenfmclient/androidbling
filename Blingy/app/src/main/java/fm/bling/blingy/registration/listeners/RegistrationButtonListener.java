package fm.bling.blingy.registration.listeners;

/**
 * Created by Chosen-pro on 5/11/16.
 */
public interface RegistrationButtonListener {
    public void goToRegistration();
    public void goToLogin();
}
