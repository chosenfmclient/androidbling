package fm.bling.blingy.inviteFriends.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.inviteFriends.listeners.SearchListener;
import fm.bling.blingy.inviteFriends.model.ContactInfo;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.ClickableView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/8/16.
 * History:
 * ***********************************
 */
public class RecyclerAdapterSearch extends RecyclerView.Adapter<RecyclerAdapterSearch.DataObjectHolder>
{
    private ArrayList<ContactInfo> mDataset;
    private ImageLoaderManager mImageLoaderManager;
    private SparseArray<DataObjectHolder>holders;
    private View.OnClickListener tileMainOnClickListener;
    private SearchListener mSearchListener;

    private int imageWidthHeight = -1;
    private int statusImageWidthHeight;

    public RecyclerAdapterSearch(ArrayList<ContactInfo> myDataset, ImageLoaderManager imageLoaderManager, SearchListener searchListener)//, InvalidateNotificationListener invalidateNotificationListener)
    {
        mDataset = myDataset;
        mImageLoaderManager = imageLoaderManager;
        mSearchListener = searchListener;

        holders = new SparseArray<>();

        imageWidthHeight        = (int) (120f * App.SCALE_X);
        statusImageWidthHeight  = (int) (80f * App.SCALE_X);

        tileMainOnClickListener = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ContactInfo contactInfo = (ContactInfo) v.getTag();
                if(contactInfo.isOnChosen)
                {
                    mSearchListener.followUnfollow(contactInfo.chosenId);
                }
                else    // invite
                {
                    mSearchListener.invite(contactInfo);
                }
            }
        };
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewIndex)
    {
        DataObjectHolder dataObjectHolder = holders.get(viewIndex);
        if(dataObjectHolder == null)
        {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.invite_friend_contact_list_item, parent, false);
            dataObjectHolder = new DataObjectHolder(view);
            holders.put(viewIndex, dataObjectHolder);
        }
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        ContactInfo contactInfo = mDataset.get(position);
        holder.Name.setText(contactInfo.name);
        holder.mainView.setTag(contactInfo);
        holder.mainView.setId(position);

        if(contactInfo.photoUrl != null)
        {
//            if(contactInfo.photoUrl.indexOf("content") >= 0)
//            {
//                holder.contactImage.setIsLocalImage(true);
//            }
//            else
//            {
//                holder.contactImage.setIsLocalImage(false);
//            }
            holder.contactImage.setUrl(contactInfo.photoUrl);
            mImageLoaderManager.DisplayImage(holder.contactImage);
        }
        else
        {
            holder.contactImage.setLayoutParams(new FrameLayout.LayoutParams(imageWidthHeight, imageWidthHeight));
            holder.contactImage.setImageResource(holder.contactImage.getPlaceHolder());
        }
        if(contactInfo.isOnChosen)
        {
            holder.address.setVisibility(View.INVISIBLE);
            if (contactInfo.status == ContactInfo.FOLLOWING)
            {
                holder.statusImage.setBackgroundResource(R.drawable.ic_followed_grey_24dp);
            }
            else
            {
                holder.statusImage.setBackgroundResource(R.drawable.ic_follow_green_24dp);
            }
        }
        else
        {
            holder.address.setVisibility(View.VISIBLE);
            if(contactInfo.isPhoneNumberSet())
                holder.address.setText(contactInfo.phone());
            else holder.address.setText(contactInfo.email);
            if (contactInfo.status == ContactInfo.NOT_INVITED)
            {
                holder.statusImage.setBackgroundResource(R.drawable.ic_invited_friend_green_24dp);
            }
            else
            {
                holder.statusImage.setBackgroundResource(R.drawable.ic_invite_friend_grey_24dp);
            }
        }
    }

    public void setData(ArrayList<ContactInfo> data)
    {
        mDataset = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder
    {
        View mainView;
        URLImageView contactImage;
        TextView Name, address;
        ClickableView statusImage;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            this.mainView   = itemView;
            contactImage    = (URLImageView) itemView.findViewById(R.id.invite_friend_contact_list_item_contact_image);
            Name            = (TextView)     itemView.findViewById(R.id.invite_friend_contact_list_item_contact_name);
            address         = (TextView)     itemView.findViewById(R.id.invite_friend_contact_list_item_contact_address);

            address.setTypeface(App.ROBOTO_REGULAR);
            address.getLayoutParams().width = App.WIDTH / 2;

            statusImage     = (ClickableView)itemView.findViewById(R.id.invite_friend_contact_list_item_contact_status);
            mainView.setOnClickListener(tileMainOnClickListener);

            Name.setTypeface(App.ROBOTO_REGULAR);
            Name.getLayoutParams().width = App.WIDTH / 2;

            statusImage.setLayoutParams(new LinearLayout.LayoutParams(statusImageWidthHeight, statusImageWidthHeight));
            contactImage.setWidthInternal(imageWidthHeight);

            contactImage.setImageLoader(mImageLoaderManager);

            contactImage.setIsRoundedImage(true);
            contactImage.setLayoutWidthHeight(imageWidthHeight, imageWidthHeight);
            contactImage.setPlaceHolder(R.drawable.extra_large_userpic_placeholder);
        }
    }

    @Override
    public void onViewRecycled(DataObjectHolder holder)
    {
        super.onViewRecycled(holder);
    }

    public void close()
    {
        for(int i=0;i<holders.size();i++)
        {
            holders.get(i).contactImage.close();
        }
        mDataset = null;
        mImageLoaderManager = null;
        holders = null;
        tileMainOnClickListener = null;
    }

}