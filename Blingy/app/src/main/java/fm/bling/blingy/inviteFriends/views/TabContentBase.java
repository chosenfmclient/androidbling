package fm.bling.blingy.inviteFriends.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/8/16.
 * History:
 * ***********************************
 */
public abstract class TabContentBase extends LinearLayout
{

    public TabContentBase(Context context)
    {
        super(context);
    }

    public TabContentBase(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public TabContentBase(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public abstract void close();

    public abstract View getOnboardingView();
}