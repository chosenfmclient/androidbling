package fm.bling.blingy.duetHome.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import fm.bling.blingy.videoHome.model.CAAVideo;

public class DuetHomeFeed {

    @SerializedName("trending_videos")
    ArrayList<CAAVideo> trendingVideos;

    @SerializedName("featured_cameo")
    CAAVideo featuredClip;

    public ArrayList<CAAVideo> getTrendingVideos() {
        return trendingVideos;
    }

    public CAAVideo getFeaturedClip() {
        return featuredClip;
    }
}
