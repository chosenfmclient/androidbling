package fm.bling.blingy.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Oren Zakay on 2/09/2015.
 */
public final class FixImageOrientation {
    private FixImageOrientation(){}
    public final static void fix(Context context, String imagePath, Uri imageUri , boolean cropToCenter, int mRatioSize) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = 4;
        Bitmap bm = BitmapFactory.decodeFile(imagePath, opts);
        if (bm != null) {

            ExifInterface exif = null;
            try {
                exif = new ExifInterface(imagePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
//            String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
//            int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_UNDEFINED);

            int rotationAngle = 0;
            switch(orientation){
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotationAngle = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotationAngle = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotationAngle = 270;
                    break;
                case ExifInterface.ORIENTATION_UNDEFINED:
                    String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};

                    Cursor cursor = context.getContentResolver().query(imageUri, projection, null, null, null);
                    orientation = -1;
                    if (cursor != null && cursor.moveToFirst()) {
//                        orientation = cursor.getInt(0);
                        orientation = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.ImageColumns.ORIENTATION));
                        cursor.close();
                    }
//                    if(orientation == 0){
//                        rotationAngle = 180;
//                    } else
                    if (orientation == 90 ) {
                        rotationAngle = 90;
                    } else if (orientation == 270) {
                        rotationAngle = 270;
                    } else if (orientation == 180) {
                        rotationAngle = 180;
                    }

//                    Toast.makeText(context,"The orientarion is : " + orientation + " and the rotation is : " + rotationAngle,Toast.LENGTH_SHORT).show();

                    break;

            }

//            if (orientation == ExifInterface.ORIENTATION_ROTATE_90)  rotationAngle = 90;
//            if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
//            if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

            Matrix matrix = new Matrix();
            matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
            Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
            if(bm != rotatedBitmap)
                bm.recycle();

            Bitmap finalBitmap = null;
            if(cropToCenter){
                finalBitmap = cropToCenter(rotatedBitmap, mRatioSize);
            }

            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = new FileOutputStream(imagePath);
                if(finalBitmap == null)
                    rotatedBitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                else
                    finalBitmap  .compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                Runtime.getRuntime().gc();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try
            {
                if(exif != null && exif.getAttribute(MediaStore.Images.ImageColumns.ORIENTATION) != null) {
                    exif.setAttribute(MediaStore.Images.ImageColumns.ORIENTATION, "0");
                    exif.saveAttributes();
                }
            }
            catch(Throwable err) {}
        }
    }

    private static Bitmap cropToCenter(Bitmap rotatedBitmap, int mRatioSize) {
        Bitmap returnedBitmap;
        if(rotatedBitmap.getWidth() < rotatedBitmap.getHeight()) {
            float scale = (((float) rotatedBitmap.getWidth() / ((float) rotatedBitmap.getHeight())));

            //Scale image
            Bitmap tmpScale = Bitmap.createScaledBitmap(rotatedBitmap, mRatioSize, ((int) (mRatioSize / scale)), true);
            if (rotatedBitmap != tmpScale)
                rotatedBitmap.recycle();
            //Crop image
            returnedBitmap = Bitmap.createBitmap(tmpScale, 0, ((int) ((((mRatioSize / scale) - mRatioSize) / 2))), mRatioSize, mRatioSize);
            if (tmpScale != returnedBitmap)
                tmpScale.recycle();
        }
        else {
            float scale = (((float) rotatedBitmap.getHeight() / ((float) rotatedBitmap.getWidth())));

            //Scale image
            Bitmap tmpScale = Bitmap.createScaledBitmap(rotatedBitmap, ((int) (mRatioSize / scale)), mRatioSize, true);
            if (rotatedBitmap != tmpScale)
                rotatedBitmap.recycle();

            //Crop image
            returnedBitmap = Bitmap.createBitmap(tmpScale, ((int) ((((mRatioSize / scale) - mRatioSize) / 2))), 0, mRatioSize, mRatioSize);
            if (tmpScale != returnedBitmap)
                tmpScale.recycle();
        }
        return returnedBitmap;
    }


}
