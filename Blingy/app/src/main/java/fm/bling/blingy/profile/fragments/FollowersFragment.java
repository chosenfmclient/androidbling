package fm.bling.blingy.profile.fragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.database.handler.FollowingDataHandler;
import fm.bling.blingy.dialogs.listeners.OnDoneLoadingListener;
import fm.bling.blingy.profile.ProfileActivity;
import fm.bling.blingy.profile.UserConnectionsActivity2;
import fm.bling.blingy.profile.adapters.UserConnectionRecyclerAdapter;
import fm.bling.blingy.profile.model.CAAApiUsers;
import fm.bling.blingy.record.utils.SimpleDividerItemDecoration;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.MultiImageLoader;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/30/16.
 * History:
 * ***********************************
 */
public class FollowersFragment extends BaseSearchFragment {

    /**
     * Main Views
     */
    private RecyclerView mRecycler;
    private FrameLayout listViewEmpty;
    private TextView mTextTotal;
    private RecyclerView.LayoutManager mLayoutManager;
    private UserConnectionRecyclerAdapter adapter;
    private View.OnClickListener mItemClickListener;
    private View.OnClickListener mFollowClickListener;

    private OnDoneLoadingListener onDoneLoadingListener;
    private MultiImageLoader mMultiImageLoader;
    private ArrayList<User> items;
    private ArrayList<User> searchDataSet;
    private String mUserId;
    private int mAmount;

    private int previousLastItem = 0;
    private int pageNumber = 0;
    private boolean calling = false;
    private boolean endPages = false;
    private boolean listInitialized = false;
    private int lastSearchLength;

    public static FollowersFragment newInstance(String user_id, int amount, OnDoneLoadingListener onDoneLoadingListener) {
        FollowersFragment myFragment = new FollowersFragment();
        myFragment.mUserId = user_id;
        myFragment.mAmount = amount;
        myFragment.onDoneLoadingListener = onDoneLoadingListener;
        return myFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFollowers();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMultiImageLoader = ((BaseActivity) getActivity()).getMultiImageLodaer();
        if(adapter != null) {
            adapter.setmMultiImageLoader(mMultiImageLoader);
            adapter.notifyDataSetChanged();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_connection_fragment, null);
        mRecycler = (RecyclerView) v.findViewById(R.id.connections_recycler);
        mTextTotal = (TextView) v.findViewById(R.id.text_total);
        listViewEmpty = (FrameLayout) v.findViewById(R.id.connection_empty);
        ((TextView)listViewEmpty.findViewById(R.id.empty_text)).setText(R.string.no_followers);

        mItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User friend = ((User)v.getTag());
                Intent i = new Intent(getActivity(), ProfileActivity.class);
                i.putExtra(Constants.USER_ID, friend.getUserId());
                getActivity().startActivity(i);
            }
        };

        mFollowClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User friend = ((User) v.getTag());
                int position = searchDataSet.indexOf(friend);
                UserConnectionRecyclerAdapter.DataObjectHolder holder = (UserConnectionRecyclerAdapter.DataObjectHolder)mRecycler.getChildViewHolder((View)(v.getParent()));
                if (!friend.getUserId().contentEquals(Constants.ME) && !friend.getUserId().contentEquals(CAAUserDataSingleton.getInstance().getUserId())) {
                    Drawable d = iFollowYou(friend) ? getResources().getDrawable(R.drawable.ic_followed_grey_24dp) : getResources().getDrawable(R.drawable.ic_follow_green_24dp);
                    holder.addFriend.setImageDrawable(d);
                    if (iFollowYou(friend)) {
                        friend.setFollowedByMe("0");
                        unFollowUser(friend.getUserId(), position);
                        if(mUserId.equalsIgnoreCase(Constants.ME))
                            ((UserConnectionsActivity2)getActivity()).updateFollowing(false, friend);
                        TrackingManager.getInstance().tapUnfollow(updatedFollowing(-1) ,friend.getUserId(),"profile", position + "");
                        updatedFollowing(-1);
                    } else {
                        friend.setFollowedByMe("1");
                        sendFollowUser(friend.getUserId(), position);
                        if(mUserId.equalsIgnoreCase(Constants.ME))
                            ((UserConnectionsActivity2)getActivity()).updateFollowing(true, friend);
                        TrackingManager.getInstance().tapFollow(friend.getUserId(), "profile" , position, updatedFollowing(1));
                    }
                }
            }
        };

        mMultiImageLoader = ((BaseActivity) getActivity()).getMultiImageLodaer();
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext(), R.drawable.line_divider, true));
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItem = ((LinearLayoutManager) mLayoutManager).findFirstVisibleItemPosition();
                if (listInitialized) {
                    final int lastItem = firstVisibleItem + visibleItemCount;
                    if (lastItem >= totalItemCount - 6 && !calling && !endPages) {
                        if (previousLastItem != lastItem) { //to avoid multiple calls for last item
//                            Log.d("Scroll", "Last:" + lastItem);
                            previousLastItem = lastItem;
                            getNextPage();
                        }
                    }
                }
            }
        });

        removeEmptyLayout();
//        loadAnimation();
        return v;
    }


    private void getFollowers() {
        App.getUrlService().getSpecificUserFollowersPage(mUserId, pageNumber, new Callback<CAAApiUsers>() {
            @Override
            public void success(CAAApiUsers caaApiUsers, Response response) {
                if (mRecycler == null || getActivity() == null)
                    return;
//                cancelAnimation();
                items = caaApiUsers.getData();
                if (items.size() < 1) {
                    showEmptyLayout();
                    endPages = true;
                }else {

                    searchDataSet = new ArrayList<>();
                    for (int i = 0; i < items.size(); i++)
                        searchDataSet.add(items.get(i));

                    adapter = new UserConnectionRecyclerAdapter(getContext(), mMultiImageLoader, searchDataSet, mItemClickListener, mFollowClickListener);

                    mTextTotal.setText("Followed by " + mAmount + " people");
                    mRecycler.setAdapter(adapter);
                    listInitialized = true;
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(onDoneLoadingListener != null)
                            onDoneLoadingListener.onDoneLoading();
                    }
                }, 500);


            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void getNextPage() {
        pageNumber++;
        calling = true;
        App.getUrlService().getSpecificUserFollowersPage(mUserId, pageNumber, new Callback<CAAApiUsers>() {
            @Override
            public void success(CAAApiUsers caaApiUsers, Response response) {
                if (mRecycler == null || getActivity() == null)
                    return;


                if (caaApiUsers.getData().size() < 1) {
                    endPages = true;
                } else {
                    for (User user : caaApiUsers.getData()) {
                        items.add(user);
                        searchDataSet.add(user);
                    }
                    if (items.size() == mAmount) {
                        endPages = true;
                    }
                    adapter.notifyDataSetChanged();
                }


                calling = false;
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void showEmptyLayout() {
        mRecycler.setVisibility(View.GONE);
        listViewEmpty.setVisibility(View.VISIBLE);
    }

    private void removeEmptyLayout() {
        listViewEmpty.setVisibility(View.GONE);
    }

    private boolean iFollowYou(User friend) {
        if (friend.getFollowedByMe().equalsIgnoreCase("1")) {
            return true;
        }
        return false;
    }

    private void sendFollowUser(String userID, final int position) {

        FollowingDataHandler.getInstance().add(userID);
        App.getUrlService().putsUsersFollow(userID, "", new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                searchDataSet.get(position).setFollowedByMe("1");
                adapter.notifyItemChanged(position);
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
        StateMachine.getInstance(getContext().getApplicationContext()).insertEvent(
                new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL,
                        StateMachineEvent.EVENT_TYPE.FOLLOW_USER.ordinal(),
                        Integer.parseInt(userID)
                )
        );
    }


    private void unFollowUser(String userID, final int position) {
        FollowingDataHandler.getInstance().remove(userID);
        App.getUrlService().deleteUsersFollow(userID, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                searchDataSet.get(position).setFollowedByMe("0");
                adapter.notifyItemChanged(position);
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });

    }

    @Override
    public void onSearch(String text) {
        if(searchDataSet == null || adapter == null)
            return;

        String search = text.toLowerCase();
        if (lastSearchLength < search.length()) {
            for (int i = searchDataSet.size() - 1; i > -1; i--) {
                if (searchDataSet.get(i).getFullName().toLowerCase().indexOf(search) == -1) {
                    searchDataSet.remove(i);
                }
            }
        } else {
            searchDataSet.clear();
            if (search.length() > 0) {
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).getFullName().toLowerCase().indexOf(search) != -1)
                        searchDataSet.add(items.get(i));
                }
            } else {
                for (int i = 0; i < items.size(); i++) {
                    searchDataSet.add(items.get(i));
                }
            }
        }
        adapter.notifyDataSetChanged();
        lastSearchLength = search.length();
    }

}
