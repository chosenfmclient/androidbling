package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAApiActivities
 * Description:
 * Created by Dawidowicz Nadav on 5/6/15.
 * History:
 * ***********************************
 */
public class CAAApiActivities {
    public ArrayList<CAAActivity> getData() {
        return data;
    }

    public void setData(ArrayList<CAAActivity> data) {
        this.data = data;
    }

    @SerializedName("data")
    private ArrayList<CAAActivity> data;
}
