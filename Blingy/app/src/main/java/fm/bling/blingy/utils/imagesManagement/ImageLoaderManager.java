package fm.bling.blingy.utils.imagesManagement;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.view.ViewGroup;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;

/**
 * Created by Ben Levi on 1/29/2016.
 */
public class ImageLoaderManager implements ImageLoaderBase
{
    private MemoryCache memoryCache = new MemoryCache();
    private FileCache fileCache;
    private Map<URLImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<URLImageView, String>());

    private ExecutorService executorService;
    private Activity mContext;
    private Paint paint;
    private PorterDuffXfermode mPorterDuffXfermode;
//    private boolean isClosed = false;

    public ImageLoaderManager(Activity context)
    {
        this.mContext = context;
        fileCache = new FileCache(mContext);
        executorService= Executors.newFixedThreadPool(10);
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        mPorterDuffXfermode = new PorterDuffXfermode(PorterDuff.Mode.SRC_IN);
    }

    public void DisplayImage(URLImageView imageView)
    {
        if(imageView.getUrl() == null || imageViews == null)
            return;
        imageViews.put(imageView, imageView.getUrl());

        Bitmap bitmap = memoryCache.get(imageView.getUrl(), imageView.getWidthInternal());
        if(bitmap!=null)
        {
            setImageView(imageView, bitmap);
        }
        else
        {
            if(imageView.getPlaceHolder() != 0)
                imageView.setImageResource(imageView.getPlaceHolder());
            queuePhoto(imageView);
        }
    }

    public void setImageView(URLImageView imageView, Bitmap bitmap)
    {
        imageView.setImageBitmap(bitmap);
        int width = imageView.getLayoutParams().width, height = imageView.getLayoutParams().height;

        if(width == ViewGroup.LayoutParams.WRAP_CONTENT)
        {
            width = bitmap.getWidth();
            imageView.layoutChanged = true;
        }

        if(imageView.isKeepAspectRatioAccordingToWidth() && width != ViewGroup.LayoutParams.MATCH_PARENT)
        {
            float scaleFactor = ((float)bitmap.getWidth()) / ((float)bitmap.getHeight());
            height = (int) (((float)width) / scaleFactor);
            imageView.layoutChanged = true;
        }
        else if(height == URLImageView.NOT_SPECIFIED || height == ViewGroup.LayoutParams.WRAP_CONTENT)
        {
            height = bitmap.getHeight();
            imageView.layoutChanged = true;
        }

        if(imageView.layoutChanged)
        {
            imageView.getLayoutParams().width  = width;
            imageView.getLayoutParams().height = height;
            imageView.requestLayout();
            imageView.layoutChanged = false;
        }

        if(imageView.getImageLoaderListener() != null)
        {
            imageView.getImageLoaderListener().onImageLoaded(imageView);
        }
        if(imageView.hasAnimation()){
            imageView.startAnimation(imageView.getImageAnimation());
        }
        imageView.invalidate();
    }

    private void queuePhoto(URLImageView imageView)
    {
      //  ImageToLoad imageToLoad = new ImageToLoad(imageView);
        executorService.submit(new ImageLoader(imageView));
    }

    public void recycle(URLImageView urlImageView)
    {
        memoryCache.delete(urlImageView.getUrl(), urlImageView.getWidthInternal());
    }

    public void recycleAndRemove(URLImageView urlImageView)
    {
        try {
            recycle(urlImageView);
            imageViews.remove(urlImageView);
        }catch (Throwable throwable) {}
    }

    /**
     *
     * works well with loading fragment be4 games
     *
     * @param urlImageView
     * try to load the bitmap from file cashing if fails display the image regularly
     *
     */
    public void DisplayImageNowLight(final URLImageView urlImageView)
    {
        new Thread()
        {
            public void run()
            {
                Bitmap bitmap = null;
                if(urlImageView != null && urlImageView.getUrl() != null)
                    bitmap = fileCache.getFile(urlImageView.getUrl(), urlImageView.getWidthInternal());

                if (bitmap != null)
                {
                    bitmap = getFilteredBitmap(bitmap, urlImageView);
                    memoryCache.put(urlImageView.getUrl(), urlImageView.getWidthInternal(), bitmap);
                    mContext.runOnUiThread(new BitmapDisplayer(bitmap, urlImageView));
                }
                else DisplayImage(urlImageView);
            }
        }.start();
    }

    /**
     * Task for the queue
     */
//    private class ImageToLoad
//    {
//        String url;
//        URLImageView imageView;
//
//        public ImageToLoad(URLImageView imageView)
//        {
//            this.url = url;
//            this.imageView = imageView;
//        }
//    }

    class ImageLoader implements Runnable
    {
        URLImageView imageView;

        ImageLoader(URLImageView imageView)
        {
            this.imageView = imageView;
        }

        @Override
        public void run()
        {
            try
            {
                if (imageView.isClosed()) return;
                Bitmap bitmap = getBitmap(imageView);

                if (isClosed(imageView, bitmap)) return;
                else if (bitmap == null)
                {
                    if(imageView.getImageLoaderListener() != null)
                        mContext.runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                if(imageView.getImageLoaderListener() != null)
                                    imageView.getImageLoaderListener().onImageLoaded(imageView);
                            }
                        });
                }
                else
                {
                    memoryCache.put(imageView.url, imageView.getWidthInternal(), bitmap);
                    mContext.runOnUiThread(new BitmapDisplayer(bitmap, imageView));
                }
            }
            catch (Throwable th)
            {
                th.printStackTrace();
            }
        }
    }

    private boolean isClosed(URLImageView urlImageView, Bitmap bitmap)
    {
        if(urlImageView.isClosed())
        {
            if(bitmap != null)
                try{bitmap.recycle();}catch(Throwable err){}
            return true;
        }
        else return false;
    }

    private Bitmap getBitmap(URLImageView urlImageView)
    {
        try
        {
            Bitmap bitmap;
            if( ! urlImageView.isDontSaveToFileCache() )
            {
                bitmap = fileCache.getFile(urlImageView.getUrl(), urlImageView.getWidthInternal());
                if (bitmap != null)
                    return bitmap;
            }
            else if( ! urlImageView.isDontUseExisting())
            {
                bitmap = fileCache.getFile(urlImageView.getUrl(), urlImageView.getWidthInternal());
                if (bitmap != null)
                {
                    bitmap = getFilteredBitmap(bitmap, urlImageView);
                    return bitmap;
                }
            }

            if(urlImageView.getUrl().contains(Constants.HTTP)) {
                bitmap = getRemoteBitmap(urlImageView);
            }
            else{
                bitmap = getLocalBitmap(urlImageView);
            }
//            if(urlImageView.isLocalImage())
//            {
//                bitmap = getLocalBitmap(urlImageView);//return getLocalBitmap(urlImageView);
//            }
//            else
//            {
//                bitmap = getRemoteBitmap(urlImageView);//return getRemoteBitmap(urlImageView);
//            }
            if(isClosed(urlImageView, bitmap)) return null;

            bitmap = getScaledBitmap(bitmap, urlImageView);
            bitmap = getFilteredBitmap(bitmap, urlImageView);

            if(!urlImageView.isDontSaveToFileCache())
            {
//                if(saveBitmap(bitmap, urlImageView))
//                    bitmap = fileCache.getFile(urlImageView.getUrl(), urlImageView.getWidthInternal());
                saveBitmap(bitmap, urlImageView);
            }

            return bitmap;
        }
        catch (Throwable ex)
        {
            if(ex instanceof OutOfMemoryError)
            {
                memoryCache.clear();
            }
            return null;
        }
    }

    private Bitmap getLocalBitmap(URLImageView urlImageView) throws Throwable
    {
        Bitmap bitmap;
        try
        {
            bitmap = BitmapFactory.decodeStream(mContext.getContentResolver().openInputStream(android.net.Uri.parse(urlImageView.getUrl())));
        }
        catch (Throwable x)
        {
            bitmap = BitmapFactory.decodeFile(urlImageView.getUrl());
        }
        return bitmap;
    }

    private Bitmap getRemoteBitmap(URLImageView urlImageView) throws Throwable
    {
        URL imageUrl = new URL(urlImageView.getUrl());
        HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
        conn.setConnectTimeout(10000);
        conn.setReadTimeout(10000);
        conn.setInstanceFollowRedirects(true);
        InputStream is = conn.getInputStream();

        Bitmap bitmap = BitmapFactory.decodeStream(is);
        is.close();
        conn.disconnect();

        return bitmap;
    }

    private Bitmap getScaledBitmap(Bitmap temp, URLImageView urlImageView)
    {
        Bitmap bitmap;
        if(urlImageView.getWidthInternal() != URLImageView.NOT_SPECIFIED)
        {
            float scale = ((float) temp.getHeight()) / ((float) temp.getWidth());
            int width = urlImageView.getMaxWidthInternal() != URLImageView.NOT_SPECIFIED &&
                        urlImageView.getMaxWidthInternal() < urlImageView.getWidthInternal() ?
                                    urlImageView.getMaxWidthInternal() : urlImageView.getWidthInternal();
            bitmap = Bitmap.createScaledBitmap(temp, (int) (((float)width) * memoryCache.resoulotionScaleFactor), (int) (((float) width) * scale * memoryCache.resoulotionScaleFactor), true);
            if (bitmap != temp)
                try
                {
                    temp.recycle();
                }
                catch (Throwable x)
                {
                    x.printStackTrace();
                }
        }
        else if(urlImageView.getMaxWidthInternal() != URLImageView.NOT_SPECIFIED && urlImageView.getMaxWidthInternal() < temp.getWidth())
        {
            float scale = ((float)temp.getHeight()) / ((float)temp.getWidth());
            int width = urlImageView.getMaxWidthInternal() < temp.getWidth() ?
                            urlImageView.getMaxWidthInternal() : temp.getWidth();
            bitmap = Bitmap.createScaledBitmap(temp, (int) (((float)width) * memoryCache.resoulotionScaleFactor), (int) (((float) width) * scale * memoryCache.resoulotionScaleFactor), true);
            if (bitmap != temp)
                try
                {
                    temp.recycle();
                }
                catch (Throwable x)
                {
                    x.printStackTrace();
                }
        }
        else if( memoryCache.resoulotionScaleFactor != 1f)
        {
            bitmap = Bitmap.createScaledBitmap(temp, (int) (((float)temp.getWidth()) * memoryCache.resoulotionScaleFactor), (int) (((float) temp.getHeight()) * memoryCache.resoulotionScaleFactor), true);
            if (bitmap != temp)
                try
                {
                    temp.recycle();
                }
                catch (Throwable x)
                {
                    x.printStackTrace();
                }
        }
        else bitmap = temp;

        if(urlImageView instanceof ScaledImageView && ((ScaledImageView)urlImageView).getAspectRatio() != 1f)
        {
            bitmap = getBitmapInAspectRatio(bitmap, ((ScaledImageView)urlImageView).getAspectRatio());
        }
        else if(urlImageView.isFixedSize() && (bitmap.getWidth() != urlImageView.getWidth() || bitmap.getHeight() != urlImageView.getHeight()))
        {
            bitmap = getFitsBitmap(bitmap, urlImageView);
        }
        return bitmap;
    }

    private Bitmap getBitmapInAspectRatio(Bitmap bitmap, float aspectRatio)
    {
        float aspectRatio1 = ((float)bitmap.getWidth()) / ((float)bitmap.getHeight());
        if(aspectRatio1 == aspectRatio)
            return bitmap;

        Bitmap temp;// = Bitmap.createBitmap(urlImageView.getWidth(), urlImageView.getHeight(), Bitmap.Config.ARGB_8888);
        Matrix matrix = new Matrix();
        int newWidth, newHeight;
        int shiftX = 0, shiftY = 0;
        if(aspectRatio1 > aspectRatio) // bitmap is wider than needed
        {
            newHeight = bitmap.getHeight();
            newWidth = (int) (((float)bitmap.getHeight()) * aspectRatio);
            shiftX = - (bitmap.getWidth() - newWidth) / 2;
        }
        else
        {
            newWidth = bitmap.getWidth();
            newHeight = (int) (((float)bitmap.getWidth()) / aspectRatio);
            shiftY = - (bitmap.getHeight() - newHeight) / 2;
        }
        temp = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(temp);
        matrix.postTranslate(shiftX, shiftY);
        canvas.drawBitmap(bitmap, matrix, paint);

        try{ bitmap.recycle(); }catch (Throwable err){}

        return temp;
    }

    private Bitmap getFitsBitmap(Bitmap bitmap, URLImageView urlImageView)
    {
        if(bitmap.getWidth() == urlImageView.getWidth() && bitmap.getHeight() >= urlImageView.getHeight())
            return bitmap;
        else
        {
            Bitmap temp = Bitmap.createBitmap(urlImageView.getWidth(), urlImageView.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(temp);
            Matrix matrix = new Matrix();
            float scale = ((float)urlImageView.getWidth()) / ((float)bitmap.getWidth());
            if(scale <  ((float)urlImageView.getHeight()) / ((float)bitmap.getHeight()))
            {
                scale = ((float)urlImageView.getHeight()) / ((float)bitmap.getHeight());
                matrix.setScale(scale, scale, bitmap.getWidth() / 2, bitmap.getHeight() / 2);
                matrix.postTranslate((bitmap.getWidth() / 2) * (scale - 1), (bitmap.getHeight() / 2)*(scale - 1));
            }
            else
            {
                matrix.setScale(scale, scale, bitmap.getWidth() / 2, bitmap.getHeight() / 2);
                matrix.postTranslate((bitmap.getWidth() / 2) * (scale - 1), (bitmap.getHeight() / 2)*(scale - 1));
            }


            canvas.drawBitmap(bitmap, matrix, paint);

            try{ bitmap.recycle(); }catch (Throwable err){}

            return temp;
        }
    }

    private Bitmap getFilteredBitmap(Bitmap bitmap, URLImageView urlImageView)
    {
        if(urlImageView.isRoundedImage())
        {
            bitmap = getRoundedBitmap(bitmap);
        }

        if(urlImageView.isBlured())
        {
            bitmap = getBluredBitmap(bitmap, urlImageView.getBlurRadius());
        }
        return bitmap;
    }

    //     * @return  true if the bitmap should be reloaded, false - don't reload the bitmap
//     *              always returns false

    /**
     *
     * @param bitmap
     * @param urlImageView
     * @throws Throwable
     */
    private void saveBitmap(Bitmap bitmap, URLImageView urlImageView) throws Throwable
    {
        OutputStream os = new FileOutputStream(fileCache.getFileForIO(urlImageView.getUrl(), urlImageView.getWidthInternal()));

        if (urlImageView.hasAlpha())
        {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
            os.flush();
            os.close();
//            return false;
        }
        else
        {
            bitmap.compress(Bitmap.CompressFormat.JPEG, memoryCache.pictureQuality, os);
            os.flush();
            os.close();
//            try
//            {
//                bitmap.recycle();
//            }
//            catch (Throwable x)
//            {
//                x.printStackTrace();
//            }
//            return false;
        }
    }

    private Bitmap getRoundedBitmap(Bitmap bitmap)
    {
        RectF rectF;
        Rect  rectSrc, rectDest;
        Bitmap roundedBitmap;

        if(bitmap.getHeight() != bitmap.getWidth())
        {
            if(bitmap.getWidth() > bitmap.getHeight())
            {
                int dif = (bitmap.getWidth() - bitmap.getHeight())/2;
                rectSrc=new Rect(dif, 0, bitmap.getHeight() + dif, bitmap.getHeight());
                rectDest = new Rect(0, 0, bitmap.getHeight(), bitmap.getHeight());
                rectF=new RectF(0, 0, bitmap.getHeight(), bitmap.getHeight());
                roundedBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            }
            else
            {
                int dif = (bitmap.getHeight() - bitmap.getWidth())/2;
                rectSrc=new Rect(0, dif, bitmap.getWidth(), bitmap.getWidth() + dif);
                rectDest = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
                rectF=new RectF(0, 0, bitmap.getWidth(), bitmap.getWidth());
                roundedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
            }
        }
        else
        {
            rectDest = rectSrc =new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            rectF=new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight());
            roundedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas can = new Canvas(roundedBitmap);
        can.drawARGB(0, 0, 0, 0);


        can.drawOval(rectF, paint);
        paint.setXfermode(mPorterDuffXfermode);
        can.drawBitmap(bitmap, rectSrc, rectDest, paint);
        paint.setXfermode(null);

        bitmap.recycle();
        return roundedBitmap;
    }

    private class BitmapDisplayer implements Runnable
    {
        private Bitmap bitmap;
        private URLImageView  urlImageView;

        public BitmapDisplayer(Bitmap bitmap, URLImageView urlImageView)
        {
            this.bitmap = bitmap;
            this.urlImageView = urlImageView;
        }

        public void run()
        {
            if(bitmap == null || isClosed(urlImageView, bitmap))
            {
                recycle(urlImageView);
                return;
            }
            try
            {
                setImageView(urlImageView, bitmap);
            }
            catch (Throwable err)
            {
                err.printStackTrace();
            }
        }
    }

    public void clearCache()
    {
        memoryCache.clear();
    }

    public void clearFiles()
    {
        fileCache.clear();
    }

    public synchronized void clearImageViews()
    {
        ArrayList<URLImageView>toDelete = new ArrayList<>();
        for(URLImageView imageView : imageViews.keySet())
        {
            if(imageView.isClosed())
            {
                toDelete.add(imageView);
                memoryCache.delete(imageViews.get(imageView), imageView.getWidthInternal());
            }
        }
        if(toDelete.size() > 0)
        {
            for (URLImageView delImage : toDelete)
            {
                imageViews.remove(delImage);
            }
            toDelete.clear();
        }
    }

    public void close()
    {
//        isClosed = true;
        try{clearCache();}catch (Throwable err){}
        try{imageViews.clear();}catch (Throwable err){}
        try{executorService.shutdownNow();}catch (Throwable err){}
        executorService = null;
        imageViews = null;
    }

    private Bitmap getBluredBitmap1(Bitmap bitmap, int radius)
    {
        Bitmap bluredBitmap = bitmap.copy(bitmap.getConfig(), true);
        bitmap.recycle();
        try
        {
            int w = bluredBitmap.getWidth();
            int h = bluredBitmap.getHeight();

            int[] pix = new int[w * h];
            bluredBitmap.getPixels(pix, 0, w, 0, 0, w, h);

            int wm = w - 1;
            int hm = h - 1;
            int wh = w * h;
            int div = radius + radius + 1;

            int r[] = new int[wh];
            int g[] = new int[wh];
            int b[] = new int[wh];
            int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
            int vmin[] = new int[Math.max(w, h)];

            int divsum = (div + 1) >> 1;
            divsum *= divsum;
            int dv[] = new int[256 * divsum];
            for (i = 0; i < 256 * divsum; i++)
            {
                dv[i] = (i / divsum);
            }

            yw = yi = 0;

            int[][] stack = new int[div][3];
            int stackpointer;
            int stackstart;
            int[] sir;
            int rbs;
            int r1 = radius + 1;
            int routsum, goutsum, boutsum;
            int rinsum, ginsum, binsum;

            for (y = 0; y < h; y++)
            {
                rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
                for (i = -radius; i <= radius; i++)
                {
                    p = pix[yi + Math.min(wm, Math.max(i, 0))];
                    sir = stack[i + radius];
                    sir[0] = (p & 0xff0000) >> 16;
                    sir[1] = (p & 0x00ff00) >> 8;
                    sir[2] = (p & 0x0000ff);
                    rbs = r1 - Math.abs(i);
                    rsum += sir[0] * rbs;
                    gsum += sir[1] * rbs;
                    bsum += sir[2] * rbs;
                    if (i > 0)
                    {
                        rinsum += sir[0];
                        ginsum += sir[1];
                        binsum += sir[2];
                    }
                    else
                    {
                        routsum += sir[0];
                        goutsum += sir[1];
                        boutsum += sir[2];
                    }
                }
                stackpointer = radius;

                for (x = 0; x < w; x++)
                {
                    r[yi] = dv[rsum];
                    g[yi] = dv[gsum];
                    b[yi] = dv[bsum];

                    rsum -= routsum;
                    gsum -= goutsum;
                    bsum -= boutsum;

                    stackstart = stackpointer - radius + div;
                    sir = stack[stackstart % div];

                    routsum -= sir[0];
                    goutsum -= sir[1];
                    boutsum -= sir[2];

                    if (y == 0)
                    {
                        vmin[x] = Math.min(x + radius + 1, wm);
                    }
                    p = pix[yw + vmin[x]];

                    sir[0] = (p & 0xff0000) >> 16;
                    sir[1] = (p & 0x00ff00) >> 8;
                    sir[2] = (p & 0x0000ff);

                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];

                    rsum += rinsum;
                    gsum += ginsum;
                    bsum += binsum;

                    stackpointer = (stackpointer + 1) % div;
                    sir = stack[(stackpointer) % div];

                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];

                    rinsum -= sir[0];
                    ginsum -= sir[1];
                    binsum -= sir[2];

                    yi++;
                }
                yw += w;
            }
            for (x = 0; x < w; x++)
            {
                rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
                yp = -radius * w;
                for (i = -radius; i <= radius; i++)
                {
                    yi = Math.max(0, yp) + x;

                    sir = stack[i + radius];

                    sir[0] = r[yi];
                    sir[1] = g[yi];
                    sir[2] = b[yi];

                    rbs = r1 - Math.abs(i);

                    rsum += r[yi] * rbs;
                    gsum += g[yi] * rbs;
                    bsum += b[yi] * rbs;

                    if (i > 0)
                    {
                        rinsum += sir[0];
                        ginsum += sir[1];
                        binsum += sir[2];
                    }
                    else
                    {
                        routsum += sir[0];
                        goutsum += sir[1];
                        boutsum += sir[2];
                    }

                    if (i < hm)
                    {
                        yp += w;
                    }
                }
                yi = x;
                stackpointer = radius;
                for (y = 0; y < h; y++)
                {
                    // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                    pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                    rsum -= routsum;
                    gsum -= goutsum;
                    bsum -= boutsum;

                    stackstart = stackpointer - radius + div;
                    sir = stack[stackstart % div];

                    routsum -= sir[0];
                    goutsum -= sir[1];
                    boutsum -= sir[2];

                    if (x == 0)
                    {
                        vmin[y] = Math.min(y + r1, hm) * w;
                    }
                    p = x + vmin[y];

                    sir[0] = r[p];
                    sir[1] = g[p];
                    sir[2] = b[p];

                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];

                    rsum += rinsum;
                    gsum += ginsum;
                    bsum += binsum;

                    stackpointer = (stackpointer + 1) % div;
                    sir = stack[stackpointer];

                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];

                    rinsum -= sir[0];
                    ginsum -= sir[1];
                    binsum -= sir[2];

                    yi += w;
                }
            }

            bluredBitmap.setPixels(pix, 0, w, 0, 0, w, h);
        }catch (Throwable err){}
        return bluredBitmap;
    }

    private Bitmap getBluredBitmap(Bitmap inputBitmap, int radius)
    {
        //radius = 15f;
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap.getWidth(), inputBitmap.getHeight(), inputBitmap.getConfig());
        RenderScript rs = RenderScript.create(mContext);
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        theIntrinsic.setRadius(radius);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        inputBitmap.recycle();

        return outputBitmap;
    }

    public boolean runOnUiThread(Runnable runnable)
    {
        if(mContext != null)
        {
            mContext.runOnUiThread(runnable);
            return true;
        }
        else return false;
    }

}
