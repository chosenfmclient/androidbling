package fm.bling.blingy.inviteFriends.listeners;

import fm.bling.blingy.inviteFriends.model.ContactInfo;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/22/16.
 * History:
 * ***********************************
 */
public interface SearchListener
{
    /**
     *
     * @param contactInfo    the user ContactInfo
     * @return  true  if following
     *          false if unfollowing
     */
    boolean followUnfollow(String id);

    void invite(ContactInfo contactInfo);

}