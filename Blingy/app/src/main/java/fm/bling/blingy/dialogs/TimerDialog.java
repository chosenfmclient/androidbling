package fm.bling.blingy.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.RadioButton;

import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.listeners.OnChoiceSelected;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/14/16.
 * History:
 * ***********************************
 */
public class TimerDialog extends Dialog implements View.OnClickListener{

    public final static int CHOICE_0 = 1;
    public final static int CHOICE_3 = 3;
    public final static int CHOICE_5 = 5;

    private OnChoiceSelected mOnChoiceSelected;

    public TimerDialog(Context context, int choice, OnChoiceSelected onChoiceSelected) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.select_timer_dialog);
        mOnChoiceSelected = onChoiceSelected;
        findViewById(R.id.choice_0s).setOnClickListener(this);
        findViewById(R.id.choice_3s).setOnClickListener(this);
        findViewById(R.id.choice_5s).setOnClickListener(this);

        switch (choice){
            case  CHOICE_0:
                ((RadioButton)findViewById(R.id.choice_0s)).setChecked(true);
                break;
            case  CHOICE_3:
                ((RadioButton)findViewById(R.id.choice_3s)).setChecked(true);
                break;
            case  CHOICE_5:
                ((RadioButton)findViewById(R.id.choice_5s)).setChecked(true);
                break;
        }
        show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.choice_0s:
                mOnChoiceSelected.onChoiceSelected(CHOICE_0);
                break;
            default:
            case R.id.choice_3s:
                mOnChoiceSelected.onChoiceSelected(CHOICE_3);
                break;
            case R.id.choice_5s:
                mOnChoiceSelected.onChoiceSelected(CHOICE_5);
                break;
        }
        dismiss();
    }
}
