package fm.bling.blingy.dialogs.comment;

import android.content.Context;
import android.os.Bundle;

import fm.bling.blingy.App;
import fm.bling.blingy.dialogs.flag.FlagBaseDialog;
import fm.bling.blingy.dialogs.adapters.CustomDialogAdapter;
import fm.bling.blingy.dialogs.listeners.FlagDialogListener;
import fm.bling.blingy.dialogs.model.CAAType;
import fm.bling.blingy.record.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/************************************
 * Project: Chosen Android Application
 * FileName: EditCommentDialog
 * Description:
 * Created by Dawidowicz Nadav on 10/01/2016.
 * History: Updated by Oren Zakay.
 *************************************/
public class EditCommentDialog extends FlagBaseDialog {

    private String commentId;
    private String[] flagsConstans = new String[]{"1", "2", "3", "4", "5"};

    public EditCommentDialog(Context context, String commentId, FlagDialogListener flagDialogListener) {
        super(context,flagDialogListener);
        this.commentId = commentId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        for (int i = 0; i < firstDialogList.length; i++) {
            map.put(flagsDialogList[i], flagsConstans[i]);
        }
    }

    @Override
    protected void onStart() {
        adapter = new CustomDialogAdapter(mContext, firstDialogList);
        listView.setAdapter(adapter);
        Utils.setListViewHeightBasedOnChildren(listView);
        super.onStart();
    }

    @Override
    protected void sendReport() {
        String formatedString = "";
        if (map.get(flagType) != null) {
            formatedString = map.get(flagType);
        } else {
            formatedString = "0";
        }
        CAAType type = new CAAType(formatedString);
        App.getUrlService().putFlagComment(commentId, type, new Callback<String>() {
            @Override
            public void success(String call, Response response) {
                dismiss();//handle toast msg in activity
                mFlagDialogListener.onFinishFlagVideoDialog(true);
            }

            @Override
            public void failure(RetrofitError error) {
                dismiss();
                mFlagDialogListener.onFinishFlagVideoDialog(false);

            }
        });
    }

}