//package fm.bling.blingy.record;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.design.widget.TabLayout;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentStatePagerAdapter;
//import android.support.v4.content.ContextCompat;
//import android.support.v4.view.MenuItemCompat;
//import android.support.v4.view.ViewPager;
//import android.support.v7.widget.SearchView;
//import android.support.v7.widget.Toolbar;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.EditText;
//
//import java.util.ArrayList;
//
//import fm.bling.blingy.BaseActivity;
//import fm.bling.blingy.R;
//import fm.bling.blingy.record.fragments.BaseAudioFragment;
//import fm.bling.blingy.record.fragments.SoundEffectFragment;
//import fm.bling.blingy.record.fragments.SoundTrackFragment;
//import fm.bling.blingy.utils.Constants;
//
///**
// * *********************************
// * Project: Chosen Android Application
// * Description:
// * Created by Oren Zakay on 6/13/16.
// * History:
// * ***********************************
// */
//public class AddSoundActivity extends BaseActivity implements SoundTrackFragment.SoundTrackListener,SoundEffectFragment.SoundEffectListener
//{
//    /**
//     * Menu Items
//     **/
//    private MenuItem mSearchMenuItem;
//
//    private SearchView mSearch;
//    private boolean isSearchCollapse = true;
//
//    private int mCurrentPos = 0;
//    private final int SOUNDTRACK_POS = 0;
//    private final int SOUNDEFFECT_POS = 1;
//
//    private Toolbar mToolbar;
//    private TabLayout mTabLayout;
//    private ViewPager mViewPager;
//    private PagerAdapter mPagerAdapter;
//
//    private String songName;
//    private String artistName;
//    private String songUrl;
//    private ArrayList<String> tagsArray = new ArrayList<>();
//    private boolean hasSongName = false;
//    private boolean hasTags = false;
//    private String mCategory = "-1";
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.add_sound_layout);
//
//        //Setting up the navigation icon.
//        mToolbar = getActionBarToolbar();
//        mToolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
//        mToolbar.setNavigationContentDescription("backClose");
//        mToolbar.setTitle("Songs");
//        mToolbar.setNavigationOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                onBackPressed();
//            }
//        });
//
//        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
//        mTabLayout.addTab(mTabLayout.newTab().setText("SONGS"));
//        mTabLayout.addTab(mTabLayout.newTab().setText("SOUND EFFECTS"));
//        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//
//        mViewPager = (ViewPager) findViewById(R.id.pager);
//        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(), mTabLayout.getTabCount());
//        mViewPager.setAdapter(mPagerAdapter);
//        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
//        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                mViewPager.setCurrentItem(tab.getPosition());
//                if (tab.getPosition() == SOUNDTRACK_POS) {
//                    mCurrentPos = SOUNDTRACK_POS;
//                    mToolbar.setTitle("Songs");
//                    if (mSearch != null)
//                        mSearch.setQueryHint("Search Songs");
//                    ((BaseAudioFragment)(mPagerAdapter.instantiateItem(mViewPager, SOUNDEFFECT_POS))).stopMediaPlayer();
//                }
//                else {
//                    mCurrentPos = SOUNDEFFECT_POS;
//                    mToolbar.setTitle("Sound Effects");
//                    if (mSearch != null)
//                        mSearch.setQueryHint("Search Sound Effects");
//                    ((BaseAudioFragment)(mPagerAdapter.instantiateItem(mViewPager, SOUNDTRACK_POS))).stopMediaPlayer();
//                }
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//            }
//        });
//
//        Intent intent = getIntent();
//        if (intent.hasExtra("song_name")) {
//            tagsArray.add(intent.getStringExtra("song_name"));
//        }
//        if (intent.hasExtra(Constants.VIDEO_TYPE_EXTRA)) {
//            this.mCategory = intent.getStringExtra(Constants.VIDEO_TYPE_EXTRA);
//        }
//        if (intent.hasExtra("tags")) {
//            this.hasTags = true;
//            for (int i = 0; i < intent.getStringArrayListExtra("tags").size(); i++) {
//                tagsArray.add(intent.getStringArrayListExtra("tags").get(i));
//            }
//        }
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu)
//    {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.search_menu, menu);
//        mSearchMenuItem = menu.findItem(R.id.action_search);
//        mSearch = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
//        mSearch.setMaxWidth(Integer.MAX_VALUE);
//        mSearch.setQueryHint("Search Soundtrack");
//        ((EditText) mSearch.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setHintTextColor(ContextCompat.getColor(this, R.color.white));
//        mSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                mSearch.clearFocus();
//                ((BaseAudioFragment) (mPagerAdapter.instantiateItem(mViewPager, mViewPager.getCurrentItem()))).onQueryTextSubmit(query);
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                ((BaseAudioFragment) (mPagerAdapter.instantiateItem(mViewPager, mViewPager.getCurrentItem()))).onSearch(newText);
//                return true;
//            }
//        });
//        MenuItemCompat.setOnActionExpandListener(mSearchMenuItem, new MenuItemCompat.OnActionExpandListener() {
//            @Override
//            public boolean onMenuItemActionExpand(MenuItem item) {
//                ((BaseAudioFragment) (mPagerAdapter.instantiateItem(mViewPager, mViewPager.getCurrentItem()))).onSearchCollapse(false);
//                isSearchCollapse = false;
//                return true;
//            }
//
//            @Override
//            public boolean onMenuItemActionCollapse(MenuItem item) {
//                ((BaseAudioFragment) (mPagerAdapter.instantiateItem(mViewPager, mViewPager.getCurrentItem()))).onSearchCollapse(true);
//                isSearchCollapse = true;
//                return true;
//            }
//        });
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle item selection
//        switch (item.getItemId()) {
//            /** Main Record screen menu. **/
//            case R.id.action_search:
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//
//        }
//    }
//
//    @Override
//    public void onSongClicked()
//    {
//        moveToRecord();
//    }
//
//    public void saveSoundtrackResult(String trackName, String artistName, String songUrl) {
//        this.songName = trackName;
//        this.artistName = artistName;
//        this.songUrl = songUrl;
//        if (!hasTags)
//            this.tagsArray.clear();
//    }
//
//    public void saveSoundEffecttrackResult(String effectName, String effectUrl) {
//        this.songName = effectName;
//        this.songUrl = effectUrl;
//        if (!hasTags)
//            this.tagsArray.clear();
//        this.tagsArray.add(effectName);
//        this.artistName = null;
//    }
//
//    private void moveToRecord() {
//        Intent startIntent = new Intent(AddSoundActivity.this, CreateVideoActivity.class);
//        if (songName != null) startIntent.putExtra("song_name", songName);
//        if (artistName != null) startIntent.putExtra("artist_name", artistName);
//        if (songUrl != null) startIntent.putExtra("song_url", songUrl);
//        if (!mCategory.equals("-1")) startIntent.putExtra(Constants.VIDEO_TYPE_EXTRA, mCategory);
//
//        if (mCurrentPos == SOUNDTRACK_POS)
//            if (artistName != null) startIntent.putStringArrayListExtra("tags", tagsArray);
//        if (mCurrentPos == SOUNDEFFECT_POS)
//            if (artistName != null) startIntent.putStringArrayListExtra("tags", tagsArray);
//
//        getRecordPermissions(startIntent);
//    }
//
//    public class PagerAdapter extends FragmentStatePagerAdapter
//    {
//        int mNumOfTabs;
//
//        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
//            super(fm);
//            this.mNumOfTabs = NumOfTabs;
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            switch (position) {
//                case 0:
//                    BaseAudioFragment soundTrackFragment = SoundTrackFragment.getInstance(null);
//                    return soundTrackFragment;
//                case 1:
//                    BaseAudioFragment soundEffectFragment = SoundEffectFragment.getInstance(null);
//                    return soundEffectFragment;
//                default:
//                    return null;
//            }
//        }
//
//        @Override
//        public int getCount()
//        {
//            return mNumOfTabs;
//        }
//
//        @Override
//        public void destroyItem(ViewGroup container, int position, Object object) {
//            super.destroyItem(container, position, object);
//        }
//    }
//
//}
