package fm.bling.blingy.record.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;

import fm.bling.blingy.App;
import fm.bling.blingy.ProcessImage;
import fm.bling.blingy.R;
import fm.bling.blingy.utils.dialogs.LoadingDialog;
import fm.bling.blingy.utils.graphics.WhiteLoadingPlaceHolder;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 13/08/2016.
 * History:
 * ***********************************
 */
public class BlurLoadingDialog extends LoadingDialog
{
    private boolean isClosed;
//    blurredBitmap
    private Bitmap currentBitmap = null;
    private boolean isBlurring = false;
    private Matrix blurredMatrix;
    private Paint blurredPaint;
    private final int blurredMax = 195;
    private float blurredCounter = 0;
    private final int mWidth, mHeight, mYLoc;
    private int majorColor;

//    public BlurLoadingDialog(Context context, int width, int height, int yLoc, Matrix matrix, Bitmap inBitmap)
//    {
//        super(context, true, true);
//        mWidth = width;
//        mHeight = height;
//        mYLoc = yLoc;
//        blurredMatrix = new Matrix(matrix);
//        init(inBitmap);
//    }

    public BlurLoadingDialog(Context context, String title, int width, int height, int yLoc, Matrix matrix, Bitmap inBitmap)
    {
        super(context, title, true, true);
        mWidth = width;
        mHeight = height;
        mYLoc = yLoc;
        blurredMatrix = new Matrix(matrix);
        majorColor = ProcessImage.getInstance().getMajorColor(inBitmap);
        init(inBitmap);
    }

    public void show()
    {
        superShow();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.TOP | Gravity.LEFT;
//        lp.flags = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        getWindow().setAttributes(lp);
    }

//    public Bitmap getBlurredBitmap()
//    {
//        return blurredBitmap;
//    }

    @Override
    protected void init(boolean hideNavigationBar)
    {
        if (widthHeight == 0)
        {
            widthHeight = (int) (250f * (App.WIDTH / 1080)); // portrait 1920 X 1080
        }

        if(App.HEIGHT == 0)
        {
            App.HEIGHT = mContext.getResources().getDisplayMetrics().heightPixels;
        }

        loadingPlaceHolder = new WhiteLoadingPlaceHolder(mContext, widthHeight, widthHeight, (App.WIDTH - widthHeight) / 2, (App.HEIGHT - widthHeight) / 2);

        mainLayout = new FrameLayout(mContext)
        {
            public void draw(Canvas canvas)
            {
                if(isClosed || blurredMatrix == null || currentBitmap == null) return;

                super.draw(canvas);

                if(isBlurring && blurredCounter <= 22)
                {
                    try
                    {
                        blurredCounter += 3f;
                        if(blurredCounter == 25)
                            isBlurring = false;
                        ProcessImage.getInstance().blurBitmap(currentBitmap, 30);
                        blurredPaint.setAlpha(255);
                        canvas.translate(0, mYLoc);
                        canvas.drawBitmap(currentBitmap, blurredMatrix, blurredPaint);
                        canvas.drawColor(majorColor);
//                        adjustBlur();
                    } catch (Throwable err) {}
                }
                else if(!isClosed)
                {
                    try
                    {
                        canvas.translate(0, mYLoc);
                        canvas.drawBitmap(currentBitmap, blurredMatrix, blurredPaint);
                        canvas.drawColor(majorColor);
                        if (mTitle != null)
                            canvas.drawText(mTitle, tittleLocX, tittleLocY, textPaint);

                        loadingPlaceHolder.draw(canvas);
                    } catch (Throwable err) {}
                }

            }
        };

        mainLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mainLayout.setBackgroundColor(ContextCompat.getColor(mContext,R.color.transparent));
        loadingPlaceHolder.attachView(mainLayout);
        setCancelable(false);
        setContentView(mainLayout);
        if(hideNavigationBar)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    private void init(Bitmap inBitmap)
    {
//        blurredMatrix.preTranslate(0, mYLoc);


        if(inBitmap != null)
            currentBitmap = inBitmap.copy(Bitmap.Config.ARGB_8888, false);
//        inBitmap.recycle();

        if(currentBitmap != inBitmap)
            inBitmap.recycle();

//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inScaled = false;
//
//        Bitmap tempBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.game_thumb_placeholder, options);
//        blurredBitmap = Bitmap.createScaledBitmap(tempBitmap, mWidth, mHeight, false); //  might need to switch the width the height
//
//        if(blurredBitmap != tempBitmap)
//            tempBitmap.recycle();

        blurredPaint = new Paint();
        blurredPaint.setAlpha(255);

        isBlurring = true;

        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.dimAmount = 0;
        getWindow().setAttributes(lp);

        show();

        mainLayout.invalidate();
        mainLayout.postInvalidate();
    }

//    private void adjustBlur()
//    {
//        if(blurredCounter < blurredMax && !isClosed)
//        {
//            blurredCounter+=5;
//            blurredPaint.setAlpha((int) blurredCounter);
////            mainLayout.postInvalidate();
////            postInvalidate();
////            try{Thread.sleep(250);}catch (Throwable err){}
//        }
//        else isBlurring = false;
//    }

//    private void getBlurredBitmap(float radius)
//    {
//        Bitmap outputBitmap = Bitmap.createBitmap(currentBitmap.getWidth(), currentBitmap.getHeight(), currentBitmap.getConfig());
//        RenderScript rs = RenderScript.create(mContext);
//        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
//        Allocation tmpIn = Allocation.createFromBitmap(rs, currentBitmap);
//        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
//        theIntrinsic.setRadius(radius);
//        theIntrinsic.setInput(tmpIn);
//        theIntrinsic.forEach(tmpOut);
//        tmpOut.copyTo(outputBitmap);
//
//
//        Bitmap savedForRecycle = blurredBitmap;
//        blurredBitmap = outputBitmap;
////        currentBitmap;
////        currentBitmap = outputBitmap;
//        savedForRecycle.recycle();
//
////        RenderScript.releaseAllContexts();
//
////        return outputBitmap;
//    }

    @Override
    public void dismiss()
    {
        if (currentBitmap != null && !currentBitmap.isRecycled()){
            currentBitmap.recycle();
            currentBitmap = null;
        }
        isClosed = true;
        superDismiss();
    }

}