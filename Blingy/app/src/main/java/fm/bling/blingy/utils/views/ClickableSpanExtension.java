package fm.bling.blingy.utils.views;

import android.text.TextPaint;
import android.text.style.ClickableSpan;

/**
 * Created by Ben on 5/1/16.
 */
abstract public class ClickableSpanExtension extends ClickableSpan
{
    @Override
    public void updateDrawState(TextPaint ds) {

    }

}
