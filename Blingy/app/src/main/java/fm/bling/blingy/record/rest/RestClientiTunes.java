package fm.bling.blingy.record.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import fm.bling.blingy.rest.ItemTypeAdapterFactory;
import fm.bling.blingy.utils.Constants;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 5/3/16.
 * History:
 * ***********************************
 */
public class RestClientiTunes
{

    private final String BASE_URL = Constants.ITUNES_BASE_URL;
    private static RestClientiTunes mRestClientiTunes = null;
    private UrlServiceiTunes apiService;


    public static RestClientiTunes getInstance(){
         if(mRestClientiTunes == null)
             synchronized (RestClientiTunes.class){
                 if(mRestClientiTunes == null)
                     mRestClientiTunes = new RestClientiTunes();
             }
        return mRestClientiTunes;
    }

    private RestClientiTunes() {
        OkHttpClient client = new OkHttpClient();
        client.setReadTimeout(15, TimeUnit.SECONDS);
        client.setConnectTimeout(15, TimeUnit.SECONDS);

        OkClient serviceClient = new OkClient(client);

        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .setConverter(new GsonConverter(gson))
                .setClient(serviceClient)
                .build();

        apiService = restAdapter.create(UrlServiceiTunes.class);
    }

    public UrlServiceiTunes getUrlService()
    {
        return apiService;
    }
}
