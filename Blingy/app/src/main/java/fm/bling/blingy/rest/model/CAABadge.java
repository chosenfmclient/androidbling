package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;



/**
 * Created by nadav on 5/6/15.
 */
public class CAABadge {

    @SerializedName("badge_id")
    private String badgeId;

    @SerializedName("status")
    private String status;

    @SerializedName("progress")
    private String progress;

    @SerializedName("date")
    private String date;

    @SerializedName("image")
    private String image;

    @SerializedName("image_share")
    private String imageShare;

    @SerializedName("name")
    private String name;

    @SerializedName("type")
    private String type;

    @SerializedName("category")
    private String category;

    @SerializedName("text")
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getBadgeId() {
        return badgeId;
    }

    public void setBadgeId(String badgeId) {
        this.badgeId = badgeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageShare() {
        return imageShare;
    }

    public void setImageShare(String imageShare) {
        this.imageShare = imageShare;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}