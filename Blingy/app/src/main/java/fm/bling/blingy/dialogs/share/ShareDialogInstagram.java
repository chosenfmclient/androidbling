package fm.bling.blingy.dialogs.share;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.math.BigInteger;

import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.share.workers.SaveVideoFromURLtoExternal;
import fm.bling.blingy.editVideo.VideoPlayerActivity;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.views.ShareView;
import fm.bling.blingy.videoHome.model.CAAVideo;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 3/29/16.
 * History:
 * ***********************************
 */
public class ShareDialogInstagram extends ShareDialogBase {

    private SaveVideoFromURLtoExternal mSaveFileWorker;
    protected String mFramePath;

    public ShareDialogInstagram(Activity activity, ShareDialogCallbackSetter dialogCallbackSetter, CAAVideo video){
        super(activity, dialogCallbackSetter, video);
        init(activity);
    }

    public ShareDialogInstagram(Activity activity, ShareDialogCallbackSetter dialogCallbackSetter, String songName, String videoID, String userID, boolean isDuet){
        super(activity, dialogCallbackSetter,userID, isDuet);
        this.songName = songName;
        this.videoID = videoID;
        init(activity);
    }

    private void init(Activity activity){
        mInstagramItem = new ShareView(activity.getApplicationContext(), R.drawable.large_share_instagram, R.string.share_to_instagram);

        mFirstLine.addView(mInstagramItem, 0);

        mInstagramItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInstagramClicked = true;
                chosenPlatform = INSTAGRAM;
                onShareProccessStart();
                TrackingManager.getInstance().sharePlatform(getNameByActivity(), ShareDialogInstagram.this.videoID, artistName, ShareDialogInstagram.this.songName, shareID, INSTAGRAM);
                if(checkAppInstalled(mContext.getResources().getString(R.string.instagram_package_name))) {
                    if (writeExternalStoragePermissions()) {
                        if(!isFileExsist(getFileName())){
                            if(isAfterRecording){
                                if(mContext instanceof VideoPlayerActivity)
                                    ((VideoPlayerActivity)mContext).renderVideoForInstagrm();
                            }
                            else{
                                mSaveFileWorker = new SaveVideoFromURLtoExternal(mVideo.getInstagramUrl(), mInstagramFilePath, isPhoto, mContext, ShareDialogInstagram.this, true);
                                mSaveFileWorker.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        }
                        else{
                            onInstagramClicked();
                        }
                    }
                }
                else{
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.not_downloaded_instagram), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onInstagramClicked() {
        final String type;
        if(isPhoto)
            type = "image/*";
        else
            type = "video/*";

        shareToInstagram(type);
    }

    @Override
    public void onFacebookClicked() {

    }

    @Override
    public void onTwitterClicked() {

    }

    @Override
    public void onSmsClicked() {

    }


    @Override
    public void onMailClicked() {

    }

    public boolean getIsInstagramClicked(){
        return isInstagramClicked;
    }

    public void callInstagramClicked(){
        mInstagramItem.callOnClick();
    }

    public String getFileName(){
            String extra;
//            if (isPhoto) {
//                extra = "blingy_pic_instagram_" + String.format("%040x", new BigInteger(1, videoID.getBytes())) + ".jpg";
//            } else {
                extra = "blingy_vid_instagram_" + String.format("%040x", new BigInteger(1, videoID.getBytes())) + ".mp4";
//            }
        mInstagramFilePath = Environment.getExternalStorageDirectory() + File.separator + extra;
        return mInstagramFilePath;
    }

    @Override
    public void onStop() {
        super.onStop();
        this.mSaveFileWorker = null;
        this.isInstagramClicked = false;
        this.mInstagramItem.removeAllViews();
        this.mInstagramItem.destroyDrawingCache();
        this.mInstagramItem = null;
    }

}
