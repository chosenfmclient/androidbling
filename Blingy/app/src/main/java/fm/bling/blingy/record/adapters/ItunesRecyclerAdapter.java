package fm.bling.blingy.record.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import fm.bling.blingy.R;
import fm.bling.blingy.record.rest.model.CAAItunesMusicElement;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.ProgressBarView;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 5/3/16.
 * History:
 * ***********************************
 */
public class ItunesRecyclerAdapter extends RecyclerView.Adapter<ItunesRecyclerAdapter.DataObjectHolder> {
    private ArrayList<CAAItunesMusicElement> mDataset;
    private ImageLoaderManager mImageLoaderManager;
    private SparseArray<DataObjectHolder> mHolders;
    private View.OnClickListener mItemClickListener;
    private View.OnClickListener mPlayPauseClickListener;

    public ItunesRecyclerAdapter(ArrayList<CAAItunesMusicElement> myDataset, ImageLoaderManager imageLoaderManager,
                                 View.OnClickListener itemClickListener , View.OnClickListener playPauseClickListener) {
        mDataset = myDataset;
        mImageLoaderManager = imageLoaderManager;
        mHolders = new SparseArray<>();
        mItemClickListener = itemClickListener;
        mPlayPauseClickListener = playPauseClickListener;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewIndex) {
        DataObjectHolder dataObjectHolder = mHolders.get(viewIndex);
        if(dataObjectHolder == null) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sound_track_list_item, parent, false);
            dataObjectHolder = new DataObjectHolder(view);
            mHolders.put(viewIndex, dataObjectHolder);
        }
        return dataObjectHolder;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        CAAItunesMusicElement itunesMusicElement = mDataset.get(position);
        holder.mItem.setTag(itunesMusicElement);
        holder.mPlayPause.setTag(holder.mItem);

        holder.mArtistName.setText(itunesMusicElement.getArtistName());
        holder.mSongName.setText(itunesMusicElement.getTrackName());

        holder.mArtistPic.setId(position);
        holder.mArtistPic.setUrl(itunesMusicElement.getArtworkUrl100());
        holder.mArtistPic.setKeepAspectRatioAccordingToWidth(true);
        mImageLoaderManager.DisplayImage(holder.mArtistPic);
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    @Override
    public void onViewRecycled(DataObjectHolder holder) {
//        if(holder.mArtistPic != null)
//            mImageLoader.recycle(holder.mArtistPic);
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        private LinearLayout mItem;
        private TextView mArtistName;
        private TextView mSongName;
        private ScaledImageView mArtistPic;
        private ImageView mPlayPause;
        private ProgressBarView mProgressBar;
//        TextView mPosition;

        public DataObjectHolder(View itemView) {
            super(itemView);
            mItem = (LinearLayout)itemView.findViewById(R.id.song_item);
            mArtistPic = (ScaledImageView) itemView.findViewById(R.id.artist_pic);
            mPlayPause = (ImageView) itemView.findViewById(R.id.play_pause);
            mArtistName = (TextView)itemView.findViewById(R.id.artist_name);
            mSongName = (TextView)itemView.findViewById(R.id.song_name);
            mProgressBar = (ProgressBarView)itemView.findViewById(R.id.progress_bar);

            mArtistPic.setAnimResID(R.anim.fade_in);

            mItem.setOnClickListener(mItemClickListener);
            mPlayPause.setOnClickListener(mPlayPauseClickListener);

//            mPosition.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //Play music
//                    isPlay = true;
//                    mPlayPause.setImageResource(R.drawable.ic_pause_circle_outline_white_24dp);
//                }
//            });
        }
    }

    public void clearData() {
        int size = this.mDataset.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.mDataset.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void close() {
        for(int i = 0 ;i < mHolders.size(); i++)
            mHolders.get(i).mArtistPic.close();
        mDataset = null;
        mImageLoaderManager = null;
    }
//    public void closeBitmaps(){
//        for(int i = 0 ;i < mHolders.size(); i++)
//            mHolders.get(i).mArtistPic.close();
//    }
}