package fm.bling.blingy.registration;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;

import java.util.concurrent.TimeUnit;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.registration.rest.model.CAAGoogleToken;
import fm.bling.blingy.registration.rest.model.CAAToken;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 3/28/16.
 * History:
 * ***********************************
 */
public class CAAGooglePlusLogin {

    private static GoogleApiClient mGoogleApiClient;

    public static void GooglePlusLogin(final CAALoginListener cAALogin) {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PROFILE))
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestScopes(new Scope(Scopes.PLUS_ME))
                .requestId()
                .requestProfile()
                .requestEmail()
                .requestIdToken(App.getAppContext().getResources().getString(R.string.server_client_id))
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(App.getAppContext())
                .addApi(Plus.API)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        OptionalPendingResult<GoogleSignInResult> optionalPendingResult = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);

        if (optionalPendingResult.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            GoogleSignInResult result = optionalPendingResult.get();
            String token = result.getSignInAccount().getIdToken();

            SharedPreferencesManager.getEditor().putString(Constants.GOOGLE_TOKEN_ID, token).commit();
            CAAGoogleToken googleToken = new CAAGoogleToken(token, Constants.APP_ID, Constants.APP_SECRET);
            googleToken.setAuto(CAALogin.isAuto);
            App.getUrlService().postGoogleToken(googleToken , new Callback<CAAToken>() {
                @Override
                public void success(CAAToken caaToken, Response response) {
                    updateGooglePlusUserSingleton(caaToken);
                    TrackingManager.getInstance().loginSubmit("google+");
                    cAALogin.didFinishLogin(true,1);
                }

                @Override
                public void failure(RetrofitError error) {
                    TrackingManager.getInstance().loginFailed(++CAALogin.failedLogins);
                    if(error.getResponse() != null)
                        cAALogin.didFinishLogin(false, error.getResponse().getStatus());
                }
            });

        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.
            optionalPendingResult.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult, cAALogin);
                }
            }, 10, TimeUnit.SECONDS);
            mGoogleApiClient.connect(GoogleApiClient.SIGN_IN_MODE_OPTIONAL);

        }
    }

    private static void handleSignInResult(GoogleSignInResult result,final CAALoginListener cAALogin) {
        if (result.isSuccess()) {
            CAAUserDataSingleton.getInstance().setSentGcmToken(false);
//            final GoogleSignInAccount acct = result.getSignInAccount();
            CAAGoogleToken googleToken = new CAAGoogleToken(result.getSignInAccount().getIdToken(),Constants.APP_ID, Constants.APP_SECRET);
            googleToken.setAuto(CAALogin.isAuto);

            App.getUrlService().postGoogleToken(googleToken , new Callback<CAAToken>() {
                @Override
                public void success(CAAToken caaToken, Response response) {
                    updateGooglePlusUserSingleton(caaToken);
                    cAALogin.didFinishLogin(true,1);
                }

                @Override
                public void failure(RetrofitError error) {
                    if(error.getResponse() != null)
                        cAALogin.didFinishLogin(false, error.getResponse().getStatus());

                }
            });
        }
        else{
            //Failed to connect.
            cAALogin.didFinishLogin(false, 1);
        }
    }

    static void updateGooglePlusUserSingleton(CAAToken caaToken){
        CAAUserDataSingleton.getInstance().setAccessToken(caaToken.getAccessToken());
        CAAUserDataSingleton.getInstance().setAccessExpireTime(caaToken.getExpiresIn());
        CAAUserDataSingleton.getInstance().setUserId(caaToken.getUserId());
        CAAUserDataSingleton.getInstance().setFirstName(caaToken.getFirstName());
        CAAUserDataSingleton.getInstance().setLastName(caaToken.getLastName());
        CAAUserDataSingleton.getInstance().setPhotoUrl(caaToken.getPhoto());
        CAAUserDataSingleton.getInstance().setAccountType(Constants.GOOGLE_PLUS_ACC);

        SharedPreferencesManager.getEditor().putString(Constants.ACCOUNT_TYPE, Constants.GOOGLE_PLUS_ACC).commit();
    }
}
