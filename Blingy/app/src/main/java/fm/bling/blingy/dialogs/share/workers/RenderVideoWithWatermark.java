package fm.bling.blingy.dialogs.share.workers;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.share.workers.listeners.onWorkerFinishedListener;
import fm.bling.blingy.record.CameraRecorder3;
import fm.bling.blingy.utils.FFMPEG;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 12/6/16.
 * History:
 * ***********************************
 */
public class RenderVideoWithWatermark extends AsyncTask<Void,Void,Boolean> {
    private Context mContext;
    private String mUrl;
    private String mVideoPath;
    private onWorkerFinishedListener mOnWorkerFinishedListener;
    private String logoPath;
    private String pathWithWaterMark;
    private String platform = "";
    private boolean isDuet = false;
    private final int mRatioSize = 480;

    private final String MUSICALLY = "musically";


    public RenderVideoWithWatermark(Context context, boolean isDuet, String url, String videoPath, String pathWithWaterMark, onWorkerFinishedListener onWorkerFinishedListener) {
        this.mContext = context;
        this.mUrl = url;
        this.mVideoPath = videoPath;
        this.pathWithWaterMark = pathWithWaterMark;
        this.mOnWorkerFinishedListener = onWorkerFinishedListener;
        this.isDuet = isDuet;

        if(!new File(Environment.getExternalStorageDirectory() + "/Blingy/").exists())
            new File(Environment.getExternalStorageDirectory() + "/Blingy/").mkdir();

        if(!new File(Environment.getExternalStorageDirectory() + "/Blingy/videos/").exists())
            new File(Environment.getExternalStorageDirectory() + "/Blingy/videos/").mkdir();

    }

    public RenderVideoWithWatermark(Context context, boolean isDuet, String platform, String url, String videoPath, String pathWithWaterMark, onWorkerFinishedListener onWorkerFinishedListener) {
        this.mContext = context;
        this.platform = platform;
        this.mUrl = url;
        this.mVideoPath = videoPath;
        this.pathWithWaterMark = pathWithWaterMark;
        this.mOnWorkerFinishedListener = onWorkerFinishedListener;
        this.isDuet = isDuet;

        if(!new File(Environment.getExternalStorageDirectory() + "/Blingy/").exists())
            new File(Environment.getExternalStorageDirectory() + "/Blingy/").mkdir();

        if(!new File(Environment.getExternalStorageDirectory() + "/Blingy/videos/").exists())
            new File(Environment.getExternalStorageDirectory() + "/Blingy/videos/").mkdir();

    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        boolean result;
        try {
            generateWaterMark();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(mUrl != null && !mUrl.isEmpty()){
           result = DownloadFile(mUrl);
        }
        else{
            result = saveVideo(false);
        }
        return result;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        this.mOnWorkerFinishedListener.onWorkerFinished(result, pathWithWaterMark);
    }

    public boolean DownloadFile(String fileURL) {
        boolean result = true;
        try {
            File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "Blingy");
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    result = false;
                }
            }

            InputStream inputStream = null;
            URL url = null;
            HttpURLConnection conn = null;
            boolean fromGoogle = false;
            if (fileURL.startsWith("content://com.google.android.apps.photos.content")) {
                inputStream = mContext.getContentResolver().openInputStream(Uri.parse(fileURL));
                fromGoogle = true;
            }
            else{
                url = new URL(fileURL);
                conn = (HttpURLConnection)url.openConnection();
                conn.setConnectTimeout(20000);
                conn.setReadTimeout(20000);
                conn.setInstanceFollowRedirects(true);
                inputStream = conn.getInputStream();
            }
            String tempFile;
            if(fromGoogle){
                tempFile = (mVideoPath = App.TEMP_MEDIA_FOLDER + "snap_temp.mp4");
            }
            else{
                tempFile = platform.equals(MUSICALLY) ? App.TEMP_MEDIA_FOLDER + "musically_temp.mp4" : pathWithWaterMark;
            }
            DataInputStream dataInputStream = new DataInputStream(inputStream);
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(tempFile));

            byte[]buffer = new byte[1024 * 10];
            int count;

            while(true)
            {
                count = dataInputStream.read(buffer);
                if(count == -1)
                    break;
                dataOutputStream.write(buffer, 0, count);
            }

            dataOutputStream.flush();
            dataOutputStream.close();
            dataInputStream.close();
            url = null;
            conn = null;

            if(fromGoogle)
                result = saveVideo(false);
            else
                result  = saveVideo(true);

        } catch (Exception e) {
            result = false;

        }
        return result;
    }

    private boolean saveVideo(boolean skipFirstProcess) {
        PowerManager powerManager = (PowerManager) mContext.getSystemService(Activity.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "VK_LOCK");
        wakeLock.acquire();

        /** First process **/
        String firstOutPath = platform.equals(MUSICALLY) ? App.TEMP_MEDIA_FOLDER + "musically_temp.mp4" : pathWithWaterMark;
        if(!skipFirstProcess) {
            final int maxBitrate = -1; // was 15496k, counts in k- kilo bytes -> -1 for default
            try {
                FFMPEG.getInstance(mContext).renderWithImage(mVideoPath, logoPath, firstOutPath, "0", "main_h-overlay_h", maxBitrate);
            } catch (Throwable err) {
                err.printStackTrace();
                return false;
            }
        }

        /** Second process if needed **/
        switch (platform){
            case MUSICALLY:
                try{
                    FFMPEG.getInstance(mContext).createVideoWithBackground(firstOutPath, createBlackBackground(firstOutPath) ,pathWithWaterMark);
                }
                catch (Throwable throwable){
                    throwable.printStackTrace();
                    return false;
                }
                break;
        }

        if (wakeLock.isHeld())
            wakeLock.release();
        return true;
    }

    private void generateWaterMark() throws IOException
    {

        Bitmap bm = BitmapFactory.decodeResource(mContext.getResources(), isDuet ? R.raw.watermark_duet : R.raw.watermark1);

        if(bm == null)
            return;

        float scaleFactor = ((float)bm.getWidth()) / ((float)bm.getHeight());
        float scaledWidth  = ((float)(mRatioSize * 0.25));

        logoPath = isDuet ? App.MAIN_FOLDER_PATH + "/duet_watermark_" + scaledWidth : App.MAIN_FOLDER_PATH + "/watermark_" + scaledWidth;
        File file = new File(logoPath);
        if(file.exists()){
            if(!bm.isRecycled())
               bm.recycle();
            return;
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        Bitmap scaled = Bitmap.createScaledBitmap(bm, (int) scaledWidth, (int) (scaledWidth / scaleFactor), true);

        if(bm != scaled)
            bm.recycle();

        FileOutputStream out = null;
        try
        {
            out = new FileOutputStream(file);
            scaled.compress(Bitmap.CompressFormat.PNG, 100, out);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        scaled.recycle();
    }

    private String createBlackBackground(String videoPath)
    {
        String path = "";
        int width = 360;
//        int height = 240;
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(videoPath);
            int orientation = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));
            if(orientation == 90 || orientation == 270){
                width  = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
//                height  = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
            }
            else {
                width  = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
//                height  = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
            }
        }
        catch (Throwable throwable){
            throwable.printStackTrace();
            if(FFMPEG.getInstance(mContext).isLiteVideo())
                width = 320;
        }

        float scaleFactor = ((float)16) / ((float)9);
        Bitmap bm = Bitmap.createBitmap(width, (int)(width * scaleFactor), Bitmap.Config.ARGB_8888);

        if(bm == null)
            return "";

        Canvas canvas = new Canvas(bm);
        canvas.drawColor(Color.BLACK);

        path = App.MAIN_FOLDER_PATH  + "/blackbackground_" +  scaleFactor + ".jpg";


        File file = new File(path);
//        if(file.exists()) {
//            if(!bm.isRecycled())
//                bm.recycle();
//            return path;
//        }

        FileOutputStream out = null;
        try
        {
            out = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, out);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        bm.recycle();

        return path;
    }


}
