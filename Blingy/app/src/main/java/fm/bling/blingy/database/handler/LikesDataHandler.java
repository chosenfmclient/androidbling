package fm.bling.blingy.database.handler;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import fm.bling.blingy.database.contract.LikesContract;
import fm.bling.blingy.database.model.ContentValueWrapper;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 12/02/17.
 * History:
 * ***********************************
 */
public class LikesDataHandler extends BaseHandler<String> {

    private final String TAG = "LikesDataHandler";
    private static LikesDataHandler mLikesDataHandler;
    private static HashMap<String,Integer> mLikesMap = new HashMap<>();

    private LikesDataHandler(){

    }

    public static LikesDataHandler getInstance(){
        if(mLikesDataHandler == null)
            mLikesDataHandler = new LikesDataHandler();
        return mLikesDataHandler;
    }

    @Override
    public long add(@NonNull String video_id) {
        ContentValueWrapper cvw = new ContentValueWrapper();
        cvw.setTableName(LikesContract.TABLE_NAME);
        cvw.addStringValue(LikesContract.COLUMN_VIDEO_ID, video_id);
        mLikesMap.put(video_id, 1);
        return DataBaseManager.insertToDB(cvw);
    }

    @Override
    public long addMany(@NonNull ArrayList<String> likes) {
        if(likes.size() > 0) {
            StringBuilder query = new StringBuilder(LikesContract.INSERT_MANY);
            Iterator<String> iterator = likes.iterator();
            while (iterator.hasNext()) {
                String video_id = iterator.next();
                mLikesMap.put(video_id, 1);
                query.append("(").append(video_id).append(")").append(iterator.hasNext() ? ",\n" : ";");
            }

            DataBaseManager.excQuery(query.toString());
            return likes.size();
        }
        return 0;
    }

    @Override
    public boolean remove(@NonNull String video_id) {
        ContentValueWrapper cvw = new ContentValueWrapper();
        cvw.setTableName(LikesContract.TABLE_NAME);
        cvw.setWhereClause(LikesContract.COLUMN_VIDEO_ID + " LIKE \'" + video_id + "\'");
        mLikesMap.remove(video_id);
        return DataBaseManager.deleteFromDB(cvw);
    }

    @Override
    public boolean update(@NonNull String video_id) {
        ContentValueWrapper cvw = new ContentValueWrapper();
        cvw.addStringValue(LikesContract.COLUMN_VIDEO_ID, video_id);
        return DataBaseManager.updateDB(cvw);
    }


    @Override
    public String get(@NonNull String video_id) {
        if(mLikesMap.get(video_id) != null)
            return video_id;
        else
            return DataBaseManager.getLike(video_id);
    }

    @Override
    public ArrayList<String> getAll(String filter) {
        ArrayList<String> ids = DataBaseManager.getAllLikes(filter);
        for (String id : ids) {
            mLikesMap.put(id, 1);
        }
        return ids;
    }

    public boolean contains(String video_id){
        return mLikesMap.containsKey(video_id);
    }

}
