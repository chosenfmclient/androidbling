package fm.bling.blingy.record.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import fm.bling.blingy.App;
import fm.bling.blingy.R;

/**
 * *********************************
 * Project: Chosen Android Application. All rights reserved.
 * Description:
 * Created by Ben Levi on 3/13/16.
 * History:
 * ***********************************
 */
public class ImageViewExtension extends ImageView
{
    private Bitmap   forground       = null;
    private boolean  isForgroundSet  = false;
    private Matrix matrix            = null;
    private int mWidth = -1, mHeight = -1;
    private boolean imageResourceSet;

    public ImageViewExtension(Context context)
    {
        super(context);
        setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    public ImageViewExtension(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    public ImageViewExtension(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    @Override
    public void draw(Canvas canvas)
    {
        super.draw(canvas);
        if(isForgroundSet)
        {
            canvas.drawBitmap(forground, matrix, null);
        }
    }

    public void onMeasure(int width, int height)
    {
        if(mWidth == -1)
            setMeasuredDimension(width, height);
        else
        {
            setMeasuredDimension(mWidth, mHeight);
        }
    }

    @Override
    public void setImageBitmap(Bitmap imageBitmap)
    {
        if(!imageResourceSet && getDrawable() != null && getDrawable() instanceof BitmapDrawable)
        {
            try{ ((BitmapDrawable)getDrawable()).getBitmap().recycle();}catch (Throwable err){}
        }
        super.setImageBitmap(imageBitmap);
        if(imageBitmap == null)
            return;
        mWidth = App.WIDTH;
        float scaleFactor = ((float) imageBitmap.getWidth()) / ((float) imageBitmap.getHeight());
        mHeight = (int) (((float)mWidth) / scaleFactor);
        if(App.HEIGHT < mHeight)
        {
            mHeight = App.HEIGHT;
            mWidth  = (int) (((float)mHeight) * scaleFactor);
        }
        imageResourceSet = false;
        requestLayout();
    }

    @Override
    public void setImageResource(int resId)
    {
        super.setImageResource(resId);
        imageResourceSet = true;
    }

    @Override
    public void setImageDrawable(Drawable imageDrawable)
    {
        super.setImageDrawable(imageDrawable);
        imageResourceSet = false;
    }

    public void setForgroundInternal(Bitmap iForground)
    {
        if(this.forground != null)
        {
            this.forground.recycle();
        }
        this.forground = iForground;
        if(iForground == null)
        {
            isForgroundSet = false;
            return;
        }

        if(matrix == null)
        {
            matrix = new Matrix();
        }
        matrix.setRectToRect(new RectF(0, 0, iForground.getWidth(), iForground.getHeight()), new RectF(0, 0, mWidth, mHeight), Matrix.ScaleToFit.FILL);
        isForgroundSet = true;
        invalidate();
    }

    public Bitmap getForgroundBitmap()
    {
        return forground;
    }

}