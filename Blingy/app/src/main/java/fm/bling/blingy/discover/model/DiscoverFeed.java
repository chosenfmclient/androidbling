package fm.bling.blingy.discover.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import fm.bling.blingy.record.rest.model.CAAItunesMusicElement;
import fm.bling.blingy.videoHome.model.CAAVideo;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 10/10/16.
 * History:
 * ***********************************
 */
public class DiscoverFeed {

    @SerializedName("trending_videos")
    ArrayList<CAAVideo> trendingVideos;

    @SerializedName("featured_clip")
    CAAItunesMusicElement featuredClip;

    public ArrayList<CAAVideo> getTrendingVideos() {
        return trendingVideos;
    }

    public CAAItunesMusicElement getFeaturedClip() {
        return featuredClip;
    }

}
