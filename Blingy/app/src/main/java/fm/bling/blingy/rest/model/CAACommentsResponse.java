package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import fm.bling.blingy.videoHome.model.CAAVideo;

/************************************
 * Project: Chosen Android Application
 * FileName: CAACommentsResponse
 * Description:
 * Created by Dawidowicz Nadav on 10/01/2016.
 * History:
 *************************************/
public class CAACommentsResponse {

    @SerializedName("total_comments")
    private String totalComments;

    @SerializedName("data")
    private ArrayList<CAACommentParams> data;

    @SerializedName("video")
    private CAAVideo video;

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public ArrayList<CAACommentParams> getData() {
        return data;
    }

    public void setData(ArrayList<CAACommentParams> data) {
        this.data = data;
    }

    public CAAVideo getVideo() {
        return video;
    }

    public void setVideo(CAAVideo video) {
        this.video = video;
    }
}
