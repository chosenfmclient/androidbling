package fm.bling.blingy.profile;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.BuildConfig;
import fm.bling.blingy.MenuActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.ScrollToTopBaseActivity;
import fm.bling.blingy.database.handler.FollowingDataHandler;
import fm.bling.blingy.dialogs.listeners.NetworkChangedListener;
import fm.bling.blingy.discover.adapters.DiscoverRecyclerAdapter;
import fm.bling.blingy.inviteFriends.InviteFriendsActivity;
import fm.bling.blingy.profile.model.UserProfile;
import fm.bling.blingy.record.utils.GridSpacingItemDecoration;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.stateMachine.PermissionListener;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.AdaptersDataTypes;
import fm.bling.blingy.videoHome.VideoHomeActivity;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;
import fm.bling.blingy.utils.imagesManagement.views.WhiteProgressBarView;
import fm.bling.blingy.videoHome.model.CAAVideo;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 9/25/16.
 * History:
 * ***********************************
 */
public class ProfileActivity extends ScrollToTopBaseActivity implements PermissionListener, NetworkChangedListener {

    private final int EDIT_PROFILE_CODE = 77;
    private final int REQ_VIDEO_CHANGE = 78;
    
    /**
     * Main Views
     */
    private View mHeader;
    private URLImageView mBluredImage;
    private ScaledImageView mUserPic;
    private ImageView mBackOrInvite;
    private ImageView mMoreButton;
    private TextView mTitle;
    private LinearLayout mSocialLinks;
    private TextView mVideosNum;
    private TextView mFollowingNum;
    private TextView mFollowersNum;
    private LinearLayout mEditOrFollow;
    private TextView mEditOrFollowText;
    private ImageView mFollowIcon;
    private TextView mNoVideos;
    private WhiteProgressBarView mProgressBarView;
    private View mWhiteLine;

    /** Recycler **/
    private RecyclerView mRecycler;
    private DiscoverRecyclerAdapter mProfileRecyclerAdapter;
    private GridLayoutManager mGridLayoutManager;
    private boolean listInitialized = false;
    private boolean calling = false;
    private boolean endReached = false;
    private int previousLastItem = 0;
    private int pageNumber = 0;
    private int noData = 0;

    private User mUser;
    private String mUserID;
    private ArrayList<CAAVideo> items = new ArrayList<>(1);

    private Animation clickAnimation;

    private boolean isFirst = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_layout);
        mHeader = getLayoutInflater().inflate(R.layout.profile_header, null);
        getWindow().setBackgroundDrawable(null);
        init();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_20));
            mHeader.findViewById(R.id.header_container).setPadding(0, App.STATUS_BAR_HEIGHT, 0, 0);
        }
        if(getIntent().hasExtra(Constants.USER_ID)){
            mUserID =  getIntent().getStringExtra(Constants.USER_ID).equalsIgnoreCase(CAAUserDataSingleton.getInstance().getUserId())?  Constants.ME : getIntent().getStringExtra(Constants.USER_ID);
        }
        else{
            mUserID = Constants.ME;
        }

        if(mUserID.equalsIgnoreCase(Constants.ME)) {
            TrackingManager.getInstance().pageView(EventConstants.MY_PROFILE_PAGE);
            mBackOrInvite.setImageResource(R.drawable.ic_profile_invite_friends_24dp);
            mBackOrInvite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getContactsPermissions(ProfileActivity.this))
                        startActivity(new Intent(ProfileActivity.this, InviteFriendsActivity.class));
                }
            });
        }
        else{
            TrackingManager.getInstance().pageView(EventConstants.PROFILE_PAGE);
            mMoreButton.setVisibility(View.INVISIBLE);
            mBackOrInvite.setImageResource(R.drawable.ic_nav_back_24dp);
            mBackOrInvite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        getProfile(mUserID);

        System.out.println(">>>>>>done here");
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!isFirst && mRecycler != null && mProfileRecyclerAdapter != null)
        {
            mProfileRecyclerAdapter.notifyDataSetChanged();
//            mNestedScrollView.smoothScrollTo(0, scrollYPos);
        }
        if(!isFirst && mUserPic != null)
        {
            mUserPic.setImageLoader(getMultiImageLodaer());
            mBluredImage.setBackgroundResource(R.drawable.game_thumb_placeholder);
            mBluredImage.setImageLoader(getMultiImageLodaer());

        }

        if(mUserID != null && !mUserID.equalsIgnoreCase(Constants.ME))
            setupFollowButton();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isFirst = false;
        if(clickAnimation != null)
            clickAnimation = null;
    }

    @Override
    public void scrollToTop() {
        try {
            if (mRecycler != null)
                mRecycler.smoothScrollToPosition(0);
        }catch (Throwable throwable) { throwable.printStackTrace(); }
    }

    private void init()
    {
        mBluredImage = (URLImageView)mHeader.findViewById(R.id.blured_image);
        mUserPic = (ScaledImageView)mHeader.findViewById(R.id.profile_pic);
        mBackOrInvite = (ImageView)mHeader.findViewById(R.id.back_or_invite_button);
        mMoreButton = (ImageView)mHeader.findViewById(R.id.more_button);
        mTitle = (TextView)mHeader.findViewById(R.id.title);
        mSocialLinks = (LinearLayout)mHeader.findViewById(R.id.social_links);
        mVideosNum = (TextView)mHeader.findViewById(R.id.videos_num);
        mFollowingNum = (TextView)mHeader.findViewById(R.id.following_num);
        mFollowersNum = (TextView)mHeader.findViewById(R.id.followers_num);
        mEditOrFollow = (LinearLayout) mHeader.findViewById(R.id.edit_or_follow_button);
        mEditOrFollowText = (TextView) mHeader.findViewById(R.id.edit_or_follow_text);
        mFollowIcon = (ImageView) mHeader.findViewById(R.id.follow_icon);
        mRecycler = (RecyclerView)findViewById(R.id.profile_recycler);
        mNoVideos = (TextView)mHeader.findViewById(R.id.no_videos);
        mWhiteLine = mHeader.findViewById(R.id.white_line);
        mProgressBarView = (WhiteProgressBarView) findViewById(R.id.progress_bar);
        mProgressBarView.updateLayout();

        mUserPic.setIsRoundedImage(true);
        mUserPic.setPlaceHolder(R.drawable.circle_shape_white_20);
        mUserPic.setAnimResID(R.anim.fade_in);
        mUserPic.setImageLoader(getMultiImageLodaer());
        mUserPic.setAutoShowRecycle(false);

        mBluredImage.setBlurRadius(70);
        mBluredImage.setIsRoundedImage(false);
        mBluredImage.setAnimResID(R.anim.fade_in);
        mBluredImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mBluredImage.setImageLoader(getMultiImageLodaer());
        mBluredImage.enableMajorColorLayer(true);
        mBluredImage.setDontUseExisting(true);
        mBluredImage.setAutoShowRecycle(false);

        mBluredImage.setPlaceHolder(R.drawable.game_thumb_placeholder);
        mBluredImage.setBackgroundResource(R.drawable.game_thumb_placeholder);

        getMultiImageLodaer().DisplayImage(mBluredImage);
        getMultiImageLodaer().DisplayImage(mUserPic);

        mMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this, MenuActivity.class));
            }
        });

        mWhiteLine.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mBluredImage.getLayoutParams().height = mWhiteLine.getBottom();
                mBluredImage.requestLayout();
                mWhiteLine.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void getProfile(String mUserID) {
        if(mUserID.equalsIgnoreCase(Constants.ME)){

            mUserPic.setUrl(CAAUserDataSingleton.getInstance().getPhotoUrl());
            mUserPic.setDontUseExisting(true);


            if(CAAUserDataSingleton.getInstance().getPhotoUrl() != null && !CAAUserDataSingleton.getInstance().getPhotoUrl().contains("user-pic-placeholder"))
            {
                mBluredImage.setUrl(CAAUserDataSingleton.getInstance().getPhotoUrl());

                mBluredImage.setBackgroundResource(R.color.main_blue);
                getMultiImageLodaer().DisplayImage(mBluredImage);
            }
            else
            {
                mBluredImage.setBackgroundResource(R.color.main_blue);
            }

            mTitle.setText(CAAUserDataSingleton.getInstance().getFullName());
            mEditOrFollowText.setText("EDIT PROFILE");

            getMultiImageLodaer().DisplayImage(mUserPic);
        }


        App.getUrlService().getUserProfile(mUserID, pageNumber ,new Callback<UserProfile>() {

            @Override
            public void success(UserProfile userProfile, Response response) {
                setupProfile(userProfile);
                setupProfileRecycler(userProfile.getVideos());
            }

            @Override
            public void failure(RetrofitError error) {
                mProgressBarView.setVisibility(View.GONE);
                setupProfileRecycler(null);
                mBluredImage.setBackgroundResource(R.color.main_blue);
                mEditOrFollow.setVisibility(View.INVISIBLE);
                mEditOrFollow.setClickable(false);
                mRecycler.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setupProfile(UserProfile userProfile) {
         mUser = userProfile.getUser();
        if(!mUserID.equalsIgnoreCase(Constants.ME)){

            mUserPic.setUrl(mUser.getPhoto());

            if(!mUser.getPhoto().contains("user-pic-placeholder")) {
                mBluredImage.setUrl(mUser.getPhoto());
                mBluredImage.setBackgroundResource(R.color.main_blue);
                getMultiImageLodaer().DisplayImage(mBluredImage);
            }
            else{
                mBluredImage.setBackgroundResource(R.color.main_blue);
            }

            mTitle.setText(mUser.getFullName());
            getMultiImageLodaer().DisplayImage(mUserPic);
            mEditOrFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(clickAnimation == null)
                        clickAnimation = AnimationUtils.loadAnimation(ProfileActivity.this, R.anim.button_click);

                    if(FollowingDataHandler.getInstance().contains(mUserID)){
                        mEditOrFollow.startAnimation(clickAnimation);
                        mEditOrFollow.setBackgroundResource(R.drawable.button_shape_active_green);
                        mEditOrFollowText.setText("FOLLOW");
                        mEditOrFollowText.setTextColor(Color.WHITE);

                        mFollowIcon.setImageResource(R.drawable.ic_follow_white_24dp);
                        mFollowIcon.setVisibility(View.VISIBLE);
                        mUser.setFollowedByMe("0");
                        setResult(Activity.RESULT_OK, new Intent().putExtra(Constants.FOLLOW_BY_ME,"0"));
                        FollowingDataHandler.getInstance().remove(mUserID);
                        App.getUrlService().deleteUsersFollow(mUserID, new Callback<String>() {
                            @Override
                            public void success(String s, Response response) {
                                TrackingManager.getInstance().tapUnfollow(updatedFollowing(-1), mUserID,"profile", "-1");
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                mEditOrFollow.setBackgroundResource(R.drawable.button_shape_color_white);
                                mEditOrFollowText.setText("FOLLOWING");
                                mEditOrFollowText.setTextColor(getResources().getColor(R.color.green_active));

                                mFollowIcon.setImageResource(R.drawable.ic_followed_green_24dp);
                                mUser.setFollowedByMe("1");
                            }
                        });
                    }
                    else {
                        mEditOrFollow.startAnimation(clickAnimation);
                        mEditOrFollow.setBackgroundResource(R.drawable.button_shape_color_white);

                        mEditOrFollowText.setTextColor(getResources().getColor(R.color.green_active));

                        mEditOrFollowText.setText("FOLLOWING");
                        mFollowIcon.setImageResource(R.drawable.ic_followed_green_24dp);
                        mUser.setFollowedByMe("1");
                        setResult(Activity.RESULT_OK, new Intent().putExtra(Constants.FOLLOW_BY_ME,"1"));

                        FollowingDataHandler.getInstance().add(mUserID);
                        App.getUrlService().putsUsersFollow(mUserID, "", new Callback<String>() {
                            @Override
                            public void success(String s, Response response) {
                                TrackingManager.getInstance().tapFollow(mUser.getUserId(),"profile" , -1, updatedFollowing(1));
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                mEditOrFollow.setBackgroundResource(R.drawable.button_shape_active_green);
                                mEditOrFollowText.setText("FOLLOW");
                                mEditOrFollowText.setTextColor(Color.WHITE);
                                mFollowIcon.setImageResource(R.drawable.ic_follow_white_24dp);
                                mUser.setFollowedByMe("0");
                            }
                        });
                        StateMachine.getInstance(getApplicationContext()).insertEvent(
                                new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL,
                                        StateMachineEvent.EVENT_TYPE.FOLLOW_USER.ordinal(),
                                        Integer.parseInt(mUserID)
                                )
                        );
                    }
                }
            });
        }
        else{
            CAAUserDataSingleton.getInstance().setFacebookLink(mUser.getFacebookHandle());
            CAAUserDataSingleton.getInstance().setTwitterLink(mUser.getTwitterHandle());
            CAAUserDataSingleton.getInstance().setInstagramLink(mUser.getInstagramHandle());
            CAAUserDataSingleton.getInstance().setYoutubeLink(mUser.getYoutubeHandle());

            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ProfileActivity.this, EditProfileActivity.class);
                    intent.putExtra(Constants.USER_ID, mUserID);
                    intent.putExtra(Constants.FACEBOOK_LINK, mUser.getFacebookHandle());
                    intent.putExtra(Constants.TWITTER_LINK, mUser.getTwitterHandle());
                    intent.putExtra(Constants.INSTAGRAM_LINK, mUser.getInstagramHandle());
                    intent.putExtra(Constants.YOUTUBE_LINK, mUser.getYoutubeHandle());
                    startActivityForResult(intent,EDIT_PROFILE_CODE);
                }
            };
            mEditOrFollow.setOnClickListener(onClickListener);
            mUserPic.setOnClickListener(onClickListener);
        }

        String videoNum = mUser.getTotalPerformances();
        String followingNum = mUser.getTotalFollowing();
        String followersNum = mUser.getTotalFollowers();
        videoNum = Integer.parseInt(videoNum) == 1 ? videoNum + " VIDEO  | " : videoNum + " VIDEOS  | ";
        followingNum = followingNum + " FOLLOWING  | ";
        followersNum = Integer.parseInt(followersNum) == 1 ? followersNum + " FOLLOWER" : followersNum + " FOLLOWERS";
        mVideosNum.setText(videoNum);
        mFollowingNum.setText(followingNum);
        mFollowersNum.setText(followersNum);
        mVideosNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mGridLayoutManager.smoothScrollToPosition(mRecycler,null,1);
//                if(videosY > 0)
//                    mNestedScrollView.smoothScrollTo(0, App.HEIGHT - videosY);
            }
        });

        View.OnClickListener followingClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, UserConnectionsActivity2.class);
                intent.putExtra(Constants.USER_ID, mUserID);
                intent.putExtra(Constants.FOLLOWING_AMOUNT, Integer.parseInt(mUser.getTotalFollowing()));
                intent.putExtra(Constants.FOLLOWERS_AMOUNT, Integer.parseInt(mUser.getTotalFollowers()));
                intent.putExtra(Constants.SELECTED_TAB, Constants.FOLLOWING);
                if(mUserID.equalsIgnoreCase(Constants.ME))
                    startActivityForResult(intent,Constants.REQ_PROFILE_FOLLOWING);
                else
                    startActivity(intent);
            }
        };

        View.OnClickListener followersClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, UserConnectionsActivity2.class);
                intent.putExtra(Constants.USER_ID, mUserID);
                intent.putExtra(Constants.FOLLOWING_AMOUNT, Integer.parseInt(mUser.getTotalFollowing()));
                intent.putExtra(Constants.FOLLOWERS_AMOUNT, Integer.parseInt(mUser.getTotalFollowers()));
                intent.putExtra(Constants.SELECTED_TAB, Constants.FOLLOWERS);
                if(mUserID.equalsIgnoreCase(Constants.ME))
                    startActivityForResult(intent,Constants.REQ_PROFILE_FOLLOWING);
                else
                    startActivity(intent);
            }
        };

        mFollowingNum.setOnClickListener(followingClickListener);
        mFollowersNum.setOnClickListener(followersClickListener);

        setupSocialLinks(mUser.getFacebookHandle(),mUser.getTwitterHandle(),mUser.getInstagramHandle(),mUser.getYoutubeHandle());

        mProgressBarView.setVisibility(View.GONE);
        mRecycler.setVisibility(View.VISIBLE);
    }

    private void setupFollowButton() {
        if(mEditOrFollow == null)
            return;

        if(FollowingDataHandler.getInstance().contains(mUserID)){
            mEditOrFollow.setBackgroundResource(R.drawable.button_shape_color_white);
            mFollowIcon.setImageResource(R.drawable.ic_followed_green_24dp);
            mFollowIcon.setVisibility(View.VISIBLE);
            mEditOrFollowText.setTextColor(getResources().getColor(R.color.green_active));
            mEditOrFollowText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 19);
            mEditOrFollowText.setText("FOLLOWING");
        }
        else
        {
            mEditOrFollow.setBackgroundResource(R.drawable.button_shape_active_green);
            mFollowIcon.setImageResource(R.drawable.ic_follow_white_24dp);
            mFollowIcon.setVisibility(View.VISIBLE);
            mEditOrFollowText.setTextColor(Color.WHITE);
            mEditOrFollowText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 19);
            mEditOrFollowText.setText("FOLLOW");
            mFollowIcon.setVisibility(View.VISIBLE);
        }
    }

    private void setupProfileRecycler( ArrayList<CAAVideo> videos) {
        if(videos == null ) {
            videos = new ArrayList<>(1);
            videos.add(null);
        }
        else
            videos.add(0, null);
        int columnPadding = (int) (15f * App.SCALE_Y);
        mGridLayoutManager = new GridLayoutManager(this, 3);
        mGridLayoutManager.setRecycleChildrenOnDetach(true);
        mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return mProfileRecyclerAdapter.getItemViewType(position) == AdaptersDataTypes.HEADER ? mGridLayoutManager.getSpanCount() : 1;
            }
        });
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(mGridLayoutManager);
        mRecycler.addItemDecoration(new GridSpacingItemDecoration(3, columnPadding, false, true));
        mRecycler.setNestedScrollingEnabled(false);


        listInitialized = true;
        items = videos;

        if(videos.size() > 1) {
            mNoVideos.setVisibility(View.GONE);
            mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int visibleItemCount = mGridLayoutManager.getChildCount();
                    int totalItemCount = mGridLayoutManager.getItemCount();
                    int firstVisibleItem = mGridLayoutManager.findFirstVisibleItemPosition();
                    if (listInitialized) {
                        final int lastItem = firstVisibleItem + visibleItemCount;
                        if (lastItem >= totalItemCount - 6 && !calling && !endReached) {
                            if (previousLastItem != lastItem) { //to avoid multiple calls for last item
//                                Log.d("Scroll", "Last:" + lastItem);
                                previousLastItem = lastItem;
                                getNextPage();
                            }
                        }
                    }
                }
            });
        }
        else{

            mNoVideos.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    mNoVideos.getLayoutParams().height = mBottomBar.getTop()  - mWhiteLine.getBottom();
                    mNoVideos.setVisibility(View.VISIBLE);
                    mNoVideos.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }
        if (mProfileRecyclerAdapter == null)
            mProfileRecyclerAdapter = new DiscoverRecyclerAdapter(items, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CAAVideo video = ((CAAVideo)v.getTag());
                    Intent songHome = new Intent(ProfileActivity.this, VideoHomeActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.CAAVIDEO, video);
                    songHome.putExtra(Constants.BUNDLE, bundle);
                    if(mUserID.equalsIgnoreCase(Constants.ME)) {
                        int pos = items.indexOf(video);
                        songHome.putExtra(Constants.POS, pos);
                        startActivityForResult(songHome, REQ_VIDEO_CHANGE);
                    }
                    else
                        startActivity(songHome);
                }
            }, mUserID.equalsIgnoreCase(Constants.ME), mHeader, Constants.PROFILE_ACTIVITY);

        mRecycler.setAdapter(mProfileRecyclerAdapter);
    }

    private void getNextPage() {
        pageNumber++;
        calling = true;
        App.getUrlService().getUserProfile(mUserID, pageNumber ,new Callback<UserProfile>() {
            @Override
            public void success(UserProfile userProfile, Response response) {
                int startRange = mProfileRecyclerAdapter.getItemCount();
                if (response.getStatus() != HttpURLConnection.HTTP_NO_CONTENT) {
                    if (userProfile.getVideos().size() < 1) {
                        noData++;
                        if (noData >= 2) {
                            endReached = true;
                        }
                        else
                        {
                            getNextPage();
                        }
                    } else {
                        noData = 0;
                        int previousItemCount = mProfileRecyclerAdapter.getItemCount();
                        for (CAAVideo video : userProfile.getVideos()) {
                            items.add(video);
                        }
                        mProfileRecyclerAdapter.notifyItemRangeInserted(previousItemCount, mProfileRecyclerAdapter.getItemCount());
                    }
                } else {
                    endReached = true;
                }
                mProfileRecyclerAdapter.notifyItemRangeChanged(startRange, mProfileRecyclerAdapter.getItemCount(), null);
                calling = false;
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void setupSocialLinks(final String facebook, final String twitter, final String instagram, final String youtube) {
        mSocialLinks.removeAllViews();

        String lastLink = "facebook";
        if(instagram != null && !instagram.isEmpty())
            lastLink = "instagram";
        if(twitter != null && !twitter.isEmpty())
            lastLink = "twitter";
        if(youtube != null && !youtube.isEmpty())
            lastLink = "youtube";

        int padding = (int)(6f * App.SCALE_X);
        if(facebook != null && !facebook.isEmpty()) {
            ImageView facebookLink = new ImageView(this);
            facebookLink.setImageResource(R.drawable.ic_social_facebook_24dp);
            facebookLink.setBackgroundResource(R.drawable.circle_shape_white_20);
            facebookLink.setPadding(padding,padding,padding,padding);
            facebookLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        String url;
                        int versionCode = getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;
                        if (versionCode >= 3002850) { //newer versions of fb app
                            url  = "fb://facewebmodal/f?href=https://www.facebook.com/" + facebook;
                        } else { //older versions of fb app
                            url = "fb://page/" + facebook;
                        }
                        getPackageManager().getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
                        Intent facebookIntent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                        facebookIntent.setPackage("com.facebook.katana");
                        startActivity(facebookIntent); //Trys to make intent with FB's URI
                    } catch (Exception e) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/" + facebook)));
                        }catch (ActivityNotFoundException t){
                            Toast.makeText(getApplicationContext(),"Please download a web browser from GooglePlay",Toast.LENGTH_SHORT).show();
                        }
                    }


                }
            });
            mSocialLinks.addView(facebookLink);
            if(!lastLink.equals("facebook"))
                ((LinearLayout.LayoutParams)facebookLink.getLayoutParams()).rightMargin = (int)(25f * App.SCALE_X);
        }
        if(instagram != null && !instagram.isEmpty()) {
            ImageView instagramLink1 = new ImageView(this);
            instagramLink1.setImageResource(R.drawable.ic_social_instagram_24dp);
            instagramLink1.setBackgroundResource(R.drawable.circle_shape_white_20);
            instagramLink1.setPadding(padding,padding,padding,padding);
            instagramLink1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri uri = Uri.parse("http://instagram.com/_u/" + instagram);
                    Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
                    likeIng.setPackage("com.instagram.android");
                    try {
                        startActivity(likeIng);
                    } catch (ActivityNotFoundException e) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/" + instagram)));
                        }catch (ActivityNotFoundException t){
                            Toast.makeText(getApplicationContext(),"Please download a web browser from GooglePlay",Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
            mSocialLinks.addView(instagramLink1);
            if(!lastLink.equals("instagram"))
                ((LinearLayout.LayoutParams)instagramLink1.getLayoutParams()).rightMargin = (int)(25f * App.SCALE_X);
        }
        if(twitter != null && !twitter.isEmpty()) {
            ImageView twittwerLink2 = new ImageView(this);
            twittwerLink2.setImageResource(R.drawable.ic_social_twitter_24dp);
            twittwerLink2.setBackgroundResource(R.drawable.circle_shape_white_20);
            twittwerLink2.setPadding(padding,padding,padding,padding);
            twittwerLink2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + twitter)));
                    } catch (Exception e) {
                        Intent twitterIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + twitter));
                        if(resolveActivity(twitterIntent))
                            startActivity(twitterIntent);
                        else
                            Toast.makeText(ProfileActivity.this,"Please download Twitter or a web browser from GooglePlay",Toast.LENGTH_SHORT).show();
                    }
                }
            });
            mSocialLinks.addView(twittwerLink2);
            if(!lastLink.equals("twitter"))
                ((LinearLayout.LayoutParams)twittwerLink2.getLayoutParams()).rightMargin = (int)(25f * App.SCALE_X);
        }

        if(youtube != null && !youtube.isEmpty()) {
            ImageView youtubeLink3 = new ImageView(this);
            youtubeLink3.setImageResource(R.drawable.ic_social_youtube_24dp);
            youtubeLink3.setBackgroundResource(R.drawable.circle_shape_white_20);
            youtubeLink3.setPadding(padding,padding,padding,padding);
            youtubeLink3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent;
                    try {
                        intent = new Intent(Intent.ACTION_VIEW);
                        intent.setPackage("com.google.android.youtube");
                        intent.setData(Uri.parse(youtube));
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(youtube));
                        if(resolveActivity(intent))
                            startActivity(intent);
                        else
                            Toast.makeText(ProfileActivity.this,"Please download YouTube or a web browser from GooglePlay",Toast.LENGTH_SHORT).show();
                    }

                }
            });
            mSocialLinks.addView(youtubeLink3);
            if(!lastLink.equals("youtube"))
                ((LinearLayout.LayoutParams)youtubeLink3.getLayoutParams()).rightMargin = (int)(25f * App.SCALE_X);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == EDIT_PROFILE_CODE && resultCode == Activity.RESULT_OK){
            if(data.hasExtra(Constants.PHOTO_PATH)) {
                startMultiImageLoader();
                mUserPic.setUrl(data.getStringExtra(Constants.PHOTO_PATH));
                mBluredImage.setUrl(data.getStringExtra(Constants.PHOTO_PATH));
                mUserPic.setImageLoader(mImageLoaderManager);
                mBluredImage.setImageLoader(mImageLoaderManager);
            }

            setupSocialLinks(CAAUserDataSingleton.getInstance().getFacebookLink(), CAAUserDataSingleton.getInstance().getTwitterLink(),
                    CAAUserDataSingleton.getInstance().getInstagramLink(), CAAUserDataSingleton.getInstance().getYoutubeLink());
            mTitle.setText(CAAUserDataSingleton.getInstance().getFullName());
        }

        if(requestCode == REQ_VIDEO_CHANGE && resultCode == Activity.RESULT_OK){
            if(data.hasExtra(Constants.STATUS)){
                String status = data.getStringExtra(Constants.STATUS);
                int pos = data.getIntExtra(Constants.POS, -1);
                if(pos != -1){
                    items.get(pos).setStatus(status);
                    mProfileRecyclerAdapter.notifyItemChanged(pos);
                }
            }

            if(data.hasExtra(Constants.PERMISSION_LEVEL)){
                int level = data.getIntExtra(Constants.PERMISSION_LEVEL, 0);
                int pos = data.getIntExtra(Constants.POS, -1);
                if(pos != -1){
                    items.get(pos).setPermissionLevel(level);
                    mProfileRecyclerAdapter.notifyItemChanged(pos);
                }
            }

            int removePos = data.getIntExtra(Constants.REMOVE_POS, -1);
            if(removePos != -1 && items != null){
                items.remove(removePos);
                mProfileRecyclerAdapter.notifyItemRemoved(removePos);
                int total = Integer.parseInt(mUser.getTotalPerformances()) - 1;
                mUser.setTotalPerformances(total + "");
                String videoNum =  total == 1 ? total + " VIDEO | " : total + " VIDEOS | ";
                mVideosNum.setText(videoNum);
            }
        }

        if(requestCode == Constants.REQ_PROFILE_FOLLOWING && resultCode == Activity.RESULT_OK){
            try {
                int followingNum = data.getIntExtra(Constants.FOLLOWING_AMOUNT, Integer.parseInt(mUser.getTotalFollowing()));
                mUser.setTotalFollowing(String.valueOf(followingNum));
                mFollowingNum.setText(followingNum + " FOLLOWING | ");
            }catch (Throwable throwable){
                if(BuildConfig.DEBUG)
                    throwable.printStackTrace();
            }
        }
    }

    @Override
    public int getSelfID() {
        return mUserID.equalsIgnoreCase(Constants.ME) ? PROFILE_SCREEN : NO_BAR;
    }

    @Override
    public void onPermissionGranted() {
        mBackOrInvite.callOnClick();
    }

    @Override
    public void onPermissionDenied() {
    }

    @Override
    public void onNetworkChangedListener(boolean isConnected) {
        if(!isConnected)
            mProgressBarView.setVisibility(View.GONE);
    }

    private int updatedFollowing(int add){
        int following = SharedPreferencesManager.getInstance().getInt(EventConstants.FOLLOWING_COUNT, 1) + add;
        SharedPreferencesManager.getEditor().putInt(EventConstants.FOLLOWING_COUNT, following).commit();
        return  following;
    }

    private boolean resolveActivity(Intent intent){
        return getPackageManager().resolveActivity(intent,0) != null;
    }
}
