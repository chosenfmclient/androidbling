package fm.bling.blingy.record;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.opencv.android.CameraBridgeViewBase;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import fm.bling.blingy.App;
import fm.bling.blingy.BuildConfig;
import fm.bling.blingy.ProcessImage;
import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.dialogs.TimerDialog;
import fm.bling.blingy.dialogs.VideoTutorialDialog;
import fm.bling.blingy.dialogs.listeners.OnChoiceSelected;
import fm.bling.blingy.editVideo.VideoPlayerActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.record.audio.AudioPlayer;
import fm.bling.blingy.record.audio.AudioRecorder;
import fm.bling.blingy.record.callbacks.DetectBackgroundCallback;
import fm.bling.blingy.record.dialogs.DetectTipsDialog;
import fm.bling.blingy.record.dialogs.SolidBackgroundOnboarding;
import fm.bling.blingy.record.utils.BlurLoadingDialog;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.FFMPEG;
import fm.bling.blingy.utils.Tic;
import fm.bling.blingy.utils.dialogs.LoadingDialog;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderListener;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;
import fm.bling.blingy.utils.imagesManagement.views.WhiteProgressBarView;
import fm.bling.blingy.utils.views.CountDownItem;
import fm.bling.blingy.utils.views.TimeSegment;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CreateVideoActivity
 * Description:
 * Created by Zach Gerstman on 5/31/15.
 * Edit by : Oren Zakay.
 * ***********************************
 */

public class CreateVideoActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback, OnChoiceSelected , DetectBackgroundCallback
{
    private final String TAG = "CreateVideoActivity";

    private final int CAMERA_VIEW_ID = 22;
    private final int DETECTED_ID = 23;
    private int VIDEO_MAX_LENGTH = 15000;

    //    private int selectedFX = 0;
    private float progressScale = ((float) App.WIDTH) / 15f;
    private int time = 0;
    private int seconds = 0;
    private int mReDetectCount = 0;
    private boolean isGotTime = false;
    private boolean isFirst = true;
    private boolean isRecording = false;
    private boolean isStopButtonShowed = false;
    private boolean isCounting  = false;
    private boolean cameraInitialized = false;
    private boolean isCameraFront = true;
    private boolean isPhotoMode = false;
    private boolean isPauseMode = false;
    private boolean isPaused = false;
    private boolean isBlingyMode = false;
    private String songName = "";
//    private String mashupID = "";
    private String artistName = "";
    private String songUrl = null;
    private String clipID = null;
    private String outputVideoFilePath;
    private ArrayList<Integer> mTimeSegments = null;
    private ArrayList<Integer> mFastTimeSegments;

    private ArrayList<String> mEffects = new ArrayList<>();
    private Animation zoomout;
    private Timer mNavBarTimer;
    private Timer clockTimer;
    private Intent intent;
    private Intent startIntent;
    private CameraRecorder3 cameraRecorder;
    private PowerManager.WakeLock wakeLock;
    private Handler mHandler = new Handler();

    /**
     * Add Audio
     **/
//    private int realHeight;

    private Paint progressPaint;
    private RectF progressRect;

    /**
     * Hyperlapse
     */
    private LinearLayout mHyperlapseContainer;
    private Button mNormalMotionButton;
    private Button mFastMotionButton;
    private final int NORMAL = 1;
    private final int SLOW = 2;
    private final int FAST = 3;
    private int speedSelected;
    private float mAudioSpeed = 1f;
    private float mRecordSpeed = 1f;
    private int mTimerChoice = TimerDialog.CHOICE_3;

    /**
     * Main Views
     */
    private FrameLayout recordLayout;
    private FrameLayout mCameraContainer;
    private FrameLayout preview;
    private FrameLayout mProgressContainer;
    private LinearLayout mBottomContainer;
    private LinearLayout mRecordButtonContainer;
    private LinearLayout buttonsLayout;
    private LinearLayout mStopButton;
    private LinearLayout mRecordButtons;
    private LinearLayout mRedetectButton;
    private TextView mPercentageText;
    private TextView mResumePauseText;
    private TextView mReadyText;
    private TextView mMainInfo;
    private TextView mBlingyMode;
    private ImageView mCaptureButton;
    private ImageView mBack;
    private ImageView mTimer;
    private ImageView mRotate;
    private ImageView mVideoTutorial;
    private View mProgressValue;
    private CountDownItem countDownItem;
    private Button mDetectMainButton;
    private WhiteProgressBarView mPercentageProgress;
    private WhiteProgressBarView mDetectProgress;

    private ImageLoaderManager mImageLoaderManager;
    private SolidBackgroundOnboarding mSolidBackgroundDialog;
    private VideoTutorialDialog mVideoTutorialDialog;
    private LoadingDialog blurLoadingDialog;

    private boolean showDetectFlow = true;
    private boolean shouldShowSilhouette = false;
    private boolean isPassedOnCreate = false;
    private boolean isActivityVisible = false;

    private boolean isSwitchingCamera;
    private boolean isFileOnDisk = false;
    private boolean isDoneProcessing = false;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        System.gc();
        Tic.tic(CAMERA_VIEW_ID);
        startIntent = getIntent();
        isPassedOnCreate = true;
        TrackingManager.getInstance().pageView(EventConstants.RECORD_PAGE);

//        isCameraFront = !Constants.isBackCamera;
        Constants.isBackCamera = false;

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.blingy_video_layout);
        init();
        deactiveCaptureButton();

        outputVideoFilePath = App.VIDEO_WORKING_FOLDER + "blingy_vid.mp4";

        mImageLoaderManager = new ImageLoaderManager(this);

//        if (getIntent().getBooleanExtra(Constants.PHOTO_MODE, false))
//        {
//            setUpPhotoMode();
//        }

        View.OnClickListener mMotionClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpMotionButtons(((int) v.getTag()));
            }
        };
        mFastMotionButton.setTag(FAST);
        mNormalMotionButton.setTag(NORMAL);
        mFastMotionButton.setOnClickListener(mMotionClickListener);
        mNormalMotionButton.setOnClickListener(mMotionClickListener);

        intent = new Intent(CreateVideoActivity.this, VideoPlayerActivity.class);

        if(getIntent().hasExtra(Constants.CLIP_ID)){
            clipID = getIntent().getStringExtra(Constants.CLIP_ID);
            intent.putExtra(Constants.CLIP_ID, clipID);
        }

        if(getIntent().hasExtra(Constants.PARENT_ID)){
            String parentID = getIntent().getStringExtra(Constants.PARENT_ID);
            intent.putExtra(Constants.PARENT_ID, parentID);
        }

        if(getIntent().hasExtra(Constants.TYPE)){
            String type = getIntent().getStringExtra(Constants.TYPE);
            intent.putExtra(Constants.TYPE, type);
        }

        if(getIntent().hasExtra(Constants.FROM))
        {
            String FROM = getIntent().getStringExtra(Constants.FROM);
            switch(FROM)
            {
                case Constants.VIDEO_PLAYER_ACTIVITY :
                    isFileOnDisk = true;
                    break;
                default:
                    isFileOnDisk = false;
                    break;
            }
        }

        intent.putExtra(EventConstants.EFFECTS, mEffects);

        if (startIntent.hasExtra("song_name"))
        {
            songName = startIntent.getStringExtra("song_name");
            intent.putExtra("song_name", songName);
        }

        if (startIntent.hasExtra("artist_name"))
        {
            artistName = startIntent.getStringExtra("artist_name");
            intent.putExtra("artist_name", artistName);
        }

        if (startIntent.hasExtra(Constants.CLIP_URL))
        {
            setUpVideoMode();
            deactiveCaptureButton();
            songUrl = getIntent().getStringExtra(Constants.CLIP_URL);
        }

        if (startIntent.hasExtra(Constants.TIME_SEGMENTS))
        {
            mTimeSegments = getIntent().getIntegerArrayListExtra(Constants.TIME_SEGMENTS);
        }
        else
        {
            App.getUrlService().getTimeSegmentsByClipID(clipID, new Callback<ArrayList<Integer>>() {
                @Override
                public void success(ArrayList<Integer> segments, Response response) {
                    mTimeSegments = segments;
                    if(isGotTime)
                        setTimeSegments(VIDEO_MAX_LENGTH);
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }

        if (startIntent.hasExtra(Constants.CLIP_URL))
        {
            String clipUrl = startIntent.getStringExtra(Constants.CLIP_URL);
            String clipThumbnail = startIntent.getStringExtra(Constants.CLIP_THUMBNAIL);
            intent.putExtra(Constants.CLIP_URL, clipUrl);
            intent.putExtra(Constants.CLIP_THUMBNAIL, clipThumbnail);
            mProgressContainer.setVisibility(View.VISIBLE);
            deactiveCaptureButton();
            if (clipThumbnail != null)
            {
                ScaledImageView mBluredThumbnail = new ScaledImageView(getApplicationContext());
                mBluredThumbnail.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                mBluredThumbnail.setUrl(clipThumbnail);
                mBluredThumbnail.setDontSaveToFileCache(true);
                mBluredThumbnail.setDontUseExisting(true);
                mBluredThumbnail.setBlurRadius(70);
                mBluredThumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);
                mBluredThumbnail.setMaxWidthInternal(App.WIDTH / 2);
                mBluredThumbnail.setAnimResID(R.anim.fade_in_slow);
                if (Build.VERSION.SDK_INT >= 21)
                    mBluredThumbnail.setAspectRatio((float) (App.WIDTH) / App.HEIGHT);
                mBluredThumbnail.setImageLoader(getImageLoaderManager());
                mBluredThumbnail.setImageLoaderListener(new ImageLoaderListener()
                {
                    @Override
                    public void onImageLoaded(URLImageView urlImageView)
                    {
                        urlImageView.invalidate();
                        urlImageView.setAnimResID(URLImageView.NOT_SPECIFIED);
                    }
                });
                getImageLoaderManager().DisplayImage(mBluredThumbnail);
                recordLayout.addView(mBluredThumbnail, 0);

            }
        }

        UiChangeListener();
        mHandler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                if (SharedPreferencesManager.getInstance().getBoolean(Constants.BACKGROUND_TUTORIAL, true))
                { // firstTime force show if in settings not off
//                    boolean isSkipable = !SharedPreferencesManager.getInstance(CreateVideoActivity.this).getBoolean(Constants.SHOW_BACKGROUND_TUTORIAL, true);
                    if (mVideoTutorialDialog == null)
                    {
                        mVideoTutorialDialog = new VideoTutorialDialog(CreateVideoActivity.this, false);
                        mVideoTutorialDialog.setOnDismissListener(new DialogInterface.OnDismissListener()
                        {
                            @Override
                            public void onDismiss(DialogInterface dialog)
                            {
                                mVideoTutorialDialog = null;
                                if (shouldShowSilhouette)
                                {
                                    shouldShowSilhouette = false;
                                    mSolidBackgroundDialog = new SolidBackgroundOnboarding(CreateVideoActivity.this, recordLayout, isCameraFront);
                                }
                            }
                        });
                    }
                }
            }
        }, 750);

        TrackingManager.getInstance().camerView(Tic.tac(CAMERA_VIEW_ID) / 1000);
    }

    /**
     *
     * @param detectionResponseCode find codes @{@link fm.bling.blingy.ProcessImage}
     */
    public void onBackgroundDetected(final int detectionResponseCode)
    {

        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                mDetectProgress.setVisibility(View.GONE);
                if(!isPaused)
                    mVideoTutorial.setVisibility(View.VISIBLE);
                int time = 1000;
                switch (detectionResponseCode)
                {
                    case ProcessImage.DetectionResponseSuccess :
                    {
                        TrackingManager.getInstance().detectCompleted(Tic.tac(DETECTED_ID) / 1000);
                        mSolidBackgroundDialog.onBackgroundDetected(detectionResponseCode);
                        mReadyText.setText(R.string.youre_in_the_video);
                        break;
                    }
                    default:
                        mSolidBackgroundDialog.onBackgroundDetected(detectionResponseCode);
                        mReadyText.setText(R.string.might_need_to_redetect);
                        startRedetectButtonAniamtion();
//                        showRedetect(true);
                        time = 3000;
                        break;

                }

                if (isRecording)
                {
                    mMainInfo.setVisibility(View.GONE);
                    mReadyText.setVisibility(View.GONE);
                    mRecordButtons.setVisibility(View.VISIBLE);
                }
                else
                {
//                    mSubInfo.setVisibility(View.GONE);
                    mMainInfo.setVisibility(View.GONE);
                    mReadyText.setVisibility(View.VISIBLE);
                    openHyperLapse();
                    mDetectMainButton.setVisibility(View.GONE);
                    mRecordButtons.setVisibility(View.VISIBLE);
                    mTimer.setVisibility(View.VISIBLE);
                    if (mTimeSegments != null && mTimeSegments.size() > 0)
                        mBlingyMode.setVisibility(View.VISIBLE);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mReadyText.setText(R.string.ready_to_record);
                        }
                    }, time);
                }
            }
        });

    }

    private void startRedetectButtonAniamtion() {
        final View view = mRedetectButton.findViewById(R.id.redetect_icon);

        ScaleAnimation scaleAnimation = new ScaleAnimation(1f,1.15f,1f,1.15f,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(500);
        scaleAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        scaleAnimation.setRepeatCount(12);
        scaleAnimation.setRepeatMode(Animation.REVERSE);
        scaleAnimation.setFillAfter(false);

        ValueAnimator colorAnim = new ValueAnimator();
        colorAnim.setIntValues(ContextCompat.getColor(this,R.color.main_blue), ContextCompat.getColor(this, R.color.main_blue_90));
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.setDuration(500);
        colorAnim.setRepeatCount(12);
        colorAnim.setRepeatMode(ValueAnimator.REVERSE);
        colorAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                view.getBackground().setColorFilter((Integer)valueAnimator.getAnimatedValue() , PorterDuff.Mode.SRC_ATOP);
            }
        });
        colorAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.getBackground().setColorFilter(ContextCompat.getColor(CreateVideoActivity.this,R.color.main_blue), PorterDuff.Mode.SRC_ATOP);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        view.startAnimation(scaleAnimation);
        colorAnim.start();
    }

    public void setTimeSegments(long duration)
    {
        isGotTime = true;
        if(duration < 0)
            return;

        VIDEO_MAX_LENGTH = (int)duration;
        progressScale = ((float) App.WIDTH) / ((float)(VIDEO_MAX_LENGTH / 1000));

        runOnUiThread(new Runnable()
        {
            @Override
            public void run() {
                int itemPos = 1;
                mFastTimeSegments = new ArrayList<>();
                if (mTimeSegments != null && mTimeSegments.size() > 0)
                {
                    for (int i = 0; i < mTimeSegments.size(); i++)
                    {
                        if (mTimeSegments.get(i) < 15f)
                        {
                            mFastTimeSegments.add(mTimeSegments.get(i) * 2);
                            mProgressContainer.addView(new TimeSegment(CreateVideoActivity.this, mTimeSegments.get(i) * (int) progressScale), itemPos++);
                        }
                    }
                }
            }
        });
    }

    private void init()
    {
        recordLayout = (FrameLayout) findViewById(R.id.record_activity);
        recordLayout.setWillNotDraw(false);
        mCaptureButton = (ImageView) findViewById(R.id.recorder_button_record);
        mVideoTutorial = (ImageView) findViewById(R.id.video_tutorial);
        mResumePauseText = (TextView) findViewById(R.id.textview_resume_pause);
        mRecordButtons = (LinearLayout) findViewById(R.id.recordButts);
        mBottomContainer = (LinearLayout) findViewById(R.id.bottom_container);
        mRecordButtonContainer = (LinearLayout) findViewById(R.id.recorder_button_container);
        buttonsLayout = (LinearLayout) findViewById(R.id.buttons_layout);
        mStopButton = (LinearLayout) findViewById(R.id.stop_button);
        mHyperlapseContainer = (LinearLayout) findViewById(R.id.hypelapse_contaniner);
        mNormalMotionButton = (Button) findViewById(R.id.normal_motion);
        mFastMotionButton = (Button) findViewById(R.id.fast_motion);
        mProgressContainer = (FrameLayout) findViewById(R.id.progress_bar);
        mProgressContainer.setWillNotDraw(false);
        mPercentageProgress = (WhiteProgressBarView) findViewById(R.id.percentage_progress_bar);
        mDetectProgress = (WhiteProgressBarView) findViewById(R.id.detect_progress_bar);
        mDetectProgress.updateLayout();
        mPercentageText = (TextView) findViewById(R.id.percentage);
        mPercentageProgress.updateLayout();
        mBack = (ImageView) findViewById(R.id.back_button);
        mRotate = (ImageView) findViewById(R.id.rotate_button);
        mTimer = (ImageView) findViewById(R.id.timer);
        mBlingyMode = (TextView) findViewById(R.id.blingy_mode);
        mCameraContainer = (FrameLayout) findViewById(R.id.camera_container);
        mReadyText = (TextView) findViewById(R.id.ready_text);
        mMainInfo = (TextView) findViewById(R.id.main_info);
//        mSubInfo = (TextView) findViewById(R.id.sub_info);
        mRedetectButton = (LinearLayout) findViewById(R.id.redetect_button);
        mDetectMainButton = (Button) findViewById(R.id.detect_main_button);
        countDownItem = new CountDownItem(this, "3");
        countDownItem.setVisibility(View.INVISIBLE);

        mBottomContainer.getLayoutParams().height = getResources().getDisplayMetrics().heightPixels - App.landscapitemHeight - App.TOOLBAR_HEIGHT;

        progressPaint = new Paint();
        progressPaint.setStyle(Paint.Style.FILL);
        progressPaint.setColor(getResources().getColor(R.color.main_blue));
        progressRect = new RectF(0, 0, 0, 0);
        mProgressValue = new View(this)//findViewById(R.id.progress_value);
        {
            public void draw(Canvas canvas)
            {
                super.draw(canvas);
                canvas.drawRect(progressRect, progressPaint);

            }

            public void onDraw(Canvas canvas)
            {
                super.onDraw(canvas);
                canvas.drawRect(progressRect, progressPaint);
            }
        };

//        mMainInfo.addTextChangedListener(new TextWatcher()
//        {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after)
//            {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count)
//            {
//                mMainInfo.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
//                if (s.toString().equals(getString(R.string.we_need_to_detect_your_background)))
//                    checkTextSize();
//            }
//
//            @Override
//            public void afterTextChanged(Editable s)
//            {
//            }
//        });

        mProgressValue.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        mProgressValue.setBackgroundColor(Color.argb(0, 0, 0, 0));
        mProgressContainer.addView(mProgressValue);
        mProgressValue.setWillNotDraw(false);

        mVideoTutorial.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mVideoTutorialDialog = new VideoTutorialDialog(CreateVideoActivity.this, true);
                mVideoTutorialDialog.setOnDismissListener(new DialogInterface.OnDismissListener()
                {
                    @Override
                    public void onDismiss(DialogInterface dialog)
                    {
                        mVideoTutorialDialog = null;
                        if (shouldShowSilhouette)
                        {
                            shouldShowSilhouette = false;
                            if (mSolidBackgroundDialog == null)
                                mSolidBackgroundDialog = new SolidBackgroundOnboarding(CreateVideoActivity.this, recordLayout, isCameraFront);
                        }
                    }
                });
            }
        });
    }

    private void openHyperLapse()
    {
        if (!isPhotoMode && songUrl != null)
        {
            mHyperlapseContainer.setVisibility(View.VISIBLE);
        }
    }

    private void closeHyperlapse()
    {
        mHyperlapseContainer.setVisibility(View.INVISIBLE);
    }

    private void setUpMotionButtons(int selected)
    {
        if (selected != speedSelected)
        {
            startIntent.putExtra("speed", selected);
            switch (selected)
            {
                case NORMAL:
                    setupNormalRecord();
                    break;
                case FAST:
                    setupFastRecord();
                    break;
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        if(isCounting)
            return;

        if (isRecording)
        {
            BaseDialog mDialog = new BaseDialog(this, "Discard Video", "if you close the camera now, your video will be discarded.");
            mDialog.setOKText("KEEP");
            mDialog.setCancelText("DELETE");
            mDialog.setCancelOnclickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    ((BaseDialog) v.getTag()).dismiss();
                    reshoot();
                }
            });
            if (!isPauseMode)
                pauseRecording();
            mDialog.show();
        }
        else
        {
            if(mSolidBackgroundDialog != null)
            {
                mSolidBackgroundDialog.superDismiss();
                mSolidBackgroundDialog = null;
            }
            close();
            super.onBackPressed();
            TrackingManager.getInstance().recordCancel("camera");
        }
    }

    private void reshoot()
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG,"Create>>>reshoot");
        isRecording = false;
        isPauseMode = false;
        isPaused = false;
        isStopped = false;
        showDetectFlow = true;

        releaseCamera(true);
//        try {
//            cameraRecorder.close();
//        }catch (Throwable throwable){
//            if(BuildConfig.DEBUG)
//                Log.d(TAG,throwable.getMessage());
//        }


        mBlingyMode.setEnabled(true);
        mBlingyMode.setClickable(true);

        cameraBuilder(lastCameraId);

        cameraRecorder.setFirstFramePath();
        if (mSolidBackgroundDialog == null)
            mSolidBackgroundDialog = new SolidBackgroundOnboarding(this, preview, isCameraFront);
        else
        {
            mSolidBackgroundDialog.showSilhouette();
            mSolidBackgroundDialog.show();
        }

//        showRedetect(false);
        restartClockTime();
        showMenuItems();
        mStopButton.setVisibility(View.INVISIBLE);
        isStopButtonShowed = false;
        deactiveStopButton();
        mResumePauseText.setVisibility(View.INVISIBLE);
        mCaptureButton.setImageResource(R.drawable.video_mode_active);
        slideInCaptureButton();
        showRedetect();
        progressRect.right = 0;
        mProgressValue.invalidate();
    }

    private int lastCameraId;

    private void cameraBuilder(int cameraID)
    {
        lastCameraId = cameraID;
        preview = (FrameLayout) findViewById(R.id.camera_preview);

        if(preview == null)
            return;

        preview.getLayoutParams().height = App.landscapitemHeight;
        preview.removeAllViews();

        cameraRecorder = new CameraRecorder3(this, VIDEO_MAX_LENGTH, outputVideoFilePath, preview, lastCameraId, songUrl, isFileOnDisk, new CameraRecorder3.OnProcessingVideoListener()
        {
            @Override
            public void onDone()
            {
                endRecording();
            }

            @Override
            public void onStart()
            {
                float scaleFactor;
                Matrix mMatrix = new Matrix();
                int screenHeight = getResources().getDisplayMetrics().heightPixels + App.TOOLBAR_HEIGHT;
                int WIDTH = 480, HEIGHT = 360;
                if(FFMPEG.getInstance(CreateVideoActivity.this).isLiteVideo())
                {
                    WIDTH = 320;
                    HEIGHT = 240;
                }
                int wToTranslate = (App.WIDTH - WIDTH) / 2, hToTranslate = (screenHeight - HEIGHT) / 2;
                mMatrix.setTranslate(wToTranslate, hToTranslate);
                mMatrix.postScale(scaleFactor = (((float)(screenHeight)) / ((float) HEIGHT)), scaleFactor, App.WIDTH / 2,  (screenHeight) / 2 );
                Bitmap firstFrame = BitmapFactory.decodeFile(Constants.thumbnail); // thumbnail - first frame of the video

                if (isActivityVisible)
                    blurLoadingDialog = new BlurLoadingDialog(CreateVideoActivity.this, "Preparing...", App.WIDTH, screenHeight, 0, mMatrix, firstFrame);

                removeAllViews();
//                releaseCamera();
            }
        });

        if (songUrl != null)
        {
            cameraRecorder.setOnDoneDownloadingListener(new AudioRecorder.OnDoneDownloadingListener()
            {
                @Override
                public void onDone(String downloadedFilePath, String downloadedFilePathFast, String downloadedFilePathSlow)
                {
                    if(!CameraBridgeViewBase.isCameraAvailable)
                        return;
                    shouldShowSilhouette = true;
                    //StartDetect Flow
                    if (showDetectFlow)
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                if(mPercentageProgress == null)
                                    return;
                                mPercentageText.setVisibility(View.GONE);
                                mPercentageProgress.setVisibility(View.GONE);

                                preview.setVisibility(View.VISIBLE);
                                if (mSolidBackgroundDialog != null)
                                    mSolidBackgroundDialog.dismiss();

                                mRecordButtons.setVisibility(View.GONE);

                                if(mVideoTutorialDialog == null) {
                                    mSolidBackgroundDialog = new SolidBackgroundOnboarding(CreateVideoActivity.this, recordLayout, isCameraFront);
                                    shouldShowSilhouette = false;
                                }
                                mRotate.setVisibility(View.VISIBLE);
                                mReadyText.setVisibility(View.GONE);

                                mMainInfo.setVisibility(View.VISIBLE);
                                mMainInfo.setText(R.string.press_detect_background);
//                                mSubInfo.setVisibility(View.VISIBLE);
                                mDetectMainButton.setVisibility(View.VISIBLE);
                                mHyperlapseContainer.setVisibility(View.GONE);

                                mDetectProgress.setVisibility(View.GONE);


                            }
                        });

                    if (downloadedFilePath == null)
                        ;///error ????

                    cameraRecorder.setOnCompleteListener(new AudioPlayer.OnCompleteListener()
                    {
                        @Override
                        public void onComplete()
                        {
                            stopRecording();
                        }
                    });
                    activeCaptureButton();
                }

                @Override
                public void onFirstFrameReady()
                {

                }

                @Override
                public void updateProgress(final int progress)
                {
                    mPercentageText.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            mPercentageText.setText(progress + "%");
                        }
                    });

                }
            });
        }
        cameraRecorder.setActivateDeactivateButtonsDispatcher(new ActivateDeactivateButtonsDispatcher()
        {
            @Override
            public void activateButtons()
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        activeCaptureButton();
                    }
                });
            }

            @Override
            public void deActivateButtons()
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        deactiveCaptureButton();
                    }
                });
            }
        });
        preview.addView(cameraRecorder);

        setUpMotionButtons(startIntent.getIntExtra("speed", 1));
//        openHyperLapse();
    }

    private boolean startCamera()
    {
        // Create an instance of Camera
        try
        {
//            cameraBuilder(isCameraFront ? Camera.CameraInfo.CAMERA_FACING_FRONT : Camera.CameraInfo.CAMERA_FACING_BACK); // Android id
            cameraBuilder(isCameraFront ? Camera.CameraInfo.CAMERA_FACING_FRONT : Camera.CameraInfo.CAMERA_FACING_BACK);   // OpenCv  id
        }
        catch (Throwable err)
        {
            err.printStackTrace();
            finish();
            return false;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            mCaptureButton.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                        v.getBackground().setHotspot(v.getWidth() / 2, v.getHeight() / 2);
                    return false;
                }
            });
        }
        mStopButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (Utils.canClick())
                {
                    stopRecording();
                }
            }
        });
        cameraInitialized = true;
        mCaptureButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (Utils.canClick())
                {
                    if (Constants.uploadVideoProgressBar < 100)
                    {
                        stillUploadingDialog();
                    }
                    else if (isRecording)
                    {
                        if (isPauseMode)
                        {
                            AnimateStartCamera();


//                            showTimeNumbers();
                        }
                        else
                        {
                            pauseRecording();
                        }
                    }
                    else if (!isPhotoMode)
                    {
                        /**
                         * First time of Recording
                         */
                        deactiveCaptureButton();
                        deactiveStopButton();
                        isPauseMode = false;
                        isRecording = true;
                        setUpRecordingMode();
                        AnimateStartCamera();
                        cameraRecorder.setAudioSpeed(mAudioSpeed);
                        cameraRecorder.setRecordSpeed(mRecordSpeed);

                        mReadyText.setVisibility(View.INVISIBLE);
                        mReadyText.setText("Ready for next scene");
                    }
//                    else {
//                        // take photo
//                        if (cameraRecorder != null) {
//                            try {
//                                String path = getFilesDir().getAbsolutePath() + "/chosen_photo.jpg";
//                                File file = new File(path);
//                                alignAndSaveImage(cameraRecorder.getPicture(), cameraRecorder.getCameraRotation(), file);
//
//                                intent.putExtra(Constants.PHOTO_MODE, true);
//                                endRecording();
//                            } catch (Throwable err) {
//                            }
//                        }
//                    }
                }
            }
        });
        mBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });
        mRotate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(!Utils.canClick())
                    return;

                if (cameraInitialized)
                {
                    if(isSwitchingCamera) return;
                    isSwitchingCamera = true;

                    if (isCameraFront)
                    {
                        mRotate.setImageResource(R.drawable.ic_camera_front_white_24dp);
                        isCameraFront = false;
                        intent.putExtra(Constants.FRONT_CAMERA, false);
                    }
                    else
                    {
                        mRotate.setImageResource(R.drawable.ic_camera_rear_white_24dp);
                        isCameraFront = true;
                        intent.putExtra(Constants.FRONT_CAMERA, true);

                    }
                    if (!switchCamera())
                    {
                        finish();
                    }
                    isSwitchingCamera = false;
                }
            }
        });
        mTimer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                new TimerDialog(CreateVideoActivity.this, mTimerChoice, CreateVideoActivity.this);
            }
        });

        mBlingyMode.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isBlingyMode)
                {
                    isBlingyMode = false;
                    mBlingyMode.setBackgroundResource(R.drawable.invite_blue_button_bg);
                    mBlingyMode.setText(R.string.basic_mode);
                }
                else
                {
                    isBlingyMode = true;
                    mBlingyMode.setBackgroundResource(R.drawable.button_shape_pink);
                    mBlingyMode.setText(R.string.blingy_mode);
                }
            }
        });

        mDetectMainButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (Utils.canClick())
                {
                    if (!isFirst)
                        mReDetectCount++;
                    else
                        isFirst = false;

                    doDetectBackground();
                }
            }
        });

        mRedetectButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mReDetectCount++;
                doDetectBackground();
            }
        });


        return true;
    }

    private void stillUploadingDialog()
    {
        if(buttonsLayout != null)
            buttonsLayout.setVisibility(View.INVISIBLE);
        String text = "Previous video is still being uploaded.\n" + String.valueOf(Constants.uploadVideoProgressBar) + "% has been uploaded.\nPlease wait and try again later.";
        BaseDialog dialog = new BaseDialog(this, "UPLOAD STATUS", text);
        dialog.removeCancelbutton();
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener()
        {
            @Override
            public void onDismiss(DialogInterface dialogInterface)
            {
                if(buttonsLayout != null)
                buttonsLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(isDoneProcessing)
            prepareAndGoToNextActivity();
        else if(blurLoadingDialog != null) return;

        hideNavBar();


        if (!startCamera())
            return;


        if (songUrl != null)
            deactiveCaptureButton();
        else
            activeCaptureButton();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Blingy camera lock");
        wakeLock.acquire();

        if (!isRecording && !isPauseMode)
        {
            deleteTempFile();
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        isActivityVisible = true;

        if(blurLoadingDialog != null) return;

        if (mVideoTutorialDialog != null && mVideoTutorialDialog.isShowing() && !isPassedOnCreate)
            mVideoTutorialDialog.resumeMediaPlayer();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        isActivityVisible = false;
        mHandler.removeCallbacksAndMessages(null);
        isPassedOnCreate = false;

        if (mVideoTutorialDialog != null && mVideoTutorialDialog.isShowing())
        {
            mVideoTutorialDialog.stopMediaplayer();
        }

        releaseCamera(false);
        try
        {
            wakeLock.release();
        }
        catch (Throwable err) { }
    }

    @Override
    protected void onPause()
    {
        if (isRecording && !isPauseMode && !isCounting)
        {
            mCaptureButton.callOnClick();
            try
            {
                Thread.sleep(2000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        super.onPause();
        if(isCounting) {
            isCounting = false;
            countDownItem.clearAnimation();

            if(!isPaused)
                reshoot();
        }

        releaseCamera(false);              // release the camera immediately on pause event
        if (mNavBarTimer != null)
        {
            mNavBarTimer.cancel();
            mNavBarTimer.purge();
            mNavBarTimer = null;
        }
        if (clockTimer != null)
        {
            clockTimer.purge();
            clockTimer.cancel();
        }

    }

    private synchronized void releaseCamera(boolean dontBlockDontCare)
    {
        if (cameraRecorder != null)
        {
            cameraRecorder.removeCameraView();
            cameraRecorder.releaseCamera(dontBlockDontCare);
            cameraRecorder.close();
            cameraRecorder = null;
        }
    }

    public static boolean isNowCanRun = false;
    public boolean switchCamera()
    {
        if(FFMPEG.getInstance(this).getIsCamera2())
        {
            isNowCanRun = false;

            releaseCamera(true);
            preview.removeAllViews();
            new Thread()
            {
                public void run()
                {
                    while (!isNowCanRun)
                    {
                        try
                        {
                            Thread.sleep(100);
                        }
                        catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                    }

                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            try
                            {
                                if (!isCameraFront) // this is called after it's set with the new value, this is why we check the negative of it !!!
                                {
                                    cameraBuilder(Camera.CameraInfo.CAMERA_FACING_BACK);
                                    if (mSolidBackgroundDialog != null)
                                        mSolidBackgroundDialog.updateSilhouette(false);
                                }
                                else
                                {
                                    cameraBuilder(Camera.CameraInfo.CAMERA_FACING_FRONT);
                                    if (mSolidBackgroundDialog != null)
                                        mSolidBackgroundDialog.updateSilhouette(true);
                                }
                            }
                            catch (Throwable err)
                            {
//                            return false;
                            }
                        }
                    });

                }
            }.start();
        }
        else
        {
            releaseCamera(true);
            try
            {
                if (!isCameraFront) // this is called after it's set with the new value, this is why we check the negative of it !!!
                {
                    cameraBuilder(Camera.CameraInfo.CAMERA_FACING_BACK);
                    if (mSolidBackgroundDialog != null)
                        mSolidBackgroundDialog.updateSilhouette(false);
                }
                else
                {
                    cameraBuilder(Camera.CameraInfo.CAMERA_FACING_FRONT);
                    if (mSolidBackgroundDialog != null)
                        mSolidBackgroundDialog.updateSilhouette(true);
                }
            }
            catch (Throwable err)
            {
                finish();
                return false;
            }
        }

        return true;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
        mHandler = null;
        stopProgress();
    }

    public void AnimateStartCamera() {
        mVideoTutorial.setVisibility(View.GONE);
        mTimer.setEnabled(false);
        mTimer.setClickable(false);
        mBlingyMode.setEnabled(false);
        mBlingyMode.setClickable(false);
        mCameraContainer.removeView(countDownItem);
        if (mTimerChoice != TimerDialog.CHOICE_0){
            ObjectAnimator translateX = ObjectAnimator.ofFloat(mRecordButtonContainer, "translationY", App.WIDTH - mRecordButtonContainer.getBottom());
            translateX.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {

                    showTimeNumbers();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }

            });
            translateX.setDuration(500);
            translateX.start();
        }
        else{
            showTimeNumbers();
        }

        ObjectAnimator translateYStopButton = ObjectAnimator.ofFloat(mStopButton, "translationY", App.WIDTH - mStopButton.getBottom());
        translateYStopButton.setDuration(500);
        translateYStopButton.start();

        hideRedetect();
        hideMenuItems();
    }

    private void showTimeNumbers()
    {
        isCounting = true;
        switch (mTimerChoice)
        {
            case TimerDialog.CHOICE_0:
                if (isRecording)
                {
                    resumeRecording();
                    activeCaptureButton();
                }
                else {
                    startRecording();
                    mCaptureButton.setImageResource(R.drawable.video_pause_red);
                    activeCaptureButton();
                }
                isCounting = false;
                break;
            case TimerDialog.CHOICE_3:
                mCameraContainer.removeView(countDownItem);
                mCameraContainer.addView(countDownItem, 3);
                Animate3Camera();
                break;
            case TimerDialog.CHOICE_5:
                mCameraContainer.removeView(countDownItem);
                mCameraContainer.addView(countDownItem, 3);
                Animate5Camera();
                break;
        }
    }

    public void Animate5Camera()
    {
        zoomout = AnimationUtils.loadAnimation(this, R.anim.zoomout);
        countDownItem.updateText("5");
        zoomout.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) { }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                if(isCounting)
                    Animate4Camera();
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }
        });
        countDownItem.startAnimation(zoomout);
    }

    public void Animate4Camera()
    {
        zoomout = AnimationUtils.loadAnimation(this, R.anim.zoomout);
        countDownItem.updateText("4");
        zoomout.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                if(isCounting)
                    Animate3Camera();
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }
        });
        countDownItem.startAnimation(zoomout);
    }

    public void Animate3Camera()
    {
        zoomout = AnimationUtils.loadAnimation(this, R.anim.zoomout);
        countDownItem.updateText("3");
        zoomout.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                if(isCounting)
                    Animate2Camera();
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }
        });
        countDownItem.startAnimation(zoomout);
    }

    public void Animate2Camera()
    {
        zoomout = AnimationUtils.loadAnimation(this, R.anim.zoomout);
        countDownItem.updateText("2");
        zoomout.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                if(isCounting)
                    Animate1Camera();
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }
        });
        countDownItem.startAnimation(zoomout);

    }

    public void Animate1Camera()
    {
        zoomout = AnimationUtils.loadAnimation(this, R.anim.zoomout);
        countDownItem.updateText("1");
        zoomout.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                if(isCounting) {
                    isCounting = false;
                    try {
                        if (cameraRecorder.isStopped)
                            return;

                        if (isRecording && isPauseMode)
                        {
                            resumeRecording();
                            activeCaptureButton();
                        }
                        else
                            startRecording();

                        CreateVideoActivity.this.runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                mCaptureButton.setImageResource(R.drawable.video_pause_red);
                                slideInCaptureButton();
                            }
                        });
                    } catch (Throwable throwable) {
                        return;
                    }
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }
        });
        countDownItem.startAnimation(zoomout);
    }

    private void startRecording()
    {
        try
        {
            if (speedSelected == FAST)
                mEffects.add("fast_motion");

            TrackingManager.getInstance().recordStart(clipID, artistName, songName);

            //Keep nav bar hidden during recording (fucking htc bullshit os ui)
            boolean hasMenuKey = ViewConfiguration.get(getApplicationContext()).hasPermanentMenuKey();
            boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
            if (!hasMenuKey && !hasBackKey)
            {
                mNavBarTimer = new Timer();
                mNavBarTimer.scheduleAtFixedRate(new TimerTask()
                {
                    @Override
                    public void run()
                    {
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                View decorView = getWindow().getDecorView();
                                decorView.setSystemUiVisibility(
                                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                                        View.SYSTEM_UI_FLAG_FULLSCREEN |
                                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                                );
                            }
                        });
                    }
                }, 0, 2000);
            }

            startClock();
            cameraRecorder.start();

        }
        catch (Throwable err)
        {
            err.printStackTrace();
        }
    }

    private void startClock()
    {
        startProgress();
        int period = 1000;
        clockTimer = new Timer();
//        time--;
        clockTimer.scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                time++;
                int minute = time / 60;
                seconds = time - (minute * 60);
//                minuteString = "";
//                if (minute < 10)
//                {
//                    minuteString = "0";
//                }
//                minuteString = minuteString + String.valueOf(minute);

//                secondString = "";
//                if (seconds < 10) {
//                    secondString = "0";
//                }
//                secondString = secondString + String.valueOf(seconds);

                System.out.println(">>>>>time.>>>>"+seconds);

                if (isBlingyMode)
                {
                    checkTimeSegments(seconds);
                }
                if (seconds == 15 * mRecordSpeed)
                {
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            mStopButton.callOnClick();
                        }
                    });
                }

                if (!isStopButtonShowed && seconds > 5 * mRecordSpeed)
                {
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if (mCaptureButton != null)
                            {
                                activeStopButton();
                            }
                        }
                    });
                }

            }
        }, 1000, period);
    }

    private void checkTimeSegments(int sec)
    {
        if (speedSelected == FAST)
        {
            if (mFastTimeSegments.contains(sec))
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        pauseRecording();
                        if (isBlingyMode)
                            mReadyText.setVisibility(View.VISIBLE);
                    }
                });
            }
        }
        else
        {
            if (mTimeSegments.contains(sec))
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        pauseRecording();
                        if (isBlingyMode)
                            mReadyText.setVisibility(View.VISIBLE);
                    }
                });
            }
        }

    }

    private synchronized void endRecording()
    {
        try
        {
            mCaptureButton.setClickable(false);
            mCaptureButton.setEnabled(false);
            if (clockTimer != null)
            {
                clockTimer.purge();
                clockTimer.cancel();
            }
            isRecording = false;
            Constants.isBackCamera = !isCameraFront;

            if(isActivityVisible)
                prepareAndGoToNextActivity();
            else
                isDoneProcessing = true;
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
        }
        close();
    }

    private void prepareAndGoToNextActivity()
    {
        if(mSolidBackgroundDialog != null)
        {
            mSolidBackgroundDialog.superDismiss();
            mSolidBackgroundDialog = null;
        }

        intent.putExtra(Constants.PORTRAIT, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        overridePendingTransition(0, 0);
        startActivity(intent);
        dismissBlurDialog();
        Tic.tac(445533);
        finish();
    }

    public void dismissBlurDialog()
    {
        try{
            if(blurLoadingDialog != null)// && blurLoadingDialog.isShowing())
            {
                blurLoadingDialog.superDismiss();
                blurLoadingDialog = null;
            }
        }catch (Throwable throwable){
            Log.d(TAG,"blur-dismiss-failed");
        }
    }


    public void hideNavBar()
    {
        boolean hasMenuKey = ViewConfiguration.get(getApplicationContext()).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        if (!hasMenuKey && !hasBackKey)
        {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public void UiChangeListener()
    {
        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
        {
            @Override
            public void onSystemUiVisibilityChange(int visibility)
            {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                {
                    decorView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                    if (mSolidBackgroundDialog != null)
                        mSolidBackgroundDialog.hideNavigation();

                }
            }
        });
    }

    public ImageLoaderManager getImageLoaderManager()
    {
        return mImageLoaderManager;
    }

    private void deactiveStopButton()
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG,"Create>>>deactiveStopButton");
        if (mStopButton != null)
        {
            mStopButton.setActivated(false);
            mStopButton.setClickable(false);
            mStopButton.setEnabled(false);
            mStopButton.setAlpha(0.3f);
        }
        else {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "Create>>>activeStopButton>>null");
        }
    }

    private void activeStopButton()
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG,"Create>>>activeStopButton");
        if (mStopButton != null)
        {
            ObjectAnimator animStop = ObjectAnimator.ofFloat(mStopButton, "translationY", 500f, 0f);
            animStop.setDuration(1000);
            mStopButton.setVisibility(View.VISIBLE);
            isStopButtonShowed = true;
            animStop.start();
            mStopButton.postInvalidateOnAnimation();

            mStopButton.setActivated(true);
            mStopButton.setClickable(true);
            mStopButton.setEnabled(true);
            mStopButton.setAlpha(1f);
        }
        else {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "Create>>>activeStopButton>>null");
        }
    }

    private void hideRedetect()
    {
        ObjectAnimator anim = ObjectAnimator.ofFloat(mRedetectButton, "translationY", App.WIDTH - mRedetectButton.getBottom());
        anim.setDuration(1000);
        activeCaptureButton();
        anim.start();
        mRedetectButton.postInvalidateOnAnimation();
    }

    private void showRedetect()
    {
        ObjectAnimator anim = ObjectAnimator.ofFloat(mRedetectButton, "translationY", 500f, 0f);
        anim.setDuration(1000);
        activeCaptureButton();
        anim.start();
        mRedetectButton.postInvalidateOnAnimation();
    }

    private void deactiveCaptureButton()
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG,"Create>>>deactiveCaptureButton");
        if (mCaptureButton != null)
        {
            mCaptureButton.setActivated(false);
            mCaptureButton.setClickable(false);
            mCaptureButton.setEnabled(false);
            mCaptureButton.setAlpha(0.3f);
        }
        else {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "Create>>>mCaptureButton>>null");
        }
    }

    private void activeCaptureButton()
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG,"Create>>>activeCaptureButton");
        if (mCaptureButton != null)
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    mCaptureButton.setActivated(true);
                    mCaptureButton.setClickable(true);
                    mCaptureButton.setEnabled(true);
                    mCaptureButton.setAlpha(1f);
                }
            });
        }
        else {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "Create>>>mCaptureButton>>null");
        }
        if(isStopButtonShowed)
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    activeStopButton();
                }
            });
    }

    private void slideInCaptureButton()
    {
        ObjectAnimator anim = ObjectAnimator.ofFloat(mRecordButtonContainer, "translationY", 500f, 0f);
        anim.setDuration(1000);
        activeCaptureButton();
        mCaptureButton.setVisibility(View.VISIBLE);
        anim.start();
        mCaptureButton.postInvalidateOnAnimation();
    }

//    private void setUpPhotoMode()
//    {
//        closeHyperlapse();
//        //Capture photo
//        mCaptureButton.setImageResource(R.drawable.photo_mode_active);
//        isPhotoMode = true;
//    }

    private void setUpVideoMode()
    {
        //Capture video
        openHyperLapse();
        mCaptureButton.setImageResource(R.drawable.video_mode_active);
        isPhotoMode = false;
    }

    private void setUpRecordingMode()
    {
        mCaptureButton.setImageResource(R.drawable.video_pause_red);
        mStopButton.setVisibility(View.INVISIBLE);
        isStopButtonShowed = false;
        closeHyperlapse();
    }

    private void showMenuItems()
    {
        mRotate.setVisibility(View.VISIBLE);
    }

    private void hideMenuItems()
    {
        mRotate.setVisibility(View.INVISIBLE);
    }

    private void restartClockTime()
    {
        time = 0;
    }

    private void stopClock()
    {
        stopProgress();
        if (clockTimer != null)
        {
            clockTimer.purge();
            clockTimer.cancel();
        }
    }

    private void resumeRecording()
    {
        mTimer.setEnabled(false);
        mTimer.setClickable(false);

        isPauseMode = false;
        isPaused = false;
        isStopped = false;

        if (speedSelected == FAST && !mEffects.contains("fast_motion"))
            mEffects.add("fast_motion");

        mCaptureButton.setImageResource(R.drawable.video_pause_red);
        mResumePauseText.setVisibility(View.INVISIBLE);
        hideRedetect();
        try
        {
            cameraRecorder.start();
        }
        catch (Throwable err)
        {
        }
        startClock();
        if (isBlingyMode)
            mReadyText.setVisibility(View.INVISIBLE);
    }

    private synchronized void pauseRecording()
    {
        if(isPaused || cameraRecorder == null) return;
        isPaused = true;

        mTimer.setEnabled(true);
        mTimer.setClickable(true);
        stopClock();
        TrackingManager.getInstance().recordPause(clipID,artistName,songName);
        showRedetect();
        cameraRecorder.pause();
        isPauseMode = true;
        mCaptureButton.setImageResource(R.drawable.video_mode_active);
        mResumePauseText.setText(R.string.resume);
        mResumePauseText.setVisibility(View.VISIBLE);
    }

    boolean isStopped = false;
    private synchronized void stopRecording()
    {
        if(isStopped) return;
        isStopped = true;
        isRecording = false;

        stopClock();
        try
        {
            cameraRecorder.stop();
            TrackingManager.getInstance().recordStop(clipID,artistName,songName);
        }catch (Throwable err){}
    }

    public void deleteTempFile()
    {
//        try
//        {
//            File file = new File(App.VIDEO_WORKING_FOLDER + "/blingy_vid.mp4");
//            file.delete();
//        }
//        catch (Throwable t)
//        {
//        }
    }

    @Override
    public void onChoiceSelected(int choice)
    {
        mTimerChoice = choice;
        switch (choice)
        {
            case TimerDialog.CHOICE_0:
                mTimer.setImageResource(R.drawable.ic_timer_off_white_24dp);
                break;
            case TimerDialog.CHOICE_3:
                mTimer.setImageResource(R.drawable.ic_timer_3_white_24dp);
                break;
            case TimerDialog.CHOICE_5:
                mTimer.setImageResource(R.drawable.ic_timer_5_white_24dp);
                break;
        }
    }

    @Override
    public void doDetectBackground()
    {
        if (cameraRecorder != null)
        {
            if (mReDetectCount == 6)
            {
                mReDetectCount = 0;
                new DetectTipsDialog(this, this);
            }
            else
            {

                mVideoTutorial.setVisibility(View.GONE);
                mReadyText.setVisibility(View.GONE);
                mDetectProgress.setVisibility(View.VISIBLE);
                mDetectMainButton.setVisibility(View.INVISIBLE);
//                mSubInfo.setVisibility(View.INVISIBLE);
                mMainInfo.setVisibility(View.VISIBLE);
                mHyperlapseContainer.setVisibility(View.GONE);
                mMainInfo.setText(R.string.detecting_stand_still);
                mRecordButtons.setVisibility(View.GONE);

                if (mRecordButtons.getVisibility() == View.VISIBLE)
                {
                    mSolidBackgroundDialog = new SolidBackgroundOnboarding(CreateVideoActivity.this, recordLayout, isCameraFront);

                }
                else
                {
                    mSolidBackgroundDialog.showSilhouette();
                    if (!mSolidBackgroundDialog.isShowing())
                        mSolidBackgroundDialog.show();
                    TrackingManager.getInstance().detectStart();
                    Tic.tic(DETECTED_ID);
                }
                cameraRecorder.detect();
            }
        }
    }

    public interface ActivateDeactivateButtonsDispatcher
    {
        void activateButtons();

        void deActivateButtons();
    }

    private void setupNormalRecord()
    {
        mNormalMotionButton.setBackgroundResource(R.drawable.left_rounded_rect_white);
        mNormalMotionButton.setTextColor(Color.BLACK);
        mFastMotionButton.setBackgroundResource(R.drawable.right_rounded_rect);
        mFastMotionButton.setTextColor(Color.WHITE);
        mAudioSpeed = 1f;
        mRecordSpeed = 1f;
        speedSelected = NORMAL;
    }

    private void setupFastRecord()
    {
        mNormalMotionButton.setBackgroundResource(R.drawable.left_rounded_rect);
        mNormalMotionButton.setTextColor(Color.WHITE);
        mFastMotionButton.setBackgroundResource(R.drawable.right_rounded_rect_white);
        mFastMotionButton.setTextColor(Color.BLACK);
        speedSelected = FAST;
        mAudioSpeed = 0.5f;
        mRecordSpeed = 2f;
    }

    private long elpasedTime = 0L;
    private boolean runProgress;

    private void startProgress()
    {
        runProgress = true;
        progressRect.bottom = mProgressValue.getHeight();
        new Thread()
        {
            public void run()
            {
                long lastTime = SystemClock.elapsedRealtime();
                long dif;

                final float progressPart = ((float) mProgressValue.getWidth()) / ((float) ((VIDEO_MAX_LENGTH * mRecordSpeed) - 100));

                while (runProgress)
                {
                    try
                    {
                        Thread.sleep(40);
                    }
                    catch (Throwable err)
                    {
                    }
                    dif = SystemClock.elapsedRealtime() - lastTime;
                    lastTime = SystemClock.elapsedRealtime();
                    elpasedTime += dif;
                    progressRect.right += ((float) dif) * progressPart;
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            mProgressValue.invalidate();
                        }
                    });
                }
            }
        }.start();
    }

    private void stopProgress()
    {
        runProgress = false;
    }

    public void removeAllViews()
    {
        recordLayout.removeAllViews();
    }

    private void close()
    {
        if(mPercentageProgress != null)
            mPercentageProgress.close();

        if(mDetectProgress != null)
            mDetectProgress.close();

        try{cameraRecorder.close();}catch (Throwable err){}


        mPercentageProgress = null;
        mDetectProgress = null;
    }

}
