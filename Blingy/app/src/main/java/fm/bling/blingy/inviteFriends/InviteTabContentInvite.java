package fm.bling.blingy.inviteFriends;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.EnterNameDialog;
import fm.bling.blingy.dialogs.listeners.EnterNameDialogListener;
import fm.bling.blingy.inviteFriends.adapters.RecyclerAdapterInvite;
import fm.bling.blingy.inviteFriends.listeners.ErrorListener;
import fm.bling.blingy.inviteFriends.listeners.InvalidateNotificationListener;
import fm.bling.blingy.inviteFriends.model.CAAEmailInvite;
import fm.bling.blingy.inviteFriends.model.CAAEmailInvites;
import fm.bling.blingy.inviteFriends.model.CAASmsInvite;
import fm.bling.blingy.inviteFriends.model.CAASmsInvites;
import fm.bling.blingy.inviteFriends.model.ContactInfo;
import fm.bling.blingy.registration.RegistrationActivity;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.dialogs.LoadingDialog;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.ClickableTextView;
import fm.bling.blingy.widget.CAAWidgetSettingsSingleton;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/14/16.
 * History:
 * ***********************************
 */
public class InviteTabContentInvite extends InviteTabContentBase
{
    private LinearLayout inviteAllLlinearLayout;
    private ClickableTextView inviteALL;
    private TextView numberOfContacts;

    private RecyclerView mRecyclerView;
    private RecyclerAdapterInvite mAdapter;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<ContactInfo> mDataSet;
    private LoadingDialog loadingDialog;
    private boolean isFirstReleased;
    private boolean transparent = false;
    private RegistrationActivity registrationActivity;
    private InvalidateNotificationListener mInvalidateNotificationListener;
    private Context mContext;

    public InviteTabContentInvite(Activity context, Paint paint, ArrayList<ContactInfo> dataSet, ImageLoaderManager imageLoaderManager, InvalidateNotificationListener invalidateNotificationListener, boolean transparent)
    {
        super(context, paint, false);
        mContext = context;
        this.mDataSet = dataSet;
        mInvalidateNotificationListener = invalidateNotificationListener;
        this.transparent = transparent;
        setFirstLine(context);
        setContent(context, imageLoaderManager);
        if (transparent)
        {
            if (context instanceof RegistrationActivity)
            {
                registrationActivity = (RegistrationActivity) context;
            }
            setBackgroundColor(getResources().getColor(R.color.transparent));
        }
    }

    private void setFirstLine(Context context)
    {
        numberOfContacts = new TextView(context);
        numberOfContacts.setTypeface(App.ROBOTO_REGULAR);
        int color = R.color.white;
        if (!transparent) color = R.color.grey_font;
        numberOfContacts.setTextColor(context.getResources().getColor(color));
        numberOfContacts.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(R.dimen.font_description));
        numberOfContacts.setPadding(padding, 0, 0, 0);
        numberOfContacts.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));
        numberOfContacts.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        if(mDataSet == null || mDataSet.size() == 0)
            numberOfContacts.setText(R.string.Contacts);
        else numberOfContacts.setText(mDataSet.size() + " " + getContext().getResources().getString(R.string.Contacts));

        numberOfContacts.setIncludeFontPadding(false);

        inviteALL = new ClickableTextView(context);
        inviteALL.setTypeface(App.ROBOTO_MEDIUM);
        inviteALL.setTextColor(context.getResources().getColor(transparent ? R.color.white : R.color.grey_font));
        inviteALL.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(R.dimen.font_description));
        inviteALL.setText(R.string.invite_all);
        inviteALL.setPadding(padding, 0, padding, 0);

        inviteALL.setLayoutParams(new LinearLayout.LayoutParams((int) (300f * App.SCALE_X), (int) (100f * App.SCALE_Y)));//context.getResources().getDimensionPixelSize(R.dimen.invite_all_button)));

        inviteALL.setGravity(Gravity.CENTER);
        inviteALL.setTextColor(Color.WHITE);
        inviteALL.setBackgroundResource(R.drawable.button_shape_active_green);
        inviteALL.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                inviteAll();
            }
        });
        inviteALL.setClickable(true);

        inviteAllLlinearLayout = new LinearLayout(context);
        inviteAllLlinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        inviteAllLlinearLayout.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
        inviteAllLlinearLayout.setPadding(0, 0, padding, 0);
        inviteAllLlinearLayout.addView(inviteALL);

        firstLine.addView(numberOfContacts);
        firstLine.addView(inviteAllLlinearLayout);
    }

    private void setContent(Context context, ImageLoaderManager imageLoaderManager)
    {
        mRecyclerView = new RecyclerView(context);
        mRecyclerView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new RecyclerAdapterInvite(context, mDataSet, imageLoaderManager, new InvalidateNotificationListener()
        {
            @Override
            public void invalidateNotification()
            {
                mAdapter.notifyDataSetChanged();
                if (mInvalidateNotificationListener != null) {
                    mInvalidateNotificationListener.invalidateNotification();
                }
                if (registrationActivity != null) {
                    registrationActivity.letsPlay();
                    registrationActivity = null;
                }
            }
        }, new ErrorListener()
        {
            @Override
            public void onError()
            {
                showNetworkErrorDialog();
            }
        }, transparent);
        mRecyclerView.setAdapter(mAdapter);


        addView(mRecyclerView);

        numberOfContacts.append(mDataSet.size() + " ", 0, 0);
        numberOfContacts.invalidate();
    }

    public void setData(ArrayList<ContactInfo> dataSet)
    {
        this.mDataSet = dataSet;
        numberOfContacts.setText(dataSet.size() + " " + getContext().getResources().getString(R.string.Contacts));
        mAdapter.setData(dataSet);
        numberOfContacts.invalidate();
    }

    private void inviteAll()
    {
        if (registrationActivity != null) {
            registrationActivity.letsPlay();
            registrationActivity = null;
        }
        String name = CAAUserDataSingleton.getInstance().getFullName().trim();
        if(name.length() == 0)
        {
            EnterNameDialog enterNameDialog = new EnterNameDialog(getContext(), R.string.invite_enter_your_name, R.string.invite_enter_your_name_dialog_wrong_name, 3, new EnterNameDialogListener()
            {
                @Override
                public void onDoneClicked()
                {
                    String name = CAAUserDataSingleton.getInstance().getFullName();
                    if (name.length() == 0)
                        return;
                    inviteAll();
                }

                @Override
                public void onCancel()
                {

                }
            });
            enterNameDialog.show();
            return;
        }
        loadingDialog = new LoadingDialog(getContext(),false,false);

        ArrayList<CAASmsInvite> caaSmsInviteArrayList = new ArrayList<>();
        ArrayList<CAAEmailInvite>caaEmailInviteArrayList = new ArrayList<>();

        for(ContactInfo contactInfo : mDataSet)
        {
            if(contactInfo.status != ContactInfo.NOT_INVITED)
                continue;
            if (contactInfo.isPhoneNumberSet())
            {
                caaSmsInviteArrayList.add(new CAASmsInvite(contactInfo.phone(), contactInfo.getName()));
            }
            else
            {
                caaEmailInviteArrayList.add(new CAAEmailInvite(contactInfo.email, contactInfo.name));
            }
        }

        if(caaSmsInviteArrayList.size() > 0)
        {
            CAASmsInvites caaSmsInvites = new CAASmsInvites(caaSmsInviteArrayList);
            App.getUrlService().postSmsInvite(caaSmsInvites, new Callback<String>() {
                @Override
                public void success(String s, Response response) {
                    synchronized (mDataSet) {
                        for (ContactInfo contactInfo : mDataSet) {
                            if (contactInfo.isPhoneNumberSet()) {
                                StateMachine.getInstance(mContext.getApplicationContext()).excludeFromInvites(contactInfo.phone(), StateMachine.INVITE_EXCLUDE_TYPE_PHONE);
                                contactInfo.status = ContactInfo.INVITED;
                            }
                        }
                        if (isFirstReleased) {
                            loadingDialog.dismiss();
                            mAdapter.notifyDataSetChanged();
                            if (mInvalidateNotificationListener != null)
                                mInvalidateNotificationListener.invalidateNotification();
                        } else isFirstReleased = true;
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (isFirstReleased)
                        loadingDialog.dismiss();
                    else isFirstReleased = true;
                    showNetworkErrorDialog();
                }
            });

        }
        else isFirstReleased = true;

        if(caaEmailInviteArrayList.size() > 0)
        {
            CAAEmailInvites caaEmailInvites = new CAAEmailInvites(caaEmailInviteArrayList, name);
            App.getUrlService().postEmailInvite(caaEmailInvites, new Callback<String>() {
                @Override
                public void success(String s, Response response) {
                    synchronized (mDataSet) {
                        for (ContactInfo contactInfo : mDataSet) {
                            if (!contactInfo.isPhoneNumberSet()) {
                                contactInfo.status = ContactInfo.INVITED;
                                StateMachine.getInstance(mContext.getApplicationContext()).excludeFromInvites(contactInfo.email, StateMachine.INVITE_EXCLUDE_TYPE_EMAIL);
                            }
                        }

                        if (isFirstReleased) {
                            loadingDialog.dismiss();
                            mAdapter.notifyDataSetChanged();
                            if (mInvalidateNotificationListener != null) {
                                mInvalidateNotificationListener.invalidateNotification();
                            }
                        } else isFirstReleased = true;
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (isFirstReleased)
                        loadingDialog.dismiss();
                    else isFirstReleased = true;
                    showNetworkErrorDialog();
                }
            });
        }
        else
        {
            if(isFirstReleased)
                loadingDialog.dismiss();
            else isFirstReleased = true;
        }

        int sum = CAAWidgetSettingsSingleton.getInstance().getMyPoints() + (CAAWidgetSettingsSingleton.getInstance().getInviteWorth() * (caaEmailInviteArrayList.size() + caaSmsInviteArrayList.size()));
        CAAWidgetSettingsSingleton.getInstance().setMyPoints(sum);
        StateMachine.getInstance(mContext).insertEvent(new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL, StateMachineEvent.EVENT_TYPE.INVITE_FRIEND.ordinal()));
    }

    public void invite(ContactInfo contactInfo)
    {
        mAdapter.inviteFriend(contactInfo);
    }

    public void close()
    {
        super.close();

        try { inviteAllLlinearLayout.removeAllViews();inviteAllLlinearLayout.destroyDrawingCache(); } catch (Throwable xz){}inviteAllLlinearLayout = null;
        inviteALL.close();
        numberOfContacts = null;

        mRecyclerView.removeAllViews(); mRecyclerView.destroyDrawingCache();mRecyclerView = null;
        mAdapter.close();
        mLayoutManager = null;
        mDataSet = null;
    }

    public void notifyDataSetChanged()
    {
        post(new Runnable()
        {
            @Override
            public void run()
            {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

}