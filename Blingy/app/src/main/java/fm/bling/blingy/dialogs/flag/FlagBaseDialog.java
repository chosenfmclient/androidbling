package fm.bling.blingy.dialogs.flag;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashMap;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.adapters.CustomDialogAdapter;
import fm.bling.blingy.dialogs.listeners.FlagDialogListener;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 8/1/16.
 * History:
 * ***********************************
 */
public abstract class FlagBaseDialog extends Dialog{

    protected Context mContext;
    protected Handler mHandler;
    protected ListView listView;
    protected TextView flagTitle;
    protected TextView flagText;
    protected TextView flagButton;
    protected TextView canelButton;
    protected LinearLayout cancelReportContainer;
    protected FlagDialogListener mFlagDialogListener;

    protected String reportType;
    protected String flagType;
    protected CustomDialogAdapter adapter;
    protected HashMap<String, String> map = new HashMap<>();

    protected String[] firstDialogList = new String[]{"Inappropriate", "Spam"};
    protected String[] flagsDialogList = new String[]{"Sexual Content", "Violent or repulsive content", "Hateful or abusive content", "Harmful dangerous acts", "Infringes my rights"};
    protected boolean inFirstDialog = true;


    public FlagBaseDialog(Context context, FlagDialogListener flagDialogListener) {
        super(context, android.R.style.Theme_Panel);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.dimAmount = 0.5f;
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);
        setCanceledOnTouchOutside(true);

        setContentView(R.layout.flag_dialog_fragment);
        init();
        this.mContext = context;
        this.mHandler = new Handler();
        this.mFlagDialogListener = flagDialogListener;

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (inFirstDialog) {
                    reportType = firstDialogList[position];
                    inFirstDialog = false;
                    if (reportType.equalsIgnoreCase("spam")) {
                        showSpamDialogView();
                    } else {
                        listView.setAdapter(new CustomDialogAdapter(mContext, flagsDialogList));
                        listView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                    }
                } else {
                    flagType = flagsDialogList[position];
                    sendReport();
                }

            }
        });

        flagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flagType = reportType;
                sendReport();
            }
        });

        canelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    protected abstract void sendReport();

    private void init(){
        listView    = (ListView)findViewById(R.id.flag_list_view);
        flagTitle   = (TextView)findViewById(R.id.flag_title);
        flagText    = (TextView)findViewById(R.id.flag_text);
        flagButton  = (TextView)findViewById(R.id.flag_video_button);
        canelButton = (TextView)findViewById(R.id.cancel_button);
        cancelReportContainer = (LinearLayout)findViewById(R.id.report_cancel_container);
        flagTitle.setTypeface(App.ROBOTO_MEDIUM);
    }

    protected void showSpamDialogView() {
        cancelReportContainer.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        flagTitle.setText("Are you sure?");
        flagText.setVisibility(View.VISIBLE);
    }

}
