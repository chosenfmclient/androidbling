package fm.bling.blingy.utils.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import fm.bling.blingy.App;
import fm.bling.blingy.R;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/14/16.
 * History:
 * ***********************************
 */
public class CountDownItem extends FrameLayout {

    private TextView mNumber;
    public CountDownItem(Context context, String text) {
        super(context);
        int widthHeight = (int)(300f * App.SCALE_Y);
        LayoutParams mainParams = new LayoutParams(widthHeight, widthHeight);
        mainParams.gravity = Gravity.CENTER;
        setLayoutParams(mainParams);
        setBackgroundResource(R.drawable.circle_shape_main_blue);

        FrameLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        mNumber = new TextView(context);
        mNumber.setLayoutParams(params);
        mNumber.setText(text);
        mNumber.setTextSize(TypedValue.COMPLEX_UNIT_SP, 45f * App.SCALE_Y);
        mNumber.setTextColor(ContextCompat.getColor(context,R.color.white));

        this.addView(mNumber);

    }

    public CountDownItem(Context context, String text, int backgroundResId) {
        super(context);
        int widthHeight = (int)(300f * App.SCALE_Y);
        LayoutParams mainParams = new LayoutParams(widthHeight, widthHeight);
        mainParams.gravity = Gravity.CENTER;
        setLayoutParams(mainParams);
        setBackgroundResource(backgroundResId);

        FrameLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        mNumber = new TextView(context);
        mNumber.setLayoutParams(params);
        mNumber.setText(text);
        mNumber.setTextSize(TypedValue.COMPLEX_UNIT_SP, 45f * App.SCALE_Y);
        mNumber.setTextColor(ContextCompat.getColor(context,R.color.white));

        this.addView(mNumber);

    }

    public void setTextSize(int unit, float size){
        mNumber.setTextSize(unit, size);
    }

    public void setTextColor(int resColor){
        mNumber.setTextColor(ContextCompat.getColor(mNumber.getContext(), resColor));
    }


    public void updateText(String text){
        if(mNumber != null){
            mNumber.setText(text);
        }
    }
}
