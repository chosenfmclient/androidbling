package fm.bling.blingy.record;

import android.annotation.TargetApi;
import android.app.ActivityManager;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;

import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import fm.bling.blingy.App;
import fm.bling.blingy.rest.model.CAAStatus;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.videoHome.model.CAAVideo;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: Utils
 * Description:
 * Created by Dawidowicz Nadav on 6/4/15.
 * History:
 * Edited and Updated By Oren Zakay 10/12/15.
 * ***********************************
 */
public class Utils {
//    public static final int MEDIA_TYPE_IMAGE = 1;
//    public static final int MEDIA_TYPE_VIDEO = 2;
//    // Orientation hysteresis amount used in rounding, in degrees
//    public static final int ORIENTATION_HYSTERESIS = 5;
//    private static int mRotation = 90;
//    public static ArrayList<String> videoIds = new ArrayList<String>();

    private static long lastTimeClicked;
    private static long clickTimeLimit = 1000;

//    /**
//     * Function to convert milliseconds time to
//     * Timer Format
//     * Hours:Minutes:Seconds
//     */
//    public String milliSecondsToTimer(long milliseconds) {
//        String finalTimerString = "";
//        String secondsString = "";
//
//        // Convert total duration into time
//        int hours = (int) (milliseconds / (1000 * 60 * 60));
//        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
//        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
//        // Add hours if there
//        if (hours > 0) {
//            finalTimerString = hours + ":";
//        }
//
//        // Prepending 0 to seconds if it is one digit
//        if (seconds < 10) {
//            secondsString = "0" + seconds;
//        } else {
//            secondsString = "" + seconds;
//        }
//
//        finalTimerString = finalTimerString + minutes + ":" + secondsString;
//
//        // return timer string
//        return finalTimerString;
//    }

//    /**
//     * Function to get Progress percentage
//     *
//     * @param currentDuration
//     * @param totalDuration
//     */
//    public int getProgressPercentage(long currentDuration, long totalDuration) {
//        Double percentage = (double) 0;
//
//        long currentSeconds = (int) (currentDuration / 1000);
//        long totalSeconds = (int) (totalDuration / 1000);
//
//        // calculating percentage
//        percentage = (((double) currentSeconds) / totalSeconds) * 100;
//        // return percentage
//        return percentage.intValue();
//    }

//    public int getProgressPercentageGames(long currentDuration, long totalDuration) {
//        Double percentage = (double) 0;
//
//        long currentSeconds = (int) (currentDuration / 1000);
//        long totalSeconds = (int) (totalDuration / 1000);
//
//        // calculating percentage
//        percentage = (((double) currentSeconds) / totalSeconds) * 360;
//
//        // return percentage
//        return percentage.intValue();
//    }

//    /**
//     * Function to change progress to timer
//     *
//     * @param progress      -
//     * @param totalDuration returns current duration in milliseconds
//     */
//    public int progressToTimer(int progress, int totalDuration) {
//        int currentDuration = 0;
//        totalDuration = (int) (totalDuration / 1000);
//        currentDuration = (int) ((((double) progress) / 100) * totalDuration);
//
//        // return current duration in milliseconds
//        return currentDuration * 1000;
//    }

//    /**
//     * Create a file Uri for saving an image or video
//     */
//    public static Uri getOutputMediaFileUri(int type) {
//        return Uri.fromFile(getOutputMediaFile(type));
//    }

//    /**
//     * Create a File for saving an image or video
//     */
//    public static File getOutputMediaFile(int type) {
//        // To be safe, you should check that the SDCard is mounted
//        // using Environment.getExternalStorageState() before doing this.
//
//        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_PICTURES), "MyCameraApp");
//        // This location works best if you want the created images to be shared
//        // between applications and persist after your app has been uninstalled.
//
//        // Create the storage directory if it does not exist
//        if (!mediaStorageDir.exists()) {
//            if (!mediaStorageDir.mkdirs()) {
//                Log.d("MyCameraApp", "failed to create directory");
//                return null;
//            }
//        }
//
//        // Create a media file name
////        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//        String timeStamp = "Chosen";
//        File mediaFile;
//        if (type == MEDIA_TYPE_IMAGE) {
//            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
//                    "IMG_" + timeStamp + ".png");
//        } else if (type == MEDIA_TYPE_VIDEO) {
//            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
//                    "VID_" + timeStamp + ".mp4");
//        } else {
//            return null;
//        }
//
//        return mediaFile;
//    }

//    //api time unit is microseconds
//    public static synchronized Bitmap createThumbnailAtTime(String path, long timeInSeconds) {
//
//        MediaMetadataRetriever mMMR = new MediaMetadataRetriever();
//        Bitmap returnedBitmap = null;
//        try{
//            mMMR.setDataSource(path);
//        }
//        catch (Throwable err){
//            try {
//                mMMR.setDataSource(path, new HashMap<String, String>());
//            }catch(Throwable throwable) {
//                return returnedBitmap;
//            }
//        }
//        Bitmap temp = mMMR.getFrameAtTime(timeInSeconds * 1000000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
//
//        if (temp != null) {
//            int[] size = getBitmapWidthHeight(temp);
//            returnedBitmap = Bitmap.createScaledBitmap(temp, size[0], size[1], false);
//
//            if (returnedBitmap != temp) {
//                temp.recycle();
//                temp = null;
//            }
//        }
//
//        if(returnedBitmap == null){
//            //mMMR.setDataSource(path);
//            Bitmap temp1 = mMMR.getFrameAtTime(timeInSeconds * 1000000, MediaMetadataRetriever.OPTION_CLOSEST);
//            if(temp1 != null)
//            {
//                int[] size = getBitmapWidthHeight(temp1);
//                returnedBitmap = Bitmap.createScaledBitmap(temp1, size[0], size[1], false);
//                if (returnedBitmap != temp1)
//                {
//                    temp1.recycle();
//                    temp1 = null;
//                }
//            }
//            else returnedBitmap = mMMR.getFrameAtTime(timeInSeconds * 1000000, MediaMetadataRetriever.OPTION_CLOSEST);
//        }
//
//        mMMR.release();
//        return returnedBitmap;
//
//    }

//    private static int[] getBitmapWidthHeight(Bitmap temp) {
//        int width = temp.getWidth();
//        int height = temp.getHeight();
//
//        int maxHeight = 468;
//        int maxWidth = 320;
//
//        if (width > height) {
//            // landscape
//            float ratio = (float) width / maxWidth;
//            width = maxWidth;
//            height = (int) (height / ratio);
//        } else if (height > width) {
//            // portrait
//            float ratio = (float) height / maxHeight;
//            height = maxHeight;
//            width = (int) (width / ratio);
//        } else {
//            // square
//            height = maxHeight;
//            width = maxWidth;
//        }
//        int[] size = new int[2];
//        size[0] = width;
//        size[1] = height;
//
//        return size;
//    }

//    public static Bitmap createThumnail(String path){
//        return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND) ;
//    }


//    /**
//     * Rounds the orientation so that the UI doesn't rotate if the user
//     * holds the device towards the floor or the sky
//     *
//     * @param orientation        New orientation
//     * @param orientationHistory Previous orientation
//     * @return Rounded orientation
//     */
//    public static int roundOrientation(int orientation, int orientationHistory) {
//        boolean changeOrientation = false;
//        if (orientationHistory == OrientationEventListener.ORIENTATION_UNKNOWN) {
//            changeOrientation = true;
//        } else {
//            int dist = Math.abs(orientation - orientationHistory);
//            dist = Math.min(dist, 360 - dist);
//            changeOrientation = (dist >= 45 + ORIENTATION_HYSTERESIS);
//        }
//        if (changeOrientation) {
//            return ((orientation + 45) / 90 * 90) % 360;
//        }
//        return orientationHistory;
//    }

//    /**
//     * Returns the orientation of the display
//     * In our case, since we're locked in Landscape, it should always
//     * be 90
//     *
//     * @param activity
//     * @return Orientation angle of the display
//     */
//    public static int getDisplayRotation(Activity activity) {
//        if (activity != null) {
//            int rotation = activity.getWindowManager().getDefaultDisplay()
//                    .getRotation();
//            switch (rotation) {
//                case Surface.ROTATION_0:
//                    mRotation = 0;
//                    break;
//                case Surface.ROTATION_90:
//                    mRotation = 90;
//                    break;
//                case Surface.ROTATION_180:
//                    mRotation = 180;
//                    break;
//                case Surface.ROTATION_270:
//                    mRotation = 270;
//                    break;
//            }
//        }
//
//        return mRotation;
//    }

//    /**
//     * Gets the corresponding path to a file from the given content:// URI
//     *
//     * @param selectedVideoUri The content:// URI to find the file path from
//     * @param contentResolver  The content resolver to use to perform the query.
//     * @return the file path as a string
//     */
//    public String getFilePathFromContentUri(Uri selectedVideoUri, ContentResolver contentResolver) {
//        String filePath;
//        String[] filePathColumn = {MediaStore.MediaColumns.DATA};
//
//        Cursor cursor = contentResolver.query(selectedVideoUri, filePathColumn, null, null, null);
//        cursor.moveToFirst();
//
//        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//        filePath = cursor.getString(columnIndex);
//        cursor.close();
//        return filePath;
//    }

//    public static void uploadVideo(CAAVideoPostResponse cAAVideoPostResponse, String path, final String videoID, String VideoUrlName) {
//        RestClientUploadFile one = new RestClientUploadFile("http:");
//        String signedurl = "/storage.googleapis.com" + cAAVideoPostResponse.getUrl().getSignedUrl().getPath() + "?" + cAAVideoPostResponse.getUrl().getSignedUrl().getQuery();
//
//        File dcim = new File(path);
//
//        UrlPostService two = one.getUrlPostService();
//        TypedFile tfile = new TypedFile(VideoUrlName, dcim);
//        Log.d("cAAVideoPostResponse", signedurl);
//        two.putFileServer(cAAVideoPostResponse.getUrl().getContentType(), signedurl, tfile, new Callback<String>() {
//            @Override
//            public void success(String s, Response response) {
//                Log.d("VideoPostResponse pic", response.getReason());
//                VideofinishUploading("public", videoID);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Log.d("failure pic", error.toString());
//            }
//        });
//
//
//    }



//    public static void setCameraDisplayOrientation(Activity activity, int cameraId, Camera camera) {
//        if(camera !=null) {
//            Camera.CameraInfo info = new Camera.CameraInfo();
//            Camera.getCameraInfo(cameraId, info);
//            int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
//            int degrees = 0;
//            switch (rotation) {
//                case Surface.ROTATION_0:
//                    degrees = 0;
//                    break;
//                case Surface.ROTATION_90:
//                    degrees = 90;
//                    break;
//                case Surface.ROTATION_180:
//                    degrees = 180;
//                    break;
//                case Surface.ROTATION_270:
//                    degrees = 270;
//                    break;
//            }
//
//            int result;
//            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
//                result = (info.orientation + degrees) % 360;
//                result = (360 - result) % 360;  // compensate the mirror
//            } else {  // back-facing
//                result = (info.orientation - degrees + 360) % 360;
//            }
//            if (result != 0 && camera != null) {
//                camera.setDisplayOrientation(result);
//            }
//        }
//    }

    public static String getPathFromMediaUri(Context context, Uri uri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(uri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static boolean canClick(){
        if(SystemClock.uptimeMillis() - lastTimeClicked < clickTimeLimit)
            return false;

        lastTimeClicked = SystemClock.uptimeMillis();
        return true;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static Boolean userHasTwitterApp(Context context) {
        Boolean hasTwitterApp = true;
        PackageManager pkManager = context.getPackageManager();
        try
        {
            final String twiPackName = "com.twitter.android";
            PackageInfo pkgInfo = pkManager.getPackageInfo(twiPackName, 0);
            String getPkgInfo = pkgInfo.toString();
            Constants.mTwitterVerion = pkgInfo.versionName;

            if (getPkgInfo.equals(twiPackName))
            {
                // APP NOT INSTALLED
                hasTwitterApp = false;
            }
        }
        catch (PackageManager.NameNotFoundException e)
        {
            hasTwitterApp = false;
        }
        return hasTwitterApp;
    }

    public static boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (isScreenOn(context)) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isScreenOn(Context context){
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            return pm.isInteractive();

        else return pm.isScreenOn();
    }

//    public static void setAccessibilityIgnore(View view) {
//        view.setClickable(false);
//        view.setFocusable(false);
//        view.setContentDescription("");
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            view.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_NO);
//        }
//    }

    public static List<List<String>> partition(List<String> list, int size){
        int divide = list.size() / size;
        List<List<String>> resultList = new ArrayList<>();
        int i = 0;
        while(i < divide){
            resultList.add(i, list.subList(i * size, (i * size)  + size));
            i++;
        }
        if(divide * size < list.size())
            resultList.add(i,list.subList(divide * size, list.size()));

        return resultList;
    }

    /**
     * Gets the state of Airplane Mode.
     *
     * @param context
     * @return true if enabled.
     */
    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

//    /**
//     * The array should be sorted.
//     * @param arrays
//     * @return the same array without duplicate values.
//     */
//    public static void removeDuplicate(ArrayList<Integer> arrays) {
//        HashSet<Integer> hashSet = new HashSet<>();
//        hashSet.addAll(arrays);
//        arrays.clear();
//        arrays.addAll(hashSet);
//        hashSet = null;
////        int[] newArrays = new int[arrays.length];
////        int count = 0;
////        for (int i = 0; i < arrays.length - 1; i++) {
////            if (arrays[i] != arrays[i + 1]) {
////                newArrays[count] = arrays[i];
////                count++;
////            }
////        }
////        return newArrays;
//    }

    public static boolean userIsMe(String userID){
        return  userID.equals(CAAUserDataSingleton.getInstance().getUserId()) || userID.equalsIgnoreCase(Constants.ME);
    }

    public static int getDimensionPixelSize(int dimenID){
        return App.getAppContext().getResources().getDimensionPixelSize(dimenID);
    }


    public static User getMyUser() {
        CAAUserDataSingleton me = CAAUserDataSingleton.getInstance();
        return new User(me.getUserId(), me.getFirstName(), me.getLastName(), me.getPhotoUrl());
    }

    public static boolean copyFile(String inputPath, String outPath){
        try{
            int count;
            byte[] buffer = new byte[1024];
            InputStream in = new FileInputStream(inputPath);
            OutputStream out = new FileOutputStream(outPath);
            while((count = in.read(buffer))  != -1){
                out.write(buffer,0 , count);
            }
            out.flush();
            out.close();
            in.close();
            return true;
        }catch (Throwable throwable){
            return false;
        }
    }
}
