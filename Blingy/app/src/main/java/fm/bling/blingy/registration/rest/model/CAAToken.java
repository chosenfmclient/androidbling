package fm.bling.blingy.registration.rest.model;

import com.google.gson.annotations.SerializedName;


/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAToken
 * Description:
 * Created by Dawidowicz Nadav on 5/20/15.
 * History:
 * ***********************************
 */

public class CAAToken {
    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("expires_in")
    private String expiresIn;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("first_name")
    private String firstName;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @SerializedName("photo")
    private String photo;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }
}
