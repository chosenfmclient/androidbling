package fm.bling.blingy.record.overlay;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;

import fm.bling.blingy.record.CameraRecorder3;


/**
 * Created by ben on 02/06/2016.
 */
public abstract class OverlayBase
{
    public static final int maskWidthHeight = 900/2;

    protected Matrix converterMatrix;
    protected Bitmap outputBitmap;
    protected Canvas canvas;
    protected int cameraRotation;
    protected Paint mPaint;

    public OverlayBase(int cameraRotation, Bitmap outputBitmap)
    {
        this.outputBitmap = outputBitmap;
        canvas = new Canvas(outputBitmap);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.GREEN);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setFilterBitmap(true);
    }

    public abstract void draw();

//    public abstract void updateLocations(Face face);

    public abstract void updateLocations(RectF face);

    public abstract void setOverlay(Bitmap overlay, boolean shouldRecycle);

    public abstract void close();

    protected synchronized void setConverter()
    {
        if(cameraRotation == 270)
        {
            converterMatrix = new Matrix();
            converterMatrix.setRotate(360 - cameraRotation, CameraRecorder3.HEIGHT / 2, CameraRecorder3.WIDTH / 2);
            converterMatrix.postTranslate(CameraRecorder3.WIDTH - ((CameraRecorder3.HEIGHT + CameraRecorder3.WIDTH) / 2),
                    CameraRecorder3.HEIGHT - ((CameraRecorder3.HEIGHT + CameraRecorder3.WIDTH) / 2));
        }
        else if(cameraRotation == 90)
        {   converterMatrix = new Matrix();
            converterMatrix.setScale(-1, -1, CameraRecorder3.HEIGHT / 2, CameraRecorder3.WIDTH / 2);
            converterMatrix.preRotate(cameraRotation, CameraRecorder3.HEIGHT / 2, CameraRecorder3.WIDTH/ 2);
            converterMatrix.postTranslate(CameraRecorder3.WIDTH - ((CameraRecorder3.HEIGHT + CameraRecorder3.WIDTH) / 2),
                    CameraRecorder3.HEIGHT - ((CameraRecorder3.HEIGHT + CameraRecorder3.WIDTH) / 2));

        }
    }

    public void setCameraRotation(int cameraRotation)
    {
        this.cameraRotation = cameraRotation;
        setConverter();
    }

}