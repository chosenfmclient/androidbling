package fm.bling.blingy.dialogs.listeners;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/2/16.
 * History:
 * ***********************************
 */
public interface UpdateLikeTotal {
    void onUpdateLikeTotal(int pos);
}
