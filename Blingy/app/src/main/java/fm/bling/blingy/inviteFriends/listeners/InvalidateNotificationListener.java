package fm.bling.blingy.inviteFriends.listeners;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/25/16.
 * History:
 * ***********************************
 */
public interface InvalidateNotificationListener
{
    void invalidateNotification();

}