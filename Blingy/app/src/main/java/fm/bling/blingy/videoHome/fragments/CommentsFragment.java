package fm.bling.blingy.videoHome.fragments;


import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.dialogs.comment.CommentBoxDialog;
import fm.bling.blingy.dialogs.comment.EditCommentDialog;
import fm.bling.blingy.dialogs.listeners.FlagDialogListener;
import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
import fm.bling.blingy.profile.ProfileActivity;
import fm.bling.blingy.record.utils.SimpleDividerItemDecoration;
import fm.bling.blingy.rest.model.CAACommentParams;
import fm.bling.blingy.rest.model.CAACommentsResponse;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.AdaptersDataTypes;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.videoHome.VideoSocialDetailsActivity;
import fm.bling.blingy.videoHome.adapters.CommentsRecyclerAdapter;
import fm.bling.blingy.videoHome.model.CAAVideo;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 9/5/16.
 * History:
 * ***********************************
 */
public class CommentsFragment extends Fragment implements FlagDialogListener, CommentBoxDialog.CommentBoxListener{

    private String mVideoID;
    private String mMediaType;
    private CAAVideo mVideo;
    private ArrayList<CAACommentParams> items;
    private int previousLastItem = 0;
    private int pageNumber = 0;
    private int noData = 0;
    private boolean listInitialized = false;
    private boolean calling = false;
    private boolean endReached = false;
//    private int commentsAdded = 0;
    private int totalcomments;

    private CommentsRecyclerAdapter adapter;


    private ObjectAnimator anim;
    private RecyclerView mCommentsRecycler;
    private ImageView spinner;
    private FrameLayout listViewEmpty;
    private FrameLayout loadingLayout;
    private TextView mTextTotal;
    private TextView mSaySomething;
    private ScaledImageView mCommentImageProfile;
    private RecyclerView.LayoutManager mLayoutManager;

    private View.OnClickListener mItemClickListener;
    private View.OnLongClickListener mItemLongClickListener;

    public static CommentsFragment newInstance(String video_id, String mediaType) {
        CommentsFragment myFragment = new CommentsFragment();
        myFragment.mVideoID = video_id;
        myFragment.mMediaType = mediaType;
        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.comments_fragment, null);
        mCommentsRecycler = (RecyclerView) v.findViewById(R.id.list_view);
        mTextTotal = (TextView)v.findViewById(R.id.text_total);
        mSaySomething = (TextView)v.findViewById(R.id.say_something);
        loadingLayout = (FrameLayout)v.findViewById(R.id.loading_layout);
        listViewEmpty = (FrameLayout)v.findViewById(R.id.comments_empty);
        spinner = (ImageView)v.findViewById(R.id.spinner);
        mCommentImageProfile = (ScaledImageView)v.findViewById(R.id.comment_image_profile);

        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mCommentsRecycler.setLayoutManager(mLayoutManager);
        mCommentsRecycler.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext(), R.drawable.line_divider_white_50, true));

        removeEmptyLayout();
        loadListeners();
        loadAnimation();
        return v;
    }

    private void loadListeners() {
        mCommentImageProfile.setDontSaveToFileCache(false);
        mCommentImageProfile.setUrl(CAAUserDataSingleton.getInstance().getPhotoUrl());
        mCommentImageProfile.setIsRoundedImage(true);
        mCommentImageProfile.setPlaceHolder(R.drawable.extra_large_userpic_placeholder);
        ((BaseActivity)getActivity()).getImageLoaderManager().DisplayImage(mCommentImageProfile);
        mSaySomething.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommentBoxDialog commentBox = CommentBoxDialog.newInstance(mVideoID, new CommentBoxDialog.CommentBoxListener() {
                    @Override
                    public void onFinishCommentVideoDialog(boolean success, String commentId, String text) {
                        if (success) {
                            CAACommentParams myComment = new CAACommentParams();
                            myComment.setText(text);
                            myComment.setCommentId(commentId);
                            User myUser = new User();
                            myUser.setFirstName(CAAUserDataSingleton.getInstance().getFirstName());
                            myUser.setLastName(CAAUserDataSingleton.getInstance().getLastName());
                            myUser.setPhoto(CAAUserDataSingleton.getInstance().getPhotoUrl());
                            myUser.setUserId(CAAUserDataSingleton.getInstance().getUserId());
                            myComment.setUser(myUser);
                            if(items == null)
                                items = new ArrayList<>();

                            items.add(0, myComment);
                            if(adapter != null) {
                                adapter.notifyDataSetChanged();
                            }
                            else{
                                adapter = new CommentsRecyclerAdapter(items, mItemClickListener, mItemLongClickListener, getActivity());
                                mCommentsRecycler.setAdapter(adapter);
                            }
                            totalcomments = ++totalcomments;
                            if(mVideoID != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    StringBuilder totalText = new StringBuilder(totalcomments).append(" ");
                                    if(totalcomments == 1 )
                                        totalText.append(totalcomments).append(" Comment");
                                    else{
                                        totalText.append(totalcomments).append(" Comments");
                                    }
                                    mTextTotal.setText(totalText);
                                }
                            });
                            getActivity().setResult(Activity.RESULT_OK, new Intent().putExtra(Constants.COMMENT_TEXT, totalcomments));
                            removeEmptyLayout();
                        }
                    }
                });
                commentBox.show(getActivity().getFragmentManager(), "show_comment_box");
            }
        });

        mCommentsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItem = ((LinearLayoutManager)mLayoutManager).findFirstVisibleItemPosition();
                if (listInitialized ) {
                    final int lastItem = firstVisibleItem + visibleItemCount;
                    if (lastItem >= totalItemCount - 4 && !calling && !endReached) {
                        if (previousLastItem != lastItem) { //to avoid multiple calls for last item
                            Log.d("Scroll", "Last:" + lastItem);
                            previousLastItem = lastItem;
                            getNextPage();
                        }
                    }
                }
            }
        });

        mItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int)v.getTag();
                if(adapter.getItemViewType(position) != AdaptersDataTypes.TYPE_LOADING) {
                    CAACommentParams cAACommentParams = items.get(position);
                    User newObj = cAACommentParams.getUser();
                    String userID = newObj.getUserId();
                    Intent i = new Intent(getActivity(), ProfileActivity.class);
                    i.putExtra(Constants.USER_ID, userID);
                    startActivity(i);
                }
            }
        };

        mItemLongClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int position = (int)v.getTag();
                if(adapter.getItemViewType(position) != AdaptersDataTypes.TYPE_LOADING ) {
                    final int pos = position;
                    PopupMenu popup = new PopupMenu(getActivity(), v);

                    popup.getMenuInflater().inflate(R.menu.comments_menu, popup.getMenu());
                    if (items.get(position).getUser().getUserId().equalsIgnoreCase(CAAUserDataSingleton.getInstance().getUserId())) {
                        popup.getMenu().removeItem(R.id.action_flag);
                    } else if (mVideo.getUser().getUserId().equalsIgnoreCase(CAAUserDataSingleton.getInstance().getUserId())) {
                        popup.getMenu().removeItem(R.id.action_edit);
                    } else {
                        popup.getMenu().removeItem(R.id.action_edit);
                    }
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                         public boolean onMenuItemClick(MenuItem item) {
                             switch (item.getItemId()) {
                                 case R.id.action_flag:
                                     flagComment(items.get(pos).getCommentId());
                                     return true;
                                 case R.id.action_edit:
                                     EditComment(items.get(pos));
                                     return true;
                                 default:
                                     return true;
                             }
                         }
                    });
                    popup.show();
                }
                return true;
            }
        };
    }

    private void getNextPage()
    {
        pageNumber++;
        calling = true;
        App.getUrlService().getVideoComments(mVideoID, pageNumber, new Callback<CAACommentsResponse>()
        {
            @Override
            public void success(CAACommentsResponse caaCommentsResponse, Response response)
            {
                if (response.getStatus() != HttpURLConnection.HTTP_NO_CONTENT)
                {
                    if (caaCommentsResponse.getData().size() < 1)
                    {
                        noData++;
                        if (noData >= 2)
                        {
                            endReached = true;
                            items.remove(items.size() - 1);
                            adapter.notifyItemRemoved(items.size());
//                            adapter.notifyDataSetChanged();
                        }
                        else
                        {
                            getNextPage();
                        }
                    }
                    else
                    {
                        items.remove(items.size() - 1);
                        adapter.notifyItemRemoved(items.size());
                        noData = 0;
                        int partitionStart = items.size();
                        for (CAACommentParams cAACommentParams : caaCommentsResponse.getData())
                        {
                            items.add(cAACommentParams);
                        }
                        items.add(new CAACommentParams());
                        adapter.notifyItemRangeInserted(partitionStart, items.size() - partitionStart);
//                        adapter.notifyDataSetChanged();
                    }
                }
                else
                {
                    endReached = true;
                    items.remove(items.size() - 1);
                    adapter.notifyItemRemoved(items.size());
//                    adapter.notifyDataSetChanged();
                }
                calling = false;
            }

            @Override
            public void failure(RetrofitError error)
            {
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getVideoComments();

    }

    private void getVideoComments() {
        App.getUrlService().getVideoComments(mVideoID, pageNumber, new Callback<CAACommentsResponse>() {
            @Override
            public void success(CAACommentsResponse caaCommentsResponse, Response response) {
                canelAnimation();
                if(mCommentsRecycler == null || getActivity() == null)
                    return;
                if(caaCommentsResponse != null && caaCommentsResponse.getData().size() > 0) {
                    removeEmptyLayout();
                    items = caaCommentsResponse.getData();
                    items.add(new CAACommentParams());
                    mVideo = caaCommentsResponse.getVideo();
                    mMediaType = caaCommentsResponse.getVideo().getMediaType();
                    adapter = new CommentsRecyclerAdapter(items, mItemClickListener, mItemLongClickListener, getActivity());
                    mCommentsRecycler.setAdapter(adapter);
                    totalcomments = Integer.valueOf(caaCommentsResponse.getTotalComments());
                    listInitialized = true;

                    StringBuilder totalText = new StringBuilder(totalcomments).append(" ");
                    if(totalcomments == 1)
                        totalText.append(totalcomments).append(" Comment");
                    else{
                        totalText.append(totalcomments).append(" Comments");
                    }
                    mTextTotal.setText(totalText);
                }
                else {
                    showEmptyLayout();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                canelAnimation();
            }
        });
    }

    private void flagComment(String commentID) {
        if (NetworkConnectivityListener.isConnectedWithDialog(getActivity())) {
            EditCommentDialog editCommentDialog = new EditCommentDialog(getActivity(), commentID, this);
            editCommentDialog.show();
        }
//        else {
//            ((NavDrawerActivity)getActivity()).showNoConnectionDialog();
//        }
    }

    private void EditComment(CAACommentParams cAACommentParams) {
        if (NetworkConnectivityListener.isConnectedWithDialog(getActivity())) {
            CommentBoxDialog commentBoxDialog = CommentBoxDialog.newInstance(cAACommentParams, this);
            commentBoxDialog.show(getActivity().getFragmentManager(), "show_comment_box");
        }
    }

    @Override
    public void onFinishFlagVideoDialog(boolean success) {
        if (success)
            showSuccessAlertDialog();
    }

    private void showSuccessAlertDialog() {
        String yesText = getActivity().getApplicationContext().getResources().getString(R.string.ok);
        String title = "Blingy";
        String text = "Thank you for the report";
        BaseDialog mDialog = new BaseDialog(getContext(),title,text);
        mDialog.setOKText(yesText);
        mDialog.removeCancelbutton();
        mDialog.show();
    }

    @Override
    public void onFinishCommentVideoDialog(boolean success, String  commentId, String text) {
        if (success) {
            for (int i = 0; i <  items.size(); i++) {
                if (items.get(i).getCommentId() != null && items.get(i).getCommentId().contentEquals(commentId)) {
                    items.get(i).setText(text);
                    break;
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    private void showEmptyLayout() {
        mCommentsRecycler.setVisibility(View.GONE);
        listViewEmpty.setVisibility(View.VISIBLE);
        listViewEmpty.findViewById(R.id.linearLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSaySomething.callOnClick();
            }
        });
    }
    private  void removeEmptyLayout(){
        mCommentsRecycler.setVisibility(View.VISIBLE);
        listViewEmpty.setVisibility(View.GONE);
    }

    public void canelAnimation() {
        if(loadingLayout != null)
            loadingLayout.setVisibility(View.GONE);
        anim.cancel();
    }

    private void loadAnimation() {
        anim = ObjectAnimator.ofFloat(spinner, "rotation", 0f, 359f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setDuration(1300);
        anim.setRepeatCount(Animation.INFINITE);
        anim.start();
    }
}
