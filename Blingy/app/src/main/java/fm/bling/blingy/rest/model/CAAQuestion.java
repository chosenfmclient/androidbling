package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;

public class CAAQuestion
{

    @SerializedName("question_id")
    private String questionId;

    @SerializedName("question")
    private String question;

    @SerializedName("type")
    private String type;

    public String getQuestionId() {
        return questionId;
    }

//    public void setQuestionId(String questionId) {
//        this.questionId = questionId;
//    }

    public String getQuestion() {
        return question;
    }

//    public void setQuestion(String question) {
//        this.question = question;
//    }

    public String getType() {
        return type;
    }

//    public void setType(String type) {
//        this.type = type;
//    }

}
