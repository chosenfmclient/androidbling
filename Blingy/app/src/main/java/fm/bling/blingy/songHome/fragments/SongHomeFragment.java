package fm.bling.blingy.songHome.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;

import fm.bling.blingy.record.CreateVideoActivity;
import fm.bling.blingy.record.rest.model.CAAItunesMusicElement;
import fm.bling.blingy.record.utils.GridSpacingItemDecoration;
import fm.bling.blingy.songHome.adapters.SongHomeRecyclerAdapter;
import fm.bling.blingy.songHome.model.SongHomeFeed;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.AdaptersDataTypes;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderListener;
import fm.bling.blingy.utils.imagesManagement.MultiImageLoader;
import fm.bling.blingy.utils.imagesManagement.views.ProgressBarView;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;
import fm.bling.blingy.utils.imagesManagement.views.WhiteProgressBarView;
import fm.bling.blingy.videoHome.VideoHomeActivity;
import fm.bling.blingy.videoHome.model.CAAVideo;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/2/16.
 * History:
 * ***********************************
 */
public class SongHomeFragment extends ScrollToTopFragment {

    /**
     * Discover Views
     */
    private RecyclerView mDiscoverRecycler;
    private SongHomeRecyclerAdapter mSongHomeRecyclerAdapter;
    private GridLayoutManager mGridLayoutManager;

    /**
     * Featured Views
     */
    private View mHeader;
    private WhiteProgressBarView mContentBarView;
    private ImageLoaderListener mImageLoaderListener;
    private URLImageView mBluredImage;
    private ScaledImageView mFeaturedImage;
    private ImageView mPlayPauseButton;
    private TextView mFeaturedSongName;
    private TextView mFeaturedArtistName;
    private TextView mShootNow;
    private ProgressBarView mFeaturedProgressBar;
    private FrameLayout mFeaturedContainer;
    private TextureView mFeatureTextureView;
    private MediaPlayer mFeaturedMediaPlayer;
    private LinearLayout mSearchBar;
    private LinearLayout mLeaderContainer;
    private ImageView mBarIcon;
    private TextView mBarTitle;
    private TextView mBanner;
    private TextView mNoVideos;
    private String mFeaturedClipURL = "";
    private boolean isPlaying = false;
    private boolean isPaused = false;
    private boolean isPrepared = false;
    private boolean isLoadingMusic = false;
    private boolean isVisible = false;

    private MultiImageLoader mMultiImageLoader;
    private Handler mHandler;
    private Animation slideOut;

    private boolean isFirst = true;
    private String mClipID;
    private String mSongClipUrl = null;
    private String mSongName;
    private String mArtistName;
    private String mClipThumbnail;
    private ArrayList<Integer> mSegments;

    private BroadcastReceiver audioReceiver;

    public static SongHomeFragment newInstance(String songClipUrl, String songName, String artist, String clipThumbnail, ArrayList<Integer> segements, String clip_id) {
        SongHomeFragment fragment = new SongHomeFragment();
        fragment.mClipID = clip_id;
        if (clip_id == null) {
            fragment.mSongClipUrl = songClipUrl;
            fragment.mSongName = songName;
            fragment.mArtistName = artist;
            fragment.mClipThumbnail = clipThumbnail;
            fragment.mSegments = segements;
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listenToAudioChanges();
        mMultiImageLoader = ((BaseActivity) getActivity()).getMultiImageLodaer();
        if (mClipID == null && mSongClipUrl != null && !mSongClipUrl.isEmpty())
            getFeaturedAndDiscoverByClip();
        else
            getFeaturedAndDiscover(mClipID);
    }

    private void removeSearchAndSetTitle() {
        mBarTitle.setTextColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.white));
        mBarTitle.setText(mSongName);
        mBarIcon.setImageResource(R.drawable.ic_nav_back_24dp);
        mBarIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        isVisible = true;
        mMultiImageLoader = ((BaseActivity) getActivity()).getMultiImageLodaer();

        if (!isFirst && mBluredImage != null) {
            mBluredImage.setImageLoader(mMultiImageLoader);
            mFeaturedImage.setImageLoader(mMultiImageLoader);
        }

        if (mHandler == null)
            mHandler = new Handler();
        if (slideOut == null) {
            slideOut = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out);
            slideOut.setDuration(300);
            slideOut.setFillAfter(true);
        }

        if (mSongHomeRecyclerAdapter != null)
            mSongHomeRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        isVisible = false;
        mFeaturedImage.setVisibility(View.VISIBLE);
        mPlayPauseButton.setVisibility(View.VISIBLE);
        releaseMediaPlayer();
    }

    @Override
    public void onStop() {
        super.onStop();
        isFirst = false;
        mMultiImageLoader = null;
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.song_home_layout, null);
        mHeader = inflater.inflate(R.layout.song_home_header, null);
        init(v);
        loadListeners();

        return v;
    }

    private void init(View mainView) {
        //Main
        mContentBarView = (WhiteProgressBarView) mainView.findViewById(R.id.progress_bar_view);
        mContentBarView.updateLayout();
        mDiscoverRecycler = (RecyclerView) mainView.findViewById(R.id.discover_recycler);
        //Header
        mBluredImage = (URLImageView) mHeader.findViewById(R.id.blured_image);
        mFeaturedImage = (ScaledImageView) mHeader.findViewById(R.id.clip_image);
        mPlayPauseButton = (ImageView) mHeader.findViewById(R.id.play_pause_button);
        mFeaturedSongName = (TextView) mHeader.findViewById(R.id.clip_name);
        mFeaturedArtistName = (TextView) mHeader.findViewById(R.id.artist_name);
        mFeaturedContainer = (FrameLayout) mHeader.findViewById(R.id.clip_container);
        mFeatureTextureView = (TextureView) mHeader.findViewById(R.id.clip_texture_view);
        mShootNow = (TextView) mHeader.findViewById(R.id.select_button);
        mFeaturedProgressBar = (ProgressBarView) mHeader.findViewById(R.id.progress_bar);
        mSearchBar = (LinearLayout) mHeader.findViewById(R.id.search_bar_container);
        mLeaderContainer = (LinearLayout) mHeader.findViewById(R.id.leaderboard_container);
        mBarIcon = (ImageView) mHeader.findViewById(R.id.bar_icon);
        mBarTitle = (TextView) mHeader.findViewById(R.id.bar_title);
        mBanner = (TextView) mHeader.findViewById(R.id.daily_pick);
        mNoVideos = (TextView) mHeader.findViewById(R.id.no_videos);


        ((LinearLayout.LayoutParams) mSearchBar.getLayoutParams()).topMargin = App.STATUS_BAR_HEIGHT;

        mBluredImage.setBlurRadius(70);
        mBluredImage.setDontSaveToFileCache(true);
        mBluredImage.setDontUseExisting(true);
        mBluredImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mBluredImage.setPlaceHolder(R.drawable.game_thumb_placeholder);
        mBluredImage.setAnimResID(R.anim.fade_in);
        mBluredImage.setImageLoader(mMultiImageLoader);
        mBluredImage.setAutoShowRecycle(true);
        mBluredImage.enableMajorColorLayer(true);

        mFeaturedImage.getLayoutParams().width = App.landscapItemWidth;
        mFeaturedImage.getLayoutParams().height = App.landscapitemHeight;
        mFeaturedImage.setDontSaveToFileCache(true);
        mFeaturedImage.setDontUseExisting(true);
        mFeaturedImage.setAspectRatio(4f / 3f);
        mFeaturedImage.setFixedSize(true);
        mFeaturedImage.setImageLoader(mMultiImageLoader);
        mFeaturedImage.setAnimResID(R.anim.fade_in);
        mFeaturedImage.setImageLoaderListener(mImageLoaderListener);
        mFeaturedImage.setTag(mPlayPauseButton);
        mFeaturedImage.setAutoShowRecycle(true);

        mFeaturedContainer.getLayoutParams().width = App.landscapItemWidth;
        mFeaturedContainer.getLayoutParams().height = App.landscapitemHeight;

        mFeatureTextureView.getLayoutParams().width = App.landscapItemWidth;
        mFeatureTextureView.getLayoutParams().height = App.landscapitemHeight;
    }

    private void loadListeners() {
        mImageLoaderListener = new ImageLoaderListener() {
            @Override
            public void onImageLoaded(URLImageView urlImageView) {
                if ((urlImageView.getTag()) != null)
                    ((ImageView) urlImageView.getTag()).setVisibility(View.VISIBLE);
            }
        };

        removeSearchAndSetTitle();
    }

    private void getFeaturedAndDiscover(String clip_id) {
        App.getUrlService().getSongHomeByClipId(0, clip_id, new Callback<SongHomeFeed>() {
            @Override
            public void success(SongHomeFeed discoverFeed, Response response) {
                if (isVisible) {
                    mContentBarView.setVisibility(View.GONE);
                    mBluredImage.setVisibility(View.VISIBLE);
                    mDiscoverRecycler.setVisibility(View.VISIBLE);
                    setupFeatured(discoverFeed.getFeaturedClip());
                    setupDiscoverRecycler(discoverFeed.getTrendingVideos());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (isVisible)
                    mContentBarView.setVisibility(View.GONE);
            }
        });
    }

    private void getFeaturedAndDiscoverByClip() {
        App.getUrlService().getDiscoverByClipUrl(0, mSongClipUrl, new Callback<SongHomeFeed>() {
            @Override
            public void success(SongHomeFeed discoverFeed, Response response) {
                if (isVisible) {
                    mContentBarView.setVisibility(View.GONE);
                    mBluredImage.setVisibility(View.VISIBLE);
                    mDiscoverRecycler.setVisibility(View.VISIBLE);
                    CAAItunesMusicElement itunesMusicElement = new CAAItunesMusicElement(mArtistName, mSongName, mSongClipUrl, mClipThumbnail, mSegments);
                    setupFeatured(itunesMusicElement);
                    setupDiscoverRecycler(discoverFeed.getTrendingVideos());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (isVisible)
                    mContentBarView.setVisibility(View.GONE);
            }
        });
    }

    private void setUpLable(String lable, String color) {
        if (lable == null || lable.isEmpty()) {
            mBanner.setVisibility(View.GONE);
            mBanner = null;
        } else {
            Drawable drawable = null;
            try {
                drawable = getResources().getDrawable(R.drawable.daily_pick_background);
                drawable.setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);

            } catch (Throwable e) {
                e.printStackTrace();
            }
            mBanner.setText(lable);
            mBanner.setTypeface(App.ROBOTO_ITALIC);
            mBanner.setBackground(drawable);
        }
    }

    private void setupDiscoverRecycler(ArrayList<CAAVideo> trendingVideos) {
        if (trendingVideos == null) {
            trendingVideos = new ArrayList<>(1);
            trendingVideos.add(null);
        } else
            trendingVideos.add(0, null);


        int columnPadding = (int) (15f * App.SCALE_Y);
        mGridLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 3);
        mGridLayoutManager.setRecycleChildrenOnDetach(true);
        mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return mSongHomeRecyclerAdapter.getItemViewType(position) == AdaptersDataTypes.HEADER ? mGridLayoutManager.getSpanCount() : 1;
            }
        });
//            mRecycler.setHasFixedSize(true);
        mDiscoverRecycler.setLayoutManager(mGridLayoutManager);
        mDiscoverRecycler.addItemDecoration(new GridSpacingItemDecoration(3, columnPadding, false, true));
        mDiscoverRecycler.setNestedScrollingEnabled(false);

        if (trendingVideos.size() > 1) {
            mNoVideos.setVisibility(View.GONE);

        } else {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, App.HEIGHT - App.TOOLBAR_HEIGHT + App.STATUS_BAR_HEIGHT);
            mHeader.findViewById(R.id.main_container).setLayoutParams(params);
            mHeader.findViewById(R.id.white_line).setBackgroundResource(R.color.white_30);
            mNoVideos.setVisibility(View.VISIBLE);
        }

        if (mSongHomeRecyclerAdapter == null)
            mSongHomeRecyclerAdapter = new SongHomeRecyclerAdapter(trendingVideos, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CAAVideo video = ((CAAVideo) v.getTag());
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.CAAVIDEO, video);
                    Intent songHome = new Intent(getActivity(), VideoHomeActivity.class);
                    songHome.putExtra(Constants.BUNDLE, bundle);
                    startActivity(songHome);
                }
            }, mHeader);

        mDiscoverRecycler.setAdapter(mSongHomeRecyclerAdapter);
    }

    private void setupFeatured(final CAAItunesMusicElement featuredClip) {
        if (featuredClip == null)
            return;

        setUpLable(featuredClip.getLable(), featuredClip.getColor());

        mBarTitle.setText(featuredClip.getTrackName());

        mFeaturedClipURL = featuredClip.getPreviewUrl();

        mClipThumbnail = featuredClip.getArtworkUrl100().replace("100x100", "600x600");

        mBluredImage.setUrl(mClipThumbnail);
        mMultiImageLoader.DisplayImage(mBluredImage);

        mFeaturedImage.setUrl(mClipThumbnail);
        mMultiImageLoader.DisplayImage(mFeaturedImage);

        mFeaturedSongName.setText(featuredClip.getTrackName());
        mFeaturedArtistName.setText(featuredClip.getArtistName());

        mShootNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreateVideoActivity.class);
                intent.putExtra(Constants.CLIP_URL, mFeaturedClipURL);
                intent.putExtra(Constants.CLIP_THUMBNAIL, mClipThumbnail);
                intent.putExtra(Constants.SONG_NAME, featuredClip.getTrackName());
                intent.putExtra(Constants.ARTIST_NAME, featuredClip.getArtistName());
                TrackingManager.getInstance().tapShootNow("discover", featuredClip.getTrackId(), featuredClip.getArtistName(), featuredClip.getTrackName(), 0);
                ((BaseActivity) getActivity()).getRecordPermissions(intent);
            }
        });

        mFeaturedContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFeaturedMediaPlayer != null) {
                    if (isPlaying) {
                        isPaused = true;
                        isPlaying = false;
                        mFeaturedMediaPlayer.pause();
                        mPlayPauseButton.setVisibility(View.VISIBLE);
                    } else if (isPrepared) {
                        playMusic();
                        mFeaturedImage.setVisibility(View.GONE);
                    }

                } else {
                    MusicPrepare();
                }
            }
        });

        mPlayPauseButton.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mFeaturedProgressBar.getLayoutParams().width = mPlayPauseButton.getWidth();
                mFeaturedProgressBar.getLayoutParams().height = mPlayPauseButton.getHeight();
                mFeaturedProgressBar.updateLayout();
                mPlayPauseButton.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void attachSurface() {
        if (mFeatureTextureView.isAvailable()) {
            mFeaturedMediaPlayer.setSurface(new Surface(mFeatureTextureView.getSurfaceTexture()));
        } else {
            mFeatureTextureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
                @Override
                public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                    mFeaturedMediaPlayer.setSurface(new Surface(mFeatureTextureView.getSurfaceTexture()));
                }

                @Override
                public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                }

                @Override
                public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                    return false;
                }

                @Override
                public void onSurfaceTextureUpdated(SurfaceTexture surface) {
                }
            });
        }
    }

    private void MusicPrepare() {
        if (isLoadingMusic || !isVisible)
            return;

        TrackingManager.getInstance().videoPlay("song_home", mClipID, mArtistName, mSongName, -1);

        if (mFeaturedMediaPlayer == null)
            mFeaturedMediaPlayer = new MediaPlayer();

        isLoadingMusic = true;
        isPrepared = false;
        isPlaying = false;

        // Set type to streaming
        mFeaturedProgressBar.setVisibility(View.VISIBLE);
        mPlayPauseButton.setVisibility(View.GONE);

        attachSurface();

        mFeaturedMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                releaseMediaPlayer();
                mFeaturedProgressBar.setVisibility(View.GONE);
                mFeaturedImage.setVisibility(View.VISIBLE);
                return false;
            }
        });
        // Attach to when audio file is prepared for playing
        mFeaturedMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                isPrepared = true;
                adjustTextureViewSize();
                if (!isPlaying)
                    playMusic();
            }
        });
        mFeaturedMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                isPlaying = false;
                isPaused = false;
                isLoadingMusic = false;
                mFeaturedImage.setVisibility(View.VISIBLE);
                mPlayPauseButton.setVisibility(View.VISIBLE);
                mPlayPauseButton.setImageResource(R.drawable.video_play);
            }
        });

        mFeaturedMediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    mp.setOnInfoListener(null);
                    if (mFeaturedImage != null) {
                        mFeaturedImage.setVisibility(View.GONE);
                    }
                    mFeaturedProgressBar.setVisibility(View.GONE);
                }
                return true;
            }
        });

        // Set the data source to the remote URL
        // Trigger an async preparation which will file listener when completed
        try {
            mFeaturedMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mFeaturedMediaPlayer.setDataSource(mFeaturedClipURL);
            mFeaturedMediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void adjustTextureViewSize() {
        if (mFeatureTextureView == null || mFeaturedMediaPlayer == null)
            return;

        float viewWidth = App.WIDTH;
        float viewHeight = App.landscapitemHeight;
        //Set it Top Centered.
        int yoff = 0;

        double aspectRatio;
        try {
            aspectRatio = ((double) mFeaturedMediaPlayer.getVideoHeight()) / ((double) mFeaturedMediaPlayer.getVideoWidth());
        } catch (Throwable err) {
            err.printStackTrace();
            return;
        }

        final float newWidth, newHeight;

        if (mFeaturedMediaPlayer.getVideoWidth() == mFeaturedMediaPlayer.getVideoHeight()) {
            newWidth = viewWidth;
            newHeight = (int) (viewWidth * aspectRatio);
        } else if (mFeaturedMediaPlayer.getVideoWidth() < mFeaturedMediaPlayer.getVideoHeight()) {
            // limited by narrow width; restrict height
            newWidth = viewWidth;
            newHeight = (int) (viewWidth * aspectRatio);
        } else {
            // limited by short height; restrict width
            newWidth = (int) (viewHeight / aspectRatio);
            newHeight = viewHeight;
        }

        int xoff = (int) ((viewWidth - newWidth) / 2f);
        Matrix txform = new Matrix();
        mFeatureTextureView.getTransform(txform);
        txform.postTranslate(xoff, yoff);
        txform.setScale(newWidth / viewWidth, newHeight / viewHeight, (int) ((viewWidth) / 2f), (int) ((viewHeight) / 2f)); // center crop

        mFeatureTextureView.setTransform(txform);
        mFeatureTextureView.requestLayout();

    }

    private void releaseMediaPlayer() {
        if (mFeaturedMediaPlayer != null) {
            mFeaturedMediaPlayer.stop();
            mFeaturedMediaPlayer.reset();
            mFeaturedMediaPlayer = null;
        }
    }

    private void playMusic() {
        if (mFeaturedMediaPlayer != null) {
            isPlaying = true;
            isPaused = false;
            mFeaturedMediaPlayer.start();
            isLoadingMusic = false;
            mPlayPauseButton.setVisibility(View.GONE);
            ((BaseActivity) getActivity()).gainAudioFocus();
        }
        if (mHandler != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mBanner != null)
                        mBanner.startAnimation(slideOut);
                }
            }, 5000);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (audioReceiver != null)
            getActivity().unregisterReceiver(audioReceiver);
    }

    private void listenToAudioChanges() {
        IntentFilter filter = new IntentFilter(Constants.AUDIO_FOCUS_CHANGED);
        audioReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int focus = intent.getIntExtra(Constants.FOCUS, -1);
                switch (focus) {
                    case AudioManager.AUDIOFOCUS_LOSS:
                        if (mFeaturedMediaPlayer != null && !isPaused && isPlaying)
                            mFeaturedContainer.callOnClick();
                        break;
                }

            }
        };
        getActivity().registerReceiver(audioReceiver, filter);
    }

    @Override
    public void scrollToTop() {
        try {
            if (mDiscoverRecycler != null)
                mDiscoverRecycler.smoothScrollToPosition(0);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

}