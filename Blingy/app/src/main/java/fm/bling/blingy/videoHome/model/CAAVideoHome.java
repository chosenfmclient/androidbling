package fm.bling.blingy.videoHome.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import fm.bling.blingy.rest.model.CAAApiActivities;


/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAVideoHome
 * Description:
 * Created by Dawidowicz Nadav on 6/9/15.
 * History:
 * ***********************************
 */
public class CAAVideoHome {

    @SerializedName("video")
    private CAAVideo video;

    @SerializedName("activities")
    private CAAApiActivities activities;

    @SerializedName("responses")
    private CAAVideoList responses;

    @SerializedName("playlist")
    private ArrayList<CAAVideo> playlist;

    public CAAVideo getVideo() {
        return video;
    }

    public void setVideo(CAAVideo video) {
        this.video = video;
    }

    public CAAApiActivities getActivities() {
        return activities;
    }

    public void setActivities(CAAApiActivities activities) {
        this.activities = activities;
    }

    public CAAVideoList getResponses() {
        return responses;
    }

    public void setResponses(CAAVideoList responses) {
        this.responses = responses;
    }

    public ArrayList<CAAVideo> getPlaylist() {
        return playlist;
    }

    public void setPlaylist(ArrayList<CAAVideo> playlist) {
        this.playlist = playlist;
    }
}