package fm.bling.blingy.widget.dialogs;

import android.content.Intent;
import android.view.View;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.database.handler.FollowingDataHandler;
import fm.bling.blingy.registration.CAALogin;
import fm.bling.blingy.registration.SignupActivity;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.stateMachine.StateMachineMessageListener;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 7/13/16.
 * History:
 * ***********************************
 */
public class FollowSuggestedDialog extends SuggestedFriendsDialog {
    private ArrayList<User> mUserList;

    public FollowSuggestedDialog(StateMachineMessageListener onReadyListener, BaseActivity context, StateMachineEvent.MESSAGE_TYPE messageType, ArrayList<User> userList) {
        super(onReadyListener, context, messageType, userList.size());
        this.mUserList = userList;
        dialogLayout.findViewById(R.id.top_bar_container).setVisibility(View.VISIBLE);
//        mPointsLayout.setVisibility(View.GONE);
        setUpUserCurrent();
    }

    @Override
    public void setUpUserCurrent() {
        User caaUser = mUserList.get(mCurrentUserNum);
        mButtonUserPic.setUrl(caaUser.getPhoto());
        StringBuilder stringBuilder = new StringBuilder("Follow ");
        stringBuilder.append(caaUser.getFullName());
        mMainText.setText(stringBuilder.toString());
        mButtonText.setText("FOLLOW");

        mButtonUserPic.setVisibility(View.VISIBLE);
        if(mImageLoaderManager != null && mButtonUserPic != null)
            mImageLoaderManager.DisplayImage(mButtonUserPic);
        mButtonFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                followUser();
            }
        });
    }

    private void followUser()
    {
//        TrackingManager.getInstance().tapButtonWidget("follow friend", mCurrentUserNum, listSize);
        if (CAALogin.userIsAnonymous())
        {
            mContext.startActivity(new Intent(mContext, SignupActivity.class));
        }
        else
        {
            Integer totalFollowing = Integer.parseInt(CAAUserDataSingleton.getInstance().getTotalFollowing()) + 1;
            CAAUserDataSingleton.getInstance().setTotalFollowing(String.valueOf(totalFollowing));

            StateMachine.getInstance(mContext.getApplicationContext()).insertEvent(
                    new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL,
                            StateMachineEvent.EVENT_TYPE.FOLLOW_USER.ordinal(),
                            Integer.parseInt(mUserList.get(mCurrentUserNum).getUserId())
                    )
            );
            if (mCurrentUserNum >= 0) {
                String id = mUserList.get(mCurrentUserNum).getUserId();
                FollowingDataHandler.getInstance().add(id);
                App.getUrlService().putsUsersFollow(id, new String(), new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {

                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
                mUserList.remove(mCurrentUserNum);
                mCurrentUserNum--;


                if (mUserList.isEmpty()) {
                    dismiss();
                } else {
                    setListSize(mUserList.size());
                    moveNextUser();
                }
            }
        }
    }

}
