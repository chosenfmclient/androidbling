package fm.bling.blingy.record.overlay;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Build;

import fm.bling.blingy.App;
import fm.bling.blingy.record.CameraRecorder3;


/**
 * Created by Ben Levi on 08/06/2016.
 * Chosen inc. all rights reserved.
 */
public class CenterToCenterOverlay extends OverlayBase
{
    private final static float faceMaskWidthHeight = 354f / 2f;
//    private float addOnScaleFactor = 1f;
    private float addOnTopPadding = 0f;

    private Bitmap overlayBitmap;
    private int overlayMidWidthHeight;
    private final Matrix overlayMatrix = new Matrix();
    ;
    private float faceWidth, faceHeight;
    private float scaleFactor;//, scaleFactorY;
    private boolean isNotInitialize = true, isMatrixNotSet = true;

    private boolean shouldAdjustBounds;
//    private RectF faceRect = new RectF();


    public CenterToCenterOverlay(Bitmap overlayBitmap, int cameraRotation, Bitmap outputBitmap, boolean shouldRecycle)
    {
        super(cameraRotation, outputBitmap);
        setOverlay(overlayBitmap, shouldRecycle);
        setCameraRotation(cameraRotation);
        setInitialAddOns();
    }

    @Override
    public void draw()
    {
        if (isMatrixNotSet) return;
        try
        {
            synchronized (overlayMatrix)
            {
                canvas.drawBitmap(overlayBitmap, overlayMatrix, mPaint);
            }
//            canvas.drawRect(faceRect.left, faceRect.top, faceRect.left + faceHeight, faceRect.top + faceWidth, mPaint);
        }catch (Throwable err) {}
    }

    @Override
    public void updateLocations(RectF faceLocation)
    {
        if (isNotInitialize) return;

        faceHeight = Math.abs(faceLocation.height());
        faceWidth = Math.abs(faceLocation.width());
//        faceRect = new RectF(faceLocation);

        switch (cameraRotation)
        {
            case 90:
                synchronized (overlayMatrix)
                {
                    overlayMatrix.reset();
                    overlayMatrix.setRotate(360 - cameraRotation, overlayMidWidthHeight, overlayMidWidthHeight);
                    overlayMatrix.preScale(scaleFactor = (((faceWidth / faceMaskWidthHeight))), scaleFactor, overlayMidWidthHeight, overlayMidWidthHeight);//Y = (faceHeight / faceMaskWidthHeight)
                    float dif = ((scaleFactor * maskWidthHeight) - (maskWidthHeight )) / 2;
                    int addOnleft = (int) (faceWidth * 0.79f);
                    int addOnTop = (int) (faceHeight * 1.11f);
                    overlayMatrix.postTranslate(faceLocation.left + dif - addOnTop + addOnTopPadding, faceLocation.top + dif - addOnleft);
                }
            break;
            case 270:
                synchronized (overlayMatrix)
                {
                    overlayMatrix.reset();
                    overlayMatrix.setRotate(360 - cameraRotation, overlayMidWidthHeight, overlayMidWidthHeight);
                    overlayMatrix.preScale(scaleFactor = ((faceWidth / faceMaskWidthHeight)), scaleFactor, overlayMidWidthHeight, overlayMidWidthHeight);//Y = (faceHeight / faceMaskWidthHeight)
                    float dif = ((scaleFactor * maskWidthHeight) - (maskWidthHeight)) / 2;
                    int addOnleft = (int) (faceWidth * 0.79f);
                    int addOnTop = (int) (faceHeight * 1.11f);
                    overlayMatrix.postTranslate(faceLocation.right + dif - (scaleFactor * maskWidthHeight) + addOnTop + addOnTopPadding, faceLocation.bottom + dif - (scaleFactor * maskWidthHeight) + addOnleft);
                }
            break;
        }
        if(isMatrixNotSet)
            isMatrixNotSet = false;
    }

    @Override
    public synchronized void setOverlay(Bitmap overlay, boolean shouldRecycle)
    {
        if (overlayBitmap != null)
            try
            {
                overlayBitmap.recycle();
            }
            catch (Throwable err)
            {
            }
        if (overlay.getWidth() == maskWidthHeight && overlay.getHeight() == maskWidthHeight)
            overlayBitmap = overlay;
        else
        {
            try
            {
                overlayBitmap = Bitmap.createScaledBitmap(overlay, maskWidthHeight, maskWidthHeight, true);
            }
            catch (Throwable err)
            {
                try{overlayBitmap.recycle();}catch (Throwable err1){}
                Runtime.getRuntime().gc();
                System.gc();
                overlayBitmap = Bitmap.createScaledBitmap(overlay, maskWidthHeight, maskWidthHeight, true);
            }
            if (shouldRecycle && overlayBitmap != overlay)
                overlay.recycle();
        }
        if (overlay != null)
        {
            overlayMidWidthHeight = maskWidthHeight / 2;
            setConverter();
        }
        isNotInitialize = false;
    }

    @Override
    public void close()
    {
        isMatrixNotSet = isNotInitialize = true;
        if (overlayBitmap != null)
            try
            {
                overlayBitmap.recycle();
            }
            catch (Throwable err)
            {
            }
        overlayBitmap = null;
        mPaint = null;
    }

    private void setInitialAddOns()
    {
//        switch (Build.BRAND)
//        {
//
//            case "g3" :
//            case "htc_m8" :
//            {
//                addOnTopPadding  = -20f;
//                return;
//            }
//            default:
//                break;
//        }
//
//        switch (Build.MODEL)
//        {
//            case "SM-G925V" :
//            case "MotoG3":
//
//            {
//                addOnTopPadding  = -20f;
//                break;
//            }
//            case "Nexus 6P":
//                CameraRecorder3.shouldAddBoundsAdjustment = true;
//                CameraRecorder3.boundAdjustAddon = (int) (15f * App.SCALE_X);
//            case "Nexus 6":
//            case "C6906"://Sony C6906
//            {
//                addOnTopPadding  = -5f;
//                break;
//            }
//            case "Nexus 9":
//            {
//                CameraRecorder3.shouldAddBoundsAdjustment = true;
//                CameraRecorder3.boundAdjustAddon = (int) (-25f * App.SCALE_X);
//                CameraRecorder3.shouldAddToToSideundsAdjustment = true;
//                CameraRecorder3.boundSidesAdjustAddon = (int) (-17f * App.SCALE_X);
//                addOnTopPadding  = 50f;
//                break;
//            }
//            case "XT1064"://Moto 2gn
//            case "0PJA10"://HTC M9
//            {
//                addOnTopPadding  = 10f;
//                break;
//            }
//            default:
//                break;
//        }
    }

}