package fm.bling.blingy.homeScreen.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.database.handler.FollowingDataHandler;
import fm.bling.blingy.database.handler.LikesDataHandler;
import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.dialogs.flag.FlagVideoDialog;
import fm.bling.blingy.dialogs.listeners.FlagDialogListener;
import fm.bling.blingy.dialogs.share.ShareDialogVideoHome;
//import fm.bling.blingy.mashup.MashupHomeActivity;
import fm.bling.blingy.duetHome.view.DuetHomeActivity;
import fm.bling.blingy.mashup.listeners.CreateCameoListener;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.rest.model.CAAStatus;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.songHome.SongHomeActivity;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.AdaptersDataTypes;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.imagesManagement.MultiImageLoader;
import fm.bling.blingy.utils.imagesManagement.views.ProgressBarView;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;
import fm.bling.blingy.utils.imagesManagement.views.WhiteProgressBarView;
import fm.bling.blingy.utils.views.GlowRippleBackground;
import fm.bling.blingy.videoHome.VideoHomeActivity;
import fm.bling.blingy.videoHome.model.CAAVideo;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 10/6/16.
 * History:
 * ***********************************
 */
public class HomeTileRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements FlagDialogListener {

    private static final String TAG = "HomeRecycler";
    private Activity mActivity;
    private ArrayList<CAAVideo> mDataSet;
    private MultiImageLoader mImageLoaderManager;
    private View.OnClickListener clipSelectedOnClickListener, commentClickListener,
                                 profileClick, likeClickListener,
                                 closeTutorialListener, rightCornerIconListener;
    private View.OnTouchListener playPauseTouchListener;
    private CreateCameoListener createCameoListener;

    private Handler mHandler;
    private Animation slideOut, fadeIn, fadeOut;
    private ShareDialogVideoHome shareDialog;
    private Drawable inviteDrawable;

    private View.OnClickListener moveToSongHomeListener, moveToDuetHome;

    public HomeTileRecyclerAdapter(Activity activity, ArrayList<CAAVideo> myDataset,
                                   MultiImageLoader imageLoaderManager,
                                   View.OnClickListener clipSelectedOnClickListener,
                                   View.OnTouchListener playPauseTouchListener,
                                   View.OnClickListener likeClickListener,
                                   View.OnClickListener commentClickListener,
                                   View.OnClickListener profileClick,
                                   View.OnClickListener rightCornerIconListener,
                                   View.OnClickListener closeTutorialListener,
                                   CreateCameoListener createCameoListener) {

        mDataSet = myDataset;
        mImageLoaderManager = imageLoaderManager;

        this.mActivity = activity;
        this.clipSelectedOnClickListener = clipSelectedOnClickListener;
        this.commentClickListener = commentClickListener;
        this.playPauseTouchListener = playPauseTouchListener;
        this.likeClickListener = likeClickListener;
        this.profileClick = profileClick;
        this.closeTutorialListener = closeTutorialListener;
        this.rightCornerIconListener = rightCornerIconListener;
        this.createCameoListener = createCameoListener;
        this.mHandler = new Handler();
        this.moveToSongHomeListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.canClick()) {
                    CAAVideo video = (CAAVideo) v.getTag();
                    Intent discover = new Intent(mActivity, SongHomeActivity.class);
                    discover.putExtra(Constants.IS_SONG_HOME, true);
                    discover.putExtra(Constants.CLIP_ID, video.getClipId());
                    mActivity.startActivity(discover);
                }
            }
        };

        this.moveToDuetHome = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.canClick()) {
                    CAAVideo video = (CAAVideo) v.getTag();
                    Intent duetHome = new Intent(mActivity, DuetHomeActivity.class);
                    duetHome.putExtra(Constants.VIDEO_ID, video.getVideoId());
                    mActivity.startActivity(duetHome);
                }
            }
        };

        loadAnimations();
    }

    private void loadAnimations() {
        slideOut = AnimationUtils.loadAnimation(mActivity,R.anim.slide_out);
        slideOut.setFillAfter(true);
        fadeIn = AnimationUtils.loadAnimation(mActivity,R.anim.fade_in);
        fadeIn.setFillAfter(true);
        fadeOut = AnimationUtils.loadAnimation(mActivity,R.anim.fade_out);
        fadeOut.setFillAfter(true);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder dataObjectHolder;
        View view;
        switch (viewType){
            case AdaptersDataTypes.TYPE_LOADING:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_recycler_view, parent, false);
                dataObjectHolder = new ProgressBarHolder(view);
                return dataObjectHolder;
            case AdaptersDataTypes.FIRST_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_recycler_first_item, parent, false);
                dataObjectHolder = new HomeTileRecyclerAdapter.DataObjectHolder(view);
                return dataObjectHolder;
            default:
            case AdaptersDataTypes.TYPE_VIDEO:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_recycler_item, parent, false);
                dataObjectHolder = new HomeTileRecyclerAdapter.DataObjectHolder(view);
                return dataObjectHolder;

        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0)
            return  AdaptersDataTypes.FIRST_ITEM;
        else
            return mDataSet.get(position).getVideoId() == null  || mDataSet.get(position).getVideoId().isEmpty() ? AdaptersDataTypes.TYPE_LOADING : AdaptersDataTypes.TYPE_VIDEO;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        if (getItemViewType(position) == AdaptersDataTypes.TYPE_VIDEO || getItemViewType(position) == AdaptersDataTypes.FIRST_ITEM)
        {
            CAAVideo caaVideo = mDataSet.get(position);
            DataObjectHolder dataObjectHolder = ((DataObjectHolder) holder);
            dataObjectHolder.mClipContainer.setOnTouchListener(playPauseTouchListener);

            boolean isMe = CAAUserDataSingleton.getInstance().isMe(caaVideo.getUser().getUserId());

            if (isMe && !caaVideo.getType().equals(Constants.CAMEO_TYPE))
            {
                if (inviteDrawable == null)
                    inviteDrawable = ContextCompat.getDrawable(mActivity.getApplicationContext(), R.drawable.ic_cameo_invite_24dp);

                dataObjectHolder.mDuetNow.setVisibility(View.GONE);
                dataObjectHolder.mShootNow.setBackgroundResource(R.drawable.button_shape_pink);
                dataObjectHolder.mShootNow.setCompoundDrawablesWithIntrinsicBounds(inviteDrawable, null, null, null);
                dataObjectHolder.mShootNow.setCompoundDrawablePadding((int) (25f * App.SCALE_X));
                dataObjectHolder.mShootNow.setText(R.string.invite_to_duet);
            }
            else
            {
                dataObjectHolder.mShootNow.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                dataObjectHolder.mShootNow.setBackgroundResource(R.drawable.button_dialog_blue);

                if (caaVideo.isCameoPermission() && !isMe)
                {
                    dataObjectHolder.mShootNow.setText(R.string.shoot);
                    dataObjectHolder.mDuetNow.setVisibility(View.VISIBLE);
                }
                else
                {
                    dataObjectHolder.mShootNow.setText(R.string.shoot_now);
                    dataObjectHolder.mDuetNow.setVisibility(View.GONE);
                }
            }


            dataObjectHolder.mShootNow.setPadding(App.MAIN_MARGIN, App.BLOCK_MARGIN, App.MAIN_MARGIN, App.BLOCK_MARGIN);
            dataObjectHolder.mShootNow.setTag(caaVideo);
            dataObjectHolder.mDuetNow.setTag(caaVideo);

            if (caaVideo.getType().equalsIgnoreCase(Constants.CAMEO_TYPE))
            {
                dataObjectHolder.mRightCornerIcon.setOnClickListener(rightCornerIconListener);
                dataObjectHolder.mRightCornerIcon.setImageResource(R.drawable.ic_cameo_24dp);
                dataObjectHolder.mRightCornerIcon.setVisibility(View.VISIBLE);
                dataObjectHolder.mRightCornerIcon.setClickable(true);
                dataObjectHolder.mSeeSimilar.setText(R.string.view_other_duets);
                dataObjectHolder.mSeeSimilar.setOnClickListener(moveToDuetHome);
            }
            else
            {
                dataObjectHolder.mRightCornerIcon.setClickable(false);
                dataObjectHolder.mRightCornerIcon.setVisibility(View.GONE);
                dataObjectHolder.mSeeSimilar.setText(R.string.more_like_this);
                dataObjectHolder.mSeeSimilar.setOnClickListener(moveToSongHomeListener);
            }


            switch (caaVideo.getStatus())
            {
                case "featured":
                    dataObjectHolder.mBanner.setText("FEATURED");
                    dataObjectHolder.mBanner.setBackgroundResource(R.drawable.featured_background);
                    dataObjectHolder.mBanner.setVisibility(View.VISIBLE);
                    break;
                case "daily_pick":
                    dataObjectHolder.mBanner.setText("DAILY PICK!");
                    dataObjectHolder.mBanner.setBackgroundResource(R.drawable.daily_pick_background);
                    dataObjectHolder.mBanner.setVisibility(View.VISIBLE);
                    break;
                default:
                    dataObjectHolder.mBanner.setVisibility(View.GONE);
                    break;
            }

            dataObjectHolder.mMoreButton.setTag(caaVideo);
            dataObjectHolder.mUserName.setTag(caaVideo);
            dataObjectHolder.mUserPic.setTag(caaVideo);

            dataObjectHolder.mSeeSimilar.setTag(caaVideo);
            dataObjectHolder.mArtistName.setTag(caaVideo);
            dataObjectHolder.mClipName.setTag(caaVideo);

            dataObjectHolder.mClipContainer.setTag(dataObjectHolder.mClipImage);
            dataObjectHolder.mClipImage.setTag(dataObjectHolder.mProgressBar);
            dataObjectHolder.mProgressBar.setTag(dataObjectHolder.mClipTextureView);
            dataObjectHolder.mClipTextureView.setTag(caaVideo);

            dataObjectHolder.mLikeButton.setTag(dataObjectHolder.mLikesCount);
            dataObjectHolder.mLikesCount.setTag(dataObjectHolder.mClipTextureView);

            dataObjectHolder.mCommentButton.setTag(caaVideo);
            dataObjectHolder.mCommentsCount.setTag(caaVideo);
            dataObjectHolder.mShareButton.setTag(caaVideo);
            if (dataObjectHolder.mGlowRippleBackground != null)
                dataObjectHolder.mGlowRippleBackground.setTag(caaVideo);

            if (dataObjectHolder.mCloseTutorialIcon != null)
                dataObjectHolder.mCloseTutorialIcon.setOnClickListener(closeTutorialListener);


            if (LikesDataHandler.getInstance().contains(caaVideo.getVideoId()))
                dataObjectHolder.mLikeButton.setImageResource(R.drawable.ic_liked_24dp);
            else
                dataObjectHolder.mLikeButton.setImageResource(R.drawable.ic_like_24dp);

            dataObjectHolder.mUserPic.setUrl(caaVideo.getUser().getPhoto());
            dataObjectHolder.mUserPic.setImageLoader(mImageLoaderManager);

            dataObjectHolder.mClipImage.setUrl(caaVideo.getFirstFrame());
            dataObjectHolder.mClipImage.setImageLoader(mImageLoaderManager);

            dataObjectHolder.mUserName.setText(caaVideo.getUser().getFullName());
            dataObjectHolder.mLikesCount.setText(String.valueOf(caaVideo.getTotalLikes()));
            dataObjectHolder.mCommentsCount.setText(String.valueOf(caaVideo.getTotalComments()));

            dataObjectHolder.mArtistName.setText(caaVideo.getArtistName());
            dataObjectHolder.mClipName.setText(caaVideo.getSongName());

            dataObjectHolder.mShootNow.setTag(caaVideo);

            if (caaVideo.getStatus().equals("duet_featured"))
            {
                dataObjectHolder.mBanner.setText("FEATURED DUET");
                dataObjectHolder.mBanner.setBackgroundResource(R.drawable.pink_featured_background);
                dataObjectHolder.mBanner.setVisibility(View.VISIBLE);

                dataObjectHolder.mBluredImage.setUrl(null);
                dataObjectHolder.mBluredImage.setPlaceHolder(R.drawable.duet_featured_bg);
                dataObjectHolder.mBluredImage.setBlurRadius(0);
                dataObjectHolder.mBluredImage.enableMajorColorLayer(false);
            }
            else
            {
                dataObjectHolder.mBluredImage.setBlurRadius(70);
                dataObjectHolder.mBluredImage.setPlaceHolder(R.drawable.game_thumb_placeholder);
                dataObjectHolder.mBluredImage.setUrl(caaVideo.getFirstFrame());
                dataObjectHolder.mBluredImage.enableMajorColorLayer(true);
            }
            dataObjectHolder.mBluredImage.setImageLoader(mImageLoaderManager);
        }
    }


    @Override
    public int getItemCount() {
        return mDataSet == null ? 0 : mDataSet.size();
    }

    private void followUser(String userID){
        FollowingDataHandler.getInstance().add(userID);
        App.getUrlService().putsUsersFollow(userID, "", new Callback<String>() {
            @Override
            public void success(String s, Response response) {}
            @Override
            public void failure(RetrofitError error) {}
        });
        StateMachine.getInstance(mActivity.getApplicationContext()).insertEvent(
                new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL,
                        StateMachineEvent.EVENT_TYPE.FOLLOW_USER.ordinal(),
                        Integer.parseInt(userID)
                )
        );
    }

    private void unFollowUser(String userID){
        FollowingDataHandler.getInstance().remove(userID);
        App.getUrlService().deleteUsersFollow(userID, new Callback<String>() {
            @Override
            public void success(String s, Response response) {}
            @Override
            public void failure(RetrofitError error) {}
        });
    }

    @Override
    public void onFinishFlagVideoDialog(boolean success) {
        BaseDialog dialog = new BaseDialog(mActivity, "Blingy", "Thank you for the report");
        dialog.removeCancelbutton();
        dialog.show();
    }

    private void showFlagVideoDialog(String videoID) {
        FlagVideoDialog flagVideoDialog = new FlagVideoDialog(mActivity, videoID, this);
        flagVideoDialog.show();
    }

    private void deleteVideo(final String videoID, String mMediaType) {
        BaseDialog dialog = new BaseDialog(mActivity, "Are you sure?", "Are you sure you want to delete this " + mMediaType + "?");
        dialog.setOKText("DELETE");
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (((BaseDialog) dialogInterface).getAnswer()) {
                    App.getUrlService().deleteVideo(videoID, new Callback<String>() {
                        @Override
                        public void success(String s, Response response) {
                        }

                        @Override
                        public void failure(RetrofitError error) {

                        }
                    });
                }
            }
        });
    }

    private void uploadVideoStatus(final CAAVideo video, String videoID, final String status) {
        App.getUrlService().putUploadVideoStatus(videoID, new CAAStatus(status), new Callback<CAAVideo>() {
            @Override
            public void success(CAAVideo caaVideo, Response response) {
                video.setStatus(caaVideo.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    public void close() {
        mDataSet = null;
        mImageLoaderManager = null;
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView mArtistName;
        TextView mClipName;
        TextView mShootNow;
        TextView mDuetNow;
        TextView mLikesCount;
        TextView mCommentsCount;
        TextView mUserName;
        ImageView mLikeButton;
        ImageView mCommentButton;
        ImageView mMoreButton;
        ImageView mShareButton;
        ImageView mRightCornerIcon;
        ImageView mCloseTutorialIcon;
        TextureView mClipTextureView;
        ScaledImageView mClipImage;
        ProgressBarView mProgressBar;
        URLImageView mBluredImage;
        ScaledImageView mUserPic;
        FrameLayout mClipContainer;
        LinearLayout mUserBar;
        LinearLayout mTileCard;
        TextView mBanner;
        View mTopLine;
        TextView mSeeSimilar;
        GlowRippleBackground mGlowRippleBackground;


        public DataObjectHolder(View itemView) {
            super(itemView);
            mArtistName = (TextView) itemView.findViewById(R.id.artist_name);
            mClipName = (TextView) itemView.findViewById(R.id.clip_name);
            mShootNow = (TextView) itemView.findViewById(R.id.select_button);
            mDuetNow = (TextView) itemView.findViewById(R.id.duet_button);
            mClipTextureView = (TextureView) itemView.findViewById(R.id.clip_texture_view);
            mProgressBar = (ProgressBarView) itemView.findViewById(R.id.progress_bar);mProgressBar.updateLayout();
            mClipImage = (ScaledImageView) itemView.findViewById(R.id.clip_image);
            mBluredImage = (URLImageView) itemView.findViewById(R.id.blured_image);
            mLikeButton = (ImageView) itemView.findViewById(R.id.like_button);
            mCommentButton = (ImageView) itemView.findViewById(R.id.comment_button);
            mCommentsCount = (TextView) itemView.findViewById(R.id.comments_count);
            mLikesCount = (TextView) itemView.findViewById(R.id.likes_count);
            mUserPic = (ScaledImageView) itemView.findViewById(R.id.user_pic);
            mUserName = (TextView) itemView.findViewById(R.id.user_name);
            mMoreButton = (ImageView) itemView.findViewById(R.id.more_button);
            mShareButton = (ImageView) itemView.findViewById(R.id.share_button);
            mRightCornerIcon = (ImageView) itemView.findViewById(R.id.right_corner_icon);
            mCloseTutorialIcon = (ImageView) itemView.findViewById(R.id.close_tutorial);
            mClipContainer = (FrameLayout) itemView.findViewById(R.id.clip_container);
            mUserBar = (LinearLayout) itemView.findViewById(R.id.user_line_container);
            mTileCard = (LinearLayout) itemView.findViewById(R.id.home_tile_card);
            mBanner = (TextView) itemView.findViewById(R.id.daily_pick);
            mSeeSimilar = (TextView) itemView.findViewById(R.id.see_similar);
            mTopLine = itemView.findViewById(R.id.top_line);
            mGlowRippleBackground = (GlowRippleBackground)itemView.findViewById(R.id.glow_ripple);


            mClipContainer.setOnTouchListener(playPauseTouchListener);
            mShootNow.setOnClickListener(clipSelectedOnClickListener);
            mDuetNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CAAVideo video = ((CAAVideo)v.getTag());
                    createCameoListener.onCreateCameo(video);
                }
            });

            mClipImage.getLayoutParams().height = App.landscapitemHeight;
            mClipImage.setMaxWidthInternal(App.WIDTH);
            mClipImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            mClipImage.setAnimResID(R.anim.fade_in);
            mClipImage.setAutoShowRecycle(true);

            mBluredImage.setBlurRadius(70);
            mBluredImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            mBluredImage.setPlaceHolder(R.drawable.game_thumb_placeholder);
            mBluredImage.enableMajorColorLayer(true);
            mBluredImage.setAutoShowRecycle(true);
            mBluredImage.setImageLoader(mImageLoaderManager);

            mClipTextureView.getLayoutParams().width = App.landscapItemWidth;
            mClipTextureView.getLayoutParams().height = App.landscapitemHeight;

            mUserPic.setAnimResID(R.anim.fade_in);
            mUserPic.setIsRoundedImage(true);
            mUserPic.setKeepAspectRatioAccordingToWidth(true);
            mUserPic.setAutoShowRecycle(true);
            mUserPic.setImageLoader(mImageLoaderManager);

            mUserPic.setOnClickListener(profileClick);
            mUserName.setOnClickListener(profileClick);

            mMoreButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    CAAVideo video = ((CAAVideo) view.getTag());
                    String videoID = ((CAAVideo) view.getTag()).getVideoId();
                    String userID = ((CAAVideo) view.getTag()).getUser().getUserId();
                    boolean followedByMe = FollowingDataHandler.getInstance().contains(userID);
//                        App.getUrlService().getDuetPermission(userID, new Callback<DuetPermission>() {
//                            @Override
//                            public void success(DuetPermission duetPermission, Response response) {
//                                Log.d(TAG,"success permission set");
//                                Constants.sFollowers.put(user_id, duetPermission.hasPermission());
                            createAndShowMenu(view, video, userID, videoID, followedByMe, video.isCameoPermission());
//                            }
//
//                            @Override
//                            public void failure(RetrofitError error) {
//                                Log.d(TAG,"failed permission set");
//                            }
//                        });
                }
            });

            mLikeButton.setOnClickListener(likeClickListener);
            mLikesCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLikeButton.callOnClick();
                }
            });

            mCommentButton.setOnClickListener(commentClickListener);
            mCommentsCount.setOnClickListener(commentClickListener);

            mShareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.canClick()) {
                        CAAVideo video = (CAAVideo) v.getTag();
                        shareDialog = new ShareDialogVideoHome(mActivity, (BaseActivity) mActivity, video, false);
                        shareDialog.show();
                    }
                }
            });


            mArtistName.setOnClickListener(moveToSongHomeListener);
            mClipName.setOnClickListener(moveToSongHomeListener);
        }

        private void createAndShowMenu(View view,final CAAVideo video,final String userID, final String videoID,final boolean followedByMe,boolean hasPermission)
        {
            if (userID != null)
            {
                PopupMenu popup = new PopupMenu(mActivity, view);
                popup.getMenuInflater().inflate(R.menu.video_home_popup_menu, popup.getMenu());
                if(video.getType().equals(Constants.CAMEO_TYPE))
                {
                    popup.getMenu().findItem(R.id.view_original_video).setVisible(true);
                }
                else
                {
                    if(Utils.userIsMe(video.getUser().getUserId()))
                    {
                        MenuItem createCameo = popup.getMenu().findItem(R.id.create_cameo);
                        String userName = "yourself";
                        createCameo.setTitle(createCameo.getTitle() + " with " + userName);
                        createCameo.setVisible(true);
                    }
                    else
                    {
                        if (hasPermission)
                        {
                            MenuItem createCameo = popup.getMenu().findItem(R.id.create_cameo);
                            String userName = video.getUser().getFullName();
                            createCameo.setTitle(createCameo.getTitle() + " with " + userName);
                            createCameo.setVisible(true);
                        }
                    }
                }

                if (Utils.userIsMe(userID)) {
                    popup.getMenu().findItem(R.id.action_flag_video).setVisible(false);
                    if(!video.getStatus().equalsIgnoreCase("secret")) {
                        if (video.getStatus().equalsIgnoreCase("private"))
                            popup.getMenu().findItem(R.id.action_private).setTitle("Make public").setVisible(true);
                        else
                            popup.getMenu().findItem(R.id.action_private).setTitle("Make private").setVisible(true);
                    }
                    popup.getMenu().findItem(R.id.action_delete).setVisible(true);
                    popup.getMenu().findItem(R.id.action_follow).setVisible(false);
                }
                else{
                    if(followedByMe) {
                        StringBuilder title = new StringBuilder("Unfollow ").append(video.getUser().getFullName());
                        popup.getMenu().findItem(R.id.action_follow).setTitle(title.toString()).setVisible(true);
                    }
                    else {
                        StringBuilder title = new StringBuilder("Follow ").append(video.getUser().getFullName());
                        popup.getMenu().findItem(R.id.action_follow).setTitle(title.toString()).setVisible(true);
                    }

                }
                popup.setOnMenuItemClickListener(
                        new PopupMenu.OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.view_original_video:
                                        String parentClipid = video.getParent() == null ? video.getClipId() : video.getParent().getClipId();
                                        TrackingManager.getInstance().tapViewOriginal(videoID,parentClipid,video.getArtistName(),video.getSongName());
                                        viewOriginalVideo(video.getParent());
                                        return true;
                                    case R.id.create_cameo:
                                        createCameoListener.onCreateCameo(video);
                                        String parentClipidsec = video.getParent() == null ? video.getClipId() : video.getParent().getClipId();
                                        TrackingManager.getInstance().tapMakeCameo(videoID,parentClipidsec,video.getArtistName(),video.getSongName(), "other");
                                        return true;
                                    case R.id.action_flag_video:
                                        showFlagVideoDialog(videoID);
                                        return true;
                                    case R.id.action_delete:
                                        deleteVideo(videoID, video.getMediaType());
                                        return true;
                                    case R.id.action_private:
                                        if (video.getStatus().equalsIgnoreCase("public"))
                                            uploadVideoStatus(video, videoID, "private");
                                        else
                                            uploadVideoStatus(video, videoID, "public");
                                        return true;
                                    case R.id.action_follow:
                                        int pos = mDataSet.indexOf(video);
                                        if(followedByMe){
                                            video.getUser().setFollowedByMe("0");
                                            unFollowUser(userID);
                                            TrackingManager.getInstance().tapUnfollow(updatedFollowing(-1),video.getUser().getUserId(), "home_screen", pos + "");
                                        }
                                        else{
                                            video.getUser().setFollowedByMe("1");
                                            TrackingManager.getInstance().tapFollow(userID, "home_screen", pos , updatedFollowing(1));
                                            followUser(userID);
                                        }
                                        return true;
                                    default:
                                        return true;
                                }
                            }
                        }
                );
                popup.show();
            }
        }

        public void playVideo(){
            mClipContainer.setOnTouchListener(playPauseTouchListener);
            float x = 1.0f; float y = 1.0f; int metaState = 0;
            MotionEvent motionEvent = MotionEvent.obtain(Constants.BLINGY_FAKE_DOWN_TIME, 0, MotionEvent.ACTION_DOWN, x, y, metaState);
            mClipContainer.dispatchTouchEvent(motionEvent);
        }

        public FrameLayout getContainer(){
            return mClipContainer;
        }

        public void slideOutBanner(int time){
            if(mHandler != null){
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(mBanner.getVisibility() != View.GONE){
                            slideOut.reset();
                            slideOut.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    mBanner.setVisibility(View.GONE);

                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });
                            mBanner.startAnimation(slideOut);
                        }
                    }
                }, time);
            }
        }

        public int getClipTop(){
            int[] pos = new int[2];
            mClipContainer.getLocationOnScreen(pos);
            return pos[1];
        }

        public int getClipBottom(){
            int[] pos = new int[2];
            mClipContainer.getLocationOnScreen(pos);
            return pos[1] + App.landscapitemHeight;
        }

        public String getUrl(){
            return ((CAAVideo)mClipTextureView.getTag()).getUrl();
        }

        public void updateLikeTotal(){
            CAAVideo video = (CAAVideo) mClipTextureView.getTag();
            mLikeButton.setImageResource(R.drawable.ic_liked_24dp);
            video.setTotalLikes(video.getTotalLikes() );
            if (mLikesCount != null) {
                mLikesCount.setText(String.valueOf(video.getTotalLikes()));
            }
        }

        public void fadeOutRightIcon() {
            mGlowRippleBackground.startAnimation(fadeOut);
            mRightCornerIcon.startAnimation(fadeOut);
            mRightCornerIcon.setClickable(false);
        }

        public void fadeInRightIcon() {
            mRightCornerIcon.setClickable(true);
            mGlowRippleBackground.startAnimation(fadeIn);
            mRightCornerIcon.startAnimation(fadeIn);
            mGlowRippleBackground.startRippleAnimation();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(mGlowRippleBackground != null)
                        mGlowRippleBackground.stopRippleAnimation();
                }
            },3000);
        }

        public void fadeOutCloseButton() {
            mCloseTutorialIcon.setClickable(false);
            mCloseTutorialIcon.startAnimation(fadeOut);
            mCloseTutorialIcon.setVisibility(View.GONE);
        }

        public void fadeInCloseButton() {
            mCloseTutorialIcon.setClickable(true);
            mCloseTutorialIcon.setVisibility(View.VISIBLE);
            mCloseTutorialIcon.startAnimation(fadeIn);
        }
    }

//    private void moveToMashupHome(String mashupID) {
//        Intent mashupHome = new Intent(mActivity, MashupHomeActivity.class);
//        mashupHome.putExtra(Constants.MASHUP_ID, mashupID);
//        mActivity.startActivity(mashupHome);
//    }

    public int updatedFollowing(int add){
        int following = SharedPreferencesManager.getInstance().getInt(EventConstants.FOLLOWING_COUNT, 1) + add;
        SharedPreferencesManager.getEditor().putInt(EventConstants.FOLLOWING_COUNT, following).commit();
        return  following;
    }

    public class ProgressBarHolder extends RecyclerView.ViewHolder {
        private WhiteProgressBarView mProgressBar;
        public ProgressBarHolder(View itemView) {
            super(itemView);
            mProgressBar = (WhiteProgressBarView) itemView.findViewById(R.id.progress_bar);
            mProgressBar.updateLayout();
        }
    }

    public void setImageLoaderManager(MultiImageLoader multiImageLoader){
        this.mImageLoaderManager = multiImageLoader;
    }

    private void viewOriginalVideo(CAAVideo parent) {
        switch (parent.getStatus()){
            case Constants.PUBLIC:
                Intent originalVideo = new Intent(mActivity, VideoHomeActivity.class);
                originalVideo.putExtra(Constants.VIDEO_ID, parent.getVideoId());
                mActivity.startActivity(originalVideo);
                break;
            case Constants.PRIVATE:
                BaseDialog privateDialog = new BaseDialog(mActivity, mActivity.getString(R.string.video_is_private), mActivity.getString(R.string.original_video_is_private));
                privateDialog.show();
                privateDialog.removeCancelbutton();
                break;
            case Constants.DELETED:
                BaseDialog deletedDialog = new BaseDialog(mActivity, mActivity.getString(R.string.video_deleted_title), mActivity.getString(R.string.original_video_deleted));
                deletedDialog.removeCancelbutton();
                deletedDialog.show();
                break;
        }

    }
}