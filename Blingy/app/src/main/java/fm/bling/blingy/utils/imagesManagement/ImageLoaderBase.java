package fm.bling.blingy.utils.imagesManagement;

import android.graphics.Bitmap;

import fm.bling.blingy.utils.imagesManagement.views.URLImageView;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 17/07/2016.
 * History:
 * ***********************************
 */
public interface ImageLoaderBase
{

    void DisplayImage(URLImageView urlImageView);

    void setImageView(URLImageView imageView, Bitmap bitmap);

    void recycle(URLImageView urlImageView);

    void recycleAndRemove(URLImageView urlImageView);

    void DisplayImageNowLight(URLImageView urlImageView);

    void clearCache();

    void clearFiles();

    void clearImageViews();

    boolean runOnUiThread(Runnable runnable);

    void close();


}
