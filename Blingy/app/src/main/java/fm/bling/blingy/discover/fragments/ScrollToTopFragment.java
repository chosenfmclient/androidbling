package fm.bling.blingy.discover.fragments;

import android.support.v4.app.Fragment;

import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.tracking.EventConstants;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 11/01/17.
 * History:
 * ***********************************
 */
public abstract class ScrollToTopFragment extends Fragment
{

    public abstract void scrollToTop();

    public int updatedFollowing(int add){
        int following = SharedPreferencesManager.getInstance().getInt(EventConstants.FOLLOWING_COUNT, 1) + add;
        SharedPreferencesManager.getEditor().putInt(EventConstants.FOLLOWING_COUNT, following).commit();
        return  following;
    }

}
