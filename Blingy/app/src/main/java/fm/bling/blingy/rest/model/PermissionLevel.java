package fm.bling.blingy.rest.model;


import com.google.gson.annotations.SerializedName;

public class PermissionLevel {

    @SerializedName("video_permission_level")
    private int video_permission_level;

    public PermissionLevel(int level){
        this.video_permission_level = level;
    }
}
