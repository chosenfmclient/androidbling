package fm.bling.blingy.rest.model;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 14/02/17.
 * History:
 * ***********************************
 */
public class Following {

    private String userId;

    private int isFollowing;

    public Following(String userId, int isFollowing) {
        this.userId = userId;
        this.isFollowing = isFollowing;
    }

    public String getUserId() {
        return userId;
    }

    public int getIsFollowing() {
        return isFollowing;
    }
}
