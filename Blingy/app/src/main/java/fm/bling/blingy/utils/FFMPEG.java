package fm.bling.blingy.utils;

import android.content.Context;
import android.hardware.camera2.CameraMetadata;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.annotation.Nullable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fm.bling.blingy.App;
import fm.bling.blingy.R;

/**
 * Created by Ben on 5/5/16.
 */
public class FFMPEG
{
    private Context mContext;
    private volatile static FFMPEG ourInstance;
    private String execPath;
    private final int defaultBitrate = 15469;
    private InputStream errorStream;
    private static boolean initializing;
    public static int lastOpenedTrack = -1;

    private int THREAD_PRIORITY = -4;

    private final int DEFAULT_VIDEO_WIDTH         = 480, DEFAULT_VIDEO_HEIGHT             = 853
                    , DEFAULT_WORKING_VIDEO_WIDTH = 360, DEFAULT_WORKING_VIDEO_WIDTH_LITE = 240;

    private final float MINIMUM_VIDEO_HEIGHT = 480, MINIMUM_VIDEO_HEIGHT_LITE = 320;

    public int loadedFps = 25;
    public float stream0StartTime = 0f;
    public boolean isFirstStreamIsVideo = true;
    public int clipDuration = 15;

    public static FFMPEG getInstance(Context context)
    {
        while (initializing)
        {
            try
            {
                Thread.sleep(100);
            }
            catch (Throwable err)
            {
            }
        }
        if (ourInstance == null)
        {
            initializing = true;
            ourInstance = new FFMPEG(context.getApplicationContext());
            initializing = false;
        }
        return ourInstance;
    }

    private FFMPEG(Context context)
    {
        mContext = context;
        initialize();
    }

    private void initialize()
    {
        File file = new File(mContext.getFilesDir(), "ffmpeg");
        execPath = file.getPath();
        if (!file.exists())
        {
            try
            {
                createFile();
            }
            catch (Throwable err)
            {
                err.printStackTrace();
            }
        }
    }

//    /**
//     * test the device and determine if the device is weak, if it is, set isLiteVideo to true, so we'll work on lower resolution
//     */
//    private void setIsLiteVideo()
//    {
//        isLiteVideo = false;
//    }

    private void createFile() throws Throwable
    {
        InputStream inputStream = mContext.getResources().openRawResource(R.raw.ffmpeg);
        OutputStream outputStream = mContext.openFileOutput("ffmpeg", 0);
        byte[] buffer = new byte[1024*1024];
        int count;
        while(true)
        {
            count = inputStream.read(buffer);
            if(count == -1)
                break;
            outputStream.write(buffer, 0, count);
        }
        inputStream.close();
        outputStream.flush();
        outputStream.close();
        File file = new File(mContext.getFilesDir(), "ffmpeg");
        file.setExecutable(true, false);
    }

    /**
     * Run any compatible Command - See FFmpeg documentation for more info
     * @param command
     * @return
     * @throws Throwable
     */
    public int runCommand(String command) throws Throwable
    {
        if(command.indexOf("ffmpeg") != 0 )
            command = "ffmpeg " + command;
        return Runtime.getRuntime().exec(execPath + command).waitFor();
    }

//    private int runCommandInternal(String command) throws Throwable
//    {
//        int priority = android.os.Process.getThreadPriority( (int) Thread.currentThread().getId() );
//        System.out.println( ">>>>>Priority>>1>>>" + priority +">>>"+ Thread.currentThread().getId()  +">>>"+ (int)Thread.currentThread().getId());
//
//        android.os.Process.setThreadPriority(-20);
//
//        System.out.println( ">>>>>Priority>2>>>>" + android.os.Process.getThreadPriority( (int) Thread.currentThread().getId() ) +">>>"+ Thread.currentThread().getId()  +">>>"+ (int)Thread.currentThread().getId());
//
//        Process process = Runtime.getRuntime().exec(execPath + " " + command);
//        errorStream = process.getErrorStream();
//
//        int ret = process.waitFor();
//
//        android.os.Process.setThreadPriority(priority);
//
//        return ret;
//    }

    private int runCommandInternal(String command) throws Throwable
    {
        Process process = Runtime.getRuntime().exec(execPath + " " + command);
        errorStream = process.getErrorStream();
        return process.waitFor();
    }

//    private int runCommandInternal(String[] command) throws Throwable
//    {
//        Process process = Runtime.getRuntime().exec(command);
//        errorStream = process.getErrorStream();
//        return process.waitFor();
//    }

    private int runCommandInternal(String[] command) throws Throwable
    {
//        int priority = android.os.Process.getThreadPriority( (int) Thread.currentThread().getPriority() );
//        int priority = Thread.currentThread().getPriority();


        int tid = android.os.Process.myTid();
        int priority = android.os.Process.getThreadPriority(tid);
        boolean isPriorityChanged = false;
        System.out.println( ">>>>>Priority>>1>>>" + priority +">>>"+ tid  +">>>"+ (int)Thread.currentThread().getId());

        if(THREAD_PRIORITY < priority) // we want better precedence
        {
            android.os.Process.setThreadPriority(THREAD_PRIORITY);
            isPriorityChanged = true;
        }


//        android.os.Process.setThreadPriority(-7);

        System.out.println( ">>>>>Priority>2>>>>" + android.os.Process.getThreadPriority(tid) +">>>"+ Thread.currentThread().getId()  +">>>"+ (int)Thread.currentThread().getId());

        Process process = Runtime.getRuntime().exec(command);
        errorStream = process.getErrorStream();


        int ret = process.waitFor();

        System.out.println(ret+">>>>>ffmpeg>>>1>" + command);
//        string err = getError();
        System.out.println(">>>>>ffmpeg>>>2>" + getError());

        if(isPriorityChanged)
            android.os.Process.setThreadPriority(priority);

        return ret;
    }

    /**
     * Render the video with an overlay
     *
     * @param videoInPath
     * @param imagePath
     * @param videoOutPath
     * @param x - starts from top left corner (0,0), can be complex as main_w and main_h (e.g: main_w - overlay_w)
     * @param y
     * @param bitRatInKiloBytes pass -1 for default, the lower the bitrate the lower the quality
     * @return the termination exit code
     * @throws Throwable
     */
    public int renderWithImage(String videoInPath, String imagePath, String videoOutPath, String x, String y, int bitRatInKiloBytes) throws Throwable
    {
        int bitrate = bitRatInKiloBytes == -1 ? defaultBitrate : bitRatInKiloBytes;
//        String cmd = "-i " + videoInPath + " -i "+imagePath+" -strict experimental -filter_complex overlay="+x+":"+y+" -threads 2 -r 30 -b "+bitrate+"k -vcodec mpeg4 -ab 48000 -ac 2 -ar 22050 -y " + videoOutPath;
//        String[] cmd = {execPath, "-i", videoInPath, "-i", imagePath, "-filter_complex", "overlay=" + x + ":" + y, "-r", "30", "-b", bitrate + "k", "-vcodec", "libx264", "-ab", "48000", "-ac", "2", "-ar", "22050", "-y", videoOutPath};
        String[] cmd = {execPath, "-i", videoInPath, "-i", imagePath, "-filter_complex", "overlay=" + x + ":" + y, "-r", "30", "-b", bitrate + "k", "-vcodec", "mpeg4", "-ab", "48000", "-ac", "2", "-ar", "22050", "-y", videoOutPath};

        int ret = runCommandInternal(cmd);

        return ret;//runCommandInternal(cmd);
    }

    /**
     * Render the video with an overlay
     *
     * @param videoInPath
     * @param imageBitmap
     * @param videoOutPath
     * @param x - starts from top left corner (0,0), can be complex as main_w and main_h (e.g: main_w - overlay_w)
     * @param y
     * @param bitRatInKiloBytes pass -1 for default, the lower the bitrate the lower the quality
     * @return the termination exit code
     * @throws Throwable
     */
    public int renderWithImageBitmap(String videoInPath, android.graphics.Bitmap imageBitmap, String videoOutPath, String x, String y, int bitRatInKiloBytes) throws Throwable
    {
        String imagePath = mContext.getFilesDir().getAbsolutePath() + File.separator + "temp_image_bitmap.jpg";
        File imageFile = new File(imagePath);
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(imageFile);
            imageBitmap.compress(android.graphics.Bitmap.CompressFormat.PNG,100,fOut);
            fOut.flush();
            fOut.close();
            imageFile.deleteOnExit();
            imageBitmap.recycle();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return renderWithImage( videoInPath, imagePath,  videoOutPath,  x,  y,  bitRatInKiloBytes);
    }

//    public int renderWithImageWithTranspose(String videoInPath, String imagePath, String videoOutPath, String x, String y, int bitRatInKiloBytes) throws Throwable
//    {
//        int bitrate = bitRatInKiloBytes == -1 ? defaultBitrate : bitRatInKiloBytes;
//        String cmd = "-y -i " + videoInPath +" -strict experimental -filter_complex movie="+imagePath+"[watermark];[watermark]transpose=1[transpose];[in][transpose]overlay="+x+":"+y+"[out]; -r 30 -b "+bitrate+"k -vcodec mpeg4 -ab 48000 -ac 2 -ar 22050 " + videoOutPath;
//        String cmd = "-y -i " + videoInPath + " -i "+imagePath+" -strict experimental -filter_complex [0:v][1:v]overlay[out] -map [out] " + videoOutPath;
//        return runCommandInternal(cmd);
//    }

    /**
     * Get the info of the file - metadata
     * this prints out the metadata
     * need to check what is better this method or MediaMetadataRetriever
     *
     * @param videoPath
     * @return the termination exit code
     * @throws Throwable
     */
    public int getVideoInfo(String videoPath) throws Throwable
    {
        String cmd = "-i " + videoPath;
        return runCommandInternal(cmd);
    }

    /**
     * Resize the video to the specified size
     * @param videoInPath
     * @param videoOutPath
     * @param width
     * @param height
     * @return the termination exit code
     * @throws Throwable
     */
    public int renderVideoToSize(String videoInPath, String videoOutPath, int width, int height) throws Throwable
    {
        String cmd = "-i " + videoInPath + " -s " + width + "x" + height + " -acodec mp3 -vcodec libx264 " + videoOutPath;
        return runCommandInternal(cmd);
    }

//    /**
//     *
//     * @param videoInPath
//     * @param videoOutPath
//     * @param width
//     * @param height
//     * @param x - the x location, starting from top left (0,0) default is center
//     * @param y - the y location, starting from top left (0,0) default is center
//     * @return the termination exit code
//     * @throws Throwable
//     */
//    public int cropVideo(String videoInPath, String videoOutPath, int width, int height, int x, int y) throws Throwable
//    {
//        String crop = "[in]crop="+width+":"+height+"[out]";
//        String[] cmd = {execPath, "-i", videoInPath, "-vf", crop, "-r", "30", "-b", defaultBitrate + "k",
//                "-vcodec", "mpeg4", "-ab", "48000", "-ac", "2", "-ar", "22050", "-y", videoOutPath};
//        return runCommandInternal(cmd);
//    }

    /**
     *
     * @param videoInPath
     * @param videoOutPath
     * @param width
     * @param height
     * @param x - the x location, starting from top left (0,0) default is center
     * @param y - the y location, starting from top left (0,0) default is center
     * @return the termination exit code
     * @throws Throwable
     */
//    public int cropVideo(String videoInPath, String videoOutPath, int width, int height, int x, int y) throws Throwable
//    {
//        String crop = "[in]crop="+width+":"+height+"[out]";
//        String[] cmd = {execPath, "-i", videoInPath, "-vf", crop, "-vcodec", "copy", "-acodec", "copy", "-y", videoOutPath};
//        return runCommandInternal(cmd);
//    }

    public int cropVideo(String videoInPath, String videoOutPath, int width, int height, int x, int y) throws Throwable
    {
        String crop = "[in]crop=" + width + ":" + height + "[out]";
        String[] cmd = {execPath, "-i", videoInPath,"-vf", crop, "-threads", "2","-r", "25", "-b", defaultBitrate + "k", "-vcodec", "mpeg4", "-ab", "48000", "-ac", "1", "-ar", "22050", "-y", videoOutPath};
        return runCommandInternal(cmd);
    }

    /**
     *crop
     * @param videoInPath
     * @param videoOutPath
     * @param widthHeight
     * @return the termination exit code
     * @throws Throwable
     */
    public int cropVideoCenterSquare(String videoInPath, String videoOutPath, int widthHeight) throws Throwable
    {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(videoInPath);
        int videoWidth = Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
        int videoHeight= Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
        int x, y;

        x = (videoWidth - widthHeight) / 2;
        y = (videoHeight - widthHeight) / 2;
        return cropVideo(videoInPath, videoOutPath, widthHeight, widthHeight, x, y);
    }

//    /**
//     * Concat the "videoInPaths" and deletes them, the full video will be in "videoOutPath"
//     * @param videoInPaths paths of videos to concat and delete
//     * @param videoOutPath path to the output file
//     * @param workingDirectory path to a working directory
//     * @return the termination exit code
//     * @throws Throwable
//     */
//    public int concatVideosTogether(String[] videoInPaths, String videoOutPath, String workingDirectory) throws Throwable
//    {
//        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
//        mediaMetadataRetriever.setDataSource(videoInPaths[0]);
//        int rotation = Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));
//        mediaMetadataRetriever.release();
//        String[] containers = new String[videoInPaths.length];
//        String concat="concat:";
//        String[] cmd;
//        for(int i=0;i<containers.length;i++)
//        {
//            containers[i]=workingDirectory+i+".ts";
//            concat+=containers[i];
//            if(i+1<containers.length) concat+="|";
//            cmd = new String[]{execPath, "-i", videoInPaths[i], "-c", "copy", "-f", "mpegts", containers[i]};
//            runCommandInternal(cmd);
//            new File(videoInPaths[i]).delete();
//        }
////        cmd = new String[]{execPath, "-y", "-i", concat, "-acodec", "copy", "-vcodec", "copy", "-bsf:a", "aac_adtstoasc", "-metadata:s:v:0", "rotate="+rotation, "-preset", "ultrafast", videoOutPath};
//        cmd = new String[]{execPath, "-y", "-i", concat, "-acodec", "copy", "-vcodec", "copy", "-bsf:a", "aac_adtstoasc", "-metadata:s:v:0", "rotate="+rotation, "-preset", "ultrafast", videoOutPath};//, "-r", "30"
//        int ret = runCommandInternal(cmd);
//        for(int i=0;i<containers.length;i++)
//            new File(containers[i]).delete();
//        return ret;
//    }

    /**
     * Creates new video with a background image.
     * @param videoFilePath the video we want to add a background
     * @param backgroundFilePath the background image we are going to add
     * @param outputFilePath the output video
     * @return the termination exit code
     * @throws Throwable
     */
    public int createVideoWithBackground(String videoFilePath, String backgroundFilePath, String outputFilePath) throws Throwable
    {
        int duration = 5; // minimum time of video.
        int videoHeight = 360; // default height.

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(videoFilePath);
        duration = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)) / 1000;
        int orientation = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));
        if(orientation == 90 || orientation == 270){
            videoHeight  = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
        }
        else {
            videoHeight  = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
        }

        android.graphics.Bitmap bg = android.graphics.BitmapFactory.decodeFile(backgroundFilePath);
//        int bgWidth  = bg.getWidth();
        int bgHeight = bg.getHeight();
        bg.recycle();

        int padding = (bgHeight - videoHeight) / 2;

        String[] cmd = {execPath, "-loop", "1", "-i", backgroundFilePath, "-i", videoFilePath,  "-vcodec", "mpeg4", "-filter_complex", "overlay=0:" + padding,"-ss","00:00.20", "-t", "0:" + duration + ".00", "-y", outputFilePath};

        return runCommandInternal(cmd);
    }

    /**
     * Concat the "videoInPaths" and deletes them, the full video will be in "videoOutPath"
     * @param videoInPaths paths of videos to concat and delete
     * @param videoOutPath path to the output file
     * @param workingDirectory path to a working directory
     * @param preSetRotation the pre set rotation in degrees or -1 to use the first video rotation as the rotation
     * @return the termination exit code
     * @throws Throwable
     */
    public int concatVideosTogether(String[] videoInPaths, String videoOutPath, String workingDirectory, int preSetRotation, int fps, String audioFilePath) throws Throwable
    {
        int rotation;

        if(preSetRotation == -1)
        {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(videoInPaths[0]);
            rotation = Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));
            mediaMetadataRetriever.release();
        }
        else rotation = preSetRotation;

        String[] containers = new String[videoInPaths.length];
        String concat="concat:";
        String[] cmd;

        for(int i=0;i<containers.length;i++)
        {
            containers[i]=workingDirectory+i+".ts";
            concat+=containers[i];
            if(i+1<containers.length) concat+="|";
            cmd = new String[]{execPath, "-i", videoInPaths[i], "-c", "copy", "-f", "mpegts", containers[i]};
            int ret = runCommandInternal(cmd);

            new File(videoInPaths[i]).delete();
        }

        System.out.println(">>>>>ffmpeg>>>>>>concate: "+concat);

//        String outAllTs = workingDirectory + "all.ts";
//        cmd = new String[]{execPath, "-y", "-i", concat, "-c", "copy", outAllTs};
//        runCommandInternal(cmd);

        if(rotation != 0)
//            cmd = new String[]{execPath, "-y", "-i", concat, "-acodec", "copy", "-vcodec", "copy", "-bsf:a", "aac_adtstoasc", "-metadata:s:v:0", "rotate="+rotation, videoOutPath};
//            cmd = new String[]{execPath, "-y", "-i", concat, "-acodec", "copy", "-vcodec", "libx264", "-r", ""+fps,"-bsf:a", "aac_adtstoasc", "-metadata:s:v:0", "rotate="+rotation, videoOutPath};
//            cmd = new String[]{execPath, "-y", "-i", concat, "-acodec", "copy", "-vcodec", "libx264", "-r", ""+fps,"-bsf:a", "aac_adtstoasc", "-metadata:s:v:0", "rotate="+rotation, videoOutPath};


//          cmd = new String[]{execPath, "-y", "-i", concat, "-vf", "fps=fps="+fps, "-acodec", "copy", "-vcodec", "libx264", "-bsf:a", "aac_adtstoasc", "-metadata:s:v:0", "rotate="+rotation, videoOutPath};

            //cmd = new String[]{execPath, "-y", "-i", concat,                        "-acodec", "copy", "-vcodec", "copy"   , "-bsf:a", "aac_adtstoasc", "-metadata:s:v:0", "rotate="+rotation, videoOutPath};

//         String[]cmd = {execPath, "-i", videoInPathTemp, "-f", "s16le", "-ar", ""+sampleRate, "-ac", "" + 1, "-i", audioFilePath, "-vcodec", "copy", "-bsf:a", "aac_adtstoasc", "-acodec", "aac", "-shortest", "-y", outputPath};// "-bsf:a", "aac_adtstoasc",

            cmd = new String[]{execPath, "-y", "-i", concat,
                    "-f", "s16le", "-ar", ""+sampleRate, "-ac", "" + 1, "-i", audioFilePath
                    , "-acodec", "aac", "-vcodec", "copy", "-shortest", "-bsf:a", "aac_adtstoasc", "-metadata:s:v:0", "rotate="+rotation, videoOutPath};

//            cmd = new String[]{execPath, "-y", "-i", concat, "-acodec", "copy", "-vcodec", "libx264", "-bsf:a", "aac_adtstoasc", "-metadata:s:v:0", "rotate="+rotation, videoOutPath};
        else
            cmd = new String[]{execPath, "-y", "-i", concat, "-acodec", "copy", "-vcodec", "copy", "-bsf:a", "aac_adtstoasc", videoOutPath};
        int ret = runCommandInternal(cmd);

//        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
//        mediaMetadataRetriever.setDataSource(videoOutPath);
//        rotation = Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));

        for(int i=0;i<containers.length;i++)
            new File(containers[i]).delete();

        return ret;
    }

    /**
     *  Join video and audio together, considering that the audio and the video are already synced !!!
     *  Short 16bit Big endian
     * @param videoFilePath
     * @param audioFilePath
     * @param outputPath
     * @return the termination exit code
     * @throws Throwable
     */
    public int coupleVideoAndAudioS16BE(String videoFilePath, String audioFilePath, String outputPath, int fps) throws Throwable
    {
//        System.out.println(">>>>audioStartTimeStamp>>>coupleVideoAndAudioS16BE");
        String[]cmd = {execPath, "-i", videoFilePath, "-f", "s16be", "-ar", "44100", "-ac", "1", "-i", audioFilePath, "-vcodec", "copy", "-acodec", "aac", "-r", ""+fps, outputPath};// "-bsf:a", "aac_adtstoasc",
        return runCommandInternal(cmd);
    }

    public int coupleVideoAndAudioS16LE(String videoFilePath, String audioFilePath, String outputPath, int fps) throws Throwable
    {
        String[]cmd = {execPath, "-i", videoFilePath, "-f", "s16le", "-ar", "44100", "-ac", "1", "-i", audioFilePath, "-vcodec", "copy", "-acodec", "aac", "-r", ""+fps, outputPath};// "-bsf:a", "aac_adtstoasc",
        return runCommandInternal(cmd);
    }

    /**
     *  Join video and audio together, considering that the audio and the video are already synced !!!
     *  mp3
     * @param videoFilePath
     * @param audioFilePath
     * @param outputPath
     * @return the termination exit code
     * @throws Throwable
     */

//    public int coupleVideoAndAudioMp3(String videoFilePath, String audioFilePath, String outputPath, int fps) throws Throwable
//    {
//        if(new File(outputPath).exists())
//            new File(outputPath).delete();
////        String[]cmd = {execPath, "-i", videoFilePath, "-i", audioFilePath, "-codec", "copy", "-r", ""+fps, "-shortest", "-preset", "ultrafast", outputPath};// "-bsf:a", "aac_adtstoasc",
//        String[]cmd = {execPath, "-i", videoFilePath, "-i", audioFilePath, "-vcodec", "copy", "-acodec", "aac", "-shortest", outputPath};// "-bsf:a", "aac_adtstoasc",
//        return runCommandInternal(cmd);
//    }

    public int coupleVideoAndAudioMp3(String videoFilePath, String audioFilePath, String outputPath, int fps) throws Throwable
    {
//        if(new File(outputPath).exists())
//            new File(outputPath).delete();
        String videoInPathTemp = videoFilePath;
        boolean deleteTemp = false;
        if(videoFilePath.equalsIgnoreCase(outputPath))
        {
            deleteTemp = true;
            videoInPathTemp = App.VIDEO_WORKING_FOLDER + "tempLocal.mp4";
            if(new File(videoInPathTemp).exists())
                new File(videoInPathTemp).delete();
            new File(videoFilePath).renameTo(new File(videoInPathTemp));
        }
//        String[]cmd = {execPath, "-i", videoFilePath, "-i", audioFilePath, "-codec", "copy", "-r", ""+fps, "-shortest", "-preset", "ultrafast", outputPath};// "-bsf:a", "aac_adtstoasc",
//        String[]cmd = {execPath, "-i", videoInPathTemp, "-f", "s16le", "-ar", ""+sampleRate, "-ac", "" + 1, "-i", audioFilePath, "-vcodec", "copy", "-bsf:a", "aac_adtstoasc", "-acodec", "aac", "-r", ""+fps, "-shortest", "-y", outputPath};// "-bsf:a", "aac_adtstoasc",

        if(true)
            return 0;

        String[]cmd = {execPath, "-i", videoInPathTemp, "-f", "s16le", "-ar", ""+sampleRate, "-ac", "" + 1, "-i", audioFilePath, "-vcodec", "copy", "-bsf:a", "aac_adtstoasc", "-acodec", "aac", "-shortest", "-y", outputPath};// "-bsf:a", "aac_adtstoasc",

        Tic.tic(400);
        int ans = runCommandInternal(cmd);
        Tic.tac(400);


        if(deleteTemp)
            new File(videoInPathTemp).delete();

        return ans;
    }

    public int coupleVideoAndAudioWav(String videoFilePath, String audioFilePath, String outputPath, int fps) throws Throwable
    {
        if(new File(outputPath).exists())
            new File(outputPath).delete();
        String[]cmd = {execPath, "-i", videoFilePath, "-i", audioFilePath, "-acodec", "aac", "-shortest", outputPath};// "-bsf:a", "aac_adtstoasc",
        return runCommandInternal(cmd);
    }


    public int splitAudioTrack(String audioFileInPath, String audioFileOutPath, int time) throws Throwable
    {
        if(new File(audioFileOutPath).exists())
            new File(audioFileOutPath).delete();
        String[]cmd = {execPath, "-ss", "0", "-t", ""+time, "-i", audioFileInPath, "-codec", "copy", "-shortest", audioFileOutPath};// "-bsf:a", "aac_adtstoasc",
        return runCommandInternal(cmd);
    }

    public int convertMp3ToPCM(String mp3FilePath, String pcmOutputPath) throws Throwable
    {
        String[]cmd = {execPath, "-i", mp3FilePath, "-acodec", "pcm_s16le", "-ac", "1", pcmOutputPath};
        return runCommandInternal(cmd);
    }

    /**
     *  Extract the first frame from the video and writes it to the output path
     * @param videoInPath
     * @param imageOutPath
     * @return the termination exit code
     * @throws Throwable
     */
    public int extractFrameFromVideo(String videoInPath, String imageOutPath) throws Throwable
    {
        String[]cmd = {execPath, "-i", videoInPath, "-frames:v", "1", imageOutPath};
        return runCommandInternal(cmd);
    }

//    /**
//     * Adds each image to a frame on the video as overlay
//     * @param videoInFileName the main video to attach the images
//     * @param imagesInFileName the images prefix name (e.g. "overlay.png" and the actual file names are overlay%d.png, where %d is a number with no leading zeros, in order)
//     * @param videoOutFileName the video out path
//     * @return the termination exit code
//     * @throws Throwable
//     */
//    public int renderWithImages(String videoInFileName, String imagesInFileName, String videoOutFileName)throws Throwable
//    {
//        String extension = imagesInFileName.substring(imagesInFileName.length()-4);
//        imagesInFileName = imagesInFileName.substring(0, imagesInFileName.length()-4);
//        String[] cmd = new String[]{execPath, "-y", "-r", CameraRecorder.sFps +"", "-i", videoInFileName, "-i", imagesInFileName + "%d" + extension, "-strict", "experimental", "-filter_complex", "overlay", "-preset", "ultrafast", "-vcodec", "libx264", "-ab", "48000", "-ac", "2", "-ar", "22050", "-b", defaultBitrate + "k", videoOutFileName};
//        return runCommandInternal(cmd);
//    }

    public int renderImagesToVideo(String framtOuputPath, String frame_extension, String outputFile, int fps) throws Throwable
    {
        String[] cmd = {execPath, "-f", "image2", "-r", fps +"", "-i", framtOuputPath +"%02d" + frame_extension, "-vcodec", "libx264", "-preset", "ultrafast", "-r", fps +"", outputFile};//, "-r", fps +"", "-b", bitrate +"", "-pix_fmt", "yuv420p"  - , "-pix_fmt", "yuv420p", "-bsf:a", "aac_adtstoasc"
//        String[] cmd = {execPath, "-f", "image2", "-i", framtOuputPath +"%02d" + frame_extension, "-vcodec", "libx264", "-preset", "ultrafast", "-vf", "fps=fps="+fps, outputFile};//, "-r", fps +"", "-b", bitrate +"", "-pix_fmt", "yuv420p"  - , "-pix_fmt", "yuv420p", "-bsf:a", "aac_adtstoasc"
        return runCommandInternal(cmd);
    }

    /**
     *
     * splits the video from the audio, the video is stored in frames (images) with the provided video name prefix
     * and the audio in the extension provided by the file name
     *
     * @param videoInFilePath
     * @param audioOutFilePath
     * @param videoOutPathAndPrefix
     * @return the termination exit code
     * @throws Throwable
     */
    public int splitAudioAndVideo(String videoInFilePath, String audioOutFilePath, String videoOutPathAndPrefix, boolean isCamera2, boolean useLastCreatedFile) throws Throwable
    {
        loadedFps = 25;// default 25
        stream0StartTime = 0;

//        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//        try
//        {
////            retriever.setDataSource(videoInFilePath);
////            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
////            String fps = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
////            String fps = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CAPTURE_FRAMERATE);
//        }
//        catch (Throwable throwable)
//        {
//            throwable.printStackTrace();
//            return -1;
//        }
//        finally
//        {
//            retriever.release();
//        }

        int width  = (int) MINIMUM_VIDEO_HEIGHT;
        int height = DEFAULT_WORKING_VIDEO_WIDTH;

        int croppedWidth  = (int) MINIMUM_VIDEO_HEIGHT, croppedHeight = DEFAULT_WORKING_VIDEO_WIDTH;

        if(isLiteVideo())
        {
            croppedWidth = width = (int) MINIMUM_VIDEO_HEIGHT_LITE;
            croppedHeight = height = DEFAULT_WORKING_VIDEO_WIDTH_LITE;

//            croppedWidth = 320;
//            croppedHeight = 240;

//            croppedWidth = 240;
//            croppedHeight = 320;

            try
            {
                int[] videoDar = getVideoDarLite(videoInFilePath);
                width = videoDar[1];
                height = videoDar[0];
            }catch (Throwable throwable) {}
        }
        else// if(false)
        {
            croppedWidth = 480;
            croppedHeight = 360;

//            croppedWidth = 360;
//            croppedHeight = 480;


//            >>>>err>>>/data/data/fm.bling.blingy/files/ffmpeg
//            04-26 21:32:36.881 25677-29261/fm.bling.blingy I/System.out: >>>>err>>>-t
//            04-26 21:32:36.881 25677-29261/fm.bling.blingy I/System.out: >>>>err>>>00:00:14
//            04-26 21:32:36.881 25677-29261/fm.bling.blingy I/System.out: >>>>err>>>-i
//            04-26 21:32:36.881 25677-29261/fm.bling.blingy I/System.out: >>>>err>>>/storage/emulated/0/videos/video/-169784465.mp4
//            04-26 21:32:36.881 25677-29261/fm.bling.blingy I/System.out: >>>>err>>>-vf
//            04-26 21:32:36.881 25677-29261/fm.bling.blingy I/System.out: >>>>err>>>scale=320:240,crop=320:240:0:0,transpose=1
//            04-26 21:32:36.881 25677-29261/fm.bling.blingy I/System.out: >>>>err>>>-y
//            04-26 21:32:36.881 25677-29261/fm.bling.blingy I/System.out: >>>>err>>>/storage/emulated/0/videos/video/frames/frame%04d.jpg
//            04-26 21:32:36.881 25677-29261/fm.bling.blingy I/System.out: >>>>err>>>-y
//            04-26 21:32:36.881 25677-29261/fm.bling.blingy I/System.out: >>>>err>>>/storage/emulated/0/videos/tempMp3.mp3
//            04-26 21:32:41.431 25677-29261/fm.bling.blingy I/System.out: 0>>>>err>>>Not assigned
//            04-26 21:32:42.041 25677-29431/fm.bling.blingy I/System.out: >>>>>error>>>0>>>Not assigned
//            04-26 21:32:42.041 25677-29431/fm.bling.blingy I/System.out: >>>>>error>>duration>14000
//

            try
            {
                int[] videoDar = getVideoDar(videoInFilePath);
                width = videoDar[1];
                height = videoDar[0];
            }catch (Throwable throwable) {}
        }

        int totalDuration = clipDuration = (int)(getFileDuration(videoInFilePath, null)/1000);
        if(clipDuration > 15)
            clipDuration = 15;

//        System.out.println(">>>>ffmpeg>>>>clipDuration:"+clipDuration);

        float absStartTime = stream0StartTime;
        if(!isFirstStreamIsVideo && absStartTime < 0)
        {
            absStartTime *= -1;
        }
        else if(isFirstStreamIsVideo && absStartTime > 0)
            ;
        else
            absStartTime = 0;

        int durationToExport = totalDuration;
        if(durationToExport > 15 + absStartTime)
            durationToExport = (int) (15 + absStartTime);
//            if(absStartTime < 0)
//        {
//
//
//        }
//        else if(clipDuration > 16)
//            clipDuration = 16;



        int xCropPos = (width - croppedWidth) / 2, yCropPos = 0;//(height - croppedHeight) / 2;
        if(xCropPos < 0)
            xCropPos = 0;


//        System.out.println(">>>>ffmpeg>>>>clipDuration:"+clipDuration+">>>>"+durationToExport+">>>"+absStartTime+">>>>width = "+width+">>>>height="+height+">>>>cW="+croppedWidth+">>>>cH="+croppedHeight+">>>>xCropPos="+xCropPos);

//            String[] cmd = {execPath, "-t", "00:00:15", "-i", videoInFilePath, "-s", height + "x" + width, "-vf", "crop=480:360", "-y", videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",

        String fileTempPath = App.VIDEO_WORKING_FOLDER + "blingy_vid.mp4";

        if(!useLastCreatedFile)
            fileTempPath = videoInFilePath;

//        String vf = "";

      String vf = "scale=" + width + ":" + height +"," + "crop=" + croppedWidth + ":" + croppedHeight + ":" + xCropPos + ":" + yCropPos;

//        String vf = "scale=" + 853 + ":" + 480 +",setsar=4:3,setdar=16:9," + "crop=" + croppedWidth + ":" + croppedHeight + ":" + xCropPos + ":" + yCropPos;
//        String vf = "crop=" + croppedWidth + ":" + croppedHeight + ":" + xCropPos + ":" + yCropPos;
//        String vf = "scale=" + 640 + ":" + 360 +"," + "crop=" + croppedWidth + ":" + croppedHeight + ":" + xCropPos + ":" + yCropPos;

        vf += (isCamera2 ? ",transpose=0" : ",transpose=1");

//        String[] cmd = {execPath, "-t", "00:00:"+seconds, "-r", "" + loadedFps, "-i", fileTempPath, "-vf", vf, "-y", "-r", "" + loadedFps, videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
//        String[] cmd = {execPath, "-ss", "00:00:02.390724", "-t", "00:00:"+seconds, "-r", "" + loadedFps, "-i", fileTempPath, "-vf", vf, "-y", "-r", "" + loadedFps, videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",

//        String[] cmd = {execPath, "-r", "" + loadedFps, "-i", fileTempPath, "-vf", vf, "-y", "-r", "" + loadedFps, "-t", "00:00:"+durationToExport, videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",

        String[] cmd = {execPath, "-r", "" + loadedFps, "-i", fileTempPath, "-vf", vf, "-y", "-r", "" + loadedFps, "-t", "00:00:"+durationToExport, "-qscale", "3", videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",

//        String[] cmd = {execPath, "-r", "" + loadedFps, "-i", fileTempPath, "-vcodec", "libx264", "-vf", vf, "-r", "" + loadedFps, "-t", "00:00:"+durationToExport, "-y", videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",

//        if(width != croppedWidth || height != croppedHeight)
//        {
////            String[] cmd = {execPath, "-t", "00:00:15", "-i", videoInFilePath, "-vf", "scale=" + height + ":" + width + ", transpose=1" +", " + "crop=" + croppedHeight + ":" + croppedWidth + ":" + xCropPos + ":" + yCropPos, "-y", videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
////            String[] cmd = {execPath, "-t", "00:00:15", "-i", videoInFilePath, "-vf", "scale=" + height + ":" + width +", " + "crop=" + croppedHeight + ":" + croppedWidth + ":" + xCropPos + ":" + yCropPos + ", transpose=1", "-y", videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
//            String[] cmd = {execPath, "-t", "00:00:15", "-i", videoInFilePath, "-vf", "scale=" + width + ":" + height +", " + "crop=" + croppedWidth + ":" + croppedHeight + ":" + xCropPos + ":" + yCropPos + ", transpose=1", "-y", videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
//
//            return runCommandInternal(cmd);
//        }
//        else
//        {
//            String[] cmd = {execPath, "-t", "00:00:15", "-i", videoInFilePath, "-vf", "scale=" + height + ":" + width + ", transpose=1", "-y", videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
//            return runCommandInternal(cmd);
//        }
//        String[] cmd = {execPath, "-t", "00:00:15", "-i", videoInFilePath, "-vf", "scale=" + width + ":" + height + ", transpose=1", "-y", videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
        return runCommandInternal(cmd);
    }

//    public int splitAudioAndVideo(String videoInFilePath, String audioOutFilePath, String videoOutPathAndPrefix) throws Throwable
//    {
//
//        int width = DEFAULT_WORKING_VIDEO_WIDTH;
//        int height = (int) MINIMUM_VIDEO_HEIGHT;
//
//        if(isLiteVideo())
//        {
//            width = DEFAULT_WORKING_VIDEO_WIDTH_LITE;
//            height = (int) MINIMUM_VIDEO_HEIGHT_LITE;
//            try
//            {
//                int[] videoDar = getVideoDarLite(videoInFilePath);
//                width = videoDar[0];
//                height = videoDar[1];
//            }catch (Throwable throwable) {}
//        }
//        else
//        {
//            try
//            {
//                int[] videoDar = getVideoDar(videoInFilePath);
//                width = videoDar[0];
//                height = videoDar[1];
//            }catch (Throwable throwable) {}
//        }
//
//
//        String[] cmd = {execPath, "-t", "00:00:15", "-i", videoInFilePath, "-s", height + "x" + width, "-y", videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
//        return runCommandInternal(cmd);
//    }

//
//    public int splitAudioAndVideo(String videoInFilePath, String audioOutFilePath, String videoOutPathAndPrefix, int fps) throws Throwable
//    {
//
////        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
////        mmr.setDataSource(videoInFilePath);
////        int width = Integer.parseInt(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
////
////        if(width < 480)
////            width = 480;
////
////        int height = width * 16 / 9;
//
//        int width = 480;
//        int height = 853;
//
////        int width = 360;
////        int height = 640;
//
//
//
//        if (fps == -1) fps = 25;
////        {
////            String[] cmd = {execPath, "-i", videoInFilePath, "-y", videoOutPathAndPrefix, "-acodec", "pcm_s16le", "-ac", "" + 1, audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
////            return runCommandInternal(cmd);
////        }
////        else
//        {       //"-ss", "00:00:00" , "-t", "00:30:00", "-r", fps + ""
////            "-s", "640X480",      //, "-vf", "transpose=1"        "-acodec", "pcm_s16le", "-ac", "" + 1
////            String[] cmd = {execPath, "-i", videoInFilePath, "-y", "-r", fps + "", videoOutPathAndPrefix, audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
////            String[] cmd = {execPath, "-i", videoInFilePath, "-y", "-r", fps + "", "-vf", "transpose=1,scale=640X480,setsar=1:1", videoOutPathAndPrefix, audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
//            String[] cmd = {execPath, "-t", "00:00:15", "-i", videoInFilePath, "-s", height + "x" + width, "-y", videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
////            String[] cmd = {execPath, "-i", videoInFilePath};// "-bsf:a", "aac_adtstoasc",
////            String[] cmd = {execPath, "-i", videoInFilePath, "-r", fps + "", "-ac", "1", "-y", videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
//            return runCommandInternal(cmd);
//        }
//    }

//    public int splitAudioAndVideo(String videoInFilePath, String audioOutFilePath, String videoOutPathAndPrefix, int fps) throws Throwable
//    {
////        if(fps == -1)
////            fps = 25;
////        {
////            String[] cmd = {execPath, "-i", videoInFilePath, "-y", videoOutPathAndPrefix, "-acodec", "pcm_s16le", "-ac", "" + 1, audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
////            return runCommandInternal(cmd);
////        }
////        else
//        {
////            "-s", "640X480",      //, "-vf", "transpose=1"        "-acodec", "pcm_s16le", "-ac", "" + 1       "-r", fps + "",
////            String[] cmd = {execPath, "-i", videoInFilePath, "-y", "-r", fps + "", videoOutPathAndPrefix, audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
////            String[] cmd = {execPath, "-i", videoInFilePath, "-y", "-r", fps + "", "-vf", "transpose=1,scale=640X480,setsar=1:1", videoOutPathAndPrefix, audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
//            String[] cmd = {execPath, "-t", "00:00:16", "-i", videoInFilePath, "-y", videoOutPathAndPrefix, "-y", audioOutFilePath};// "-bsf:a", "aac_adtstoasc",
//            return runCommandInternal(cmd);
//        }
//    }

    /**
     * returns array of two ints widht the dar (display aspect ratio) assigned to it
     * 0 - new width
     * 1 - new height
     */
    private int[] getVideoDar(String videoInFilePath) throws Throwable
    {
//        clearLogcat();

//        int ret = runCommandInternal(cmd);
        int[] newWidthHeight = new int[2];
//        Thread.sleep(100);


        String[] cmd = {execPath, "-i", videoInFilePath};// "-bsf:a", "aac_adtstoasc",

        String dar = getDarFromOutput(cmd);
        System.out.println(">>>>>ffmpeg>>>>>dar>>>>"+dar);
        if(dar != null)
        {
            newWidthHeight[0] = Integer.parseInt(dar.substring(4, dar.indexOf(":")));
            newWidthHeight[1] = Integer.parseInt(dar.substring(dar.indexOf(":") + 1));

            // work around
            //todo: change this so it will be more dynamic to support portrait
            if(newWidthHeight[0] < newWidthHeight[1])
            {
                newWidthHeight[0] = DEFAULT_VIDEO_HEIGHT;
                newWidthHeight[1] = DEFAULT_VIDEO_WIDTH;
            }

            int width = DEFAULT_WORKING_VIDEO_WIDTH;
            int height = width * newWidthHeight[0] / newWidthHeight[1];

            float scaleUp;
            if(height < MINIMUM_VIDEO_HEIGHT)
            {
                scaleUp = MINIMUM_VIDEO_HEIGHT / ((float)height);
                height = (int) MINIMUM_VIDEO_HEIGHT;
                width = (int) (((float)width) * scaleUp);
            }

            newWidthHeight[0] = width;
            newWidthHeight[1] = height;
        }
        else
        {
            newWidthHeight[0] = DEFAULT_VIDEO_HEIGHT;
            newWidthHeight[1] = DEFAULT_VIDEO_WIDTH;

            int width = DEFAULT_WORKING_VIDEO_WIDTH;
            int height = width * newWidthHeight[0] / newWidthHeight[1];

            float scaleUp;
            if(height < MINIMUM_VIDEO_HEIGHT)
            {
                scaleUp = MINIMUM_VIDEO_HEIGHT / ((float)height);
                height = (int) MINIMUM_VIDEO_HEIGHT;
                width = (int) (((float)width) * scaleUp);
            }

            newWidthHeight[0] = width;
            newWidthHeight[1] = height;
        }
        return newWidthHeight;
    }

    private String getDarFromOutput(String[] cmd) throws Throwable
    {
        ProcessBuilder pb = new ProcessBuilder("bash", "-c", "ioreg -l | awk '/IOPlatformSerialNumber/ { print $4;}'");
        try
        {
            pb.command(cmd);
            pb.redirectErrorStream(true);

            Process p = pb.start();

            String line, dar = null;
            // reads from the process's stdout & stderr probably till android O
            BufferedReader processOutputStream = new BufferedReader(new InputStreamReader(p.getInputStream()));

            while ((line = processOutputStream.readLine()) != null)
            {
                System.out.println(">>>>ffmpeg file metadata>>>>"+line);
                if(line.indexOf("Duration:") != -1)
                {
                    int index = line.indexOf("start:");
                    if(index != -1)
                    {
                        index = line.indexOf(" ", index) + 1;
                        int indexEnd = line.indexOf(",", index);
//                        System.out.println(">>>>>ffmpeg>>>>stream0StartTime>>1>>"+stream0StartTime+">>>"+line.length()+">>>"+">>>"+index+">>>"+indexEnd);
                        stream0StartTime = Float.parseFloat(line.substring(index, indexEnd));
//                        System.out.println(">>>>>ffmpeg>>>>stream0StartTime>>2>>"+stream0StartTime+">>>"+indexEnd);
                    }
                }

//                if(line.indexOf("#0:0") != -1)
//                {
//                    isFirstStreamIsVideo = line.indexOf("Video:") != -1;
//                }


                if(line.indexOf("Stream") != -1 && line.indexOf("Video") != -1)
                {

                    isFirstStreamIsVideo = line.indexOf("#0:0") != -1;

                    if(line.indexOf("fps") != -1)
                    {
                        int endIndex = line.indexOf("fps") - 1;
                        int startIndex = endIndex;

                        while(true)
                        {
                            startIndex--;
                            if(line.charAt(startIndex) == ',')
                            {
                                startIndex += 2; // to pass the comma and the whitespace too
                                loadedFps = (int) Float.parseFloat(line.substring(startIndex, endIndex));
                                break;
                            }
                        }
//                        , 29.97 fps,
                    }

                    if (line.indexOf("DAR") != -1)
                    {   int darIndex = line.indexOf("DAR");
                        int endIndex = line.indexOf(']', darIndex);
                        if( endIndex != -1 && endIndex < line.indexOf(',', darIndex))
                        {
                            dar = line.substring(darIndex, endIndex);
    //                    break;
                        }
                        else
                        {
                            endIndex = line.indexOf(',', darIndex);
                            dar = line.substring(darIndex, endIndex);
                        }
                    }
                    else// if(line.indexOf("Stream") != -1 && line.indexOf("Video") != -1)
                    {
                        try
                        {
                            int index = 0;
                            int commaCounter = 0;
                            boolean isInBraces = false;
                            while(index++ < line.length())
                            {
                                if(line.charAt(index) == '(')
                                {
                                    isInBraces = true;
                                    continue;
                                }
                                else if(line.charAt(index) == ')')
                                {
                                    isInBraces = false;
                                    continue;
                                }
                                else if(isInBraces)
                                    continue;
                                else if(line.charAt(index) == ',')
                                {
                                    commaCounter++;
                                    if(commaCounter == 2)
                                    {
                                        index++;
                                        break;
                                    }
                                }
                            }

//                        int resIndex = line.indexOf(",", line.indexOf(",") + 1) + 1;

                            if(index > 0 && index < line.length())
                            {
                                dar = line.substring(index, line.indexOf(",", index));
                                dar = dar.replaceAll("x",":");
                                dar = dar.replaceAll(" ", "");
                                dar = "DAR " + dar;
//                            break;
                            }
                        }
                        catch (Throwable err){}
                    }
                }


//                else continue;
            }

//            System.out.println(">>>ffmpeg file metaData out report : isFirstStreamIsVideo :" + isFirstStreamIsVideo+" . stream0StartTime:"+stream0StartTime
//            +" . FPS:"+loadedFps+" . dar:"+dar+" . clipDuration:"+clipDuration);

            p.getInputStream().close();
            p.getOutputStream().close();
            p.getErrorStream().close();
            processOutputStream.close();

            return dar;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

//        06-05 12:23:55.381 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>ffmpeg version n3.0.1 Copyright (c) 2000-2016 the FFmpeg developers
//        06-05 12:23:55.381 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>  built with gcc 4.8 (GCC)
//        06-05 12:23:55.381 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>  configuration: --target-os=linux --cross-prefix=/home/vagrant/SourceCode/ffmpeg-android/toolchain-android/bin/arm-linux-androideabi- --arch=arm --cpu=cortex-a8 --enable-runtime-cpudetect --sysroot=/home/vagrant/SourceCode/ffmpeg-android/toolchain-android/sysroot --enable-pic --enable-libx264 --enable-libass --enable-libfreetype --enable-libfribidi --enable-libmp3lame --enable-fontconfig --enable-pthreads --disable-debug --disable-ffserver --enable-version3 --enable-hardcoded-tables --disable-ffplay --disable-ffprobe --enable-gpl --enable-yasm --disable-doc --disable-shared --enable-static --pkg-config=/home/vagrant/SourceCode/ffmpeg-android/ffmpeg-pkg-config --prefix=/home/vagrant/SourceCode/ffmpeg-android/build/armeabi-v7a --extra-cflags='-I/home/vagrant/SourceCode/ffmpeg-android/toolchain-android/include -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=2 -fno-strict-overflow -fstack-protector-all' --extra-ldflags='-L/home/vagrant/SourceCode/ffmpeg-android/toolchain-android/lib -Wl,-z,relro -Wl,-z,now -pie' --extra-libs='-lpng -lexpat -lm' --extra-cxxflags=
//        06-05 12:23:55.381 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>  libavutil      55. 17.103 / 55. 17.103
//        06-05 12:23:55.381 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>  libavcodec     57. 24.102 / 57. 24.102
//        06-05 12:23:55.381 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>  libavformat    57. 25.100 / 57. 25.100
//        06-05 12:23:55.381 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>  libavdevice    57.  0.101 / 57.  0.101
//        06-05 12:23:55.381 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>  libavfilter     6. 31.100 /  6. 31.100
//        06-05 12:23:55.381 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>  libswscale      4.  0.100 /  4.  0.100
//        06-05 12:23:55.381 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>  libswresample   2.  0.101 /  2.  0.101
//        06-05 12:23:55.381 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>  libpostproc    54.  0.100 / 54.  0.100
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>Input #0, mov,mp4,m4a,3gp,3g2,mj2, from '/data/user/0/fm.bling.blingy/files/videos/video/-1026062279.mp4':
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>  Metadata:
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>    major_brand       : MP4
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>    minor_version     : 0
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>    compatible_brands : MP4 mp42isom
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>    creation_time     : 2010-08-31 21:02:23
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>    timecode          : 00:00:00:00
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>  Duration: 00:00:30.00, start: -2.390724, bitrate: 1135 kb/s
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>    Stream #0:0(eng): Audio: aac (LC) (mp4a / 0x6134706D), 44100 Hz, stereo, fltp, 125 kb/s (default)
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>    Metadata:
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>      creation_time   : 2010-08-31 21:02:23
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>      handler_name    : ?Apple Alias Data Handler
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>    Stream #0:1(eng)  : Video: h264 (Constrained Baseline) (avc1 / 0x31637661), yuv420p(tv, smpte170m/smpte170m/bt709), 640x464, 931 kb/s, 29.97 fps, 29.97 tbr, 2997 tbn, 5994 tbc (default)
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>    Metadata:
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>      creation_time   : 2010-08-31 21:02:23
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>      handler_name    : ?Apple Alias Data Handler
//        06-05 12:23:55.441 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>    Stream #0:2(eng)  : Data: none (tmcd / 0x64636D74) (default)
//        06-05 12:23:55.451 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>    Metadata:
//        06-05 12:23:55.451 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>      creation_time   : 2010-08-31 21:02:23
//        06-05 12:23:55.451 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>      handler_name    : ?Apple Alias Data Handler
//        06-05 12:23:55.451 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>      timecode        : 00:00:00:00
//        06-05 12:23:55.451 16479-17519/fm.bling.blingy I/System.out: >>>>ffmpeg file metadata>>>>At least one output file must be specified
//        06-05 12:23:

    private int[] getVideoDarLite(String videoInFilePath) throws Throwable
    {
//        clearLogcat();
//
//        String[] cmd = {execPath, "-i", videoInFilePath};// "-bsf:a", "aac_adtstoasc",
//        int ret = runCommandInternal(cmd);
//        int[] newWidthHeight = new int[2];
//        Thread.sleep(100);

        String[] cmd = {execPath, "-i", videoInFilePath};// "-bsf:a", "aac_adtstoasc",

//        String dar;
//        if((dar = getDarFromLogCat()) != null)

        String dar = getDarFromOutput(cmd);
        int[] newWidthHeight = new int[2];
        if(dar != null)
        {
            newWidthHeight[0] = Integer.parseInt(dar.substring(4, dar.indexOf(":")));
            newWidthHeight[1] = Integer.parseInt(dar.substring(dar.indexOf(":") + 1));

            // work around
            //todo: change this so it will be more dynamic to support portrait
            if(newWidthHeight[0] < newWidthHeight[1])
            {
                newWidthHeight[0] = DEFAULT_VIDEO_HEIGHT;
                newWidthHeight[1] = DEFAULT_VIDEO_WIDTH;
            }

            int width = DEFAULT_WORKING_VIDEO_WIDTH_LITE;
            int height = width * newWidthHeight[0] / newWidthHeight[1];
//
            float scaleUp = 0f;
            if(height < MINIMUM_VIDEO_HEIGHT_LITE)
            {
                scaleUp = MINIMUM_VIDEO_HEIGHT_LITE / ((float)height);
                height = (int) MINIMUM_VIDEO_HEIGHT_LITE;
                width = (int) (((float)width) * scaleUp);
            }

            newWidthHeight[0] = width;
            newWidthHeight[1] = height;
        }
        else
        {
            newWidthHeight[0] = DEFAULT_VIDEO_HEIGHT;
            newWidthHeight[1] = DEFAULT_VIDEO_WIDTH;

            int width = DEFAULT_WORKING_VIDEO_WIDTH_LITE;
            int height = width * newWidthHeight[0] / newWidthHeight[1];

            float scaleUp = 0f;
            if(height < MINIMUM_VIDEO_HEIGHT_LITE)
            {
                scaleUp = MINIMUM_VIDEO_HEIGHT_LITE / ((float)height);
                height = (int) MINIMUM_VIDEO_HEIGHT_LITE;
                width = (int) (((float)width) * scaleUp);
            }

            newWidthHeight[0] = width;
            newWidthHeight[1] = height;
        }
        return newWidthHeight;
    }

    private void clearLogcat() throws Throwable
    {
        Process process = Runtime.getRuntime().exec("logcat -c");
        process.waitFor();
//        process.destroy();
    }

    private String getDarFromLogCat()
    {
        try
        {
            Process process = Runtime.getRuntime().exec("logcat -d");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String dar = null, line;
            int lineCount = 0;
            int counter = 0;
            while ((line = bufferedReader.readLine()) != null && counter++ < 100)
            {
                lineCount++;
                if (line.indexOf("DAR") != -1 && line.indexOf(']', line.indexOf("DAR")) != -1)
                {
                    dar = line.substring(line.indexOf("DAR"), line.indexOf(']', line.indexOf("DAR")));
//                    break;
                }
                else continue;
            }
            return dar;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public int extractFirstFrameFromVideo(String videoInPath, String imageOutPath) throws Throwable
    {
        String[]cmd = {execPath, "-i", videoInPath, "-frames:v", "1", "-y", imageOutPath};
        return runCommandInternal(cmd);
    }


//    public int renderImagesToVideo(String framtOuputPath, String frame_extension, String outputFile, int fps) throws Throwable
//    {
//        String[] cmd = {execPath, "-f", "image2", "-r", fps +"", "-i", framtOuputPath +"%02d" + frame_extension, "-vcodec", "libx264", "-preset", "ultrafast", "-r", fps +"", "-pix_fmt", "yuv420p", "-bsf:a", "aac_adtstoasc", outputFile};//, "-r", fps +"", "-b", bitrate +"", "-pix_fmt", "yuv420p"
//        return runCommandInternal(cmd);
//    }

//    public int renderImagesToVideo(String framtOuputPath, String frame_extension, String outputFile, int fps) throws Throwable
//    {
//        String[] cmd = {execPath, "-f", "image2", "-r", fps +"", "-i", framtOuputPath +"%02d" + frame_extension, "-vcodec", "libx264", "-preset", "ultrafast", "-r", fps +"", "-pix_fmt", "yuv420p", "-bsf:a", "aac_adtstoasc", outputFile};//, "-r", fps +"", "-b", bitrate +"", "-pix_fmt", "yuv420p"
//        return runCommandInternal(cmd);
//    }

//    public int renderImagesToVideo(String framtOuputPath, String frame_extension, String outputFile, int fps) throws Throwable
//    {
//        String[] cmd = {execPath, "-f", "image2", "-framerate", fps +"", "-i", framtOuputPath +"%02d" + frame_extension, "-vcodec", "libx264", "-preset", "ultrafast", "-r", fps +"", "-pix_fmt", "yuv420p", outputFile};
//        return runCommandInternal(cmd);
//    }

    /**
     * Get the error msg as String, NOTE: it doesn't have to be an error msg !!!
     * @return the error message
     * @throws Throwable
     */
    public String getError() throws Throwable
    {
        if(errorStream.available()>0)
        {
            return convertInputStreamToString(errorStream);
        }
        return null;
    }

    private String convertInputStreamToString(InputStream inputStream) {
        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            String str;
            StringBuilder sb = new StringBuilder();
            while ((str = r.readLine()) != null) {
                sb.append(str);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param videoInFileName  original file path.
     * @param startTime        time in seconds.
     * @param duration         time in seconds.
     * @param videoOutFileName video file output path
     * @return the termination exit code
     * @throws Throwable
     */
    public int getVideoFromTime(String videoInFileName, String startTime , String duration , String videoOutFileName) throws Throwable{
        String cmd = "-i " + videoInFileName + " -ss " + startTime + " -t " + duration + " -c copy " + videoOutFileName;
        return runCommandInternal(cmd);
    }

    public int bufferSize;
    public int bitrate = 128000;
    public int sampleRate = 44100;
    public int channels = 2;
    public AudioTrack getAudioTrack(InputStream wavStream) throws Throwable
    {
        int HEADER_SIZE = 44;

        ByteBuffer buffer = ByteBuffer.allocate(HEADER_SIZE);
        buffer.order(ByteOrder.LITTLE_ENDIAN);

        wavStream.read(buffer.array(), buffer.arrayOffset(), buffer.capacity());

        buffer.rewind();
        buffer.position(buffer.position() + 20);
        int format = buffer.getShort();
        channels = buffer.getShort();
        sampleRate = buffer.getInt();
        buffer.position(buffer.position() + 6);
        int bits = buffer.getShort();

        bitrate = sampleRate * channels * bits;

        int dataSize = 0;
        while (buffer.getInt() != 0x61746164)
        {
            int size = buffer.getInt();
            wavStream.skip(size);

            buffer.rewind();
            wavStream.read(buffer.array(), buffer.arrayOffset(), 8);
            buffer.rewind();
        }
        dataSize = buffer.getInt();

        int audioFormat = bits == 16 ? AudioFormat.ENCODING_PCM_16BIT : AudioFormat.ENCODING_PCM_8BIT;
        int channelConfig = channels == 2 ? AudioFormat.CHANNEL_OUT_STEREO : AudioFormat.CHANNEL_OUT_MONO;
        bufferSize = AudioTrack.getMinBufferSize(sampleRate, channelConfig, audioFormat);

        if(bufferSize < 0)
            bufferSize = AudioTrack.getMinBufferSize(44100, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);

        return new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, channelConfig, audioFormat, bufferSize * 2, AudioTrack.MODE_STREAM);
    }

    private void testMem()
    {
        android.app.ActivityManager actManager = (android.app.ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        android.app.ActivityManager.MemoryInfo memInfo = new android.app.ActivityManager.MemoryInfo();
        actManager.getMemoryInfo(memInfo);
        totalMemory = memInfo.totalMem / (1000 * 1000);
    }

    private final int MIN_NUMBER_OF_CORES = 4;
//    private final long MIN_MEMORY = 1712014001L;
    private final long MIN_MEMORY = 2000L; // 2gb
    private long totalMemory;
    private boolean isLiteVideo = false, isLiteVideoSet = false;
    public boolean isLiteVideo()
    {
//        if(true)
//            return true;

        if(!isLiteVideoSet)
        {

            int cores = Runtime.getRuntime().availableProcessors();
            testMem();
//            isLiteVideo = cores < MIN_NUMBER_OF_CORES;
            isLiteVideo = totalMemory < MIN_MEMORY || cores < MIN_NUMBER_OF_CORES;
            isLiteVideoSet = true;
        }
        return isLiteVideo;
    }

    /**
     * Get the duration of the video
     * @param path - can be null if the uri not null.
     * @param uri - can be null if the path not null.
     * @return     video duration
     *          -1 something went wrong.
     */
    public long getFileDuration(@Nullable String path, @Nullable Uri uri)
    {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try
        {
            if (path != null)
                retriever.setDataSource(path);
            else
                retriever.setDataSource(mContext, uri);
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            return Long.parseLong(time);
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
            return -1;
        }
        finally
        {
            retriever.release();
        }
    }

    private boolean isCamera2IsSet = false, isCamera2 = false;
    public boolean getIsCamera2()
    {
        if(true)
            return false; // canceling camera II

        if(isCamera2IsSet)
            return isCamera2;
        if(android.os.Build.VERSION.SDK_INT <= 21) isCamera2 = false;
        else
        {
            android.hardware.camera2.CameraManager manager = (android.hardware.camera2.CameraManager) mContext.getSystemService(Context.CAMERA_SERVICE);
            try
            {
                String cameraIdS = manager.getCameraIdList()[0];
                android.hardware.camera2.CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraIdS);
                int support = characteristics.get(android.hardware.camera2.CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);

                isCamera2 = (support == CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_FULL || support == CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_3);
            }
            catch (Throwable e)
            {
                e.printStackTrace();
            }
        }
        isCamera2IsSet = true;

        return isCamera2;
    }

}