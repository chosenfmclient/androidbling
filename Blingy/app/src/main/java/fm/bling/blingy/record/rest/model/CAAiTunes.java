package fm.bling.blingy.record.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 5/3/16.
 * History:
 * ***********************************
 */
public class CAAiTunes
{

    @SerializedName("resultCount")
    private String resultCount;

    @SerializedName("results")
    private ArrayList<CAAItunesMusicElement> results;

    public String getResultCount() {
        return resultCount;
    }

    public ArrayList<CAAItunesMusicElement> getResults() {
        return results;
    }

}
