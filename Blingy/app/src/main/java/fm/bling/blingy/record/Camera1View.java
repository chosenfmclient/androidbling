package fm.bling.blingy.record;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fm.bling.blingy.ProcessImage;
import fm.bling.blingy.utils.FFMPEG;
import fm.bling.blingy.utils.Tic;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 24/05/2017.
 * History:
 * ***********************************
 */
public class Camera1View extends View implements Camera.PreviewCallback
{
    public static final int AR_OUTPUT_IMAGE_ROTATION = 270;
    private final int NUMBER_OF_MAX_FRAMES          = 2;
    private final int NUMBER_OF_MAX_DROPPED_FRAMES  = 1;

    private final int MAGIC_TEXTURE_ID = 10;

    public static final int CAMERA_IN_WIDTH = 640, CAMERA_IN_HEIGHT = 480, CAMERA_IN_HEIGHT_YUV = CAMERA_IN_HEIGHT + (CAMERA_IN_HEIGHT / 2);
    public int CAMERA_OUT_WIDTH = 360, CAMERA_OUT_HEIGHT = 480;

    public static final Size cameraInSize = new Size(CAMERA_IN_HEIGHT, CAMERA_IN_WIDTH);

    private int currentLoadedFrames = 0;
    private int currentLoadedDroppedFrames = 0;

    private Bitmap currentBitmap;
    private Paint mPaint;
    private Matrix mMatrix;

    private Camera mCamera;
    private int mCameraId;
    private int cameraRotation;
    private boolean isFirstThread = false;

    private byte[] mBuffer;
    public static int bufferSize;
    private SurfaceTexture mSurfaceTexture;
    private OnFrameAvailableListener mOnFrameAvailableListener;

    private OnImageAdjustmentsOff mOnImageAdjustmentsOff;
    private boolean isClosed = false;

    private long currentTime;

//    private VideoRecorder mVideoRecorder;

    private ExecutorService firstFrameProcessorExecutor, secondFrameProcessorExecutor, droppedFramesExecutor;

    private String frameOutputPathPrefix;

    public Camera1View(Context context, int cameraId, String iFrameOutputPathPrefix) throws Throwable
    {
        super(context);
        mCameraId = cameraId;
        frameOutputPathPrefix = iFrameOutputPathPrefix;
        setBackgroundColor(Color.argb(0, 0, 0, 0));
        init(context);
    }

    private void init(Context context) throws Throwable
    {
        if (mCamera != null) return;

        firstFrameProcessorExecutor = Executors.newFixedThreadPool(1);
        secondFrameProcessorExecutor = Executors.newFixedThreadPool(1);
        droppedFramesExecutor = Executors.newFixedThreadPool(1);


        if (FFMPEG.getInstance(context).isLiteVideo())
        {
            CAMERA_OUT_WIDTH = 240;
            CAMERA_OUT_HEIGHT = 320;
        }

        int height = (int) (((float) context.getResources().getDisplayMetrics().widthPixels) * (((float) CAMERA_OUT_WIDTH) / ((float) CAMERA_OUT_HEIGHT)));
        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));

        mPaint = new Paint();
        mPaint.setAntiAlias(true);

        currentBitmap = Bitmap.createBitmap(CAMERA_OUT_WIDTH, CAMERA_OUT_HEIGHT, Bitmap.Config.ARGB_8888);
        initCamera(context);
    }

    private void initCamera(Context context) throws Throwable
    {
        if (mCamera != null) return;

        mMatrix = new Matrix();
        try
        {
            mCamera = Camera.open(mCameraId);
        }
        catch (Throwable err)
        {
            try
            {
                releaseCamera();
            }
            catch (Throwable err2)
            {
            }
            try
            {
                mCamera = Camera.open(mCameraId);
            }
            catch (Throwable err1)
            {
                releaseCamera();
                throw new Throwable("couldn't open the camera");
            }
        }

        Camera.Parameters params = mCamera.getParameters();
        params.setPreviewSize(CAMERA_IN_WIDTH, CAMERA_IN_HEIGHT);
        params.setPreviewFormat(ImageFormat.NV21);
//        params.setFocusMode(Camera.Parameters.FOCUS_MODE_FIXED);

        try
        {
            mCamera.setParameters(params);
        }
        catch (Throwable err)
        {
            throw new Throwable("couldn't set the camera's parameters");
        }

        bufferSize = CAMERA_IN_WIDTH * CAMERA_IN_HEIGHT;
        bufferSize = bufferSize * ImageFormat.getBitsPerPixel(params.getPreviewFormat()) / 8;
        mBuffer = new byte[bufferSize];

        mCamera.addCallbackBuffer(mBuffer);
        mCamera.setPreviewCallbackWithBuffer(this);

        mSurfaceTexture = new SurfaceTexture(MAGIC_TEXTURE_ID);
        mCamera.setPreviewTexture(mSurfaceTexture);

        //    public final static int RES_960X720 = 1, RES_1280X960 = 2, RES_1280_720 = 3, RES_1024X768 = 4, RES_640X480 = 5, RES_CUSTOM = 0;
        //    We are doing 640 * 480 static so we pass 5 here


        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(mCameraId, info);
        cameraRotation = info.orientation;

        ProcessImage.getInstance().setCameraRes(5, FFMPEG.getInstance(context).isLiteVideo(), CAMERA_IN_HEIGHT, CAMERA_IN_WIDTH);
        ProcessImage.getInstance().setCameraRotation(cameraRotation, mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT);

        float screenWidth = getResources().getDisplayMetrics().widthPixels;
        float scaleFactor;

        mMatrix.setRotate(AR_OUTPUT_IMAGE_ROTATION); // according to the ar output image
        switch (AR_OUTPUT_IMAGE_ROTATION)
        {
            case 90:
                mMatrix.postTranslate(CAMERA_OUT_HEIGHT, 0);
                break;
            case 270:
                mMatrix.postTranslate(0, CAMERA_OUT_WIDTH);
                break;
            case 180:
                mMatrix.setRotate(AR_OUTPUT_IMAGE_ROTATION, CAMERA_OUT_WIDTH / 2, CAMERA_OUT_HEIGHT / 2);
                break;
        }
        mMatrix.preScale(1, 1, CAMERA_OUT_WIDTH / 2, CAMERA_OUT_HEIGHT / 2);

//        if (mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT)
//        {
//            switch (cameraRotation)
//            {
//                case 90:
//                    mMatrix.setRotate(cameraRotation);
//                    mMatrix.postTranslate(CAMERA_OUT_HEIGHT, 0);
//                    break;
//                case 270:
//                    mMatrix.setRotate(cameraRotation);
//                    mMatrix.postTranslate(0, CAMERA_OUT_WIDTH);
//                    break;
//                case 180:
//                    mMatrix.setRotate(cameraRotation, CAMERA_OUT_WIDTH / 2, CAMERA_OUT_HEIGHT / 2);
//                    break;
//            }
//            mMatrix.preScale(1, 1, CAMERA_OUT_WIDTH / 2, CAMERA_OUT_HEIGHT / 2);
//        }
//        else
//        {
//            switch (cameraRotation)
//            {
//                case 90:
//                    mMatrix.setRotate(cameraRotation);
//                    mMatrix.postTranslate(CAMERA_OUT_HEIGHT, 0);
//                    break;
//                case 270:
//                    mMatrix.setRotate(cameraRotation);
//                    mMatrix.postTranslate(0, CAMERA_OUT_WIDTH);
//                    break;
//                case 180:
//                    mMatrix.setRotate(cameraRotation, CAMERA_OUT_WIDTH / 2, CAMERA_OUT_HEIGHT / 2);
//                    break;
//            }
//            mMatrix.preScale(1, 1, CAMERA_OUT_WIDTH / 2, CAMERA_OUT_HEIGHT / 2);
//        }
        mMatrix.postScale(scaleFactor = (screenWidth / ((float) CAMERA_OUT_HEIGHT)), scaleFactor);
        mCamera.startPreview();
    }

    private void releaseCamera()
    {
        if (mCamera != null)
        {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
        }
        mCamera = null;
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera)
    {
        if(isClosed) return;

        if (currentLoadedFrames >= NUMBER_OF_MAX_FRAMES)
        {
            if (currentLoadedDroppedFrames < NUMBER_OF_MAX_DROPPED_FRAMES && mOnFrameAvailableListener != null && mOnFrameAvailableListener.shouldCatchDroppedFrames())
            {
                currentLoadedDroppedFrames++;
                droppedFramesExecutor.submit(new DroppedFrameProcessor(data, SystemClock.elapsedRealtimeNanos()));

//                processDroppedFrameSynced(data, SystemClock.elapsedRealtimeNanos());

                mCamera.addCallbackBuffer(mBuffer = new byte[mBuffer.length]);
                return;
            }

            mCamera.addCallbackBuffer(mBuffer);
            return;
        }

        if (isFirstThread = !isFirstThread)
        {
            firstFrameProcessorExecutor.submit(new FrameProcessor(data, SystemClock.elapsedRealtimeNanos()));
        }
        else
            secondFrameProcessorExecutor.submit(new FrameProcessor(data, SystemClock.elapsedRealtimeNanos()));
        currentLoadedFrames++;
//        new Thread(new FrameProcessor(data, SystemClock.elapsedRealtimeNanos())).start();
        mCamera.addCallbackBuffer(mBuffer);
    }

    @SuppressLint("MissingSuperCall")
    public void draw(Canvas canvas)
    {
        if (isClosed) return;
        try
        {
            synchronized (this)
            {
                canvas.drawBitmap(currentBitmap, mMatrix, mPaint);
            }
        }
        catch (Throwable err)
        {
            err.printStackTrace();
        }
//        canvas.drawText(FPSLabel + " | Rot : " + cameraRotation + " | time : " + elpasedTime, 20, 450, mPaint);
    }

    public void setOnFrameAvailableListener(OnFrameAvailableListener onFrameAvailableListener)
    {
        mOnFrameAvailableListener = onFrameAvailableListener;
    }

//    public void setVideoRecorder(VideoRecorder videoRecorder)
//    {
//        this.mVideoRecorder = mVideoRecorder;
//    }

//    private void recycleBitmap(Bitmap bitmapToRecycle)
//    {
//        while (bitmapToRecycle == currentBitmap)
//        {
//            if (isClosed)
//                return;
//            try
//            {
//                Thread.sleep(40);
//            }
//            catch (Throwable e)
//            {
//            }
//        }
//        bitmapToRecycle.recycle();
//    }


    public class FrameProcessor implements Runnable
    {
        Mat frameNV21 = null;
        long mTime;
        boolean isFirst;

        public FrameProcessor(byte[] data, long iTime)
        {
            try { frameNV21 = new Mat(CAMERA_IN_HEIGHT_YUV, CAMERA_IN_WIDTH, CvType.CV_8UC1); } catch (Throwable err) { return; }
            frameNV21.put(0, 0, data);
            mTime = iTime;
            isFirst = isFirstThread;
        }

        @Override
        public void run()
        {
//            if (isFirst)
//                Tic.tic();
            if(frameNV21 == null) return;

            Mat rgba = new Mat(cameraInSize, CvType.CV_8UC4);

//            Tic.tacTicWithHighLow();
//            Tic.tic();
            Imgproc.cvtColor(frameNV21, rgba, Imgproc.COLOR_YUV2RGBA_NV21, 4);
//            Tic.tacWithAverage();
//            Tic.tac(747);

            frameNV21.release();

//            Tic.tic();
            if (!isClosed && mOnFrameAvailableListener != null)
                mOnFrameAvailableListener.onFrameAvailable(rgba, mTime);
            else return;
//            Tic.tacWithAverage();


            if (currentTime < mTime)
            {
                currentTime = mTime;
                synchronized (Camera1View.this)
                {
//                Bitmap temp;
//                temp = Bitmap.createBitmap(CAMERA_OUT_WIDTH, CAMERA_OUT_HEIGHT, Bitmap.Config.ARGB_8888);
//                Utils.matToBitmap(rgba, temp);
                    if(!isClosed && currentBitmap!= null && !currentBitmap.isRecycled())
                        try { Utils.matToBitmap(rgba, currentBitmap); } catch (Throwable err) { }
//                try { currentBitmap.recycle(); } catch (Throwable err) { }
//                currentBitmap = temp;
                    postInvalidate();
                }
            }

            rgba.release();

//            if (isFirst)
//                Tic.tacWithAverage();

            currentLoadedFrames--;
        }
    }

    public class DroppedFrameProcessor implements Runnable
    {
//        Mat frameNV21;
        byte[] frameBuffer;
        long mTime;

        public DroppedFrameProcessor(byte[] iFrameBuffer, long iTime)
        {
//            frameNV21 = new Mat(CAMERA_IN_HEIGHT_YUV, CAMERA_IN_WIDTH, CvType.CV_8UC1);
//            frameNV21.put(0, 0, data);
            frameBuffer = iFrameBuffer;
            mTime = iTime;
        }

        @Override
        public void run()
        {

//            Mat bgra = new Mat(cameraInSize, CvType.CV_8UC4);

//            Tic.tacTicWithHighLow();
//            Tic.tic();

//            Imgproc.cvtColor(frameNV21, bgra, Imgproc.COLOR_YUV2RGBA_NV21, 4);
//            frameNV21.release();

            if(isClosed || mOnFrameAvailableListener == null)
                return;


//            Tic.tacWithAverage();
//            Tic.tac(747);
            String framePath = mOnFrameAvailableListener.shouldSaveDroppedFrame(mTime);
            if(framePath != null)
            {
//                Tic.tic(223);
                saveBuffer(frameBuffer, framePath);
//                ProcessImage.getInstance().saveBufferToFile(buffer, buffer.length, framePath);
//                Tic.tac(223);
            }

//            if (!isClosed && mOnFrameAvailableListener != null)
//                mOnFrameAvailableListener.onFrameDropped(bgra, mTime);
//            bgra.release();

            currentLoadedDroppedFrames--;

//            String framePath = mVideoRecorder.addFrame(mTime, false);
//            ProcessImage.getInstance().saveFrame(bgra.nativeObj, framePath);
//
//            bgra.release();
        }
    }

    private void processDroppedFrameSynced(byte[] buffer, long time)
    {

        if (!isClosed && mOnFrameAvailableListener != null)
        {
            String framePath = mOnFrameAvailableListener.shouldSaveDroppedFrame(time);
            if(framePath != null)
            {
                saveBuffer(buffer, framePath);
//                ProcessImage.getInstance().saveBufferToFile(buffer, buffer.length, framePath);
            }
        }


    }

    private byte[] loadBuffer(String framePath)
    {
        try
        {
            InputStream inputStream = new FileInputStream(framePath);
            byte[] dstBuffer = new byte[bufferSize];
            byte[] tmpBuffer = new byte[1024*50];//50k
            int len, index = 0;
            while((len = inputStream.read(tmpBuffer)) != -1)
            {
                System.arraycopy(tmpBuffer, 0, dstBuffer, index, len);
                index += len;
            }
            return dstBuffer;
        }
        catch (Throwable err)
        {
            return null;
        }

    }

    private void saveBuffer(byte[] buffer, String framePath)
    {
        File frameFile = new File(framePath);
        try
        {
            OutputStream outputStream = new FileOutputStream(frameFile);
            outputStream.write(buffer, 0, buffer.length);
            outputStream.flush();
            outputStream.close();
        }
        catch (Throwable err)
        {
            err.printStackTrace();
        }
    }


    public void turnOffImageAdjustments(OnImageAdjustmentsOff onImageAdjustmentsOff)
    {
        mOnImageAdjustmentsOff = onImageAdjustmentsOff;

        Camera.Parameters params = mCamera.getParameters();
        params.setAutoExposureLock(false);
        params.setAutoWhiteBalanceLock(false);
        mCamera.setParameters(params);

        new Thread()
        {
            public void run()
            {
                try{Thread.sleep(500);}catch (Throwable err){}
                Camera.Parameters params = mCamera.getParameters();
                params.setAutoExposureLock(true);
                params.setAutoWhiteBalanceLock(true);

                mCamera.setParameters(params);
                mOnImageAdjustmentsOff.onActionDone();

            }
        }.start();
    }

    public int getCameraRotation()
    {
        return cameraRotation;
    }

    public interface OnImageAdjustmentsOff
    {
        void onActionDone();
    }

    public void close(boolean dontBlockDontCare)
    {
        if (isClosed) return;
        isClosed = true;

//        try { Thread.sleep(250); } catch (Throwable err) { }

        if(dontBlockDontCare)
        {
            releaseCamera();

            firstFrameProcessorExecutor.shutdownNow();
            secondFrameProcessorExecutor.shutdownNow();
            droppedFramesExecutor.shutdownNow();
            if(currentLoadedFrames != 0)
            {
                new Thread()
                {
                    public void run()
                    {
                        int counter = 0;
                        while (currentLoadedFrames != 0 && counter < 10)
                        {
                            try { Thread.sleep(1000); } catch (Throwable err) { }
                            counter++;
                        }
                        try { currentBitmap.recycle();} catch (Throwable err) { }

                    }
                }.start();
            }
            else try { currentBitmap.recycle();} catch (Throwable err) { }
        }
        else
        {
            synchronized (this)
            {
                releaseCamera();

                firstFrameProcessorExecutor.shutdown();
                secondFrameProcessorExecutor.shutdown();
                droppedFramesExecutor.shutdown();
                try { firstFrameProcessorExecutor.awaitTermination(40000, java.util.concurrent.TimeUnit.MILLISECONDS); } catch (Throwable err) { }
                try { secondFrameProcessorExecutor.awaitTermination(40000, java.util.concurrent.TimeUnit.MILLISECONDS); } catch (Throwable err) { }
                try { droppedFramesExecutor.awaitTermination(40000, java.util.concurrent.TimeUnit.MILLISECONDS); } catch (Throwable err) { }

                try { currentBitmap.recycle();} catch (Throwable err) { }
            }
        }

    }

}