package fm.bling.blingy.discover.fragments;


import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.database.handler.FollowingDataHandler;
import fm.bling.blingy.inviteFriends.model.CAAApiSuggestedFriends;
import fm.bling.blingy.profile.ProfileActivity;
import fm.bling.blingy.profile.adapters.UserConnectionRecyclerAdapter;
import fm.bling.blingy.record.utils.SimpleDividerItemDecoration;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SearchPeopleFragment extends ScrollToTopFragment {

    /**
     * Main Views
     */
    private FrameLayout mEmptyFrame;
    private TextView mNoResult;
    private View mLine;
    private View.OnClickListener mItemClickListener;
    private View.OnClickListener mFollowClickListener;
    private Handler mHandler;
    private ImageView spinner;
    private ObjectAnimator anim;

    /**
     * Recycler
     */
    private RecyclerView mRecycleTrackList;
    private UserConnectionRecyclerAdapter mRecyclerAdapter;

    private boolean isVisible = false;
    private ArrayList<User> searchDataSet;


    public static SearchPeopleFragment newInstance() {
        SearchPeopleFragment fragment = new SearchPeopleFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        isVisible = true;
        if (mRecyclerAdapter != null && mRecycleTrackList != null) {
            mRecyclerAdapter.setmMultiImageLoader(((BaseActivity) getActivity()).getMultiImageLodaer());
            mRecyclerAdapter.notifyDataSetChanged();
        }
        if (mHandler == null)
            mHandler = new Handler();
    }

    @Override
    public void onStop() {
        super.onStop();
        isVisible = false;
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.search_layout, null);
        init(v);
        return v;
    }

    public void init(View mainView) {
        mEmptyFrame = (FrameLayout) mainView.findViewById(R.id.empty_view);
        spinner = (ImageView) mainView.findViewById(R.id.spinner);
        mNoResult = (TextView) mainView.findViewById(R.id.no_search_results);
        mLine = mainView.findViewById(R.id.line);

        mRecycleTrackList = (RecyclerView) mainView.findViewById(R.id.search_recycler);
        mRecycleTrackList.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        mRecycleTrackList.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext(), R.drawable.line_divider, true));

        loadListeners();
    }

    private void loadListeners() {
        mItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User friend = ((User) v.getTag());
                Intent i = new Intent(getActivity(), ProfileActivity.class);
                i.putExtra(Constants.USER_ID, friend.getUserId());
                getActivity().startActivity(i);
            }
        };

        mFollowClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User friend = ((User) v.getTag());
                int position = searchDataSet.indexOf(friend);
                UserConnectionRecyclerAdapter.DataObjectHolder holder = (UserConnectionRecyclerAdapter.DataObjectHolder) mRecycleTrackList.getChildViewHolder((View) (v.getParent()));
                if (!friend.getUserId().contentEquals(Constants.ME) && !friend.getUserId().contentEquals(CAAUserDataSingleton.getInstance().getUserId())) {
                    Drawable d = !iFollowYou(friend) ? getResources().getDrawable(R.drawable.ic_follow_green_24dp) : getResources().getDrawable(R.drawable.ic_followed_grey_24dp);
                    holder.addFriend.setImageDrawable(d);
                    int pos = searchDataSet.indexOf(friend);
                    if (iFollowYou(friend)) {
                        friend.setFollowedByMe("0");
                        unFollowUser(friend.getUserId(), position);

                        TrackingManager.getInstance().tapUnfollow(updatedFollowing(-1), friend.getUserId(), "search_people", pos + "");
                        updatedFollowing(-1);
                    } else {
                        friend.setFollowedByMe("1");
                        sendFollowUser(friend.getUserId(), position);
                        TrackingManager.getInstance().tapFollow(friend.getUserId(), "search_people", position, updatedFollowing(1));
                    }
                }
            }
        };
    }

    private void prepareData(ArrayList<User> data) {
        if (isVisible) {
            showResults();
            searchDataSet = data;
            mRecyclerAdapter = new UserConnectionRecyclerAdapter(getContext(), ((BaseActivity) getActivity()).getMultiImageLodaer(), data, mItemClickListener, mFollowClickListener);
            mRecycleTrackList.setAdapter(mRecyclerAdapter);
        }
    }

    public boolean onSearch(final String text) {
        if (text.length() >= 3 && getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mRecyclerAdapter != null)
                        mRecyclerAdapter.clearData();
                    showProgressBar();
                    TrackingManager.getInstance().searchDiscover("people", text);
                    App.getUrlService().getSearchUsers("user", 0, text, new Callback<CAAApiSuggestedFriends>() {
                        @Override
                        public void success(CAAApiSuggestedFriends caaApiSuggestedFriends, Response response) {
                            if (caaApiSuggestedFriends != null && caaApiSuggestedFriends.getData() != null && caaApiSuggestedFriends.getData().size() > 0)
                                prepareData(caaApiSuggestedFriends.getData());
                            else if (isVisible) {
                                showNoResult();
                                if (mRecyclerAdapter != null && mRecyclerAdapter.getItemCount() > 0)
                                    mRecyclerAdapter.clearData();
                            }

                        }

                        @Override
                        public void failure(RetrofitError error) {
                        }
                    });
                }
            });

            return true;
        } else return false;

    }

    private boolean iFollowYou(User friend) {
        if (friend.getFollowedByMe().equalsIgnoreCase("1")) {
            return true;
        }
        return false;
    }

    private void sendFollowUser(String userID, final int position) {

        FollowingDataHandler.getInstance().add(userID);
        App.getUrlService().putsUsersFollow(userID, "", new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                searchDataSet.get(position).setFollowedByMe("1");
                mRecyclerAdapter.notifyItemChanged(position);
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
        StateMachine.getInstance(getContext().getApplicationContext()).insertEvent(
                new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL,
                        StateMachineEvent.EVENT_TYPE.FOLLOW_USER.ordinal(),
                        Integer.parseInt(userID)
                )
        );
    }


    private void unFollowUser(String userID, final int position) {
        FollowingDataHandler.getInstance().remove(userID);
        App.getUrlService().deleteUsersFollow(userID, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                searchDataSet.get(position).setFollowedByMe("0");
                mRecyclerAdapter.notifyItemChanged(position);
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private void showResults() {
        mRecycleTrackList.setVisibility(View.VISIBLE);
        mEmptyFrame.setVisibility(View.GONE);
    }

    private void showNoResult() {
        mRecycleTrackList.setVisibility(View.GONE);
        mEmptyFrame.setVisibility(View.VISIBLE);
        spinner.setVisibility(View.GONE);
        mNoResult.setVisibility(View.VISIBLE);
        mLine.setVisibility(View.VISIBLE);
        canelAnimation();

    }

    private void showProgressBar() {
        loadAnimation();
        mRecycleTrackList.setVisibility(View.GONE);
        mEmptyFrame.setVisibility(View.VISIBLE);
        mNoResult.setVisibility(View.GONE);
        mLine.setVisibility(View.GONE);
    }

    public void canelAnimation() {
        anim.cancel();
    }

    private void loadAnimation() {
        spinner.setVisibility(View.VISIBLE);
        anim = ObjectAnimator.ofFloat(spinner, "rotation", 0f, 359f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setDuration(1300);
        anim.setRepeatCount(Animation.INFINITE);
        anim.start();

    }

    @Override
    public void scrollToTop() {
        try {
            if (mRecycleTrackList != null)
                mRecycleTrackList.smoothScrollToPosition(0);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
