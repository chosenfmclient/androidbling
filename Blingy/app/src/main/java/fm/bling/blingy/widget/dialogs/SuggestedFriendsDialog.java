package fm.bling.blingy.widget.dialogs;

import android.view.View;

import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.stateMachine.StateMachineMessageListener;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 7/12/16.
 * History:
 * ***********************************
 */
public abstract class SuggestedFriendsDialog extends InviteWidgetBaseDialog {

    protected int listSize = 0;
    protected int mCurrentUserNum = 0;

    SuggestedFriendsDialog(StateMachineMessageListener onReadyListener, BaseActivity context, StateMachineEvent.MESSAGE_TYPE messageType, int listSize) {
        super(onReadyListener, context, messageType);
        this.listSize = listSize;
        dialogLayout.findViewById(R.id.top_bar_container).setVisibility(View.GONE);
        setUpBackNext();
        checkBackSkipVisibility();
    }

    private void setUpBackNext()
    {
        mSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveNextUser();
            }
        });
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveBackUser();
            }
        });
    }

    public abstract void setUpUserCurrent();

    protected void moveBackUser() {
        if(mCurrentUserNum > 0) {
            mCurrentUserNum--;
            fadeOut.setDuration(200);
            fadeOut.setFillAfter(true);
            fadeIn.setDuration(200);
            fadeIn.setFillAfter(true);
            mMainText.startAnimation(fadeOut);
            mButtonFrame.startAnimation(fadeOut);
            setUpUserCurrent();
            mMainText.startAnimation(fadeIn);
            mButtonFrame.startAnimation(fadeIn);
//            TrackingManager.getInstance().tapBackWidget(listSize);
            checkBackSkipVisibility();
        }
    }

    protected void moveNextUser() {
        if(mCurrentUserNum < listSize - 1) {
            mCurrentUserNum++;
            fadeOut.setDuration(200);
            fadeOut.setFillAfter(true);
            fadeIn.setDuration(200);
            fadeIn.setFillAfter(true);
            mMainText.startAnimation(fadeOut);
            mButtonFrame.startAnimation(fadeOut);
            setUpUserCurrent();
            mMainText.startAnimation(fadeIn);
            mButtonFrame.startAnimation(fadeIn);
//            TrackingManager.getInstance().tapSkipWidget(listSize);
            checkBackSkipVisibility();
        }
    }

    private void checkBackSkipVisibility(){
        if(mCurrentUserNum == 0)
            mBack.setVisibility(View.GONE);
        else
            mBack.setVisibility(View.VISIBLE);

        if(mCurrentUserNum == (listSize - 1))
            mSkip.setVisibility(View.GONE);
        else
            mSkip.setVisibility(View.VISIBLE);

    }

    public void setListSize(int listSize) {
        this.listSize = listSize;
    }
}
