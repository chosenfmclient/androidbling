package fm.bling.blingy.duetHome.repository;


import fm.bling.blingy.App;
import fm.bling.blingy.duetHome.DuetHomeMVPR;
import fm.bling.blingy.duetHome.model.DuetHomeFeed;
import fm.bling.blingy.songHome.model.SongHomeFeed;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DuetHomeRepository implements DuetHomeMVPR.Repository{


    private DuetHomeMVPR.Model mModel;

    public DuetHomeRepository(DuetHomeMVPR.Model model){
        this.mModel = model;
    }

    @Override
    public void getDuetHome(int page) {
        App.getUrlService().getDuetHome(page, new Callback<DuetHomeFeed>() {
            @Override
            public void success(DuetHomeFeed data, Response response) {
                mModel.loadDuetHomeData(data);
            }

            @Override
            public void failure(RetrofitError error) {
                mModel.loadDataFailed();
            }
        });
    }
}
