package fm.bling.blingy.registration;

import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONObject;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAFacebookLogin
 * Description: Gets Facebook session info and sets CAAUserDataSingleton
 * Created by Zach Gerstman on 5/12/15.
 * History:
 * ***********************************
 */
public class CAAFacebookLogin {


    /**
     * Log in with facebook and update user data singleton
     */
    public static void FacebookLogin(CAALoginListener cAALogin) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if(accessToken != null) {
            if (!accessToken.isExpired()) {
                updateFacebookUserSingleton(accessToken);
                cAALogin.didFinishLogin(true, 1);
            }
        } else {
            //no longer logged into fb
            SharedPreferencesManager.getEditor().putString(Constants.ACCOUNT_TYPE, "anonymous").commit();
            CAALogin.Login(cAALogin);
        }
    }

    /**
     * Updates user data singleton for facebook sign up user
     * @param accessToken Facebook access token object
     */
    public static void updateFacebookUserSingleton(AccessToken accessToken) {
        SharedPreferencesManager.getEditor().putString(Constants.ACCOUNT_TYPE, "facebook").commit();
        CAAUserDataSingleton.getInstance().setAccessToken(accessToken.getToken());
        App.getUrlService().getSpecificUser(Constants.ME, new Callback<User>() {
            @Override
            public void success(User caaUser, Response response) {
                TrackingManager.getInstance().loginSubmit("facebook");
                CAAUserDataSingleton.getInstance().setUserId(caaUser.getUserId());
                CAAUserDataSingleton.getInstance().setFirstName(caaUser.getFirstName());
                CAAUserDataSingleton.getInstance().setLastName(caaUser.getLastName());
                CAAUserDataSingleton.getInstance().setPhotoUrl(caaUser.getPhoto());
            }

            @Override
            public void failure(RetrofitError error) {
                TrackingManager.getInstance().loginFailed(++CAALogin.failedLogins);
            }
        });

        CAAUserDataSingleton.getInstance().setFacebookId(accessToken.getUserId());
        CAAUserDataSingleton.getInstance().setAccountType("facebook");

        GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject me, GraphResponse response) {
                        if (response.getError() != null) {
                            // handle error
                        } else {
                            String email = me.optString("email");
                            CAAUserDataSingleton.getInstance().setEmail(email);
                            if (CAAUserDataSingleton.getInstance().getFirstName().isEmpty()) {
                                String firstName = me.optString("first_name");
                                CAAUserDataSingleton.getInstance().setFirstName(firstName);
                            }
                            if (CAAUserDataSingleton.getInstance().getLastName().isEmpty()) {
                                String lastName = me.optString("last_name");
                                CAAUserDataSingleton.getInstance().setLastName(lastName);
                            }
                        }
                    }
                }).executeAsync();
    }

}
