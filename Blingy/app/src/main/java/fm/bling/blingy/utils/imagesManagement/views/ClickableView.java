package fm.bling.blingy.utils.imagesManagement.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Ben Levi on 1/25/2016.
 */
public class ClickableView extends View
{
    public static int lightenColor = Color.argb(155, 255, 255, 255);

    private ColorFilter baseColorFilter = null;

    public ClickableView(Context context)
    {
        super(context);
    }

    public ClickableView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public ClickableView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }


    public boolean onTouchEvent(MotionEvent me)
    {
        if(!isClickable())return false;

        if(me.getAction() == MotionEvent.ACTION_DOWN)
        {
            select();
        }
        else if(me.getAction() == MotionEvent.ACTION_MOVE)
            ;
        else
        {
            if(me.getAction() == MotionEvent.ACTION_UP)
            {
                if( me.getX() > 0 && me.getX() < getWidth() && me.getY() > 0 && me.getY() < getHeight() )
                    performClick();
            }
            deSelect();
        }
        return true;
    }

    private void select()
    {
        Drawable drawable = getBackground();
        if(drawable != null)
        {
            drawable.setColorFilter(new PorterDuffColorFilter(lightenColor, PorterDuff.Mode.MULTIPLY));
        }
    }

    private void deSelect()
    {
        Drawable drawable = getBackground();
        if(drawable != null)
        {
            drawable.setColorFilter(baseColorFilter);
        }
    }

    public void close()
    {
        setBackgroundResource(0);
    }

    public void paintPixels(int color)
    {
        Drawable drawable = getBackground();
        drawable.mutate();
        if(drawable != null)
        {
            drawable.setColorFilter( baseColorFilter = new PorterDuffColorFilter(color,PorterDuff.Mode.MULTIPLY));
        }
    }

}