package fm.bling.blingy.inviteFriends.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/25/16.
 * History:
 * ***********************************
 */
public class CAASmsInvites {
    @SerializedName("phone")
    private ArrayList<CAASmsInvite> phones;

    public CAASmsInvites(ArrayList<CAASmsInvite> phones) {
        this.phones = phones;
    }
}