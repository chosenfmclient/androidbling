package fm.bling.blingy.registration;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import java.util.Arrays;
import java.util.List;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.TermsAndConditionsActivity;
import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.dialogs.EmailSignupDialog;
import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
import fm.bling.blingy.registration.dialogs.FacebookSignupDialog;
import fm.bling.blingy.registration.listeners.RegistrationButtonListener;
import fm.bling.blingy.registration.rest.model.CAACFacebookSignup;
import fm.bling.blingy.registration.rest.model.CAAGooglePlusSignup;
import fm.bling.blingy.registration.rest.model.CAAToken;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Chosen-pro on 6/25/15.
 */
public class SignupActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener, RegistrationButtonListener {

    private CallbackManager mCallbackManager;
    private Fragment newFragment;
    private Boolean dialog = false;
    private String TAG_LOADING = "TAG_LOADING";
    private Activity activity;

    FrameLayout loginButton;
    FrameLayout emailButton;
    FrameLayout mGoogleButton;
    TextView termsCondition;

    private LoginManager loginManager;
    List<String> permissionNeeds;

    GoogleApiClient mGoogleApiClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        setContentView(R.layout.login_layout);
        init();

        //Setting up the navigation icon.
        Toolbar toolbar = getActionBarToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
        toolbar.setNavigationContentDescription("backClose");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PROFILE))
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestScopes(new Scope(Scopes.PLUS_ME))
                .requestId()
                .requestProfile()
                .requestEmail()
                .requestIdToken(getResources().getString(R.string.server_client_id))
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Plus.API)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmailSignupDialog dialog = new EmailSignupDialog(SignupActivity.this, SignupActivity.this);
                dialog.show();
            }
        });

        mGoogleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, Constants.GOOGLE_SIGN_IN);
            }
        });

        mCallbackManager = CallbackManager.Factory.create();

        loginManager = LoginManager.getInstance();
        loginManager.registerCallback(mCallbackManager, mCallback);
        permissionNeeds = Arrays.asList("email");
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(NetworkConnectivityListener.isConnectedWithDialog(SignupActivity.this)){
                    loginManager.logInWithReadPermissions(activity, permissionNeeds);
                }
//                else{
//                    showNoConnectionDialog();
//                }
            }
        });
        termsCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SignupActivity.this,  TermsAndConditionsActivity.class);
                startActivity(i);
            }
        });

    }

    private void init() {
        loginButton = (FrameLayout) findViewById(R.id.facebook_button);
        emailButton = (FrameLayout) findViewById(R.id.signIn_button_connectEmail);
        mGoogleButton = (FrameLayout) findViewById(R.id.connect_via_google_plus);
        termsCondition = (TextView) findViewById(R.id.terms);
    }

    @Override
    public void goToRegistration()
    {
        Intent i = new Intent(SignupActivity.this, EmailSignupActivity.class);
        startActivityForResult(i, Constants.REQUEST_LOGIN);
    }

    @Override
    public void goToLogin()
    {
        Intent i = new Intent(SignupActivity.this, LoginActivity.class);
        startActivityForResult(i, Constants.REQUEST_LOGIN);
    }

    private FacebookCallback<LoginResult> mCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            dialog = true;
            FrameLayout frame = (FrameLayout)findViewById(R.id.fragFrame);
            frame.bringToFront();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            newFragment = FacebookSignupDialog.newInstance();
            ft.add(R.id.fragFrame, newFragment, TAG_LOADING);
            ft.commit();
            callFacebookSignupApi(loginResult.getAccessToken());
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

            //TODO show error to user
        }
    };

    private void callFacebookSignupApi(final AccessToken token) {
        CAACFacebookSignup facebookSignup = new CAACFacebookSignup(token.getToken());
        App.getUrlService().postFacebookSignup(facebookSignup, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                CAAFacebookLogin.updateFacebookUserSingleton(token);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        CAAUserDataSingleton.getInstance().setSentGcmToken(false);
                        Fragment fragment = getFragmentManager().findFragmentByTag(TAG_LOADING);
                        if (fragment != null) {
                            getFragmentManager().beginTransaction().remove(fragment).commit();
                        }
                        getFragmentManager().executePendingTransactions();
                        setResult(Activity.RESULT_OK);
                        finish();
                        Log.d("Facebook login", "success");
                    }
                }, 2000);

            }

            @Override
            public void failure(RetrofitError error) {
                if (activity != null) {
                    Fragment fragment = getFragmentManager().findFragmentByTag(TAG_LOADING);
                    if (fragment != null)
                        getFragmentManager().beginTransaction().remove(fragment).commit();
                    setResult(Activity.RESULT_OK);
                    dialog = false;
                    if (error.getResponse() != null && error.getResponse().getStatus() == 403) {
                        String errorMsg = getResources().getString(R.string.error_email_facebook_msg);
                        BaseDialog dialog = new BaseDialog(SignupActivity.this, "Login Error", errorMsg);
                            dialog.show();
                    }
                }
            }
        });
    }

    private void callGooglePlusSignupApi(final String token,final String googleProfile, final String Nickname, final String birthday, final String gender) {
        CAAGooglePlusSignup googlePlusSignup = new CAAGooglePlusSignup(token, googleProfile, Nickname, birthday, gender, Constants.APP_ID, Constants.APP_SECRET);
        App.getUrlService().postGooglePlusSignup(googlePlusSignup, new Callback<CAAToken>() {
            @Override
            public void success(CAAToken caaToken, Response response) {
                CAAGooglePlusLogin.updateGooglePlusUserSingleton(caaToken);
                setResult(Activity.RESULT_OK);
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                if (activity != null) {
                    Fragment fragment = getFragmentManager().findFragmentByTag(TAG_LOADING);
                    if (fragment != null)
                        getFragmentManager().beginTransaction().remove(fragment).commit();
                    setResult(Activity.RESULT_OK);
                    dialog = false;
                    if (error.getResponse() != null && error.getResponse().getStatus() == 403) {
                        String errorMsg = getResources().getString(R.string.error_email_facebook_msg);
//                        String yesText = getApplicationContext().getResources().getString(R.string.ok);
                        BaseDialog dialog = new BaseDialog(activity, "Login Error", errorMsg);
//                        if (dialog.shouldBeShown()) {
                            dialog.show();
//                        }
                    }
//                    else {
//                        CAANetworkErrors networkErrors = new CAANetworkErrors();
//                        ChosenAlertDialog dialog = networkErrors.getDialog(activity, error.getResponse());
//                        if (dialog.shouldBeShown()) {
//                            dialog.show();
//                        }
//                    }
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (!dialog)
            super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == Constants.REQUEST_LOGIN) {
            if (resultCode == Activity.RESULT_OK) {
                //  setResult(10);
                setResult(Activity.RESULT_OK);
                CAAUserDataSingleton.getInstance().setSentGcmToken(false);
                finish();
            }
        }
        if (requestCode == Constants.GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this,"Failed to connect with Google+.",Toast.LENGTH_SHORT).show();
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {

            dialog = true;
            FrameLayout frame = (FrameLayout)findViewById(R.id.fragFrame);
            frame.bringToFront();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            newFragment = FacebookSignupDialog.newInstance();
            ft.add(R.id.fragFrame, newFragment, TAG_LOADING);
            ft.commit();

            setResult(Activity.RESULT_OK);
            CAAUserDataSingleton.getInstance().setSentGcmToken(false);
            final GoogleSignInAccount acct = result.getSignInAccount();

            String personNickName = "";
            String personGooglePlusProfile = "";
            String birthday = "";
            int personGender = -1;
            String gender = "";

            if(mGoogleApiClient.hasConnectedApi(Plus.API)) {
                if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                    Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                    if(currentPerson.hasNickname())
                        personNickName = currentPerson.getNickname();
                    if(currentPerson.hasUrl())
                        personGooglePlusProfile = currentPerson.getUrl();
                    if(currentPerson.hasGender())
                        personGender = currentPerson.getGender();
                    if(currentPerson.hasBirthday())
                        birthday = currentPerson.getBirthday();
                }
                if(personGender != -1){
                    gender = personGender == 0 ? "male" : "female";
                }
            }
            String token = acct.getIdToken();
            SharedPreferencesManager.getEditor().putString(Constants.GOOGLE_TOKEN_ID, token).commit();
            callGooglePlusSignupApi(token, personGooglePlusProfile, personNickName, birthday, gender);
        }
        else{
            TrackingManager.getInstance().registerFaild(CAALogin.failedLogins++);
        }
    }
}
