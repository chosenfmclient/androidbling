package fm.bling.blingy.record.fragments;


import android.support.v4.app.Fragment;

/**
 * Created by Oren Zakay on 2/2/16.
 */
public class BaseAudioFragment extends Fragment {
    String tag;

    public String getFragmentTag() {
        return tag;
    }

    public void onSearch(String text) {
    }

    public void onQueryTextSubmit(String query) {
    }

    public void onSearchCollapse(boolean isCollapse) {
    }

    public void stopMediaPlayer() {
    }

}