package fm.bling.blingy.editVideo.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Chosen-pro on 2/9/16.
 */
public class CAAVideoTypeData {
    @SerializedName("label")
    private String label;

    @SerializedName("types")
    private ArrayList<String> types;

    public ArrayList<String> getTypes() {
        return types;
    }

//    public void setTypes(ArrayList<String> types) {
//        this.types = types;
//    }

    public String getLabel() {
        return label;
    }

//    public void setLabel(String label) {
//        this.label = label;
//    }
}
