package fm.bling.blingy.editVideo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.BuildConfig;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.dialogs.share.ShareDialogProccessCalback;
import fm.bling.blingy.dialogs.share.ShareDialogRecording;
import fm.bling.blingy.dialogs.share.workers.RenderForInstagram;
import fm.bling.blingy.dialogs.share.workers.listeners.onRenderInstagramFinished;
import fm.bling.blingy.editVideo.rest.model.CAAPostVideos;
import fm.bling.blingy.record.CameraRecorder3;
import fm.bling.blingy.record.CountingTypedFile;
import fm.bling.blingy.record.ProgressListener;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.record.callbacks.ActivityResultsReceiver;
import fm.bling.blingy.record.callbacks.PermissionResultReciever;
import fm.bling.blingy.editVideo.rest.model.CAAVideoPostResponse;
import fm.bling.blingy.record.utils.ImageViewExtension;
import fm.bling.blingy.record.utils.TextureVideo;
import fm.bling.blingy.editVideo.rest.RestClientUploadFile;
import fm.bling.blingy.rest.UrlPostService;
import fm.bling.blingy.rest.model.CAAStatus;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.FFMPEG;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.utils.imagesManagement.views.WhiteProgressBarView;
import fm.bling.blingy.videoHome.VideoHomeActivity;
import fm.bling.blingy.videoHome.model.CAAVideo;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class VideoPlayerActivity extends AppCompatActivity implements ShareDialogProccessCalback {

    private final String TAG = "VideoPlayerActivity";

//    private String mashupID;
    private String parentID;
    private String videoID;
    private String clipID;
    private String userID;
    private boolean isActivityVisible = false;
    private String logoPath;

    private String path;
    private String pathWithWaterMark;
    private Uri uri;
    private String type  = "";

    private String url = "VID_Blingy_out.mp4";
    private String songID = null;
    private String songName = null;
    private String artistName = null;
    private String videoStatus = null;
    private ArrayList<String> mVideoEffects = new ArrayList<>();

    private String songClipUrl;

    private String songImageUrl;
    private ArrayList<String> clip = new ArrayList<>();
    private String mShareTag;


    private String frameID;
    private String mFramePath;

    private ActivityResultsReceiver mActivityResultsReceiver;

    private PermissionResultReciever mPermissionResultsReciever;

    private Thread clipFinishedListener;

    private int width, height;
    private boolean didReachRecordEnd = false;
    private boolean moveToMashup = false;
    private boolean uploadComplete = false;
    private boolean isShareComleted = true;
    private boolean isUploading = false;
    private boolean noDialog = false;
    private boolean externalMode = false;
    private boolean isFirstTime = true;
    private boolean videoIsPortrait = false;
//    private boolean isPhoto;
    private boolean movingToExternalStorage = false;
    private boolean uploadMode = false;


//    private VideoViewExtension vidView;
    private TextureVideo vidView;
    private ImageViewExtension imageView;
    private LinearLayout mUploadProgressContainer;
    private WhiteProgressBarView mUploadingProgressBar;

    private FrameLayout mFragmentContainer;
    private Toolbar mToolbar;
    private LinearLayout keepItLayout;
    private Button mSavePrivateButton;
    private Button mPostButton;
    private ScaledImageView mBluredImage;

    private Handler mHandler;
    private ShareDialogRecording mShareDialog;
    private ImageLoaderManager mImageLoaderManager;
    private RenderForInstagram renderForInstagram;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TrackingManager.getInstance().recordReview();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.edit_video_layout);

//        if(getIntent().hasExtra(Constants.MASHUP_ID)) {
//            mashupID = getIntent().getStringExtra(Constants.MASHUP_ID);
//            //Toast.makeText(this,"Im mashup video",Toast.LENGTH_SHORT).show();
//        }
        //Toast.makeText(this,"Im regular video",Toast.LENGTH_SHORT).show();


        /**
         * Set up intent variables
         */
        processIntent();


        initalizeUi();

        mHandler = new Handler();
        songClipUrl = getIntent().getStringExtra(Constants.CLIP_URL);
        songImageUrl = getIntent().getStringExtra(Constants.CLIP_THUMBNAIL);

        mImageLoaderManager = new ImageLoaderManager(this);

        mBluredImage.setDontUseExisting(true);
        mBluredImage.setDontSaveToFileCache(true);
        mBluredImage.setBlurRadius(25);
        mBluredImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mBluredImage.setAnimResID(R.anim.fade_in_slow);
        mBluredImage.setImageLoader(mImageLoaderManager);
        mBluredImage.setUrl(Constants.thumbnail);

        /**
         * Get URI
         */

        path = App.VIDEO_WORKING_FOLDER + "blingy_vid.mp4";

        /**
         * Set up buttons
         */
        mSavePrivateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mashupID = "";// do not add to mashup
                mPostButton.setVisibility(View.GONE);
                mSavePrivateButton.setVisibility(View.GONE);
                mUploadProgressContainer.setVisibility(View.VISIBLE);
                postContentToServer("private");
            }
        });

        mPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postContentToServer("public");
                mPostButton.setVisibility(View.GONE);
                mSavePrivateButton.setVisibility(View.GONE);
                mUploadProgressContainer.setVisibility(View.VISIBLE);



            }
        });

        /**
         * Set variables based on type
         */
        prepareWithType(type);

        /**
         * Set up toolbar
         */
        mToolbar = getActionBarToolbar();
        if (mToolbar != null) setUpToolbar();

        prepareVideoPreview();

    }

    private void initalizeUi() {
        vidView = (TextureVideo) findViewById(R.id.myVideo);
        imageView = (ImageViewExtension) findViewById(R.id.myPhoto);
        mFragmentContainer = (FrameLayout) findViewById(R.id.fragment_container);
        mBluredImage = (ScaledImageView) findViewById(R.id.blured_image);
        keepItLayout = (LinearLayout) findViewById(R.id.keep_it_layout);
        mSavePrivateButton = (Button) findViewById(R.id.save_private);
        mUploadProgressContainer = (LinearLayout) findViewById(R.id.upload_progress);
        mUploadingProgressBar = (WhiteProgressBarView) findViewById(R.id.progress_bar);
        mUploadingProgressBar.updateLayout();

        vidView.getLayoutParams().height = App.landscapitemHeight;
        mPostButton = (Button) findViewById(R.id.post);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (mImageLoaderManager == null)
            mImageLoaderManager = new ImageLoaderManager(this);


//        restartVideo();
//        if (uploadComplete)
//            onUploadCompleted();
    }

    @Override
    protected void onResume() {
        super.onResume();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();

        if(isScreenOn)
        {
            isActivityVisible = true;
            userID = CAAUserDataSingleton.getInstance().getUserId();
            restartVideo();
            if (uploadComplete)
                onUploadCompleted();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        isActivityVisible = false;
        if (vidView != null)
            vidView.pause();
    }

    @Override
    public void onStop() {
        super.onStop();
        uploadMode = false;
        mHandler.removeCallbacksAndMessages(null);
        try {
            mImageLoaderManager.clearCache();
            mImageLoaderManager.clearImageViews();
        } catch (Throwable err) {
            err.printStackTrace();
        }

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (mImageLoaderManager != null)
            mImageLoaderManager.close();

        try
        {
            vidView.close();
        }
        catch (Throwable err)
        {
        }
        vidView = null;

        if (externalMode)
        {
            File f = new File(pathWithWaterMark);
            Uri contentUri = Uri.fromFile(f);
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
            getApplicationContext().sendBroadcast(mediaScanIntent);
        }
        if (clipFinishedListener != null && !clipFinishedListener.isInterrupted())
        {
            clipFinishedListener.interrupt();
            clipFinishedListener = null;
        }

        clip.clear();

//        if(imageView != null)
//        {
//            try
//            {
//                ((BitmapDrawable)imageView.getDrawable()).getBitmap().recycle();
//            }catch (Throwable err){}
//        }

        Constants.isBackCamera = false;
    }

    private void prepareWithType(String type)
    {
        /**
         * Upload  - Can still be danceoff as well
         */
        if (type.equalsIgnoreCase(Constants.UPLOAD) || getIntent().hasExtra("uri"))
        {

            try
            {
                if (getIntent().hasExtra("uri") && getIntent().getStringExtra("uri") != null)
                    uri = Uri.parse(getIntent().getStringExtra("uri"));
            }
            catch (Throwable err)
            {
            }

            if (getIntent().hasExtra("path"))
            {
                path = getIntent().getStringExtra("path");
            }
            else
            {
                path = Utils.getPathFromMediaUri(this, uri);
            }

            if (type.contentEquals(Constants.UPLOAD))
            {
                uploadMode = true;
                this.type = "";
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            if (!(new File(Constants.thumbnail).exists()))
                                new File(Constants.thumbnail);
                            FFMPEG.getInstance(getApplicationContext()).extractFirstFrameFromVideo(path, Constants.thumbnail);
                            runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    mImageLoaderManager.DisplayImage(mBluredImage);
                                }
                            });

                        }
                        catch (Throwable throwable)
                        {
                            throwable.printStackTrace();
                        }
                    }
                }.start();
                mSavePrivateButton.callOnClick();
                keepItLayout.setVisibility(View.VISIBLE);

            }
            else
            {
                artistName = getIntent().getStringExtra(Constants.ARTIST_NAME);
                mVideoEffects = getIntent().getStringArrayListExtra(EventConstants.EFFECTS);
            }

            if (getIntent().getStringExtra("song_id") != null)
            {
                songID = getIntent().getStringExtra("song_id");
            }

        }
        else
        {
            mImageLoaderManager.DisplayImage(mBluredImage);
            keepItLayout.setVisibility(View.VISIBLE);
        }
    }

//    public static void setCaaApiFramesAndWatermark(final Context lContext)
//    {
//        if (caaApiFramesAndWatermark != null)
//            return;
//
//        new Thread()
//        {
//            public void run()
//            {
//                App.getUrlService(lContext).getFramesAndWatermark(new Callback<CAAApiFramesAndWatermark>()
//                {
//                    @Override
//                    public void success(CAAApiFramesAndWatermark iCaaApiFramesAndWatermark, Response response)
//                    {
//                        caaApiFramesAndWatermark = iCaaApiFramesAndWatermark;
//                        new Thread()
//                        {
//                            public void run()
//                            {
//                                caaApiFramesAndWatermark.setFramesAndWaterMarkCashing(lContext);
//                            }
//                        }.start();
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error)
//                    {
//                        error.printStackTrace();
//                    }
//                });
//            }
//        }.start();
//    }

    private void processIntent() {
        if (getIntent().hasExtra(Constants.PORTRAIT)) {
            videoIsPortrait = getIntent().getBooleanExtra(Constants.PORTRAIT, true);
        }
        if(getIntent().hasExtra(Constants.PARENT_ID)){
            parentID = getIntent().getStringExtra(Constants.PARENT_ID);
        }
        if(getIntent().hasExtra(Constants.CLIP_ID)){
            clipID = getIntent().getStringExtra(Constants.CLIP_ID);
        }

        if(getIntent().hasExtra(Constants.TYPE)){
            type = getIntent().getStringExtra(Constants.TYPE);
        }

        if (getIntent().hasExtra("song_name")) {
            songName = getIntent().getStringExtra("song_name");
        }
        if (getIntent().hasExtra("artist_name")) {
            artistName = getIntent().getStringExtra("artist_name");
        }
//        isPhoto = getIntent().getBooleanExtra(Constants.PHOTO_MODE, false);

        if (getIntent().hasExtra("tags")) {
            mShareTag = getIntent().getStringExtra(Constants.SHARE_TAG);
        }
    }

    private void setUpToolbar() {
        mToolbar.setBackgroundResource(R.color.transparent);
        mToolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        mToolbar.setNavigationContentDescription("backClose");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        setToolbarFont();
    }

    public void setToolbarFont() {
        for (int i = 0; i < mToolbar.getChildCount(); i++) {
            if (mToolbar.getChildAt(i) instanceof TextView) {
                ((TextView) mToolbar.getChildAt(i)).setTypeface(App.ROBOTO_REGULAR);
            }
        }
    }

//    private void preparePhotoPreview()
//    {
//        Bitmap bitmap = null;
//        vidView.stopPlayback();
//        vidView.setVisibility(View.GONE);
//        imageView.setVisibility(View.VISIBLE);
//
//        try
//        {
//            bitmap = BitmapFactory.decodeFile(path);
//            imageView.setImageBitmap(bitmap);
//            height = bitmap.getHeight();
//            width = bitmap.getWidth();
////            new Thread(){ public void run(){ setThumbnail(null); }}.start();
//        }
//        catch (Throwable err)
//        {
//            try{bitmap.recycle();}catch (Throwable errIn){}
//            System.gc();
//            Runtime.getRuntime().gc();
//            preparePhotoPreview();
//        }
//    }

    private void prepareVideoPreview() {
        try {
            MediaMetadataRetriever meta = new MediaMetadataRetriever();
            meta.setDataSource(path);
            if (!videoIsPortrait) {
                try {
                    width = Integer.parseInt(meta.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
                } catch (Throwable err) {
                }
                try {
                    height = Integer.parseInt(meta.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
                } catch (Throwable err) {
                }
            } else {
                try {
                    width = Integer.parseInt(meta.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
                } catch (Throwable err) {
                }
                try {
                    height = Integer.parseInt(meta.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
                } catch (Throwable err) {
                }
            }
            meta.release();
            if(width <= 0)
                width = CameraRecorder3.WIDTH;

            imageView.setImageBitmap(null);
            imageView.setVisibility(View.GONE);
            vidView.setVisibility(View.VISIBLE);
            if (uri == null) {
                vidView.setDataSource(path);
            } else {
                vidView.setDataSource(this, uri);
            }
        } catch (Throwable throwablet) {
            throwablet.printStackTrace();
        }

        vidView.setListener(new TextureVideo.MediaPlayerListener() {
            @Override
            public void onVideoPrepared() {
                if (isFirstTime) {
                    isFirstTime = false;
                    setClip(0);
                }
            }

            @Override
            public void onVideoEnd() {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            vidView.play();
                        } catch (Throwable throwable) {
                        }
                    }
                }, 300);
            }
        });
//        vidView.setMediaController(null);
//        vidView.setOnCompletionListener(
//                new MediaPlayer.OnCompletionListener() {
//                    @Override
//                    public void onCompletion(MediaPlayer mp) {
//                        mHandler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                try {
//                                    vidView.start();
//                                } catch (Throwable throwable) {
//                                }
//                                ;
//                            }
//                        }, 300);
//                    }
//                }
//        );

//        vidView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                vidView.start();
//
//                if (isFirstTime) {
//                    isFirstTime = false;
//                    setClip(0);
//                }
//            }
//        });
    }

    @Override
    public void onBackPressed() {
        String yesText = "YES";
        String title = "Are you sure?";
        String text = isUploading ? "Your upload is still in progress. Are you sure you want to close?": "Are you sure you want to quit? Your video won't be published";;
        BaseDialog mDialog = new BaseDialog(this, title, text);
        mDialog.setOKText(yesText);
        mDialog.show();
        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (((BaseDialog) dialog).getAnswer()) {
                    VideoPlayerActivity.super.onBackPressed();
                }
            }
        });
        TrackingManager.getInstance().recordCancel(isUploading ? "uploading" : "review");
    }

    private void getExternalStoragePermission() {
        String[] permissions = new String[]{"", ""};
        if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(VideoPlayerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            permissions[0] = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        }

        if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(VideoPlayerActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            permissions[1] = Manifest.permission.READ_EXTERNAL_STORAGE;
        }

        if ((!permissions[0].isEmpty() || !permissions[1].isEmpty())) {
            ActivityCompat.requestPermissions(VideoPlayerActivity.this, permissions, Constants.OPTIONAL_PERMISSION);
        } else {
            moveVideoToExternal();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == Constants.OPTIONAL_PERMISSION) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED &&
                        (permissions[i].equalsIgnoreCase(Manifest.permission.WRITE_EXTERNAL_STORAGE) || permissions[i].equalsIgnoreCase(Manifest.permission.READ_EXTERNAL_STORAGE))) {
                    SharedPreferences settings = getApplicationContext().getSharedPreferences(getApplicationContext().getResources().getString(R.string.user_shared_prefs), Context.MODE_MULTI_PROCESS);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putInt(Constants.SAVE_SETTING, Constants.SAVE_INTERNAL);
                    editor.apply();
                    return;
                }
            }
            moveVideoToExternal();
        }
    }


    public synchronized void moveVideoToExternal() {
        if (movingToExternalStorage) return;

        movingToExternalStorage = true;
        String timeStamp = (Long.valueOf((System.currentTimeMillis() / 1000))).toString();

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "Blingy");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("FILE mkdir", "failed to create directory");
                return;
            }
        }

        try {
            generateWaterMark(parentID != null);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        if (isPhoto) {
//            pathWithWaterMark = Environment.getExternalStorageDirectory() + "/Blingy/" + "blingy_" + timeStamp + ".jpg";
////            if (mShareFragment != null) mShareFragment.setFilePath(pathWithWaterMark);

//        } else {
            pathWithWaterMark = Environment.getExternalStorageDirectory() + "/Blingy/" + "blingy_" + timeStamp + ".mp4";
//            if (mShareFragment != null) mShareFragment.setFilePath(pathWithWaterMark);
//        }

//        if (caaApiFramesAndWatermark != null && caaApiFramesAndWatermark.watermarkPath != null && (new File(caaApiFramesAndWatermark.watermarkPath).exists()))
//        {
        SaveWithWatermark saveWithWatermark = new SaveWithWatermark(pathWithWaterMark, videoIsPortrait);
        saveWithWatermark.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        externalMode = true;
//        }

    }

    public void renderVideoForInstagrm() {
        try {
            generateWaterMark(parentID != null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        onRenderInstagramFinished onRenderFinished = new onRenderInstagramFinished() {
            @Override
            public void onRenderInstagramFinished(boolean result) {
                if (result) {
                    Constants.isFFmpegIsDoneProcessing = true;
                    movingToExternalStorage = false;
                    mShareDialog.onInstagramClicked();
                }
            }
        };
        String extra = "blingy_vid_instagram_" + String.format("%040x", new BigInteger(1, videoID.getBytes())) + ".mp4";
        renderForInstagram = new RenderForInstagram(path, Environment.getExternalStorageDirectory() + File.separator + extra, logoPath, parentID != null, this, onRenderFinished);
        renderForInstagram.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void setClip(int clipStart) {
        clip.clear();
        clip.add(String.valueOf(clipStart));
        clip.add(String.valueOf(clipStart + 15));
    }

    public void postContentToServer(final String status) {
        videoStatus = status;
        TrackingManager.getInstance().recordSubmit();
        isUploading = true;
//        StateMachineEvent.EVENT_TYPE eventType = isPhoto ? StateMachineEvent.EVENT_TYPE.RECORD_PHOTO : StateMachineEvent.EVENT_TYPE.RECORD_VIDEO;
        StateMachineEvent.EVENT_TYPE eventType = StateMachineEvent.EVENT_TYPE.RECORD_VIDEO;
        if (uploadMode) {
            eventType = StateMachineEvent.EVENT_TYPE.UPLOAD;
        }
        StateMachine.getInstance(getApplicationContext()).insertEvent(
                new StateMachineEvent(
                        StateMachineEvent.EVENT_CLASS.RECORDING,
                        eventType.ordinal()));

        CAAPostVideos postOject = null;
        if(parentID != null) { // its a cameo video

            postOject = new CAAPostVideos(type, parentID, Constants.thumbnailName, url, songID);
        }
        else {
            postOject = new CAAPostVideos(type.equalsIgnoreCase("") ? null : type, parentID, Constants.thumbnailName, url, songID, songName, clip, artistName, frameID, null, songClipUrl, songImageUrl);
        }

        mFragmentContainer.setVisibility(View.INVISIBLE);

        if (uploadMode)
            postOject.setStatus(videoStatus = "secret");

        App.getUrlService().postUploadVideo(postOject, new Callback<CAAVideoPostResponse>() {
            @Override
            public void success(CAAVideoPostResponse cAAVideoPostResponse, Response response) {
                videoID = cAAVideoPostResponse.getVideoId();

                if (!uploadMode && isActivityVisible) {
                    openShareDialog();
                }

                songName = cAAVideoPostResponse.getSongName();

                if (!VideoPlayerActivity.this.isFinishing()) {

                    mFragmentContainer.setVisibility(View.VISIBLE);
                    didReachRecordEnd = true;

                    uploadContent(cAAVideoPostResponse, path, status);
                    uploadThumbnail(cAAVideoPostResponse);
                    SharedPreferences sharedPreferences = getSharedPreferences(getResources().getString(R.string.user_shared_prefs), Context.MODE_MULTI_PROCESS);
                    if (!sharedPreferences.getBoolean(Constants.RATE_AFTER_UPLOAD_VIDEO, false)) {
                        sharedPreferences.edit().putBoolean(Constants.RATE_AFTER_UPLOAD_VIDEO, true).apply();
                    }

                    if (!uploadMode) {
                        int saveExternal = SharedPreferencesManager.getInstance().getInt(Constants.SAVE_SETTING, Constants.NO_EXTERNAL_SETTING);
                        switch (saveExternal) {
                            case Constants.NO_EXTERNAL_SETTING:
                                BaseDialog dialog = new BaseDialog(VideoPlayerActivity.this, "Save Video", getString(R.string.record_save_video_dialog));
//                                if (isPhoto)
//                                    dialog = new BaseDialog(VideoPlayerActivity.this, "Save Photo", getString(R.string.record_save_photo_dialog));
//                                else
//                                        dialog = new BaseDialog(VideoPlayerActivity.this, "Save Video", getString(R.string.record_save_video_dialog));

                                dialog.setOKText("YES");
                                dialog.setCancelText("NO");

                                dialog.show();
                                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        if (((BaseDialog) dialogInterface).getAnswer()) {
                                            SharedPreferencesManager.getEditor().putInt(Constants.SAVE_SETTING, Constants.SAVE_EXTERNAL).apply();
                                            getExternalStoragePermission();
                                        } else {
                                            SharedPreferencesManager.getEditor().putInt(Constants.SAVE_SETTING, Constants.SAVE_INTERNAL).apply();
                                        }
                                    }
                                });
                                break;
                            case Constants.SAVE_EXTERNAL:
                                getExternalStoragePermission();
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });

        mFragmentContainer.setVisibility(View.INVISIBLE);

    }

//    private void addToMashup(PostMashup postMashup){
//        App.getUrlService().postMashup(postMashup, new Callback<MashupOBJ>() {
//            @Override
//            public void success(MashupOBJ mashupOBJ, Response response) {
//              moveToMashup = true;
//              onUploadCompleted();
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                moveToMashup = false;
//                onUploadCompleted();
//            }
//        });
//    }

    public void uploadContent(CAAVideoPostResponse cAAVideoPostResponse, String path, String status) {
        SendFileTask fileTask = new SendFileTask(path, cAAVideoPostResponse, status);
        String response = "";
        fileTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, response);
    }

    public void uploadThumbnail(CAAVideoPostResponse cAAVideoPostResponse) {
        RestClientUploadFile one = new RestClientUploadFile("http:");
        String signedurlThumbnail = "/storage.googleapis.com" + cAAVideoPostResponse.getThumbnail().getSignedUrl().getPath() + "?" + cAAVideoPostResponse.getThumbnail().getSignedUrl().getQuery();

        UrlPostService two = one.getUrlPostService();
        TypedFile tfile = new TypedFile(Constants.thumbnailName, new File(Constants.thumbnail));
        two.putFileServer(cAAVideoPostResponse.getThumbnail().getContentType(), signedurlThumbnail, tfile, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    public void VideofinishUploading(String status) {
        isUploading = false;
        CAAStatus stat = new CAAStatus(status);

        App.getUrlService().putUploadVideoStatus(videoID, stat, new Callback<CAAVideo>() {
            @Override
            public void success(CAAVideo cAAVideoPostResponse, Response response) {
                uploadComplete = true;
                Constants.uploadVideoProgressBar = 101;
//                if(mashupID!=null && !mashupID.isEmpty())
//                    addToMashup(new PostMashup(videoID, mashupID));
//                else
                    onUploadCompleted();
            }

            @Override
            public void failure(RetrofitError error) {
                uploadComplete = true;
                Constants.uploadVideoProgressBar = 101;
                onUploadCompleted();
            }
        });
    }

    private void onUploadCompleted() {

        //Write in Shared preferences to save the number of videos i uploaded
        SharedPreferencesManager.getEditor().putInt(Constants.VIDEOS_CREATED,(SharedPreferencesManager.getInstance().getInt(Constants.VIDEOS_CREATED,0)) + 1).commit();

        if (isActivityVisible) {

            if(parentID != null)
                TrackingManager.getInstance().tapCameoCompleted(videoID, clipID, artistName, songName, videoStatus);

            TrackingManager.getInstance().recordCompleted(type, videoID, artistName, songName, mVideoEffects, SharedPreferencesManager.getInstance().getInt(EventConstants.VIDEOS_CREATED, 0));
            mUploadProgressContainer.setVisibility(View.GONE);
            if (uploadMode) {
                BaseDialog thanksDialog = new BaseDialog(this, getString(R.string.thanks), getString(R.string.thanks_info));
                thanksDialog.removeCancelbutton();
                thanksDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        moveToVideoHome();
                    }
                });
                thanksDialog.show();
            } else {
                moveToVideoHome();
            }
        }
    }

    private void moveToVideoHome() {
        if(BuildConfig.DEBUG)
            Log.d(TAG,"moveToVideoHome>>>>" + isShareComleted);
        if(isShareComleted) {
            if (renderForInstagram != null)
                renderForInstagram.dismissLoading();

            try{
                mShareDialog.dismiss();
                mShareDialog = null;
            } catch (Throwable throwable){
                Log.d(TAG, throwable.toString());
            }

//            if (moveToMashup) {
//                Intent mashUpIntent = new Intent(VideoPlayerActivity.this,MashupHomeActivity.class);
//                mashUpIntent.putExtra(Constants.MASHUP_ID,mashupID);
//                startActivity(mashUpIntent);
//
//
//            } else {
//
//                Intent intent = new Intent(VideoPlayerActivity.this, VideoHomeActivity.class);
//
//                intent.putExtra(Constants.FROM, Constants.VIDEO_PLAYER_ACTIVITY);
//                intent.putExtra(Constants.VIDEO_ID, videoID);
//                intent.putExtra(Constants.CLIP_NAME, songName);
//                intent.putExtra(Constants.ARTIST_NAME, artistName);
//                intent.putExtra(Constants.STATUS, videoStatus);
//                overridePendingTransition(R.anim.fade_out_slow, R.anim.fade_in_slow);
//                startActivity(intent);
//                VideoPlayerActivity.this.finish();
//            }
            Intent intent = new Intent(VideoPlayerActivity.this, VideoHomeActivity.class);

            intent.putExtra(Constants.FROM, Constants.VIDEO_PLAYER_ACTIVITY);

            if(uploadMode) intent.putExtra(Constants.TYPE, Constants.UPLOAD);
            else intent.putExtra(Constants.TYPE, type);

            intent.putExtra(Constants.VIDEO_ID, videoID);
            intent.putExtra(Constants.CLIP_NAME, songName);
            intent.putExtra(Constants.ARTIST_NAME, artistName);
            intent.putExtra(Constants.STATUS, videoStatus);
            overridePendingTransition(R.anim.fade_out_slow, R.anim.fade_in_slow);
            startActivity(intent);
            try { vidView.close();} catch (Throwable err) { }
            vidView = null;
            VideoPlayerActivity.this.finish();
        }
    }

    private void restartVideo() {
        if (vidView != null) {
            if (!vidView.isPlaying()) {
                vidView.play();
                vidView.setVisibility(View.VISIBLE);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mActivityResultsReceiver != null) {
            mActivityResultsReceiver.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void generateWaterMark(boolean isDuet) throws IOException {


        Bitmap bm = BitmapFactory.decodeResource(getResources(), isDuet ? R.raw.watermark_duet : R.raw.watermark1);

        if (bm == null) {
            return;
        }

        if(width <= 0)
            width = CameraRecorder3.WIDTH;

        float scaleFactor = ((float) bm.getWidth()) / ((float) bm.getHeight());
        float scaledWidth = 0.25f * ((float) width);


        logoPath = isDuet ? App.MAIN_FOLDER_PATH + "/duet_watermark_" + scaledWidth : App.MAIN_FOLDER_PATH + "/watermark_" + scaledWidth;

        File file = new File(logoPath);
        if (file.exists())
            return;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        int bHeight = (int) (scaledWidth / scaleFactor);

        Bitmap scaled = Bitmap.createScaledBitmap(bm, (int) scaledWidth, bHeight, true);

        if (bm != scaled)
            bm.recycle();

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            scaled.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        scaled.recycle();
    }


    private class SendFileTask extends AsyncTask<String, Integer, String> {

        private String filePath;
        private String status = "public";
        private long totalSize;
        CAAVideoPostResponse cAAVideoPostResponse;

        public SendFileTask(String filePath, CAAVideoPostResponse cAAVideoPostResponse, String status) {
            this.filePath = filePath;
            this.status = uploadMode ? "secret" : status;
            this.cAAVideoPostResponse = cAAVideoPostResponse;
        }


        @Override
        protected String doInBackground(String... params) {
            try {
                Constants.uploadVideoProgressBar = 0;
//                UploadingVideo = 0;
                File movieFile = new File(path);

                totalSize = movieFile.length();

                ProgressListener listener = new ProgressListener() {
                    @Override
                    public void transferred(long num) {
                        publishProgress((int) ((num / (float) totalSize) * 100));
                    }
                };

                RestClientUploadFile one = new RestClientUploadFile("http:");
                String signedurl = "/storage.googleapis.com" + cAAVideoPostResponse.getUrl().getSignedUrl().getPath() + "?" + cAAVideoPostResponse.getUrl().getSignedUrl().getQuery();

                UrlPostService two = one.getUrlPostService();
                TypedFile tfile = new CountingTypedFile("VID_Chosen.mp4", movieFile, listener);
                return two.uploadFile(cAAVideoPostResponse.getUrl().getContentType(), signedurl, tfile);
            } catch (Throwable err) {
                err.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            Constants.uploadVideoProgressBar = values[0];
//            UploadingVideo = uploadVideoProgressBar;

        }


        @Override
        protected void onPostExecute(String s) {
            VideofinishUploading(status);
        }
    }

    private class SaveWithWatermark extends AsyncTask<String, Integer, String> {
        private String pathWithWaterMark;
//        private Bitmap frameBitmap;

        public SaveWithWatermark(String pathWithWaterMark, boolean videoIsPortrait) {
            this.pathWithWaterMark = pathWithWaterMark;
//            frameBitmap = vidView.getForgroundBitmap();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            handleShareCliked();
            movingToExternalStorage = false;
        }

        @Override
        protected String doInBackground(String... params) {
//            if (isPhoto) {
//                savePhoto();
//            } else {
                saveVideo();
//            }

            if (mShareDialog != null) {
                mShareDialog.setFilePath(pathWithWaterMark);
                Constants.isFFmpegIsDoneProcessing = true;
            }
            return "Finish to watermarking";
        }

//        private void savePhoto() {
//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inMutable = true;
//            Bitmap main = BitmapFactory.decodeFile(path, options);
//            Canvas canvas = new Canvas(main);
////            if (frameBitmap != null) {
////                canvas.drawBitmap(frameBitmap, 0, 0, null);
////            } else {
//                Bitmap logo = BitmapFactory.decodeFile(logoPath);
//                canvas.drawBitmap(logo, main.getWidth() - logo.getWidth(), main.getHeight() - logo.getHeight(), null);
//                logo.recycle();
////            }
//
//            try {
//                FileOutputStream fos = new FileOutputStream(pathWithWaterMark);
//                main.compress(Bitmap.CompressFormat.JPEG, 100, fos);
//                fos.flush();
//                fos.close();
//            } catch (Throwable xz) {
//                xz.printStackTrace();
//            }
//            main.recycle();
//        }

        private void saveVideo() {
            PowerManager powerManager = (PowerManager) VideoPlayerActivity.this.getSystemService(Activity.POWER_SERVICE);
            PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "VK_LOCK");
            wakeLock.acquire();

            final int maxWidth = 320;
            final int maxBitrate = -1; // was 15496k, counts in k- kilo bytes -> -1 for default
//            if (frameBitmap == null) {
                try {
//                    FFMPEG.getInstance(VideoPlayerActivity.this).renderWithImage(path, logoPath, pathWithWaterMark, "main_w-overlay_w", "main_h-overlay_h", maxBitrate);
                    FFMPEG.getInstance(VideoPlayerActivity.this).renderWithImage(path, logoPath, pathWithWaterMark, "0", "main_h-overlay_h", maxBitrate);
                } catch (Throwable err) {
                    err.printStackTrace();
                }
//            } else {
//                try {
//                    FFMPEG.getInstance(VideoPlayerActivity.this).renderWithImage(path, mFramePath, pathWithWaterMark, 0 + "", 0 + "", -1);
//                } catch (Throwable err) {
//                    err.printStackTrace();
//                }
//            }
            if (wakeLock.isHeld())
                wakeLock.release();
        }
    }

    public Toolbar getActionBarToolbar() {
        if (mToolbar == null) {
            mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
            if (mToolbar != null) {
                setSupportActionBar(mToolbar);
            }
        }
        return mToolbar;
    }

    public void setActivityResultsReceiver(ActivityResultsReceiver activityResultsReceiver) {
        mActivityResultsReceiver = activityResultsReceiver;
    }

    public void setPermissionResultReiever(PermissionResultReciever permissionResultReciver) {
        mPermissionResultsReciever = permissionResultReciver;
    }

    @Override
    public void onStartShareProcess() {
        isShareComleted = false;
    }

    @Override
    public void onEndShareProcess() {
        isShareComleted = true;
        if (uploadComplete)
            moveToVideoHome();
    }

    public void handleShareCliked() {
        if (mShareDialog != null && mShareDialog.isFacebookClicked()) {
            mShareDialog.callSendFacebook();
            Constants.isFFmpegIsDoneProcessing = true;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    private void openShareDialog() {
        if(BuildConfig.DEBUG)
            Log.d(TAG,"openShareDialog");
        mShareDialog = new ShareDialogRecording(this, this, videoID, userID,Constants.VIDEO_TYPE, songName, parentID != null, path);
    }
}