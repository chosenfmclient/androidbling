package fm.bling.blingy.mashup.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 1/9/17.
 * History:
 * ***********************************
 */
public class InviteeContact {

    @SerializedName("name")
    private String name;

    @SerializedName("phone")
    private String phone;

    @SerializedName("email")
    private String email;

    @SerializedName("user_id")
    private String user_id;

    private String photoUrl;
    private String videoID;
    private boolean isSelected = false;

    public InviteeContact(String name, String phone, String email, String photoUrl, String videoID) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.photoUrl = photoUrl;
        this.videoID = videoID;
    }

    public InviteeContact(String name, String user_id, String photoUrl, String videoID) {
        this.name = name;
        this.user_id = user_id;
        this.photoUrl = photoUrl;
        this.videoID = videoID;
    }

//    /**
//     * I made this params to be able to add a section above the invite mashup recycler
//     */
//    private int type;
//    private String text;
//
//    public InviteeContact(int type, String text) {
//        this.type = type;
//        this.text = text;
//    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getUser_id() {
        return user_id;
    }

//    public int getType() {
//        return type;
//    }
//
//    public String getText() {
//        return text;
//    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public boolean equals(Object obj) {
        InviteeContact contact = (InviteeContact) obj;

//        if(this.name == null && contact.name == null)
//            if(!this.text.equals(contact.text))
//                return false;
//
//        if(this.type != contact.type)
//            return false;

        if (this.user_id != null) {
            if (contact.user_id == null) return false;
            else if (!this.user_id.equals(contact.user_id)) return false;
        } else {
            if (contact.user_id != null) return false;
        }

        if (this.name != null) {
            if (contact.name == null) return false;
            else if (!this.name.equals(contact.name)) return false;
        } else {
            if (contact.name != null) return false;
        }

        if (this.phone != null) {
            if (contact.phone == null) return false;
            else if (!this.phone.equals(contact.phone)) return false;
        } else {
            if (contact.phone != null) return false;
        }

        if (this.email != null) {
            if (contact.email == null) return false;
            else if (!this.email.equals(contact.email)) return false;
        } else {
            if (contact.email != null) return false;
        }

        if (this.photoUrl != null) {
            if (contact.photoUrl == null) return false;
            else if (!this.photoUrl.equals(contact.photoUrl)) return false;
        } else {
            if (contact.photoUrl != null) return false;
        }

        return true;
    }

    public String toSqlValues(){
        StringBuilder builder  = new StringBuilder(videoID != null ? "\'" + videoID + "\'" : null)
                .append(",").append(" ").append(user_id != null ? "\'" + user_id+ "\'" : null)
                .append(",").append(" ").append(name != null ? "\'" + name.replace("'","\'\'") + "\'" : null)
                .append(",").append(" ").append(email != null ? "\'" + email.replace("'","\'\'") + "\'" : null)
                .append(",").append(" ").append(photoUrl != null ? "\'" + photoUrl + "\'" : null)
                .append(",").append(" ").append(phone != null ? "\'" + phone + "\'" : null);

        return builder.toString();
    }
}
