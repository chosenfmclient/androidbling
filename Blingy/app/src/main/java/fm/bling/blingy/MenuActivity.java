package fm.bling.blingy;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.inviteFriends.InviteFriendsActivity;
import fm.bling.blingy.profile.EditProfileActivity;
import fm.bling.blingy.stateMachine.PermissionListener;
import fm.bling.blingy.upload.PickPhotoVideoActivity;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 11/1/16.
 * History:
 * ***********************************
 */
public class MenuActivity extends BaseActivity implements PermissionListener {

    /**
     * Main Views
     */
    private Toolbar mToolbar;
    private LinearLayout mSettings;
    private LinearLayout mEditProfile;
//    private LinearLayout mShareProfile;
    private LinearLayout mFindFriends;
    private LinearLayout mUploadVideo;
    private LinearLayout mPrizes;
    private LinearLayout mSupport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_activity);
        init();
    }

    private void init(){
        mToolbar = getActionBarToolbar();
        if(mToolbar != null) {
            mToolbar.setNavigationIcon(R.drawable.ic_nav_back_24dp);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        mSettings = (LinearLayout)findViewById(R.id.settings);
        mEditProfile = (LinearLayout)findViewById(R.id.edit_profile);
//        mShareProfile = (LinearLayout)findViewById(R.id.share_profile);
        mFindFriends = (LinearLayout)findViewById(R.id.find_friends);
        mUploadVideo = (LinearLayout)findViewById(R.id.upload_video);
        mPrizes = (LinearLayout)findViewById(R.id.prizes);
        mSupport = (LinearLayout)findViewById(R.id.support);

        loadListeners();
    }

    private void loadListeners() {
        mSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuActivity.this, SettingsActivity.class));
            }
        });
        mEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuActivity.this, EditProfileActivity.class));
            }
        });
//        mShareProfile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new ShareDialogProfile(MenuActivity.this,MenuActivity.this, CAAUserDataSingleton.getInstance().getUserId());
//            }
//        });
        mFindFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getContactsPermissions(MenuActivity.this))
                    startActivity(new Intent(MenuActivity.this, InviteFriendsActivity.class));
            }
        });
        mUploadVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseDialog infoDialog = new BaseDialog(MenuActivity.this, getString(R.string.submit_viedo),getString(R.string.submit_video_info));
                infoDialog.show();
                infoDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if(((BaseDialog)dialog).getAnswer()){
                            MenuActivity.this.checkUploadPermissions(new Intent(MenuActivity.this, PickPhotoVideoActivity.class));
                        }
                    }
                });
            }
        });
        mPrizes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuActivity.this, PrizesActivity.class));
            }
        });
        mSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuActivity.this, SupportActivity.class));
            }
        });

    }

    @Override
    public int getSelfID() {
        return NO_BAR;
    }

    @Override
    public void onPermissionGranted() {
        mFindFriends.callOnClick();
    }

    @Override
    public void onPermissionDenied() {
    }
}
