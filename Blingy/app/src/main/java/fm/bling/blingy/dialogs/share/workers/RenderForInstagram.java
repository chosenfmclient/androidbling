package fm.bling.blingy.dialogs.share.workers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.SystemClock;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.ProcessImage;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.share.workers.listeners.onRenderInstagramFinished;
import fm.bling.blingy.utils.FFMPEG;
import fm.bling.blingy.utils.Tic;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 5/22/16.
 * History:
 * ***********************************
 */
public class RenderForInstagram extends AsyncTask<Void,Void,Boolean> {

    private Context mContext;
    private boolean isPhoto = false;
    private String mOriginalPath;
    private String mFileName;
//    private String mFramePath;
    private String mLogoPath;
//    private ArrayList<String> clipTime;
    private onRenderInstagramFinished onRenderInstagramFinished;
    private final int mRatioSize;
    private boolean isDuet = false;

    private ProgressDialog mProgressdialog;

    public RenderForInstagram(String originalPath, String pathToSave, String logoPath, boolean isDuet, Context context , onRenderInstagramFinished listener){
        this.mContext = context;
        this.mOriginalPath = originalPath;
        this.mFileName = pathToSave;
        this.mLogoPath = logoPath;
        this.isDuet = isDuet;
        mRatioSize = FFMPEG.getInstance(context).isLiteVideo() ? 240 : 360;

//        this.isPhoto = isPhoto;
//        this.mFramePath = framePath;
//        this.clipTime = clip;
        this.onRenderInstagramFinished = listener;
        this.mProgressdialog = new ProgressDialog(mContext);
        this.mProgressdialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                RenderForInstagram.this.cancel(true);
            }
        });
    }

    @Override
    protected void onPreExecute() {
        this.mProgressdialog = ProgressDialog.show(this.mContext, null, "Please wait ...", true);
        this.mProgressdialog.setCancelable(false);
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            if(!isPhoto) {

//                String tempPath1 = App.MAIN_FOLDER_PATH + File.separator + "temp_first_instagram_vid_"+ SystemClock.uptimeMillis() +".mp4";
//                File tempFile1 = new File(tempPath1);

                String tempPath2 = App.MAIN_FOLDER_PATH + File.separator + "temp_sec_instagram_vid_"+ SystemClock.uptimeMillis() +".mp4";
                File tempFile2 = new File(tempPath2);
//                if(clipTime.size() > 0 && !clipTime.get(0).equalsIgnoreCase("0")) {
//                    //Video more than 15sec
//                    FFMPEG.getInstance(mContext).getVideoFromTime(mOriginalPath, clipTime.get(0), "15", tempPath1);
//                    FFMPEG.getInstance(mContext).cropVideoCenterSquare(tempPath1, tempPath2, mRatioSize);
//                }
//                else{
//                    //Video less than 15sec
                FFMPEG.getInstance(mContext).cropVideoCenterSquare(mOriginalPath, tempPath2, mRatioSize);
//                }

//                if(mFramePath != null) {
//                    Bitmap frame = BitmapFactory.decodeFile(mFramePath);
//                    Bitmap scaledFrame = Bitmap.createScaledBitmap(frame, mRatioSize, mRatioSize, true);
//                    if(frame != scaledFrame)
//                        frame.recycle();
//                    FFMPEG.getInstance(mContext).renderWithImageBitmap(tempPath2, scaledFrame, mFileName, "0", "0", -1);
//                }
//                else{
//                    BitmapFactory.Options options = new BitmapFactory.Options();
//                    options.inScaled = false;
//                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap tmpLogo = BitmapFactory.decodeFile(mLogoPath);

                    float logoScale = (((float) tmpLogo.getWidth() / ((float) tmpLogo.getHeight())));

                    int width  = ((int)(mRatioSize * 0.25));

                    int height = ((int)(width / logoScale));

                    tmpLogo = Bitmap.createScaledBitmap(tmpLogo, width, height, true);

                    FFMPEG.getInstance(mContext).renderWithImageBitmap(tempPath2, tmpLogo, mFileName, "0", "main_h-overlay_h", -1);

//                }
//                tempFile1.deleteOnExit();
                tempFile2.deleteOnExit();
            }
//            else{
//                savePhoto();
//            }
        } catch (Throwable throwable) {
            if(this.mProgressdialog != null && mProgressdialog.isShowing())
                this.mProgressdialog.dismiss();
            this.onRenderInstagramFinished.onRenderInstagramFinished(false);
            throwable.printStackTrace();
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        try {
            if (this.mProgressdialog != null)
                this.mProgressdialog.dismiss();
        }catch (Throwable throwable){}
        this.onRenderInstagramFinished.onRenderInstagramFinished(result);
    }

    public void dismissLoading(){
        if(this.mProgressdialog != null) {
            this.mProgressdialog.dismiss();
            this.mProgressdialog = null;
        }
    }

//    private void savePhoto() {
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inMutable = true;
//        Bitmap tmpPic = BitmapFactory.decodeFile(mOriginalPath);
//        Bitmap main;
//        if(tmpPic.getWidth() < tmpPic.getHeight()) {
//            float scale = (((float) tmpPic.getWidth() / ((float) tmpPic.getHeight())));
//
//            //Scale image
//            Bitmap tmpScale = Bitmap.createScaledBitmap(tmpPic, mRatioSize, ((int) (mRatioSize / scale)), true);
//            if (tmpScale != tmpPic)
//                tmpPic.recycle();
//            //Crop image
//            main = Bitmap.createBitmap(tmpScale, 0, ((int) ((((mRatioSize / scale) - mRatioSize) / 2))), mRatioSize, mRatioSize);
//            if (tmpScale != main)
//                tmpScale.recycle();
//        }
//        else {
//            float scale = (((float) tmpPic.getHeight() / ((float) tmpPic.getWidth())));
//
//            //Scale image
//            Bitmap tmpScale = Bitmap.createScaledBitmap(tmpPic, ((int) (mRatioSize / scale)), mRatioSize, true);
//            if (tmpScale != tmpPic)
//                tmpPic.recycle();
//            //Crop image
//            main = Bitmap.createBitmap(tmpScale, ((int) ((((mRatioSize / scale) - mRatioSize) / 2))), 0, mRatioSize, mRatioSize);
//            if (tmpScale != main)
//                tmpScale.recycle();
//        }
//        Canvas canvas = new Canvas(main);
//
//        if(mFramePath != null) {
//            Bitmap frame = BitmapFactory.decodeFile(mFramePath);
//            Bitmap scaledFrame = Bitmap.createScaledBitmap(frame,mRatioSize,mRatioSize, true);
//            if(frame != scaledFrame)
//                frame.recycle();
//
//            canvas.drawBitmap(scaledFrame, 0, 0, null);
//            scaledFrame.recycle();
//        }
//        else {
//            Bitmap tmpLogo = BitmapFactory.decodeFile(mLogoPath);
//
//            float logoScale = (((float) tmpLogo.getWidth() / ((float) tmpLogo.getHeight())));
//            int width  = ((int)(mRatioSize * 0.32));
//            int height = ((int)(width / logoScale));
//
//            Bitmap logo = Bitmap.createScaledBitmap(tmpLogo,width,height,true);
//            if(tmpLogo != logo)
//                tmpLogo.recycle();
//            canvas.drawBitmap(logo, main.getWidth() - logo.getWidth(), main.getHeight() - logo.getHeight(), null);
//            logo.recycle();
//        }
//        try {
//            FileOutputStream fos = new FileOutputStream(mFileName);
//            main.compress(Bitmap.CompressFormat.JPEG, 100, fos);
//            fos.flush();
//            fos.close();
//        }
//        catch(Throwable xz){xz.printStackTrace();}
//        main.recycle();
//    }
}
