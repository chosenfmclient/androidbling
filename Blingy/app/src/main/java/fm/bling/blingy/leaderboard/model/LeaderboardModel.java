package fm.bling.blingy.leaderboard.model;

import android.support.annotation.Nullable;

import java.util.ArrayList;

import fm.bling.blingy.leaderboard.LeaderboardMVPR;
import fm.bling.blingy.leaderboard.LeaderboardRepository;
import fm.bling.blingy.rest.model.User;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 02/03/17.
 * History:
 * ***********************************
 */
public class LeaderboardModel implements LeaderboardMVPR.Model {

    private LeaderboardRepository mRepository;
    private LeaderboardMVPR.Presenter mPresenter;

    public LeaderboardModel(LeaderboardMVPR.Presenter presenter){
        this.mPresenter = presenter;
        this.mRepository = new LeaderboardRepository(this);
    }

    @Override
    public void getLeaderBoard(int page) {
        mRepository.getLeaderBoard(page);
    }

    @Override
    public void loadLeaderboardData(ArrayList<User> users) {
        mPresenter.loadLeaderboardData(users);
    }

    @Override
    public void loadNextPage(@Nullable ArrayList<User> users) {
        mPresenter.loadNextPage(users);
    }
}
