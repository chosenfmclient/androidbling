package fm.bling.blingy.utils.imagesManagement;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * Created by Ben Levi on 1/29/2016.
 */
public class MemoryCache2
{
    private final int limit;
    private internalLRUCache cache;
    public final int pictureQuality;
    public final float resoulotionScaleFactor;

    static int inMemCounter = 0;

    public MemoryCache2()
    {
        limit = (int) (Runtime.getRuntime().maxMemory() / 2L); //use 50% of available heap size
        cache = new internalLRUCache(limit);

        if( limit >= 50331648 ) //mid device memory allocation, if it's higher than this don't compress images
        {
            pictureQuality = 85;
            resoulotionScaleFactor = 1;
        }
        else
        {                                                      //   100663296
            int picQuality         = (int) (85f * (((float)limit) / 50331648f));  // 50331648 is the mem that mid devices provide to the app
                                                                                  // we are decreasing the quality according to the available memory
            if(picQuality < 50)
                pictureQuality  = 50;
            else pictureQuality = picQuality;

            float resScaleFactor   = ((float)limit) / 134217728f;
            if(resScaleFactor < 0.5f)
                resoulotionScaleFactor = 0.5f;
            else resoulotionScaleFactor = resScaleFactor;
        }
    }

    public Bitmap get(String id)
    {
        try
        {
            return cache.get(id);
        }
        catch (NullPointerException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public void put(String id, Bitmap bitmap)
    {
        inMemCounter++;
        try
        {
            cache.put(id, bitmap);
        }
        catch (Throwable th)
        {
            th.printStackTrace();
        }
    }

    public void delete(String id)
    {
        cache.remove(id);
    }

    public void clear()
    {
        try
        {
            cache.evictAll();
        }
        catch (NullPointerException ex)
        {
            ex.printStackTrace();
        }
    }

    private class internalLRUCache extends LruCache<String, Bitmap>
    {

        /**
         * @param maxSize for caches that do not override {@link #sizeOf}, this is
         *                the maximum number of entries in the cache. For all other caches,
         *                this is the maximum sum of the sizes of the entries in this cache.
         */
        public internalLRUCache(int maxSize)
        {
            super(maxSize);
        }

        @Override
        protected void entryRemoved(boolean evicted, String key, Bitmap oldValue, Bitmap newValue)
        {
            try{oldValue.recycle();inMemCounter--;}catch (Throwable e){}
        }

        @Override
        public int sizeOf(String key, Bitmap bitmap)
        {
            return getSizeInBytes(bitmap);

        }

        private int getSizeInBytes(Bitmap bitmap)
        {
            if (bitmap == null)
                return 0;
            return bitmap.getRowBytes() * bitmap.getHeight();
        }

    }

}