package fm.bling.blingy.homeScreen.model;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;


/**
 * Created by nadav on 5/6/15.
 */

public class CAANotification {


    @SerializedName("notification_id")
    private String notificationId;

    @SerializedName("type")
    private String type;

    @SerializedName("status")
    private String status;

    @SerializedName("date")
    private String date;

    @SerializedName("title")
    private String title;

    @SerializedName("text")
    private String text;

    @SerializedName( "object")
    private JsonElement object;

    @SerializedName("image")
    private String image;

    @SerializedName("image_type")
    private String imageType;

    private int adapterType;

    private CharSequence adapterText;


    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public JsonElement getObject() {

        return object;
    }

    public void setObject(JsonElement object) {

        this.object = object;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public int getAdapterType()
    {
        return adapterType;
    }

    public void setAdapterType(int adapterType)
    {
        this.adapterType = adapterType;
    }

    public CharSequence getAdapterText()
    {
        return adapterText;
    }

    public void setAdapterText(CharSequence adapterText)
    {
        this.adapterText = adapterText;
    }
}
