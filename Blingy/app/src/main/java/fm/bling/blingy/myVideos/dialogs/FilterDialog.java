package fm.bling.blingy.myVideos.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import fm.bling.blingy.R;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 8/1/16.
 * History:
 * ***********************************
 */
public class FilterDialog extends Dialog {

    private FilterSelectedListener mFilterSelectedListener;

    private final int ALL     = 0;
    private final int PUBLIC  = 1;
    private final int PRIVATE = 2;
    private RadioGroup mFilterGroup;

    public FilterDialog(Context context, FilterSelectedListener filterSelectedListener, int checked) {
        super(context, android.R.style.Theme_Panel);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.dimAmount = 0.5f;
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);
        setCanceledOnTouchOutside(true);
        this.mFilterSelectedListener = filterSelectedListener;
        setContentView(R.layout.filter_layout);
        mFilterGroup = (RadioGroup) findViewById(R.id.filter_group);
        mFilterGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.filter_all:
                        mFilterSelectedListener.onFilterSelceted(ALL);
                        dismiss();
                        break;
                    case R.id.filter_public:
                        mFilterSelectedListener.onFilterSelceted(PUBLIC);
                        dismiss();
                        break;
                    case R.id.filter_private:
                        mFilterSelectedListener.onFilterSelceted(PRIVATE);
                        dismiss();
                        break;
                }
            }
        });

        checkedFilter(checked);
        show();
    }

    private void checkedFilter(int checked) {
        switch (checked) {
            case ALL:
                ((RadioButton)findViewById(R.id.filter_all)).setChecked(true);
                break;
            case PUBLIC:
                ((RadioButton)findViewById(R.id.filter_public)).setChecked(true);
                break;
            case PRIVATE:
                ((RadioButton)findViewById(R.id.filter_private)).setChecked(true);
                break;
        }
    }


    public interface FilterSelectedListener{
        void onFilterSelceted(int selected);
    }
}
