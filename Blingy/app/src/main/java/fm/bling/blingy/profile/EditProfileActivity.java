package fm.bling.blingy.profile;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.BaseDialog;
import fm.bling.blingy.editVideo.rest.RestClientUploadFile;
import fm.bling.blingy.profile.dialogs.EditPhotoDialog;
import fm.bling.blingy.profile.model.CAAPhoto;
import fm.bling.blingy.profile.model.CAAPhotoEditProfile;
import fm.bling.blingy.profile.model.CAAPutUser;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.rest.UrlPostService;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.FixImageOrientation;
import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 10/31/16.
 * History:
 * ***********************************
 */
public class EditProfileActivity extends BaseActivity implements EditPhotoDialog.OnDialogFinished{

    /**
     * Main Views
     */
    private ImageView mCloseButton;
    private ScaledImageView mUserPic;
    private TextView mAddPhoto;
    private TextView mSaveButton;
    private EditText mFirstName;
    private EditText mLastName;
    private EditText mFacebookLink;
    private EditText mTwitterLink;
    private EditText mInstagramLink;
    private EditText mYoutubeLink;

    private String mFacebookLinkS;
    private String mTwitterLinkS;
    private String mInstagramLinkS;
    private String mYoutubeLinkS;

    private final int RESULT_LOAD_IMG = 1;
    private final int TAKE_PICTURE = 2;
    private final int CAMERA_PERMISSION = 8;
    private Intent resultIntent;
    private String[] cameraPermissions;

    private Uri outputFileUri;
    private File file;
    private File photofile;
    private String mCurrentPhotoPath;
    private boolean photoUpdated = false;
    private boolean photoUpdateComplete = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);

        TrackingManager.getInstance().pageView(EventConstants.EDIT_PROFILE_PAGE);
        if(savedInstanceState != null){
            mCurrentPhotoPath = savedInstanceState.getString(Constants.PHOTO_PATH, "");
            outputFileUri = Uri.parse(savedInstanceState.getString(Constants.URI_PATH, ""));
        }

        resultIntent = getIntent();
        mFacebookLinkS = CAAUserDataSingleton.getInstance().getFacebookLink();
        mTwitterLinkS = CAAUserDataSingleton.getInstance().getTwitterLink();
        mInstagramLinkS = CAAUserDataSingleton.getInstance().getInstagramLink();
        mYoutubeLinkS = CAAUserDataSingleton.getInstance().getYoutubeLink();

        init();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if(mCurrentPhotoPath != null)
            outState.putString(Constants.PHOTO_PATH, mCurrentPhotoPath);
        if(outputFileUri != null)
            outState.putString(Constants.URI_PATH, outputFileUri.getPath());

        super.onSaveInstanceState(outState);
    }

    private void init(){
        mUserPic = (ScaledImageView)findViewById(R.id.user_pic);
        mCloseButton = (ImageView) findViewById(R.id.close_button);
        mAddPhoto = (TextView) findViewById(R.id.add_photo);
        mSaveButton = (TextView) findViewById(R.id.save_button);
        mFirstName = (EditText) findViewById(R.id.first_name);
        mLastName = (EditText)findViewById(R.id.last_name);
        mFacebookLink = (EditText)findViewById(R.id.facebook_link);
        mTwitterLink = (EditText)findViewById(R.id.twitter_link);
        mInstagramLink = (EditText)findViewById(R.id.instagram_link);
        mYoutubeLink = (EditText)findViewById(R.id.youtube_link);

        mUserPic.setIsRoundedImage(true);
        mUserPic.setDontUseExisting(true);
        mUserPic.setKeepAspectRatioAccordingToWidth(true);
        mUserPic.setImageLoader(getMultiImageLodaer());
        mUserPic.setAnimResID(R.anim.fade_in);
        mUserPic.setPlaceHolder(R.drawable.extra_large_userpic_placeholder);
        mUserPic.setUrl(CAAUserDataSingleton.getInstance().getPhotoUrl());
        mMultiImageLoader.DisplayImage(mUserPic);

        mFirstName.setText(CAAUserDataSingleton.getInstance().getFirstName());
        mLastName.setText(CAAUserDataSingleton.getInstance().getLastName());
        mFacebookLink.setText(mFacebookLinkS);
        mTwitterLink.setText(mTwitterLinkS);
        mInstagramLink.setText(mInstagramLinkS);
        mYoutubeLink.setText(mYoutubeLinkS);

        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrackingManager.getInstance().editProfileCancel();
                onBackPressed();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasFirstName()) {
                    if (didChanges()) {
                        if(mYoutubeLink.getText().toString().isEmpty())
                            put();
                        else
                            if(checkYoutubeLink())
                                put();
                    } else {
                        if(!mYoutubeLink.getText().toString().isEmpty()){
                            if(checkYoutubeLink())
                                finish();
                        }
                        else
                            finish();
                    }
                }
            }
        });

        mAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrackingManager.getInstance().editPic();
                EditPhotoDialog d = new EditPhotoDialog();
                d.show(getFragmentManager(), "photo");
            }
        });
    }

    private boolean checkYoutubeLink() {
        try {
            URL url = new URL(mYoutubeLink.getText().toString());
            if(url.getHost().equalsIgnoreCase("www.youtube.com") || url.getHost().equalsIgnoreCase("m.youtube.com") || url.getHost().equalsIgnoreCase("youtube.com"))
                if(url.getPath().startsWith("/user/") || url.getPath().startsWith("/channel/"))
                    return true;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            showYoutubeError();
            return false;
        }
        showYoutubeError();
        return false;
    }

    private void showYoutubeError() {
        BaseDialog youtubeError = new BaseDialog(this,"Invalid link",
                "Your YouTube link isn't valid. Please make sure it has the format \n\"https://www.youtube.com/user\" or \n\"https://www.youtube.com/channel\".");
        youtubeError.removeCancelbutton();
        youtubeError.show();
        youtubeError.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mYoutubeLink.requestFocus();
            }
        });
    }

    private boolean hasFirstName(){
        if(mFirstName.getText().toString().trim().equals("")){
            Toast.makeText(this,"Please fill in a first name.",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean didChanges() {
        JSONObject editedPatams = new JSONObject();
        try {
        if(mFacebookLink != null && !mFacebookLink.getText().toString().equals(mFacebookLinkS))
                editedPatams.put("facebook_link", mFacebookLink.getText().toString());
        if(mTwitterLink != null && !mTwitterLink.getText().toString().equals(mTwitterLinkS))
            editedPatams.put("twitter_link", mTwitterLink.getText().toString());
        if(mInstagramLink != null && !mInstagramLink.getText().toString().equals(mInstagramLinkS))
            editedPatams.put("instagram_link", mInstagramLink.getText().toString());
        if(mYoutubeLink != null && !mYoutubeLink.getText().toString().equals(mYoutubeLinkS))
            editedPatams.put("youtube_link", mYoutubeLink.getText().toString());
        if(mFirstName != null && !mFirstName.getText().toString().equals(CAAUserDataSingleton.getInstance().getFirstName()))
            editedPatams.put("first_name", mFirstName.getText().toString());
        if(mLastName != null && !mLastName.getText().toString().equals(CAAUserDataSingleton.getInstance().getLastName()))
            editedPatams.put("last_name", mLastName.getText().toString());

            if(photoUpdated)
                editedPatams.put("profile_pic_updated", photoUpdated);
        } catch (JSONException e) {
//            e.printStackTrace();
        }
        TrackingManager.getInstance().editProfileSubmit(editedPatams);
        return editedPatams.length() > 0;
    }

    private void put() {
        final CAAPutUser user = new CAAPutUser(mFirstName.getText().toString(),
                                         mLastName.getText().toString(),
                                         mTwitterLink.getText().toString(),
                                         mYoutubeLink.getText().toString(),
                                         mInstagramLink.getText().toString(),
                                         mFacebookLink.getText().toString());


        App.getUrlService().putsUsersPhoto(" ",new Callback<CAAPhoto>() {
            @Override
            public void success(CAAPhoto photo, Response response) {
                CAAUserDataSingleton.getInstance().setPhotoUrl(photo.getPhoto());
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });

        App.getUrlService().putEditProfile(user, new Callback<User>() {
            @Override
            public void success(User caaUser, Response response) {
                resultIntent = new Intent();
                CAAUserDataSingleton.getInstance().setFacebookLink(mFacebookLink.getText().toString());
                CAAUserDataSingleton.getInstance().setTwitterLink(mTwitterLink.getText().toString());
                CAAUserDataSingleton.getInstance().setInstagramLink(mInstagramLink.getText().toString());
                CAAUserDataSingleton.getInstance().setYoutubeLink(mYoutubeLink.getText().toString());

                if (photofile != null && photoUpdateComplete) {
                    resultIntent.putExtra(Constants.PHOTO_PATH, photofile.getAbsolutePath());
                }
                CAAUserDataSingleton.getInstance().setFirstName(mFirstName.getText().toString());
                CAAUserDataSingleton.getInstance().setLastName(mLastName.getText().toString());

                setResult(RESULT_OK, resultIntent);
                finish();
//                showSuccessAlertDialog();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
            // Get the Image from data
            Uri selectedImage = data.getData();
//            String imagePath;
            if(selectedImage.getScheme().equalsIgnoreCase("file"))
                mCurrentPhotoPath = selectedImage.getPath();
            else
                mCurrentPhotoPath = Utils.getPathFromMediaUri(this, selectedImage);

            FixImageOrientation.fix(this, mCurrentPhotoPath, selectedImage ,true, 640);

            postProfilePhoto(mCurrentPhotoPath);
        }
        if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK) {
            FixImageOrientation.fix(this, mCurrentPhotoPath, outputFileUri ,true, 640);
            postProfilePhoto(mCurrentPhotoPath);
            galleryAddPic();
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        if(file == null)
            file = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    public void postProfilePhoto(final String path) {
        TrackingManager.getInstance().editPicSubmit();
        final CAAPhoto photo = new CAAPhoto("Profile_" + SystemClock.elapsedRealtime() + ".png");
        photoUpdated = true;

//        CAAUserDataSingleton.getInstance().setPhotoUrl(path);
        mUserPic.setUrl(path);

        startMultiImageLoader();
        mMultiImageLoader.DisplayImage(mUserPic);

        photoUpdateComplete = true;
        App.getUrlService().postUserPhoto(photo, new Callback<CAAPhotoEditProfile>() {
            @Override
            public void success(CAAPhotoEditProfile method, Response response) {
                try {
                    File dcim = new File(path);
                    photofile = dcim;
                    RestClientUploadFile one = new RestClientUploadFile("http:");
                    String signedurlThumbnail = "/storage.googleapis.com" + method.getPhoto().getSignedUrl().getPath() + "?" + method.getPhoto().getSignedUrl().getQuery();
                    UrlPostService two = one.getUrlPostService();
                    TypedFile tfile = new TypedFile(photo.getPhoto(), dcim);
                    two.putFileServer(method.getPhoto().getContentType(), signedurlThumbnail, tfile, new Callback<String>() {
                        @Override
                        public void success(String s, Response response) {
                        }

                        @Override
                        public void failure(RetrofitError error) {
                        }
                    });
                }catch (Throwable throwable){
                    Log.d("EditProfile", throwable.toString());
                }
            }

            @Override
            public void failure(RetrofitError error) { }
        });
    }

    private void openGallery() {
        try {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }catch (ActivityNotFoundException exc){
            Toast.makeText(this, "Please install any gallery app from Google Play.", Toast.LENGTH_SHORT).show();
        }
    }

    private void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = null;
        try {
            file = createImageFile();
        } catch (IOException e) {
            Log.d("EditProfile", "Edit I/O exception");
        }
//        mCurrentPhotoPath = file.getAbsolutePath();
        if (file != null) {
            outputFileUri = Uri.fromFile(file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(intent, TAKE_PICTURE);
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String imageFileName = "profile_photo";
        File storageDir = Environment.getExternalStorageDirectory();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void cameraPermissionDialog() {
        BaseDialog dialog = new BaseDialog(this,"Permissions",getString(R.string.edit_photo_permission_dialog));
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (((BaseDialog)dialogInterface).getAnswer()) {
                    ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            CAMERA_PERMISSION);
                }
            }
        });
    }


    public void galleryPermissionDialog() {
        BaseDialog dialog = new BaseDialog(this, "Permissions", getString(R.string.upload_permission_dialog));
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (((BaseDialog)dialogInterface).getAnswer()) {
                    ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            Constants.CRITICAL_PERMISSION);
                }
            }
        });
    }

    @Override
    public void onSelectImageFromGallery() {
        if (PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                galleryPermissionDialog();
            } else {
                ActivityCompat.requestPermissions(EditProfileActivity.this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                        Constants.CRITICAL_PERMISSION);
            }
        } else {
            openGallery();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CAMERA_PERMISSION) {
            cameraPermissions = new String[]{"", "", ""};
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED &&
                        (permissions[i].equalsIgnoreCase(Manifest.permission.READ_EXTERNAL_STORAGE) || permissions[i].equalsIgnoreCase(Manifest.permission.WRITE_EXTERNAL_STORAGE) || permissions[i].equalsIgnoreCase(Manifest.permission.CAMERA))) {
                    this.cameraPermissions[i] = permissions[i];
                }
            }

            if (!this.cameraPermissions[0].isEmpty() || !this.cameraPermissions[1].isEmpty()|| !this.cameraPermissions[2].isEmpty()) {
                cameraPermissionDialog();
            } else {
                takePhoto();
            }
        } else if (requestCode == Constants.CRITICAL_PERMISSION) {
            if(permissions[0].equalsIgnoreCase(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openGallery();
                } else {
                    galleryPermissionDialog();
                }
            }
        }
    }

    @Override
    public void onTakeImageWithCamera() {
        cameraPermissions = new String[]{"", "", ""};
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED)
            cameraPermissions[0] = Manifest.permission.CAMERA;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
            cameraPermissions[1] = Manifest.permission.READ_EXTERNAL_STORAGE;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
            cameraPermissions[2] = Manifest.permission.WRITE_EXTERNAL_STORAGE;

        if ((cameraPermissions[0].isEmpty() && cameraPermissions[1].isEmpty() && cameraPermissions[2].isEmpty())) {
            takePhoto();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                cameraPermissionDialog();
            } else {
                ActivityCompat.requestPermissions(this, cameraPermissions, CAMERA_PERMISSION);
            }
        }
    }

}
