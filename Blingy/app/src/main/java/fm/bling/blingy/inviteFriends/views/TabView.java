package fm.bling.blingy.inviteFriends.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.TextView;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.inviteFriends.listeners.TabListener;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/14/16.
 * History:
 * ***********************************
 */
public class TabView extends TextView
{
    private boolean selected;
    private TabListener mTabListener;
    private Paint mPaint;
    private final String mTabName;
    private int mTabIndex;
    private int strokWidth;

    public TabView(Context context, int tabWidth, int tabHeight, String tabName, int tabIndex, Paint paint, TabListener tabListener)
    {
        super(context);
        this.mTabListener = tabListener;
        this.mTabName = tabName;
        this.mTabIndex = tabIndex;
        this.mPaint = paint;
        setLayoutParams(new ViewGroup.LayoutParams(tabWidth, tabHeight));
        setGravity(Gravity.CENTER);
        setText(tabName);
        setTypeface(App.ROBOTO_REGULAR);
        setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(R.dimen.font_description));
        setTextColor(Color.WHITE);
        setIncludeFontPadding(false);
        setAllCaps(true);

        strokWidth = (int) (mPaint.getStrokeWidth() / 2f);
    }

    public String getTabName()
    {
        return mTabName;
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        if(selected)
            canvas.drawLine(0, getHeight() - strokWidth, getWidth(), getHeight() - strokWidth, mPaint);
    }

    public boolean onTouchEvent(MotionEvent me)
    {
        if(me.getAction() == MotionEvent.ACTION_UP && me.getX() > 0 && me.getX() < getWidth() && me.getY() > 0 && me.getY() < getHeight())
        {
            mTabListener.onTabSelected(mTabName, mTabIndex);
        }
        return true;
    }

    public void setSelected(boolean isSelected)
    {
        if(isSelected)
        {
            setTypeface(App.ROBOTO_MEDIUM);

        }
        else if(selected)setTypeface(App.ROBOTO_REGULAR);

        selected = isSelected;
    }

    public boolean isTabSelected()
    {
        return selected;
    }

    public void close()
    {
        destroyDrawingCache();
        mTabListener = null;
        mPaint = null;
    }

}