package fm.bling.blingy.stateMachine;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.HashMap;

import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.mashup.model.MashupDetails;
import fm.bling.blingy.registration.CAALogin;
import fm.bling.blingy.stateMachine.mashup.MashupContract;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.widget.CAAWidgetSettingsSingleton;
import fm.bling.blingy.widget.model.CAAInviteWidgetSetting;

/**
 * Created by Chosen-pro on 03/07/2016.
 */

public class StateMachine extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 3;

    public static final int INVITE_EXCLUDE_TYPE_EMAIL = 1;
    public static final int INVITE_EXCLUDE_TYPE_PHONE = 2;

    private static final String EVENTS_TABLE_NAME = "events";

    private static final String EVENTS_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + EVENTS_TABLE_NAME + " (" +
                    "class INT, " +
                    "event_type INT, " +
                    "object_id INT, " +
                    "date INT" +
                    ");";
    private static final String INVITED_EXCLUDE_TABLE_NAME = "invite_exclude";

    private static final String INVITE_EXCLUDE_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + INVITED_EXCLUDE_TABLE_NAME + " (" +
                    "content TEXT, " +
                    "type INT, " +
                    "date INT" +
                    ");";

    private static final String EVENTS_INDEX_DATE_CREATE = "CREATE INDEX IF NOT EXISTS events_date_idx ON " + EVENTS_TABLE_NAME + "(date);";
    private static final String EVENTS_CLASS_CREATE = "CREATE INDEX IF NOT EXISTS events_class_idx ON " + EVENTS_TABLE_NAME + "(class);";
    private static final String EVENTS_EVENT_TYPE_INDEX_CREATE = "CREATE INDEX IF NOT EXISTS events_sub_class_idx ON " + EVENTS_TABLE_NAME + "(event_type);";

    private static final String INVITE_EXCLUDE_TYPE_INDEX_CREATE = "CREATE INDEX IF NOT EXISTS invite_type_idx ON " + INVITED_EXCLUDE_TABLE_NAME + "(type);";
    private static final String INVITE_EXCLUDE_DATE_INDEX_CREATE = "CREATE INDEX IF NOT EXISTS invite_date_idx ON " + INVITED_EXCLUDE_TABLE_NAME + "(date);";

    private static final String STATE_MACHINE_CREATION_DATE = "state_machine_creation_date";

    private Context mContext;

    private static ArrayList<OnStateMachineUpdated> onStateMachineUpdateds;

    private StateMachine(Context context) {
        super(context, Constants.BLINGY_EVENT_SQLITE_DB, null, DATABASE_VERSION);
        mContext = context;
    }

    private static StateMachine ourInstance;

    public static StateMachine getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new StateMachine(context);
        }
        return ourInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Cursor cursor = db.rawQuery("SELECT name FROM " + Constants.BLINGY_EVENT_SQLITE_DB + " WHERE type=\'table\' AND name='" + EVENTS_TABLE_NAME + "';", null);
            cursor.close();
        } catch (Exception e) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(mContext.getResources().getString(R.string.user_shared_prefs), Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(STATE_MACHINE_CREATION_DATE, timeInSeconds());
            editor.apply();
        }

        db.execSQL(MashupContract.QUERY_CREATE_TABLE);

        db.execSQL(EVENTS_TABLE_CREATE);
        db.execSQL(EVENTS_INDEX_DATE_CREATE);
        db.execSQL(EVENTS_CLASS_CREATE);
        db.execSQL(EVENTS_EVENT_TYPE_INDEX_CREATE);

        db.execSQL(INVITE_EXCLUDE_TABLE_CREATE);
        db.execSQL(INVITE_EXCLUDE_DATE_INDEX_CREATE);
        db.execSQL(INVITE_EXCLUDE_TYPE_INDEX_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + INVITED_EXCLUDE_TABLE_NAME + ";");
        db.execSQL("DROP TABLE IF EXISTS " + EVENTS_TABLE_NAME + ";");
        onCreate(db);
    }

    public void insertEvent(StateMachineEvent event) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(EVENTS_TABLE_CREATE);
        db.execSQL("INSERT INTO " + EVENTS_TABLE_NAME + " (class, event_type, object_id, date) " +
                "VALUES (" +
                event.getEventClass().ordinal() + ", " +
                event.getEventOrdinal() + ", " +
                event.getObjectId() + ", " +
                event.getEventDate() + ");");
        db.close();
    }

    /**
     * StateMachineMessage getStateMachineMessage(NavDrawerFragmentActivity, StateMachineMessageListener)
     * Check if widget needs to be presented to the user and return a message if that is the case
     *
     * @param activity        - must be an instance of NavDrawerFragmentActivity because this contains methods for getting permissions
     * @param onReadyListener - listener will be called when all data has been collected for widget
     * @return StateMachineMessage
     */
    public synchronized Message getStateMessage(BaseActivity activity, StateMachineMessageListener onReadyListener) {
        StateMachineEvent.MESSAGE_TYPE messageType;
        SQLiteDatabase db = getReadableDatabase();
        HashMap<String, CAAInviteWidgetSetting> settings = CAAWidgetSettingsSingleton.getInstance().getSettings();

        if (settings.containsKey(StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_SMS.name().toLowerCase()) &&
                settings.get(StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_SMS.name().toLowerCase()).getStatus() > 0 &&
                shouldSMSInviteAFriend(db)) {
            messageType = StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_SMS;
        } else if (settings.containsKey(StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_EMAIL.name().toLowerCase()) &&
                settings.get(StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_EMAIL.name().toLowerCase()).getStatus() > 0 &&
                shouldEmailInviteAFriend(db)) {
            messageType = StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_EMAIL;
        } else if (settings.containsKey(StateMachineEvent.MESSAGE_TYPE.YOU_SHOULD_SIGN_UP.name().toLowerCase()) &&
                settings.get(StateMachineEvent.MESSAGE_TYPE.YOU_SHOULD_SIGN_UP.name().toLowerCase()).getStatus() > 0 &&
                shouldSignUp(db)) {
            messageType = StateMachineEvent.MESSAGE_TYPE.YOU_SHOULD_SIGN_UP;
        } else if (settings.containsKey(StateMachineEvent.MESSAGE_TYPE.SUGGEST_FOLLOW.name().toLowerCase()) &&
                settings.get(StateMachineEvent.MESSAGE_TYPE.SUGGEST_FOLLOW.name().toLowerCase()).getStatus() > 0 &&
                shouldSuggestFollow(db)) {
            messageType = StateMachineEvent.MESSAGE_TYPE.SUGGEST_FOLLOW;
        } else if (settings.containsKey(StateMachineEvent.MESSAGE_TYPE.PLAY_NEW_GAME.name().toLowerCase()) &&
                settings.get(StateMachineEvent.MESSAGE_TYPE.PLAY_NEW_GAME.name().toLowerCase()).getStatus() > 0 &&
                shouldPlayNewGame(db)) {
            messageType = StateMachineEvent.MESSAGE_TYPE.PLAY_NEW_GAME;
        } else if (settings.containsKey(StateMachineEvent.MESSAGE_TYPE.TAKE_A_PHOTO.name().toLowerCase()) &&
                settings.get(StateMachineEvent.MESSAGE_TYPE.TAKE_A_PHOTO.name().toLowerCase()).getStatus() > 0 &&
                shouldTakePhoto(db)) {
            messageType = StateMachineEvent.MESSAGE_TYPE.TAKE_A_PHOTO;
        } else if (settings.containsKey(StateMachineEvent.MESSAGE_TYPE.TRY_UPLOAD.name().toLowerCase()) &&
                settings.get(StateMachineEvent.MESSAGE_TYPE.TRY_UPLOAD.name().toLowerCase()).getStatus() > 0 &&
                shouldTryUpload(db)) {
            messageType = StateMachineEvent.MESSAGE_TYPE.TRY_UPLOAD;
        } else return null;

        return new Message(activity, messageType, onReadyListener);
    }

    private int getPeriodOfMessage(StateMachineEvent.MESSAGE_TYPE type, int defaultPeriod) {
        CAAInviteWidgetSetting setting = CAAWidgetSettingsSingleton.getInstance().getSettingByType(type.name().toLowerCase());

        int time;
        if (setting == null) time = 0;
        else time = setting.getTime();

        if (time < 5)
            time = defaultPeriod;

        return time;
    }

    private boolean shouldSMSInviteAFriend(SQLiteDatabase db) {
        int time = getPeriodOfMessage(StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_SMS, 60 * 60);
        return shouldShowMessageAgain(db, StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_SMS, time);
    }

    private synchronized boolean shouldEmailInviteAFriend(SQLiteDatabase db) {
        int time = getPeriodOfMessage(StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_EMAIL, 60 * 60 * 24);
        return shouldShowMessageAgain(db, StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_EMAIL, time);
    }

    private synchronized boolean shouldSignUp(SQLiteDatabase db) {
        int time = getPeriodOfMessage(StateMachineEvent.MESSAGE_TYPE.YOU_SHOULD_SIGN_UP, 60 * 60 * 6);
        return CAALogin.userIsAnonymous() && shouldShowMessageAgain(db, StateMachineEvent.MESSAGE_TYPE.YOU_SHOULD_SIGN_UP, time);
    }

    private synchronized boolean shouldSuggestFollow(SQLiteDatabase db) {
        if (db == null || !db.isOpen()) db = getReadableDatabase();

        SQLiteStatement count = db.compileStatement("SELECT COUNT(*) FROM " + EVENTS_TABLE_NAME +
                " WHERE class = " + StateMachineEvent.EVENT_CLASS.SOCIAL.ordinal() +
                " AND event_type = " + StateMachineEvent.EVENT_TYPE.FOLLOW_USER.ordinal() + ";"
        );
        int usersFollowed = (int) count.simpleQueryForLong();

        int periodMultiplyer;
        if (usersFollowed == 0) periodMultiplyer = 1;
        else if (usersFollowed < 5) periodMultiplyer = 2;
        else if (usersFollowed < 10) periodMultiplyer = 3;
        else if (usersFollowed < 25) periodMultiplyer = 4;
        else periodMultiplyer = 5;

        int period = getPeriodOfMessage(StateMachineEvent.MESSAGE_TYPE.SUGGEST_FOLLOW, 60 * 60);
        if (!shouldShowMessageAgain(db, StateMachineEvent.MESSAGE_TYPE.SUGGEST_FOLLOW, periodMultiplyer * period)) {
            return false;
        }

        Cursor c = db.rawQuery("SELECT date FROM " + EVENTS_TABLE_NAME +
                        " WHERE class = " + StateMachineEvent.EVENT_CLASS.SOCIAL.ordinal() +
                        " AND event_type = " + StateMachineEvent.EVENT_TYPE.FOLLOW_USER.ordinal() +
                        " ORDER BY date DESC" +
                        " LIMIT 1;"
                , null);

        if (c.moveToFirst()) {
            int time = c.getInt(0);
            c.close();
            return (time < timeInSeconds() - (periodMultiplyer * period));
        } else {
            c.close();
            return true;
        }
    }

    private synchronized boolean shouldPlayNewGame(SQLiteDatabase db) {
        int period = getPeriodOfMessage(StateMachineEvent.MESSAGE_TYPE.PLAY_NEW_GAME, 60 * 60);
        if (!shouldShowMessageAgain(db, StateMachineEvent.MESSAGE_TYPE.PLAY_NEW_GAME, period)) {
            return false;
        }

        if (db == null || !db.isOpen()) db = getReadableDatabase();

        SQLiteStatement count = db.compileStatement("SELECT COUNT(*) FROM " + EVENTS_TABLE_NAME +
                " WHERE class = " + StateMachineEvent.EVENT_CLASS.GAMES.ordinal() +
                " AND event_type = " + StateMachineEvent.EVENT_TYPE.PLAY_GAME.ordinal() +
                " AND date > " + (timeInSeconds() - period) + ";"
        );

        return (int) count.simpleQueryForLong() < 1;
    }

    private synchronized boolean shouldTakePhoto(SQLiteDatabase db) {
        int period = getPeriodOfMessage(StateMachineEvent.MESSAGE_TYPE.TAKE_A_PHOTO, 60 * 60 * 48);
        if (!shouldShowMessageAgain(db, StateMachineEvent.MESSAGE_TYPE.TAKE_A_PHOTO, period)) {
            return false;
        }

        if (db == null || !db.isOpen()) db = getReadableDatabase();

        Cursor c = db.rawQuery(
                "SELECT * FROM " + EVENTS_TABLE_NAME +
                        " WHERE class = " + StateMachineEvent.EVENT_CLASS.RECORDING.ordinal() + " AND event_type = " + StateMachineEvent.EVENT_TYPE.RECORD_PHOTO.ordinal() +
                        " ORDER BY date DESC LIMIT 1;"
                , null
        );

        boolean hasTakenAPhoto = c.moveToFirst();
        c.close();

        return !hasTakenAPhoto;
    }


    private synchronized boolean shouldTryUpload(SQLiteDatabase db) {
        int period = getPeriodOfMessage(StateMachineEvent.MESSAGE_TYPE.TRY_UPLOAD, 60 * 60 * 48);
        if (!shouldShowMessageAgain(db, StateMachineEvent.MESSAGE_TYPE.TRY_UPLOAD, period)) {
            return false;
        }

        if (db == null || !db.isOpen()) db = getReadableDatabase();

        Cursor c = db.rawQuery(
                "SELECT * FROM " + EVENTS_TABLE_NAME +
                        " WHERE class = " + StateMachineEvent.EVENT_CLASS.RECORDING.ordinal() + " AND event_type = " + StateMachineEvent.EVENT_TYPE.UPLOAD.ordinal() +
                        " ORDER BY date DESC LIMIT 1;"
                , null
        );

        boolean hasUploaded = c.moveToFirst();
        c.close();

        return !hasUploaded;
    }

    /**
     * @param db              our event db
     * @param messageType     tye of message
     * @param periodOfMessage time to wait before showing message again
     * @return true if this message hasn't been shown recently
     */
    private synchronized boolean shouldShowMessageAgain(SQLiteDatabase db, StateMachineEvent.MESSAGE_TYPE messageType, int periodOfMessage) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(mContext.getResources().getString(R.string.user_shared_prefs), Context.MODE_MULTI_PROCESS);
        int startDate = sharedPreferences.getInt(STATE_MACHINE_CREATION_DATE, timeInSeconds());

        if (startDate + periodOfMessage > timeInSeconds()) {
            return false;
        }

        if (db == null || !db.isOpen()) db = getReadableDatabase();

        Cursor c = db.rawQuery(
                "SELECT date FROM " + EVENTS_TABLE_NAME +
                        " WHERE class = " + StateMachineEvent.EVENT_CLASS.STATE_MESSAGE.ordinal() + " AND event_type = " + messageType.ordinal() +
                        " ORDER BY date DESC LIMIT 1;"
                , null
        );

        Integer timeOfLastMessage;

        if (c.moveToFirst()) {
            timeOfLastMessage = c.getInt(0);
            c.close();
            return (timeOfLastMessage < timeInSeconds() - periodOfMessage);
        } else {
            c.close();
            return true;
        }
    }

    public synchronized ArrayList<String> getTemporarilyExcluded(int type) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(INVITE_EXCLUDE_TABLE_CREATE);
        Cursor c = db.rawQuery(
                "SELECT content FROM " + INVITED_EXCLUDE_TABLE_NAME +
                        " WHERE date > " + (timeInSeconds() - CAAWidgetSettingsSingleton.getInstance().getInviteExclusionTime()) +
                        " AND type  = " + type + ";"
                , null
        );
        ArrayList<String> excluded = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                excluded.add(c.getString(0));
            } while (c.moveToNext());
        }
        return excluded;
    }

    public synchronized void excludeFromInvites(String content, int type) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(INVITE_EXCLUDE_TABLE_CREATE);
        db.execSQL("INSERT INTO " + INVITED_EXCLUDE_TABLE_NAME + " (content, type, date) " +
                "VALUES ('" +
                content + "', " +
                type + ", " +
                timeInSeconds() + ");");
        db.close();

        if (onStateMachineUpdateds != null)
            for (OnStateMachineUpdated osmu : onStateMachineUpdateds)
                osmu.onContactUpdate(content);
    }

    public synchronized void insertMashup(String mashup_id, int step, int videos_num) {
            SQLiteDatabase db = getWritableDatabase();
        try {
            db.execSQL(MashupContract.QUERY_CREATE_TABLE);
            db.execSQL(String.format(MashupContract.QUERY_INSERT_MASHUP, mashup_id, step, videos_num));
        }catch (Throwable throwable){
            // There is mashup_id like that.
        }finally {
            db.close();
        }
    }

    public synchronized void updateStep(String mashup_id, int step) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(MashupContract.QUERY_CREATE_TABLE);
        db.execSQL(String.format(MashupContract.QUERY_UPDATE_STEP, step, mashup_id));
        db.close();
    }

    public synchronized void updateVideos(String mashup_id, int videos_num) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(MashupContract.QUERY_CREATE_TABLE);
        db.execSQL(String.format(MashupContract.QUERY_UPDATE_VIDEOS_NUM, videos_num, mashup_id));
        db.close();
    }

    public synchronized void deleteMashup(String mashup_id) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(MashupContract.QUERY_CREATE_TABLE);
        db.execSQL(String.format(MashupContract.QUERY_DELETE_MASHUP, mashup_id));
        db.close();
    }

    public synchronized MashupDetails getMashupDetails(String mashup_id) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(MashupContract.QUERY_CREATE_TABLE);



        Cursor c = db.rawQuery(String.format(MashupContract.QUERY_GET_MASHUP_DETAILS, mashup_id), null);

        int step, videosNum;
        try {
            if (c.moveToFirst()) {
                step = c.getInt(c.getColumnIndex(MashupContract.COLUMN_STEP));
                videosNum = c.getInt(c.getColumnIndex(MashupContract.COLUMN_VIDEOS_NUM));
                return new MashupDetails(mashup_id, step, videosNum);
            } else {
                return null;
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            c.close();
            db.close();
        }
    }

    public static int timeInSeconds() {
        return (int) (System.currentTimeMillis() / 1000);
    }

    public static void addOnStateMachineUpdated(OnStateMachineUpdated onStateMachineUpdated) {
        if (onStateMachineUpdateds == null)
            onStateMachineUpdateds = new ArrayList<>();
        onStateMachineUpdateds.add(onStateMachineUpdated);
    }

    public static void removeOnStateMachineUpdated(OnStateMachineUpdated onStateMachineUpdated) {
        onStateMachineUpdateds.remove(onStateMachineUpdated);
    }

    public interface OnStateMachineUpdated {
        void onContactUpdate(String phoneEmail);
    }

}
