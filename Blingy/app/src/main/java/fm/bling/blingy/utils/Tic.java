package fm.bling.blingy.utils;

import android.os.SystemClock;

import java.util.HashMap;

import fm.bling.blingy.BuildConfig;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 19/07/2016.
 * History:
 *
 * Util Benchmark class.
 * ***********************************
 */
public class Tic
{
    private static long time;
    private static long logCounter, counter = 0, average = 0, highest = 0, lowest;

    private static HashMap<Integer, Long> time4Id;

    public static void tic()
    {
        logCounter++;
        time = SystemClock.elapsedRealtime();
    }

    public static long tac()
    {
        long elapsedTime =  SystemClock.elapsedRealtime() - time;
        if(BuildConfig.DEBUG)
            System.out.println(logCounter+">>tic>tac>> Elapsed time since start : " + elapsedTime + " ms");
        return elapsedTime;
    }

    public static long tacWithAverage()
    {
        long elapsedTime =  SystemClock.elapsedRealtime() - time;

        counter++;
        average += elapsedTime;

        if(elapsedTime > highest)
            highest = elapsedTime;
        else if (elapsedTime < lowest)
            lowest = elapsedTime;

        System.out.println(logCounter+">>tic>tac>> Elapsed time since start : " + elapsedTime
                + " ms, min : " + lowest + " ms, max : " + highest + " ms, average : " + (average / counter) );
        return elapsedTime;
    }


    public static long tacTic()
    {
        long elapsedTime =  SystemClock.elapsedRealtime() - time;
        tic();
        if(BuildConfig.DEBUG)
            System.out.println(logCounter+">>tic>tac>> Elapsed time since start : " + elapsedTime + " ms");
        return elapsedTime;
    }

    public static void tic(int id)
    {
        if(time4Id == null)
            time4Id = new HashMap<>();
        logCounter++;
        time4Id.put(id, SystemClock.elapsedRealtime());
    }

    public static long tac(int id)
    {
        if(time4Id == null || time4Id.get(id) == null)
            return 0L;
        long elapsedTime =  SystemClock.elapsedRealtime() - time4Id.get(id);
        if(BuildConfig.DEBUG)
            System.out.println(logCounter+">>>id>>>"+id+">>tic>>tac> Elapsed time since start : " + elapsedTime + " ms");
        return elapsedTime;
    }

    public static long tacTic(int id)
    {
        if(time4Id == null || time4Id.get(id) == null)
            return 0L;
        long elapsedTime =  SystemClock.elapsedRealtime() - time4Id.get(id);
        tic(id);
        if(BuildConfig.DEBUG)
            System.out.println(logCounter+">>>id>>>"+id+">>tic>>tac> Elapsed time since start : " + elapsedTime + " ms");
        return elapsedTime;
    }

}