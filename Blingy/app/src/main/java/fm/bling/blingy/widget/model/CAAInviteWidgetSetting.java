package fm.bling.blingy.widget.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chosen-pro on 17/07/2016.
 */

public class CAAInviteWidgetSetting {
    @SerializedName("type")
    private String type;
    @SerializedName("text")
    private String text;
    @SerializedName("time")
    private int time;
    @SerializedName("status")
    private int status;

    public CAAInviteWidgetSetting(String type, String text, int time, int status) {
        this.type = type;
        this.text = text;
        this.time = time;
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getStatus() {
        return status;
    }
}
