package fm.bling.blingy.dialogs.share.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAPostShare
 * Description:
 * Created by Dawidowicz Nadav on 5/12/15.
 * History:
 * ***********************************
 */
public class CAAPostShare
{
    public CAAPostShare(String platform, String shareId, String content)
    {
        this.platform = platform;
        this.shareId  = shareId;
        this.content  = content;
    }

    @SerializedName("platform")
    private String platform;

    @SerializedName("share_id")
    private String shareId;

    @SerializedName("text")
    private String content;

    @SerializedName("email_subject")
    private String subject;

    @SerializedName("object_id")
    private String objectId; // json object >>

    public String getShareId()
    {
        return shareId;
    }

    public String getContent()
    {
        return content;
    }

    public String getObjectId()
    {
        return objectId;
    }

    public String getSubject()
    {
        return subject;
    }
}