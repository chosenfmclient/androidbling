//package fm.bling.blingy.mashup;
//
//import android.content.Context;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//
//import fm.bling.blingy.R;
//import fm.bling.blingy.mashup.listeners.SelectedCallback;
//import fm.bling.blingy.mashup.model.InviteeContact;
//import fm.bling.blingy.utils.AdaptersDataTypes;
//import fm.bling.blingy.utils.Constants;
//import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
//import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
//
///**
// * *********************************
// * Project: Blin.gy Android Application
// * Description:
// * Created by Oren Zakay on 1/4/17.
// * History:
// * ***********************************
// */
//public class InviteMashupAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//
//    private ImageLoaderManager mImageLoaderManager;
//    private ArrayList<InviteeContact> mContactsData;
//    private LayoutInflater mLayoutInflater;
//    private SelectedCallback mSelectedCallback;
//
//    private int mContactsSize, mFollowingSize;
//
//    public InviteMashupAdapter(ImageLoaderManager imageLoaderManager, ArrayList<InviteeContact> contacts, SelectedCallback selectedCallback){
//        this.mImageLoaderManager = imageLoaderManager;
//        this.mContactsData = contacts;
//        this.mSelectedCallback = selectedCallback;
//    }
//
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        RecyclerView.ViewHolder holder = null;
//        if(mLayoutInflater == null)
//            mLayoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View view;
//        switch (viewType) {
//            case AdaptersDataTypes.BASE_ITEM:
//                view = mLayoutInflater.inflate(R.layout.mashup_invite_contact_item, parent, false);
//                holder = new InviteMashupAdapter.DataObjectHolder(view);
//                break;
//            case AdaptersDataTypes.TYPE_SECTION:
//                view = mLayoutInflater.inflate(R.layout.mashup_invite_section_item, parent, false);
//                holder = new InviteMashupAdapter.SectionHolder(view);
//                break;
//        }
//        return holder;
//    }
//
//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        InviteeContact contact = mContactsData.get(position);
//        if(holder instanceof DataObjectHolder){
//            DataObjectHolder dataObjectHolder = ((DataObjectHolder)holder);
//            dataObjectHolder.textName.setText(contact.getName());
//
//            if(contact.isInvited()){
//                dataObjectHolder.itemView.setEnabled(false);
//                dataObjectHolder.itemView.setClickable(false);
//
//                dataObjectHolder.inviteFriend.setImageResource(R.drawable.ic_group_invite_checked_24dp);
//                dataObjectHolder.inviteFriend.setAlpha(0.5f);
//                dataObjectHolder.inviteFriend.setEnabled(false);
//                dataObjectHolder.inviteFriend.setClickable(false);
//            }
//            else{
//                dataObjectHolder.itemView.setEnabled(true);
//                dataObjectHolder.itemView.setClickable(true);
//
//                dataObjectHolder.inviteFriend.setAlpha(1f);
//                dataObjectHolder.inviteFriend.setEnabled(true);
//                dataObjectHolder.inviteFriend.setClickable(true);
//
//                if(contact.isSelected())
//                    dataObjectHolder.inviteFriend.setImageResource(R.drawable.ic_group_invite_checked_24dp);
//                else
//                    dataObjectHolder.inviteFriend.setImageResource(R.drawable.ic_group_invite_unchecked_24dp);
//            }
//
//            if(contact.getPhotoUrl() != null){
//                dataObjectHolder.imageProfile.setUrl(contact.getPhotoUrl());
//                mImageLoaderManager.DisplayImage(((DataObjectHolder)holder).imageProfile);
//            }
//            else{
//                dataObjectHolder.imageProfile.setImageResource(R.drawable.extra_large_userpic_placeholder);
//            }
//
//            dataObjectHolder.inviteFriend.setTag(position);
//            dataObjectHolder.itemView.setTag(((DataObjectHolder)holder).inviteFriend);
//        }
//        else{
//            ((SectionHolder)holder).mTextTotal.setText(contact.getText());
//            ((SectionHolder)holder).mSelectAll.setTag(contact.getType());
//        }
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        if(mContactsData.get(position).getName() == null)
//            return AdaptersDataTypes.TYPE_SECTION; // in that case we need to get the type from the object
//        else
//            return AdaptersDataTypes.BASE_ITEM;
//    }
//
//    @Override
//    public void onViewRecycled(RecyclerView.ViewHolder holder) {
//        super.onViewRecycled(holder);
//    }
//
//    @Override
//    public int getItemCount() {
//        return mContactsData.size();
//    }
//
//    public void setContactsSize(int contactsSize){
//        this.mContactsSize = contactsSize;
//    }
//
//    public void setFollowingSize(int followingSize){
//        this.mFollowingSize = followingSize;
//    }
//
//    public int getHeaderPositionByType(int type){
//        for (InviteeContact item : mContactsData){
//            if(item.getName() == null && item.getType() == type)
//                return mContactsData.indexOf(item);
//        }
//        return -1;
//    }
//
//    public class DataObjectHolder extends RecyclerView.ViewHolder{
//        private LinearLayout mItem;
//        private TextView textName;
//        private ScaledImageView imageProfile;
//        private ImageView inviteFriend;
//
//        public DataObjectHolder(View itemView) {
//            super(itemView);
//            mItem = (LinearLayout)itemView.findViewById(R.id.connection_item);
//            mItem.setBackgroundResource(R.drawable.notification_ripple_white_background);
//            textName = (TextView)itemView.findViewById(R.id.text_user_name);
//            imageProfile = (ScaledImageView)itemView.findViewById(R.id.image_view_profile);
//            inviteFriend = (ImageView) itemView.findViewById(R.id.check_icon);
//
//            imageProfile.setIsRoundedImage(true);
//            imageProfile.setPlaceHolder(R.drawable.extra_large_userpic_placeholder);
//            imageProfile.setImageLoader(mImageLoaderManager);
//
//
//            inviteFriend.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    int pos = (int)v.getTag();
//                    InviteeContact inviteeContact = mContactsData.get(pos);
//                    if(inviteeContact.isSelected()) {
//                        inviteFriend.setImageResource(R.drawable.ic_group_invite_unchecked_24dp);
//                        inviteeContact.setSelected(false);
//                        mSelectedCallback.onRemove(inviteeContact);
//                    }
//                    else{
//                        inviteFriend.setImageResource(R.drawable.ic_group_invite_checked_24dp);
//                        inviteeContact.setSelected(true);
//                        mSelectedCallback.onSelected(inviteeContact);
//                    }
//                }
//            });
//
//            mItem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    inviteFriend.callOnClick();
//                }
//            });
//        }
//    }
//
//    public class SectionHolder extends RecyclerView.ViewHolder {
//        private TextView mTextTotal;
//        private TextView mSelectAll;
//        public SectionHolder(View itemView) {
//            super(itemView);
//
//            mTextTotal = (TextView) itemView.findViewById(R.id.text_total);
//            mSelectAll = (TextView) itemView.findViewById(R.id.select_all);
//            mSelectAll.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if((v.getTag()).equals(Constants.FOLLOWING)){
//                        if(mFollowingSize > 0){
//                            ArrayList<InviteeContact> following = new ArrayList<>();
//                            for (int i = 1; i < mFollowingSize + 1; i++){
//                                if(!mContactsData.get(i).isSelected() && !mContactsData.get(i).isInvited()) {
//                                    mContactsData.get(i).setSelected(true);
//                                    following.add(mContactsData.get(i));
//                                }
//                            }
//                            notifyItemRangeChanged(1, mFollowingSize);
//                            mSelectedCallback.onSelectedAll(following);
//                        }
//                    }
//                    else{
//                        if(mContactsSize > 0){
//                            ArrayList<InviteeContact> contacts = new ArrayList<>();
//                            int start = mFollowingSize > 0 ? mFollowingSize + 2 : 1;
//                            for (int i = start; i < mContactsData.size(); i++){
//                                if(!mContactsData.get(i).isSelected() && !mContactsData.get(i).isInvited()) {
//                                    mContactsData.get(i).setSelected(true);
//                                    contacts.add(mContactsData.get(i));
//                                }
//                            }
//                            notifyItemRangeChanged(start, mContactsData.size());
//                            mSelectedCallback.onSelectedAll(contacts);
//                        }
//                    }
//                }
//            });
//        }
//    }
//
//}
