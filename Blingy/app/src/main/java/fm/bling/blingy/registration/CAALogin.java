package fm.bling.blingy.registration;

import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.AccessToken;

import fm.bling.blingy.R;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.utils.Constants;


/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAALogin
 * Description: Retrieves login info and sets in the user data singleton
 * Created by Zach Gerstman on 5/12/15.
 * History:
 * ***********************************
 */
public class CAALogin
{

    public static String isAuto = "0";
    public static int failedLogins = 0;
    /**
     * Login a user
     * @return Access Token - string
     */
    public static String Login(CAALoginListener cAAlogin)
    {
        /**
         * Get data from shared preferences
         */
        SharedPreferences settings = SharedPreferencesManager.getInstance();
        String accountType = settings.getString(Constants.ACCOUNT_TYPE, "empty");

        if (accountType.equals("empty")) {
            SharedPreferencesManager.getEditor().putString(Constants.ACCOUNT_TYPE, "anonymous").commit();
        }
        switch (accountType) {
            case Constants.GOOGLE_PLUS_ACC:
                CAAGooglePlusLogin.GooglePlusLogin(cAAlogin);
                break;
            case "facebook":
                CAAFacebookLogin.FacebookLogin(cAAlogin);
                break;
            case "email":
                String email = settings.getString("email", "");
                String password = settings.getString("password", "");
                CAAEmailLogin.EmailLogin(email, password, cAAlogin);
                break;
            case "anonymous":
            case "empty":
                cAAlogin.didFinishLogin(true, 200);
                break;
            default:
                break;
        }

        CAAUserDataSingleton userDataSingleton = CAAUserDataSingleton.getInstance();

        return userDataSingleton.getAccessToken();
    }

//
//    public static String getNewToken(Context context, CAALoginListener cAALogin) {
//        CAAUserDataSingleton userDataSingleton = CAAUserDataSingleton.getInstance();
//        String accountType = userDataSingleton.getAccountType();
//        switch (accountType) {
//            case Constants.GOOGLE_PLUS_ACC:
//                CAAGooglePlusLogin.GooglePlusLogin(context, cAALogin);
//                break;
//            case "facebook":
//                AccessToken accessToken = AccessToken.getCurrentAccessToken();
//                userDataSingleton.setAccessToken(accessToken.getToken());
//                cAALogin.didFinishLogin(true, 1);
//                break;
//            case "email":
//                CAAEmailLogin.RefreshAccessToken(context, userDataSingleton.getEmail(), userDataSingleton.getPassword(), cAALogin);
//                break;
//            case "anonymous":
//                CAAAnonymousLogin.refreshAnonymousToken(context);
//                break;
//            default:
//                break;
//        }
//
//        return userDataSingleton.getAccessToken();
//    }

    public static void updateUserSingleton(String accountType, String accessToken, String email, String password, String firstName, String lastName, String deviceIdentifier, String userId) {
        CAAUserDataSingleton userDataSingleton = CAAUserDataSingleton.getInstance();
        if (!email.isEmpty()) {
            userDataSingleton.setEmail(email);
        }
        if (!password.isEmpty()) {
            userDataSingleton.setPassword(password);
        }
        if (!accessToken.isEmpty()) {
            userDataSingleton.setAccessToken(accessToken);
        }
        if (!lastName.isEmpty()) {
            userDataSingleton.setLastName(lastName);
        }
        if (!firstName.isEmpty()) {
            userDataSingleton.setFirstName(lastName);
        }
        if (!deviceIdentifier.isEmpty()) {
            userDataSingleton.setDeviceIdentifier(deviceIdentifier);
        }
        if (!accountType.isEmpty()) {
            userDataSingleton.setAccountType(accountType);
        }
        if (!userId.isEmpty()) {
            userDataSingleton.setUserId(userId);
        }
    }

//    public static Boolean loginDataSaved() {
//        CAAUserDataSingleton userDataSingleton = CAAUserDataSingleton.getInstance();
//        return !userDataSingleton.getAccessToken().isEmpty();
//    }

    public static Boolean userIsAnonymous() {
        String accountType = CAAUserDataSingleton.getInstance().getAccountType();
        if (accountType == null) {
            SharedPreferencesManager.getInstance().getString(Constants.ACCOUNT_TYPE, "anonymous");
        }
        if (accountType.equals("anonymous")) {
            return true;
        }
        return false;
    }
}
