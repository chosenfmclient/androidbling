package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chosen-pro on 1/13/16.
 */
public class CAAPushMetric {

    public CAAPushMetric(String objectId, String object, String notificationId, String notificationType, String type, String text) {
        this.objectId = objectId;
        this.object = object;
        this.notificationId = notificationId;
        this.notificationType = notificationType;
        this.type = type;
        this.text = text;
    }

    public CAAPushMetric(String objectId, String object, int massNotificationId, String notificationType, String type, String text) {
        this.objectId = objectId;
        this.object = object;
        this.massNotificationId = String.valueOf(massNotificationId);
        this.notificationType = notificationType;
        this.type = type;
        this.text = text;
    }

    @SerializedName("object_id")
    private String objectId;

    @SerializedName("object")
    private String object;

    @SerializedName("notification_id")
    private String notificationId;

    @SerializedName("mass_notification_id")
    private String massNotificationId;

    @SerializedName("notification_type")
    private String notificationType;

    @SerializedName("type")
    private String type;

    @SerializedName("alert")
    private String text;

    @SerializedName("auto")
    private String auto = "0";

    public String getAuto() {
        return auto;
    }

    public void setAuto(String auto) {
        this.auto = auto;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMassNotificationId() {
        return massNotificationId;
    }

    public void setMassNotificationId(String massNotificationId) {
        this.massNotificationId = massNotificationId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
