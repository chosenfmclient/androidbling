package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;

public class CAAUserExperience
{
    @SerializedName("added")
    private String added;

    @SerializedName("current")
    private String current;

    @SerializedName("required")
    private String required;

    @SerializedName("required_total")
    private String requiredTotal;

    @SerializedName("earned")
    private String earned;

    @SerializedName("lacked")
    private String lacked;

    public String getLacked() {
        return lacked;
    }

//    public void setLacked(String lacked) {
//        this.lacked = lacked;
//    }

    public String getAdded() {
        return added;
    }

//    public void setAdded(String added) {
//        this.added = added;
//    }

    public String getCurrent() {
        return current;
    }

//    public void setCurrent(String current) {
//        this.current = current;
//    }

    public String getRequired() {
        return required;
    }

//    public void setRequired(String required) {
//        this.required = required;
//    }

    public String getRequiredTotal() {
        return requiredTotal;
    }

//    public void setRequiredTotal(String requiredTotal) {
//        this.requiredTotal = requiredTotal;
//    }

    public String getEarned() {
        return earned;
    }

//    public void setEarned(String earned) {
//        this.earned = earned;
//    }

    @Override
    public String toString() {
        String tmp = added + "\n" + current + "\n" + required + "\n" + requiredTotal + "\n" + earned + "\n" + lacked ;

        return tmp;
    }
}
