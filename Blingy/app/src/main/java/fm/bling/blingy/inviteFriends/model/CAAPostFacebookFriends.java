package fm.bling.blingy.inviteFriends.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAPostTwitter
 * Description:
 * Created by Zach on 08/30/15.
 * History:
 * ***********************************
 */
public class CAAPostFacebookFriends {
    @SerializedName("facebook_ids")
    private List <String> facebookIds;

    public CAAPostFacebookFriends(List<String> facebookIds) {
        this.facebookIds = facebookIds;
    }

    public List<String> getFacebookIds() {
        return facebookIds;
    }

    public void setFacebookIds(List<String> facebookIds) {
        this.facebookIds = facebookIds;
    }
}
