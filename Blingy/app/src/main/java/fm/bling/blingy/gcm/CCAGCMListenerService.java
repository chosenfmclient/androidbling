package fm.bling.blingy.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.record.Utils;
import fm.bling.blingy.registration.CAALogin;
import fm.bling.blingy.registration.CAALoginListener;
import fm.bling.blingy.rest.model.CAAPushMetric;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.splashScreen.NotificationEnum;
import fm.bling.blingy.splashScreen.SplashScreenActivity;
import fm.bling.blingy.tracking.TrackingManager;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by : Oren Zakay.
 */
public class CCAGCMListenerService extends FirebaseMessagingService implements CAALoginListener {
    private CAAPushMetric pushMetric;
    private String isAuto = "0";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData().containsKey("type") && remoteMessage.getData().containsKey("text")) {
            Boolean appInForeground = Utils.isAppOnForeground(getApplicationContext());
            if (appInForeground) {
                postPushMetric(remoteMessage.getData(), true);
            } else {
                isAuto = "1";
                generateNotification(getApplicationContext(), remoteMessage.getData());
            }
        }
    }

    private void generateNotification(Context context, Map<String, String> data) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String text = data.get("text");
        int notificationID = NotificationEnum.BLINGY_DEFAULT.getID();
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setDefaults(NotificationCompat.DEFAULT_ALL)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setContentTitle("Blin.gy")
                        .setContentText(text)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setAutoCancel(true)
                        .setSound(alarmSound)
                        .setColor(getResources().getColor(R.color.main_blue))
                        .setLights(Color.MAGENTA, 500, 200)
                        .setTicker(text)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(text));


        Intent resultIntent = new Intent(context, SplashScreenActivity.class);
        resultIntent.putExtra("text", text);
        if (data.containsKey("object")) {
            resultIntent.putExtra("object", data.get("object"));
            resultIntent.putExtra("type", data.get("type"));
            if (data.containsKey("object_id")) {
                resultIntent.putExtra("object_id", data.get("object_id"));
            }
            if (data.containsKey("notification_id")) {
                resultIntent.putExtra("notification_id", data.get("notification_id"));
            }
            if (data.containsKey("mass_notification_id")) {
                resultIntent.putExtra("mass_notification_id", data.get("mass_notification_id"));
            }
            if (data.containsKey("notification_type")) {
                String notificationType = data.get("notification_type");
                resultIntent.putExtra("notification_type", notificationType);
                notificationID = NotificationEnum.getNotificationID(notificationType);
            }
            if (data.containsKey("video_id")) {
                resultIntent.putExtra("video_id", data.get("video_id"));
            }
        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(resultIntent);
        int requestID = (int) System.currentTimeMillis();
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(requestID, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(notificationID, mBuilder.build());
        postPushMetric(data, false);
    }

    private void postPushMetric(Map<String, String> data, boolean active) {
        String objectId = data.containsKey("object_id") ? data.get("object_id") : "";
        String object = data.containsKey("object") ? data.get("type") : "";
        String notificationId = data.containsKey("notification_id") ? data.get("notification_id") : "";
        int massNotificationId = data.containsKey("mass_notification_id") ? Integer.valueOf(data.get("mass_notification_id")) : 0;
        String notificationType = data.containsKey("notification_type") ? data.get("notification_type") : "";
        String text = data.containsKey("text") ? data.get("text") : "";
        String type = active ? "active" : "received";

        if (massNotificationId > 0) {
            pushMetric = new CAAPushMetric(objectId, object, massNotificationId, notificationType, type, text);
            TrackingManager.getInstance().receivedPushNotification(notificationType, text, "mass_notification" ,massNotificationId + "","push");
        } else {
            pushMetric = new CAAPushMetric(objectId, object, notificationId, notificationType, type, text);
            TrackingManager.getInstance().receivedPushNotification(notificationType, text, "generated" , notificationId, "push");
        }


        pushMetric.setAuto(isAuto);
        if (CAAUserDataSingleton.getInstance().getAccessToken().isEmpty() || CAAUserDataSingleton.getInstance().getAccessToken() == null) {
            if(isAuto.equalsIgnoreCase("1"))
                CAALogin.isAuto = "1";
            CAALogin.Login(this);
        } else {
            pushMetricApi();
        }
    }

    public void didFinishLogin(Boolean success, int status) {
        if (success) {
            pushMetricApi();
        }
    }

    private void pushMetricApi() {
        App.getUrlService().postPushMetric(pushMetric, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                CAALogin.isAuto = "0";
            }

            @Override
            public void failure(RetrofitError error) {
//                if(error != null && error.getResponse() != null && error.getResponse().getStatus() == 401){
//                    if(isAuto.equalsIgnoreCase("1"))
//                        CAALogin.isAuto = "1";
//                    CAALogin.Login(CCAGCMListenerService.this);
//                }
            }
        });
    }
}
