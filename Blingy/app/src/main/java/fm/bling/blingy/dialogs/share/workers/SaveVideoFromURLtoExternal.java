package fm.bling.blingy.dialogs.share.workers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import fm.bling.blingy.dialogs.share.workers.listeners.onWorkerFinishedListener;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 3/30/16.
 * History:
 * ***********************************
 */
public class SaveVideoFromURLtoExternal extends AsyncTask<Void,Void,Boolean> {

    private Context mContext;
    private boolean isPhoto = false;
    private boolean isInstagram = false;
    private String mURL;
    private ProgressDialog mProgressdialog;
    private String mFileName;
    private onWorkerFinishedListener mOnWorkerFinishedListener;

    public SaveVideoFromURLtoExternal(String URL, String pathToSave, boolean isPhoto, Context context , onWorkerFinishedListener listener, boolean isInstagram){
        this.mContext = context;
        this.isPhoto = isPhoto;
        this.isInstagram = isInstagram;
        this.mURL = URL;
        this.mFileName = pathToSave;
        this.mOnWorkerFinishedListener = listener;
        this.mProgressdialog = new ProgressDialog(mContext);
        this.mProgressdialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
             SaveVideoFromURLtoExternal.this.cancel(true);

            }
        });

    }

    @Override
    protected void onPreExecute() {
        this.mProgressdialog = ProgressDialog.show(this.mContext, null, "Please wait ...", true);
        this.mProgressdialog.setCancelable(false);
    }

    @Override
    protected Boolean doInBackground(Void... arg) {
        return DownloadFile(mURL);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if(result){
            this.mProgressdialog.dismiss();
            this.mOnWorkerFinishedListener.onWorkerFinished(result, mFileName);
        }
    }

    public boolean DownloadFile(String fileURL) {
        try {

            if(isInstagram) {
                File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "Blingy");
                if (!mediaStorageDir.exists()) {
                    if (!mediaStorageDir.mkdirs()) {
                        return false;
                    }
                }
            }

            InputStream inputStream = null;
            URL url = null;
            HttpURLConnection conn = null;
            if (fileURL.startsWith("content://com.google.android.apps.photos.content")) {
                inputStream = mContext.getContentResolver().openInputStream(Uri.parse(fileURL));
            }
            else{
                url = new URL(fileURL);
                 conn = (HttpURLConnection)url.openConnection();
                conn.setConnectTimeout(20000);
                conn.setReadTimeout(20000);
                conn.setInstanceFollowRedirects(true);
                inputStream = conn.getInputStream();
            }

            DataInputStream dataInputStream = new DataInputStream(inputStream);
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(mFileName));

            byte[]buffer = new byte[1024 * 10];
            int count;

            while(true)
            {
                count = dataInputStream.read(buffer);
                if(count == -1)
                    break;
                dataOutputStream.write(buffer, 0, count);
            }

            dataOutputStream.flush();
            dataOutputStream.close();
            dataInputStream.close();
            url = null;
            conn = null;
            return true;

        } catch (Exception e) {
            return false;

        }
    }
}
