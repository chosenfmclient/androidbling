package fm.bling.blingy.dialogs;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;

import fm.bling.blingy.R;
import fm.bling.blingy.registration.listeners.RegistrationButtonListener;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;


/**
 * Created by Zach
 */
public class EmailSignupDialog extends ChosenChoiceDialog {

    public EmailSignupDialog(Activity activity, final RegistrationButtonListener listener) {
        super(activity, R.layout.email_signup_dialog);
        final ChosenChoiceDialog mDialog = EmailSignupDialog.this;

        LinearLayout registration = (LinearLayout)mDialog.findViewById(R.id.registration);
        registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mDialog.dismissed) {
                    TrackingManager.getInstance().register(EventConstants.EMAIL);
                    dismiss();
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                listener.goToRegistration();
                            }catch (Throwable throwable){}
                        }
                    },130);

                }
            }
        });

        LinearLayout login = (LinearLayout)mDialog.findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mDialog.dismissed) {
                    TrackingManager.getInstance().register(EventConstants.LOGIN);
                    dismiss();
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                listener.goToLogin();
                            }catch (Throwable throwable){}
                        }
                    },130);

                }
            }
        });
    }
}
