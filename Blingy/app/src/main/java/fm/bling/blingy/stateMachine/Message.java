package fm.bling.blingy.stateMachine;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.inviteFriends.InviteHandler;
import fm.bling.blingy.inviteFriends.listeners.InviteDataReadyListener;
import fm.bling.blingy.inviteFriends.model.ContactInfo;
import fm.bling.blingy.rest.model.CAAPreferredUsers;

import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.widget.CAAWidgetSettingsSingleton;
import fm.bling.blingy.widget.dialogs.ButtonTitleDialog;
import fm.bling.blingy.widget.dialogs.FollowSuggestedDialog;
import fm.bling.blingy.widget.dialogs.InviteSuggestedDialog;
import fm.bling.blingy.widget.dialogs.InviteWidgetBaseDialog;
import fm.bling.blingy.widget.model.CAAInviteWidgetSettingsResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by Chosen-pro on 05/07/2016.
 */

public class Message implements PermissionListener, InviteDataReadyListener {
    private StateMachineEvent.MESSAGE_TYPE messageType;
    private BaseActivity activity;
    private StateMachineMessageListener onReadyListener;
    private InviteWidgetBaseDialog inviteWidgetBaseDialog;
    private ArrayList<User> userList;
    private InviteHandler inviteHandler;

    public Message(BaseActivity a, StateMachineEvent.MESSAGE_TYPE messageType, StateMachineMessageListener onReadyListener) {
        this.activity = a;
        this.messageType = messageType;
        this.onReadyListener = onReadyListener;

        /**
         * Update User data before showing widget
         */
        final CAAUserDataSingleton userDataSingleton = CAAUserDataSingleton.getInstance();
        final CAAWidgetSettingsSingleton widgetSettingsSingleton = CAAWidgetSettingsSingleton.getInstance();
        App.getUrlService().getWidgetSettings(new Callback<CAAInviteWidgetSettingsResponse>() {
            @Override
            public void success(CAAInviteWidgetSettingsResponse widgetResponse, Response response) {
                if (response.getStatus() != 204) {
                    userDataSingleton.setUserId(widgetResponse.getMe().getUserId());
                    userDataSingleton.setFirstName(widgetResponse.getMe().getFirstName());
                    userDataSingleton.setLastName(widgetResponse.getMe().getLastName());
                    userDataSingleton.setPhotoUrl(widgetResponse.getMe().getPhoto());
                    userDataSingleton.setTotalFollowing(widgetResponse.getMe().getTotalFollowing());
                    widgetSettingsSingleton.setSettings(widgetResponse);

                    prepareMessageByType();
                } else {
                    widgetSettingsSingleton.setWidgetOff();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                prepareMessageByType();
            }
        });
    }

    private void prepareMessageByType()
    {
        switch (messageType)
        {
            case INVITE_THIS_FRIEND_EMAIL:
            case INVITE_THIS_FRIEND_SMS:
                activity.getContactsPermissions(this);
                break;
            case SUGGEST_FOLLOW:
                App.getUrlService().getPrefferedUsers(new Callback<CAAPreferredUsers>() {
                    @Override
                    public void success(CAAPreferredUsers caaPreferredUsers, Response response) {
                        userList = caaPreferredUsers.getUsers();
                        if (!userList.isEmpty()) {
                            inviteWidgetBaseDialog = new FollowSuggestedDialog(onReadyListener, activity, messageType, userList);
                            dialogReady();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                    }
                });
                break;
            case YOU_SHOULD_SIGN_UP:
                inviteWidgetBaseDialog = new ButtonTitleDialog(onReadyListener, activity, messageType, "SIGN UP", "Sign up to get the most out of Blin.gy!");
                dialogReady();
                break;
            case TAKE_A_PHOTO:
                inviteWidgetBaseDialog = new ButtonTitleDialog(onReadyListener, activity, messageType, "TAKE A PHOTO", "Take a photo?");
                dialogReady();
                break;
            case TRY_UPLOAD:
                inviteWidgetBaseDialog = new ButtonTitleDialog(onReadyListener, activity, messageType, "UPLOAD", "Taken any snaps lately? Upload them!");
                dialogReady();
                break;
            default:
                break;
        }
    }

    @Override
    public void onPermissionGranted() {
        switch (messageType)
        {
            case INVITE_THIS_FRIEND_SMS:
            case INVITE_THIS_FRIEND_EMAIL:
                inviteHandler = new InviteHandler(activity, this, false);
                break;
            default:
                break;
        }

    }

    @Override
    public void onPermissionDenied() {
        if(inviteWidgetBaseDialog != null)
            inviteWidgetBaseDialog.dismiss();
    }

    @Override
    public void onInviteDataReady()
    {
        final ArrayList<ContactInfo> contacts;
        if (messageType == StateMachineEvent.MESSAGE_TYPE.INVITE_THIS_FRIEND_SMS)
        {
            contacts = inviteHandler.getPhoneContacts();
        }
        else
        {
            contacts = inviteHandler.getEmailContacts();
        }
        inviteHandler = null;

        if (contacts.size() > 0)
        {
            activity.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    inviteWidgetBaseDialog = new InviteSuggestedDialog(onReadyListener, activity, messageType, contacts);
                    dialogReady();
                }
            });
        }
        else
        {
            StateMachine.getInstance(activity.getApplicationContext()).insertEvent(new StateMachineEvent(StateMachineEvent.EVENT_CLASS.STATE_MESSAGE, messageType.ordinal()));
        }
    }

    private void dialogReady()
    {
        if (!CAAWidgetSettingsSingleton.getInstance().isWidgetOn()) {
            onReadyListener.onWidgetReady(null);
        } else {
            StateMachine.getInstance(activity.getApplicationContext()).insertEvent(new StateMachineEvent(StateMachineEvent.EVENT_CLASS.STATE_MESSAGE, messageType.ordinal()));
            onReadyListener.onWidgetReady(inviteWidgetBaseDialog);
        }
    }
}
