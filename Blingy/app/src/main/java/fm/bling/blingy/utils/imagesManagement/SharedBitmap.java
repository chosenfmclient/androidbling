package fm.bling.blingy.utils.imagesManagement;

import android.graphics.Bitmap;

import java.util.ArrayList;

import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
import fm.bling.blingy.utils.imagesManagement.views.URLImageView;


/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 17/07/2016.
 * History:
 * ***********************************
 */
public class SharedBitmap
{
    protected static ArrayList<URLImageView> leftOver = new ArrayList<>();

    private Bitmap wrappedBitmap;
    private boolean isClosed;
    private ArrayList<URLImageView> mImageViews;
    private String imageHashName;

    public SharedBitmap()
    {
        mImageViews = new ArrayList<>();
    }

    public SharedBitmap(URLImageView urlImageView)
    {
        bitmapsCount++;
        mImageViews = new ArrayList<>();
        mImageViews.add(urlImageView);
        imageHashName = urlImageView.getImageHashName();
    }

    public void addURLImageView(URLImageView urlImageView)
    {
        if (!mImageViews.contains(urlImageView))
            mImageViews.add(urlImageView);
    }

    /**
     *
     * @param urlImageView
     * @return true if there is no other urlImageView attached to this SharedBitmap
     */
    public boolean removeURLImageView(URLImageView urlImageView)
    {
        while(mImageViews.contains(urlImageView))
            mImageViews.remove(urlImageView);
        if(mImageViews.size() != 0)
        {
            for(int l = mImageViews.size() - 1; l > -1; l--)
            {
                if(!mImageViews.get(l).isAttached())
                {
                    URLImageView urlImageViewForLeftOver = mImageViews.remove(l);
                    if(leftOver.contains(urlImageViewForLeftOver))
                        leftOver.remove(urlImageViewForLeftOver);
                }
                else if(!leftOver.contains(urlImageView))// if (lis.indexOf(urlImageView.getImageHashName()) == -1)
                    leftOver.add(urlImageView);
            }
        }

        return mImageViews.size() == 0;
    }

    public void setBitmap(Bitmap bitmap)
    {
        wrappedBitmap = bitmap;
        for (URLImageView urlImageView : mImageViews)
        {
//            if(!urlImageView.isAutoShowRecycle() || urlImageView.isAttached())
                urlImageView.onImageLoaded(bitmap);
                urlImageView.invalidate();
        }
    }

    public Bitmap getBitmap()
    {
        return wrappedBitmap;
    }

    public static int bitmapsCount = 0;
    public void recycle()
    {
        bitmapsCount--;
//        if(isClosed) return;
        isClosed = true;

        try{wrappedBitmap.recycle();}catch (Throwable err){}
        wrappedBitmap = null;

        try{mImageViews.clear();}catch (Throwable err){}
        mImageViews = null;
    }

//    public boolean isLocalImage()
//    {
//        return mImageViews.get(0).isLocalImage();
//    }

    public String getUrl()
    {
        return mImageViews.get(0).getUrl();
    }

    public int getMaxWidthInternal()
    {
        return mImageViews.get(0).getMaxWidthInternal();
    }

    public int getWidthInternal()
    {
        return mImageViews.get(0).getWidthInternal();
    }

    public int getWidth()
    {
        return mImageViews.get(0).getWidth();
    }

    public int getHeight()
    {
        return mImageViews.get(0).getHeight();
    }

    public int getBlurRadius()
    {
        return mImageViews.get(0).getBlurRadius();
    }

    public boolean isFixedSize()
    {
        return mImageViews.get(0).isFixedSize();
    }

    public boolean isBlured()
    {
        return mImageViews.get(0).isBlured();
    }

    public boolean isRoundedImage()
    {
        return mImageViews.get(0).isRoundedImage();
    }

    public boolean isDontSaveToFileCache()
    {
        return mImageViews.get(0).isDontSaveToFileCache();
    }

    public boolean hasAlpha()
    {
        return mImageViews.get(0).hasAlpha();
    }

    public boolean hasMajorColorLayer()
    {
        return mImageViews.get(0).hasMajorColorLayer();
    }

    public void setMajorColorLayerColor(int color)
    {
        mImageViews.get(0).setMajorColorLayerColor(color);
    }

    public boolean isClosed()
    {
        return isClosed;
    }

    public float getAspectRatio()
    {
        if(mImageViews.size() > 0 && mImageViews.get(0) instanceof ScaledImageView)
            return ((ScaledImageView)mImageViews.get(0)).getAspectRatio();
        return 1f;
    }

//    static {
//        new Thread(){
//            @Override
//            public void run() {
//                super.run();
//                while (true) {
//                    try {
//                        Thread.sleep(500);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }.start();
//    }
}