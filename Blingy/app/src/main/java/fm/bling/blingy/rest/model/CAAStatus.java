package fm.bling.blingy.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAStatus
 * Description:
 * Created by Dawidowicz Nadav on 6/14/15.
 * History:
 * ***********************************
 */
public class CAAStatus
{
    @SerializedName("status")
    private String status;

    @SerializedName("add_song")
    private boolean addSong = true;

    public CAAStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

//    public boolean shouldAddSong() {
//        return addSong;
//    }

    public void setAddSong(boolean addSong) {
        this.addSong = addSong;
    }
}
