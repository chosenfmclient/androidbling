package fm.bling.blingy.utils.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import fm.bling.blingy.R;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 3/16/16.
 * History:
 * ***********************************
 */
public class CommentShape extends LinearLayout {

    private Paint paint;
    private RectF mRect;
    private boolean isFirst = true;
    private int paddingLeftRight,
                paddingTopBottom,
                width;

    public CommentShape(Context context) {
        super(context);
        preparePaint();
    }

    public CommentShape(Context context, AttributeSet attrs) {
        super(context, attrs);
        preparePaint();
    }

    public CommentShape(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        preparePaint();
    }

    private void preparePaint(){
        setLayerType(LAYER_TYPE_SOFTWARE, null);
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setColor(getResources().getColor(R.color.white));
        mRect = new RectF();
    }


    @Override
    public void draw(Canvas canvas) {
        if (isFirst) {
            mRect.set(0, 0, ((float) canvas.getWidth()), ((float) canvas.getHeight()) - paddingTopBottom);
            width = canvas.getWidth();
            paddingLeftRight = (int) (((float) canvas.getWidth()) * 0.1f);
            paddingTopBottom = (int) (((float) canvas.getHeight()) * 0.1f);
            this.setPadding(getResources().getDimensionPixelSize(R.dimen.padding_small), getResources().getDimensionPixelSize(R.dimen.padding_small), paddingLeftRight, paddingTopBottom);
            requestLayout();
            postInvalidate();
            isFirst = false;
        }
        if(width != canvas.getWidth()) {
            mRect.set(0, 0, ((float) canvas.getWidth()), ((float) canvas.getHeight()) - paddingTopBottom);
            paint.setShadowLayer(5f, 0, 5f, getResources().getColor(R.color.black_30));
        }
        canvas.drawRoundRect(mRect, 100f, 100f, paint);
        super.draw(canvas);
    }
}
