package fm.bling.blingy.splashScreen;

import android.app.NotificationManager;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;


import com.appsee.Appsee;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.opencv.android.OpenCVLoader;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.BuildConfig;
import fm.bling.blingy.ProcessImage;
import fm.bling.blingy.R;
import fm.bling.blingy.database.handler.DataBaseManager;
import fm.bling.blingy.database.handler.DuetInvitedHandler;
import fm.bling.blingy.database.handler.FollowingDataHandler;
import fm.bling.blingy.database.handler.LikesDataHandler;
import fm.bling.blingy.dialogs.AcceptPrivacyDialog;
import fm.bling.blingy.dialogs.messages.NetworkDialog;
import fm.bling.blingy.dialogs.share.model.CAAPostShare;
import fm.bling.blingy.discover.DiscoverActivity;
import fm.bling.blingy.homeScreen.HomeScreenActivity;
import fm.bling.blingy.inviteFriends.InviteFriendsActivity;
import fm.bling.blingy.leaderboard.view.LeaderboardActivity;
//import fm.bling.blingy.mashup.MashupHomeActivity;
import fm.bling.blingy.networkConnectivity.NetworkConnectivityListener;
import fm.bling.blingy.profile.ProfileActivity;
import fm.bling.blingy.record.PickMusicClipActivity;
import fm.bling.blingy.registration.CAAFacebookLogin;
import fm.bling.blingy.registration.CAALogin;
import fm.bling.blingy.registration.CAALoginListener;
import fm.bling.blingy.registration.RegistrationActivity;
import fm.bling.blingy.rest.BRequestInterceptor;
import fm.bling.blingy.rest.model.CAAApiUserDeepLink;
import fm.bling.blingy.rest.model.CAAPushMetric;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.songHome.SongHomeActivity;
import fm.bling.blingy.videoHome.VideoHomeActivity;
import fm.bling.blingy.splashScreen.views.SplashScreenView;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.widget.CAAWidgetSettingsSingleton;
import fm.bling.blingy.widget.model.CAAInviteWidgetSettingsResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * History: Updated by Oren Zakay.
 */
public class SplashScreenActivity extends BaseActivity implements CAALoginListener {

    private String object = "";
    private String object_id = "";
    private String video_id = "";
    private String share_id;
    private boolean loginFinished = false;
    private boolean objectMode = false;
    private boolean deepLinkCallFinished = false;
    private boolean uri = false;
    private boolean mustSendShare = false;
    private boolean gettingDeeplinkObject = false;
    private boolean registrationFlow = false;
    private boolean launchedFirstTimePrivacyDialog;
    private boolean isPush = false;
    private boolean isActivityVisible = false;

    private CallbackManager mCallbackManager;
    private LoginManager loginManager;
    private List<String> permissionNeeds;

    private final String TAG = "SplashScreen";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isOpenApp = true;
        isActivityVisible = true;
        setContentView(new SplashScreenView(this, true));

//        if(!BuildConfig.DEBUG)
        {
            try {
                AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                int middleVolume = maxVolume / 2;
                if (currentVolume < middleVolume) {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, middleVolume, 0);
                }
            } catch (Throwable throwable) {
                if (BuildConfig.DEBUG)
                    Log.e(TAG, throwable.getMessage());
            }
        }

        Appsee.start(getString(R.string.appsee_key));

        SharedPreferencesManager.getEditor().putBoolean(Constants.ADDED_VIDEO_TO_10, false).apply();
        SharedPreferences sharedPreferences = SharedPreferencesManager.getInstance();
        if (sharedPreferences.getBoolean(Constants.FIRST_LAUNCH, true) && sharedPreferences.getString(Constants.ACCOUNT_TYPE, "anonymous").equalsIgnoreCase("anonymous")) {
            registrationFlow = true;
            Log.d("RATE", "FIRST_LAUNCH " + System.currentTimeMillis());
        }

        mCallbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();
        loginManager.registerCallback(mCallbackManager, mCallback);
        permissionNeeds = Arrays.asList("email");

        try {
            CAAUserDataSingleton.getInstance().setAppVersion(getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("NameNotFoundException", e.getMessage());
        }
        if (getIntent().hasExtra("object")) {
            // Yesh push
            postPushMetric();
            object = getIntent().getStringExtra("object");
            if (getIntent().hasExtra("object_id")) {
                object_id = getIntent().getStringExtra("object_id");
            }
            if (getIntent().hasExtra("video_id")) {
                video_id = getIntent().getStringExtra("video_id");
            }

            objectMode = true;
            isPush = true;
        }

        Uri deeplink = getIntent().getData();
        if (deeplink != null) {
            isPush = false;
            parseDeeplinkData(deeplink);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }

        loginUser();

        OpenCVLoader.initDebug();
        ProcessImage.getInstance().initMachineLearning();
    }


    private void postPushMetric() {
        String objectId = getIntent().hasExtra("object_id") ? getIntent().getStringExtra("object_id") : "";
        String object = getIntent().hasExtra("object") ? getIntent().getStringExtra("object") : "";
        String notificationId = getIntent().hasExtra("notification_id") ? getIntent().getStringExtra("notification_id") : "";
        int massNotificationId = getIntent().hasExtra("mass_notification_id") ? Integer.valueOf(getIntent().getStringExtra("mass_notification_id")) : 0;
        String notificationType = getIntent().hasExtra("notification_type") ? getIntent().getStringExtra("notification_type") : "";

        if (!notificationType.equals("")) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(NotificationEnum.getNotificationID(notificationType));
        }

        String text = getIntent().hasExtra("text") ? getIntent().getStringExtra("text") : "";
        String type = "react";

        CAAPushMetric pushMetric;
        if (massNotificationId > 0) {
            pushMetric = new CAAPushMetric(objectId, object, massNotificationId, notificationType, type, text);
        } else {
            pushMetric = new CAAPushMetric(objectId, object, notificationId, notificationType, type, text);
        }

        App.getUrlService().postPushMetric(pushMetric, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void parseDeeplinkData(Uri deeplink) {
        List<String> params = deeplink.getPathSegments();
        if (params.size() > 0) {
            uri = true;
            object = params.get(0);
            if (object.equalsIgnoreCase("m") && params.size() > 1) {
                object = "marketing";
                object_id = params.get(1);
            } else if (object.equalsIgnoreCase("v")) {
                object = "video";
            } else if (object.equalsIgnoreCase("du") || object.equalsIgnoreCase("duet")){
                object = "duet";
            } else if(object.equalsIgnoreCase("d") || object.equalsIgnoreCase("discover")){
                object = "discover";
            } else if (object.equalsIgnoreCase("u")) {
                object = "user";
            }

            if (params.size() > 1 && (object.equalsIgnoreCase("user") || object.equalsIgnoreCase("video") ||
                    object.equalsIgnoreCase("discover") || object.equalsIgnoreCase("duet"))) {
                objectMode = true;
                object_id = params.get(1);
                if (object_id.contains("_")) {
                    String ids[] = object_id.split("_");
                    object_id = ids[0];

                    if (ids.length > 1) {
                        share_id = ids[1];
                    }
                }
            }
        }
        if (share_id == null) {
            share_id = deeplink.getQueryParameter("share_id");
        }

        String page = deeplink.getQueryParameter("page");
        if ((page != null && !page.isEmpty()) &&
                (object == null || object.isEmpty())) {
            object = page;
            objectMode = true;
        }

        if (share_id != null) {
            mustSendShare = true;
            if (loginFinished) {
                postDeeplink(share_id);
            }
        }
    }

    private void loginUser() {
        if (CAALogin.Login(this).equals("")) {
            String accountType = SharedPreferencesManager.getInstance().getString(Constants.ACCOUNT_TYPE, "empty");
            if (accountType.equalsIgnoreCase("facebook"))
                loginManager.logInWithReadPermissions(SplashScreenActivity.this, permissionNeeds);
        }
    }

    public void didFinishLogin(Boolean success, int status) {
        CAALogin.isAuto = "0";
        if (success) {
            BRequestInterceptor.waitingForAccess = false;
            if(!CAAUserDataSingleton.getInstance().getAccessToken().isEmpty()) {
                App.getUrlService().getWidgetSettings(new Callback<CAAInviteWidgetSettingsResponse>() {
                    @Override
                    public void success(CAAInviteWidgetSettingsResponse response, Response response2) {
                        if (response2.getStatus() == 204) {
                            CAAWidgetSettingsSingleton.getInstance().setWidgetOff();
                        } else {
                            CAAWidgetSettingsSingleton.getInstance().setSettings(response);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (isActivityVisible) {
                            if (error != null && error.getKind() == RetrofitError.Kind.NETWORK) {
                                NetworkDialog networkDialog = new NetworkDialog(SplashScreenActivity.this);
                                networkDialog.show();
                            }
                        }
                    }
                });
            }
            LikesDataHandler.getInstance().getAll("");
            FollowingDataHandler.getInstance().getAll("");

//            FollowingDataHandler.getInstance().clear();

            DuetInvitedHandler.getInstance().removeAll();
            getDeeplink();
        }
    }

    private void launchRegistration() {
        Intent i = new Intent(SplashScreenActivity.this, RegistrationActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
        overridePendingTransition(0, 0);
        finish();
    }

    public void launchApp() {
        Intent i = new Intent(SplashScreenActivity.this, HomeScreenActivity.class);
        startActivity(i);
        finish();
    }

    public void launchObject() {
        Intent homeScreenIntent = new Intent(SplashScreenActivity.this, HomeScreenActivity.class);
        if (registrationFlow) homeScreenIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(homeScreenIntent);
        switch (object) {
            case "leaderboard":
                Intent leaderIntent = new Intent(SplashScreenActivity.this, LeaderboardActivity.class);
                stackBuilder.addNextIntent(leaderIntent);
                break;
            case "user":
                Intent resultIntent = new Intent(SplashScreenActivity.this, ProfileActivity.class);
                resultIntent.putExtra(Constants.USER_ID, object_id);
                stackBuilder.addNextIntent(resultIntent);
                break;
            case "duet":
            case "video":
                Intent videoIntent = new Intent(SplashScreenActivity.this, VideoHomeActivity.class);
                videoIntent.putExtra(Constants.VIDEO_ID, object_id);
                if (isFromInvite())
                    videoIntent.putExtra(Constants.FROM_INVITE, true);
                stackBuilder.addNextIntent(videoIntent);
                break;
            case "comment":
                if (!video_id.isEmpty()) {
                    Intent commentVideoIntent = new Intent(SplashScreenActivity.this, VideoHomeActivity.class);
                    commentVideoIntent.putExtra(Constants.VIDEO_ID, video_id);
                    commentVideoIntent.putExtra(Constants.IS_NOTIFICATION, true);
                    stackBuilder.addNextIntent(commentVideoIntent);
                }
                break;
            case "record":
                Intent recordIntent = new Intent(SplashScreenActivity.this, PickMusicClipActivity.class);
                recordIntent.putExtra(Constants.TERM, object_id);
                stackBuilder.addNextIntent(recordIntent);
                break;
            case "discover":
                Intent discoverIntent = new Intent(SplashScreenActivity.this, SongHomeActivity.class);
                discoverIntent.putExtra(Constants.IS_SONG_HOME, true);
                discoverIntent.putExtra(Constants.CLIP_ID, object_id);
                stackBuilder.addNextIntent(discoverIntent);
                break;
            case "invite":
                stackBuilder.addNextIntent(new Intent(SplashScreenActivity.this, InviteFriendsActivity.class));
                break;
            default:
                break;
        }

        if (registrationFlow) {
            Intent registrationIntent = new Intent(SplashScreenActivity.this, RegistrationActivity.class);
            registrationIntent.putExtra("deeplink", true);
            if (object.equals("duet"))
                registrationIntent.putExtra("duet", true);

            registrationIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            stackBuilder.addNextIntent(registrationIntent);
        }

        if (registrationFlow)
            overridePendingTransition(0, 0);

        for (Intent i : stackBuilder.getIntents()) {
            i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        }
        stackBuilder.startActivities();
        finish();
    }

    public void postDeeplink(final String shareId) {
        App.getUrlService().postDeeplink(new CAAPostShare("", shareId, ""), new Callback<String>() {
            @Override
            public void success(String string, Response response) {
                deepLinkCallFinished = true;
            }

            @Override
            public void failure(RetrofitError error) {
                deepLinkCallFinished = true;
                if (error != null && error.getKind() == RetrofitError.Kind.NETWORK) {
                    NetworkDialog networkDialog = new NetworkDialog(SplashScreenActivity.this);
                    networkDialog.show();
                }
            }
        });
    }

    public void getDeeplink() {
        if (object.equalsIgnoreCase("marketing")) {
            gettingDeeplinkObject = true;
            getMarketingDeeplink();
        } else if (uri) {
            if (mustSendShare) {
                postDeeplink(share_id);
            }
            deepLinkCallFinished = true;
            launch();
        }

        if (!gettingDeeplinkObject && !CAAUserDataSingleton.getInstance().getAccessToken().isEmpty()) {
            App.getUrlService().getSpecificUserDeepLink(Constants.ME, new Callback<CAAApiUserDeepLink>() {
                @Override
                public void success(CAAApiUserDeepLink deepLink, Response response) {
                    if (!uri) {
                        if (response.getStatus() != HttpURLConnection.HTTP_NO_CONTENT) {
                            object = deepLink.getType();
                            if (object == null || object.isEmpty()) {
                                if (deepLink.getPage() != null && !deepLink.getPage().isEmpty()) {
                                    object = deepLink.getPage();
                                    objectMode = true;
                                }
                            }
                            object_id = deepLink.getObjectId();
                            objectMode = true;

                            if (deepLink.getShareId() != null) {
                                postDeeplink(deepLink.getShareId());
                            }
                        }
                        deepLinkCallFinished = true;
                        launch();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    deepLinkCallFinished = true;
                    launch();

                }
            });
        }
        else{
            if(object.isEmpty()) {
                deepLinkCallFinished = true;
                launch();
            }
        }
    }

    public void launch() {
        if (SplashScreenView.videoFinished && deepLinkCallFinished) {
            boolean isPrivacyShown = SharedPreferencesManager.getInstance().getBoolean(Constants.PRIVACY_SHOWN, false);
            if (!isPrivacyShown) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AcceptPrivacyDialog acceptPrivacyDialog = new AcceptPrivacyDialog(SplashScreenActivity.this);
                        acceptPrivacyDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                new Thread() {
                                    @Override
                                    public void run() {

                                        if (objectMode || isPush) {
                                            launchObject();
                                        } else if (registrationFlow) {
                                            launchRegistration();
                                        } else {
                                            launchApp();
                                        }
                                    }
                                }.start();

                                launchedFirstTimePrivacyDialog = false;
                            }
                        });
                        launchedFirstTimePrivacyDialog = true;
                        if (isActivityVisible)
                            acceptPrivacyDialog.show();
                    }
                });
            } else {
                if (NetworkConnectivityListener.isConnected()) {
                    new Thread() {
                        @Override
                        public void run() {
                            if (objectMode || isPush) {
                                launchObject();
                            } else if (registrationFlow) {
                                launchRegistration();
                            } else {
                                launchApp();
                            }
                        }
                    }.start();
                }
            }
        }
    }

    private void getMarketingDeeplink() {
        App.getUrlService().getMarketingDeeplink(object_id, new Callback<CAAApiUserDeepLink>() {
            @Override
            public void success(CAAApiUserDeepLink caaApiUserDeepLink, Response response) {
                if (caaApiUserDeepLink != null) {
                    object = caaApiUserDeepLink.getType();
                    object_id = caaApiUserDeepLink.getObjectId();

                    objectMode = true;
                    deepLinkCallFinished = true;
                    if (caaApiUserDeepLink.getShareId() != null && !caaApiUserDeepLink.getShareId().isEmpty()) {
                        postDeeplink(caaApiUserDeepLink.getShareId());
                    }
                } else {
                    objectMode = false;
                    deepLinkCallFinished = true;
                }
                launch();
            }

            @Override
            public void failure(RetrofitError error) {
                deepLinkCallFinished = true;
                objectMode = false;
                launch();
            }
        });
    }

    private FacebookCallback<LoginResult> mCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            CAAFacebookLogin.updateFacebookUserSingleton(loginResult.getAccessToken());
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {
            //TODO show error to user
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        deepLinkCallFinished = true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SplashScreenView.videoFinished = false;
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        isActivityVisible = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        isActivityVisible = false;
        if (!launchedFirstTimePrivacyDialog) {
            SplashScreenView.videoFinished = false;
            finish();
        }
    }

    public boolean isFromInvite() {
        return getIntent().hasExtra("type") &&
                getIntent().getStringExtra("type").equals("cameo_invite") ||
                object.equalsIgnoreCase("duet");
    }
}