package fm.bling.blingy.dialogs.messages;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;

import org.opencv.R;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 05/01/16.
 * History:
 * ***********************************
 */
public class HelpUsFixItDialogII extends Dialog
{
    private TableLayout main;
//    private Context mContext;
    private TextView title;
//    private TextView content;
    private Button iAgreeButton;
    private View.OnClickListener mOcl;

    public HelpUsFixItDialogII(Context context, View.OnClickListener ocl)
    {
        super(context, android.R.style.Theme_Panel);
//        mContext = context;
        mOcl = ocl;

//        int padding = context.getResources().getDimensionPixelSize(R.dimen.blocks_margin_card_side);
        int padding = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, 19, getContext().getResources().getDisplayMetrics() );

        title = new TextView(context);
        title.setLayoutParams(new ViewGroup.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
//        title.setText("Camera is not supported");
        title.setText("uh oh,out of memory. Help us fix it !");
        title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 17 );
        title.setTextColor(Color.BLACK);
        title.setPadding(0, 0, 0, padding);
        title.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);
        title.setGravity(Gravity.CENTER);
        title.setIncludeFontPadding(false);

//        content = new TextView(context);
//        content.setLayoutParams(new ViewGroup.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
//        content.setText("Your camera is not supported yet, please help us fix it by sending a report to our desk.");
//        content.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
//        content.setTextColor(Color.BLACK);
//        content.setPadding(0, 0, 0, padding);
//        content.setGravity(Gravity.LEFT);
//        content.setIncludeFontPadding(false);
//        content.setMovementMethod(LinkMovementMethod.getInstance());
//        content.setHighlightColor(Color.TRANSPARENT);

//        SpannableStringBuilder ssb = (SpannableStringBuilder) content.getEditableText();
//        String str = ssb.toString();
//
//        int ind;
//        ssb.setSpan(new ClickableSpanExtension()
//        {
//            @Override
//            public void onClick(View widget)
//            {
//                startTermsOfService();
//            }
//        }, ind = str.indexOf("Terms of Service"), ind + "Terms of Service".length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
//        ssb.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.main_blue)), str.indexOf("Terms of Service"), ind + "Terms of Service".length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//        ssb.setSpan(new ClickableSpanExtension()
//        {
//            @Override
//            public void onClick(View widget)
//            {
//                startPrivacy();
//            }
//        }, ind = str.indexOf("Privacy Policy"), ind + "Privacy Policy".length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
//        ssb.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.main_blue)), str.indexOf("Privacy Policy"), ind + "Privacy Policy".length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);

        iAgreeButton = new Button(context);
        iAgreeButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        iAgreeButton.setGravity(Gravity.CENTER);
        iAgreeButton.setText("Send");
        iAgreeButton.setBackgroundResource(R.drawable.button_dialog_blue_ocv);//Color(mContext.getResources().getColor(R.color.main_blue));
        iAgreeButton.setTextColor(Color.WHITE);
//        iAgreeButton.setPadding(padding * 3, 0, padding*3, 0);
        iAgreeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mOcl.onClick(null);
                dismiss();
            }
        });

        main = new TableLayout(context);
        main.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        main.setBackgroundColor(Color.WHITE);
        main.setPadding(padding, padding, padding, padding);
        main.addView(title);
//        main.addView(content);
        main.addView(iAgreeButton);
        main.setGravity(Gravity.CENTER);

        setContentView(main);
    }

//    private void startTermsOfService()
//    {
//        Intent intent = new Intent(mContext, WebViewActivity.class);
//        intent.putExtra("title", "Terms Of Service");
//        intent.putExtra("url", mContext.getResources().getString(R.string.terms_url));
//        mContext.startActivity(intent);
//    }
//
//    private void startPrivacy()
//    {
//        Intent intent = new Intent(mContext, WebViewActivity.class);
//        intent.putExtra("title", "Privacy Policy");
//        intent.putExtra("url", mContext.getResources().getString(R.string.privacy_url));
//        mContext.startActivity(intent);
//    }

//    @Override
//    public void onBackPressed()
//    {
//        super.onBackPressed();
//        System.exit(0);
//    }

}