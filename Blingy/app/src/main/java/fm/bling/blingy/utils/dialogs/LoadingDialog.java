package fm.bling.blingy.utils.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.text.TextPaint;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.utils.graphics.WhiteLoadingPlaceHolder;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/16/16.
 * History:
 * ***********************************
 */
public class LoadingDialog extends Dialog
{
    protected FrameLayout mainLayout;
    protected WhiteLoadingPlaceHolder loadingPlaceHolder;
    protected Context mContext;
    protected final String mTitle;
    protected TextPaint textPaint;
    protected int tittleLocX, tittleLocY;

//    private int savedFlags;

    protected int widthHeight = (int) (205f * App.SCALE_X);

    public LoadingDialog(Context context, boolean hideStausBar, boolean hideNavigationBar)
    {
        super(context, hideStausBar ? R.style.FullScreen : android.R.style.Theme_Panel);
        mContext = context;
        mTitle = null;
        init(hideNavigationBar);
    }

    public LoadingDialog(Context context, String title, boolean hideStausBar, boolean hideNavigationBar)
    {
        super(context, hideStausBar ? R.style.FullScreen : android.R.style.Theme_Panel);
        mContext = context;

        if(App.HEIGHT == 0)
        {
            App.HEIGHT = mContext.getResources().getDisplayMetrics().heightPixels;
            App.SCALE_Y =((float)App.HEIGHT) / 1920f;
        }

        if(widthHeight == 0)
        {
            widthHeight = (int) (250f * App.SCALE_X); // portrait 1920 X 1080
        }

        if(title != null)
        {
            mTitle = title;
            textPaint = new TextPaint();
            textPaint.setTextSize(mContext.getResources().getDimensionPixelSize(R.dimen.font_title_extra_large));
            textPaint.setColor(Color.WHITE);
            textPaint.setAntiAlias(true);
            textPaint.setTypeface(App.ROBOTO_REGULAR);
            Rect rect = new Rect();
            textPaint.getTextBounds(mTitle, 0, mTitle.length(), rect);
            tittleLocX = (App.WIDTH - rect.width()) / 2;
            int padding = (int) (100f * App.SCALE_Y);
            tittleLocY = ((App.HEIGHT + rect.height()) / 2) + (widthHeight/2) + padding;
        }
        else mTitle = null;
        init(hideNavigationBar);
    }

    protected void init(boolean hideNavigationBar)
    {
        if(widthHeight == 0) {
            widthHeight = (int) (250f * App.SCALE_X); // portrait 1920 X 1080
        }

        loadingPlaceHolder = new WhiteLoadingPlaceHolder(mContext, widthHeight, widthHeight,
                (App.WIDTH - widthHeight) / 2, (App.HEIGHT - widthHeight) / 2);

        mainLayout = new FrameLayout(mContext)
        {
            public void draw(Canvas canvas)
            {
                super.draw(canvas);
                try
                {
                    if(mTitle != null)
                        canvas.drawText(mTitle, tittleLocX, tittleLocY, textPaint);

                    loadingPlaceHolder.draw(canvas);
                }
                catch (Throwable err) {}
            }
        };

        mainLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mainLayout.setVisibility(View.INVISIBLE);
        mainLayout.setBackgroundColor(mContext.getResources().getColor(R.color.black_20));
        loadingPlaceHolder.attachView(mainLayout);
        setCancelable(false);
        setContentView(mainLayout);
        if(hideNavigationBar)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        if(mContext != null)
            show();
    }

    public void show()
    {
        super.show();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.TOP | Gravity.LEFT;
//        lp.flags = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        getWindow().setAttributes(lp);

        Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        anim.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {

            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                mainLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {

            }
        });
        mainLayout.startAnimation(anim);
        //mainLayout.startLayoutAnimation();
        anim.startNow();
    }

    protected void superShow()
    {
        if(mContext != null)
            super.show();
    }

    public void dismiss()
    {
        if (mContext != null && mainLayout != null)
        {
            Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
            mContext = null;
            anim.setAnimationListener(new Animation.AnimationListener()
            {
                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation)
                {
                    mainLayout.setVisibility(View.INVISIBLE);
                    superDismiss();
                }

                @Override
                public void onAnimationRepeat(Animation animation) { }
            });
            mainLayout.startAnimation(anim);
        }
        else
        {
            superDismiss();
        }
    }

    public void superDismiss()
    {
        if (isShowing())
            try
            {
                super.dismiss();
            } catch (Throwable err) { }
    }

    public void onStop()
    {
        super.onStop();
        close();
    }

    public void close()
    {
        mContext = null;
        try
        {
            loadingPlaceHolder.detachView(mainLayout);
        }
        catch (Throwable err) { }
        mainLayout = null;
        try
        {
            loadingPlaceHolder.close();
        }
        catch (Throwable err) { }
        loadingPlaceHolder = null;
    }

}