package fm.bling.blingy.videoHome.fragments;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class CAAFeatured implements Serializable {


    @SerializedName("object")
    private JsonElement object;

    @SerializedName("image_url")
    private String imageUrl;

    @SerializedName("type")
    private String type;

    @SerializedName("home_tile_id")
    private String tileId;

    private ArrayList<CAAFeatured> featured;

    public String getTileId() {
        return tileId;
    }

//    public void setTileId(String tileId) {
//        this.tileId = tileId;
//    }

    public ArrayList<CAAFeatured> getFeatured() {
        return featured;
    }

//    public void setFeatured(ArrayList<CAAFeatured> featured) {
//        this.featured = featured;
//    }

    public String getType() {
        return type;
    }

//    public void setType(String type) {
//        this.type = type;
//    }

    public String getImageUrl() {
        return imageUrl;
    }

//    public void setImageUrl(String imageUrl) {
//        this.imageUrl = imageUrl;
//    }

    public JsonElement getObject() {
        return object;
    }

//    public void setObject(JsonElement object) {
//        this.object = object;
//    }

}
