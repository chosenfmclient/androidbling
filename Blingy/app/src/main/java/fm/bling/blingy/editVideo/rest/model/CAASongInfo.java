package fm.bling.blingy.editVideo.rest.model;

import com.google.gson.annotations.SerializedName;

import java.net.URL;

public class CAASongInfo
{
    @SerializedName("song_id")
    private String songId;

    @SerializedName("artist_name")
    private String artistName;

    @SerializedName("song_name")
    private String songName;

    @SerializedName("genre")
    private String genre;

    @SerializedName("lyrics_url")
    private URL lyricsUrl;

    @SerializedName("song_url")
    private URL songUrl;

    @SerializedName("accuracy_url")
    private URL accuracyUrl;

    @SerializedName("is_new")
    private int isNew;

    public int getIsNew() {
        return isNew;
    }

//    public void setIsNew(int isNew) {
//        this.isNew = isNew;
//    }

    public String getSongId() {
        return songId;
    }

//    public void setSongId(String songId) {
//        this.songId = songId;
//    }

    public String getArtistName() {
        return artistName;
    }

//    public void setArtistName(String artistName) {
//        this.artistName = artistName;
//    }

    public String getSongName() {
        return songName;
    }

//    public void setSongName(String songName) {
//        this.songName = songName;
//    }

    public String getGenre() {
        return genre;
    }

//    public void setGenre(String genre) {
//        this.genre = genre;
//    }

    public URL getLyricsUrl() {
        return lyricsUrl;
    }

//    public void setLyricsUrl(URL lyricsUrl) {
//        this.lyricsUrl = lyricsUrl;
//    }

    public URL getSongUrl() {
        return songUrl;
    }

//    public void setSongUrl(URL songUrl) {
//        this.songUrl = songUrl;
//    }

    public URL getAccuracyUrl() {
        return accuracyUrl;
    }

//    public void setAccuracyUrl(URL accuracyUrl) {
//        this.accuracyUrl = accuracyUrl;
//    }
}
