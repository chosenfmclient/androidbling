package fm.bling.blingy.upload.adapters;

import android.app.Activity;
import android.graphics.Rect;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.record.utils.GridSpacingItemDecoration;
import fm.bling.blingy.upload.FileManager;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 08/07/2016.
 * History:
 * ***********************************
 */
public class PickPhotoVideoVPAdapter extends PagerAdapter
{
    private int columnPadding = (int) (20f * App.SCALE_Y);

    private Activity mActivity;
    private static FileManager photoFileManager, videoFileManager;
    private PickPhotoVideoRVAdapter.onFolderSelectedListener mOnFolderSelectedListener;
    private GridSpacingItemDecoration mGridSpacingItemDecoration, mFolderGridSpacingItemDecoration;
    private ArrayList<PickPhotoVideoRVAdapter>pickPhotoVideoRVAdapters;

    public PickPhotoVideoVPAdapter(Activity activity, PickPhotoVideoRVAdapter.onFolderSelectedListener onFolderSelectedListener)
    {
        mActivity = activity;
        mOnFolderSelectedListener = onFolderSelectedListener;
        mGridSpacingItemDecoration = new GridSpacingItemDecoration(2, columnPadding / 2, false);
        mFolderGridSpacingItemDecoration = new GridSpacingItemDecoration(3, columnPadding / 3, false);
        pickPhotoVideoRVAdapters = new ArrayList<>();
    }

    @Override
    public int getCount()
    {
        return 1;
    }

    @Override
    public boolean isViewFromObject(View view, Object object)
    {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
//        switch(position)
//        {
//           case 0: return "PHOTO";
//           case 1:
        return "VIDEO";
//        }
//        return "PARKING TITLE";//data.get(position).getTabName();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
        RecyclerView recyclerView = new RecyclerView(container.getContext());
        recyclerView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        GridLayoutManager gridLayoutManager = new GridLayoutManager(container.getContext(), 2);
        gridLayoutManager.setRecycleChildrenOnDetach(false);
        recyclerView.addItemDecoration(mGridSpacingItemDecoration);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);
//        if(position == 0)
//        {
//            pickPhotoVideoRVAdapters.add(new PickPhotoVideoRVAdapter(mActivity, photoFileManager, mOnFolderSelectedListener));
//            recyclerView.setAdapter(pickPhotoVideoRVAdapters.get(pickPhotoVideoRVAdapters.size() - 1));
//        }
//        else if(position == 1)
//        {
            pickPhotoVideoRVAdapters.add(new PickPhotoVideoRVAdapter(mActivity, videoFileManager, mOnFolderSelectedListener));
            recyclerView.setAdapter(pickPhotoVideoRVAdapters.get(pickPhotoVideoRVAdapters.size() - 1));
//        }
        container.addView(recyclerView);
        return recyclerView;
    }

    private static boolean isProcessingPhotos = true, isProcessingVideos = true;
    public static void initializeFileManager()
    {
//        if(photoFileManager == null)
//        {
//            isProcessingPhotos = true;
//            new Thread()
//            {
//                public void run()
//                {
//                    photoFileManager = new FileManager(FileManager.PHOTO);
//                    isProcessingPhotos = false;
//                }
//            }.start();
//        }
        if(videoFileManager == null)
        {
            isProcessingVideos = true;

                new Thread() {
                    public void run() {
                        videoFileManager = new FileManager(FileManager.VIDEO);
                        isProcessingVideos = false;
                    }
                }.start();
        }
    }

    public static boolean isProcessingPhotos()
    {
        return false;
    }

    public static boolean isProcessingVideos()
    {
        return isProcessingVideos;
    }

    public GridSpacingItemDecoration getFolderGridSpacingItemDecoration()
    {
        return mFolderGridSpacingItemDecoration;
    }

//    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration
//    {
//
//        private int spanCount;
//        private int spacing;
//        private boolean includeEdge;
//
//        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge)
//        {
//            this.spanCount = spanCount;
//            this.spacing = spacing;
//            this.includeEdge = includeEdge;
//        }
//
//        @Override
//        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
//        {
//            int position = parent.getChildAdapterPosition(view); // item position
//            int column = position % spanCount; // item column
//
//            if(column>0)
//                outRect.left = spacing;// / spanCount; // column * ((1f / spanCount) * spacing)
//            if (position >= spanCount)
//            {
//                outRect.top = spacing; // item top
//            }
//        }
//    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {}

    public void close()
    {
        mActivity = null;
        mOnFolderSelectedListener = null;
        mGridSpacingItemDecoration = null;
        mFolderGridSpacingItemDecoration = null;
        for(PickPhotoVideoRVAdapter pickPhotoVideoRVAdapter:pickPhotoVideoRVAdapters)
            try{pickPhotoVideoRVAdapter.close();}catch (Throwable err){}
    }

}