package fm.bling.blingy.record.audio;

import android.content.Context;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.audiofx.NoiseSuppressor;
import android.os.SystemClock;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import fm.bling.blingy.App;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.utils.FFMPEG;
import fm.bling.blingy.utils.Sonic;

/**
 * Created by ben on 06/06/2016.
 */
public class AudioRecorder
{
    private final int SAMPLE_RATE = 44100, CHANNEL_CONFIG = android.media.AudioFormat.CHANNEL_CONFIGURATION_MONO, AUDIO_FORMAT = android.media.AudioFormat.ENCODING_PCM_16BIT;
    private final int minimumBufferSize;
    private AudioRecord audioRecord;
    private boolean isRecording, isDoneReadingData;
    private String mOutputFileName, internalOutputFileName, tempOutputFileName, internalOutputFastFileName, internalOutputSlowFileName;
    private DataOutputStream outputStream;
    private String mSoundTrackUrl;
    private long pausedTime;
    private boolean soundTrackSet;
    private OnDoneDownloadingListener mOnDoneDownloadingListener;
    private Context mContext;
    private int numOfActionsBeforeProceed, counterOfActions = 0;
    public String framePath;
    public String framesFilesPaths;
    boolean isClosed = false;
//    boolean isInterupted=

    boolean isUseLastCreatedFile = false;

    public AudioRecorder(Context context, String outputFileName, String soundTrackUrl, boolean isUseLastCreatedFile, OnDoneDownloadingListener onDoneDownloadingListener)
    {
        mOutputFileName = outputFileName;
        mOnDoneDownloadingListener = onDoneDownloadingListener;
        mContext = context;
        minimumBufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, CHANNEL_CONFIG, AUDIO_FORMAT);

        this.isUseLastCreatedFile = isUseLastCreatedFile;

        if ((mSoundTrackUrl = soundTrackUrl) == null)
        {
            try
            {
                outputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(mOutputFileName)));
            }
            catch (Throwable err)
            {
                err.printStackTrace();
                throw new RuntimeException();
            }
            audioRecord = new AudioRecord(MediaRecorder.AudioSource.CAMCORDER,
                    SAMPLE_RATE, CHANNEL_CONFIG, AUDIO_FORMAT, minimumBufferSize);
            NoiseSuppressor.create(audioRecord.getAudioSessionId());
        }
        else
        {
            if (!new File(App.VIDEO_WORKING_FOLDER + "audio/").exists())
                new File(App.VIDEO_WORKING_FOLDER + "audio/").mkdir();
            if (!new File(App.VIDEO_WORKING_FOLDER + "video/").exists())
                new File(App.VIDEO_WORKING_FOLDER + "video/").mkdir();
            internalOutputFileName = App.VIDEO_WORKING_FOLDER + "audio/outputR.wav";
            internalOutputFastFileName = App.VIDEO_WORKING_FOLDER + "audio/outputF.wav";
            internalOutputSlowFileName = App.VIDEO_WORKING_FOLDER + "audio/outputS.wav";
            tempOutputFileName = App.VIDEO_WORKING_FOLDER + "audio/temp_output1.wav";

            framesFilesPaths = App.VIDEO_WORKING_FOLDER+ "video/frames/";
            if (!new File(framesFilesPaths).exists())
                new File(framesFilesPaths).mkdirs();
            framePath = framesFilesPaths + "frame%04d.jpg";

            soundTrackSet = true;

            saveSongFromUrlToFile();
        }
    }

    public void start() throws Throwable
    {
        if (soundTrackSet)
        {
            if (pausedTime != 0)
            {
                pausedTime = SystemClock.elapsedRealtime() - pausedTime;
                pausedTime = 0;
            }
        }
        else
        {
            audioRecord.startRecording();
            new Thread(new RecordAudioRunnable()).start();
        }
        isRecording = true;
    }

    public void pause()
    {
        isRecording = false;
        pausedTime = SystemClock.elapsedRealtime();
    }

    public void stop()
    {
        isRecording = false;
        if (!soundTrackSet)
        {
            while (!isDoneReadingData)
            {
                try
                {
                    Thread.sleep(50);
                }
                catch (Throwable err)
                {
                }
            }
            try
            {
                outputStream.flush();
                outputStream.close();
                outputStream = null;
            }
            catch (Throwable err)
            {
            }
        }
        else
        {
            if (pausedTime != 0)
            {
                pausedTime = SystemClock.elapsedRealtime() - pausedTime;
            }
        }
    }

    public String getOutputFilePath()
    {
        return mOutputFileName;
    }


    String videoFileName;// = App.VIDEO_WORKING_FOLDER + "video/" + mSoundTrackUrl.hashCode() + ".mp4";
    private static Thread cachingThread;
    private static boolean isCachingThreadRunning;
    private boolean isFromCaching;
    private void saveSongFromUrlToFile()
    {
//        numOfActionsBeforeProceed = 3;//3 actions - 1 pitch, 2 fast track, 3 slow track
        videoFileName = App.VIDEO_WORKING_FOLDER + "video/" + mSoundTrackUrl.hashCode() + ".mp4";
        isFromCaching = new File(videoFileName).exists();

        if(isFromCaching)
        {
            new Thread()
            {
                public void run()
                {
                    try
                    {
                        HttpURLConnection conn = (HttpURLConnection) new URL(mSoundTrackUrl).openConnection();
                        int contentLength = conn.getContentLength();
                        isFromCaching &= (contentLength == new File(videoFileName).length());
                    }
                    catch (Throwable err) {}
                    saveSongFromUrlToFileII();
                }
            }.start();
        }
        else saveSongFromUrlToFileII();
    }

    private void saveSongFromUrlToFileII()
    {
        if(isClosed) return;

        if(cachingThread != null)
        {
//            cachingThread.interrupt();
            new Thread()
            {
                public void run()
                {
                    while(isCachingThreadRunning)
                        try{sleep(100);}catch (Throwable err){}
                    initClip();
                }
            }.start();
        }
        else initClip();
    }

    private boolean canRunBot;
//    private boolean canRunBotII;
    private int lastProgress;
    private void updateProgressBot(int current, final int count, final long time)
    {
        canRunBot = true;
        lastProgress = current;
        new Thread()
        {
            public void run()
            {
                long sleepTime = time / count;
                for(int i = 0; i < count && canRunBot; i++)
                {
                    lastProgress ++;
                    mOnDoneDownloadingListener.updateProgress(lastProgress);
                    try{Thread.sleep(sleepTime);}catch (Throwable err){}
                    if(i == 5 && canRunBot)
                        mOnDoneDownloadingListener.onFirstFrameReady();
                }
            }
        }.start();
    }

//    private void updateProgressBotII(final long time)
//    {
//        canRunBot = false;
//        canRunBotII = true;
//        new Thread()
//        {
//            public void run()
//            {
//                int count = 100 - lastProgress;
//                long sleepTime = time / count;
//
//                for(int i = 0; i < count && canRunBotII; i++)
//                {
//                    lastProgress ++;
//                    mOnDoneDownloadingListener.updateProgress(100 + lastProgress);
//                    try{Thread.sleep(sleepTime);}catch (Throwable err){}
//                    if(i == 5 && canRunBot)
//                        mOnDoneDownloadingListener.onFirstFrameReady();
//                }
//            }
//        }.start();
//    }

//    public final int NOT_ENOUGH_SPACE = -1;
    /**
     * 0 for no errors
     */
    public int errorCode = 0;
    /**
     * initialized with "Not assigned"
     */
    public String errorMessage = "Not assigned";
    private void initClip()
    {
        errorCode = 0;
        errorMessage = "Not assigned";

        if(isFromCaching)
        {
            cachingThread = new Thread()
            {
                public void run()
                {
                    super.run();
                    isCachingThreadRunning = true;
                    try
                    {
//                        DataInputStream dataInputStream = new DataInputStream(new FileInputStream(videoFileName));
//
//                        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(mOutputFileName));
//
//                        byte[] buffer = new byte[1024 * 50]; // 50 kb
//                        int count;
//
//                        while (!isClosed)
//                        {
//                            count = dataInputStream.read(buffer);
//                            if (count == -1)
//                                break;
//                            dataOutputStream.write(buffer, 0, count);
//                        }
//
//                        dataOutputStream.flush();
//                        dataOutputStream.close();
//                        dataInputStream.close();

                        if(isClosed)
                        {
                            isCachingThreadRunning = false;
                            return;
                        }

                        internalOutputFileName = tempOutputFileName;

                        if(FFMPEG.lastOpenedTrack != mSoundTrackUrl.hashCode())
                        {
                            if (new File(tempOutputFileName).exists())
                                new File(tempOutputFileName).delete();
                            long loadingTime = SharedPreferencesManager.getInstance().getLong("loadingTime", 45000);
                            long timeStarted = SystemClock.elapsedRealtime();
                            updateProgressBot(0, 100, loadingTime);
                            deleteExistingFrames();
                            String tempMp3File = App.VIDEO_WORKING_FOLDER + "tempMp3.mp3";
                            String tempFilePath = App.VIDEO_WORKING_FOLDER + "video/" + mSoundTrackUrl.hashCode() + ".mp4";//framePath +"movie.mp4";

                            if(isClosed)
                            {
                                isCachingThreadRunning = false;
                                return;
                            }

//                            long timeStarted =  SystemClock.elapsedRealtime();
                            errorCode = FFMPEG.getInstance(mContext).splitAudioAndVideo(tempFilePath, tempMp3File, framePath, FFMPEG.getInstance(mContext).getIsCamera2(), isUseLastCreatedFile);//convertMp3ToPCM(mOutputFileName, tempOutputFileName);

//                            if(isConversionWentWrong())
//                            {
//                                deleteExistingFrames();
//                                errorCode = FFMPEG.getInstance(mContext).splitAudioAndVideo(tempFilePath, tempMp3File, framePath, FFMPEG.getInstance(mContext).getIsCamera2());//convertMp3ToPCM(mOutputFileName, tempOutputFileName);
//                            }

                            if(isClosed || isErrorOccurred(errorCode))
                            {
                                isCachingThreadRunning = false;
                                return;
                            }

//                            updateProgressBotII(timeDif/10);

                            errorCode = FFMPEG.getInstance(mContext).convertMp3ToPCM(tempMp3File, tempOutputFileName);


                            if(isErrorOccurred(errorCode)) return;

                            FFMPEG.lastOpenedTrack = mSoundTrackUrl.hashCode();
                            internalOutputFileName = tempOutputFileName;

                            long timeDif = SystemClock.elapsedRealtime() - timeStarted;
                            SharedPreferencesManager.getEditor().putLong("loadingTime", timeDif);
                            SharedPreferencesManager.getEditor().apply();

                            initTracks();
                        }
                        else proceed();
                    }
                    catch (Throwable err)
                    {
                        err.printStackTrace();
//                        throw new RuntimeException(err);
                    }
                    isCachingThreadRunning = false;

                }
            };
            cachingThread.start();
        }
        else
        {
            cachingThread = new Thread()
            {
                public void run()
                {
                    super.run();
                    isCachingThreadRunning = true;
                    try
                    {
                        URL url = new URL(mSoundTrackUrl);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setConnectTimeout(20000);
                        conn.setReadTimeout(20000);
                        conn.setInstanceFollowRedirects(true);

                        final String tempFilePath = App.VIDEO_WORKING_FOLDER + "video/" + mSoundTrackUrl.hashCode() + ".mp4";//framePath +"movie.mp4";

                        DataInputStream dataInputStream = new DataInputStream(conn.getInputStream());
//                        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(mOutputFileName));
//                        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(tempFilePath));
//                        DataOutputStream cachingStream = new DataOutputStream(new FileOutputStream(App.VIDEO_WORKING_FOLDER + "audio/" + mSoundTrackUrl.hashCode() + ".mp3"));
                        DataOutputStream cachingStream = new DataOutputStream(new FileOutputStream(tempFilePath));//App.VIDEO_WORKING_FOLDER + "video/" + mSoundTrackUrl.hashCode() + ".mp4"));

                        byte[] buffer = new byte[1024 * 50]; // 50 kb
                        int count;

                        int length = conn.getContentLength();
                        int sum = 0;

                        while (true)
                        {
                            count = dataInputStream.read(buffer);
                            if (count == -1)
                            {
                                mOnDoneDownloadingListener.updateProgress(50);
                                break;
                            }
                            sum += count;
                            mOnDoneDownloadingListener.updateProgress(sum * 50 / length);
                            cachingStream.write(buffer, 0, count);
                        }

                        cachingStream.flush();
                        cachingStream.close();
                        dataInputStream.close();
                        if (isClosed)
                        {
                            isCachingThreadRunning = false;
                            return;
                        }

                        if (new File(tempOutputFileName).exists())
                            new File(tempOutputFileName).delete();

//                        new Thread(){
//                            @Override
//                            public void run() {
//                                try {
//                                    String tempMp3File = App.VIDEO_WORKING_FOLDER + "tempMp3.mp3";
//                                    FFMPEG.getInstance(mContext).splitAudioAndVideo(tempFilePath, tempMp3File, framePath, 25);//convertMp3ToPCM(mOutputFileName, tempOutputFileName);
//                                    FFMPEG.getInstance(mContext).convertMp3ToPCM(tempMp3File, tempOutputFileName);
//
//                                    internalOutputFileName = tempOutputFileName;
//                                    proceed();
//                                }catch (Throwable throwable){
//                                    throwable.printStackTrace();
//                                }
//                            }
//                        }.start();
                        long loadingTime = SharedPreferencesManager.getInstance().getLong("loadingTime", 45000);
                        long timeStarted = SystemClock.elapsedRealtime();
                        updateProgressBot(50, 50, loadingTime);

                        deleteExistingFrames();
                        String tempMp3File = App.VIDEO_WORKING_FOLDER + "tempMp3.mp3";

                        errorCode = FFMPEG.getInstance(mContext).splitAudioAndVideo(tempFilePath, tempMp3File, framePath, FFMPEG.getInstance(mContext).getIsCamera2(), isUseLastCreatedFile);//convertMp3ToPCM(mOutputFileName, tempOutputFileName);

                        if(isClosed || isErrorOccurred(errorCode))
                        {
                            isCachingThreadRunning = false;
                            return;
                        }

                        errorCode = FFMPEG.getInstance(mContext).convertMp3ToPCM(tempMp3File, tempOutputFileName);

                        FFMPEG.lastOpenedTrack = mSoundTrackUrl.hashCode();
                        internalOutputFileName = tempOutputFileName;

                        long timeDif = SystemClock.elapsedRealtime() - timeStarted;
                        SharedPreferencesManager.getEditor().putLong("loadingTime", timeDif);
                        SharedPreferencesManager.getEditor().apply();

                        if(isErrorOccurred(errorCode)) return;

                        initTracks();

//                        proceed();
                    }
                    catch (Throwable err)
                    {
                        err.printStackTrace();
                    }
                    isCachingThreadRunning = false;
                }
            };
            cachingThread.start();
        }
    }

    private boolean isErrorOccurred(int errorCode)
    {
        if (errorCode != 0)
        {
            try{errorMessage = FFMPEG.getInstance(mContext).getError();}catch (Throwable er){}
            System.out.println(">>>>>>ffmpeg>>>isErrorOccurred>>>>"+errorMessage+">>>"+errorCode);
            errorMessage = errorMessage.substring(errorMessage.length() - 50);
            isCachingThreadRunning = false;
            proceed();
            return true;
        }
        return false;
    }


    private void deleteExistingFrames()
    {
        File pathToDelete = new File(framesFilesPaths);
        File[]files = pathToDelete.listFiles();
        if(files != null && files.length > 0)
        {
            for(int i=0; i < files.length; i++)
            {
//                if(files[i].getName().indexOf(".jpg") != -1)
                    files[i].delete();
            }
        }
    }

    private boolean isConversionWentWrong()
    {
        File pathToDelete = new File(framesFilesPaths);
        File[]files = pathToDelete.listFiles();

        return files.length < 90; // less than 90 frames extracted
    }

    private void initTracks()
    {

//        numOfActionsBeforeProceed = 2;
//        new Thread()
//        {
//            public void run()
//            {
//                try{createFastTrack();}catch (Throwable err){}
//                proceed();
//            }
//        }.start();

        new Thread()
        {
            public void run()
            {
//                Tic.tic(458);

                try{createSlowTrack();}catch (Throwable err){}
//                Tic.tac(458);
                proceed();
            }
        }.start();

//        new Thread()
//        {
//            public void run()
//            {
//                try{changePitch();}catch (Throwable err){}
//                proceed();
//            }
//        }.start();
    }

    private synchronized void proceed()
    {
        counterOfActions++;
//        if(counterOfActions >= numOfActionsBeforeProceed)
            if (mOnDoneDownloadingListener != null)
                mOnDoneDownloadingListener.onDone(internalOutputFileName, internalOutputFastFileName, internalOutputSlowFileName);
    }

    private void changePitch()
    {
        try
        {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(tempOutputFileName));
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(internalOutputFileName));
            readAndWriteWavHeaderFile(dataInputStream, dataOutputStream);

            Sonic sonic;
            byte modifiedSamples[] = new byte[2048];
            sonic = new Sonic(sampleRate, 1);
            sonic.setPitch(1.0275f);

            byte[] buffer = new byte[minimumBufferSize];
            int count;
            while (true)
            {
                count = dataInputStream.read(buffer);
                if (count == -1)
                    break;
                sonic.putBytes(buffer, count);

                int available = sonic.availableBytes();
                if (available > 0)
                {
                    if (modifiedSamples.length < available)
                    {
                        modifiedSamples = new byte[available * 2];
                    }
                    sonic.receiveBytes(modifiedSamples, available);
                    dataOutputStream.write(modifiedSamples, 0, available);
                }
            }

            sonic.close();
            dataOutputStream.flush();
            dataOutputStream.close();
            dataInputStream.close();

        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
        }
    }

//    private void createFastSlowTracks()
//    {
//        try
//        {
//            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(tempOutputFileName));
//
//            DataOutputStream dataOutputStreamFast = new DataOutputStream(new FileOutputStream(internalOutputFastFileName));
//            DataOutputStream dataOutputStreamSlow = new DataOutputStream(new FileOutputStream(internalOutputSlowFileName));
//
////            dataInputStream.mark(1024000); // one mega
//            readAndWriteWavHeaderFile(dataInputStream, dataOutputStreamFast);
//            dataInputStream.close();
//
//            dataInputStream = new DataInputStream(new FileInputStream(tempOutputFileName));
//            readAndWriteWavHeaderFile(dataInputStream, dataOutputStreamSlow);
//
//            Sonic sonicFast, sonicSlow;
//
//            byte modifiedSamplesFast[] = new byte[2048];
//            byte modifiedSamplesSlow[] = new byte[2048];
//
//            sonicFast = new Sonic(44100, 1);
//            sonicSlow = new Sonic(44100, 1);
//
//            sonicSlow.setSpeed(0.33f);
////            sonicSlow.setSpeed(0.5f);
//            sonicFast.setSpeed(2);
//
//
//            byte[] buffer = new byte[minimumBufferSize];
////            byte[] bufferFast = new byte[minimumBufferSize];
//            int count, available;
//            while (true)
//            {
//                count = dataInputStream.read(buffer);
//                if (count == -1)
//                    break;
//
//                sonicFast.putBytes(buffer, count);
//                sonicSlow.putBytes(buffer, count);
//
//                available = sonicFast.availableBytes();
//                if (available > 0)
//                {
//                    if (modifiedSamplesFast.length < available)
//                    {
//                        modifiedSamplesFast = new byte[available * 2];
//                    }
//                    sonicFast.receiveBytes(modifiedSamplesFast, available);
//                    dataOutputStreamFast.write(modifiedSamplesFast, 0, available);
//                }
//
//                available = sonicSlow.availableBytes();
//                if (available > 0)
//                {
//                    if (modifiedSamplesSlow.length < available)
//                    {
//                        modifiedSamplesSlow = new byte[available * 2];
//                    }
//                    sonicSlow.receiveBytes(modifiedSamplesSlow, available);
//                    dataOutputStreamSlow.write(modifiedSamplesSlow, 0, available);
//                }
//            }
//
//            sonicFast.close();
//            sonicSlow.close();
//            dataInputStream.close();
//
//            dataOutputStreamFast.flush();
//            dataOutputStreamSlow.flush();
//
//            dataOutputStreamFast.close();
//            dataOutputStreamSlow.close();
//        }
//        catch (Throwable throwable)
//        {
//            throwable.printStackTrace();
//        }
//    }

    private void createFastTrack()
    {
        try
        {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(tempOutputFileName));
            DataOutputStream dataOutputStreamFast = new DataOutputStream(new FileOutputStream(internalOutputFastFileName));

            readAndWriteWavHeaderFile(dataInputStream, dataOutputStreamFast);

            byte modifiedSamplesFast[] = new byte[2048];

            Sonic sonicFast = new Sonic(sampleRate, 1);
            sonicFast.setSpeed(2);
            byte[] buffer = new byte[minimumBufferSize];
            int count, available;
            while (true)
            {
                count = dataInputStream.read(buffer);
                if (count == -1)
                    break;

                sonicFast.putBytes(buffer, count);

                available = sonicFast.availableBytes();
                if (available > 0)
                {
                    if (modifiedSamplesFast.length < available)
                    {
                        modifiedSamplesFast = new byte[available * 2];
                    }
                    sonicFast.receiveBytes(modifiedSamplesFast, available);
                    dataOutputStreamFast.write(modifiedSamplesFast, 0, available);
                }
            }

            sonicFast.close();
            dataInputStream.close();

            dataOutputStreamFast.flush();
            dataOutputStreamFast.close();
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
        }
    }




    private void createSlowTrack()
    {
        try
        {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(tempOutputFileName));
            DataOutputStream dataOutputStreamSlow = new DataOutputStream(new FileOutputStream(internalOutputSlowFileName));

            readAndWriteWavHeaderFile(dataInputStream, dataOutputStreamSlow);

            byte modifiedSamplesSlow[] = new byte[2048];

            Sonic sonicSlow = new Sonic(sampleRate, 1);
            sonicSlow.setSpeed(0.5f);
            byte[] buffer = new byte[minimumBufferSize];
            int count, available;
            while (true)
            {
                count = dataInputStream.read(buffer);
                if (count == -1)
                    break;

                sonicSlow.putBytes(buffer, count);

                available = sonicSlow.availableBytes();
                if (available > 0)
                {
                    if (modifiedSamplesSlow.length < available)
                    {
                        modifiedSamplesSlow = new byte[available * 2];
                    }
                    sonicSlow.receiveBytes(modifiedSamplesSlow, available);
                    dataOutputStreamSlow.write(modifiedSamplesSlow, 0, available);
                }
            }

            sonicSlow.close();
            dataInputStream.close();

            dataOutputStreamSlow.flush();
            dataOutputStreamSlow.close();
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
        }
    }

//    private void initialSetup()
//    {
//        try
//        {
//            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(tempOutputFileName));
//            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(internalOutputFileName));
//
//            DataOutputStream dataOutputStreamFast = new DataOutputStream(new FileOutputStream(internalOutputFastFileName));
//            DataOutputStream dataOutputStreamSlow = new DataOutputStream(new FileOutputStream(internalOutputSlowFileName));
//
//            readAndWriteWavHeaderFileMulti(dataInputStream, dataOutputStream, dataOutputStreamFast, dataOutputStreamSlow);
//
//            Sonic sonic;
//            byte modifiedSamples[] = new byte[2048];
//            sonic = new Sonic(44100, 1);
//            sonic.setPitch(1.0275f);
//
//            Sonic sonicFast, sonicSlow;
//
//            byte modifiedSamplesFast[] = new byte[2048];
//            byte modifiedSamplesSlow[] = new byte[2048];
//
//            sonicFast = new Sonic(44100, 1);
//            sonicSlow = new Sonic(44100, 1);
//
//            sonicSlow.setSpeed(0.33f);
////            sonicSlow.setSpeed(0.5f);
//            sonicFast.setSpeed(2);
//
//            byte[] buffer = new byte[minimumBufferSize];
//            int count, available;
//            while (true)
//            {
//                count = dataInputStream.read(buffer);
//                if (count == -1)
//                    break;
//
//                sonic.putBytes(buffer, count);
//                sonicFast.putBytes(buffer, count);
//                sonicSlow.putBytes(buffer, count);
//
//                available = sonic.availableBytes();
//                if (available > 0)
//                {
//                    if (modifiedSamples.length < available)
//                    {
//                        modifiedSamples = new byte[available * 2];
//                    }
//                    sonic.receiveBytes(modifiedSamples, available);
//                    dataOutputStream.write(modifiedSamples, 0, available);
//                }
//
//                available = sonicFast.availableBytes();
//                if (available > 0)
//                {
//                    if (modifiedSamplesFast.length < available)
//                    {
//                        modifiedSamplesFast = new byte[available * 2];
//                    }
//                    sonicFast.receiveBytes(modifiedSamplesFast, available);
//                    dataOutputStreamFast.write(modifiedSamplesFast, 0, available);
//                }
//
//                available = sonicSlow.availableBytes();
//                if (available > 0)
//                {
//                    if (modifiedSamplesSlow.length < available)
//                    {
//                        modifiedSamplesSlow = new byte[available * 2];
//                    }
//                    sonicSlow.receiveBytes(modifiedSamplesSlow, available);
//                    dataOutputStreamSlow.write(modifiedSamplesSlow, 0, available);
//                }
//            }
//
//            sonic.close();
//            sonicFast.close();
//            sonicSlow.close();
//
//            dataOutputStreamFast.flush();
//            dataOutputStreamSlow.flush();
//            dataOutputStream.flush();
//
//            dataOutputStreamFast.close();
//            dataOutputStreamSlow.close();
//            dataOutputStream.close();
//            dataInputStream.close();
//        }
//        catch (Throwable throwable)
//        {
//            throwable.printStackTrace();
//        }
//    }

    public boolean isSoundTrackSet()
    {
        return soundTrackSet;
    }

    private class RecordAudioRunnable implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                short[]buffer = new short[minimumBufferSize];
                while (isRecording)
                {
                    int bufferReadResult = audioRecord.read(buffer, 0, minimumBufferSize);
                    for (int i = 0; i < bufferReadResult; i++) {
                        outputStream.writeShort(buffer[i]);
                    }
                }
                audioRecord.stop();
                isDoneReadingData = true;
            }
            catch (Throwable t)
            {
                t.printStackTrace();
            }
        }
    }

    public void close()
    {
        isClosed = true;
//        canRunBotII =
        canRunBot = false;
        mOnDoneDownloadingListener = null;
        mContext = null;
        if(outputStream != null)
            try{outputStream.close();}catch (Throwable err){}
        try
        {
            audioRecord.release();
            audioRecord = null;
            outputStream = null;
        }catch (Throwable err){}
    }

    public interface OnDoneDownloadingListener
    {
        void onDone(String downloadedFilePath, String downloadedFilePathFast, String downloadedFilePathSlow);

        void onFirstFrameReady();

        void updateProgress(int progress);
    }

    private int sampleRate;
    public void readAndWriteWavHeaderFile(InputStream wavInputStream, OutputStream wavOutputStream) throws Throwable
    {
        int HEADER_SIZE = 44;
        byte[]buffer = new byte[HEADER_SIZE];

        int readCount = wavInputStream.read(buffer);
        if(readCount != buffer.length)
            throw new Throwable("Something went wrong, the bytes read count is not the same length as the buffer size, \nreadCount : " + readCount +
                    "\n buffer.length : " + buffer.length);
        wavOutputStream.write(buffer);
        if(sampleRate == 0)
            sampleRate = getIntFromBufferLittleIndian(buffer, 24); // at byte 24 we have the sample rate, part of the wave header

        int endOfHeaderFlag = getIntFromBufferLittleIndian(buffer, 36);
        int skipSize;
        boolean isFirst = true;

        while (endOfHeaderFlag != 0x61746164)
        {
            if(isFirst)
            {
                skipSize = getIntFromBufferLittleIndian(buffer, 40);
                isFirst = false;
            }
            else skipSize = getIntFromBufferLittleIndian(buffer, 4);

            buffer = new byte[skipSize];

            readCount = wavInputStream.read(buffer);

            if(readCount != buffer.length)
                throw new Throwable("Something went wrong, the bytes read count is not the same length as the buffer size");
            wavOutputStream.write(buffer);

            buffer = new byte[8]; // two ints :)

            readCount = wavInputStream.read(buffer);

            if(readCount != buffer.length)
                throw new Throwable("Something went wrong, the bytes read count is not the same length as the buffer size");
            wavOutputStream.write(buffer);
            endOfHeaderFlag = getIntFromBufferLittleIndian(buffer, 0);
        }
        wavOutputStream.flush();
    }

//    public void readAndWriteWavHeaderFileMulti(InputStream wavInputStream, OutputStream wavOutputStream, OutputStream wavOutputStreamFast, OutputStream wavOutputStreamSlow) throws Throwable
//    {
//        int HEADER_SIZE = 44;
//        byte[]buffer = new byte[HEADER_SIZE];
//
//        int readCount = wavInputStream.read(buffer);
//        if(readCount != buffer.length)
//            throw new Throwable("Something went wrong, the bytes read count is not the same length as the buffer size");
//        wavOutputStream.write(buffer);
//        wavOutputStreamFast.write(buffer);
//        wavOutputStreamSlow.write(buffer);
//
//        int endOfHeaderFlag = getIntFromBufferLittleIndian(buffer, 36);
//        int skipSize;
//        boolean isFirst = true;
//
//        while (endOfHeaderFlag != 0x61746164)
//        {
//            if(isFirst)
//            {
//                skipSize = getIntFromBufferLittleIndian(buffer, 40);
//                isFirst = false;
//            }
//            else skipSize = getIntFromBufferLittleIndian(buffer, 4);
//
//            buffer = new byte[skipSize];
//
//            readCount = wavInputStream.read(buffer);
//            if(readCount != buffer.length)
//                throw new Throwable("Something went wrong, the bytes read count is not the same length as the buffer size");
//            wavOutputStream.write(buffer);
//            wavOutputStreamFast.write(buffer);
//            wavOutputStreamSlow.write(buffer);
//
//            buffer = new byte[8]; // two ints :)
//
//            readCount = wavInputStream.read(buffer);
//            if(readCount != buffer.length)
//                throw new Throwable("Something went wrong, the bytes read count is not the same length as the buffer size");
//            wavOutputStream.write(buffer);
//            wavOutputStreamFast.write(buffer);
//            wavOutputStreamSlow.write(buffer);
//            endOfHeaderFlag = getIntFromBufferLittleIndian(buffer, 0);
//        }
//        wavOutputStream.flush();
//        wavOutputStreamFast.flush();
//        wavOutputStreamSlow.flush();
//    }


//    private String getTimeToSplit(String filePath) {
//        long seconds;
//        try{
//            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//            if (filePath != null)
//                retriever.setDataSource(filePath);
//            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//            seconds = Long.parseLong(time) / 1000;
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//            return "00:00:16";
//        }
//        if(seconds >= 16)
//            return "00:00:16";
//        else
//            if(seconds >= 10)
//                return "00:00:" + seconds;
//            else
//                return "00:00:0" + seconds;
//    }

    private int getIntFromBufferLittleIndian(byte[] buffer, int startOffset)
    {
        int retInt = ((buffer[startOffset + 3] & 0xFF) << 24) | ((buffer[startOffset + 2] & 0xFF) << 16)
            | ((buffer[startOffset + 1] & 0xFF) << 8) | (buffer[startOffset] & 0xFF);
        return retInt;
    }

}