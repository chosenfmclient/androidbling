package fm.bling.blingy.inviteFriends;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.BaseActivity;
import fm.bling.blingy.R;
import fm.bling.blingy.database.contract.DuetContract;
import fm.bling.blingy.database.handler.DuetInvitedHandler;
import fm.bling.blingy.dialogs.messages.NetworkDialog;
import fm.bling.blingy.inviteFriends.adapters.RecyclerAdapterInvite;
import fm.bling.blingy.inviteFriends.listeners.InvalidateNotificationListener;
import fm.bling.blingy.inviteFriends.model.CAAEmailInvite;
import fm.bling.blingy.inviteFriends.model.CAAEmailInvites;
import fm.bling.blingy.inviteFriends.model.CAASmsInvite;
import fm.bling.blingy.inviteFriends.model.CAASmsInvites;
import fm.bling.blingy.inviteFriends.model.ContactInfo;
import fm.bling.blingy.mashup.listeners.CameoInviteListener;
import fm.bling.blingy.mashup.model.Invitee;
import fm.bling.blingy.mashup.model.InviteeContact;
import fm.bling.blingy.profile.fragments.BaseSearchFragment;
import fm.bling.blingy.record.utils.SimpleDividerItemDecoration;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.stateMachine.StateMachine;
import fm.bling.blingy.stateMachine.StateMachineEvent;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;
import fm.bling.blingy.utils.dialogs.LoadingDialog;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.widget.CAAWidgetSettingsSingleton;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 12/5/16.
 * History:
 * ***********************************
 */
public class InviteFragment extends BaseSearchFragment implements CameoInviteListener<ContactInfo>{


    /**
     * Main Views
     */
    private RecyclerView mRecycler;
    private RecyclerAdapterInvite adapter;
    private LinearLayoutManager mLayoutManager;
    private FrameLayout listViewEmpty;
    private TextView mTextTotal;

    private ArrayList<ContactInfo> items;
    private ArrayList<ContactInfo> searchDataSet;

    private ImageLoaderManager imageLoaderManager;
    private NetworkDialog mNetworkDialog;
    private LoadingDialog loadingDialog;
    private boolean isFirstReleased;
    private int lastSearchLength;
    private boolean isCameo = false;

    private ArrayList<InviteeContact> sendList;
    private ArrayList<InviteeContact> invitedDuetList;
    private String mVideoID;

    public static InviteFragment newInstance(boolean isCameo, String mVideoID) {
        InviteFragment myFragment = new InviteFragment();
        myFragment.searchDataSet = new ArrayList<>();
        myFragment.items = new ArrayList<>();
        myFragment.isCameo = isCameo;
        myFragment.mVideoID = mVideoID;
        if(isCameo)
            myFragment.invitedDuetList = DuetInvitedHandler.getInstance().getAll(mVideoID);
        if (InviteHandler.contacts != null) {
            for (int i = 0; i < InviteHandler.contacts.size(); i++) {
                ContactInfo contactInfo = InviteHandler.contacts.get(i);
                contactInfo.cameoInvited = 0;
                if(isCameo && myFragment.invitedDuetList != null){
                        InviteeContact contact = new InviteeContact(contactInfo.name,contactInfo.phone(),contactInfo.email,contactInfo.photoUrl, mVideoID);
                    for (int j = 0; j < myFragment.invitedDuetList.size(); j++) {
                        InviteeContact invitedContact = myFragment.invitedDuetList.get(j);
                        if(contact.equals(invitedContact)){
                            contactInfo.cameoInvited = 1;
                            break;
                        }
                    }
                }
                myFragment.searchDataSet.add(contactInfo);
                myFragment.items.add(contactInfo);
            }
        }
        return myFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (imageLoaderManager == null)
            imageLoaderManager = ((BaseActivity) getActivity()).getImageLoaderManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.follow_invite_fragment, null);
        TextView inviteAll = (TextView) v.findViewById(R.id.follow_all);
        inviteAll.setVisibility(View.GONE);
        if(isCameo)
            inviteAll.setBackgroundResource(R.drawable.button_shape_pink);

        inviteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inviteAll();
            }
        });

        mRecycler = (RecyclerView) v.findViewById(R.id.connections_recycler);
        mTextTotal = (TextView) v.findViewById(R.id.text_total);
        listViewEmpty = (FrameLayout) v.findViewById(R.id.connection_empty);

        if (items != null && items.size() > 0) {

            if (isCameo)
                mTextTotal.setText(items.size() + " Friends to do a Duet with");
            else
                mTextTotal.setText(items.size() + " Contacts");

            removeEmptyLayout();
            if (imageLoaderManager == null)
                imageLoaderManager = ((BaseActivity) getActivity()).getImageLoaderManager();
            mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            mRecycler.setLayoutManager(mLayoutManager);
            mRecycler.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext(), R.drawable.line_divider, true));

            adapter = new RecyclerAdapterInvite(getContext(), searchDataSet, imageLoaderManager, new InvalidateNotificationListener() {
                @Override
                public void invalidateNotification() {
                    adapter.notifyDataSetChanged();
                }
            }, isCameo ? this : null);

            mRecycler.setAdapter(adapter);
            inviteAll.setVisibility(View.VISIBLE);
        } else {
            showEmptyLayout();
        }
        return v;
    }

    @Override
    public void onStop() {
        super.onStop();
        if(isCameo && sendList != null && sendList.size() > 0) {
            invitePeople();
        }
    }


    private void inviteAll() {
        loadingDialog = new LoadingDialog(getContext(), false, false);

        if(isCameo){
            int count = 0;
            for (ContactInfo contactInfo : searchDataSet) {
                if (contactInfo.cameoInvited != ContactInfo.NOT_INVITED)
                    continue;
                this.addToInvite(contactInfo);
                count++;
            }
            if(count > 0)
                showToastInvitedCount(count);
            invitePeople();
        }
        else {
            ArrayList<CAASmsInvite> caaSmsInviteArrayList = new ArrayList<>();
            ArrayList<CAAEmailInvite> caaEmailInviteArrayList = new ArrayList<>();

            for (ContactInfo contactInfo : searchDataSet) {
                if (contactInfo.status != ContactInfo.NOT_INVITED)
                    continue;
                if (contactInfo.isPhoneNumberSet()) {
                    caaSmsInviteArrayList.add(new CAASmsInvite(contactInfo.phone(), contactInfo.getName()));
                } else {
                    caaEmailInviteArrayList.add(new CAAEmailInvite(contactInfo.email, contactInfo.name));
                }
            }

            if (caaSmsInviteArrayList.size() > 0) {
                CAASmsInvites caaSmsInvites = new CAASmsInvites(caaSmsInviteArrayList);
                App.getUrlService().postSmsInvite(caaSmsInvites, new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {
                        synchronized (searchDataSet) {
                            for (ContactInfo contactInfo : searchDataSet) {
                                if (contactInfo.isPhoneNumberSet()) {
                                    StateMachine.getInstance(getContext().getApplicationContext()).excludeFromInvites(contactInfo.phone(), StateMachine.INVITE_EXCLUDE_TYPE_PHONE);
                                    contactInfo.status = ContactInfo.INVITED;
                                }
                            }
                            if (isFirstReleased) {
                                loadingDialog.dismiss();
                                adapter.notifyDataSetChanged();
                            } else isFirstReleased = true;
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (isFirstReleased)
                            loadingDialog.dismiss();
                        else isFirstReleased = true;
                        showNetworkDialog();
                    }
                });

            } else isFirstReleased = true;

            if (caaEmailInviteArrayList.size() > 0) {
                CAAEmailInvites caaEmailInvites = new CAAEmailInvites(caaEmailInviteArrayList, CAAUserDataSingleton.getInstance().getFullName());
                App.getUrlService().postEmailInvite(caaEmailInvites, new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {
                        synchronized (searchDataSet) {
                            for (ContactInfo contactInfo : searchDataSet) {
                                if (!contactInfo.isPhoneNumberSet()) {
                                    contactInfo.status = ContactInfo.INVITED;
                                    StateMachine.getInstance(getContext().getApplicationContext()).excludeFromInvites(contactInfo.email, StateMachine.INVITE_EXCLUDE_TYPE_EMAIL);
                                }
                            }

                            if (isFirstReleased) {
                                loadingDialog.dismiss();
                                adapter.notifyDataSetChanged();
                            } else isFirstReleased = true;
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (isFirstReleased)
                            loadingDialog.dismiss();
                        else isFirstReleased = true;
                        showNetworkDialog();
                    }
                });
            } else {
                if (isFirstReleased)
                    loadingDialog.dismiss();
                else isFirstReleased = true;
            }
            int invteSent = SharedPreferencesManager.getInstance().getInt(EventConstants.INVITES_SENT, 0);
            invteSent += (caaSmsInviteArrayList.size() + caaEmailInviteArrayList.size());
            SharedPreferencesManager.getEditor().putInt(EventConstants.INVITES_SENT, invteSent);

            TrackingManager.getInstance().inviteSend("all", invteSent);

            int sum = CAAWidgetSettingsSingleton.getInstance().getMyPoints() + (CAAWidgetSettingsSingleton.getInstance().getInviteWorth() * (caaEmailInviteArrayList.size() + caaSmsInviteArrayList.size()));
            CAAWidgetSettingsSingleton.getInstance().setMyPoints(sum);
            StateMachine.getInstance(getContext()).insertEvent(new StateMachineEvent(StateMachineEvent.EVENT_CLASS.SOCIAL, StateMachineEvent.EVENT_TYPE.INVITE_FRIEND.ordinal()));
        }
    }

    private void showEmptyLayout() {
        mRecycler.setVisibility(View.GONE);
        listViewEmpty.setVisibility(View.VISIBLE);
    }

    private void removeEmptyLayout() {
        listViewEmpty.setVisibility(View.GONE);
    }

    private void showNetworkDialog() {
        if (mNetworkDialog == null) {
            mNetworkDialog = new NetworkDialog(getContext());
            mNetworkDialog.show();
        }

    }

    @Override
    public void onSearch(String text) {
        if (searchDataSet == null || adapter == null)
            return;

        String search = text.toLowerCase();
        if (lastSearchLength < search.length()) {
            for (int i = searchDataSet.size() - 1; i > -1; i--) {
                if (!searchDataSet.get(i).name.toLowerCase().contains(search)) {
                    searchDataSet.remove(i);
                }
            }
        } else {
            searchDataSet.clear();
            if (search.length() > 0) {
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).name.toLowerCase().contains(search))
                        searchDataSet.add(items.get(i));
                }
            } else {
                for (int i = 0; i < items.size(); i++) {
                    searchDataSet.add(items.get(i));
                }
            }
        }
        adapter.notifyDataSetChanged();
        lastSearchLength = search.length();
    }


    private void invitePeople(){
        if(InviteHandler.contacts != null){
            for (ContactInfo contactInfo : InviteHandler.contacts)
                contactInfo.cameoInvited = ContactInfo.NOT_INVITED;
        }

        if(sendList != null && sendList.size() > 0){
            SharedPreferencesManager.getEditor().putBoolean(Constants.INVITED_TO_DUET,true).apply();
            Invitee invitee = new Invitee(sendList, mVideoID);
            TrackingManager.getInstance().tapInviteFriendsCameo(sendList.size());
            DuetInvitedHandler.getInstance().addMany(sendList);
            App.getUrlService().postCameoInvite(invitee, new Callback<String>() {
                @Override
                public void success(String s, Response response) {
                    sendList.clear();
                }
                @Override
                public void failure(RetrofitError error) {
                }
            });

        }
    }

    @Override
    public void addToInvite(ContactInfo contactInfo) {
        if(sendList == null)
            sendList = new ArrayList<>(1);
        sendList.add(new InviteeContact(contactInfo.getName(), contactInfo.phone(), contactInfo.getEmail(), contactInfo.getPhotoUrl(), mVideoID));

    }

    private void showToastInvitedCount(int count) {
        if(isCameo){
            Toast.makeText(getActivity(), "Invited " + count + " users to your Duet!", Toast.LENGTH_SHORT).show();
        }
        else{
            //TODO show other msg
        }

    }
}
