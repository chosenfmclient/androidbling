package fm.bling.blingy.upload;

import java.io.File;
import java.util.ArrayList;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 04/07/2016.
 * History:
 * ***********************************
 */
public class FolderManager {
    private final int minimumFileSize = 1024 * 35;
    private final File mFolderFile;
    private String[] mExtensions;
    private String[] mFilePaths;
    private final byte mType;
    private int recentPosition = -1;
    private int lastWeekPosition = -1;
    private int twoWeeksPosition = -1;
    private int recentCount;
    private int lastWeekCount;

    public FolderManager(File folderFile, byte type) {
        mFolderFile = folderFile;
        mType = type;
        getExtensions(type);
        init();
    }

    private void getExtensions(byte type) {
        switch (type) {
            case FileManager.PHOTO:
                mExtensions = new String[]{".jpg", ".png"};
                break;
            case FileManager.VIDEO:
                mExtensions = new String[]{".avi", ".mp4", ".mpeg", ".m4v"};
                break;
        }
    }

    public void init() {
        //                                                   sec    m    hr   d    w        two weeks
        final long LAST_WEEK = System.currentTimeMillis() - (1000 * 60 * 60 * 24 * 7);
        final long TWO_WEEKS_AGO = System.currentTimeMillis() - ((1000 * 60 * 60 * 24 * 7) * 2);

        ArrayList<File> sortedArray = new ArrayList<>();
        File[] files = mFolderFile.listFiles();
        lastWeekCount = 0;
        boolean hasTwoWeeksAgo = false;
        if (files != null) {
            for (File file : files) {
                if (!file.isDirectory() && file.length() > minimumFileSize && isInExtensions(file.getName())) {
                    if (file.lastModified() < TWO_WEEKS_AGO) {
                        sortedArray.add(file);
                        if (!hasTwoWeeksAgo)
                            hasTwoWeeksAgo = true;
                    } else if (file.lastModified() < LAST_WEEK) {
                        sortedArray.add(recentCount, file);
                        lastWeekCount++;

                    } else {
                        recentCount++;

//                    int index = 0;
//                    for (int i = 0; i < sortedArray.size(); i++)
//                    {
//                        if (file.lastModified() < sortedArray.get(i).lastModified())
//                        {
//                            index = i;
//                            break;
//                        }
//                    }
                        sortedArray.add(0, file);

                    }
                }
            }
        }

        if (hasTwoWeeksAgo) {
            twoWeeksPosition = recentCount + lastWeekCount;
        }

        if (lastWeekCount > 0) {
            lastWeekPosition = recentCount;
        }

        if (recentCount > 0)
            recentPosition = 0;

        mFilePaths = new String[sortedArray.size()];

        for (int i = 0; i < mFilePaths.length; i++) {
            mFilePaths[i] = sortedArray.get(i).getPath();
        }

//        mExtensions = null;
    }

    public boolean isInExtensions(String name) {
        if (name.charAt(0) == '.')
            return false;
        try {
            for (String ext : mExtensions) {
                if (name.substring(name.length() - ext.length()).equals(ext))
                    return true;
            }
        } catch (Throwable err) {
        }
        return false;
    }

    public String[] getFilePaths() {
        return mFilePaths;
    }

    public byte getType() {
        return mType;
    }

    public boolean hasFiles() {
        return mFilePaths.length > 0;
    }

    public void close() {
//        mExtensions = null;
//        mFilePaths = null;
    }

    public int getRecentPosition() {
        return recentPosition;
    }

    public int getLastWeekPosition() {
        return lastWeekPosition;
    }

    public int getTwoWeeksPosition() {
        return twoWeeksPosition;
    }

    public int getRecentCount() {
        return recentCount;
    }

    public int getLastWeekCount() {
        return lastWeekCount;
    }

}