package fm.bling.blingy.rest;

import java.util.ArrayList;

import fm.bling.blingy.dialogs.comment.model.CAACommentText;
import fm.bling.blingy.dialogs.model.CAAType;
import fm.bling.blingy.dialogs.share.model.CAANativeShare;
import fm.bling.blingy.dialogs.share.model.CAANativeVideo;
import fm.bling.blingy.dialogs.share.model.CAAPostPlatformAndTag;
import fm.bling.blingy.dialogs.share.model.CAAPostShare;
import fm.bling.blingy.dialogs.share.model.CAAShare;
import fm.bling.blingy.discover.model.DiscoverFeed;
import fm.bling.blingy.discover.model.Videos;
import fm.bling.blingy.duetHome.model.DuetHomeFeed;
import fm.bling.blingy.duetHome.model.DuetPermission;
import fm.bling.blingy.editVideo.rest.model.CAAPostVideos;
import fm.bling.blingy.editVideo.rest.model.CAAVideoPostResponse;
import fm.bling.blingy.gcm.model.CAAPutFCMSettings;
import fm.bling.blingy.homeScreen.model.VideoFeed;
import fm.bling.blingy.inviteFriends.model.CAAApiSuggestedFriends;
import fm.bling.blingy.inviteFriends.model.CAAContactEmails;
import fm.bling.blingy.inviteFriends.model.CAAEmailInvites;
import fm.bling.blingy.inviteFriends.model.CAAFollowAll;
import fm.bling.blingy.homeScreen.model.CAAApiNotifications;
import fm.bling.blingy.homeScreen.model.CAANotificationUnread;
import fm.bling.blingy.inviteFriends.model.CAASmsInvites;
import fm.bling.blingy.inviteFriends.model.ContactsInviteeEmail;
import fm.bling.blingy.mashup.model.Invitee;
import fm.bling.blingy.mashup.model.InviteeContactsResponse;
import fm.bling.blingy.mashup.model.MashupOBJ;
import fm.bling.blingy.mashup.model.PostMashup;
import fm.bling.blingy.profile.model.CAAApiUsers;
import fm.bling.blingy.profile.model.CAAPhoto;
import fm.bling.blingy.profile.model.CAAPhotoEditProfile;
import fm.bling.blingy.profile.model.CAAPutUser;
import fm.bling.blingy.profile.model.UserProfile;
import fm.bling.blingy.registration.rest.model.CAACFacebookSignup;
import fm.bling.blingy.registration.rest.model.CAAEmailError;
import fm.bling.blingy.registration.rest.model.CAAEmailSignup;
import fm.bling.blingy.registration.rest.model.CAAGooglePlusSignup;
import fm.bling.blingy.registration.rest.model.CAAGoogleToken;
import fm.bling.blingy.registration.rest.model.CAAPostPassword;
import fm.bling.blingy.registration.rest.model.CAAPostSignup;
import fm.bling.blingy.registration.rest.model.CAAPostToken;
import fm.bling.blingy.registration.rest.model.CAAToken;
import fm.bling.blingy.record.rest.model.CAAiTunes;
import fm.bling.blingy.rest.model.CAAApiUserDeepLink;
import fm.bling.blingy.rest.model.CAACommentParams;
import fm.bling.blingy.rest.model.CAACommentsResponse;
import fm.bling.blingy.rest.model.CAALike;
import fm.bling.blingy.rest.model.CAALikeResponse;
import fm.bling.blingy.rest.model.CAAPreferredUsers;
import fm.bling.blingy.rest.model.CAAPushMetric;
import fm.bling.blingy.rest.model.CAAStatus;
import fm.bling.blingy.inviteFriends.model.CAAApiContactsResponse;
import fm.bling.blingy.rest.model.LeaderboardData;
import fm.bling.blingy.rest.model.PermissionLevel;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.songHome.model.SongHomeFeed;
import fm.bling.blingy.videoHome.model.CAAVideo;
import fm.bling.blingy.widget.model.CAAInviteWidgetSettingsResponse;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

public interface UrlService {

/*****************************************************************************************************************************************************************************************
 *
 * GET Functions
 *********************************************************************************************************************************************************************************/

    @GET("/home")
    void getHomeScreen(@Query("page") int page, Callback<VideoFeed> callback);

    @GET("/notifications")
    void getNotifications(Callback<CAAApiNotifications> callback);

    @GET("/notifications")
    void getNotificationsPage(@Query("page") int page, Callback<CAAApiNotifications> callback);

    @GET("/discover")
    void getDiscover(@Query("page") int page, Callback<DiscoverFeed> callback);

//    @GET("/discover")
//    void getDiscoverByVideoId(@Query("page") int page, @Query("video_id") String video_id ,Callback<DiscoverFeed> callback);

//    @GET("/song-home/{id}")
//    void getSongHomeByClipId(@Query("page") int page, @Path("id") String clip_id ,Callback<DiscoverFeed> callback);

    @GET("/song-home/{id}")
    void getSongHomeByClipId(@Query("page") int page, @Path("id") String clip_id ,Callback<SongHomeFeed> callback);

//    @GET("/discover")
//    void getDiscoverByClipUrl(@Query("page") int page, @Query("song_clip_url") String songClipUrl ,Callback<DiscoverFeed> callback);

    @GET("/discover")
    void getDiscoverByClipUrl(@Query("page") int page, @Query("song_clip_url") String songClipUrl ,Callback<SongHomeFeed> callback);

    @GET("/search")
    void getSearchVideo(@Query("type") String type , @Query("q") String query, Callback<CAAiTunes> callback);

    @GET("/search")
    void getSearchUsers(@Query("type") String type, @Query("page") int page, @Query("q") String query, Callback<CAAApiSuggestedFriends> callback);

    @GET("/record")
    void getRecordClips(Callback<CAAiTunes> callback);

//    @GET("/record")
//    void searchRecordClips(@Query("term") String term, Callback<CAAiTunes> callback);

//    @GET("/time-segments")
//    void getTimeSegments(@Query("song_clip_url") String songClipUrl, Callback<ArrayList<Integer>> callback);

    @GET("/time-segments")
    void getTimeSegmentsByClipID(@Query("clip_id") String songClipUrl, Callback<ArrayList<Integer>> callback);

//    @GET("/soundtracks")
//    void getSoundtracks(Callback<CAAiTunes> callback);

//    @GET("/sound-effects")
//    void getSoundEffects(Callback<CAASoundEffects> callback);

//    @GET("/suggested-friends")
//    void getSuggestedFriends(Callback<CAAApiSuggestedFriends> callback);

//    @GET("/settings")
//    void getSettings(Callback<CAAApiSettings> callback);

//    @GET("/videos/{id}/mashups")
//    void getVideoMashups(@Path("id") String id ,Callback<ArrayList<CAAVideo>> callback);

    @GET("/videos/{id}")
    void getVideoPreview(@Path("id") String id ,Callback<CAAVideo> callback);

    @GET("/users/{id}")
    void getSpecificUser(@Path("id") String id, Callback<User> callback);

    @GET("/users/{id}/followers")
    void getSpecificUserFollowersPage(@Path("id") String id, @Query("page") int page, Callback<CAAApiUsers> callback);

    @GET("/users/{id}/following")
    void getSpecificUserFollowingPage(@Path("id") String id, @Query("page") int page, Callback<CAAApiUsers> callback);


//    void getUserProfile(@Path("id") String id,@Query("type") String type, @Query("page") int page ,Callback<UserProfile> callback);

    @GET("/users/{id}/profile")
    void getUserProfile(@Path("id") String id,@Query("page") int page ,Callback<UserProfile> callback);

//    @GET("/users/{id}/videos")
//    void getSpecificUserVideos(@Path("id") String id, @Query("type") String type, @Query("status") String status, @Query("sort") String sort, Callback<CAAVideoList> callback);
//
//    @GET("/users/{id}/videos")
//    void getSpecificUserVideosPage(@Path("id") String id, @Query("type") String type, @Query("status") String status, @Query("sort") String sort, @Query("page") int page, Callback<CAAVideoList> callback);

    @GET("/users/me/notifications")
    void getsUserNotificationsUnreadCount(Callback<CAANotificationUnread> callback);

    @GET("/users/{id}/deep-link")
    void getSpecificUserDeepLink(@Path("id") String id, Callback<CAAApiUserDeepLink> callback);

    @GET("/users/me/deep-link")
    void getMarketingDeeplink(@Query("m_id") String marketingId, Callback<CAAApiUserDeepLink> callback);

    @GET("/native-video")
    void getNativeVideo(Callback<CAANativeVideo> callback);

    @GET("/videos/{id}/comments")
    void getVideoComments(@Path("id") String video_id, @Query("page") int page, Callback<CAACommentsResponse> cAACommentsResponse);

    @GET("/videos/{id}/likes")
    void getVideoLikes(@Path("id") String video_id, @Query("page") int page, Callback<CAALikeResponse> cAACommentsResponse);

//    @GET("/video-types")
//    void getVideoCatagories(Callback<CAAVideoTypeResponse> cAAVideoTypeResponse);

//    @GET("/video-frames")
//    void getFramesAndWatermark(Callback<CAAApiFramesAndWatermark> caaApiFramesAndWatermarkCallback);
//
//    @GET("/faces")
//    void getFaceDetection(Callback<CAAFaceDetection> caaFaceDetectionCallback);
//
    @GET("/preferred-users")
    void getPrefferedUsers(Callback<CAAPreferredUsers> caaPrefferedUsersCallback);

    @GET("/widget-settings")
    void getWidgetSettings(Callback<CAAInviteWidgetSettingsResponse> callback);

    @GET("/users/me/likes")
    void getUserLikes(Callback<ArrayList<String>> videoIds);

    @GET("/following")
    void getUserFollowing(Callback<ArrayList<String>> usersIds);


//    @GET("/mashups/{id}")
//    void getMashup(@Path("id") String mashup_id ,Callback<MashupOBJ> callback);

//    @GET("/leaderboard")
//    Observable<LeaderboardData> getLeaderboard(@Query("page") int page);

    @GET("/leaderboard")
    void getLeaderboard(@Query("page") int page, Callback<LeaderboardData> data);

    @GET("/cameo-song-home")
    void getDuetHome(@Query("page") int page, Callback<DuetHomeFeed> callback);

    @GET("/users/me/videos")
    void getMyVideos(Callback<Videos> videos);

    @GET("/users/{id}/cameo-permission")
    void getDuetPermission(@Path("id") String user_id , Callback<DuetPermission> duetPermission);


/*****************************************************************************************************************************************************************************************
 *
 * POST Functions
 *********************************************************************************************************************************************************************************/


    @POST("/native-video")
    void postNativeVideo(@Body CAANativeShare share, Callback<CAANativeVideo> callback);

//    @POST("/mail-message")
//    void postMailMessage(@Body CAAPostMailMessage mail, Callback <String > call );
//
//    @POST("/videos/{id}/share")
//    void postVideoShare(@Path("id") String id ,@Body CAAPostShare share ,Callback <String > call );

    @POST("/videos/{id}/share")
    void postVideoShare(@Path("id") String id, @Body CAAPostPlatformAndTag caaPostPlatformAndTag, Callback<CAAShare> call);

//    @POST("/users/{id}/share")
//    void postUsersShare(@Path("id") String id ,@Body CAAPostShare share ,Callback <String > call );

    @POST("/users/{id}/share")
    void postUsersShare(@Path("id") String id, @Body CAAPostPlatformAndTag caaPostPlatformAndTag, Callback<CAAShare> call);//, @Query("platform") String platform

//    @POST("/users/{id}/deep-link")
//    void postUsersDeepLink(@Path("id") String id ,@Body CAAPostShare share ,Callback <String > call );

    @POST("/password")
    void postPassword(@Body CAAPostPassword share, Callback<String> call);

    @POST("/signup")
    void postSignup(@Body CAAPostSignup share, Callback<User> call);

    @POST("/token")
    void postToken(@Body CAAPostToken share, Callback<CAAToken> call);

    @POST("/google-token")
    void postGoogleToken(@Body CAAGoogleToken caaGoogleToken, Callback<CAAToken> call);

//    @POST("/google-token")
//    CAAToken postGoogleToken(@Body CAAGoogleToken caaGoogleToken);

//    @POST("/google-token")
//    Observable<CAAToken> postGoogleTokenRx(@Body CAAGoogleToken caaGoogleToken);

//    @POST("/token")
//    CAAToken postTokenSync(@Body CAAPostToken share);

//    @POST("/token")
//    Observable<CAAToken> postTokenRx(@Body CAAPostToken share);

//    @POST("/anonymous-token")
//    void postAnonymousToken(@Body CAAPostAnonymousToken token, Callback<CAAToken> call);

//    @POST("/anonymous-token")
//    CAAToken postAnonymousTokenSync(@Body CAAPostAnonymousToken token);

//    @POST("/users")
//    void postUsers(@Body CAAPostUsers share ,Callback <String > call );

    @POST("/videos")
    void postUploadVideo(@Body CAAPostVideos share, Callback<CAAVideoPostResponse> call);

    @POST("/users/me/photo")
    void postUserPhoto(@Body CAAPhoto photo, Callback<CAAPhotoEditProfile> method);

//    @POST("/find-friends")
//    void postFindFriends(@Body CAAPostTwitter twitterList, Callback<CAAApiContactsResponse> call);
//
//    @POST("/find-friends")
//    void postFindFriends(@Body CAAPostFacebookFriends facebookIds, Callback<CAAApiContactsResponse> call);

    @POST("/find-friends")
    void postFindFriends(@Body CAAContactEmails contacts, Callback<CAAApiContactsResponse> call);

    @POST("/email-invite")
    void postEmailInvite(@Body CAAEmailInvites invites, Callback<String> call);

    @POST("/sms-invites")
    void postSmsInvite(@Body CAASmsInvites invites, Callback<String> call);

    @POST("/follow")
    void postFollowAll(@Body CAAFollowAll userIds, Callback<String> call);

    @POST("/email")
    void getEmailCheck(@Body CAAEmailSignup emailSignup, Callback<CAAEmailError> callback);

    @POST("/facebook-signup")
    void postFacebookSignup(@Body CAACFacebookSignup facebookSignup, Callback<User> call);

    @POST("/google-signup")
    void postGooglePlusSignup(@Body CAAGooglePlusSignup googlePlusSignup, Callback<CAAToken> call);

    @POST("/users/me/deep-link")
    void postDeeplink(@Body CAAPostShare share, Callback<String> call);

    @POST("/videos/{id}/like")
    void postLikeVideo(@Path("id") String id, @Body CAAType type, Callback<CAALike> call);

//    @POST("/app-rate")
//    void postAppRate(@Body CAARateApp rateApp, Callback<String> call);

    @POST("/push-metric")
    void postPushMetric(@Body CAAPushMetric metric, Callback<String> call);

    @POST("/videos/{id}/comments")
    void postLeaveComment(@Path("id") String video_id, @Body CAACommentText cCommentText, Callback<CAACommentParams> cAACommentParams);


    @POST("/mashups")
    void postMashup(@Body PostMashup postMashup, Callback<MashupOBJ> call);

    @POST("/cameo-invite-screen")
    void postFollowingEmailToRemove(@Body ContactsInviteeEmail postMashup, Callback<InviteeContactsResponse> call);

    @POST("/cameo-invite")
    void postCameoInvite(@Body Invitee contacts, Callback<String> s);





/*****************************************************************************************************************************************************************************************
 *
 * PUT Functions
 *********************************************************************************************************************************************************************************/

    @PUT("/signup")
    void putsSignup(@Body CAAPostPassword share, Callback<String> call);

//    @PUT("/gcm-settings")
//    void putGcmSettings(@Body CAAPutGCMSettings settings, Callback<CAAPutGCMSettings> call);

    @PUT("/fcm-settings")
    void putFCMSettings(@Body CAAPutFCMSettings settings, Callback<CAAPutFCMSettings> call);

    @PUT("/users/me")
    void putEditProfile(@Body CAAPutUser user, Callback<User> call);

    @PUT("/videos/{id}")
    void putUploadVideoStatus(@Path("id") String id, @Body CAAStatus status, Callback<CAAVideo> call);

    @PUT("/users/{id}/follow")
    void putsUsersFollow(@Path("id") String id, @Body String empty, Callback<String> call);

    @PUT("/users/me/photo")
    void putsUsersPhoto(@Body String empty, Callback<CAAPhoto> photo);

    @PUT("/users/me/notifications")
    void putsUsersNotifications(@Body String empty, Callback<String> call);

    @PUT("/notifications/{id}")
    void putsNotifications(@Path("id") String id, @Body CAAStatus status, Callback<String> call);

    @PUT("/videos/{id}/flag")
    void putVideoFlag(@Path("id") String videoId, @Body CAAType type, Callback<String> callback);

    @PUT("/videos/{id}/share")
    void putVideoShare(@Path("id") String id, @Body CAAPostShare share, Callback<String> call);

    @PUT("/users/{id}/share")
    void putUserShare(@Path("id") String id, @Body CAAPostShare share, Callback<String> call);

    @PUT("/comments/{id}")
    void putEditComment(@Path("id") String id, @Body CAACommentText cCommentText, Callback<String> call);

    @PUT("/comments/{id}/flag")
    void putFlagComment(@Path("id") String commentid, @Body CAAType type, Callback<String> call);

    @PUT("/videos/{id}/cameo-permission")
    void putDuetPermissionLevel(@Path("id") String video_id, @Body PermissionLevel permissionLevel, Callback<String> call);

//    @PUT("/mashups/{id}")
//    void putMashupForceComplete(@Path("id") String mashup_id, @Body String empty, Callback<String> call);

/*****************************************************************************************************************************************************************************************
 *
 * DELETE Functions
 *********************************************************************************************************************************************************************************/

    @DELETE("/videos/{id}/like")
    void deleteLikeVideo(@Path("id") String id, Callback<String> call);

    @DELETE("/users/{id}/follow")
    void deleteUsersFollow(@Path("id") String id, Callback<String> call);

    @DELETE("/videos/{id}")
    void deleteVideo(@Path("id") String videoID, Callback<String> call);

//    @DELETE("/mashups/{id}")
//    void deleteMashup(@Path("id") String mashup_id, Callback<String> call);

}