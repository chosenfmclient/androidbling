package fm.bling.blingy.homeScreen.model;

import com.google.gson.annotations.SerializedName;


/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAACounters
 * Description:
 * Created by Dawidowicz Nadav on 5/9/15.
 * History:
 * ***********************************
 */
public class CAACounters {

    @SerializedName("unread")
    private String unread;

    public String getUnread() {
        return unread;
    }

    public void setUnread(String unread) {
        this.unread = unread;
    }
}
