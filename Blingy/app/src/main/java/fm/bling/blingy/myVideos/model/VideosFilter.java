package fm.bling.blingy.myVideos.model;

/**
 * Created by Kiril on 6/2/2015.
 */

public class VideosFilter {

    private String filterType;
    private String sortType;

    public VideosFilter(String filterType, String sortType) {
        this.filterType = filterType;
        this.sortType = sortType;
    }

    public String getFilterType() {
        return filterType;
    }

    public boolean equals(VideosFilter filter) {
        return (filter.getFilterType().equalsIgnoreCase(this.getFilterType())
                && filter.getSortType().equalsIgnoreCase(this.getSortType()));
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }
}
