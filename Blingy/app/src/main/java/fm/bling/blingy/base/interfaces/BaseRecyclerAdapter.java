package fm.bling.blingy.base.interfaces;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 02/03/17.
 * History:
 * ***********************************
 */

/**
 * BaseAdpter with {@link fm.bling.blingy.utils.imagesManagement.MultiImageLoader}
 * or {@link fm.bling.blingy.utils.imagesManagement.ImageLoaderManager}
 * base onn the <T> param.
 * @param <T> could be {@link fm.bling.blingy.utils.imagesManagement.MultiImageLoader}
 *           or {@link fm.bling.blingy.utils.imagesManagement.ImageLoaderManager}
 */
public interface BaseRecyclerAdapter<T> {

    void setImageLoaderManager(T imageloader);
}
