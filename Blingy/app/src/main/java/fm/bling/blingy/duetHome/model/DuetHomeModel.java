package fm.bling.blingy.duetHome.model;

import fm.bling.blingy.duetHome.DuetHomeMVPR;
import fm.bling.blingy.duetHome.repository.DuetHomeRepository;
import fm.bling.blingy.songHome.model.SongHomeFeed;


public class DuetHomeModel implements DuetHomeMVPR.Model {

    private DuetHomeMVPR.Repository mRepository;
    private DuetHomeMVPR.Presenter mPresenter;

    public DuetHomeModel(DuetHomeMVPR.Presenter presenter){
        this.mPresenter = presenter;
        this.mRepository = new DuetHomeRepository(this);
    }

    @Override
    public void getDuetHome(int page) {
        mRepository.getDuetHome(page);
    }

    @Override
    public void loadDuetHomeData(DuetHomeFeed data) {
        mPresenter.loadDuetHomeData(data);
    }

    @Override
    public void loadDataFailed() {
        mPresenter.loadDataFailed();
    }
}
