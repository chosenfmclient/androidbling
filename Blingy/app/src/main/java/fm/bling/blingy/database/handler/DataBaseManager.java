package fm.bling.blingy.database.handler;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import fm.bling.blingy.App;
import fm.bling.blingy.database.contract.DuetContract;
import fm.bling.blingy.database.contract.FollowingContract;
import fm.bling.blingy.database.contract.LikesContract;
import fm.bling.blingy.database.model.ContentValueWrapper;
import fm.bling.blingy.database.model.CursorWrapper;
import fm.bling.blingy.mashup.model.InviteeContact;
import fm.bling.blingy.rest.model.Following;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 08/02/17.
 * History:
 * ***********************************
 */
public class DataBaseManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "blingy_db";
    private static final int DATABASE_VERSION = 1;

    private static final String TAG = "DataBaseManager";

    private DataBaseManager() {
        super(App.getAppContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Initialize database on first use of app
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(LikesContract.CREATE_TABLE);
        db.execSQL(FollowingContract.CREATE_TABLE);
        db.execSQL(DuetContract.CREATE_TABLE);
    }

    /**
     * On database upgrade - drop all tables and re-create the database
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL(LikesContract.DELETE_TABLE);
        db.execSQL(FollowingContract.DELETE_TABLE);
        db.execSQL(DuetContract.DELETE_TABLE);

        // Create tables again
        onCreate(db);
    }

    private static SQLiteDatabase getDatabase(Boolean isWritable) {
        DataBaseManager dbHandler = new DataBaseManager();

        if (isWritable)
            return dbHandler.getWritableDatabase();
        else
            return dbHandler.getReadableDatabase();
    }


    /**
     * Updated if a unique constraint violation occurs for the insert.
     * therefore you must have a primary key that is unique.
     * @param cvw - see {@link ContentValueWrapper} for more info.
     * @return rowId
     */
    static long insertToDB(ContentValueWrapper cvw) {
        SQLiteDatabase db = getDatabase(true);
        long rowId = db.replace(
                cvw.getTableName(),
                null,
                cvw.getContentValue());

        db.close();
        return rowId;
    }

    static void excQuery(String query) {
        SQLiteDatabase db = getDatabase(true);
        db.execSQL(query);
        db.close();
    }

    static boolean updateDB(ContentValueWrapper cvw) {
        SQLiteDatabase db = getDatabase(true);

        int rowsAffected = db.update(
                cvw.getTableName(),
                cvw.getContentValue(),
                cvw.getWhereClause(),
                null);

        db.close();
        return (rowsAffected == 1);
    }


    static boolean deleteFromDB(ContentValueWrapper cvw) {
        SQLiteDatabase db = getDatabase(true);

        int rowsAffected = db.delete(
                cvw.getTableName(),
                cvw.getWhereClause(),
                null);

        db.close();
        return (rowsAffected == 1);
    }

    /**
     * Read from database all that you need, also it is in your
     * responsibility to close the cursor after is not used.
     * @param cw - wrapper to read from {@link CursorWrapper}
     * @return cursor -  a {@link Cursor} object that holds the data.
     */
    static Cursor readFromDB(CursorWrapper cw) {
        SQLiteDatabase db = getDatabase(false);
        Cursor cursor = db.query(
                cw.getTableName(),
                cw.isAllCols() ? null : cw.getColumns(),
                cw.getWhereClause(), null, "", "", "");
        db.close();
        return cursor;
    }

    static String getLike(String video_id){
        SQLiteDatabase db = getDatabase(false);
        CursorWrapper cw = new CursorWrapper(LikesContract.TABLE_NAME)
                .addColumn(LikesContract.COLUMN_VIDEO_ID)
                .setWhereClause(LikesContract.COLUMN_VIDEO_ID + " = " + video_id);

        String returnedLike = null;
        Cursor cursor = db.query(
                cw.getTableName(),
                cw.isAllCols() ? null : cw.getColumns(),
                cw.getWhereClause(), null, "", "", "");
        try {
            if (cursor != null && cursor.moveToFirst()) {
                int video_id_index = cursor.getColumnIndex(LikesContract.COLUMN_VIDEO_ID);
                returnedLike = cursor.getString(video_id_index);
            }
        }catch (Throwable throwable){
            Log.e(TAG, throwable.getMessage());
        }finally {
            db.close();
            cursor.close();
        }
        return  returnedLike;
    }

    static ArrayList<String> getAllLikes(String filter) {
        SQLiteDatabase db = getDatabase(false);
        CursorWrapper cw = new CursorWrapper(LikesContract.TABLE_NAME)
                .addColumn(LikesContract.COLUMN_VIDEO_ID);

        if(filter != null && !filter.isEmpty())
            cw.setWhereClause(filter);

        ArrayList<String> likeList = new ArrayList<>();
        Cursor cursor = db.query(
                cw.getTableName(),
                cw.isAllCols() ? null : cw.getColumns(),
                cw.getWhereClause(), null, "", "", "");

        try {
            if (cursor != null && cursor.moveToFirst()) {
                int video_id_index = cursor.getColumnIndex(LikesContract.COLUMN_VIDEO_ID);
                do {
                    likeList.add(cursor.getString(video_id_index));
                } while (cursor.moveToNext());
            }
        }catch (Throwable throwable){
            Log.e(TAG, throwable.getMessage());
        }finally {
            db.close();
            cursor.close();
        }
        return likeList;
    }

    static String getFollowing(String user_id){
        SQLiteDatabase db = getDatabase(false);
        CursorWrapper cw = new CursorWrapper(FollowingContract.TABLE_NAME)
                .addColumn(FollowingContract.COLUMN_USER_ID)
                .setWhereClause(FollowingContract.COLUMN_USER_ID + " = " + user_id);

        String returnedFollowing = null;
        Cursor cursor = db.query(
                cw.getTableName(),
                cw.isAllCols() ? null : cw.getColumns(),
                cw.getWhereClause(), null, "", "", "");
        try {
            if (cursor != null && cursor.moveToFirst()) {
                int user_id_index = cursor.getColumnIndex(FollowingContract.COLUMN_USER_ID);
                returnedFollowing = cursor.getString(user_id_index);
            }
        }catch (Throwable throwable){
            Log.e(TAG, throwable.getMessage());
        }finally {
            db.close();
            cursor.close();
        }
        return  returnedFollowing;
    }

    static ArrayList<String> getAllFollowing(String filter) {
        SQLiteDatabase db = getDatabase(false);
        CursorWrapper cw = new CursorWrapper(FollowingContract.TABLE_NAME)
                .addColumn(FollowingContract.COLUMN_USER_ID);

        if(filter != null && !filter.isEmpty())
            cw.setWhereClause(filter);

        ArrayList<String> followingList = new ArrayList<>();
        Cursor cursor = db.query(
                cw.getTableName(),
                cw.isAllCols() ? null : cw.getColumns(),
                cw.getWhereClause(), null, "", "", "");

        try {
            if (cursor != null && cursor.moveToFirst()) {
                int user_id_index = cursor.getColumnIndex(FollowingContract.COLUMN_USER_ID);
                do {
                    followingList.add(cursor.getString(user_id_index));
                } while (cursor.moveToNext());
            }
        }catch (Throwable throwable){
            Log.e(TAG, throwable.getMessage());
        }finally {
            db.close();
            cursor.close();
        }
        return followingList;
    }

    static ArrayList<InviteeContact> getAllInvted(String filter) {
        SQLiteDatabase db = getDatabase(false);
        CursorWrapper cw = new CursorWrapper(DuetContract.TABLE_NAME);

        if(filter != null && !filter.isEmpty())
            cw.setWhereClause(filter);

        ArrayList<InviteeContact> invited = new ArrayList<>();
        Cursor cursor = db.query(
                cw.getTableName(),
                cw.isAllCols() ? null : cw.getColumns(),
                cw.getWhereClause(), null, "", "", "");

        try {
            if (cursor != null && cursor.moveToFirst()) {
                int video_id_index = cursor.getColumnIndex(DuetContract.COLUMN_VIDEO_ID);
                int user_id_index = cursor.getColumnIndex(DuetContract.COLUMN_USER_ID);
                int user_name_index = cursor.getColumnIndex(DuetContract.COLUMN_USER_NAME);
                int email_index = cursor.getColumnIndex(DuetContract.COLUMN_EMAIL);
                int phone_index = cursor.getColumnIndex(DuetContract.COLUMN_PHONE);
                int photo_index = cursor.getColumnIndex(DuetContract.COLUMN_PHOTO);

                do {
                    String videoId = cursor.getString(video_id_index);
                    String userId = cursor.getString(user_id_index);
                    InviteeContact contact;
                    if(userId == null)
                        contact = new InviteeContact(cursor.getString(user_name_index),cursor.getString(phone_index),cursor.getString(email_index),cursor.getString(photo_index),videoId);
                    else
                        contact = new InviteeContact(cursor.getString(user_name_index),userId, cursor.getString(photo_index),videoId);
                    invited.add(contact);
                } while (cursor.moveToNext());
            }
        }catch (Throwable throwable){
            Log.e(TAG, throwable.getMessage());
        }finally {
            db.close();
            cursor.close();
        }
        return invited;
    }


}
