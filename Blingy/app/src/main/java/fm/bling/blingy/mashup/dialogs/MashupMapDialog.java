//package fm.bling.blingy.mashup.dialogs;
//
//import android.animation.ObjectAnimator;
//import android.animation.PropertyValuesHolder;
//import android.app.Dialog;
//import android.content.Context;
//import android.os.Handler;
//import android.view.Gravity;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewTreeObserver;
//import android.view.WindowManager;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import fm.bling.blingy.App;
//import fm.bling.blingy.R;
//import fm.bling.blingy.mashup.listeners.StepsCallback;
//import fm.bling.blingy.record.Utils;
//import fm.bling.blingy.utils.imagesManagement.MultiImageLoader;
//import fm.bling.blingy.utils.imagesManagement.views.ScaledImageView;
//import fm.bling.blingy.utils.interpolators.CustomBounceInterpolator;
//
//import static android.view.View.ALPHA;
//import static android.view.View.SCALE_X;
//import static android.view.View.SCALE_Y;
//import static android.view.View.TRANSLATION_X;
//
///**
// * *********************************
// * Project: Blin.gy Android Application
// * Description:
// * Created by Oren Zakay on 12/18/16.
// * History:
// * ***********************************
// */
//public class MashupMapDialog extends Dialog implements View.OnClickListener{
//
//    public static final int RECORD_VIDEO = 1;
//    public static final int INVITE_FRIENDS = 2;
//    public static final int COLLECT_VIDEOS = 3;
//    public static final int CREATE_MASHUP = 4;
//
//    /**
//     * Main Views
//     */
//    private Context mContext;
//    private LinearLayout mMainLayout;
////    private ConstraintLayout mMainLayout;
//    private LinearLayout mShareContainer;
//    private TextView mTitle;
//    private ScaledImageView mFirstStep;
//    private ScaledImageView mSecondStep;
//    private ImageView mThirdStep;
//    private ImageView mFourthStep;
//    private ImageView mShareStep;
//    private TextView mFirstText;
//    private TextView mSecondText;
//    private TextView mThirdText;
//    private TextView mFourthText;
//    private TextView mShareText;
//    private TextView mNewLable;
//
//
//    private View mBaseLine;
//    private View mFirstLine;
//    private View mSecLine;
//    private View mThirdLine;
//    private View mFourthLine;
//    private View mFifthLine;
//
//    /**
//     * Utils
//     */
//    private MultiImageLoader mImageLoaderManager;
//    private String mURLuserPicture = null;
//    private int mStep;
//    private int padding = (int)(35f * App.SCALE_X);
//    private int paddingSmall = (int)(15f * App.SCALE_X);
//    private boolean startAnimation = false;
//    private boolean dismissed;
//    private StepsCallback mStepsCallback;
//
//    private Handler mHandler;
//    private Animation bounceShareAnim;
//    private ObjectAnimator firstStepAnim, firstLineAnim, firstTextAnim;
//    private ObjectAnimator secStepAnim, secLineAnim, secTextAnim;
//    private ObjectAnimator thirdStepAnim, thirdLineAnim, thirdTextAnim;
//    private ObjectAnimator fourthStepAnim, fourthLineAnim, fourthTextAnim;
//    private ObjectAnimator shareStepAnim, shareTextAnim, shareContainerAnim, fadeinShareText;
//
//    private MashupMapDialog(MapBuilder builder){
//        super(builder.context, android.R.style.Theme_Panel);
//        setContentView(R.layout.mashup_map);
//        this.mContext = builder.context;
//        this.mStep = builder.step;
//        this.mStepsCallback = builder.stepsCallback;
//        this.startAnimation = builder.startAnimation;
//        this.mURLuserPicture = builder.imageUrl;
//        this.mImageLoaderManager = builder.mImageLoaderManager;
//        init();
//        if(builder.titleResID > 0)
//            setDialogTitle(builder.titleResID);
//
//    }
////    public MashupMapDialog(Context context, int step, StepsCallback stepsCallback, boolean startAnimation) {
////        super(context, android.R.style.Theme_Panel);
////        setContentView(R.layout.mashup_map);
////        this.mContext = context;
////        this.mStep = step;
////        this.mStepsCallback = stepsCallback;
////        this.startAnimation = startAnimation;
////        init();
////    }
////
////    public MashupMapDialog(Context context, int step, StepsCallback stepsCallback, int titleResID, boolean startAnimation) {
////        super(context, android.R.style.Theme_Panel);
////        setContentView(R.layout.mashup_map);
////        this.mContext = context;
////        this.mStep = step;
////        this.mStepsCallback = stepsCallback;
////        this.startAnimation = startAnimation;
////        init();
////        setDialogTitle(titleResID);
////    }
////
////    public MashupMapDialog(Context context, int step, StepsCallback stepsCallback, int titleResID, boolean startAnimation,String imageUrl) {
////        super(context, android.R.style.Theme_Panel);
////        setContentView(R.layout.mashup_map);
////        this.mContext = context;
////        this.mStep = step;
////        this.mStepsCallback = stepsCallback;
////        this.startAnimation = startAnimation;
////        this.mURLuserPicture = imageUrl;
////        init();
////        setDialogTitle(titleResID);
////    }
//
//
//    private void setDialogTitle(int titleResID) {
//        mTitle.setText(titleResID);
//    }
//
//    private void init() {
//        this.mMainLayout = (LinearLayout) findViewById(R.id.main_container);
//        this.mTitle = (TextView) findViewById(R.id.title_bar);
//        this.mFirstStep = (ScaledImageView) findViewById(R.id.first_step);
//        this.mSecondStep = (ScaledImageView) findViewById(R.id.second_step);
//        this.mThirdStep = (ImageView) findViewById(R.id.third_step);
//        this.mFourthStep = (ImageView) findViewById(R.id.fourth_step);
//        this.mFirstText = (TextView) findViewById(R.id.record_video_text);
//        this.mSecondText = (TextView) findViewById(R.id.invite_friends_text);
//        this.mThirdText = (TextView) findViewById(R.id.collect_videos_text);
//        this.mFourthText = (TextView) findViewById(R.id.create_mashup_text);
//        this.mBaseLine = findViewById(R.id.base_line);
//        this.mFirstLine = findViewById(R.id.first_line);
//        this.mSecLine = findViewById(R.id.sec_line);
//        this.mThirdLine = findViewById(R.id.third_line);
//        this.mFourthLine = findViewById(R.id.fourth_line);
//        this.mFifthLine = findViewById(R.id.fifth_line);
//        this.findViewById(R.id.close_button).setOnClickListener(this);
//
//        this.mHandler = new Handler();
//        this.mShareContainer = (LinearLayout)findViewById(R.id.share_container);
//        this.mShareStep = (ImageView) findViewById(R.id.share_step);
//        this.mShareText = (TextView) findViewById(R.id.share_text);
//        this.mNewLable = (TextView) findViewById(R.id.new_text);
//
//
//
//        if(mHandler == null && startAnimation) {
//
//            this.bounceShareAnim = AnimationUtils.loadAnimation(mContext, R.anim.bounce_with_custom_interpolator);
//            this.bounceShareAnim.setInterpolator(new CustomBounceInterpolator(0.3, 30));
//
//            this.mShareStep.setPadding(padding,padding,padding,padding);
//            this.mShareStep.setBackgroundResource(R.drawable.circle_main_blue_with_stroke);
//            this.mShareStep.setOnClickListener(this);
//
//        }
//
//        this.mFirstStep.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                mFirstText.setX((int)mFirstStep.getX() - ((mFirstText.getWidth() / 2) - (mFirstStep.getWidth() / 2)));
//                mFirstStep.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                mBaseLine.setX(mFirstStep.getX());
//            }
//        });
//        this.mSecondStep.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                mSecondText.setX((int)mSecondStep.getX() - ((mSecondText.getWidth() / 2) - (mSecondStep.getWidth() / 2)));
//                mSecondStep.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//            }
//        });
//        this.mThirdStep.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                mThirdText.setX((int)mThirdStep.getX() - ((mThirdText.getWidth() / 2) - (mThirdStep.getWidth() / 2)));
//                mThirdStep.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//
//            }
//        });
//        this.mFourthStep.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                mFourthText.setX((int)mFourthStep.getX() - ((mFourthText.getWidth() / 2) - (mFourthStep.getWidth() / 2)));
//                mFourthStep.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                mBaseLine.getLayoutParams().width = (int)(mFourthStep.getX() - mFirstLine.getX());
//                createAnimators();
//            }
//        });
//
//        setupStep();
//    }
//
//    private void setupStep() {
//        mFirstStep.setPadding(0,0,0,0);
//        mSecondStep.setPadding(0,0,0,0);
//        mThirdStep.setPadding(0,0,0,0);
//        mFourthStep.setPadding(0,0,0,0);
//
//
//        switch (mStep){
//            default:
//            case RECORD_VIDEO:
//                mFirstStep.setPadding(padding,padding,padding,padding);
//                mFirstStep.setImageResource(R.drawable.ic_record_button_start_24dp);
//                mFirstStep.setBackgroundResource(R.drawable.circle_main_blue_with_stroke);
//                mFirstStep.setOnClickListener(this);
//                mSecondStep.setImageResource(0);
//                mSecondStep.setBackgroundResource(R.drawable.circle_white_stroke);
//                mThirdStep.setImageResource(0);
//                mThirdStep.setBackgroundResource(R.drawable.circle_white_stroke);
//                mFourthStep.setImageResource(0);
//                mFourthStep.setBackgroundResource(R.drawable.circle_white_stroke);
//                break;
//            case INVITE_FRIENDS:
//                mSecondStep.setPadding(padding,padding,padding,padding);
//                mSecondStep.setImageResource(R.drawable.ic_group_invite_friends_24dp);
//                mSecondStep.setBackgroundResource(R.drawable.circle_main_blue_with_stroke);
//                mSecondStep.setOnClickListener(this);
//                mFirstStep.setPadding(paddingSmall,paddingSmall,paddingSmall,paddingSmall);
//                mFirstStep.setImageResource(R.drawable.ic_group_completed_v_24dp);
//                mFirstStep.setBackgroundResource(R.drawable.white_circle);
//                mThirdStep.setImageResource(0);
//                mThirdStep.setBackgroundResource(R.drawable.circle_white_stroke);
//                mFourthStep.setImageResource(0);
//                mFourthStep.setBackgroundResource(R.drawable.circle_white_stroke);
//                break;
//            case COLLECT_VIDEOS:
//                if(mURLuserPicture != null){
//                    mFirstStep.setUrl(mURLuserPicture);
//                    mFirstStep.setIsRoundedImage(true);
//                    mFirstStep.setImageLoader(mImageLoaderManager);
//                    mFirstStep.setKeepAspectRatioAccordingToWidth(true);
//                    mFirstStep.setWidthInternal(Utils.getDimensionPixelSize(R.dimen.userpic_small));
//                    mImageLoaderManager.DisplayImage(mFirstStep);
////                    mFirstStep.setAutoShowRecycle(true);
//                    mSecondStep.setUrl(mURLuserPicture);
//                    mSecondStep.setIsRoundedImage(true);
//                    mSecondStep.setKeepAspectRatioAccordingToWidth(true);
//                    mSecondStep.setImageLoader(mImageLoaderManager);
//                    mSecondStep.setWidthInternal(Utils.getDimensionPixelSize(R.dimen.userpic_small));
//                    mImageLoaderManager.DisplayImage(mSecondStep);
////                    mSecondStep.setAutoShowRecycle(true);
//                }else {
//                    mFirstStep.setImageResource(R.drawable.ic_group_completed_v_24dp);
//                    mSecondStep.setImageResource(R.drawable.ic_group_completed_v_24dp);
//                }
//
//                mThirdStep.setPadding(padding,padding,padding,padding);
//                mThirdStep.setImageResource(R.drawable.ic_record_button_start_24dp);
//                mThirdStep.setBackgroundResource(R.drawable.circle_main_blue_with_stroke);
//                mThirdStep.setOnClickListener(this);
//                mFirstStep.setPadding(paddingSmall,paddingSmall,paddingSmall,paddingSmall);
//                mFirstStep.setBackgroundResource(R.drawable.white_circle);
//                mSecondStep.setPadding(paddingSmall,paddingSmall,paddingSmall,paddingSmall);
//                mSecondStep.setBackgroundResource(R.drawable.white_circle);
//                mFourthStep.setPadding(paddingSmall,paddingSmall,paddingSmall,paddingSmall);
//                mFourthStep.setImageResource(0);
//                mFourthStep.setBackgroundResource(R.drawable.circle_white_stroke);
//                break;
//            case CREATE_MASHUP:
//                mTitle.setText(R.string.create_mashup_now);
//                mFourthStep.setPadding(padding,padding,padding,padding);
//                mFourthStep.setImageResource(R.drawable.ic_group_mix_now_24dp);
//                mFourthStep.setBackgroundResource(R.drawable.circle_main_blue_with_stroke);
//                mFourthStep.setOnClickListener(this);
//                mFirstStep.setPadding(paddingSmall,paddingSmall,paddingSmall,paddingSmall);
//                mFirstStep.setImageResource(R.drawable.ic_group_completed_v_24dp);
//                mFirstStep.setBackgroundResource(R.drawable.white_circle);
//                mSecondStep.setPadding(paddingSmall,paddingSmall,paddingSmall,paddingSmall);
//                mSecondStep.setImageResource(R.drawable.ic_group_completed_v_24dp);
//                mSecondStep.setBackgroundResource(R.drawable.white_circle);
//                mThirdStep.setPadding(paddingSmall,paddingSmall,paddingSmall,paddingSmall);
//                mThirdStep.setImageResource(R.drawable.ic_group_completed_v_24dp);
//                mThirdStep.setBackgroundResource(R.drawable.white_circle);
//                break;
//        }
//
//        if(startAnimation) {
//            mFourthStep.setPadding(paddingSmall, paddingSmall, paddingSmall, paddingSmall);
//            mFourthStep.setImageResource(R.drawable.ic_group_completed_v_24dp);
//            mFourthStep.setBackgroundResource(R.drawable.white_circle);
//            mFourthStep.setOnClickListener(this);
//        }
//    }
//
//    private void createAnimators(){
//        PropertyValuesHolder pvhX1 = PropertyValuesHolder.ofFloat(TRANSLATION_X, -(50f * App.SCALE_X));
//        PropertyValuesHolder scaleX1 = PropertyValuesHolder.ofFloat(SCALE_X, 0.5f);
//        PropertyValuesHolder scaleY1 = PropertyValuesHolder.ofFloat(SCALE_Y, 0.5f);
//        PropertyValuesHolder pvhX1T = PropertyValuesHolder.ofFloat(TRANSLATION_X, mFirstText.getX() - (50f * App.SCALE_X));
//
//        PropertyValuesHolder scaleX1L = PropertyValuesHolder.ofFloat(SCALE_X, 0.6f);
//        PropertyValuesHolder pvhX1L = PropertyValuesHolder.ofFloat(TRANSLATION_X, -(110f * App.SCALE_X));
//
//        firstStepAnim = ObjectAnimator.ofPropertyValuesHolder(mFirstStep, pvhX1, scaleX1, scaleY1).setDuration(1000);
//        firstTextAnim = ObjectAnimator.ofPropertyValuesHolder(mFirstText, pvhX1T).setDuration(1000);
//        firstLineAnim = ObjectAnimator.ofPropertyValuesHolder(mFirstLine, pvhX1L, scaleX1L).setDuration(1000);
//
//        PropertyValuesHolder pvhX2 = PropertyValuesHolder.ofFloat(TRANSLATION_X, -(170f * App.SCALE_X));
//        PropertyValuesHolder pvhX2T = PropertyValuesHolder.ofFloat(TRANSLATION_X, mSecondText.getX(), mSecondText.getX() - (170f * App.SCALE_X ));
//
//        PropertyValuesHolder pvhX2L = PropertyValuesHolder.ofFloat(TRANSLATION_X, -(230f * App.SCALE_X));
//
//        secStepAnim = ObjectAnimator.ofPropertyValuesHolder(mSecondStep, pvhX2, scaleX1, scaleY1).setDuration(1000);
//        secTextAnim = ObjectAnimator.ofPropertyValuesHolder(mSecondText, pvhX2T).setDuration(1000);
//        secLineAnim = ObjectAnimator.ofPropertyValuesHolder(mSecLine, pvhX2L,scaleX1L).setDuration(1000);
//
//        PropertyValuesHolder pvhX3 = PropertyValuesHolder.ofFloat(TRANSLATION_X, -(290f * App.SCALE_X));
//        PropertyValuesHolder pvhX3T = PropertyValuesHolder.ofFloat(TRANSLATION_X, mThirdText.getX(), mThirdText.getX() - (290f * App.SCALE_X ));
//
//        PropertyValuesHolder pvhX3L = PropertyValuesHolder.ofFloat(TRANSLATION_X, -(350f * App.SCALE_X));
//
//        thirdStepAnim = ObjectAnimator.ofPropertyValuesHolder(mThirdStep, pvhX3, scaleX1, scaleY1).setDuration(1000);
//        thirdTextAnim = ObjectAnimator.ofPropertyValuesHolder(mThirdText, pvhX3T).setDuration(1000);
//        thirdLineAnim = ObjectAnimator.ofPropertyValuesHolder(mThirdLine, pvhX3L,scaleX1L).setDuration(1000);
//
//        PropertyValuesHolder pvhX4 = PropertyValuesHolder.ofFloat(TRANSLATION_X, -(410f * App.SCALE_X));
//        PropertyValuesHolder pvhX4T = PropertyValuesHolder.ofFloat(TRANSLATION_X, mFourthText.getX(), mFourthText.getX() - (400f * App.SCALE_X ));
//
//        fourthStepAnim = ObjectAnimator.ofPropertyValuesHolder(mFourthStep, pvhX4, scaleX1, scaleY1).setDuration(1000);
//        fourthTextAnim = ObjectAnimator.ofPropertyValuesHolder(mFourthText, pvhX4T).setDuration(1000);
//        fourthLineAnim = ObjectAnimator.ofPropertyValuesHolder(mFourthLine, pvhX3L).setDuration(1000);
//
//        PropertyValuesHolder alpha = PropertyValuesHolder.ofFloat(ALPHA, 0.0f, 1.0f);
//        shareStepAnim = ObjectAnimator.ofPropertyValuesHolder(mShareStep, alpha).setDuration(1000);
//
//        PropertyValuesHolder shareT = PropertyValuesHolder.ofFloat(TRANSLATION_X, App.WIDTH, mShareStep.getX() + (15f * App.SCALE_X));
//        shareTextAnim = ObjectAnimator.ofPropertyValuesHolder(mShareText, shareT).setDuration(1);
//
//        float start = App.WIDTH + mFourthLine.getX();
//        PropertyValuesHolder traX = PropertyValuesHolder.ofFloat(TRANSLATION_X, start , mContext.getResources().getDimensionPixelOffset(R.dimen.padding_tiny));
//        shareContainerAnim = ObjectAnimator.ofPropertyValuesHolder(mShareContainer, traX).setDuration(1000);
//
//        PropertyValuesHolder alphaS = PropertyValuesHolder.ofFloat(ALPHA, 0.0f, 1.0f);
//        fadeinShareText = ObjectAnimator.ofPropertyValuesHolder(mShareText, alphaS).setDuration(2000);
//    }
//
//    private void startShareAnimation(){
//
//        firstStepAnim.start();
//        firstTextAnim.start();
//
//        firstLineAnim.start();
//
//        secStepAnim.start();
//        secTextAnim.start();
//
//        secLineAnim.start();
//
//        thirdStepAnim.start();
//        thirdTextAnim.start();
//
//        thirdLineAnim.start();
//
//        fourthStepAnim.start();
//        fourthTextAnim.start();
//
//        shareContainerAnim.start();
//
//        shareTextAnim.start();
//        fadeinShareText.start();
//        mShareText.setVisibility(View.VISIBLE);
//
//        mShareStep.setVisibility(View.VISIBLE);
//        mFourthLine.setVisibility(View.VISIBLE);
//        mFifthLine.setVisibility(View.VISIBLE);
//        mNewLable.setVisibility(View.VISIBLE);
//
//        mShareStep.startAnimation(bounceShareAnim);
//    }
//
//    @Override
//    public void show() {
//        WindowManager.LayoutParams lp = getWindow().getAttributes();
//        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
//
//        Animation slideIn = AnimationUtils.loadAnimation(mContext, R.anim.slide_up);
//        slideIn.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                mMainLayout.setVisibility(View.VISIBLE);
//                mHandler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if(startAnimation)
//                            startShareAnimation();
//                    }
//                },1000);
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//            }
//        });
//
//        super.show();
//        mMainLayout.startAnimation(slideIn);
//    }
//
//    public void dismiss() {
//        if(mHandler != null) {
//            mHandler.removeCallbacksAndMessages(null);
//            mHandler = null;
//        }
//        if (dismissed)
//            return;
//        dismissed = true;
//        Animation slideOut = AnimationUtils.loadAnimation(mContext, R.anim.slide_down);
//        slideOut.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                mMainLayout.setVisibility(View.INVISIBLE);
//                superDismiss();
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//            }
//        });
//        mMainLayout.startAnimation(slideOut);
//    }
//
//    public boolean onTouchEvent(MotionEvent me) {
//        if (!dismissed) {
//            if (me.getAction() == MotionEvent.ACTION_DOWN) {
//                if (me.getY() < mMainLayout.getTop())
//                {
//                    dismiss();
//                }
//            }
//        }
//        return true;
//    }
//
//    private void superDismiss() {
//        super.dismiss();
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        mMainLayout.removeAllViews();
//        mMainLayout.destroyDrawingCache();
//        mMainLayout = null;
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()){
//            default:
//            case R.id.close_button:
//                break;
//            case R.id.first_step:
//                mStepsCallback.onFirstStepClicked();
//                break;
//            case R.id.second_step:
//                mStepsCallback.onSecondStepClicked();
//                break;
//            case R.id.third_step:
//                mStepsCallback.onThirdStepClicked();
//                break;
//            case R.id.fourth_step:
//                mStepsCallback.onFourthStepClicked();
//                break;
//            case R.id.share_step:
//                mStepsCallback.onShareStepClicked();
//                break;
//        }
//        dismiss();
//    }
//
//
//    /**
//     * Map Builder
//     */
//    public static class MapBuilder{
//        private Context context;
//        private int step;
//        private StepsCallback stepsCallback;
//        private int titleResID = -1;
//        private boolean startAnimation;
//        private String imageUrl;
//        private MultiImageLoader mImageLoaderManager;
//
//        public MapBuilder(Context context, int step, StepsCallback stepsCallback, boolean startAnimation){
//            this.context = context;
//            this.step = step;
//            this.stepsCallback = stepsCallback;
//            this.startAnimation = startAnimation;
//        }
//
//        public MapBuilder setTitleRes(int titleResID){
//            this.titleResID = titleResID;
//            return this;
//        }
//
//        public MapBuilder setImageLoader(MultiImageLoader multiImageLoader){
//            this.mImageLoaderManager = multiImageLoader;
//            return this;
//        }
//
//        public MapBuilder setImageUrl(String url){
//            this.imageUrl = url;
//            return this;
//        }
//
//        public MashupMapDialog build(){
//            return new MashupMapDialog(this);
//        }
//
//    }
//}
