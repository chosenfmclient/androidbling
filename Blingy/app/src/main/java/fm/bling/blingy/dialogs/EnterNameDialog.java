package fm.bling.blingy.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.dialogs.listeners.EnterNameDialogListener;
import fm.bling.blingy.singletones.CAAUserDataSingleton;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.utils.imagesManagement.views.ClickableView;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/25/16.
 * History:
 * ***********************************
 */
public class EnterNameDialog extends Dialog
{
    private FrameLayout fullScreenView;
    private TableLayout main;

    private TextView title;
    private EditText nameEditText;
    private LinearLayout clickLinearLayout;
    private ClickableView clickAbleView;
    private Context mContext;
    private boolean dismissed;
    private EnterNameDialogListener mEnterNameDialogListener;
    private int mWrongMessage;
    private int mMinimumChars;

    /**
     *
     * @param context
     * @param titleId
     * @param wrongMessage  ( -1 ) to not show any wrong message
     * @param minimumChars  ( -1 ) to not check for minimum chars
     * @param enterNameDialogListener
     */
    public EnterNameDialog(final Context context, int titleId, int wrongMessage, int minimumChars, EnterNameDialogListener enterNameDialogListener)
    {
        super(context, android.R.style.Theme_Panel);
        mContext = context;
        mEnterNameDialogListener = enterNameDialogListener;
        mWrongMessage = wrongMessage;
        mMinimumChars = minimumChars;

        int padding = context.getResources().getDimensionPixelSize(R.dimen.blocks_margin_card_side);


        title = new TextView(context);
        title.setLayoutParams(new ViewGroup.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
        title.setText(titleId);
        title.setTypeface(App.ROBOTO_REGULAR);
        title.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(R.dimen.font_title_medium));
        title.setTextColor(context.getResources().getColor(R.color.black));
        title.setPadding(0, 0, 0, padding);
        title.setGravity(Gravity.CENTER);

        nameEditText = new EditText(context);
        nameEditText.setHint(R.string.invite_enter_your_name);
        nameEditText.setSingleLine();
        nameEditText.setInputType(EditorInfo.TYPE_TEXT_FLAG_CAP_SENTENCES);
        nameEditText.setLayoutParams(new ViewGroup.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
        nameEditText.setTextColor(context.getResources().getColor(R.color.black));
        nameEditText.setTypeface(App.ROBOTO_REGULAR);
        nameEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(R.dimen.font_title_medium));
        nameEditText.setId(R.id.enter_your_name);

        clickAbleView = new ClickableView(context);
        clickAbleView.setBackgroundResource(R.drawable.arrow_icon_blue_perform);
        clickAbleView.setLayoutParams(new ViewGroup.LayoutParams((int) (200f * App.SCALE_X), (int) (200f * App.SCALE_X)));
        clickAbleView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String name = nameEditText.getText().toString().trim();
                if (mMinimumChars != -1 && name.length() < mMinimumChars)
                {
                    if(mWrongMessage != -1)
                        Toast.makeText(v.getContext(), mWrongMessage, Toast.LENGTH_LONG).show();
                    return;
                }
                CAAUserDataSingleton.getInstance().setFirstName(name);
                CAAUserDataSingleton.getInstance().saveName(v.getContext());
                SharedPreferencesManager.getEditor().putString("temp_name", name);
                SharedPreferencesManager.getEditor().commit();
                mEnterNameDialogListener.onDoneClicked();
                dismiss();
            }
        });
        clickAbleView.setClickable(true);
        clickAbleView.setId(R.id.send);

        clickLinearLayout = new LinearLayout(context);
        clickLinearLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        clickLinearLayout.setGravity(Gravity.CENTER);
        clickLinearLayout.addView(clickAbleView);

        main = new TableLayout(context);
        main.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT));
        main.setBackgroundColor(context.getResources().getColor(R.color.white));
        main.setPadding(padding, padding, padding, padding);
        main.addView(title);
        main.addView(nameEditText);
        main.addView(clickLinearLayout);
        main.setVisibility(View.INVISIBLE);

        fullScreenView = new FrameLayout(context);
        fullScreenView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        fullScreenView.addView(main);
        fullScreenView.setForegroundGravity(Gravity.TOP);
        setContentView(fullScreenView);
    }

    public void show()
    {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.gravity = Gravity.TOP|Gravity.LEFT;
        lp.x = 0; lp.y = 0;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width  = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);
        super.show();

        Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.slide_down_from_top);
        anim.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                main.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }
        });
        main.startAnimation(anim);
    }

    public void dismiss()
    {
        if(dismissed)
            return;
        dismissed = true;
        Animation slideOut = AnimationUtils.loadAnimation(main.getContext(), R.anim.slide_up_to_top);
        slideOut.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {

            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                main.setVisibility(View.INVISIBLE);
                superDismiss();
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {

            }
        });
        main.startAnimation(slideOut);
    }

    public boolean onTouchEvent(MotionEvent me)
    {
        if(!dismissed)
        {
            if(me.getAction() == MotionEvent.ACTION_DOWN)
            {
                if(me.getX() < main.getLeft() || me.getX() > main.getRight() || me.getY() < main.getTop() || me.getY() > main.getBottom())
                {
                    mEnterNameDialogListener.onCancel();
                    dismiss();
                }
            }
        }
        return true;
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        mEnterNameDialogListener.onCancel();
    }

    private void superDismiss()
    {
        super.dismiss();
    }

    @Override
    public void onStop()
    {
        super.onStop();

        fullScreenView.removeAllViews();fullScreenView.destroyDrawingCache();fullScreenView = null;
        main.removeAllViews();main.destroyDrawingCache();main=null;
        title=null;
        nameEditText=null;
        clickAbleView=null;
    }

}