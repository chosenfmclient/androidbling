package fm.bling.blingy.dialogs.share;


import fm.bling.blingy.record.callbacks.ActivityResultsReceiver;
import fm.bling.blingy.record.callbacks.PermissionResultReciever;

/**
 * Created by Zach on 3/1/16.
 */
public interface ShareDialogCallbackSetter {

    void setActivityResultsReceiver(ActivityResultsReceiver activityResultsReceiver);

    void setPermissionResultReiever(PermissionResultReciever permissionResultReiever);


}