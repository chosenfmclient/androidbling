package fm.bling.blingy.dialogs.model;

import com.google.gson.annotations.SerializedName;

/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAType
 * Description:
 * Created by Dawidowicz Nadav on 7/2/15.
 * History:
 * ***********************************
 */
public class CAAType {
    @SerializedName("type")
    private String type;

    public CAAType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
