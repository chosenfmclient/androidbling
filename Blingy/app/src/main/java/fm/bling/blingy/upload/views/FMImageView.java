package fm.bling.blingy.upload.views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 03/08/2016.
 * History:
 * ***********************************
 */
public class FMImageView extends ImageView
{
    private boolean isAttached = true;

    public FMImageView(Context context)
    {
        super(context);
    }

    public FMImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public FMImageView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public void draw(Canvas canvas)
    {
        try
        {
            super.draw(canvas);
        }
        catch (Throwable err)
        {

        }
    }

    public boolean isAttached()
    {
        return isAttached;
    }

//    @Override
//    protected void onAttachedToWindow()
//    {
//        super.onAttachedToWindow();
//        isAttached = true;
//    }
//
//    @Override
//    protected void onDetachedFromWindow()
//    {
//        super.onDetachedFromWindow();
//        isAttached = false;
//    }

}