package fm.bling.blingy.utils.views;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import fm.bling.blingy.App;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/28/16.
 * History:
 * ***********************************
 */
public class TextViewLight extends AppCompatTextView {
    public TextViewLight(Context context) {
        super(context);
        this.setTypeface(App.ROBOTO_LIGHT);
    }

    public TextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(App.ROBOTO_LIGHT);
    }

    public TextViewLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setTypeface(App.ROBOTO_LIGHT);
    }
}
