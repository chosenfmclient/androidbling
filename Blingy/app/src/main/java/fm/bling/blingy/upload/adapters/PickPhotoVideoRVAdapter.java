package fm.bling.blingy.upload.adapters;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.upload.FileManager;
import fm.bling.blingy.upload.views.FMImageView;
import fm.bling.blingy.utils.FFMPEG;


/**
 * *********************************
 * Chosen inc. All rights reserved.
 * Description: Android Application
 * Created by Ben Levi on 08/07/2016.
 * History:
 * ***********************************
 */
public class PickPhotoVideoRVAdapter extends RecyclerView.Adapter<PickPhotoVideoRVAdapter.DataObjectHolder>
{
    private ArrayList<DataStructure> mDataSet;
    private final int tileWidthHeight, padding;
    private onFolderSelectedListener mOnFolderSelectedListener;
    private FileManager mFileManager;
    private ExecutorService executorService;
    private Activity mActivity;
    private ArrayList<DataObjectHolder>dataObjectHolders;

    public PickPhotoVideoRVAdapter(Activity activity, FileManager fileManager, onFolderSelectedListener onFolderSelectedListener)
    {
        mActivity = activity;
        padding = (int) (10f * App.SCALE_Y);
        tileWidthHeight = (int) (((((float)mActivity.getResources().getDisplayMetrics().widthPixels) / 2f) /2f));
        mOnFolderSelectedListener = onFolderSelectedListener;
        executorService = Executors.newFixedThreadPool(20);
        dataObjectHolders = new ArrayList<>();

        mFileManager = fileManager;

        buildDataStructure();
    }

    private void buildDataStructure()
    {
        File[] folders = mFileManager.getFolders();
        mDataSet = new ArrayList<>();

        DataStructure dataStructure;

        for (int i = 0; i < folders.length; i++)
        {
            dataStructure = getDataStructure(folders[i]);
            if(dataStructure != null)
                mDataSet.add(dataStructure);
        }
    }

    private DataStructure getDataStructure(File folder)
    {
        int filesCount = 0;
        String imagePath = null;

        filesCount = mFileManager.getFolderManager(folder.getPath()).getFilePaths().length;
        imagePath = mFileManager.getFolderManager(folder.getPath()).getFilePaths()[0];

//        File[] files = folder.listFiles();
//
//        for (int i = 0; i < files.length; i++)
//        {
//            if (mFileManager.isInExtensions(files[i].getName()))
//            {
//                filesCount++;
//                if (imagePath == null)
//                {
//                    imagePath = files[i].getPath();
//                }
//            }
//        }
        if(imagePath == null)
            return null;
        else
            return new DataStructure(imagePath, folder.getName(), folder.getPath(), filesCount);
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewIndex)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_photo_pick_item, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        dataObjectHolders.add(dataObjectHolder);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        DataStructure tile = mDataSet.get(position);
        holder.rippleView.setTag(tile);

        holder.folderName.setText(tile.folderName);
        holder.filesCount.setText(tile.fileCount + "");
        holder.folderPath = tile.folderPath;

        executorService.submit(new loadImage(holder));
    }

    @Override
    public int getItemCount()
    {
        return mDataSet.size();
    }

    @Override
    public void onViewRecycled(DataObjectHolder holder)
    {
        try
        {
            ((BitmapDrawable) holder.gameImage.getDrawable()).getBitmap().recycle();
        }
        catch (Throwable err)
        {
            try
            {
                ((Bitmap) holder.filesCount.getTag()).recycle();
            }
            catch (Throwable err1)
            {
                err.printStackTrace();
            }
            err.printStackTrace();
        }
        try
        {
            holder.gameImage.setImageBitmap(null);
        }
        catch (Throwable err) {}
        try
        {
            dataObjectHolders.remove(holder);
        }
        catch (Throwable err) {}
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder
    {
        View rippleView;
        FMImageView gameImage;
        TextView folderName;
        TextView filesCount;
        String folderPath;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            rippleView = itemView.findViewById(R.id.folder);
            rippleView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    mOnFolderSelectedListener.onFolderSelected(((DataStructure) v.getTag()).folderName, ((DataStructure) v.getTag()).folderPath, mFileManager.getType(), mFileManager);
                }
            });

            gameImage = (FMImageView) itemView.findViewById(R.id.game_image_pic);
            int fulltileWidthHeight = tileWidthHeight * 2;
            gameImage.getLayoutParams().width = fulltileWidthHeight;
            gameImage.getLayoutParams().height = fulltileWidthHeight;
//            gameImage.setPadding(20, 20, 20, 20);

            folderName = (TextView) itemView.findViewById(R.id.folder_name);
            filesCount = (TextView) itemView.findViewById(R.id.files_count);

            itemView.getLayoutParams().width = fulltileWidthHeight;
            itemView.getLayoutParams().height = fulltileWidthHeight;
        }
    }

    public void close()
    {
        mDataSet.clear();
        mDataSet = null;
        mOnFolderSelectedListener = null;
        executorService.shutdownNow();
        executorService = null;
        mActivity = null;

        try
        {
            for (DataObjectHolder dataObjectHolder : dataObjectHolders)
            {
                try
                {
                    ((BitmapDrawable) dataObjectHolder.gameImage.getDrawable()).getBitmap().recycle();
                }
                catch (Throwable err)
                {
                    try
                    {
                        ((Bitmap) dataObjectHolder.filesCount.getTag()).recycle();
                    }
                    catch (Throwable err1)
                    {
                        err.printStackTrace();
                    }
                    err.printStackTrace();
                }
                try
                {
                    dataObjectHolder.gameImage.setImageBitmap(null);
                }
                catch (Throwable err)
                {
                }
            }
        }
        catch (Throwable err) {}
        try
        {
            dataObjectHolders.clear();
        }
        catch (Throwable err) {}
        dataObjectHolders = null;
    }

    public class DataStructure
    {
        private final String imagePath;
        private final String folderName;
        private final String folderPath;
        private final int fileCount;

        public DataStructure(String imagePath, String folderName, String folderPath, int fileCount)
        {
            this.imagePath = imagePath;
            this.folderName = folderName;
            this.fileCount = fileCount;
            this.folderPath = folderPath;
        }

    }

    public interface onFolderSelectedListener
    {
        void onFolderSelected(String folderName, String folderPath, byte fileManagerType, FileManager fileManager);
    }

    private class loadImage implements Runnable
    {
        DataObjectHolder mHolder;

        private loadImage(DataObjectHolder holder)
        {
            mHolder = holder;
        }

        public void run()
        {
            try
            {
                Bitmap bitmap = null;

                if(mFileManager.getType() == FileManager.PHOTO)
                {
                    if (new File(App.TEMP_MEDIA_FOLDER + ((DataStructure) mHolder.rippleView.getTag()).imagePath.hashCode()).exists())
                    {
                        bitmap = BitmapFactory.decodeFile(App.TEMP_MEDIA_FOLDER + ((DataStructure) mHolder.rippleView.getTag()).imagePath.hashCode());
                    }
                    else
                    {
                        bitmap = extractAndSavePhoto(mHolder);
                    }
                }
                else if(mFileManager.getType() == FileManager.VIDEO)
                {
                    if (new File(App.TEMP_MEDIA_FOLDER + ((DataStructure) mHolder.rippleView.getTag()).imagePath.hashCode() + ".jpg").exists())
                        bitmap = BitmapFactory.decodeFile(App.TEMP_MEDIA_FOLDER + ((DataStructure) mHolder.rippleView.getTag()).imagePath.hashCode() + ".jpg");
                    else bitmap = extractAndSaveVideo(mHolder);
                }

                if(bitmap == null)
                {
                    removeItemFromList(mHolder);
                    return;
                }

                mHolder.filesCount.setTag(bitmap);
                mActivity.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        mHolder.gameImage.setImageBitmap((Bitmap) mHolder.filesCount.getTag());//URI(Uri.fromFile(new File(((DataStructure)mHolder.rippleView.getTag()).imagePath)));
//                        mHolder.filesCount.setTag(null);
                        mHolder.gameImage.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.fade_in));
                        mHolder.gameImage.invalidate();
                    }
                });
            }
            catch (Throwable err)
            {
                err.printStackTrace();
            }

        }
    }

    private Bitmap extractAndSavePhoto(DataObjectHolder holder) throws Throwable
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(((DataStructure) holder.rippleView.getTag()).imagePath, options);

        int widthRatio = (int) (((float) options.outWidth) / ((float) tileWidthHeight));
        int heightRatio = (int) (((float) options.outHeight) / ((float) tileWidthHeight));
        options.inJustDecodeBounds = false;
        options.inScaled = false;

        if (widthRatio > heightRatio)
            options.inSampleSize = widthRatio;
        else options.inSampleSize = heightRatio;

        Bitmap temp = BitmapFactory.decodeFile(((DataStructure) holder.rippleView.getTag()).imagePath, options);
        float scaleFactor = ((float)temp.getWidth()) / ((float)temp.getHeight());
        Bitmap bitmap;
        if(temp.getWidth() > temp.getHeight())
        {
            bitmap = Bitmap.createScaledBitmap(temp, (int) (((float)tileWidthHeight) * scaleFactor), tileWidthHeight, true);
        }
        else
        {
            bitmap = Bitmap.createScaledBitmap(temp, tileWidthHeight, (int) (((float)tileWidthHeight) / scaleFactor), true);
        }

        if(bitmap != temp)
            temp.recycle();

        String frameOutPath;
        OutputStream outputStream = new FileOutputStream(frameOutPath = App.TEMP_MEDIA_FOLDER + ((DataStructure) holder.rippleView.getTag()).imagePath.hashCode());
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        outputStream.flush();
        outputStream.close();
        bitmap.recycle();

        rotateUploadedPhoto(new File(frameOutPath), ((DataStructure) holder.rippleView.getTag()).imagePath);

        return BitmapFactory.decodeFile(frameOutPath);
    }

    private Bitmap extractAndSaveVideo(DataObjectHolder holder) throws Throwable
    {
        String frameOutPath = App.TEMP_MEDIA_FOLDER + ((DataStructure) holder.rippleView.getTag()).imagePath.hashCode() +".jpg";
        if(new File(frameOutPath).exists())
            new File(frameOutPath).delete();
        int ret = FFMPEG.getInstance(mActivity).extractFrameFromVideo(((DataStructure) holder.rippleView.getTag()).imagePath, frameOutPath);

        if(ret == 1)
            return null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(frameOutPath, options);

        int widthRatio = (int) (((float) options.outWidth) / ((float) tileWidthHeight));
        int heightRatio = (int) (((float) options.outHeight) / ((float) tileWidthHeight));
        options.inJustDecodeBounds = false;
        options.inScaled = false;

        if (widthRatio > heightRatio)
            options.inSampleSize = widthRatio;
        else options.inSampleSize = heightRatio;

        Bitmap temp = BitmapFactory.decodeFile(frameOutPath, options);
        float scaleFactor = ((float) temp.getWidth()) / ((float) temp.getHeight());

        Bitmap bitmap;
        if (temp.getWidth() > temp.getHeight())
        {
            bitmap = Bitmap.createScaledBitmap(temp, (int) (((float) tileWidthHeight) * scaleFactor), tileWidthHeight, true);
        }
        else
        {
            bitmap = Bitmap.createScaledBitmap(temp, tileWidthHeight, (int) (((float) tileWidthHeight) / scaleFactor), true);
        }

        if (bitmap != temp)
            temp.recycle();

        OutputStream outputStream = new FileOutputStream(frameOutPath);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        bitmap.recycle();
        outputStream.flush();
        outputStream.close();

        return BitmapFactory.decodeFile(frameOutPath);
    }

    private synchronized void removeItemFromList(DataObjectHolder holder)
    {
        int index = mDataSet.indexOf(holder.rippleView.getTag());
        if(index == -1)return;
//        if(index <= lastIndex)
        mActivity.runOnUiThread(new removeItem(index));
    }

    private class removeItem implements Runnable
    {
        final int mIndex;

        private removeItem(int index)
        {
            mIndex = index;
        }

        public void run()
        {
//            notifyItemRemoved(mIndex);
            try{mDataSet.remove(mIndex);}catch (Throwable err){}
            try{notifyItemRangeChanged(mIndex, 1);}catch (Throwable err){}
            try{notifyDataSetChanged();}catch (Throwable err){}
        }
    }

    private void rotateUploadedPhoto(File uploadedPhoto, String originalFilePath) throws Throwable
    {
        ExifInterface exifInterface = new ExifInterface(originalFilePath);
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);

        if (orientation != -1)
        {
            Bitmap canvasBitmap = null;
            Matrix matrix = new Matrix();
            Bitmap bitmap = BitmapFactory.decodeFile(uploadedPhoto.getPath());

            switch (orientation)
            {
                case ExifInterface.ORIENTATION_ROTATE_180:
                    canvasBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                    matrix.postRotate(180, bitmap.getWidth() / 2, bitmap.getHeight() / 2);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    canvasBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
                    matrix.postRotate(90, bitmap.getHeight() / 2, bitmap.getWidth() / 2);
                    matrix.postTranslate((canvasBitmap.getWidth() - bitmap.getWidth()) / 2, (bitmap.getHeight() - canvasBitmap.getHeight()) / 2);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    canvasBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
                    matrix.postRotate(270, bitmap.getHeight() / 2, bitmap.getWidth() / 2);
                    matrix.postTranslate((bitmap.getWidth() - canvasBitmap.getWidth()) / 2, (canvasBitmap.getHeight() - bitmap.getHeight()) / 2);
                    break;

                case ExifInterface.ORIENTATION_UNDEFINED:
                    String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};
                    Uri imageUri = Uri.fromFile(new File(originalFilePath));

                    Cursor cursor = mActivity.getContentResolver().query(imageUri, projection, null, null, null);
                    orientation = -1;
                    if (cursor != null && cursor.moveToFirst())
                    {
                        orientation = cursor.getInt(0);
                        cursor.close();
                    }

                    if (orientation == 90)
                    {
                        canvasBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
                        matrix.postRotate(orientation, bitmap.getHeight() / 2, bitmap.getWidth() / 2);
                        matrix.postTranslate((canvasBitmap.getWidth() - bitmap.getWidth()) / 2, (bitmap.getHeight() - canvasBitmap.getHeight()) / 2);
                    }
                    else if (orientation == 270)
                    {
                        canvasBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
                        matrix.postRotate(orientation, bitmap.getHeight() / 2, bitmap.getWidth() / 2);
                        matrix.postTranslate((bitmap.getWidth() - canvasBitmap.getWidth()) / 2, (canvasBitmap.getHeight() - bitmap.getHeight()) / 2);
                    }
                    else if (orientation == 180)
                    {
                        canvasBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                        matrix.postRotate(180, bitmap.getWidth() / 2, bitmap.getHeight() / 2);
                    }

                    break;
            }

            if (canvasBitmap == null)
            {
                bitmap.recycle();
                return;
            }
            Canvas canvas = new Canvas(canvasBitmap);
            canvas.drawColor(Color.BLUE);
            canvas.drawBitmap(bitmap, matrix, null);

            bitmap.recycle();

            FileOutputStream fos = new FileOutputStream(uploadedPhoto);
            canvasBitmap.compress(Bitmap.CompressFormat.JPEG, 85, fos);
            fos.flush();
            fos.close();

            canvasBitmap.recycle();
        }
        exifInterface = new ExifInterface(uploadedPhoto.getPath());
        exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, 1 + "");
        exifInterface.saveAttributes();
    }

}