package fm.bling.blingy.inviteFriends;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import fm.bling.blingy.App;
import fm.bling.blingy.R;
import fm.bling.blingy.inviteFriends.adapters.RecyclerAdapterSearch;
import fm.bling.blingy.inviteFriends.listeners.SearchListener;
import fm.bling.blingy.inviteFriends.model.ContactInfo;
import fm.bling.blingy.rest.model.User;
import fm.bling.blingy.utils.imagesManagement.ImageLoaderManager;
import fm.bling.blingy.utils.imagesManagement.views.ClickableView;


/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Ben Levi on 2/14/16.
 * History:
 * ***********************************
 */
public class InviteTabContentSearch extends InviteTabContentBase
{
    private EditText searchBox;
    private LinearLayout searchBoxAllLlinearLayout;
    private ClickableView searchButton;
    private Paint unFocusePaint, focusedPaint;
    private InputMethodManager inputMethodService;

    private RecyclerView mRecyclerView;
    private RecyclerAdapterSearch mAdapter;
    private LinearLayoutManager mLayoutManager;
    private static ArrayList<ContactInfo> mDataSet, searchDataSet;

    public InviteTabContentSearch(Context context, Paint paint, ArrayList<ContactInfo> dataSet, List<User> dataSetChosenFriends,
                                  ImageLoaderManager imageLoaderManager, SearchListener searchListener)
    {
        super(context, paint, true);
        unFocusePaint = paint;
        focusedPaint = new Paint(unFocusePaint);
        focusedPaint.setStrokeWidth(unFocusePaint.getStrokeWidth() * 2);
        focusedPaint.setColor(context.getResources().getColor(R.color.main_blue));
        inputMethodService = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

        if(mDataSet == null)
        {
            mDataSet = new ArrayList<>();
            mDataSet.addAll(dataSet);
            if (dataSetChosenFriends != null && dataSetChosenFriends.size() > 0)
                setDataSet(dataSetChosenFriends);
        }
        searchDataSet = new ArrayList<>();
        for (int i = 0; i < mDataSet.size(); i++)
            searchDataSet.add(mDataSet.get(i));

        setFirstLine(context);
        setContent(context, imageLoaderManager, searchListener);
    }

    private void setDataSet(List<User> dataSetChosenFriends)
    {
        int i;
        ContactInfo ci;
        for (User user : dataSetChosenFriends)
        {
            i = 0;
            for (; i < mDataSet.size(); i++)
            {
                if ( ( ci = mDataSet.get(i)).name.equals(user.getFullName()))
                {
                    ci.isOnChosen = true;
                    break;
                }
                else if (ci.name.compareToIgnoreCase(user.getFullName()) > 0)
                {
                    mDataSet.add(i, new ContactInfo(ContactInfo.NOT_FROM_CONTACT_ID, user.getFullName(), user.getPhoto(), null,
                            user.getFollowedByMe().equals("1")?ContactInfo.FOLLOWING:ContactInfo.NOT_FOLLOWING, true, user.getUserId()));
                    break;
                }
            }
            if(i ==  mDataSet.size())
            {
                mDataSet.add(new ContactInfo(ContactInfo.NOT_FROM_CONTACT_ID, user.getFullName(), user.getPhoto(), null,
                        user.getFollowedByMe().equals("1")?ContactInfo.FOLLOWING:ContactInfo.NOT_FOLLOWING, true, user.getUserId()));
            }
        }
    }

    private void setFirstLine(Context context)
    {
        int searchButtonWidthHeight = context.getResources().getDimensionPixelSize(R.dimen.invite_second_row_height) - (context.getResources().getDimensionPixelSize(R.dimen.main_margin));

        searchBox = new EditText(context);
        searchBox.setBackgroundColor(context.getResources().getColor(R.color.transparent));
        searchBox.setTypeface(App.ROBOTO_REGULAR);
        searchBox.setHint(R.string.search);
        searchBox.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        searchBox.setPadding(context.getResources().getDimensionPixelSize(R.dimen.blocks_margin), 0, 0, 0);
        searchBox.setInputType(InputType.TYPE_CLASS_TEXT);
        searchBox.setTextColor(context.getResources().getColor(R.color.grey_font));
        searchBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(R.dimen.font_description));
        searchBox.setGravity(Gravity.LEFT | Gravity.BOTTOM);
        searchBox.setSingleLine();
        searchBox.setIncludeFontPadding(false);
        searchBox.setOnFocusChangeListener(new OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if (hasFocus)
                {
                    mPaint = focusedPaint;
                    searchBox.setTextColor(getContext().getResources().getColor(R.color.black));
                }
                else
                {
                    mPaint = unFocusePaint;
                    searchBox.setTextColor(getContext().getResources().getColor(R.color.grey_font));
                    inputMethodService.hideSoftInputFromWindow(searchBox.getWindowToken(), 0);
                }
                firstLine.invalidate();
            }
        });
        searchBox.addTextChangedListener(new TextWatcher()
        {
            int lastSearchLength;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

                String search = s.toString().toLowerCase();
                if(lastSearchLength < search.length())
                {
                    for(int i = searchDataSet.size() - 1; i > -1; i--)
                    {
                        if(searchDataSet.get(i).name.toLowerCase().indexOf(search) == -1)
                        {
                            searchDataSet.remove(i);
                        }
                    }
                }
                else
                {
                    searchDataSet.clear();
                    if(search.length() > 0)
                    {
                        for(int i=0; i < mDataSet.size(); i++)
                        {
                            if(mDataSet.get(i).name.toLowerCase().indexOf(search) != -1)
                                searchDataSet.add(mDataSet.get(i));
                        }
                    }
                    else
                    {
                        for(int i=0; i < mDataSet.size(); i++)
                        {
                            searchDataSet.add(mDataSet.get(i));
                        }
                    }
                }
                mAdapter.notifyDataSetChanged();
                lastSearchLength = search.length();
            }

            @Override
            public void afterTextChanged(Editable s)
            {
            }

        });

        searchBoxAllLlinearLayout = new LinearLayout(context);
        searchBoxAllLlinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        searchBoxAllLlinearLayout.setGravity(Gravity.BOTTOM | Gravity.LEFT);
        searchBoxAllLlinearLayout.setPadding(padding, 0, padding, padding / 2);

        searchButton = new ClickableView(context);
        searchButton.setLayoutParams(new LinearLayout.LayoutParams(searchButtonWidthHeight, searchButtonWidthHeight));
        searchButton.setBackgroundResource(R.drawable.ic_search_white_36dp);
        searchButton.paintPixels(context.getResources().getColor(R.color.list_separator_color));
        searchButton.setClickable(true);

        searchBoxAllLlinearLayout.addView(searchButton);
        searchBoxAllLlinearLayout.addView(searchBox);
        firstLine.addView(searchBoxAllLlinearLayout);
    }

    private void setContent(Context context, ImageLoaderManager imageLoaderManager, SearchListener searchListener)
    {
        mRecyclerView = new RecyclerView(context);
        mRecyclerView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        //  mRecyclerView.setScrollIndicators(0,0);//s(0);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new RecyclerAdapterSearch(searchDataSet, imageLoaderManager, searchListener);
        mRecyclerView.setAdapter(mAdapter);

        addView(mRecyclerView);
    }

    public void close()
    {
        super.close();

        try { searchBoxAllLlinearLayout.removeAllViews();searchBoxAllLlinearLayout.destroyDrawingCache();}catch (Throwable xz){}searchBoxAllLlinearLayout = null;
        searchButton.close();searchButton = null;
        searchBox = null;
        unFocusePaint = null;
        focusedPaint  = null;
        mRecyclerView.removeAllViews(); mRecyclerView.destroyDrawingCache();mRecyclerView = null;
        mAdapter.close();
        mLayoutManager = null;
        searchDataSet.clear(); searchDataSet = null;
        inputMethodService = null;
    }

    public void setData(ArrayList<ContactInfo> dataSet, List<User> data)
    {
        mDataSet = dataSet;
        if(data == null || data.size() == 0)
            return;
        setDataSet(data);
        searchDataSet.clear();
        for(int i=0; i< mDataSet.size();i++)
            searchDataSet.add(mDataSet.get(i));
        mAdapter.notifyDataSetChanged();
    }

    public void notifyDataSetChanged()
    {
        post(new Runnable()
        {
            @Override
            public void run()
            {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    public void hideKeyboard()
    {
        if(inputMethodService != null)
            inputMethodService.hideSoftInputFromWindow(searchBox.getWindowToken(), 0);
    }
}