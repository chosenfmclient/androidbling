package fm.bling.blingy.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.io.IOException;

import fm.bling.blingy.R;
import fm.bling.blingy.singletones.SharedPreferencesManager;
import fm.bling.blingy.tracking.EventConstants;
import fm.bling.blingy.tracking.TrackingManager;
import fm.bling.blingy.utils.Constants;

/**
 * *********************************
 * Project: Blin.gy Android Application
 * Description:
 * Created by Oren Zakay on 12/01/17.
 * History:
 * ***********************************
 */
public class VideoTutorialDialog extends Dialog {

    private Context mContext;
    private FrameLayout mMainContainer;
    private TextureView mTextureView;
    private MediaPlayer mMediaPlayer;
    private boolean isSkipable;

    public VideoTutorialDialog(Context context, boolean isSkipable) {
        super(context, R.style.FullScreen);
        this.mContext = context;
        this.isSkipable = isSkipable;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.video_tutorial_dialog);
        mMainContainer = (FrameLayout)findViewById(R.id.main_container);
        mTextureView = (TextureView)findViewById(R.id.texture_view_video);
        ImageView mSkip = (ImageView) findViewById(R.id.skip);
        adjustTextureViewSize();
        initilizeFirstVideo();
        hideNavigation();
        if(this.isSkipable) {
            mSkip.setVisibility(View.VISIBLE);
            mSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }
        else
            setCancelable(false);
        TrackingManager.getInstance().appseeStartScreen(EventConstants.RECORD_VIDEO_TUTORIAL);
    }

    private void initilizeFirstVideo() {
        Uri mFirstUri = Uri.parse("android.resource://" + mContext.getPackageName() + "/" + R.raw.background_tutorial2);
        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(mContext, mFirstUri);
            mMediaPlayer.prepareAsync();
            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if(VideoTutorialDialog.this.isShowing()) {
                        SharedPreferencesManager.getEditor().putBoolean(Constants.BACKGROUND_TUTORIAL, false).commit();
                        dismiss();
                    }
                }
            });
            attachSurface();
            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(mMediaPlayer != null)
                                mMediaPlayer.start();
                        }
                    },10);
                }
            });
            mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    if(mp != null)
                        mp.setOnErrorListener(null);
                    releaseMediaPlayer();
                    initilizeFirstVideo();
                    return true;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        show();
    }

    @Override
    public void show() {
        super.show();
        Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        mMainContainer.startAnimation(anim);
        anim.startNow();
    }

    public void dismiss()
    {
        releaseMediaPlayer();
        if (mContext != null)
        {
            Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
            anim.setDuration(600);
            anim.setAnimationListener(new Animation.AnimationListener()
            {
                @Override
                public void onAnimationStart(Animation animation)
                {
                }

                @Override
                public void onAnimationEnd(Animation animation)
                {
                    superDismiss();
                }

                @Override
                public void onAnimationRepeat(Animation animation)
                {
                }
            });
            mMainContainer.startAnimation(anim);
        }
        else {
            superDismiss();
        }
    }

    public void superDismiss()
    {
        releaseMediaPlayer();
        super.dismiss();
    }

    public void stopMediaplayer(){
        try {
            if (mMediaPlayer != null && mMediaPlayer.isPlaying())
                mMediaPlayer.pause();
        }catch (Throwable throwable){
            throwable.printStackTrace();
        }
    }

    public void resumeMediaPlayer(){
        try {
            if (mMediaPlayer != null && !mMediaPlayer.isPlaying())
                mMediaPlayer.start();
        }catch (Throwable throwable){
            throwable.printStackTrace();
        }
    }

    private void attachSurface() {
        try {
            if(mTextureView == null)
                mTextureView = (TextureView)findViewById(R.id.texture_view_video);

            if (mTextureView.isAvailable()) {
                mMediaPlayer.setSurface(new Surface(mTextureView.getSurfaceTexture()));
            } else {
                mTextureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
                    @Override
                    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                        if(mMediaPlayer != null){
                            mMediaPlayer.setSurface(new Surface(mTextureView.getSurfaceTexture()));
                        }
                    }

                    @Override
                    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                    }

                    @Override
                    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                        return false;
                    }

                    @Override
                    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
                    }
                });
            }
        }catch (Throwable throwable){throwable.printStackTrace();}
    }

    private void adjustTextureViewSize() {
        float viewHeight = mContext.getResources().getDisplayMetrics().heightPixels * 0.63f;
        float scaleH = viewHeight / 840;
        float viewWidth = scaleH * 640;


        mTextureView.getLayoutParams().width = (int)viewWidth;
        mTextureView.getLayoutParams().height = (int)viewHeight;
    }

    private void releaseMediaPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer = null;
        }
    }

    public void hideNavigation(){
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}
