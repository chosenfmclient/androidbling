package fm.bling.blingy.profile.model;

import com.google.gson.annotations.SerializedName;


/**
 * *********************************
 * Project: Chosen Android Application
 * FileName: CAAPhotoEditProfile
 * Description:
 * Created by Dawidowicz Nadav on 7/16/15.
 * History:
 * ***********************************
 */
public class CAAPhotoEditProfile {
    @SerializedName("photo")
    private CAAMethod photo;

    public CAAMethod getPhoto() {
        return photo;
    }

    public void setPhoto(CAAMethod photo) {
        this.photo = photo;
    }


}
