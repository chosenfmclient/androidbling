package fm.bling.blingy.utils.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import fm.bling.blingy.App;

/**
 * *********************************
 * Project: Chosen Android Application
 * Description:
 * Created by Oren Zakay on 11/28/16.
 * History:
 * ***********************************
 */
public class TextViewBold extends TextView {
    public TextViewBold(Context context) {
        super(context);
        this.setTypeface(App.ROBOTO_BOLD);
    }

    public TextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(App.ROBOTO_BOLD);
    }

    public TextViewBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setTypeface(App.ROBOTO_BOLD);
    }
}
