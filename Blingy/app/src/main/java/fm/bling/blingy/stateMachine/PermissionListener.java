package fm.bling.blingy.stateMachine;

/**
 * Created by Chosen-pro on 06/07/2016.
 */

public interface PermissionListener {
    void onPermissionGranted();
    void onPermissionDenied();
}
