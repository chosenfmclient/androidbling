package org.opencv.android;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.media.ExifInterface;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.OutputStream;
import java.util.List;

/**
 * This class is an implementation of the Bridge View between OpenCV and Java Camera.
 * This class relays on the functionality available in base class and only implements
 * required functions:
 * connectCamera - opens Java camera and sets the PreviewCallback to be delivered.
 * disconnectCamera - closes the camera and stops preview.
 * When frame is delivered via callback from Camera - it processed via OpenCV to be
 * converted to RGBA32 and then passed to the external callback for modifications if required.
 */
public class JavaCameraView extends CameraBridgeViewBase implements PreviewCallback, Camera.FaceDetectionListener
{
    private static final int MAGIC_TEXTURE_ID = 10;
    private static final String TAG = "JavaCameraView";

    private byte mBuffer[];
    private Mat[] mFrameChain;
    private int mChainIdx = 0;
    private Thread mThread;
    private boolean mStopThread;

    protected Camera mCamera;
    protected JavaCameraFrame[] mCameraFrame;
    private SurfaceTexture mSurfaceTexture;

    public static String errorMessage = "";

    public static class JavaCameraSizeAccessor implements ListItemAccessor
    {

        @Override
        public int getWidth(Object obj) {
            Camera.Size size = (Camera.Size) obj;
            return size.width;
        }

        @Override
        public int getHeight(Object obj) {
            Camera.Size size = (Camera.Size) obj;
            return size.height;
        }
    }

    public JavaCameraView(Context context, int cameraId) {
        super(context, cameraId);
    }

    public JavaCameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected boolean initializeCamera(int width, int height) {
        Log.d(TAG, "Initialize java camera");
        boolean result = true;
        synchronized (this) {
            mCamera = null;

            if (mCameraIndex == CAMERA_ID_ANY) {
                Log.d(TAG, "Trying to open camera with old open()");
                try {
                    mCamera = Camera.open();
                }
                catch (Exception e){
                    Log.e(TAG, "Camera is not available (in use or does not exist): " + e.getLocalizedMessage());
                }

                if(mCamera == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                    boolean connected = false;
                    for (int camIdx = 0; camIdx < Camera.getNumberOfCameras(); ++camIdx) {
                        Log.d(TAG, "Trying to open camera with new open(" + Integer.valueOf(camIdx) + ")");
                        try {
                            mCamera = Camera.open(camIdx);
                            connected = true;
                        } catch (RuntimeException e) {
                            Log.e(TAG, "Camera #" + camIdx + "failed to open: " + e.getLocalizedMessage());
                        }
                        if (connected) break;
                    }
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                    int localCameraIndex = mCameraIndex;
                    if (mCameraIndex == CAMERA_ID_BACK) {
                        Log.i(TAG, "Trying to open back camera");
                        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                        for (int camIdx = 0; camIdx < Camera.getNumberOfCameras(); ++camIdx) {
                            Camera.getCameraInfo( camIdx, cameraInfo );
                            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                                localCameraIndex = camIdx;
                                break;
                            }
                        }
                    } else if (mCameraIndex == CAMERA_ID_FRONT) {
                        Log.i(TAG, "Trying to open front camera");
                        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                        for (int camIdx = 0; camIdx < Camera.getNumberOfCameras(); ++camIdx) {
                            Camera.getCameraInfo( camIdx, cameraInfo );
                            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                                localCameraIndex = camIdx;
                                break;
                            }
                        }
                    }
                    if (localCameraIndex == CAMERA_ID_BACK) {
                        Log.e(TAG, "Back camera not found!");
                    } else if (localCameraIndex == CAMERA_ID_FRONT) {
                        Log.e(TAG, "Front camera not found!");
                    } else {
                        Log.d(TAG, "Trying to open camera with new open(" + Integer.valueOf(localCameraIndex) + ")");
                        try {
                            mCamera = Camera.open(localCameraIndex);
                        } catch (RuntimeException e) {
                            Log.e(TAG, "Camera #" + localCameraIndex + "failed to open: " + e.getLocalizedMessage());
                        }
                    }
                }
            }

            if (mCamera == null)
            {
                errorMessage = "mCamera == null, line 138";

                return false;
            }

            /* Now set camera parameters */
            try {
                Camera.Parameters params = mCamera.getParameters();
                Log.d(TAG, "getSupportedPreviewSizes()");
                List<Camera.Size> sizes = params.getSupportedPreviewSizes();

                boolean isFaceDetectionAvailable = false;//params.getMaxNumDetectedFaces() != 0;

                if (sizes != null) {
                    /* Select the size that fits surface considering maximum size allowed */
//                    Size frameSize = calculateCameraFrameSize(sizes, new JavaCameraSizeAccessor(), width, height);

                    params.setPreviewFormat(ImageFormat.NV21);

//                    Log.d(TAG, "Set preview size to " + Integer.valueOf((int)frameSize.width) + "x" + Integer.valueOf((int)frameSize.height));
//                    params.setPreviewSize((int)frameSize.width, (int)frameSize.height);

//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH && !Build.MODEL.equals("GT-I9100"))
//                        params.setRecordingHint(true);

                    List<String> FocusModes = params.getSupportedFocusModes();
                    if (FocusModes != null && FocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO))
                    {
                        params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                    }
//                    params.setPreviewSize(1280, 960);

//                    params.setExposureCompensation(-10);
//                    params.setAutoExposureLock(true);
//                    params.setAutoWhiteBalanceLock(true);

//                    params.setExposureCompensation(-5);


//                    new Thread()
//                    {
//                        public void run()
//                        {
//                            try{Thread.sleep(1500);}catch (Throwable err){}
//                            Camera.Parameters params = mCamera.getParameters();
//                            params.setAutoExposureLock(true);
//
//                            params.setAutoWhiteBalanceLock(true);
//
//                            mCamera.setParameters(params);
//                        }
//                    }.start();

//                    params.setAutoWhiteBalanceLock(true);


                    int cameraRes = setPreviewSize(params);

                    try { mCamera.setParameters(params); }
                    catch (Throwable err)
                    {
                        cameraRes = setPreviewSizeII(params);
                        try { mCamera.setParameters(params); }
                        catch (Throwable err1)
                        {
                            cameraRes = setPreviewSizeIII(params);
                            mCamera.setParameters(params);
                        }
                    }

                    if(mOnCameraStartingListener != null)
                    {
                        mOnCameraStartingListener.onSetCameraRes(cameraRes, cameraWidth, cameraHeight);
                    }
                    params = mCamera.getParameters();




                    mFrameWidth = params.getPreviewSize().width;
                    mFrameHeight = params.getPreviewSize().height;

                    if (true || ( (getLayoutParams().width == LayoutParams.MATCH_PARENT) && (getLayoutParams().height == LayoutParams.MATCH_PARENT) ) )
                        mScale = Math.min(((float)height)/mFrameHeight, ((float)width)/mFrameWidth);
                    else
                        mScale = 0;

                    if (mFpsMeter != null) {
                        mFpsMeter.setResolution(mFrameWidth, mFrameHeight);
                    }

                    int size = mFrameWidth * mFrameHeight;
                    size  = size * ImageFormat.getBitsPerPixel(params.getPreviewFormat()) / 8;
                    mBuffer = new byte[size];

                    mCamera.addCallbackBuffer(mBuffer);
                    mCamera.setPreviewCallbackWithBuffer(this);

                    mFrameChain = new Mat[2];
                    mFrameChain[0] = new Mat(mFrameHeight + (mFrameHeight/2), mFrameWidth, CvType.CV_8UC1);
                    mFrameChain[1] = new Mat(mFrameHeight + (mFrameHeight/2), mFrameWidth, CvType.CV_8UC1);

                    AllocateCache();

                    mCameraFrame = new JavaCameraFrame[2];
                    mCameraFrame[0] = new JavaCameraFrame(mFrameChain[0], mFrameWidth, mFrameHeight);
                    mCameraFrame[1] = new JavaCameraFrame(mFrameChain[1], mFrameWidth, mFrameHeight);

                    if(isFaceDetectionAvailable)
                        mCamera.setFaceDetectionListener(this);
                    setConverter();

//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    mSurfaceTexture = new SurfaceTexture(MAGIC_TEXTURE_ID);
                    mCamera.setPreviewTexture(mSurfaceTexture);

//                    } else
//                       mCamera.setPreviewDisplay(null);

                    /* Finally we are ready to start the preview */
                    Log.d(TAG, "startPreview");
                    mCamera.startPreview();

                    if(isFaceDetectionAvailable)
                        mCamera.startFaceDetection();
                }
                else
                {
                    result = false;
                    errorMessage = "Camera preview sizes is null";
                }
            } catch (Exception e) {
                result = false;
                e.printStackTrace();
                errorMessage = (e.getCause() != null ? "cuase : " + e.getCause() : "" )
                                + (e.getMessage() != null ? "message : " + e.getMessage() : "" )
                                + (e.getLocalizedMessage() != null ? "localized msg : " + e.getLocalizedMessage() : "" )
                                + (e.toString() != null ? "raw msg : " + e.toString() : "" );

            }
        }

        return result;
    }

    protected void releaseCamera() {
        synchronized (this) {
            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);

                mCamera.release();
            }
            mCamera = null;
            if (mFrameChain != null) {
                mFrameChain[0].release();
                mFrameChain[1].release();
            }
            if (mCameraFrame != null) {
                mCameraFrame[0].release();
                mCameraFrame[1].release();
            }
        }
    }

    private boolean mCameraFrameReady = false;

    @Override
    protected boolean connectCamera(int width, int height) {

        /* 1. We need to instantiate camera
         * 2. We need to start thread which will be getting frames
         */
        /* First step - initialize camera connection */
        Log.d(TAG, "Connecting to camera");
        if (!initializeCamera(width, height))
            return false;

        mCameraFrameReady = false;

        /* now we can start update thread */
        Log.d(TAG, "Starting processing thread");
        mStopThread = false;
        mThread = new Thread(new CameraWorker());
        mThread.start();

        return true;
    }

    @Override
    protected void disconnectCamera() {
        /* 1. We need to stop thread which updating the frames
         * 2. Stop camera and release it
         */
        Log.d(TAG, "Disconnecting from camera");
        try {
            mStopThread = true;
            Log.d(TAG, "Notify thread");
            synchronized (this) {
                this.notify();
            }
            Log.d(TAG, "Wating for thread");
            if (mThread != null)
                mThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            mThread =  null;
        }

        /* Now release camera */
        releaseCamera();

        mCameraFrameReady = false;
    }

    @Override
    public void onPreviewFrame(byte[] frame, Camera arg1)
    {
//        Log.d(TAG, "Preview Frame received. Frame size: " + frame.length);
        synchronized (this) {
            mFrameChain[mChainIdx].put(0, 0, frame);
            mCameraFrameReady = true;
            this.notify();
        }
        if (mCamera != null)
            mCamera.addCallbackBuffer(mBuffer);
    }

    private class JavaCameraFrame implements CvCameraViewFrame
    {
        @Override
        public Mat gray() {
            return mYuvFrameData.submat(0, mHeight, 0, mWidth);
        }

        @Override
        public Mat rgba() {
            Imgproc.cvtColor(mYuvFrameData, mRgba, Imgproc.COLOR_YUV2RGBA_NV21, 4);
            return mRgba;
        }

        public JavaCameraFrame(Mat Yuv420sp, int width, int height) {
            super();
            mWidth = width;
            mHeight = height;
            mYuvFrameData = Yuv420sp;
            mRgba = new Mat();
        }

        public void release() {
            mRgba.release();
        }

        private Mat mYuvFrameData;
        private Mat mRgba;
        private int mWidth;
        private int mHeight;
    };

    private class CameraWorker implements Runnable {

        @Override
        public void run() {
            do {
                boolean hasFrame = false;
                synchronized (JavaCameraView.this) {
                    try {
                        while (!mCameraFrameReady && !mStopThread) {
                            JavaCameraView.this.wait();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (mCameraFrameReady)
                    {
                        mChainIdx = 1 - mChainIdx;
                        mCameraFrameReady = false;
                        hasFrame = true;
                    }
                }

                if (!mStopThread && hasFrame) {
                    if (!mFrameChain[1 - mChainIdx].empty())
                        deliverAndDrawFrame(mCameraFrame[1 - mChainIdx]);
                }
            } while (!mStopThread);
            Log.d(TAG, "Finish processing thread");
        }
    }

    ////////BLINGy/////////////ADDED BY BEN////////////////////////
    private final int WIDTH = 640, HEIGHT = 480;
    private Camera.Face face;
    private Matrix converterMatrix;
    private RectF rectf = new RectF();
    public int faceLeft, faceTop, faceRight, faceBottom;

    @Override
    public void onFaceDetection(Camera.Face[] faces, Camera camera)
    {
        try
        {
            if (faces.length > 0)
                face = faces[0];
            else face = null;

            if (face != null)
            {
                rectf.left = face.rect.left;
                rectf.top = face.rect.top;
                rectf.right = face.rect.right;
                rectf.bottom = face.rect.bottom;
                converterMatrix.mapRect(rectf);

                faceLeft = (int) rectf.left;
                faceTop = (int) rectf.top;
                faceRight = (int) rectf.right;
                faceBottom = (int) rectf.bottom;


//                if (shouldAddBoundsAdjustment)
//                {
//                    rectf.left -= boundAdjustAddon;
//                    rectf.top -= boundAdjustAddon;
//                    rectf.right += boundAdjustAddon;
//                    rectf.bottom += boundAdjustAddon;
//
//                    if (shouldAddToToSideundsAdjustment)
//                    {
//                        rectf.top -= boundSidesAdjustAddon;
//                        rectf.bottom -= boundSidesAdjustAddon;
//                    }
//                }

            }
            else
            {
                faceBottom = faceRight = faceTop = faceLeft = 0;

//////////////// we are checking only the left, to check if face is detected in the native code

//                faceTop = (int) rectf.top;
//                faceRight = (int) rectf.right;
//                faceBottom = (int) rectf.bottom;
            }
        }
        catch (Throwable err)
        {
            err.printStackTrace();
        }
    }

    private void setConverter()
    {
        converterMatrix = new Matrix();
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(getSystemCamaeraId(), info);
        converterMatrix.postScale(((float) WIDTH) / 2000f, ((float) HEIGHT) / 2000f);
        converterMatrix.postTranslate(((float) WIDTH) / 2f, ((float) HEIGHT) / 2f);
    }

    private int getSystemCamaeraId()
    {
        if(mCameraIndex == CAMERA_ID_FRONT)
            return Camera.CameraInfo.CAMERA_FACING_FRONT;
        return Camera.CameraInfo.CAMERA_FACING_BACK;
    }

    public int getCameraRotation()
    {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(getSystemCamaeraId(), info);
        return info.orientation;
    }

    public boolean isMirrored()
    {
        return mCameraIndex == CAMERA_ID_FRONT;
    }

    public void turnOffImageAdjustments(final onImageAdjustmentsOff onImageAdjustmentsOffListener)
    {
//        if(true)return;
//
//        Camera.Parameters params = mCamera.getParameters();
//        params.setAutoExposureLock(true);
//        params.setExposureCompensation(params.getMinExposureCompensation());
//        params.setAutoWhiteBalanceLock(true);
//        mCamera.setParameters(params);



        Camera.Parameters params = mCamera.getParameters();
        params.setAutoExposureLock(false);
//        params.setExposureCompensation(-5);
//        params.setAutoWhiteBalanceLock(true);
        mCamera.setParameters(params);

        new Thread()
        {
            public void run()
            {
                try{Thread.sleep(500);}catch (Throwable err){}
                Camera.Parameters params = mCamera.getParameters();
                int exposureCompensation = params.getExposureCompensation();
                params.setAutoExposureLock(true);
                params.setAutoWhiteBalanceLock(true);

                mCamera.setParameters(params);
                onImageAdjustmentsOffListener.onActionDone(exposureCompensation, params.getMinExposureCompensation(), params.getMaxExposureCompensation());

            }
        }.start();
    }

    private OnCameraStartingListener mOnCameraStartingListener;
    public void setOnCameraStartingListener(OnCameraStartingListener onCameraStartingListener)
    {
        mOnCameraStartingListener = onCameraStartingListener;
    }

    public interface OnCameraStartingListener
    {
        public void onCameraStarting(Camera camera);

        public void onSetCameraRes(int cameraRes, int width, int height);
    }

    public final static int RES_960X720 = 1, RES_1280X960 = 2, RES_1280_720 = 3, RES_1024X768 = 4, RES_640X480 = 5, RES_CUSTOM = 0;
    private int cameraWidth, cameraHeight;
    /**
     * searching the sizes in this order, once one of these resolutions is found it returns
     * @param params
     * @return  the type @ProcessImage.RES_960X720  @ProcessImage.RES_1280X960 @ProcessImage.RES_1280_720
     */
    private int setPreviewSize(Camera.Parameters params)
    {
        List<Camera.Size> supportedPicturesSizes = params.getSupportedPreviewSizes();


        for(Camera.Size size : supportedPicturesSizes)
        {
            if(size.width == 960 && size.height == 720)
            {
                cameraWidth = 960;
                cameraHeight = 720;
                params.setPreviewSize(960, 720);
                return RES_960X720;
            }
        }

        for(Camera.Size size : supportedPicturesSizes)
        {
            if(size.width == 1024 && size.height == 768)
            {
                cameraWidth = 1024;
                cameraHeight = 768;
                params.setPreviewSize(1024, 768);
                return RES_1024X768;
            }
        }

        for(Camera.Size size : supportedPicturesSizes)
        {
            if(size.width == 1280 && size.height == 960)
            {
                cameraWidth = 1280;
                cameraHeight = 960;
                params.setPreviewSize(1280, 960);
                return RES_1280X960;
            }
        }

        for(Camera.Size size : supportedPicturesSizes)
        {
            if(size.width == 640 && size.height == 480)
            {
                cameraWidth = 640;
                cameraHeight = 480;
                params.setPreviewSize(640, 480);
                return RES_640X480;
            }
        }

        cameraWidth = 1280;
        cameraHeight = 720;
        params.setPreviewSize(1280, 720);
        return RES_1280_720;
    }

    private int setPreviewSizeII(Camera.Parameters params)
    {
        cameraWidth = 640;
        cameraHeight = 480;
        params.setPreviewSize(640, 480);
        return RES_640X480;
    }

    private int setPreviewSizeIII(Camera.Parameters params)
    {
        List<Camera.Size> supportedPicturesSizes = params.getSupportedPreviewSizes();

        for(Camera.Size size : supportedPicturesSizes)
        {
            cameraWidth = size.width;
            cameraHeight = size.height;
            params.setPreviewSize(size.width, size.height);
            return RES_CUSTOM;
        }
        return RES_CUSTOM;
    }

    public interface onImageAdjustmentsOff
    {
        void onActionDone(int exposureCompensation, int minExposure, int maxExposure);
    }

    public void releaseExposure()
    {
        Camera.Parameters params = mCamera.getParameters();
        params.setAutoExposureLock(false);
        mCamera.setParameters(params);
    }

    public int minExposure = 0, maxExposure = 0;
    public void getExposureLevel(final ExposureListener exposureListener)
    {
        mCamera.takePicture(null, null, null, new Camera.PictureCallback()
        {
            @Override
            public void onPictureTaken(byte[] data, Camera camera)
            {
                mCamera.startPreview();

                try
                {
                    OutputStream outputStream = getContext().openFileOutput("exposure.jpg", Context.MODE_PRIVATE);
                    outputStream.write(data);
                    outputStream.flush();
                    outputStream.close();

                    if(minExposure == 0)
                    {
                        minExposure = mCamera.getParameters().getMinExposureCompensation();
                        maxExposure = mCamera.getParameters().getMaxExposureCompensation();
                    }

                    android.media.ExifInterface exif = new android.media.ExifInterface(getContext().getFilesDir() + "/exposure.jpg");
                    String exposure = exif.getAttribute(android.media.ExifInterface.TAG_EXPOSURE_TIME);
                    String iso = exif.getAttribute(android.media.ExifInterface.TAG_ISO);
                    String whiteBalance = exif.getAttribute(ExifInterface.TAG_WHITE_BALANCE);
                    exposureListener.onExposureReceived(Double.parseDouble(exposure));
                }
                catch (Throwable err)
                { }

            }
        });
    }

    public void startCamera()
    {
        mCamera.startPreview();
    }

    public interface ExposureListener
    {
        void onExposureReceived(double exposureTime);
    }

}